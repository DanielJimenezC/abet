//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReunionProfesorAcuerdo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReunionProfesorAcuerdo()
        {
            this.ReunionProfesorAcuerdoHistorico = new HashSet<ReunionProfesorAcuerdoHistorico>();
            this.ReunionProfesorTarea = new HashSet<ReunionProfesorTarea>();
            this.ReunionProfesorTareaHistorico = new HashSet<ReunionProfesorTareaHistorico>();
        }
    
        public int IdReunionProfesorAcuerdo { get; set; }
        public int IdReunionProfesor { get; set; }
        public Nullable<int> IdReunionProfesorCompletada { get; set; }
        public string DescripcionEspanol { get; set; }
        public string DescripcionIngles { get; set; }
        public string ComentarioCierreEspanol { get; set; }
        public string ComentarioCierreIngles { get; set; }
        public Nullable<System.DateTime> FechaCierre { get; set; }
        public Nullable<int> IdEstado { get; set; }
        public Nullable<int> SemanaDeadline { get; set; }
        public Nullable<System.DateTime> FechaDeadline { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public System.DateTime FechaActualizacion { get; set; }
    
        public virtual ReunionProfesor ReunionProfesor { get; set; }
        public virtual ReunionProfesor ReunionProfesor1 { get; set; }
        public virtual ReunionProfesorEstado ReunionProfesorEstado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReunionProfesorAcuerdoHistorico> ReunionProfesorAcuerdoHistorico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReunionProfesorTarea> ReunionProfesorTarea { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReunionProfesorTareaHistorico> ReunionProfesorTareaHistorico { get; set; }
    }
}
