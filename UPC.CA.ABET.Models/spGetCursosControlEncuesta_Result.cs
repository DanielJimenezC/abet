//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class spGetCursosControlEncuesta_Result
    {
        public int IdCurso { get; set; }
        public string Codigo { get; set; }
        public string NombreEspanol { get; set; }
        public string LogroFinCicloEspanol { get; set; }
        public string LogroFinCicloIngles { get; set; }
        public int IdMallaCurricular { get; set; }
    }
}
