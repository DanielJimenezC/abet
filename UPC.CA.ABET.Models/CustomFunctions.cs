﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects.DataClasses;

namespace UPC.CA.ABET.Models
{
    public static class CustomFunctions
    {
        [DbFunction("AbetModel", "CastToInteger")]
        public static int CastToInteger(string expression)
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }
    }
}
