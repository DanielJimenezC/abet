//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Seccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Seccion()
        {
            this.AlumnoSeccion = new HashSet<AlumnoSeccion>();
            this.ArchivoTemplateEncuesta = new HashSet<ArchivoTemplateEncuesta>();
            this.ComentarioDelegado = new HashSet<ComentarioDelegado>();
            this.DocenteHorasProgramadas = new HashSet<DocenteHorasProgramadas>();
            this.DocenteSeccion = new HashSet<DocenteSeccion>();
            this.Encuesta = new HashSet<Encuesta>();
            this.Hallazgo = new HashSet<Hallazgo>();
            this.ObservacionDelegado = new HashSet<ObservacionDelegado>();
            this.ReunionProfesorHallazgo = new HashSet<ReunionProfesorHallazgo>();
            this.SeccionCurso = new HashSet<SeccionCurso>();
            this.SeccionPeriodoAcademico = new HashSet<SeccionPeriodoAcademico>();
            this.SeccionSede = new HashSet<SeccionSede>();
            this.TemplateEncuesta = new HashSet<TemplateEncuesta>();
        }
    
        public int IdSeccion { get; set; }
        public string Codigo { get; set; }
        public int IdSede { get; set; }
        public Nullable<bool> RevisionHorarioATiempo { get; set; }
        public Nullable<bool> EsMatriculaExtemporanea { get; set; }
        public int IdCursoPeriodoAcademico { get; set; }
        public Nullable<int> IdLogCarga { get; set; }
        public Nullable<int> IdModulo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlumnoSeccion> AlumnoSeccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArchivoTemplateEncuesta> ArchivoTemplateEncuesta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ComentarioDelegado> ComentarioDelegado { get; set; }
        public virtual CursoPeriodoAcademico CursoPeriodoAcademico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocenteHorasProgramadas> DocenteHorasProgramadas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocenteSeccion> DocenteSeccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Encuesta> Encuesta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hallazgo> Hallazgo { get; set; }
        public virtual LogCarga LogCarga { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ObservacionDelegado> ObservacionDelegado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReunionProfesorHallazgo> ReunionProfesorHallazgo { get; set; }
        public virtual Sede Sede { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeccionCurso> SeccionCurso { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeccionPeriodoAcademico> SeccionPeriodoAcademico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeccionSede> SeccionSede { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TemplateEncuesta> TemplateEncuesta { get; set; }
    }
}
