//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class ReporteEVDComentarios_ant_Result
    {
        public string CODIGOALUMNO { get; set; }
        public string NOMBREALUMNO { get; set; }
        public string SECCION { get; set; }
        public string CODIGOCURSO { get; set; }
        public string CURSO { get; set; }
        public string CODIGODOCENTE { get; set; }
        public string NOMBREDOCENTE { get; set; }
        public string ESTADO { get; set; }
    }
}
