//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class Usp_ListarEVD_Result
    {
        public int IdEncuestaVirtualDelegado { get; set; }
        public string CicloAcademico { get; set; }
        public string NombreEspanol { get; set; }
        public string NombreIngles { get; set; }
        public string Codigo { get; set; }
        public Nullable<int> CantidadTotalDelegados { get; set; }
        public string Estado { get; set; }
        public Nullable<int> IdModulo { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public Nullable<int> IdPeriodoAcademico { get; set; }
    }
}
