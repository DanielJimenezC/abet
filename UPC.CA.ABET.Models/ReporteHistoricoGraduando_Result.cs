//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class ReporteHistoricoGraduando_Result
    {
        public Nullable<decimal> Puntaje { get; set; }
        public Nullable<decimal> Minimo { get; set; }
        public Nullable<decimal> Maximo { get; set; }
        public Nullable<int> Cuenta { get; set; }
        public Nullable<int> IdSubModalidadPeriodoAcademico { get; set; }
        public string CicloAcademico { get; set; }
        public string Nombre { get; set; }
    }
}
