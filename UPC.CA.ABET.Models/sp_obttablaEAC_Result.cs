//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class sp_obttablaEAC_Result
    {
        public Nullable<int> idOutcome { get; set; }
        public string NombreOutcome { get; set; }
        public string criterio { get; set; }
        public string notaMaxCriterio { get; set; }
        public Nullable<double> diferencialAppp { get; set; }
        public string Incipiente { get; set; }
        public string proceso { get; set; }
        public string Esperado { get; set; }
    }
}
