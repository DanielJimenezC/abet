//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CursoMallaCurricular
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CursoMallaCurricular()
        {
            this.IndicadoresCursoMatricula = new HashSet<IndicadoresCursoMatricula>();
            this.MallaCocosDetalle = new HashSet<MallaCocosDetalle>();
            this.RequisitoCurso = new HashSet<RequisitoCurso>();
        }
    
        public int IdCursoMallaCurricular { get; set; }
        public int IdCurso { get; set; }
        public int IdNivel { get; set; }
        public int IdMallaCurricular { get; set; }
        public string LogroFinCicloEspanol { get; set; }
        public string LogroFinCicloIngles { get; set; }
        public string TipoEvaluacion { get; set; }
        public Nullable<bool> SilaboEntregadoTiempo { get; set; }
        public Nullable<bool> EsFormacion { get; set; }
        public Nullable<bool> EsElectivo { get; set; }
        public Nullable<int> IdLogCarga { get; set; }
        public string NombreMalla { get; set; }
    
        public virtual Curso Curso { get; set; }
        public virtual LogCarga LogCarga { get; set; }
        public virtual MallaCurricular MallaCurricular { get; set; }
        public virtual NivelAcademico NivelAcademico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IndicadoresCursoMatricula> IndicadoresCursoMatricula { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MallaCocosDetalle> MallaCocosDetalle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RequisitoCurso> RequisitoCurso { get; set; }
    }
}
