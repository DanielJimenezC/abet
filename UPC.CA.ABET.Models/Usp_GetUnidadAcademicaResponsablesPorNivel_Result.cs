//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class Usp_GetUnidadAcademicaResponsablesPorNivel_Result
    {
        public Nullable<int> IdDocente { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreEspanol { get; set; }
        public string NombreIngles { get; set; }
        public Nullable<int> IdSubModalidadPeriodoAcademico { get; set; }
    }
}
