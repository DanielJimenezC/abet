//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class SP_LIST_ESTADO_CALIFICACION_EVALUACION_POR_ALUMNO_Result
    {
        public int IdAlumnoSeccion { get; set; }
        public int IdAlumno { get; set; }
        public string codigo { get; set; }
        public string Nombres { get; set; }
        public int IdCurso { get; set; }
        public string NombreCurso { get; set; }
        public int IdProyectoAcademico { get; set; }
        public string CodigoProyecto { get; set; }
        public int IdEvaluador { get; set; }
        public string CodigoEvaluador { get; set; }
        public string NombreEvaluador { get; set; }
        public string CodigoRol { get; set; }
        public int IdEmpresa { get; set; }
        public string CodigoEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string Evaluacion { get; set; }
        public string ESTADO { get; set; }
    }
}
