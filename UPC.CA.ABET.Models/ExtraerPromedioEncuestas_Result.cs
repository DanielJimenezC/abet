//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class ExtraerPromedioEncuestas_Result
    {
        public Nullable<int> IdPeriodoAcademico { get; set; }
        public string Ciclo { get; set; }
        public string Curso { get; set; }
        public Nullable<double> Puntaje_MO { get; set; }
        public Nullable<double> Puntaje_SI { get; set; }
        public Nullable<double> Puntaje_CS { get; set; }
        public Nullable<double> Puntaje_VI { get; set; }
        public Nullable<double> Puntaje_Promedio { get; set; }
        public Nullable<int> Cantidad_MO { get; set; }
        public Nullable<int> Cantidad_SI { get; set; }
        public Nullable<int> Cantidad_CS { get; set; }
        public Nullable<int> Cantidad_VI { get; set; }
    }
}
