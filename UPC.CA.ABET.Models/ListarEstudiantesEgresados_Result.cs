//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class ListarEstudiantesEgresados_Result
    {
        public int IdAlumno { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public string Codigo { get; set; }
        public string AnioEgreso { get; set; }
        public string AnioTitulacion { get; set; }
        public string CorreoElectronico { get; set; }
        public Nullable<int> CiclosMatriculados { get; set; }
        public Nullable<int> IdLogCarga { get; set; }
        public Nullable<int> IdCarreraEgreso { get; set; }
        public Nullable<int> CantidadCursos { get; set; }
        public Nullable<int> NumeroEvaluaciones { get; set; }
    }
}
