//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class uspGetDocentesCargoNivel_Result
    {
        public int IdDocente { get; set; }
        public int Nivel { get; set; }
        public string NombreDocente { get; set; }
        public int IdUnidadAcademicaResponsable { get; set; }
        public int IdUnidadAcademica { get; set; }
        public string CargoEspanol { get; set; }
        public string CargoIngles { get; set; }
        public int IdSede { get; set; }
    }
}
