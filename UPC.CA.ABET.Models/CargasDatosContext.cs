﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UPC.CA.ABET.Models
{
    public class CargarDatosContext 
    {
        public HttpSessionStateBase session { get; set; }
        public AbetEntities context { get; set; }
        public String currentCulture { get; set; }
    }
}
