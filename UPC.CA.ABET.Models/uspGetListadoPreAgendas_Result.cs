//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class uspGetListadoPreAgendas_Result
    {
        public int IdPreAgenda { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaReunion { get; set; }
        public string Motivo { get; set; }
        public int IdUnidaAcademica { get; set; }
        public Nullable<int> Semana { get; set; }
        public Nullable<bool> FueEjecutada { get; set; }
        public Nullable<int> IdUsuarioCreador { get; set; }
        public string TipoPreagenda { get; set; }
        public Nullable<int> IdSede { get; set; }
        public Nullable<int> Nivel { get; set; }
        public Nullable<int> IdSubModalidadPeriodoAcademico { get; set; }
        public int IdUnidadAcademica { get; set; }
        public Nullable<int> IdUnidadAcademicaPadre { get; set; }
        public string Tipo { get; set; }
        public int Nivel1 { get; set; }
        public string NombreEspanol { get; set; }
        public string NombreIngles { get; set; }
        public Nullable<int> IdCarreraPeriodoAcademico { get; set; }
        public Nullable<int> IdCursoPeriodoAcademico { get; set; }
        public Nullable<bool> EsVisible { get; set; }
        public Nullable<int> IdCargo { get; set; }
        public Nullable<int> IdLogCarga { get; set; }
        public Nullable<int> IdEscuela { get; set; }
        public Nullable<int> IdSubModalidadPeriodoAcademico1 { get; set; }
    }
}
