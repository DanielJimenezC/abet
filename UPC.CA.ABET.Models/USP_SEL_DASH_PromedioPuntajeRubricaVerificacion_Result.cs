//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class USP_SEL_DASH_PromedioPuntajeRubricaVerificacion_Result
    {
        public string Nombre { get; set; }
        public string Pregunta { get; set; }
        public Nullable<decimal> Promedio { get; set; }
    }
}
