//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class FuncionReporteVerificacionNotasSede2_Result
    {
        public string CodigoCurso { get; set; }
        public string NombreCurso { get; set; }
        public Nullable<int> IdCurso { get; set; }
        public string Comision { get; set; }
        public Nullable<int> IdOutcome { get; set; }
        public string Outcome { get; set; }
        public string OutcomeDescripcion { get; set; }
        public Nullable<int> IdSeccion { get; set; }
        public Nullable<int> IdAlumnoSeccion { get; set; }
        public Nullable<int> IdNivelAceptacion { get; set; }
        public Nullable<int> NivelAlcanzado { get; set; }
        public string ColorNivelAceptacion { get; set; }
        public Nullable<int> NivelMaximo { get; set; }
        public Nullable<int> IdPeriodoAcademico { get; set; }
        public string CicloAcademico { get; set; }
        public Nullable<int> IdSubmadalidadPeriodoAcademico { get; set; }
        public Nullable<int> IdModulo { get; set; }
        public string Modulo { get; set; }
        public Nullable<int> IdSede { get; set; }
        public string Sede { get; set; }
    }
}
