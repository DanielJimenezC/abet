//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocenteSeccion
    {
        public int IdDocenteSeccion { get; set; }
        public int IdDocente { get; set; }
        public int IdSeccion { get; set; }
        public Nullable<int> IdLogCarga { get; set; }
    
        public virtual Docente Docente { get; set; }
        public virtual LogCarga LogCarga { get; set; }
        public virtual Seccion Seccion { get; set; }
    }
}
