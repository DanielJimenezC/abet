﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Professor.NeoImprovementActions
{
    public class CoordinadorNotificacionMail
    {
        #region Propiedades

        public Int32 Idcurso { get; set; }
        public String Carrera { get; set; }
        public String CodCurso { get; set; }
        public String Curso { get; set; }
        public String Correo { get; set; }
        public String CodCoor { get; set; }
        public String Coordinador { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        #endregion

        public String getEmail()
        {
            String anio = CodCoor.Substring(0, 4);
            
                return CodCoor + "@upc.edu.pe";
           
        }
    }
}
