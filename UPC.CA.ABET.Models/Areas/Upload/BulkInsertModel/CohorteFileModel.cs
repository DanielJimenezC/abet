﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class CohorteFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.CODIGO_ALUMNO)]
        public string Codigo { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.NOMBRES_Y_APELLIDOS)]
        public string NombresApellidos { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.ESTUDIO)]
        public string Estudio { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.PROMOCION_EGRESO)]
        public string PromocionEgreso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.NUMERO_CICLOS_MATRICULADOS)]
        public string CiclosMatriculados { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.DIRECCION)]
        public string Direccion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.DISTRITO)]
        public string Distrito { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.EMAIL)]
        public string Email { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE.TELEFONOS)]
        public string Telefonos { get; set; }
    }
}
