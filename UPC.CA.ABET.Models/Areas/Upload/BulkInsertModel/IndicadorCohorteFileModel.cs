﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using LinqToExcel.Attributes;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class IndicadorCohorteFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE_INGRESANTES.CICLO)]
        public string ciclo { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE_INGRESANTES.CANTIDAD_INGRESANTES)]
        public string cantidadIngresantes { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE_INGRESANTES.CANTIDAD_GRADUADOS)]
        public string cantidadEgresados { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.COHORTE_INGRESANTES.CODIGO_CARRERA)]
        public string codigoCarrera { get; set; }

    }
}
