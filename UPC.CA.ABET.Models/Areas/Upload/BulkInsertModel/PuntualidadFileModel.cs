﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class PuntualidadFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.PUNTUALIDAD_DOCENTES.DOCENTES)]
        public string nombreDocente { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.PUNTUALIDAD_DOCENTES.MINUTOS_TARDANZA)]
        public string minutosTardanza { get; set; }
    }
}
