﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class OrganigramaFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.ID)]
        public string Id { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.NIVEL)]
        public string Nivel { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.UNIDAD_ACADEMICA)]
        public string UnidadAcademica { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.NOMBRE_INGLES)]
        public string NombreIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.NOMBRE_USUARIO)]
        public string NombreUsuario { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.PADRE)]
        public string Padre { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.SEDE)]
        public string Sede { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.CARRERA)]
        public string Carrera { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.CURSO)]
        public string Curso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.TIPO)]
        public string Tipo { get; set; }

        [ExcelColumn(ConstantHelpers.UPLOAD.ORGANIGRAMA.ESCUELA)]
        public string Escuela { get; set; }
    }
}
