﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class CurriculaFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.CODIGO_CURRICULA)]
        public string CodigoCurricula { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.CARRERA)]
        public string Carrera { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.NIVEL)]
        public string Nivel { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.CODIGO_DE_CURSO)]
        public string CodigoDeCurso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.NOMBRE_DE_CURSO)]
        public string NombreDeCurso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.NOMBRE_EN_INGLES)]
        public string NombreEnIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.REQUISITOS)]
        public string Requisitos { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.LOGRO)]
        public string Logro { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.LOGRO_INGLES)]
        public string LogroIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.CAMPO_EVALUACION)]
        public string CampoEvaluacion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.ES_ELECTIVO)]
        public string EsElectivo { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.CURRICULA.NOMBRE_MALLA)]
        public string NombreMalla { get; set; }
    }
}
