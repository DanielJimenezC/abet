﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class SeccionFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.SECCION.CODIGO_CURSO)]
        public string codigocurso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.SECCION.SECCIONES)]
        public string seccion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.SECCION.DOCENTE)]
        public string docente { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.SECCION.LOCAL)]
        public string local { get; set; }
    }
}
