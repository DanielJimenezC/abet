﻿using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class OutcomesFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.CODIGO_OUTCOME)]
        public string CodigoOutcome { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.NOMBRE_OUTCOME)]
        public string NombreOutcome { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.NOMBRE_OUTCOME_INGLES)]
        public string NombreOutcomeIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.DESCRIPCION)]
        public string Descripcion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.DESCRIPCION_INGLES)]
        public string DescripcionIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.OUTCOMES.COMISION)]
        public string Comision { get; set; }
    }
}
