﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class DelegadosFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.DELEGADOS.CODIGO_CURSO)]
        public string CodigoCurso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.DELEGADOS.CODIGO_SECCION)]
        public string CodigoSeccion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.DELEGADOS.CODIGO_ALUMNO)]
        public string CodigoAlumno { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.DELEGADOS.NOMBRE_ALUMNO)]
        public string NombreAlumno { get; set; }
    }
}
