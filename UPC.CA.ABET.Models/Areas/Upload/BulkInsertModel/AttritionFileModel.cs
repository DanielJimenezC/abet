﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class AttritionFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.BAJA)]
        public string Baja { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.CAMBIO_DE_MODALIDAD)]
        public string CambioDeModalidad { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.DESERCION)]
        public string Desercion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.RESERVA)]
        public string Reserva { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.SOLO_INGLES)]
        public string SoloIngles { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.CICLO)]
        public string Ciclo { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.CARRERA)]
        public string Carrera { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.TOTAL_ATTRITION)]
        public string TotalAttrition { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ATTRITION.TOTAL_MATRICULADOS)]
        public string TotalMatriculados { get; set; }
    }
}
