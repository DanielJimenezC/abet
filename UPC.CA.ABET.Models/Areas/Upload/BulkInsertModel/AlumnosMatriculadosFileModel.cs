﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class AlumnosMatriculadosFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.ALUMNOS_MATRICULADOS.CODIGO_ALUMNO)]
        public string CodigoDeAlumno { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ALUMNOS_MATRICULADOS.NOMBRE_COMPLETO)]
        public string NombreCompleto { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ALUMNOS_MATRICULADOS.CARRERA)]
        public string Carrera { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ALUMNOS_MATRICULADOS.ESTADO_MATRICULA)]
        public string EstadoMatricula { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ALUMNOS_MATRICULADOS.SEDE)]
        public string Sede { get; set; }
    }
}
