﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using LinqToExcel.Attributes;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class EvaluacionesFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_CURSO)]
        public string CodigoCurso { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_SECCION)]
        public string CodigoSeccion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_ALUMNO)]
        public string CodigoAlumno { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.EVALUACIONES.NOTA)]
        public string NotaAlumno { get; set; }
    }
}
