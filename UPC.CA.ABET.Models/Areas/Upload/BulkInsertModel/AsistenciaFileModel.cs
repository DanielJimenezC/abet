﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class AsistenciaFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.NOMBRE_USUARIO)]
        public string NombreUsuario { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.TOTAL_CLASES)]
        public string TotalClases { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.TOTAL)]
        public string Total { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.LISTA)]
        public string Lista { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.CLASES_INASISTIDAS)]
        public string ClasesInasistidas { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.RECUPERACIONES_ASISTIDAS)]
        public string RecuperacionesAsistidas { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.RECUPERACIONES_INASISTIDAS)]
        public string RecuperacionesInasistidas { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.ADICIONALES_ASISTIDAS)]
        public string AdicionalesAsistidas { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.ADICIONALES_INASISTIDAS)]
        public string AdicionalesInasistidas { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.ADELANTOS_ASISTIDOS)]
        public string AdelantosAsistidos { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.ASISTENCIA_DOCENTES.ADELANTOS_INASISTIDOS)]
        public string AdelantosInasistidos { get; set; }

    }
}
