﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel
{
    public class DocentesFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.DOCENTES.NOMBRE_DE_USUARIO)]
        public string NombreDeUsuario { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.DOCENTES.NOMBRE_COMPLETO_DEL_DOCENTE)]
        public string NombreCompletoDelDocente { get; set; }
    }
}
