﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel
{
    public class TituladosFileModel
    {
        [ExcelColumn(ConstantHelpers.UPLOAD.TITULADOS.CODIGO)]
        public string codigo { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.TITULADOS.ESTUDIO)]
        public string estudio { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.TITULADOS.FECHA_RESOLUCION)]
        public string fechaResolucion { get; set; }
        [ExcelColumn(ConstantHelpers.UPLOAD.TITULADOS.NOMBRES_Y_APELLIDOS)]
        public string nombresApellidos { get; set; }
    }
}
