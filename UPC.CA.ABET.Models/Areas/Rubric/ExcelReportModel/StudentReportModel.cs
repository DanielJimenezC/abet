﻿
#region Imports

using System;

#endregion
namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class StudentReportModel
    {
        
        #region Propiedades

        public Int32 CursoId { get; set; }
        public String CodigoCurso { get; set; }
        public String NombreCurso { get; set; }
        public String CodigoAlumno { get; set; }
        public String NombreAlumno { get; set; }
        public Int32 AlumnoSeccionId { get; set; }
        public String CodigoCarrera { get; set; }
        public String NombreCarrera { get; set; }
        public String SedeAlumno { get; set; }
        public String SeccionCurso { get; set; }
        public int idRubricaControlEvaluacion { get; set; }
        public int idRubricaControl { get; set; }
        public int idDocenteCalificador { get; set; }
        public int idSeccion { get; set; }
        public int idAlumno { get; set; }
        public int idRubricaControlNivelAceptacion { get; set; }
        public decimal peso { get; set; }
        public string descriptionEspanol { get; set; }
        public string descriptionEspa { get; set; }
        public int? idCarreraEgreso { get; set; }
        public string ApellidoAlum { get; set; }
        public string NombreDocente { get; set; }
        public string ApellidoDocente { get; set; }
        public string codigoDocente { get; set; }
        public int SedeSeccion { get; set; }
        public string NombreCursoE { get; set; }
        public string NombreCursoI { get; set; }
        public int idRubricaControlPregunta { get; set; }
        public string preguntaEspanol { get; set; }
        public string preguntaIngles { get; set; }
        public decimal puntajeMaximo { get; set; }
        public int idRubricaControlCriterio { get; set; }
        public int idRubricaControlEvaluacionResultado { get; set; }
        public decimal idPuntaje { get; set; }
        public object nombre_NivelAcep { get; set; }
        public int idRubricaControlCriterioCapstone { get; set; }
        public string nombreCriterioEspa { get; set; }
        public decimal puntajeMinimo { get; set; }
        public int idRubricaControlEvaluacionResultadoCapstone { get; set; }
        public object idRubricaControlEvaluacionCapstone { get; set; }
        public decimal idPuntajeCapstone { get; set; }
        public object NombreProyecto { get; set; }
        public object obs { get; set; }
        #endregion
    }
}
