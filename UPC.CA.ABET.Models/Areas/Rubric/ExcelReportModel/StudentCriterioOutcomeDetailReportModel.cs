﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class StudentCriterioOutcomeDetailReportModel
    {
        #region Propiedades

        public Int32 OutcomeCalificadoId { get; set; }
        public String Criterio { get; set; }
        public String DescripcionCriterio { get; set; }
        public Decimal NotaCriterio { get; set; }
        public Decimal NotaAlumno { get; set; }
        public Int32 CriterioId { get; set; }
        public Boolean NivelesPorDefecto { get; set; }
        #endregion
    }
}
