﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class OutcomeReportModel
    {
        #region Propiedades

        public String Outcome { get; set; }
        public Decimal NotaAlumno { get; set; }
        public Decimal NotaMaximaOutcome { get; set; }
        public Int32 NivelAlcanzado { get; set; }
        public String Descripcion { get; set; }
        public String Comision { get; set; }

        #endregion
    }
}
