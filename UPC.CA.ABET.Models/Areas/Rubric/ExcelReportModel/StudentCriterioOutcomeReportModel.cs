﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class StudentCriterioOutcomeReportModel
    {
        #region Propiedades

        public Int32 OutcomeCalificadoId { get; set; }
        public String Outcome { get; set; }
        public String DescripcionOutcome { get; set; }
        public String Comision { get; set; }
        public String ObservacionRubrica { get; set; }
        public Decimal NotaOutcome { get; set; }
        public Decimal NotaRubricaCalificada { get; set; }
        #endregion
    }
}
