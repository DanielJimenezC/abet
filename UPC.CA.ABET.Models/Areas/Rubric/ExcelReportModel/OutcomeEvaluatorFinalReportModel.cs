﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class OutcomeEvaluatorFinalReportModel
    {
        #region Propiedades

        public String Outcome { get; set; }
        public String DescripcionOutcome { get; set; }
        public Decimal? NotaOutcome { get; set; }
        public String ComentarioOutcome { get; set; }
        public String Comision { get; set; }
        public String RolEvaluador { get; set; }
        public String CodigoEvaluador { get; set; }
        public String NombreEvaluador { get; set; }
        public String Observaciones { get; set; }
        #endregion
    }
}
