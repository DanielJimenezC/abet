﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class StudentGradeReportModel
    {
        #region Propiedades

        public String Alumno { get; set; }
        public String Codigo { get; set; }
        public String Carrera { get; set; }
        public String SedeAlumno { get; set; }
        public String Curso { get; set; }
        public String SeccionCurso { get; set; }
        public String NotaPa { get; set; }
        public String NotaTp { get; set; }
        public String NotaTf { get; set; }
        public Decimal PorcentajeAvance { get; set; }
        public Decimal NotaFinalCurso { get; set; }

        #endregion
    }
}
