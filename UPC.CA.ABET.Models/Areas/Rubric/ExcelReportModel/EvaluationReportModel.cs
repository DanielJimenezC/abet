﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class EvaluationReportModel
    {
        #region Propiedades

        public Int32 EvaluacionId { get; set; }
        public Int32 TipoEvaluacionId { get; set; }
        public String TipoEvaluacion { get; set; }
        public Decimal PesoEvaluacion { get; set; }
        public String CodigoCarrera { get; set; }
        public String CodigoCurso { get; set; }
        public String NombreCurso { get; set; }

        #endregion
    }
}
