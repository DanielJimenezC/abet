﻿
#region Imports

using System;

#endregion


namespace UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel
{
    public class StudentOutcomeReportModel
    {
        #region Propiedades

        public String Apellidos { get; set; }
        public Int32 AlumnoId { get; set; }
        public Int32 AlumnoSeccionId { get; set; }
        public Int32 CarreraId { get; set; }
        public Int32 CursoId { get; set; }
        public Int32 CarreraCursoPeriodoAcademicoId { get; set; }
        public String NombreAlumno { get; set; }
        public String CodigoAlumno { get; set; }
        public String Carrera { get; set; }
        public String Sede { get; set; }
        public String Curso { get; set; }
        public String Seccion { get; set; }
        public String CodigoProyecto { get; set; }
        public String CodigoCursoReporte { get; set; }
        public String Observaciones { get; set; }
        #endregion
    }
}
