﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.AdministrationModel
{
    public class AlumnoAdminModel
    {
        #region Propiedades

        public Int32 AlumnoId { get; set; }
        public String Codigo { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public Boolean estaRetirado { get; set; }
        public String CursoMatriculado { get; set; }
        public Int32 AlumnoSeccionId { get; set; }
        #endregion
    }
}
