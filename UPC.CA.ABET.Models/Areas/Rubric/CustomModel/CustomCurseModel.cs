﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Rubric.CustomModel
{
    public class CustomCurseModel
    {
        public int? IdCurso { get; set;}
        public String Codigo { get; set; }
        public String Abreviatura { get; set; }
        public String NombreCompleto { get; set; }
    }
}
