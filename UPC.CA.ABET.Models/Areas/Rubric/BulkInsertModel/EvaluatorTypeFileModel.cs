﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct EvaluatorTypeFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorTypeFile.CODIGO)]
        public String Codigo { get; set; }
        
        //ESROLCOMITE
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorTypeFile.PESO)]
        public String Peso { get; set; }
        //NOMBRECURSO
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorTypeFile.CURSO)]
        public string Curso { get; set; }

        #endregion
    }
    public struct EvaluatorTypeMasterFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorTypeFile.CODIGO)]
        public String Codigo { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorTypeFile.NOMBRE)]
        public String Nombre { get; set; }

        #endregion
    }

    public struct GrupoComiteFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.GrupoComiteFile.CODIGO_DOCENTE)]
        public String CodigoDocente { get; set; }

        #endregion
    }
    public struct CourseToEvaluationFileModel
    {
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.CourseToEvaluationFile.CODIGO)]
        public String Codigo { get; set; }
    }

}
