﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public class EvaluatorValidatorModel
    {
        #region Propiedades

        public Int32 DocenteId { get; set; }
        public Int32 ProyectoId { get; set; }
        public Int32 TipoEvaluadorId { get; set; }

        #endregion
    }
}
