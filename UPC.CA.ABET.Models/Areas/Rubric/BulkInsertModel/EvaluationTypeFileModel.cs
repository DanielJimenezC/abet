﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct EvaluationTypeFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluationTypeFile.TIPO_EVALUACION)]
        public String TipoEvaluacion { get; set; }

        #endregion
    }
}
