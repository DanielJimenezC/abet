﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct EvaluatorFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorFile.TIPO_EVALUADOR)]
        public String TipoEvaluador { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorFile.CODIGO_DOCENTE)]
        public String CodigoDocente { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluatorFile.CODIGO_PROYECTO)]
        public String CodigoProyecto { get; set; }

        #endregion
    }
}
