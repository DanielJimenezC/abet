﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public class OucomeGoalCommissionFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.COMPETENCIA_GENERAL)]
        public String CompetenciaGeneral { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_ESPERADO)]
        public String LogroEsperado { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_SOBRESALIENTE)]
        public String LogroSobresaliente { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_EJEMPLAR)]
        public String LogroEjemplar { get; set; }

        #endregion
    }
}
