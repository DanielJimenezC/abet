﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public class EvaluatorModel
    {
        #region Propiedades

        public Int32 TipoEvaluadorId { get; set; }
        public Int32 DocenteId { get; set; }
        public String CodigoDocente { get; set; }
        public String NombreDocente { get; set; }
        public String RolEvaluador { get; set; }
        public String CodigoTipoEvaluador { get; set; }
        public String PesoEvaluador { get; set; }

        #endregion
    }
}
