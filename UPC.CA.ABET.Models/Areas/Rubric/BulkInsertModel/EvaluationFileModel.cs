﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct EvaluationFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluationFile.CURSO)]
        public String Curso { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluationFile.CARRERA)]
        public String Carrera { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluationFile.TIPO_EVALUACION)]
        public String TipoEvaluacion { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.EvaluationFile.NOTA)]
        public String Nota { get; set; }

        #endregion
    }
}
