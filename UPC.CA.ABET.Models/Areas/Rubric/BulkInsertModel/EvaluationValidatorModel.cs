﻿
#region Imports

using System;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public class EvaluationValidatorModel
    {
        #region Propiedades

        public Int32 CursoId { get; set; }
        public String CodigoCurso { get; set; }
        public Int32 CarreraCursoPeriodoAcademicoId { get; set; }
        public Int32 TipoEvaluacionId { get; set; }
        public Decimal Peso { get; set; }

        #endregion
    }
}
