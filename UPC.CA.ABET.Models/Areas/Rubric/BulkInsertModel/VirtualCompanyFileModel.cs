﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct VirtualCompanyFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.VirtualCompanyFile.CODIGO)]
        public String Codigo { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.VirtualCompanyFile.NOMBRE)]
        public String Nombre { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.VirtualCompanyFile.DESCRIPCION)]
        public String Descripcion { get; set; }

        #endregion
    }
}
