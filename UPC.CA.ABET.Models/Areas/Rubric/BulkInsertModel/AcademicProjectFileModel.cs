﻿
#region Imports

using System;
using LinqToExcel.Attributes;
using UPC.CA.ABET.Helpers;

#endregion

namespace UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel
{
    public struct AcademicProjectFileModel
    {
        #region Propiedades

        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CODIGO)]
        public String Codigo { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CURSO)]
        public String Curso { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.NOMBRE)]
        public String Nombre { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.DESCRIPCION)]
        public String Descripcion { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CODIGO_EMPRESA)]
        public String CodigoEmpresa { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.ALUMNO_01)]
        public String CodigoAlumno01 { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.ALUMNO_02)]
        public String CodigoAlumno02 { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CODIGO_COAUTOR)]
        public String CodDocenteCoautor { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CODIGO_CLIENTE)]
        public String CodDocenteCliente { get; set; }
        [ExcelColumn(ConstantHelpers.RUBRICFILETYPE.AcademicProjectFile.CODIGO_GERENTE)]
        public String CodGerente { get; set; }

        public String RazonError { get; set; }

        #endregion
    }
}
