﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Models.Areas.Survey
{
    public class StudenNotificationGRA
    {
        public Int32 IdAlumno { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdNotificacion { get; set; }
        public String Correo { get; set; }
        public String Codigo { get; set; }
        public String NombreCompleto { get; set; }
        public String NombreCarreraEspanol { get; set; }
        public String NombreCarreraIngles { get; set; }
        public String FechaEnvio { get; set; }
        public Boolean Estado { get; set; }

        public String getEmail()
        {
            String anio = Codigo.Substring(0, 4);
            if (anio.ToInteger() >= 2010)
                return "u" + Codigo + "@upc.edu.pe";
            else
                return Codigo.Replace("200", "u") + "@upc.edu.pe";
        }
    }
}
