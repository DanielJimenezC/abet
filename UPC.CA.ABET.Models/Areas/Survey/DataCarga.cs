﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
    public class DataCarga
    {
        public List<String> lstError { get; set; }
        public List<String> lstRepetidos { get; set; }
        public Int32 CantSuccess { get; set; }
        public Boolean Error { get; set; }
        public DataCarga()
        {
            Error = false;
        }
    }
}
