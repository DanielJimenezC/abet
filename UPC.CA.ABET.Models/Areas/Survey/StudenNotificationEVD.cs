﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
	public class StudenNotificationEVD
	{
		public Int32 IdAlumno { get; set; }
		public Int32 IdCarrera { get; set; }
		public Int32 IdNotificacion { get; set; }
		public String Correo { get; set; }
		public String Codigo { get; set; }
		public String NombreCompleto { get; set; }
		public String NombreCarreraEspanol { get; set; }
		public String NombreCarreraIngles { get; set; }
		public String FechaEnvio { get; set; }
		public Boolean Estado { get; set; }
		public String getEmail { get; set; }

	}
}

