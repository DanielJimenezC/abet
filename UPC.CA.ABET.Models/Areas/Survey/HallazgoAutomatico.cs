﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
	public class HallazgoAutomatico
	{
		public string CodigoCurso { get; set; }
		public int CantidadAlumnos { get; set; }
		public int CodigoDocente { get; set; }
		public string Comentario { get; set; }
		public int IdCurso { get; set; }
		public int IdAlumno { get; set; }
		public int IdComentarioDelegado { get; set; }
		public string NombreNivelSatisfaccion { get; set; }
		public string DescripcionPregunta { get; set; }
		public bool borrar { get; set; }
		public int IdSubModalidadPeriodoAcademico { get; set; }
	}
}
