﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
    public class EVDDelegate
    {
        public String Codigo_Alumno { get; set; }
        public String Nombre_Alumno { get; set; }
        public String Codigo_Seccion { get; set; }
        public String Codigo_Curso { get; set; }
        public String Nombre_Curso { get; set; }
        public String Codigo_Docente { get; set; }
        public String Nombre_Docente { get; set; }
        public Int32 Modulo_Id { get; set; }
        public String Modulo { get; set; }
    }


}
