﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
	public class EVDHallazgo
	{
		public int? IdOutcome { get; set; }
		public int? IdCurso { get; set;}
		public int? IdSubModalidadperiodoAcademico { get; set; }
		public int? IdOutcomeComision { get; set; }
		public int? IdComision { get; set; }
		public int? IdEncuestaVirtualDelegado { get; set; }
		public int? IdComentarioDelegado { get; set; }
		public int? IdDocente { get; set; }
		public int? IdAlumno { get; set; }
		public int? IdModulo { get; set; }
	}
}
