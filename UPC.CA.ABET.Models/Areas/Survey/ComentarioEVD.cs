﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
	public class ComentarioEVD
	{
		public int IdComentarioDelegado { get; set; }
		public String Docente { get; set; }
		public String Curso { get; set; }
		public String Comentario { get; set; }
	}
}
