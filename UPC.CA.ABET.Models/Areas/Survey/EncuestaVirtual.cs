﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Survey
{
    public class EncuestaVirtual
    {
        /*ID ENCUESTA*/
        public int? Id { get; set; }

        public int? IdEncuestaToken { get; set; }
        public string Nombre { get; set; }

        public string Codigo { get; set; }
        public string Carrera { get; set; }
        public string Ciclo { get; set; }
        public DateTime FechaFin { get; set; }
        public string Comentario { get; set; }
        public bool Status { get; set; }
        public int? Status2 { get; set; }
        public int? IdAlumno { get; set; }
        public int? IdCarrera { get; set; }
        public int? IdPeriodoAcademico { get; set; }
        public int? IdSubModalidadPeriodoAcademico { get; set; }
    }
}
