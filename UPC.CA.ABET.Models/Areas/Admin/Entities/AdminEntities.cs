﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Models.Areas.Admin.Entities
{
    public class DocenteEntity
    {
        public String Codigo { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
    }
}
