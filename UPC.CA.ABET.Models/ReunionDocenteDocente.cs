//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReunionDocenteDocente
    {
        public int IdReunionDocenteDocente { get; set; }
        public int IdReunionDocente { get; set; }
        public int IdDocente { get; set; }
    
        public virtual Docente Docente { get; set; }
        public virtual ReunionDocente ReunionDocente { get; set; }
    }
}
