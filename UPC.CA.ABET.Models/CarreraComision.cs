//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CarreraComision
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CarreraComision()
        {
            this.MallaCocos = new HashSet<MallaCocos>();
        }
    
        public int IdCarreraComision { get; set; }
        public int IdComision { get; set; }
        public int IdCarrera { get; set; }
        public Nullable<int> IdSubModalidadPeriodoAcademico { get; set; }
    
        public virtual Carrera Carrera { get; set; }
        public virtual Comision Comision { get; set; }
        public virtual SubModalidadPeriodoAcademico SubModalidadPeriodoAcademico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MallaCocos> MallaCocos { get; set; }
    }
}
