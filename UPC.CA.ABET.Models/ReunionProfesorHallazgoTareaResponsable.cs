//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReunionProfesorHallazgoTareaResponsable
    {
        public int IdReunionProfesorHallazgoTareaResponsable { get; set; }
        public int IdReunionProfesorHallazgoTarea { get; set; }
        public int IdDocenteResponsable { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public System.DateTime FechaActualizacion { get; set; }
    
        public virtual Docente Docente { get; set; }
        public virtual ReunionProfesorHallazgoTarea ReunionProfesorHallazgoTarea { get; set; }
    }
}
