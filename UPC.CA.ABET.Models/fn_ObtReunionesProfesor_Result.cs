//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Models
{
    using System;
    
    public partial class fn_ObtReunionesProfesor_Result
    {
        public string Codigo { get; set; }
        public string Encargado { get; set; }
        public Nullable<int> Nivel { get; set; }
        public string Area { get; set; }
        public string AreaEncargadaIngles { get; set; }
        public Nullable<int> IdUnidadAcademica { get; set; }
        public Nullable<int> IdDocente { get; set; }
        public string Semana { get; set; }
        public Nullable<int> NumSemana { get; set; }
        public string Lugar { get; set; }
        public Nullable<System.DateTime> Inicio { get; set; }
        public Nullable<System.DateTime> Fin { get; set; }
        public Nullable<System.DateTime> PlazoHasta { get; set; }
        public string NombreReunion { get; set; }
        public string NombreReunionIngles { get; set; }
    }
}
