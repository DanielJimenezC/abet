﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ComisionResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ComisionResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation.ComisionResourc" +
                            "e", typeof(ComisionResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Code.
        /// </summary>
        public static string Codigo {
            get {
                return ResourceManager.GetString("Codigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Academic Period.
        /// </summary>
        public static string IdAcreditadoraPeriodoAcademico {
            get {
                return ResourceManager.GetString("IdAcreditadoraPeriodoAcademico", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name.
        /// </summary>
        public static string Nombre {
            get {
                return ResourceManager.GetString("Nombre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name Spanish.
        /// </summary>
        public static string NombreEspanol {
            get {
                return ResourceManager.GetString("NombreEspanol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name English.
        /// </summary>
        public static string NombreIngles {
            get {
                return ResourceManager.GetString("NombreIngles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Comissions.
        /// </summary>
        public static string TituloLista {
            get {
                return ResourceManager.GetString("TituloLista", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Comission.
        /// </summary>
        public static string TituloSingle {
            get {
                return ResourceManager.GetString("TituloSingle", resourceCulture);
            }
        }
    }
}
