using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.AcademicPeriod;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class ListAlumnoMatriculadoViewModel
    {
        public Int32 p { get; set; }
        [Display(Name = "CadenaBuscar", ResourceType = typeof(LayoutResource))]
        public String CadenaBuscar { get; set; }

        [Display(Name = "PeriodoAcademico", ResourceType = typeof(AcademicPeriodResource))]
        public Int32? PeriodoAcademicoId { get; set; }
        public IPagedList<AlumnoMatriculado> LstAlumnoMatriculado { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public ListAlumnoMatriculadoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, String cadenaBuscar, Int32? SubModalidadPeriodoAcademico, Int32 pModalidadId,Int32 pEscuelaId)
        {
            this.p = p ?? 1;
            this.CadenaBuscar = !String.IsNullOrEmpty(cadenaBuscar) ? cadenaBuscar.ToUpper() : String.Empty;
            IQueryable<AlumnoMatriculado> query = dataContext.context.AlumnoMatriculado.Include(x => x.Alumno).Include(x => x.SubModalidadPeriodoAcademico).AsQueryable();

            //validar
            if (SubModalidadPeriodoAcademico.HasValue)            
                query = query.Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico && x.Carrera.IdEscuela==pEscuelaId);            

            if (!String.IsNullOrEmpty(CadenaBuscar))
                foreach (var token in CadenaBuscar.Split(' '))
                    query = query.Where(x => x.Alumno.Nombres.Contains(token) || x.Alumno.Apellidos.Contains(token) || x.Alumno.Codigo.Contains(token));

            LstAlumnoMatriculado = query.OrderBy(x => x.IdAlumnoMatriculado).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);

            //var periodoAcademico = dataContext.context.PeriodoAcademico.Find();
            //LstPeriodoAcademico = dataContext.context.PeriodoAcademico.Where(x => x.CicloAcademico.ToInteger() < periodoAcademico.CicloAcademico.ToInteger()).ToList();
            //LstPeriodoAcademico = dataContext.context.PeriodoAcademico.OrderBy(x => x.CicloAcademico).ToList();

            LstPeriodoAcademico = (from a in dataContext.context.PeriodoAcademico
                                   join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                   join c in dataContext.context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                                   where c.IdModalidad == pModalidadId
                                   select a).OrderBy(x => x.CicloAcademico).ToList();
        }
    }
}
