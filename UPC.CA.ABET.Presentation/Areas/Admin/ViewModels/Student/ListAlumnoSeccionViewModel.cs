using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using PagedList;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

//************** BEGIN CONTROLLER *******************//
/*
	public ActionResult ListAlumnoSeccion(Int32? p, String CadenaBuscar)
	{
		var viewModel = new ListAlumnoSeccionViewModel();
		viewModel.CargarDatos(CargarDatosContext(),p, CadenaBuscar);
		return View(viewModel);
	}
*/
//************** END CONTROLLER *******************//
namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class ListAlumnoSeccionViewModel
    {
		public Int32 p { get; set; }        
        [Display(Name = "CadenaBuscar", ResourceType = typeof(LayoutResource))]
        public String CadenaBuscar { get; set; }
        public IPagedList<AlumnoSeccion> LstAlumnoSeccion { get; set; }

        public ListAlumnoSeccionViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, String cadenaBuscar)
        {
			this.p = p ?? 1;
            this.CadenaBuscar = cadenaBuscar;
			IQueryable<AlumnoSeccion> query = dataContext.context.AlumnoSeccion.AsQueryable();
            //if (!String.IsNullOrEmpty(CadenaBuscar))
            //    foreach (var token in CadenaBuscar.Split(' '))
            //        query = query.Where(x => x.Nombre.Contains(token) || x.Apellidos.Contains(token));
            query = query.OrderBy(x => x.IdAlumnoSeccion);
            LstAlumnoSeccion = query.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
