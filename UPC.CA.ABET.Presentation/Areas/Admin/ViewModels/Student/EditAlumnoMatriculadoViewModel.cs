using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

//************** BEGIN CONTROLLER *******************//
/*
	public ActionResult EditAlumnoMatriculado(Int32? IdAlumnoMatriculado)
	{
		var EditAlumnoMatriculadoViewModel = new EditAlumnoMatriculadoViewModel();
		EditAlumnoMatriculadoViewModel.CargarDatos(CargarDatosContext(), IdAlumnoMatriculado);
		return View(EditAlumnoMatriculadoViewModel);
	}

	[HttpPost]
	public ActionResult EditAlumnoMatriculado(EditAlumnoMatriculadoViewModel model)
	{
		try
		{
			if (!ModelState.IsValid)
			{
				model.CargarDatos(CargarDatosContext(), model.IdAlumnoMatriculado);
				TryUpdateModel(model);
				PostMessage(MessageType.Error);
				return View(model);
			}
			using (var transaction = new TransactionScope())
			{

				var AlumnoMatriculado = new AlumnoMatriculado();

				if (model.IdAlumnoMatriculado.HasValue)
				{
					AlumnoMatriculado = context.AlumnoMatriculado.First(x => x.IdAlumnoMatriculado == model.IdAlumnoMatriculado);
				}
				else
				{
					//Establecer las variables por defecto
					context.AlumnoMatriculado.Add(AlumnoMatriculado);
				}

				AlumnoMatriculado.IdAlumno = model.IdAlumno;
				AlumnoMatriculado.IdPeriodoAcademico = model.IdPeriodoAcademico;
				AlumnoMatriculado.IdSede = model.IdSede;
				AlumnoMatriculado.IdCarrera = model.IdCarrera;
				AlumnoMatriculado.EstadoMatricula = model.EstadoMatricula;

				context.SaveChanges();

				transaction.Complete();

				PostMessage(MessageType.Success);
				return RedirectToAction("ListAlumnoMatriculado");
			}
		}
		catch (Exception ex)
		{
			InvalidarContext();
			PostMessage(MessageType.Error);
			model.CargarDatos(CargarDatosContext(), model.IdAlumnoMatriculado);
			TryUpdateModel(model);
			return View(model);
		}
	}
*/
//************** END CONTROLLER *******************//
namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class EditAlumnoMatriculadoViewModel
    {
		public Int32? IdAlumnoMatriculado { get; set; }

		[Display(Name = "IdAlumno")]
		[Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
		public Int32 IdAlumno { get; set; }
		[Display(Name = "IdPeriodoAcademico")]
		[Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
		public Int32 IdPeriodoAcademico { get; set; }
        [Display(Name = "IdSubModalidadPeriodoAcademico")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        [Display(Name = "IdSede")]
		public Int32? IdSede { get; set; }
		[Display(Name = "IdCarrera")]
		[Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
		public Int32 IdCarrera { get; set; }
		[Display(Name = "EstadoMatricula")]
		[Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
		public String EstadoMatricula { get; set; }

		public List<Alumno> LstAlumno { get; set; }
		public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<SubModalidadPeriodoAcademico > LstSubModalidadPeriodoAcademico { get; set; }
        public List<Sede> LstSede { get; set; }
		public List<Carrera> LstCarrera { get; set; }

        public EditAlumnoMatriculadoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdAlumnoMatriculado)
        {
		this.IdAlumnoMatriculado = IdAlumnoMatriculado;

            if (IdAlumnoMatriculado.HasValue)
            {
                var AlumnoMatriculado = dataContext.context.AlumnoMatriculado.First(x => x.IdAlumnoMatriculado == IdAlumnoMatriculado);
				this.IdAlumnoMatriculado = AlumnoMatriculado.IdAlumnoMatriculado;
				this.IdAlumno = AlumnoMatriculado.IdAlumno;
				this.IdSubModalidadPeriodoAcademico = AlumnoMatriculado.IdSubModalidadPeriodoAcademico.Value;
				this.IdSede = AlumnoMatriculado.IdSede;
				this.IdCarrera = AlumnoMatriculado.IdCarrera.Value;
				this.EstadoMatricula = AlumnoMatriculado.EstadoMatricula;
            }

			LstAlumno = dataContext.context.Alumno.ToList();
			LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            LstSede = dataContext.context.Sede.ToList();
			LstCarrera = dataContext.context.Carrera.ToList();
        }
    }
}
