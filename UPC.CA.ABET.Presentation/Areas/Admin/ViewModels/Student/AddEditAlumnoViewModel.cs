using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class AddEditAlumnoViewModel
    {
        public Int32? IdAlumno { get; set; }

        [Display(Name = "Codigo")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Codigo { get; set; }
        [Display(Name = "Nombres")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Nombres { get; set; }
        [Display(Name = "CorreoElectronico")]
        public String CorreoElectronico { get; set; }
        [Display(Name = "AnioEgreso")]
        public String AnioEgreso { get; set; }
        [Display(Name = "AnioTitulacion")]
        public String AnioTitulacion { get; set; }
        [Display(Name = "CiclosMatriculados")]
        public Int32? CiclosMatriculados { get; set; }
        [Display(Name = "Apellidos")]
        public String Apellidos { get; set; }


        public AddEditAlumnoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdAlumno)
        {
            this.IdAlumno = IdAlumno;

            if (IdAlumno.HasValue)
            {
                var Alumno = dataContext.context.Alumno.First(x => x.IdAlumno == IdAlumno);
                this.IdAlumno = Alumno.IdAlumno;
                this.Codigo = Alumno.Codigo;
                this.Nombres = Alumno.Nombres;
                this.CorreoElectronico = Alumno.CorreoElectronico;
                this.AnioEgreso = Alumno.AnioEgreso;
                this.AnioTitulacion = Alumno.AnioTitulacion;
                this.CiclosMatriculados = Alumno.CiclosMatriculados;
                this.Apellidos = Alumno.Apellidos;
            }

        }
    }
}
