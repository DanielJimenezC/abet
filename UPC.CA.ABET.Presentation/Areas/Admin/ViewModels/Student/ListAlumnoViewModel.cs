using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using PagedList;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class ListAlumnoViewModel
    {
        public Int32 p { get; set; }
        [Display(Name = "CadenaBuscar", ResourceType = typeof(LayoutResource))]
        public String CadenaBuscar { get; set; }
        public IPagedList<Alumno> LstAlumno { get; set; }

        public ListAlumnoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, String cadenaBuscar)
        {
            this.p = p ?? 1;
            this.CadenaBuscar = cadenaBuscar;
            IQueryable<Alumno> query = dataContext.context.Alumno.AsQueryable();
            if (!String.IsNullOrEmpty(CadenaBuscar))
                foreach (var token in CadenaBuscar.Split(' '))
                    query = query.Where(x => x.Nombres.Contains(token) || x.Apellidos.Contains(token) || x.Codigo.Contains(token));

            LstAlumno = query.OrderBy(x => x.Apellidos).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
