using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Models;

//************** BEGIN CONTROLLER *******************//
/*
        public ActionResult AddEditAlumnoSeccion(Int32? IdAlumnoSeccion)
        {
            var AddEditAlumnoSeccionViewModel = new AddEditAlumnoSeccionViewModel();
            AddEditAlumnoSeccionViewModel.CargarDatos(CargarDatosContext(), IdAlumnoSeccion);
            return View(AddEditAlumnoSeccionViewModel);
        }

        [HttpPost]
        public ActionResult AddEditAlumnoSeccion(AddEditAlumnoSeccionViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdAlumnoSeccion);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }
                using (var transaction = new TransactionScope())
                {

                    var alumnoSeccion = new AlumnoSeccion();

                    if (model.IdAlumnoSeccion.HasValue)
                    {
                        alumnoSeccion = context.AlumnoSeccion.First(x => x.IdAlumnoSeccion == model.IdAlumnoSeccion);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        context.AlumnoSeccion.Add(alumnoSeccion);
                    }

                    alumnoSeccion.IdAlumnoMatriculado = model.IdAlumnoMatriculado;
                    alumnoSeccion.IdSeccion = model.IdSeccion;
                    alumnoSeccion.EsDelegado = model.EsDelegado;
                    alumnoSeccion.IdProyectoAcademico = model.IdProyectoAcademico;
                    alumnoSeccion.Nota = model.Nota;

                    context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAlumnoSeccion");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdAlumnoSeccion);
                TryUpdateModel(model);
                return View(model);
            }
        }
*/
//************** END CONTROLLER *******************//
namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student
{
    public class AddEditAlumnoSeccionViewModel
    {
        public Int32? IdAlumnoSeccion { get; set; }

        [Display(Name = "IdAlumnoMatriculado")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdAlumnoMatriculado { get; set; }
        [Display(Name = "IdSeccion")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdSeccion { get; set; }
        [Display(Name = "EsDelegado")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Boolean EsDelegado { get; set; }
        [Display(Name = "IdProyectoAcademico")]
        public Int32? IdProyectoAcademico { get; set; }
        [Display(Name = "Nota")]
        public String Nota { get; set; }

        public List<AlumnoMatriculado> LstAlumnoMatriculado { get; set; }
        public List<Seccion> LstSeccion { get; set; }
        public List<ProyectoAcademico> LstProyectoAcademico { get; set; }

        public AddEditAlumnoSeccionViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdAlumnoSeccion)
        {
            this.IdAlumnoSeccion = IdAlumnoSeccion;

            if (IdAlumnoSeccion.HasValue)
            {
                var AlumnoSeccion = dataContext.context.AlumnoSeccion.First(x => x.IdAlumnoSeccion == IdAlumnoSeccion);
                this.IdAlumnoSeccion = AlumnoSeccion.IdAlumnoSeccion;
                this.IdAlumnoMatriculado = AlumnoSeccion.IdAlumnoMatriculado;
                this.IdSeccion = AlumnoSeccion.IdSeccion;
                this.EsDelegado = AlumnoSeccion.EsDelegado;
                this.IdProyectoAcademico = AlumnoSeccion.IdProyectoAcademico;
                this.Nota = AlumnoSeccion.Nota;
            }

            LstAlumnoMatriculado = dataContext.context.AlumnoMatriculado.ToList();
            LstSeccion = dataContext.context.Seccion.ToList();
            LstProyectoAcademico = dataContext.context.ProyectoAcademico.ToList();
        }
    }
}
