﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Report
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Records = new List<ListReportViewModel>();
        }
        public int CycleId { get; set; }
        public List<ListReportViewModel> Records { get; set; }
    }
}