﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Report
{
    public class ManageReportViewModel
    {
        public int IdCargo { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public int IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public string Cargo { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public string Codigo { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public string Nombres { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public string Apellidos { get; set; }
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public int IdSubModalidadPeriodoAcademico { get; set; }

    }
}