﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Report
{
    public class ListReportViewModel
    {
        public int IdCargo { get; set; }
        public int IdModalidad { get; set; }
        public int IdSubModalidad { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public string PeriodoAcademico { get; set; }
        public string Modalidad { get; set; }
        public string SubModalidad { get; set; }
        public string Cargo { get; set; }
        public string Codigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int IdSubmodalidadPeriodoAcademico { get; set; }



 
    }
}