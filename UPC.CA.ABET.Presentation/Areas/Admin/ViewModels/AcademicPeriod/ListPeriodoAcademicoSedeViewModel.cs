using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using PagedList;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.AcademicPeriod
{
    public class ListPeriodoAcademicoSedeViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<PeriodoAcademicoSede> LstPeriodoAcademicoSede { get; set; }

        public ListPeriodoAcademicoSedeViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
            this.p = p ?? 1;
            IQueryable<PeriodoAcademicoSede> queryPeriodoAcademicoSede = dataContext.context.PeriodoAcademicoSede.AsQueryable();
            queryPeriodoAcademicoSede = queryPeriodoAcademicoSede.OrderBy(x => x.IdPeriodoAcademicoSede);
            LstPeriodoAcademicoSede = queryPeriodoAcademicoSede.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
