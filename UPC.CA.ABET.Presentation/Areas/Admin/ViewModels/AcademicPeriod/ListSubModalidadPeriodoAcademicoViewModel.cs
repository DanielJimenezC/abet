﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using PagedList;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.AcademicPeriod
{
    public class ListSubModalidadPeriodoAcademicoViewModel
    {
        //new
        public Int32 p { get; set; }
        public IPagedList<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }

        public ListSubModalidadPeriodoAcademicoViewModel()
        {

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
            this.p = p ?? 1;
            var query = dataContext.context.SubModalidadPeriodoAcademico.AsQueryable();
            LstSubModalidadPeriodoAcademico = query.OrderBy(x => x.PeriodoAcademico.CicloAcademico).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}