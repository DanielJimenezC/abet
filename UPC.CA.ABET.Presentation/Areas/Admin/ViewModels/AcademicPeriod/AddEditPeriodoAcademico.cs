﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.AcademicPeriod;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.AcademicPeriod
{
    public class AddEditPeriodoAcademico
    {
        public Int32? IdPeriodoAcademico { get; set; }

        [Display(Name = "FechaInicioPeriodo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaInicioPeriodo { get; set; }

        [Display(Name = "FechaFinPeriodo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaFinPeriodo { get; set; }
        [Display(Name = "Semanas", ResourceType = typeof(AcademicPeriodResource))]
        public string Semanas { get; set; }
        [Display(Name = "SemanasModulo", ResourceType = typeof(AcademicPeriodResource))]
        public string SemanasModulo { get; set; }
        public bool EsMallaArizona { get; set; }
        [Display(Name = "FechaInicioCiclo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaInicioCiclo { get; set; }
        [Display(Name = "FechaFinCiclo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaFinCiclo { get; set; }
        [Display(Name = "FechaInicioModulo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaInicioModulo { get; set; }
        [Display(Name = "FechaFinModulo", ResourceType = typeof(AcademicPeriodResource))]
        public DateTime? FechaFinModulo { get; set; }
        [Display(Name = "NombreModulo", ResourceType = typeof(AcademicPeriodResource))]
        public string NombreModulo { get; set; }
        [Display(Name = "IdentificadorSeccion", ResourceType = typeof(AcademicPeriodResource))]
        public string IdentificadorSeccion { get; set; }       
        [Display(Name = "CicloAcademico", ResourceType = typeof(AcademicPeriodResource))]
        [Required]
        public String CicloAcademico { get; set; }        
        public List<Modalidad> LstModalidad { get; set; }
        public List<SubModalidad> LstSubModalidad { get; set; }
        public Modulo Modulo { get; set; }
        public List<Modulo> LstModulo { get; set; }
        public String ModalidadText { get; set; }
        public string SubModalitiesListUrl { get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }
        public IEnumerable<SelectListItem> SubModal
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
        public void CargarDatos(CargarDatosContext dataContext, int? IdPeriodoAcademico)
        {
  
            this.LstModulo = dataContext.context.Modulo.ToList();
            EsMallaArizona = false;
            if (IdPeriodoAcademico.HasValue)
            {
                this.IdPeriodoAcademico = IdPeriodoAcademico;
                var periodoAcademico = dataContext.context.PeriodoAcademico.Find(IdPeriodoAcademico.Value);
                var subModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico.Value);
                ModalidadText = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.IdPeriodoAcademico == periodoAcademico.IdPeriodoAcademico).SubModalidad.Modalidad.NombreEspanol;
                FechaInicioPeriodo = periodoAcademico.FechaInicioPeriodo;
                FechaFinPeriodo = periodoAcademico.FechaFinPeriodo;
                FechaInicioCiclo = periodoAcademico.FechaInicioPeriodo;
                FechaFinCiclo = periodoAcademico.FechaFinPeriodo;
                CicloAcademico = periodoAcademico.CicloAcademico;
                if (periodoAcademico.Semanas.HasValue)
                    Semanas = periodoAcademico.Semanas.Value.ToString();
                EsMallaArizona = subModalidadPeriodoAcademico.EsMallaArizona;
            }

        }
       
    }
}