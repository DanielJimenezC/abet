using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.Web.Mvc;
using PagedList;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.AcademicPeriod
{
    public class ListPeriodoAcademicoViewModel
    {
        public Int32 p { get; set; }
 
        //public IPagedList<SubModalidadPeriodoAcademicoModulo> LstSubModalidadPeriodoAcademicoModulo { get; set; }
        public IPagedList<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico{ get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }
        public Int32? ModalidadId { get; set; }
        public ListPeriodoAcademicoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, Int32? m)
        {
            this.p = p ?? 1;
     
			var query = (from a in dataContext.context.SubModalidadPeriodoAcademico
						  join b in dataContext.context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
						  join c in dataContext.context.Modalidad on b.IdModalidad equals c.IdModalidad
						  where c.IdModalidad == m
						  select a).OrderBy(x => x.PeriodoAcademico.CicloAcademico).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);

			LstSubModalidadPeriodoAcademico = query;		
		}
    }
}
