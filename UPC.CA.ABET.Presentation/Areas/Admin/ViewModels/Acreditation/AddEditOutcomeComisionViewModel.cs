using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class AddEditOutcomeComisionViewModel
    {
        public Int32? IdOutcomeComision { get; set; }

        [Display(Name = "Outcome", ResourceType = typeof(AcreditationResource))]
        [Required]
        public Int32 IdOutcome { get; set; }
        [Display(Name = "Comision", ResourceType = typeof(AcreditationResource))]
        [Required]
        public Int32 IdComision { get; set; }
        [Display(Name = "PeriodoAcademico", ResourceType = typeof(AcreditationResource))]
        [Required]
        public Int32 IdPeriodoAcademico { get; set; }
        [Display(Name = "SubModalidadPeriodoAcademico", ResourceType = typeof(AcreditationResource))]
        [Required]
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }

        public Int32 IdAcreditadora { get; set; }

        public List<Outcome> LstOutcome { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public AddEditOutcomeComisionViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idOutcomeComision, Int32 idAcreditadora )
        {
            //this.IdAcreditadora = idAcreditadora;
            //this.IdOutcomeComision = idOutcomeComision;

            //if (idOutcomeComision.HasValue)
            //{
            //    var OutcomeComision = dataContext.context.OutcomeComision.First(x => x.IdOutcomeComision == idOutcomeComision);
            //    this.IdOutcomeComision = OutcomeComision.IdOutcomeComision;
            //    this.IdOutcome = OutcomeComision.IdOutcome;
            //    this.IdComision = OutcomeComision.IdComision;
            //    this.IdSubModalidadPeriodoAcademico = OutcomeComision.IdSubModalidadPeriodoAcademico.Value; 
            //}

            //LstOutcome = dataContext.context.Outcome.ToList();
            //LstComision = dataContext.context.Comision.ToList();
            //LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();

            this.IdAcreditadora = idAcreditadora;
            this.IdOutcomeComision = idOutcomeComision;

            if (idOutcomeComision.HasValue)
            {
                var OutcomeComision = dataContext.context.OutcomeComision.First(x => x.IdOutcomeComision == idOutcomeComision);
                this.IdOutcomeComision = OutcomeComision.IdOutcomeComision;
                this.IdOutcome = OutcomeComision.IdOutcome;
                this.IdComision = OutcomeComision.IdComision;
                this.IdPeriodoAcademico = OutcomeComision.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            }

            LstOutcome = dataContext.context.Outcome.ToList();
            LstComision = dataContext.context.Comision.ToList();
            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
        }
    }
}
