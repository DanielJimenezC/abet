using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class ListAcreditadoraViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<Acreditadora> LstAcreditadora { get; set; }

        public ListAcreditadoraViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
            this.p = p ?? 1;
            var queryAcreditadora = dataContext.context.Acreditadora.AsQueryable();
            queryAcreditadora = queryAcreditadora.OrderBy(x => x.IdAcreditadora);
            LstAcreditadora = queryAcreditadora.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
