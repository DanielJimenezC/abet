using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class ListOutcomeViewModel
    {
		public Int32 p { get; set; }
        public IPagedList<Outcome> LstOutcome { get; set; }

        public ListOutcomeViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
			this.p = p ?? 1;
			var queryOutcome = dataContext.context.Outcome.AsQueryable();
            queryOutcome = queryOutcome.OrderBy(x => x.IdOutcome);
            LstOutcome = queryOutcome.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
