using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class AddEditAcreditadoraViewModel
    {
        public Int32? IdAcreditadora { get; set; }

        [Display(Name = "Nombre", ResourceType = typeof(AcreditationResource))]
        [Required]
        public String Nombre { get; set; }
        [Display(Name = "NombreIndicador", ResourceType = typeof(AcreditationResource))]
        [Required]
        public String NombreIndicador { get; set; }
        [Display(Name = "Descripcion", ResourceType = typeof(AcreditationResource))]
        public String Descripcion { get; set; }
        [Display(Name = "RutaImagen", ResourceType = typeof(AcreditationResource))]

        public HttpPostedFileBase RutaImagen { get; set; }

        public String RutaFoto { get; set; }

        public AddEditAcreditadoraViewModel()
        {

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdAcreditadora)
        {
            this.IdAcreditadora = IdAcreditadora;

            if (IdAcreditadora.HasValue)
            {
                var Acreditadora = dataContext.context.Acreditadora.First(x => x.IdAcreditadora == IdAcreditadora);
                this.IdAcreditadora = Acreditadora.IdAcreditadora;
                this.Nombre = Acreditadora.Nombre;
                this.NombreIndicador = Acreditadora.NombreIndicador;
                this.Descripcion = Acreditadora.Descripcion;
                this.RutaFoto = Acreditadora.RutaImagen;
            }

        }
    }
}
