using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class AddEditComisionViewModel
    {
        public Int32? IdComision { get; set; }

        [Display(Name = "Codigo", ResourceType = typeof(ComisionResource))]
        [Required]
        public String Codigo { get; set; }
        [Display(Name = "NombreEspanol", ResourceType = typeof(ComisionResource))]
        [Required]
        public String NombreEspanol { get; set; }
        [Display(Name = "NombreIngles", ResourceType = typeof(ComisionResource))]
        [Required]
        public String NombreIngles { get; set; }

        [Required]
        public Int32 IdAcreditadoraPeriodoAcademico { get; set; }
        public Int32 IdAcreditadora { get; set; }
        public AcreditadoraPeriodoAcademico AcreditadoraPeriodoAcademico { get; set; }

        public AddEditComisionViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idComision, Int32 idAcreditadoraPeriodoAcademico, Int32 idAcreditadora)
        {
            this.IdComision = idComision;
            this.IdAcreditadoraPeriodoAcademico = idAcreditadoraPeriodoAcademico;
            this.IdAcreditadora = idAcreditadora;

            this.AcreditadoraPeriodoAcademico = dataContext.context.AcreditadoraPeriodoAcademico.Find(IdAcreditadoraPeriodoAcademico);
            if (IdComision.HasValue)
            {
                var Comision = dataContext.context.Comision.First(x => x.IdComision == IdComision);
                this.IdComision = Comision.IdComision;
                this.Codigo = Comision.Codigo;
                this.NombreEspanol = Comision.NombreEspanol;
                this.NombreIngles = Comision.NombreIngles;
            }

        }
    }
}
