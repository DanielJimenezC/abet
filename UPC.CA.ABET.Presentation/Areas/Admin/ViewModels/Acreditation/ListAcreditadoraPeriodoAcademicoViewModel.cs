﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class ListAcreditadoraPeriodoAcademicoViewModel
    {
        [Display(Name = "PeriodoAcademico", ResourceType = typeof(AcreditationResource))]
        public Int32? IdPeriodoAcademico { get; set; }
        [Display(Name = "SubModalidadPeriodoAcademico", ResourceType = typeof(AcreditationResource))]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Acreditadora Acreditadora { get; set; }
        public Int32 p { get; set; }
        public IPagedList<AcreditadoraPeriodoAcademico> LstAcreditadoraPeriodoAcademico { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, Int32 IdAcreditadora, Int32? IdPeriodoAcademico)
        {
            this.p = p ?? 1;

			if(IdPeriodoAcademico.HasValue)
			{
				this.IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
			}

			int ModalidadId = Convert.ToInt32(dataContext.session.GetModalidadId());

			Acreditadora = dataContext.context.Acreditadora.FirstOrDefault(x => x.IdAcreditadora == IdAcreditadora);
			 LstPeriodoAcademico = (from a in dataContext.context.SubModalidadPeriodoAcademico
								   join b in dataContext.context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
								   join c in dataContext.context.PeriodoAcademico on a.IdPeriodoAcademico equals c.IdPeriodoAcademico
								   where b.IdModalidad == ModalidadId
								   select c
								  ).ToList();

            var query = dataContext.context.AcreditadoraPeriodoAcademico.
                Include(x => x.Acreditadora).
                Include(x => x.SubModalidadPeriodoAcademico).
                Include(x => x.Comision).
                Where(x => x.IdAcreditadora == IdAcreditadora && x.SubModalidadPeriodoAcademico.SubModalidad.IdModalidad== ModalidadId).
                AsQueryable();

            if (IdSubModalidadPeriodoAcademico.HasValue)
                query = query.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico.Value);

            LstAcreditadoraPeriodoAcademico = query.OrderBy(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
