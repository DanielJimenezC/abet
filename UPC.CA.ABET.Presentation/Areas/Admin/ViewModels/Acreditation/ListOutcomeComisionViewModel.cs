using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

//************** BEGIN CONTROLLER *******************//
/*
	public ActionResult ListOutcomeComision(Int32? p)
	{
		var ListOutcomeComisionViewModel = new ListOutcomeComisionViewModel();
		ListOutcomeComisionViewModel.CargarDatos(CargarDatosContext(),p);
		return View(ListOutcomeComisionViewModel);
	}
*/
//************** END CONTROLLER *******************//
namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class ListOutcomeComisionViewModel
    {
		public Int32 p { get; set; }
        public IPagedList<OutcomeComision> LstOutcomeComision { get; set; }

        public ListOutcomeComisionViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
			this.p = p ?? 1;
			var queryOutcomeComision = dataContext.context.OutcomeComision.AsQueryable();
            queryOutcomeComision = queryOutcomeComision.OrderBy(x => x.IdOutcomeComision);
            LstOutcomeComision = queryOutcomeComision.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
