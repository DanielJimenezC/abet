using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class AddEditOutcomeViewModel
    {
        public Int32? IdOutcome { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public String Nombre { get; set; }
        [Display(Name = "NombreIngles")]
        public String NombreIngles { get; set; }
        [Display(Name = "DescripcionEspanol")]
        [Required]
        public String DescripcionEspanol { get; set; }
        [Display(Name = "DescripcionIngles")]
        [Required]
        public String DescripcionIngles { get; set; }
        public Int32 IdAcreditadora { get; set; }

        public AddEditOutcomeViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdOutcome, Int32 idAcreditadora)
        {
            this.IdOutcome = IdOutcome;
            this.IdAcreditadora = idAcreditadora;
            if (IdOutcome.HasValue)
            {
                var Outcome = dataContext.context.Outcome.First(x => x.IdOutcome == IdOutcome);
                this.IdOutcome = Outcome.IdOutcome;
                this.Nombre = Outcome.Nombre;
                this.NombreIngles = Outcome.NombreIngles;
                this.DescripcionEspanol = Outcome.DescripcionEspanol;
                this.DescripcionIngles = Outcome.DescripcionIngles;
            }

        }
    }
}
