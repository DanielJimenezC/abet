using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Acreditation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class AddEditAcreditadoraPeriodoAcademicoViewModel
    {
        public Int32? IdAcreditadoraPreiodoAcademico { get; set; }

        [Display(Name = "Acreditadora", ResourceType = typeof(AcreditationResource))]
        public Int32? IdAcreditadora { get; set; }
        [Display(Name = "PeriodoAcademico", ResourceType = typeof(AcreditationResource))]
        public Int32? IdPeriodoAcademico { get; set; }
        public List<Acreditadora> LstAcreditadora { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademicoModalidad { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }

        public AddEditAcreditadoraPeriodoAcademicoViewModel()
        {
        }
        public List<PeriodoAcademico> ListaPeriodoAcademicoConModalidad(CargarDatosContext dataContext, List<PeriodoAcademico> listar)
        {
			int ModalidadId = dataContext.session.GetModalidadId();

            LstPeriodoAcademicoModalidad = (from pa in LstPeriodoAcademico
                                            join submoda in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submoda.IdPeriodoAcademico
                                            join subm in dataContext.context.SubModalidad on submoda.IdSubModalidad equals subm.IdSubModalidad
                                            join moda in dataContext.context.Modalidad on subm.IdModalidad equals moda.IdModalidad
											where moda.IdModalidad==ModalidadId
                                            select new PeriodoAcademico
                                            {
                                                IdPeriodoAcademico = pa.IdPeriodoAcademico,
                                                CicloAcademico = Convert.ToString(moda.NombreEspanol + " - " + pa.CicloAcademico)
                                            }).ToList();


            return LstPeriodoAcademicoModalidad;
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idAcreditadoraPreiodoAcademico, Int32 idAreditadora)
        {
            this.IdAcreditadoraPreiodoAcademico = idAcreditadoraPreiodoAcademico;
            this.IdAcreditadora = idAreditadora;
            var lstPeriodoAcademicoId = dataContext.context.AcreditadoraPeriodoAcademico.Where(x => x.IdAcreditadora == idAreditadora).Select(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico).ToList();
            if (IdAcreditadoraPreiodoAcademico.HasValue)
            {
                var acreditadoraPeriodoAcademico = dataContext.context.AcreditadoraPeriodoAcademico.FirstOrDefault(x => x.IdAcreditadoraPreiodoAcademico == idAcreditadoraPreiodoAcademico);
                this.IdAcreditadoraPreiodoAcademico = acreditadoraPeriodoAcademico.IdAcreditadoraPreiodoAcademico;
                this.IdAcreditadora = acreditadoraPeriodoAcademico.IdAcreditadora;
                this.IdPeriodoAcademico = acreditadoraPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                //lstPeriodoAcademicoId.Remove(IdPeriodoAcademico);

                LstPeriodoAcademico = dataContext.context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == this.IdPeriodoAcademico).ToList();
                LstPeriodoAcademicoModalidad = ListaPeriodoAcademicoConModalidad(dataContext, LstPeriodoAcademico);
            }
            else
            {
                LstPeriodoAcademico = dataContext.context.PeriodoAcademico.Where(x => !lstPeriodoAcademicoId.Contains(x.IdPeriodoAcademico)).ToList();
                LstPeriodoAcademicoModalidad = ListaPeriodoAcademicoConModalidad(dataContext, LstPeriodoAcademico);
            }
             LstPeriodoAcademico = dataContext.context.PeriodoAcademico.Where(x => !lstPeriodoAcademicoId.Contains(x.IdPeriodoAcademico)).ToList();
            LstPeriodoAcademicoModalidad = ListaPeriodoAcademicoConModalidad(dataContext, LstPeriodoAcademico);



        }
    }
}
