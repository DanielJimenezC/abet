using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation
{
    public class ListComisionAcreditadoraViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<Comision> LstComision { get; set; }
        public AcreditadoraPeriodoAcademico AcreditadoraPeriodoAcademico { get; set; }

        public ListComisionAcreditadoraViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, Int32 idAcreditadoraPeriodoAcademico)
        {
            this.p = p ?? 1;

            AcreditadoraPeriodoAcademico = dataContext.context.AcreditadoraPeriodoAcademico.FirstOrDefault(x => x.IdAcreditadoraPreiodoAcademico == idAcreditadoraPeriodoAcademico);

            var query = dataContext.context.Comision.Where(x => x.IdAcreditadoraPeriodoAcademico == idAcreditadoraPeriodoAcademico).AsQueryable();

            LstComision = query.OrderBy(x => x.IdComision).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
