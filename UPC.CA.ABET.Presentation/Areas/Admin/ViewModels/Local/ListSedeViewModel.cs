using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Local
{
    public class ListSedeViewModel
    {
		public Int32 p { get; set; }
        public IPagedList<Sede> LstSede { get; set; }

        public ListSedeViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
			this.p = p ?? 1;
			IQueryable<Sede> querySede = dataContext.context.Sede.AsQueryable();
            querySede = querySede.OrderBy(x => x.IdSede);
            LstSede = querySede.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
