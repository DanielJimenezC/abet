using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Local
{
    public class AddEditSedeViewModel
    {
        public Int32? IdSede { get; set; }

        [Display(Name = "Codigo")]
        [Required]
        public String Codigo { get; set; }
        [Display(Name = "Nombre")]
        [Required]
        public String Nombre { get; set; }


        public AddEditSedeViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdSede)
        {
            this.IdSede = IdSede;

            if (IdSede.HasValue)
            {
                var Sede = dataContext.context.Sede.First(x => x.IdSede == IdSede);
                this.IdSede = Sede.IdSede;
                this.Codigo = Sede.Codigo;
                this.Nombre = Sede.Nombre;
            }

        }
    }
}
