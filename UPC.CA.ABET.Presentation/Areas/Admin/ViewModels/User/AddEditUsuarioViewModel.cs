using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.User;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class AddEditUsuarioViewModel
    {
        public Int32? IdUsuario { get; set; }

        [Display(Name = "Codigo", ResourceType = typeof(UserResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Codigo { get; set; }
        [Display(Name = "Escuela", ResourceType = typeof(UserResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdEscuelaDefecto { get; set; }
        [Display(Name = "Password", ResourceType = typeof(UserResource))]
        public String Password { get; set; }
        [Display(Name = "Correo", ResourceType = typeof(UserResource))]
        [DataType(DataType.EmailAddress)]
        public String Correo { get; set; }
        [Display(Name = "IdDocente", ResourceType = typeof(UserResource))]
        public Int32? IdDocente { get; set; }
        [Display(Name = "Nombres", ResourceType = typeof(UserResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Nombres { get; set; }
        [Display(Name = "Apellidos", ResourceType = typeof(UserResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Apellidos { get; set; }
        [Display(Name = "Imagen", ResourceType = typeof(UserResource))]
        public HttpPostedFileBase Imagen { get; set; }
        public String RutaImagen { get; set; }
        [Display(Name = "Firma", ResourceType = typeof(UserResource))]
        public HttpPostedFileBase ImagenFirma { get; set; }
        public String RutaImagenFirma { get; set; }
        public List<Docente> LstDocente { get; set; }
        public List<Escuela> LstEscuelas { get; set; }

        public AddEditUsuarioViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdUsuario)
        {
            this.IdUsuario = IdUsuario;

            if (IdUsuario.HasValue)
            {
                var Usuario = dataContext.context.Usuario.First(x => x.IdUsuario == IdUsuario);
                this.IdUsuario = Usuario.IdUsuario;
                this.Codigo = Usuario.Codigo;
                this.Password = Usuario.Password;
                this.IdDocente = Usuario.IdDocente;
                this.IdEscuelaDefecto = Usuario.IdEscuelaDefecto;
                this.Nombres = Usuario.Nombres;
                this.Apellidos = Usuario.Apellidos;
                this.RutaImagen = Usuario.Imagen;
                this.RutaImagenFirma = Usuario.Firma;
                this.Correo = Usuario.Correo;
            }
            LstDocente = new List<Docente>();
            LstEscuelas = new List<Escuela>();
            LstEscuelas = dataContext.context.Escuela.ToList();
            //LstDocente = dataContext.context.Docente.ToList();
        }
    }
}
