using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class ListRolUsuarioViewModel
    {
        public Int32 IdUsuario { get; set; }
        public Usuario Usuario { get; set; }
        public List<RolUsuario> LstRolUsuario { get; set; }
        public String UltimoAcceso { get; set; }
        public List<Rol> LstRol { get; set; }
        public List<PeriodoAcademico> LstperiodoAcademico { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }
        public List<Escuela> LstEscuela { get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }
		public int CantidadActivos { get; set; }

        public ListRolUsuarioViewModel()
        {
            LstRolUsuario = new List<RolUsuario>();
            UltimoAcceso = LayoutResource.Ninguno;
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idUsuario)
        {
            this.IdUsuario = idUsuario;
            Usuario = dataContext.context.Usuario.Include(x => x.LogAcceso).FirstOrDefault(x => x.IdUsuario == IdUsuario);
            var logAcceso = Usuario.LogAcceso.OrderByDescending(x => x.FechaAcceso).FirstOrDefault();

            if (logAcceso != null)
            {
                var fechaAcceso = logAcceso.FechaAcceso;
                UltimoAcceso = (DateTime.Now - fechaAcceso).ToDiferenceTimeString(dataContext.currentCulture);
            }


            //LstperiodoAcademico = dataContext.context.PeriodoAcademico.OrderBy(x => x.CicloAcademico).ToList();
            //LstRol = dataContext.context.Rol.Where(x => x.Estado == ConstantHelpers.ESTADO.ACTIVO).ToList();
            //LstRolUsuario = dataContext.context.RolUsuario.Include(x => x.PeriodoAcademico).Include(x => x.Rol).Where(x => x.IdUsuario == IdUsuario).ToList();
            //LstEscuela = dataContext.context.Escuela.ToList();


            LstperiodoAcademico = dataContext.context.PeriodoAcademico.OrderBy(x => x.CicloAcademico).ToList();
            LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.OrderBy(x => x.PeriodoAcademico.CicloAcademico).ToList();
			CantidadActivos = LstSubModalidadPeriodoAcademico.Count(x => x.PeriodoAcademico.Estado == "ACT");

            LstRol = dataContext.context.Rol.Where(x => x.Estado == ConstantHelpers.ESTADO.ACTIVO).ToList();
           LstRolUsuario = dataContext.context.RolUsuario.Include(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico).Include(x => x.Rol).Where(x => x.IdUsuario == IdUsuario).ToList();
            //LstRolUsuario = dataContext.context.RolUsuario.Include(x => x.Rol).Where(x => x.IdUsuario == idUsuario).ToList();
            LstEscuela = dataContext.context.Escuela.ToList();
        }
    }
}
