using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class ListUsuarioViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<Usuario> LstUsuario { get; set; }

        [Display(Name = "CadenaBuscar", ResourceType = typeof(LayoutResource))]
        public String CadenaBuscar { get; set; }
        public ListUsuarioViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, String cadenaBuscar)
        {
            this.p = p ?? 1;
            this.CadenaBuscar = cadenaBuscar;
            IQueryable<Usuario> query = dataContext.context.Usuario.Include(x => x.Docente).Distinct().AsQueryable();

            if (!String.IsNullOrEmpty(CadenaBuscar))
                foreach (var token in CadenaBuscar.Split(' '))
                    query = query.Where(x => x.Nombres.Contains(token) || x.Apellidos.Contains(token) || x.Codigo.Contains(token)).Distinct();

            query = query.OrderBy(x => x.IdUsuario);
            LstUsuario = query.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
