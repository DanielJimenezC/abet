using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class ListRolViewModel
    {
		public Int32 p { get; set; }
        public IPagedList<Rol> LstRol { get; set; }

        public ListRolViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
			this.p = p ?? 1;
			IQueryable<Rol> queryRol = dataContext.context.Rol.AsQueryable();
            queryRol = queryRol.OrderBy(x => x.IdRol);
            LstRol = queryRol.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
