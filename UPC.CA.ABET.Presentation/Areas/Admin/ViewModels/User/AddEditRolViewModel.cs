using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class AddEditRolViewModel
    {
        public Int32? IdRol { get; set; }

        [Display(Name = "Descripcion")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Descripcion { get; set; }
        [Display(Name = "Acronimo")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Acronimo { get; set; }
        [Display(Name = "Estado")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String Estado { get; set; }


        public AddEditRolViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdRol)
        {
            this.IdRol = IdRol;

            if (IdRol.HasValue)
            {
                var Rol = dataContext.context.Rol.First(x => x.IdRol == IdRol);
                this.IdRol = Rol.IdRol;
                this.Descripcion = Rol.Descripcion;
                this.Acronimo = Rol.Acronimo;
                this.Estado = Rol.Estado;
            }

        }
    }
}
