﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class _AddUsuarioSecundarioViewModel
    {
        [Required]
        public Int32 IdUsuario { get; set; }
        [Required]
        public Int32 IdUsuarioSecundario { get; set; }
        public String Codigo { get; set; }
        public void CargarDatos(CargarDatosContext dataContext, Int32 idUsuario)
        {
            this.IdUsuario = idUsuario;
            var usuario = dataContext.context.Usuario.Find(IdUsuario);
            if (usuario.IdUsuarioSecundario.HasValue)
            {
                this.IdUsuarioSecundario = usuario.IdUsuarioSecundario.Value;
                this.Codigo = usuario.Usuario2.Codigo +  " - " + usuario.Usuario2.Nombres + " " + usuario.Usuario2.Apellidos;
            }
        }
    }
}