using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User
{
    public class _AddEditRolUsuarioViewModel
    {
        public Int32? IdRolUsuario { get; set; }

        [Display(Name = "IdRol")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdRol { get; set; }
        [Display(Name = "IdUsuario")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdUsuario { get; set; }
        [Display(Name = "IdPeriodoAcademico")]
        public Int32? IdPeriodoAcademico { get; set; }
        [Display(Name = "IdSubModalidadPeriodoAcademico")]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }

        public List<Rol> LstRol { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }

        public _AddEditRolUsuarioViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdRolUsuario, Int32 IdUsuario)
        {
            this.IdRolUsuario = IdRolUsuario;
            this.IdUsuario = IdUsuario;

            if (IdRolUsuario.HasValue)
            {
                var RolUsuario = dataContext.context.RolUsuario.First(x => x.IdRolUsuario == IdRolUsuario);
                this.IdRolUsuario = RolUsuario.IdRolUsuario;
                this.IdRol = RolUsuario.IdRol;
                this.IdSubModalidadPeriodoAcademico = RolUsuario.IdSubModalidadPeriodoAcademico;
            }

            LstRol = dataContext.context.Rol.ToList();
            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
        }
    }
}
