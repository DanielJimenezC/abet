﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Articulation
{
    public class AddEditArticulacionViewModel
    {
        public Int32? IdMallaCocos { get; set; }

        public List<Carrera> LstCarrera { get; set; }
        public List<Acreditadora> LstAceditadora { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public MallaCocos Articulacion { get; set; }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idMallaCocos)
        {
            if (idMallaCocos.HasValue)
                this.Articulacion = dataContext.context.MallaCocos.FirstOrDefault(x => x.IdMallaCocos == IdMallaCocos);

            var escuelaId = dataContext.session.GetEscuelaId();

            this.LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            this.LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            this.LstAceditadora = dataContext.context.Acreditadora.ToList();
            this.LstComision = dataContext.context.Comision.ToList();
        }
    }
}