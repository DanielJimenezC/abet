﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Articulation
{
    public class _ListArticulacionViewModel
    {
        public int p { get; set; }
        public int? CarreraId { get; set; }
        public int? AcreditadoraId { get; set; }
        public int? ComisionId { get; set; }
        public int? PeriodoAcademicoId { get; set; }
        public int? SubModadalidadPeriodoAcademicoId { get; set; }
        public IPagedList<MallaCocos> LstArticulacion { get; set; }

        public void CargarDatos(CargarDatosContext dataContext, int? p, ArticulacionViewModel model)
        {
            this.p = p ?? 1;
            this.SubModadalidadPeriodoAcademicoId = model.SubModalidadPeriodoAcademicoId;
            this.CarreraId = model.CarreraId;
            this.ComisionId = model.ComisionId;
            this.AcreditadoraId = model.AcreditadoraId;

            var queryArticulacion = dataContext.context.MallaCocos.AsQueryable();
            if (this.SubModadalidadPeriodoAcademicoId.HasValue)
            {
                queryArticulacion = queryArticulacion.Where(x => x.IdSubModalidadPeriodoAcademico == SubModadalidadPeriodoAcademicoId);
            }
            if (this.CarreraId.HasValue)
            {
                queryArticulacion = queryArticulacion.Where(x => x.CarreraComision.IdCarrera == CarreraId);
            }
            if (this.ComisionId.HasValue)
            {
                queryArticulacion = queryArticulacion.Where(x => x.CarreraComision.IdComision == ComisionId);
            }
            if (this.AcreditadoraId.HasValue)
            {
                queryArticulacion = queryArticulacion.Where(x => x.CarreraComision.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora == AcreditadoraId);
            }
            LstArticulacion = queryArticulacion.OrderByDescending(x => x.IdSubModalidadPeriodoAcademico).ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}