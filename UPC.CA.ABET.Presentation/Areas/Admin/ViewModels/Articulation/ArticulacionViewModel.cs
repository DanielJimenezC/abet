﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Articulation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Articulation
{
    public class ArticulacionViewModel
    {
        [Display(Name = "Carrera", ResourceType = typeof(ArticulacionResource))]
        public Int32? CarreraId { get; set; }
        [Display(Name = "Acreditadora", ResourceType = typeof(ArticulacionResource))]
        public Int32? AcreditadoraId { get; set; }
        [Display(Name = "Comision", ResourceType = typeof(ArticulacionResource))]
        public Int32? ComisionId { get; set; }
        [Display(Name = "PeriodoAcademico", ResourceType = typeof(ArticulacionResource))]
        public Int32? PeriodoAcademicoId { get; set; }

        [Display(Name = "SubModalidadPeriodoAcademico", ResourceType = typeof(ArticulacionResource))]
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }

        public List<Carrera> LstCarrera { get; set; }
        public List<Acreditadora> LstAceditadora { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }

        public void CargarDatos(CargarDatosContext dataContext, Int32? carreraId, Int32? acreditadoraId, Int32? comisionId, Int32? SubModalidadPeriodoAcademicoId)
        {
            this.SubModalidadPeriodoAcademicoId = SubModalidadPeriodoAcademicoId;
            this.CarreraId = carreraId;
            this.ComisionId = comisionId;
            this.AcreditadoraId = acreditadoraId;

            var idEscuela = dataContext.session.GetEscuelaId();
   //OBSERVACION         var PeriodoAcademicoId = dataContext.session.GetPeriodoAcademicoId();
            var idSubModalidadPeriodoAcademico = dataContext.session.GetPeriodoAcademicoId();

            this.LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            this.LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == idEscuela).ToList();
            this.LstAceditadora = dataContext.context.Acreditadora.ToList();
            this.LstComision = dataContext.context.Comision.Include(x => x.AcreditadoraPeriodoAcademico).Where(x => x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).ToList();
        }
    }
}