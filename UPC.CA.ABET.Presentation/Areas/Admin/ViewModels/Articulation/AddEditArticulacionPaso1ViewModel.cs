﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Articulation;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Articulation
{
    public class AddEditArticulacionPaso1ViewModel
    {
        public Int32? IdMallaCocos { get; set; }
        [Display(Name = "Carrera", ResourceType = typeof(ArticulacionResource))]
        public Int32? CarreraId { get; set; }
        [Display(Name = "Acreditadora", ResourceType = typeof(ArticulacionResource))]
        public Int32? AcreditadoraId { get; set; }
        [Display(Name = "Comision", ResourceType = typeof(ArticulacionResource))]
        public Int32? ComisionId { get; set; }
        [Display(Name = "PeriodoAcademico", ResourceType = typeof(ArticulacionResource))]


        public Int32? PeriodoAcademicoId { get; set; }
        internal void CargarDatos(CargarDatosContext dataContext, int? idMallaCocos)
        {
            this.IdMallaCocos = idMallaCocos;

            throw new NotImplementedException();
        }
    }
}