using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;


namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class ListEscuelaViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<Escuela> LstEscuela { get; set; }

        public ListEscuelaViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p)
        {
            this.p = p ?? 1;
            var queryEscuela = dataContext.context.Escuela.AsQueryable();
            queryEscuela = queryEscuela.OrderBy(x => x.IdEscuela);
            LstEscuela = queryEscuela.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
