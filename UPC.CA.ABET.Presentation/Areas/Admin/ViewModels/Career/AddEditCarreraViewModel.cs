using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Career;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class AddEditCarreraViewModel
    {
        public Int32? IdCarrera { get; set; }

        [Display(Name = "Codigo", ResourceType = typeof(CarreraResource))]
        [Required]
        public String Codigo { get; set; }
        [Display(Name = "NombreEspanol", ResourceType = typeof(CarreraResource))]
        [Required]
        public String NombreEspanol { get; set; }
        [Display(Name = "NombreIngles", ResourceType = typeof(CarreraResource))]
        [Required]
        public String NombreIngles { get; set; }
        [Display(Name = "Escuela", ResourceType = typeof(CarreraResource))]
        [Required]
        public Int32? IdEscuela { get; set; }

        public List<Escuela> LstEscuela { get; set; }
		public List<Modalidad> LstModalidad { get; set; }
		public Int32? IdModalidad { get; set; }

        public AddEditCarreraViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdCarrera)
        {
            this.IdCarrera = IdCarrera;

            if (IdCarrera.HasValue)
            {
                var Carrera = dataContext.context.Carrera.First(x => x.IdCarrera == IdCarrera);
				var Submodalidad = dataContext.context.SubModalidad.Where(x => x.IdSubModalidad == Carrera.IdSubmodalidad).FirstOrDefault();
                this.IdCarrera = Carrera.IdCarrera;
                this.Codigo = Carrera.Codigo;
                this.NombreEspanol = Carrera.NombreEspanol;
                this.NombreIngles = Carrera.NombreIngles;
                this.IdEscuela = Carrera.IdEscuela;
				this.IdModalidad = Submodalidad.IdModalidad;
			}
            LstEscuela = dataContext.context.Escuela.ToList();
            LstModalidad = dataContext.context.Modalidad.ToList();

            LstModalidad = LstModalidad.Where(e => e.IdModalidad == HttpContext.Current.Session.GetModalidadId()).ToList();
        }
    }
}
