using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using PagedList;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class ListCarreraViewModel
    {
        public Int32 p { get; set; }
        public IPagedList<Carrera> LstCarrera { get; set; }

        public ListCarreraViewModel()
        {
        }

		public void CargarDatos(CargarDatosContext ctx, Int32? p)
		{
			int ModalidadId = ctx.session.GetModalidadId();

			this.p = p ?? 1;

			var queryCarrera = (from a in ctx.context.Carrera
								join b in ctx.context.SubModalidad on a.IdSubmodalidad equals b.IdSubModalidad
								where b.IdModalidad == ModalidadId
								select a);



			queryCarrera = queryCarrera.OrderBy(x => x.IdCarrera);


            LstCarrera = queryCarrera.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}
