using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class AddEditCarreraPeriodoAcademicoViewModel
    {
        public Int32? IdCarreraPeriodoAcademico { get; set; }

        [Display(Name = "IdCarrera")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdCarrera { get; set; }
        [Display(Name = "IdPeriodoAcademico")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdPeriodoAcademico { get; set; }

        [Display(Name = "IdSubModalidadPeriodoAcademico")]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }

        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public AddEditCarreraPeriodoAcademicoViewModel()
        {
        }
         

        public void CargarDatos(CargarDatosContext dataContext, Int32? idCarreraPeriodoAcademico, Int32 idCarrera)
        {
            this.IdCarreraPeriodoAcademico = idCarreraPeriodoAcademico;
            this.IdCarrera = idCarrera;
            var lstPeriodoAcademicoId = dataContext.context.CarreraPeriodoAcademico.Where(x => x.IdCarrera == IdCarrera).Select(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico).ToList();

			int ModalidadId = dataContext.session.GetModalidadId();

            if (IdCarreraPeriodoAcademico.HasValue)
            {
                var carreraPeriodoAcademico = dataContext.context.CarreraPeriodoAcademico.First(x => x.IdCarreraPeriodoAcademico == idCarreraPeriodoAcademico);
                this.IdCarreraPeriodoAcademico = carreraPeriodoAcademico.IdCarreraPeriodoAcademico;
                this.IdCarrera = carreraPeriodoAcademico.IdCarrera;
                //this.IdSubModalidadPeriodoAcademico = carreraPeriodoAcademico.IdSubModalidadPeriodoAcademico.Value;

                this.IdPeriodoAcademico =
                    carreraPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico;


                lstPeriodoAcademicoId.Remove(IdPeriodoAcademico);

            }
          
			LstPeriodoAcademico = (from a in dataContext.context.SubModalidadPeriodoAcademico
								   join b in dataContext.context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
								   join c in dataContext.context.PeriodoAcademico on a.IdPeriodoAcademico equals c.IdPeriodoAcademico
								   where b.IdModalidad == ModalidadId
								   select c
								   ).Where(x => !lstPeriodoAcademicoId.Contains(x.IdPeriodoAcademico)).ToList();

			//  LstPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => !lstPeriodoAcademicoId.Contains(x.IdPeriodoAcademico)).ToList();
		}
    }
}
