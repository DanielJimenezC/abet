using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Career;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class AddEditEscuelaViewModel
    {
        public Int32? IdEscuela { get; set; }

        [Display(Name = "Codigo", ResourceType= typeof(EscuelaResource))]
        [Required]
        public String Codigo { get; set; }
        [Display(Name = "Nombre", ResourceType = typeof(EscuelaResource))]
        [Required]
        public String Nombre { get; set; }


        public AddEditEscuelaViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? IdEscuela)
        {
            this.IdEscuela = IdEscuela;

            if (IdEscuela.HasValue)
            {
                var Escuela = dataContext.context.Escuela.First(x => x.IdEscuela == IdEscuela);
                this.IdEscuela = Escuela.IdEscuela;
                this.Codigo = Escuela.Codigo;
                this.Nombre = Escuela.Nombre;
            }

        }
    }
}
