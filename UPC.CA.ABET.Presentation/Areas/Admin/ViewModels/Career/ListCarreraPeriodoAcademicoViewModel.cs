﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career
{
    public class ListCarreraPeriodoAcademicoViewModel
    {
        public Int32 p { get; set; }
        public Int32 IdCarrera { get; set; }
        public Carrera Carrera { get; set; }
        public IPagedList<CarreraPeriodoAcademico> ListCarreraPeriodoAcademico { get; set; }

        public ListCarreraPeriodoAcademicoViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? p, Int32 idCarrera)
        {
            this.IdCarrera = idCarrera;
            this.Carrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera);            
            this.p = p ?? 1;
			int ModalidadId = dataContext.session.GetModalidadId();

            //  var queryCarrera = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico).Include(x => x.Carrera).Where(x => x.IdCarrera == idCarrera).AsQueryable();
            //queryCarrera = queryCarrera.OrderBy(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico);
            //ListCarreraPeriodoAcademico = queryCarrera.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);

            var querycarrera = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico).Include(x => x.Carrera).Where(x => x.IdCarrera == idCarrera).AsQueryable();

            querycarrera = querycarrera.OrderBy(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico);

			querycarrera = querycarrera.Where(x => x.SubModalidadPeriodoAcademico.SubModalidad.IdModalidad == ModalidadId);

            ListCarreraPeriodoAcademico = querycarrera.ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}