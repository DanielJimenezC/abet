﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Report;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class ReportController : BaseController
    {        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (TempData["CycleId"] != null)
            {
                TempData.Keep("CycleId");
            }
            base.OnActionExecuting(filterContext);
        }

        public ActionResult Index()
        {
            var reportBaseModel = new ReportBaseModel(Context);
            int idsubmodalidadPeriodoAcademico =  Convert.ToInt32( Session.GetSubModalidadPeriodoAcademicoId());

            var cycles = (from a in Context.PeriodoAcademico
                          join b in Context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                          where b.IdSubModalidadPeriodoAcademico == idsubmodalidadPeriodoAcademico
                          select a.CicloAcademico).ToList(); 
            ViewBag.DropDownListCycle = cycles;

            var cycleId = 0;

            if (TempData["CycleId"] != null)
            {
                cycleId = TempData["CycleId"].ToInteger();
            }

            var data = Context.CargoReporteLogro.Where(x => cycleId == 0 || x.IdSubModalidadPeriodoAcademico == cycleId).ToList();
            var records = data.Select(x => new ListReportViewModel()
            {
                IdCargo = x.IdCargo,
                IdSubmodalidadPeriodoAcademico = x.IdSubModalidadPeriodoAcademico.Value,
                PeriodoAcademico = x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                Cargo = x.Cargo,
                Codigo = x.Codigo,
                Nombres = x.Nombres,
                Apellidos = x.Apellidos

            }).ToList();

            return View(new IndexViewModel() 
            { 
                Records = records,
                CycleId = cycleId
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(IndexViewModel viewModel)
        {
            TempData["CycleId"] = viewModel.CycleId;
            return RedirectToAction("Index");
            
            //var data = context.CargoReporteLogro.Where(x => x.IdPeriodoAcademico == viewModel.CycleId).ToList();
            //var records = data.Select(x => new ListReportViewModel()
            //{
            //    IdCargo = x.IdCargo,
            //    IdPeriodoAcademico = x.IdPeriodoAcademico,
            //    PeriodoAcademico = x.PeriodoAcademico.CicloAcademico,
            //    Cargo = x.Cargo,
            //    Codigo = x.Codigo,
            //    Nombres = x.Nombres,
            //    Apellidos = x.Apellidos

            //}).ToList();
            

            //return PartialView("_ListReport", records);
        }

        public ActionResult Manage(int? id)
        {
            var viewModel = new ManageReportViewModel 
            {
                IdPeriodoAcademico = TempData["CycleId"] != null ? TempData.Peek("CycleId").ToInteger() : 0
            };

            if (id.HasValue)
            {
                var item = Context.CargoReporteLogro.FirstOrDefault(x => x.IdCargo == id.Value);

                viewModel = new ManageReportViewModel()
                {
                    IdCargo = item.IdCargo,
                    IdSubModalidadPeriodoAcademico = item.IdSubModalidadPeriodoAcademico.Value,
                    Cargo = item.Cargo,
                    Codigo = item.Codigo,
                    Nombres = item.Nombres,
                    Apellidos = item.Apellidos
                };
            }

            ViewBag.DropDownListCycle = new ReportBaseModel(Context).DropDownListCycle();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ManageReportViewModel viewModel)
        {
            var semesterCount = Context.CargoReporteLogro.Where(x => x.IdSubModalidadPeriodoAcademico == viewModel.IdSubModalidadPeriodoAcademico && x.IdCargo != viewModel.IdCargo).Count();

            if (semesterCount >= AppSettingsHelper.GetAppSettings("ReportMaxItems").ToInteger())
            {
                PostMessage(MessageType.Error, string.Format(MessageResource.ElMaximoNumeroDeRegistrosPorCicloEs + " {0}", AppSettingsHelper.GetAppSettings("ReportMaxItems").ToInteger()));
                return RedirectToAction("Manage");
            }            

            if (ModelState.IsValid)
            {
                if (viewModel.IdCargo == 0)
                {
                    var item = new CargoReporteLogro()
                    {
                        IdSubModalidadPeriodoAcademico = viewModel.IdSubModalidadPeriodoAcademico,
                        Cargo = viewModel.Cargo,
                        Nombres = viewModel.Nombres,
                        Apellidos = viewModel.Apellidos,
                        Codigo = viewModel.Codigo
                    };

                    Context.CargoReporteLogro.Add(item);
                }
                else 
                {
                    var item = Context.CargoReporteLogro.FirstOrDefault(x => x.IdCargo == viewModel.IdCargo); 

                    item.IdSubModalidadPeriodoAcademico = viewModel.IdSubModalidadPeriodoAcademico;
                    item.Cargo = viewModel.Cargo;
                    item.Nombres = viewModel.Nombres;
                    item.Apellidos = viewModel.Apellidos;
                    item.Codigo = viewModel.Codigo;
                }

                Context.SaveChanges();

                TempData.Remove("CycleId");

                return RedirectToAction("Index");
            }

            return RedirectToAction("Manage");
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDelete(int id)
        {
            var success = false;

            if (ModelState.IsValid)
            {
                var item = Context.CargoReporteLogro.FirstOrDefault(x => x.IdCargo == id);

                Context.CargoReporteLogro.Remove(item);
                Context.SaveChanges();

                success = true;
            }

            ViewBag.Saved = success;

            return PartialView("_Delete", id);
        }

        public ActionResult Duplicate()
        {
            var cycleFromId = 0;
            if (TempData["CycleId"] != null)
            {
                cycleFromId = TempData["CycleId"].ToInteger();
            }

            ViewBag.CycleFromId = cycleFromId;
            //var cycles = new LoadCombosLogic(context).ListCycles().Where(x => x.Key != cycleFromId).ToList();
            var cycles = Context.CargoReporteLogro
                .Select(crl => crl.SubModalidadPeriodoAcademico)
                .Distinct()
                .Select(pa => new { Key = pa.IdSubModalidadPeriodoAcademico, Value = pa.PeriodoAcademico.CicloAcademico })
                .ToList();
            
            ViewBag.DropDownListCycle =  new SelectList(cycles, "Key", "Value");
            return PartialView("_Duplicate");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDuplicate(int CycleFromId, int CycleToId)
        {
            var success = false;

            if (ModelState.IsValid)
            {
                var charges = Context.CargoReporteLogro.Where(c => c.IdSubModalidadPeriodoAcademico == CycleToId).ToList();

                var newCharges = charges.Select(c => new CargoReporteLogro
                {
                    Apellidos = c.Apellidos,
                    Cargo = c.Cargo,
                    Codigo = c.Codigo,
                    IdSubModalidadPeriodoAcademico = CycleFromId,
                    Nombres = c.Nombres
                });

                Context.CargoReporteLogro.AddRange(newCharges);

                Context.SaveChanges();
                success = true;
            }

            ViewBag.Saved = success;
            ViewBag.CycleFromId = CycleFromId;
            ViewBag.DropDownListCycle = new ReportBaseModel(Context).DropDownListCycle();

            return PartialView("_Duplicate");
        }

    }
}