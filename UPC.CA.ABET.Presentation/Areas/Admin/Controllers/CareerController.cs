﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Career;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class CareerController : BaseController
    {
        // GET: Admin/Faculty
        public ActionResult ListCarrera(Int32? p)
        {
            var viewModel = new ListCarreraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p);
            return View(viewModel);
        }

        public ActionResult AddEditCarrera(Int32? IdCarrera)
        {
            var viewModel = new AddEditCarreraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdCarrera);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditCarrera(AddEditCarreraViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdCarrera);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error);
                        return View(model);
                    }

                    var carrera = new Carrera();

                    if (model.IdCarrera.HasValue)
                    {
                        carrera = Context.Carrera.First(x => x.IdCarrera == model.IdCarrera);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Carrera.Add(carrera);
                    }

					int SubModalidadId = Context.SubModalidad.Where(x => x.IdModalidad == model.IdModalidad && x.NombreEspanol == "Regular").FirstOrDefault().IdSubModalidad;

                    carrera.Codigo = model.Codigo;
                    carrera.NombreEspanol = model.NombreEspanol;
                    carrera.NombreIngles = model.NombreIngles;
                    carrera.IdEscuela = model.IdEscuela;
					carrera.IdSubmodalidad = SubModalidadId;

					Context.SaveChanges();
                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListCarrera");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdCarrera);
                TryUpdateModel(model);
                return View(model);
            }
        }
        public ActionResult ListEscuela(Int32? p)
        {
            var ListEscuelaViewModel = new ListEscuelaViewModel();
            ListEscuelaViewModel.CargarDatos(CargarDatosContext(), p);
            return View(ListEscuelaViewModel);
        }

        public ActionResult AddEditEscuela(Int32? IdEscuela)
        {
            var EditEscuelaViewModel = new AddEditEscuelaViewModel();
            EditEscuelaViewModel.CargarDatos(CargarDatosContext(), IdEscuela);
            return View(EditEscuelaViewModel);
        }

        [HttpPost]
        public ActionResult AddEditEscuela(AddEditEscuelaViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdEscuela);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error);
                        return View(model);
                    }

                    var escuela = new Escuela();

                    if (model.IdEscuela.HasValue)
                    {
                        escuela = Context.Escuela.First(x => x.IdEscuela == model.IdEscuela);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Escuela.Add(escuela);
                    }

                    escuela.Codigo = model.Codigo;
                    escuela.Nombre = model.Nombre;
                    Context.SaveChanges();

                    var lstEscuela = Context.Escuela.Select(x => new JsonEntity() { Text = x.Nombre, Value = x.IdEscuela.ToString() }).ToList();
                    CacheHelper.Replace<List<JsonEntity>>(lstEscuela, CacheKey.LstEscuela);

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListEscuela");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdEscuela);
                TryUpdateModel(model);
                return View(model);
            }
        }

        public ActionResult ListCarreraPeriodoAcademico(Int32 IdCarrera, Int32? p)
        {
            var viewModel = new ListCarreraPeriodoAcademicoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, IdCarrera);
            return View(viewModel);
        }

        public ActionResult AddEditCarreraPeriodoAcademico(Int32? IdCarreraPeriodoAcademico, Int32 IdCarrera)
        {
            var viewModel = new AddEditCarreraPeriodoAcademicoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdCarreraPeriodoAcademico, IdCarrera);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditCarreraPeriodoAcademico(AddEditCarreraPeriodoAcademicoViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdCarreraPeriodoAcademico, model.IdCarrera);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }
                using (var transaction = new TransactionScope())
                {

                    var CarreraPeriodoAcademico = new CarreraPeriodoAcademico();

                    if (model.IdCarreraPeriodoAcademico.HasValue)
                    {
                        CarreraPeriodoAcademico = Context.CarreraPeriodoAcademico.First(x => x.IdCarreraPeriodoAcademico == model.IdCarreraPeriodoAcademico);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.CarreraPeriodoAcademico.Add(CarreraPeriodoAcademico);
                    }
                    var submodalidaPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                    CarreraPeriodoAcademico.IdCarrera = model.IdCarrera;
                    CarreraPeriodoAcademico.IdSubModalidadPeriodoAcademico = submodalidaPeriodoAcademico;

                    Context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListCarreraPeriodoAcademico", new { IdCarrera = model.IdCarrera });
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdCarreraPeriodoAcademico, model.IdCarrera);
                TryUpdateModel(model);
                return View(model);
            }
        }
    }
}