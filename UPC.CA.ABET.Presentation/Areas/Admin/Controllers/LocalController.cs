﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Local;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class LocalController : BaseController
    {
        // GET: Admin/Local
        public ActionResult ListSede(Int32? p)
        {
            var ListSedeViewModel = new ListSedeViewModel();
            ListSedeViewModel.CargarDatos(CargarDatosContext(), p);
            return View(ListSedeViewModel);
        }

        public ActionResult EditSede(Int32? IdSede)
        {
            var EditSedeViewModel = new AddEditSedeViewModel();
            EditSedeViewModel.CargarDatos(CargarDatosContext(), IdSede);
            return View(EditSedeViewModel);
        }

        [HttpPost]
        public ActionResult EditSede(AddEditSedeViewModel model)
        {
            try
            {
                using (var TransactionScope = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdSede);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error);
                        return View(model);
                    }

                    var sede = new Sede();

                    if (model.IdSede.HasValue)
                    {
                        sede = Context.Sede.First(x => x.IdSede == model.IdSede);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Sede.Add(sede);
                    }

                    sede.Codigo = model.Codigo;
                    sede.Nombre = model.Nombre;

                    Context.SaveChanges();

                    TransactionScope.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListSede");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdSede);
                TryUpdateModel(model);
                return View(model);
            }
        }
    }
}