﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class AdminModularityPeriodApiController : BaseController
    {

        // GET: Upload/ModularityPeriodApi

        [HttpPost]
        public ActionResult GetSubModalitiesList(Int32 ModalityId)
        {
            var subModalitiesList = Context.SubModalidad
                        .Where(x => x.IdModalidad == ModalityId)
                        .OrderByDescending(x => x.IdSubModalidad)
                        .Select(x => new { key = x.IdSubModalidad, value = x.NombreEspanol })
                        .ToList();
            return Json(subModalitiesList);
        }

        [HttpPost]
        public ActionResult GetTermsList(Int32 SubModalityId)
        {
            var termsList = Context.SubModalidadPeriodoAcademico
                        .Where(x => x.IdSubModalidad == SubModalityId)
                        .OrderByDescending(x => x.IdSubModalidadPeriodoAcademico)
                        .Select(x => new { key = x.IdSubModalidadPeriodoAcademico, value = x.PeriodoAcademico.CicloAcademico })
                        .ToList();
            return Json(termsList);
        }

    }
}