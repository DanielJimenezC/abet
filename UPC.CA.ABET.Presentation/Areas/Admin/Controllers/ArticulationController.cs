﻿#region Import
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Articulation;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
#endregion

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.AdministradorEscuela)]
    public class ArticulationController : BaseController
    {
        #region LstArticulacion
        public ActionResult Index(Int32? CarreraId, Int32? AcreditadoraId, Int32? ComisionId, Int32? PeriodoAcademicoId)
        {
            var viewModel = new ArticulacionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), CarreraId, AcreditadoraId, ComisionId, PeriodoAcademicoId);
            return View(viewModel);
        }

        public ActionResult _ListArticulacion(Int32? p, ArticulacionViewModel model)
        {
            var viewModel = new _ListArticulacionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, model);
            return View(viewModel);
        }
        #endregion


        public ActionResult AddEditArticulacionPaso1(Int32? IdMallaCocos)
        {
            if (IdMallaCocos.HasValue)
            {
                RedirectToAction("AddEditArticulacion", new { IdMallaCocos = IdMallaCocos });
            }
            var viewModel = new AddEditArticulacionPaso1ViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdMallaCocos);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditArticulacionPaso1(AddEditArticulacionPaso1ViewModel model)
        {
            return RedirectToAction("AddEditArticulacion", new { AcreditadoraId = model.AcreditadoraId});
        }

        public ActionResult AddEditArticulacion(AddEditArticulacionPaso1ViewModel model)
        {
            var viewModel = new AddEditArticulacionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), model.IdMallaCocos);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditArticulacion(AddEditArticulacionViewModel model, FormCollection form)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    PostMessage(MessageType.Error);
                    TryUpdateModel(model);
                    model.CargarDatos(CargarDatosContext(), model.IdMallaCocos);
                    return View(model);
                }
                using (var transaction = new TransactionScope())
                {
                    transaction.Complete();
                }

                return RedirectToAction("Articulacion");
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                model.CargarDatos(CargarDatosContext(), model.IdMallaCocos);
                return View(model);
            }
        }
    }
}