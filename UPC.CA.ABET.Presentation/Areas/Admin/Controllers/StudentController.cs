﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Student;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class StudentController : BaseController
    {
        // GET: Admin/Student
        public ActionResult ListAlumnoMatriculado(Int32? p, String CadenaBuscar, Int32? PeriodoAcademicoId)
        {
            var viewModel = new ListAlumnoMatriculadoViewModel();
            int idsubmoda;
            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();


            //SubModalidad
            if (PeriodoAcademicoId != null)
            {
                idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId && x.SubModalidad.IdModalidad==ModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                idsubmoda = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId).IdSubModalidadPeriodoAcademico;
            }


            //Modalidad



            viewModel.CargarDatos(CargarDatosContext(), p, CadenaBuscar, idsubmoda, ModalidadId, EscuelaId);

            return View(viewModel);
        }


        public ActionResult UnsubscribeAlumnoMatriculado(Int32? IdAlumnoMatriculado)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    var alumnoMatriculado = Context.AlumnoMatriculado.FirstOrDefault(x => x.IdAlumnoMatriculado == IdAlumnoMatriculado);
                    alumnoMatriculado.EstadoMatricula = ConstantHelpers.ESTADO_MATRICULA.MATRICULADO == alumnoMatriculado.EstadoMatricula ? ConstantHelpers.ESTADO_MATRICULA.RETIRADO : ConstantHelpers.ESTADO_MATRICULA.MATRICULADO;

                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Success, ex.Message);
                throw;
            }

            return RedirectToAction("ListAlumnoMatriculado");
        }

        public ActionResult ListAlumno(Int32? p, String CadenaBuscar)
        {
            var viewModel = new ListAlumnoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, CadenaBuscar);
            return View(viewModel);
        }


        public ActionResult AddEditAlumno(Int32? IdAlumno)
        {
            var AddEditAlumnoViewModel = new AddEditAlumnoViewModel();
            AddEditAlumnoViewModel.CargarDatos(CargarDatosContext(), IdAlumno);
            return View(AddEditAlumnoViewModel);
        }

        [HttpPost]
        public ActionResult AddEditAlumno(AddEditAlumnoViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdAlumno);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }
                using (var transaction = new TransactionScope())
                {

                    var alumno = new Alumno();

                    if (model.IdAlumno.HasValue)
                    {
                        alumno = Context.Alumno.First(x => x.IdAlumno == model.IdAlumno);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Alumno.Add(alumno);
                    }

                    alumno.Codigo = model.Codigo;
                    alumno.Nombres = model.Nombres.ToUpper();
                    alumno.CorreoElectronico = model.CorreoElectronico;
                    alumno.AnioEgreso = model.AnioEgreso;
                    alumno.AnioTitulacion = model.AnioTitulacion;
                    alumno.CiclosMatriculados = model.CiclosMatriculados;
                    alumno.Apellidos = model.Apellidos.ToUpper();

                    Context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAlumnoMatriculado");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error, ex.Message);
                model.CargarDatos(CargarDatosContext(), model.IdAlumno);
                TryUpdateModel(model);
                return View(model);
            }
        }
    }
}