﻿#region Import
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.Acreditation;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
#endregion

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class AcreditationController : BaseController
    {

        #region Acreditadora
        // GET: Admin/Committee
        public ActionResult ListAcreditadora(Int32? p)
        {
            var viewModel = new ListAcreditadoraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p);
            return View(viewModel);
        }

        public ActionResult AddEditAcreditadora(Int32? IdAcreditadora)
        {
            var viewModel = new AddEditAcreditadoraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdAcreditadora);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditAcreditadora(AddEditAcreditadoraViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdAcreditadora);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }

                using (var TransactionScope = new TransactionScope())
                {
                    var acreditadora = new Acreditadora();

                    if (model.IdAcreditadora.HasValue)
                    {
                        acreditadora = Context.Acreditadora.First(x => x.IdAcreditadora == model.IdAcreditadora);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Acreditadora.Add(acreditadora);
                    }

                    acreditadora.Nombre = model.Nombre;
                    acreditadora.NombreIndicador = model.NombreIndicador;
                    acreditadora.Descripcion = model.Descripcion;

                    if (model.RutaImagen != null && model.RutaImagen.ContentLength != 0)
                    {
                        var fileName = Guid.NewGuid().ToString().Substring(0, 6) + "_" + Path.GetFileName(model.RutaImagen.FileName);
                        var path = Server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var image = System.Drawing.Image.FromStream(model.RutaImagen.InputStream);
                        image = image.GetThumbnailImage(320, (320 * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                        image.Save(Path.Combine(path, fileName));
                        acreditadora.RutaImagen = fileName;
                    }

                    Context.SaveChanges();

                    TransactionScope.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAcreditadora");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdAcreditadora);
                TryUpdateModel(model);
                return View(model);
            }
        }

        public ActionResult ListAcreditadoraPeriodoAcademico(Int32? p, Int32 IdAcreditadora, Int32? IdPeriodoAcademico)
        {
            var viewModel = new ListAcreditadoraPeriodoAcademicoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, IdAcreditadora, IdPeriodoAcademico);
            return View(viewModel);
        }

        public ActionResult AddEditAcreditadoraPeriodoAcademico(Int32? IdAcreditadoraPeriodoAcademico, Int32 IdAcreditadora)
        {
            var viewModel = new AddEditAcreditadoraPeriodoAcademicoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdAcreditadoraPeriodoAcademico, IdAcreditadora);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditAcreditadoraPeriodoAcademico(AddEditAcreditadoraPeriodoAcademicoViewModel model)
        {
            

            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdAcreditadoraPreiodoAcademico, model.IdAcreditadora.Value);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }
                using (var transaction = new TransactionScope())
                {

                    var acreditadoraPeriodoAcademico = new AcreditadoraPeriodoAcademico();

                    if (model.IdAcreditadoraPreiodoAcademico.HasValue)
                    {
                        acreditadoraPeriodoAcademico = Context.AcreditadoraPeriodoAcademico.First(x => x.IdAcreditadoraPreiodoAcademico == model.IdAcreditadoraPreiodoAcademico);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.AcreditadoraPeriodoAcademico.Add(acreditadoraPeriodoAcademico);
                    }
                    var submodalidaPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                    acreditadoraPeriodoAcademico.IdAcreditadora = model.IdAcreditadora;
                    acreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico = submodalidaPeriodoAcademico;

                    Context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAcreditadoraPeriodoAcademico", new { IdAcreditadora = model.IdAcreditadora });
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdAcreditadoraPreiodoAcademico, model.IdAcreditadora.Value);
                TryUpdateModel(model);
                return View(model);
            }
        }
        #endregion

        public ActionResult AddEditComision(Int32? IdComision, Int32 IdAcreditadoraPeriodoAcademico, Int32 IdAcreditadora)
        {
            var viewModel = new AddEditComisionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdComision, IdAcreditadoraPeriodoAcademico, IdAcreditadora);
            return View(viewModel);        
        }

        [HttpPost]
        public ActionResult AddEditComision(AddEditComisionViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdComision, model.IdAcreditadoraPeriodoAcademico, model.IdAcreditadora);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error);
                    return View(model);
                }

                using (var transaction = new TransactionScope())
                {
                    var comision = new Comision();

                    if (model.IdComision.HasValue)
                    {
                        comision = Context.Comision.First(x => x.IdComision == model.IdComision);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Comision.Add(comision);
                    }

                    comision.Codigo = model.Codigo;
                    comision.NombreEspanol = model.NombreEspanol;
                    comision.NombreIngles = model.NombreIngles;
                    comision.IdAcreditadoraPeriodoAcademico = model.IdAcreditadoraPeriodoAcademico;

                    Context.SaveChanges();
                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAcreditadoraPeriodoAcademico", new { IdAcreditadora = model.IdAcreditadora });
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdComision, model.IdAcreditadoraPeriodoAcademico, model.IdAcreditadora);
                TryUpdateModel(model);
                return View(model);
            }
        }


        public ActionResult AddEditOutcomeComision(Int32? IdOutcomeComision, Int32 IdAcreditadora)
        {
            var viewModel = new AddEditOutcomeComisionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdOutcomeComision, IdAcreditadora);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditOutcomeComision(AddEditOutcomeComisionViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {

                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdOutcomeComision, model.IdAcreditadora);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                        return View(model);
                    }

                    var outcomeComision  = new OutcomeComision();

                    if (model.IdOutcomeComision.HasValue)
                    {
                        outcomeComision = Context.OutcomeComision.First(x => x.IdOutcomeComision == model.IdOutcomeComision);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.OutcomeComision.Add(outcomeComision);
                    }

                    var submodalidaPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                    outcomeComision.IdOutcome = model.IdOutcome;
                    outcomeComision.IdComision = model.IdComision;
                    outcomeComision.IdSubModalidadPeriodoAcademico = submodalidaPeriodoAcademico;

                    Context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListAcreditadoraPeriodoAcademico", new { IdAcreditadora = model.IdPeriodoAcademico });
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdOutcomeComision, model.IdAcreditadora);
                TryUpdateModel(model);
                return View(model);
            }
        }
    }
}