﻿    #region Inport
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.AcademicPeriod;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
#endregion

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class AcademicPeriodController : BaseController
    {
        protected AdminLogic adminLogic;

        public AcademicPeriodController()
        {
            adminLogic = new AdminLogic(CargarDatosContext().context);
        }

        // GET: Admin/AcademicPeriodC
        //[MenuSection("PeriodoAcademico","ListaPeriodoAcademico")]
        public ActionResult AddEditPeriodoAcademico(int? IdPeriodoAcademico)
        {
            var viewModel = new AddEditPeriodoAcademico();
            viewModel.CargarDatos(CargarDatosContext(), IdPeriodoAcademico);
            viewModel.Modal = GetModelSelectList();
            viewModel.SubModalitiesListUrl = GetAdminSubModalitiesListUrl;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditPeriodoAcademico(int? IdModalidad, int? IdPeriodoAcademico, string CicloAcademico, DateTime FechaInicioPeriodo, DateTime FechaFinPeriodo, string Semanas, bool EsMallaArizona)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (IdPeriodoAcademico.HasValue && IdPeriodoAcademico.Value > 0 && CicloAcademico != null)
                    {
                        var periodoAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico);
                        var subModalidadPeriodoAcademico = periodoAcademico.SubModalidadPeriodoAcademico.FirstOrDefault();
                        periodoAcademico.FechaInicioPeriodo = FechaInicioPeriodo;
                        periodoAcademico.FechaFinPeriodo = FechaFinPeriodo;
                        periodoAcademico.CicloAcademico = CicloAcademico;
                        periodoAcademico.Semanas = int.Parse(Semanas);
                        subModalidadPeriodoAcademico.EsMallaArizona = EsMallaArizona;
                        Context.SaveChanges();
                        transaction.Complete();
                        PostMessage(MessageType.Success);
                    }
                    else
                    {
                        if (IdModalidad.HasValue && CicloAcademico != null)
                        {
                            int idsubmoda = Context.SubModalidad.Where(x => x.IdModalidad == IdModalidad && x.NombreEspanol == "Regular").FirstOrDefault().IdSubModalidad;

                            var lstPeriodoAcademicoinsubmoda = (from submodapa in Context.SubModalidadPeriodoAcademico
                                                                join pa in Context.PeriodoAcademico on submodapa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                                where (submodapa.IdSubModalidad == idsubmoda)
                                                                select new 
                                                                {
                                                                    pa.IdPeriodoAcademico,
                                                                    pa.CicloAcademico,
                                                                    pa.FechaInicioPeriodo,
                                                                    pa.FechaFinPeriodo,
                                                                    pa.Estado
                                                                }).ToList();



                            var periodoAcademico = lstPeriodoAcademicoinsubmoda.FirstOrDefault(x => x.CicloAcademico == CicloAcademico);

                            if (periodoAcademico == null)
                            {
                                PeriodoAcademico pa = new PeriodoAcademico
                                {
                                    CicloAcademico = CicloAcademico,
                                    FechaInicioPeriodo = FechaInicioPeriodo,
                                    FechaFinPeriodo = FechaFinPeriodo,
                                    Estado = ConstantHelpers.ESTADO.INACTIVO,
                                    Semanas = int.Parse(Semanas),
                                };
                                Context.PeriodoAcademico.Add(pa);
                                Context.SaveChanges();

                                int periodoUltimoCreado = Context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).First().IdPeriodoAcademico;
                                var IdSM = Context.SubModalidad.Where(x => x.IdModalidad == IdModalidad && x.NombreEspanol == "Regular").FirstOrDefault().IdSubModalidad;

                                SubModalidadPeriodoAcademico smpa = new SubModalidadPeriodoAcademico
                                {
                                    IdPeriodoAcademico = periodoUltimoCreado,
                                    EsMallaArizona = EsMallaArizona
                                };
                                smpa.IdSubModalidad = IdSM;
                                Context.SubModalidadPeriodoAcademico.Add(smpa);
                                Context.SaveChanges();

                                if (IdModalidad == int.Parse(ConstantHelpers.MODALITY.PREGRADO_REGULAR))
                                {
                                    Modulo modulo = new Modulo
                                    {
                                        NombreEspanol = "Módulo Regular",
                                        NombreIngles = "Regular Module",
                                        FechaInicio = FechaInicioPeriodo,
                                        FechaFin = FechaFinPeriodo,
                                        IdentificadorSeccion = "R",
                                        Estado = ConstantHelpers.ESTADO.ACTIVO,
                                        Semanas = int.Parse(Semanas),
                                    };
                                    Context.Modulo.Add(modulo);
                                    Context.SaveChanges();

                                    SubModalidadPeriodoAcademicoModulo submodamodu = new SubModalidadPeriodoAcademicoModulo
                                    {
                                        IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.OrderByDescending(x => x.IdSubModalidadPeriodoAcademico).First().IdSubModalidadPeriodoAcademico,
                                        IdModulo = Context.Modulo.OrderByDescending(x => x.IdModulo).First().IdModulo
                                    };
                                    Context.SubModalidadPeriodoAcademicoModulo.Add(submodamodu);
                                    Context.SaveChanges();
                                    transaction.Complete();

                                }
                                else
                                {
                                    transaction.Complete();
                                }

                                PostMessage(MessageType.Success);

                            }
                            else
                            {
                                PostMessage(MessageType.Error);
                            }
                        }
                    }
                    
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var errors in ex.EntityValidationErrors)
                    foreach (var error in errors.ValidationErrors)
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
           
            }
            return RedirectToAction("AddEditPeriodoAcademico");
        }

        [HttpPost]
        public ActionResult GuardarModulo(string NombreModulo , DateTime FechaInicioModulo , DateTime FechaFinModulo , string IdentificadorSeccion , string CicloAcademico , DateTime FechaInicioPeriodo , DateTime FechaFinPeriodo, string SemanasModulo)
        {
            try
            {
                //Creacion del módulo
                Modulo modulo = new Modulo
                {
                    NombreEspanol = NombreModulo,
                    NombreIngles = "",
                    FechaInicio = FechaInicioModulo,
                    FechaFin = FechaFinModulo,
                    Estado = ConstantHelpers.ESTADO.ACTIVO,
                    IdentificadorSeccion = IdentificadorSeccion,
                    Semanas = int.Parse(SemanasModulo),
                };
                Context.Modulo.Add(modulo);
                Context.SaveChanges();

                var IdmoduloObtenido = Context.Modulo.OrderByDescending(x => x.IdModulo).First().IdModulo;

                var periodoAcademicoObtenido = Context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).First();


                var submodalidadPeriodoAcademicoObtenido = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == periodoAcademicoObtenido.IdPeriodoAcademico);

                SubModalidadPeriodoAcademicoModulo SMPAM = new SubModalidadPeriodoAcademicoModulo
                {
                    IdSubModalidadPeriodoAcademico = submodalidadPeriodoAcademicoObtenido.IdSubModalidadPeriodoAcademico,
                    IdModulo = IdmoduloObtenido
                };
                Context.SubModalidadPeriodoAcademicoModulo.Add(SMPAM);
                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var errors in ex.EntityValidationErrors)
                    foreach (var error in errors.ValidationErrors)
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);

            }
            return RedirectToAction("AddEditPeriodoAcademico");

        }

        public ActionResult EnablePeriodoAcademico(Int32 IdPeriodoAcademico)
        {
           
            try
            {
                using (var transaction = new TransactionScope())
                {
                    AbetEntities z = new AbetEntities();

                    int idSubModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidad;
                    int idModalidad = Context.SubModalidad.Where(x => x.IdSubModalidad == idSubModalidad).FirstOrDefault().IdModalidad;


                    // var lstPeriodoAcademico = context.PeriodoAcademico.ToList();

                    var lstSubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidad == idSubModalidad).ToList();

                    //var lstperiodoacademico = (from a in z.SubModalidadPeriodoAcademico
                    //                           join b in z.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                    //                           where (a.IdSubModalidad == idSubModalidad)
                    //                           select new PeriodoAcademico
                    //                           {
                    //                               IdPeriodoAcademico=a.IdPeriodoAcademico,
                    //                               Estado= b.Estado
                    //                           }
                    //    ).ToList();

                    foreach (var preriodoAcademico in lstSubmoda)
                        preriodoAcademico.PeriodoAcademico.Estado = ConstantHelpers.ESTADO.INACTIVO;


                    //foreach (var preriodoAcademico in lstPeriodoAcademico)                      
                    //    preriodoAcademico.Estado = ConstantHelpers.ESTADO.INACTIVO;

                    //foreach (var preriodoAcademico in lstperiodoacademico)
                    //    preriodoAcademico.Estado = ConstantHelpers.ESTADO.INACTIVO;


                    var periodoAcademico = Context.PeriodoAcademico.Find(IdPeriodoAcademico);
                    periodoAcademico.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    Context.SaveChanges();
                    transaction.Complete();

                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("ListPeriodoAcademico");
        }

        public ActionResult EnableSubModalidadPeriodoAcademico(Int32 IdSubModalidadPeriodoAcademico)
        {
            //RECIBE ID PERIODO ACADEMICO
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            try
            {
                using (var transaction = new TransactionScope())
                {
                    var lstSubmodalidadperiodoacademico = Context.SubModalidadPeriodoAcademico.ToList();

                    foreach (var submodalidadperiodoacademico in lstSubmodalidadperiodoacademico)
                        submodalidadperiodoacademico.PeriodoAcademico.Estado = ConstantHelpers.ESTADO.INACTIVO;

                    var submodaperiodo = Context.SubModalidadPeriodoAcademico.Find(idsubmoda);
                    submodaperiodo.PeriodoAcademico.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("ListSubModalidadPeriodoAcademico");
        }

        //public ActionResult ListPeriodoAcademico(Int32? p, String CadenaBuscar)
        //{
        //    var viewModel = new ListPeriodoAcademicoViewModel();
        //    viewModel.CargarDatos(CargarDatosContext(), p);
        //    return View(viewModel);
        //}
        public ActionResult ListSubModalidadPeriodoAcademico(Int32? p, String CadenaBuscar, Int32? m)
        {
            var viewModel = new ListPeriodoAcademicoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, m);
            return View(viewModel);
        }


        public ActionResult ListPeriodoAcademico(Int32? p, Int32? m,bool? Creado)
        {
			int parModalidadId = Session.GetModalidadId();

            var ListPeriodoAcademicoViewModel = new ListPeriodoAcademicoViewModel();
            ListPeriodoAcademicoViewModel.Modal = GetModelSelectList();
            ListPeriodoAcademicoViewModel.CargarDatos(CargarDatosContext(), p, parModalidadId);

			if (Creado == true)
			{
				PostMessage(MessageType.Success,"Periodo Creado Satisfactoriamente");
				return View(ListPeriodoAcademicoViewModel);
			}
			else
			{
				return View(ListPeriodoAcademicoViewModel);
			}


		}
       
        public JsonResult TraerSubModalidad(int id)
        {
            var lstSubModalidad = from sm in Context.SubModalidad
                                  where sm.IdModalidad == id
                                  select new
                                  {
                                      IdSubModalidad = sm.IdSubModalidad,
                                      NombreEspanol = sm.NombreEspanol
                                  };
                //context.SubModalidad.Where(x => x.IdModalidad == id).ToList();
            //var listSubModalidad = new SelectList(lstSubModalidad, "IdSubModalidad", "NombreEspanol");
            return Json(lstSubModalidad, JsonRequestBehavior.AllowGet);
        }

        protected IEnumerable<SelectListItem> GetModelSelectList()
        {
            var result = new List<SelectListItem>();
            PopulateWithModel(result);
            return result;
        }

        private void PopulateWithModel(List<SelectListItem> list)
        {
            AddNewDefaultItem(list);
            foreach (var item in adminLogic.GetModalType())
            {
                AddNewItem(list, item.NombreEspanol, item.IdModalidad.ToSafeString());
            }
        }

        protected void AddNewDefaultItem(List<SelectListItem> list)
        {
            AddNewItem(list, "-- " + LayoutResource.Seleccione + " --", "0");
        }

        protected void AddNewItem(List<SelectListItem> list, string text, string value)
        {
            list.Add(new SelectListItem
            {
                Text = text,
                Value = value
            });
        }

    }
}