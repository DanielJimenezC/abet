﻿#region Import
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models.Areas.Admin.Entities;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
#endregion

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class JsonController : BaseController
    {
        // GET: Admin/Json
        #region Usuarios

        public JsonResult GetModules(Int32 IdPeriodoAcademico)
        {

            var data = (from e in Context.PeriodoAcademico
                               join f in Context.SubModalidadPeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                               join g in Context.SubModalidadPeriodoAcademicoModulo on f.IdSubModalidadPeriodoAcademico equals g.IdSubModalidadPeriodoAcademico
                               join h in Context.Modulo on g.IdModulo equals h.IdModulo
                               where e.IdPeriodoAcademico == IdPeriodoAcademico
                               select new
                               {
                                   Nombre = h.NombreEspanol,
                                   h.FechaInicio,
                                   h.FechaFin,
                                   Identificador = h.IdentificadorSeccion,
                                   h.Semanas,
                               }).OrderByDescending(x=>x.Nombre).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocentes(String cadenaBuscar)
        {
            var query = Context.Docente.AsQueryable();
            if (!String.IsNullOrEmpty(cadenaBuscar))
                foreach (var token in cadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));

            var data = query.Select(x => new { id = x.IdDocente, text = x.Codigo + " - " + x.Nombres + " " + x.Apellidos }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocente(Int32 IdDocente)
        {
            var docente = Context.Docente.Find(IdDocente);
            var data = new DocenteEntity()
            {
                Codigo = docente.Codigo,
                Apellidos = docente.Apellidos,
                Nombres = docente.Nombres
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUsuariosSecundarios(String cadenaBuscar)
        {
            var query = Context.Usuario.Where(x => x.Docente.EsAdministrativo.HasValue && !x.Docente.EsAdministrativo.Value && !x.IdUsuarioSecundario.HasValue && x.IdDocente.HasValue).AsQueryable();
            if (!String.IsNullOrEmpty(cadenaBuscar))
                foreach (var token in cadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));

            var data = query.Select(x => new { id = x.IdUsuario, text = x.Codigo + " - " + x.Nombres + " " + x.Apellidos }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Articulacion
        public JsonResult GetCarrerasPeriodoAcademico(Int32 IdSubModalidadPeridoAcademico)
        {
            var data = Context.CarreraPeriodoAcademico.Include(x => x.Carrera).Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeridoAcademico).Select(x => new { Value = x.IdCarrera, Text = currentCulture == ConstantHelpers.CULTURE.INGLES ? x.Carrera.NombreIngles : x.Carrera.NombreEspanol }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAcreditadoraPeriodoAcademico(Int32 IdSubModalidadPeridoAcademico)
        {
            var data = Context.AcreditadoraPeriodoAcademico.Include(x => x.Acreditadora).Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeridoAcademico).Select(x => new { Value = x.IdAcreditadora, Text = x.Acreditadora.Nombre }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetComisiones(Int32 AcreditadoraId, Int32 CarreraId, Int32 IdSubModalidadPeridoAcademico)
        {
            var data = Context.CarreraComision.Include(x => x.Comision).Include(x => x.Comision.AcreditadoraPeriodoAcademico).Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeridoAcademico && x.IdCarrera == CarreraId && x.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora == AcreditadoraId).Select(x => new { Value = x.IdComision, Text = currentCulture == ConstantHelpers.CULTURE.INGLES ? x.Comision.NombreIngles : x.Comision.NombreEspanol }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarreraComision(Int32 AcreditadoraId, Int32 CarreraId, Int32 IdSubModalidadPeridoAcademico)
        {
            var data = Context.CarreraComision.Include(x => x.Comision).Include(x => x.Comision.AcreditadoraPeriodoAcademico).Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeridoAcademico && x.IdCarrera == CarreraId && x.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora == AcreditadoraId).Select(x => new { Value = x.IdCarreraComision, Text = currentCulture == ConstantHelpers.CULTURE.INGLES ? x.Comision.NombreIngles : x.Comision.NombreEspanol }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}