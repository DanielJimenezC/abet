﻿using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    public class OrganizationChartController : Controller
    {

        [AuthorizeUser(AppRol.Administrador, AppRol.AdministradorEscuela, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Index()
        {
            return View();
        }

        // editar solo pueden los Admin
        // [AuthorizeUser(AppRol.Administrador, AppRol.AdministradorEscuela)]
    }
}