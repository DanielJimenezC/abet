﻿#region Inmport
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.User;
using UPC.CA.ABET.Presentation.Areas.Admin.ViewModels.User;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
#endregion

namespace UPC.CA.ABET.Presentation.Areas.Admin.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador)]
    public class UserController : BaseController
    {
        protected AdminLogic adminLogic;

        public UserController()
        {
            adminLogic = new AdminLogic(CargarDatosContext().context);
        }

        public ActionResult ListUsuario(Int32? p, String CadenaBuscar)
        {
            var viewModel = new ListUsuarioViewModel();
            viewModel.CargarDatos(CargarDatosContext(), p, CadenaBuscar);
            return View(viewModel);
        }

        public ActionResult AddEditUsuario(Int32? IdUsuario)
        {
            var viewModel = new AddEditUsuarioViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdUsuario);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditUsuario(AddEditUsuarioViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                    return View(model);
                }

                using (var transaction = new TransactionScope())
                {
                    var fechaActual = DateTime.Now;
                    var usuario = new Usuario();

                    if (model.IdUsuario.HasValue)
                    {
                        usuario = Context.Usuario.First(x => x.IdUsuario == model.IdUsuario);
                    }
                    else
                    {
                        Usuario usuarioExistente = null;
                        if (model.IdDocente.HasValue)
                            usuarioExistente = Context.Usuario.FirstOrDefault(x => x.IdDocente == model.IdDocente || x.Codigo.ToLower() == model.Codigo.ToLower());
                        else
                            usuarioExistente = Context.Usuario.FirstOrDefault(x => x.Codigo.ToLower() == model.Codigo.ToLower());

                        if (usuarioExistente != null)
                        {
                            model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                            TryUpdateModel(model);
                            PostMessage(MessageType.Warning, UserResource.UsuarioRepetido);
                            return View(model);
                        }

                        usuario.IdDocente = model.IdDocente;
                        usuario.FechaCreacion = fechaActual;
                        //Establecer las variables por defecto
                        usuario.Estado = ConstantHelpers.ESTADO.ACTIVO;
                        Context.Usuario.Add(usuario);
                    }

                    var security = new SecurityLogic();

                    if (!String.IsNullOrEmpty(model.Password) && usuario != null)
                    {
                        usuario.Password = security.MD5Hash(model.Password);
                        PostMessage(MessageType.Info, MessageResource.ContrasenaModificadaCorrectamente);
                    }
                    else if (usuario == null)
                    {
                        usuario.Password = security.MD5Hash(model.Password);
                    }

                    usuario.Codigo = model.Codigo.Trim().ToLower();
                    usuario.Nombres = model.Nombres;
                    usuario.Apellidos = model.Apellidos;
                    usuario.Correo = model.Correo;
                    usuario.FechaAlta = fechaActual;

                    if (usuario.IdDocente.HasValue && usuario.Docente != null)
                    {
                        usuario.Docente.Codigo = usuario.Codigo;
                    }
                    usuario.IdEscuelaDefecto = model.IdEscuelaDefecto;


                    if (model.Imagen != null && model.Imagen.ContentLength != 0)
                    {
                        var fileName = Guid.NewGuid().ToString().Substring(0, 6) + "_" + Path.GetFileName(model.Imagen.FileName);
                        var path = Server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var image = System.Drawing.Image.FromStream(model.Imagen.InputStream);
                        image = image.GetThumbnailImage(320, (320 * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                        image.Save(Path.Combine(path, fileName));
                        usuario.Imagen = fileName;
                    }


                    if (model.ImagenFirma != null && model.ImagenFirma.ContentLength != 0)
                    {                       
                        var fileName = Guid.NewGuid().ToString().Substring(0, 6) + "_" + Path.GetFileName(model.ImagenFirma.FileName);
                        var path = Server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var image = System.Drawing.Image.FromStream(model.ImagenFirma.InputStream);
                        image = image.GetThumbnailImage(320, (320 * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                        image.Save(Path.Combine(path, fileName));
                        usuario.Firma = fileName;
                    }

                    

                    //usuario.Imagen = model.Imagen.SaveImageToServer(Server);
                    //usuario.Firma = model.ImagenFirma.SaveImageToServer(Server);

                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                    return RedirectToAction("ListRolUsuario", new { IdUsuario = usuario.IdUsuario });
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                TryUpdateModel(model);
                return View(model);
            }
        }

        public ActionResult DisableUsuario(Int32 IdUsuario)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    var usuario = Context.Usuario.Find(IdUsuario);
                    usuario.Estado = usuario.Estado == ConstantHelpers.ESTADO.ACTIVO ? ConstantHelpers.ESTADO.INACTIVO : ConstantHelpers.ESTADO.ACTIVO;
                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("ListUsuario");
        }

        public ActionResult ListRol(Int32? p)
        {
            var ListRolViewModel = new ListRolViewModel();
            ListRolViewModel.CargarDatos(CargarDatosContext(), p);
            return View(ListRolViewModel);
        }

        public ActionResult AddEditRol(Int32? IdRol)
        {
            var viewModel = new AddEditRolViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdRol);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditRol(AddEditRolViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdRol);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                        return View(model);
                    }

                    var rol = new Rol();

                    if (model.IdRol.HasValue)
                    {
                        rol = Context.Rol.First(x => x.IdRol == model.IdRol);
                    }
                    else
                    {
                        //Establecer las variables por defecto
                        Context.Rol.Add(rol);
                    }

                    rol.Descripcion = model.Descripcion;
                    rol.Acronimo = model.Acronimo;
                    rol.Estado = model.Estado;

                    Context.SaveChanges();

                    transaction.Complete();

                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdRol);
                TryUpdateModel(model);
                return View(model);
            }
            return RedirectToAction("ListRol");
        }

        public ActionResult ListRolUsuario(Int32 IdUsuario)
        {
            var viewModel = new ListRolUsuarioViewModel();
            viewModel.Modal = GetModelSelectList();
            viewModel.CargarDatos(CargarDatosContext(), IdUsuario);
            return View(viewModel);
        }

        /// <summary>
        /// Guarda la la matriz de Roles por usuario
        /// </summary>
        /// <param name="model">objeto que contiene los datos del usuario</param>
        /// <param name="form">Formulario como Dicccionario</param>
        /// <returns>respuesta de confirmacion o negacion del la transsaccion</returns>
        [HttpPost]
        public ActionResult ListRolUsuario(ListRolUsuarioViewModel model, FormCollection form)
            {
            if (!ModelState.IsValid)
            {
                model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                TryUpdateModel(model);
                PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                return View(model);
            }
            try
            {
                var idUsuario = model.IdUsuario;
                var lstRolUsuario = Context.RolUsuario.Where(x => x.IdRolUsuario == model.IdUsuario).ToList();
                var rolesUsuarioKey = form.AllKeys.Where(x => x.StartsWith("chk-"));
                var fechaActual = DateTime.Now;
                System.Diagnostics.Debug.WriteLine("NUEVA SOLICITUD");

                //int PeriodoAcademicoActual = context.PeriodoAcademico.Where(x => x.Estado == "ACT").Select(x => x.IdPeriodoAcademico).FirstOrDefault();
                var LstPeriodoAcademicoActual = (from smpa in Context.SubModalidadPeriodoAcademico
                                                 join PA in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals PA.IdPeriodoAcademico
                                                 where PA.Estado == ConstantHelpers.ESTADO.ACTIVO
                                                 select new
                                                 {
                                                     CicloAcademico = PA.CicloAcademico,
                                                     IdSubModalidadPeriodoAcademico1 = smpa.IdSubModalidadPeriodoAcademico,
                                                     IdPeriodoAcademico = PA.IdPeriodoAcademico
                                                 }).AsEnumerable();



                ////List<RolUsuario> rolUsuarioAnteriores = context.RolUsuario.Include("SubModalidadPeriodoAcademico").Where(x=> x.IdUsuario == idUsuario).ToList();
                var rolUsuarioAnteriores = (from ru in Context.RolUsuario
                                            join lst in LstPeriodoAcademicoActual on ru.IdSubModalidadPeriodoAcademico equals lst.IdSubModalidadPeriodoAcademico1
                                            where ru.IdUsuario == idUsuario
                                            select new
                                            {
                                                IdRolUsuario = ru.IdRolUsuario,
                                                IdRol = ru.IdRol,
                                                IdUsuario = ru.IdUsuario,
                                                FechaCreacion = ru.FechaCreacion,
                                                Estado = ru.Estado,
                                                IdEscuela = ru.IdEscuela,
                                                IdSubModalidadPeriodoAcademico = ru.IdSubModalidadPeriodoAcademico
                                            }).ToList();

                if (rolUsuarioAnteriores != null)
                {
                    foreach (var item in rolUsuarioAnteriores)
                    {
                        var RUAitem = Context.RolUsuario.Find(item.IdRolUsuario);
                        Context.RolUsuario.Remove(RUAitem);
                    }
                }

            

                //    //foreach (RolUsuario item in rolUsuarioAnteriores)
                //    //{
                //    //    context.RolUsuario.Remove(item);
                //    //}

            //    context.SaveChanges();
            //}





            using (var transaction = new TransactionScope())
                {
                    foreach (var rolUsuarioKey in rolesUsuarioKey)
                    {
                        System.Diagnostics.Debug.WriteLine(rolUsuarioKey);

                        var value = form[rolUsuarioKey.ToString()] == "on" || form[rolUsuarioKey.ToString()] == "true" ? true : false;

                        var rolId = rolUsuarioKey.Split('-')[3].ToInteger();
                        var idSubModalidadPeriodoAcademico = rolUsuarioKey.Split('-')[2].ToInteger();
                        var rolUsuarioId = rolUsuarioKey.Split('-')[4].ToInteger();
                        var escuelaId = rolUsuarioKey.Split('-')[1].ToInteger();

                        //    System.Diagnostics.Debug.WriteLine("RolID: "+ rolId+", periodo: "+periodoAcademicoId+", rolUsuario: "+rolUsuarioId+", escuela: "+escuelaId);

                        var rolUsuario = Context.RolUsuario.FirstOrDefault(x => x.IdRolUsuario == rolUsuarioId);
                         
                        

                        if (rolUsuario == null)
                        {
                            rolUsuario = new RolUsuario();
                            rolUsuario.IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
                            rolUsuario.IdRol = rolId;
                            rolUsuario.IdUsuario = idUsuario;
                            rolUsuario.FechaCreacion = fechaActual;
                            rolUsuario.IdEscuela = escuelaId;
                            rolUsuario.Estado = "ACT";

                          
                            Context.RolUsuario.Add(rolUsuario);
                        }
                        else
                        {
                            rolUsuario = new RolUsuario();
                            rolUsuario.IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
                            rolUsuario.IdRol = rolId;
                            rolUsuario.IdUsuario = idUsuario;
                            rolUsuario.FechaCreacion = fechaActual;
                            rolUsuario.IdEscuela = escuelaId;
                            rolUsuario.Estado = "ACT";
                            Context.RolUsuario.Add(rolUsuario);
                        }
                       





                    }


                    PostMessage(MessageType.Success);
                    //context.Database.Log = x => System.Diagnostics.Debug.WriteLine("QUERY: " + x);
                    Context.SaveChanges();
                    transaction.Complete();
                }

            }
            catch (Exception ex)
            {
                model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                TryUpdateModel(model);
               
                System.Diagnostics.Debug.WriteLine("EXCEPTION: "+ ex.InnerException);
                PostMessage(MessageType.Error, ex.Message);
                return View(model);
                throw;
            }
            return RedirectToAction("ListUsuario");
        }


        public ActionResult DeleteUsuarioSecundario(Int32 IdUsuario)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    var usuario = Context.Usuario.Find(IdUsuario);
                    var usuarioSecundario = Context.Usuario.Find(usuario.IdUsuarioSecundario);

                    usuario.IdUsuarioSecundario = null;
                    usuarioSecundario.IdUsuarioSecundario = null;

                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }


        public ActionResult _AddUsuarioSecundario(Int32 IdUsuario)
        {
            var viewModel = new _AddUsuarioSecundarioViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdUsuario);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult _AddUsuarioSecundario(_AddUsuarioSecundarioViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                    TryUpdateModel(model);
                    PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                    return View(model);
                }

                using (var transaction = new TransactionScope())
                {
                    var usuario = Context.Usuario.Find(model.IdUsuario);
                    var usuarioSecundario = Context.Usuario.Find(model.IdUsuarioSecundario);

                    usuario.IdUsuarioSecundario = model.IdUsuarioSecundario;
                    usuarioSecundario.IdUsuarioSecundario = model.IdUsuario;

                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdUsuario);
                TryUpdateModel(model);
                return View(model);
            }
            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }

        public ActionResult _AddEditRolUsuario(Int32? IdRolUsuario, Int32 IdUsuario)
        {
            var viewModel = new _AddEditRolUsuarioViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdRolUsuario, IdUsuario);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult _AddEditRolUsuario(_AddEditRolUsuarioViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.CargarDatos(CargarDatosContext(), model.IdRolUsuario, model.IdUsuario);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error, ErrorResource.DatosIncorrectos);
                        return View(model);
                    }

                    var rolUsuario = new RolUsuario();

                    if (model.IdRolUsuario.HasValue)
                    {
                        rolUsuario = Context.RolUsuario.First(x => x.IdRolUsuario == model.IdRolUsuario);
                    }
                    else
                    {
                        rolUsuario.FechaCreacion = DateTime.Now;
                        //Establecer las variables por defecto
                        Context.RolUsuario.Add(rolUsuario);
                    }

                    //smpa  
                    int idsmpa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico.Value).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                    rolUsuario.IdRol = model.IdRol;
                    rolUsuario.IdUsuario = model.IdUsuario;
                    rolUsuario.IdSubModalidadPeriodoAcademico = idsmpa;

                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.CargarDatos(CargarDatosContext(), model.IdRolUsuario, model.IdUsuario);
                TryUpdateModel(model);
                return View(model);
            }
            return RedirectToAction("ListRolUsuario");
        }

        protected IEnumerable<SelectListItem> GetModelSelectList()
        {
            var result = new List<SelectListItem>();
            PopulateWithModel(result);
            return result;
        }

        private void PopulateWithModel(List<SelectListItem> list)
        {
            AddNewDefaultItem(list);
            foreach (var item in adminLogic.GetModalType())
            {
                AddNewItem(list, item.NombreEspanol, item.IdModalidad.ToSafeString());
            }
        }

        protected void AddNewDefaultItem(List<SelectListItem> list)
        {
            AddNewItem(list, "-- "+LayoutResource.Seleccione+" --", "0");
        }

        protected void AddNewItem(List<SelectListItem> list, string text, string value)
        {
            list.Add(new SelectListItem
            {
                Text = text,
                Value = value
            });
        }
    }
}