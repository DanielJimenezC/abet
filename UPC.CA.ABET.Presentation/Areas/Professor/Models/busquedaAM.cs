﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class busquedaAM
    {
        public int IdAccionMejora { get; set; }
        public string DescripcionEspanol { get; set; }
        public string DescripcionIngles { get; set; }
        public string Estado { get; set; }
        public string Codigo { get; set; }
        public int IdEscuela { get; set; }
        public string Anio { get; set; }
        public int ParaPlan { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public int IdInstrumento { get; set; }
        public int IdConstituyente { get; set; }
        public int IdModalidad { get; set; }
        public int? IdOutcomeComision { get; set; }
        public int? IdCurso { get; set; }
        public string Outcomenombre { get; set; }
    }
}

