﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class AccionMejoraFiltrado
    {
        public int? Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Constituyente { get; set; }
        public string Estado { get; set; }
        public bool ParaPlan { get; set; }
        public int? IdCurso { get; set; }
        public string Instrumento { get; set; }
        public string Comision { get; set; }
        public string Outcomes { get; set; }
        public string Carreras { get; set; }
        public int? IdPeriodoAcademico { get; set; }
        public string PeriodoAcademico { get; set; }
        public string EstadoPlanMejora { get; set; }
        public int EvidenciasListas { get; set; }
        public Boolean Criticidad { get; set; }
        public Boolean NoRelacionado { get; set; }
    }
}