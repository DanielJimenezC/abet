﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class HallazgosPlanResultados
    {
        public int IdHallazgo { get; set; }
        public int IdHallazgoAccionMejora { get; set; }
        public string Evidencia { get; set; }
        public string CodigoHallazgo { get; set; }
        public string DescripcionHallazgo { get; set; }
        public int? Identificador { get; set; }
        public string Instrumento { get; set; }
        public string Tipo { get; set; }

        public string HallazgoResultado_1 { get; set; }
        public string HallazgoPorcentaje_1 { get; set; }
        public string HallazgoColor_1 { get; set; }
        public string HallazgoResultado_2 { get; set; }
        public string HallazgoPorcentaje_2 { get; set; }
        public string HallazgoColor_2 { get; set; }
    }
}