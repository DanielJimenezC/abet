﻿
namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    using System;
    using System.Collections.Generic;
    public partial class CheckboxModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
        public string Codigo { get; set; }
        public bool Checked { get; set; }
    }
}

