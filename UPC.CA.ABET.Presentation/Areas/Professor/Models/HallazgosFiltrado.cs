﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class HallazgosFiltrado
    {
        //public int IdHallazgo { get; set; }
        //public string Codigo { get; set; }
        //public int? Identificador { get; set; }

        public string HallazgoDescripcion { get; set; }
        public string Porcentaje { get; set; }
        public string Color { get; set; }

        public string DesEvidencia { get; set; }
        //public string Porcentaje { get; set; }
        public string ColorNivelAceptacion { get; set; }
        //public string Cantidad { get; set; }
        //public string Total_Alumnos { get; set; }
    }
}