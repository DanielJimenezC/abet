﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ExportPlan;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using System.Data.SqlClient;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class ExportPlanModel : GenericModel
    {
        public ExportPlanModel(ReportBaseModel Model, ControllerBase Controller) : base(Model, Controller)
        {
            this.model.ReportCode = "PDR";
        }

        public void LoadDropDownListsByPlanOfResults(PlanOfResultsViewModel ViewModel)
        {
           

        }

        //public SelectList DropDownListCareerForModalityExport(int idModalidad)
        //{

        //    return model.DropDownListCareerForModalityExportWithID(idModalidad);
        //}

        public void LoadDropDownPlanOfResults(PlanOfResultsViewModel ViewModel)
        {
            

        }

        public void ExistDataPlanOfResults(PlanOfResultsViewModel ViewModel)
        {
           
           
            

        }
      
        private class ProcedureResult
        {
            public string IdIdioma { get; set; }
            public string CodComision { get; set; }
            public int IdCarrera { get; set; }
            public int Anio { get; set; }
            public int Modalidad { get; set; }
            public int IdCostituyente { get; set; }
            public int IdInstrumento { get; set; }
          
        }

    }

}