﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class EvidenciaForm
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Ruta { get; set; }
    }
}