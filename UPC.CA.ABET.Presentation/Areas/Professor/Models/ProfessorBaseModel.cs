﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class ProfessorBaseModel
    {
        public readonly LoadCombosLogic loadCombosLogic;
        public IEnumerable<ComboItem> modalities;

        public ProfessorBaseModel(AbetEntities Context)
        {
            this.loadCombosLogic = new LoadCombosLogic(Context);
        }
        public SelectList DropDownListModalities()
        {
            modalities = loadCombosLogic.ListModalities();
            return modalities.ToSelectList();
        }
    }
    public static class ProfessorBaseModelExtensions
    {
        public static SelectList ToSelectList(this IEnumerable<ComboItem> data)
        {
            return new SelectList(data, "Key", "Value");
        }
        public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new SelectList(dictionary, "Key", "Value");
        }
    }
}