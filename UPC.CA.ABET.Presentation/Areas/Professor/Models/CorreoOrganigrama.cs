﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class CorreoOrganigrama
    {
        public CorreoOrganigrama()
        {
            Areas = new List<AreaCorreoOrganigrama>();
        }
        public string Email { get; set; }
        public string NombreDirector { get; set; }
        public string NombreResponsable { get; set; }
        public List<AreaCorreoOrganigrama> Areas { get; set; }
    }

    public class AreaCorreoOrganigrama
    {
        public AreaCorreoOrganigrama() {
            SubAreas = new List<SubAreaCorreoOrganigrama>();
        }
        public string Email { get; set; }
        public string NombreArea { get; set; }
        public string NombreResponsable { get; set; }
        public List<SubAreaCorreoOrganigrama> SubAreas { get; set; }
    }

    public class SubAreaCorreoOrganigrama
    {
        public SubAreaCorreoOrganigrama()
        {
            Cursos = new List<CursoCorreoOrganigrama>();
        }
        public string Email { get; set; }
        public string NombreResponsable { get; set; }
        public string NombreSubArea { get; set; }
        public List<CursoCorreoOrganigrama> Cursos { get; set; }
    }

    public class CursoCorreoOrganigrama
    {
        public string Email { get; set; }
        public string NombreResponsable { get; set; }
        public string CodigoCurso { get; set; }
        public string NombreCurso { get; set; }
    }
}