﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Models
{
    public class PlanMejoraModel
    {
        public int IdPlanMejora { get; set; }
        public int? Anio { get; set; }
        public string Nombre { get; set; }
        public int? IdEscuela { get; set; }
        public string Estado { get; set; }
        public int IdModalidad { get; set; }
    }
}