﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.PlanMejora {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PlanMejoraResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PlanMejoraResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.PlanMejora.PlanMejoraRes" +
                            "ource", typeof(PlanMejoraResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Acciones de Mejora.
        /// </summary>
        public static string AccionesDeMejora {
            get {
                return ResourceManager.GetString("AccionesDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Year.
        /// </summary>
        public static string Anio {
            get {
                return ResourceManager.GetString("Anio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Career.
        /// </summary>
        public static string Carrera {
            get {
                return ResourceManager.GetString("Carrera", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Cycle.
        /// </summary>
        public static string Ciclo {
            get {
                return ResourceManager.GetString("Ciclo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Código.
        /// </summary>
        public static string Codigo {
            get {
                return ResourceManager.GetString("Codigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Constituyente.
        /// </summary>
        public static string Constituyente {
            get {
                return ResourceManager.GetString("Constituyente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Consultar.
        /// </summary>
        public static string Consultar {
            get {
                return ResourceManager.GetString("Consultar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Consultar Planes de Mejora.
        /// </summary>
        public static string ConsultarPlanesDeMejora {
            get {
                return ResourceManager.GetString("ConsultarPlanesDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Course.
        /// </summary>
        public static string Curso {
            get {
                return ResourceManager.GetString("Curso", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a General Data.
        /// </summary>
        public static string DatosGenerales {
            get {
                return ResourceManager.GetString("DatosGenerales", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Description of the previous action.
        /// </summary>
        public static string DescripcionDeLaAccionPrevia {
            get {
                return ResourceManager.GetString("DescripcionDeLaAccionPrevia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Edit Assessment Plan.
        /// </summary>
        public static string EditarPlanDeAssessment {
            get {
                return ResourceManager.GetString("EditarPlanDeAssessment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a State.
        /// </summary>
        public static string Estado {
            get {
                return ResourceManager.GetString("Estado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Generate report.
        /// </summary>
        public static string GenerarReporte {
            get {
                return ResourceManager.GetString("GenerarReporte", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Implemented.
        /// </summary>
        public static string Implementado {
            get {
                return ResourceManager.GetString("Implementado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Instrument.
        /// </summary>
        public static string Instrumento {
            get {
                return ResourceManager.GetString("Instrumento", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Maintenance.
        /// </summary>
        public static string Mantenimiento {
            get {
                return ResourceManager.GetString("Mantenimiento", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Monitor Assessment Plan.
        /// </summary>
        public static string MonitorearPlanDeAssessment {
            get {
                return ResourceManager.GetString("MonitorearPlanDeAssessment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Level of acceptance.
        /// </summary>
        public static string NivelDeAceptacion {
            get {
                return ResourceManager.GetString("NivelDeAceptacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Pending.
        /// </summary>
        public static string Pendiente {
            get {
                return ResourceManager.GetString("Pendiente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Academic period.
        /// </summary>
        public static string PeriodoAcademico {
            get {
                return ResourceManager.GetString("PeriodoAcademico", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assessment Plan.
        /// </summary>
        public static string PlanDeAssessment {
            get {
                return ResourceManager.GetString("PlanDeAssessment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Improvement plan.
        /// </summary>
        public static string PlanDeMejora {
            get {
                return ResourceManager.GetString("PlanDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a The present functionality allows to consult a Plans of Improvement.
        /// </summary>
        public static string PresenteFuncionalidadPermiteRealizarUnaConsultaDePlanesDeMejora {
            get {
                return ResourceManager.GetString("PresenteFuncionalidadPermiteRealizarUnaConsultaDePlanesDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a The present functionality allows to register Improvement Plans.
        /// </summary>
        public static string PresenteFuncionalidadPermiteRegistrarPlanesDeMejora {
            get {
                return ResourceManager.GetString("PresenteFuncionalidadPermiteRegistrarPlanesDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Register.
        /// </summary>
        public static string Registrar {
            get {
                return ResourceManager.GetString("Registrar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Register Assessment Plan.
        /// </summary>
        public static string RegistrarPlanDeAssessment {
            get {
                return ResourceManager.GetString("RegistrarPlanDeAssessment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Register improvement plans.
        /// </summary>
        public static string RegistrarPlanesDeMejora {
            get {
                return ResourceManager.GetString("RegistrarPlanesDeMejora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Campus.
        /// </summary>
        public static string Sede {
            get {
                return ResourceManager.GetString("Sede", resourceCulture);
            }
        }
    }
}
