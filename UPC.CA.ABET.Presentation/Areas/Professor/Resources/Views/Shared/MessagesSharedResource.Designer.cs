﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.Shared {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MessagesSharedResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MessagesSharedResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.Shared.MessagesSharedRes" +
                            "ource", typeof(MessagesSharedResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a An error occurred while trying to delete the improvement action.\n Technical detail:.
        /// </summary>
        public static string EliminarAMError {
            get {
                return ResourceManager.GetString("EliminarAMError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a You can not delete the improvement action because there is in an Action Plan.
        /// </summary>
        public static string EliminarAMestaPlanAccion {
            get {
                return ResourceManager.GetString("EliminarAMestaPlanAccion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Improvement action not found.
        /// </summary>
        public static string EliminarAMnoEncontro {
            get {
                return ResourceManager.GetString("EliminarAMnoEncontro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Improvement action was successfully deleted.
        /// </summary>
        public static string EliminarAMSuccess {
            get {
                return ResourceManager.GetString("EliminarAMSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Are you sure?.
        /// </summary>
        public static string EstaSeguro {
            get {
                return ResourceManager.GetString("EstaSeguro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Operation Failed!.
        /// </summary>
        public static string OperacionFallida {
            get {
                return ResourceManager.GetString("OperacionFallida", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a The PPP survey finding was successfully edited.
        /// </summary>
        public static string Seeditosatisfactoriamente {
            get {
                return ResourceManager.GetString("Seeditosatisfactoriamente", resourceCulture);
            }
        }
    }
}
