﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstantHelpersFromHelpers = UPC.CA.ABET.Helpers.ConstantHelpers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Helpers
{
    public static class ProfessorConstantHelpers
    {
        public static string GetAreaForCreateNewFinding(int instrumentId)
        {
            var result = "";
            switch (instrumentId)
            {
                default:
                    result = "Survey";
                    break;
            }
            return result;
        }

        public static string GetControllerForCreateNewFinding(int instrumentId)
        {
            var result = "";
            switch (instrumentId)
            {
                default:
                    result = "Register";
                    break;
            }
            return result;
        }

        public static string GetActionForCreateNewFinding(int instrumentId)
        {
            var result = "";
            switch (instrumentId)
            {
                case ConstantHelpersFromHelpers.INSTRUMENTOS.PPP:
                    result = "LstHallazgosPPP";
                    break;
                case ConstantHelpersFromHelpers.INSTRUMENTOS.GRA:
                    result = "LstHallazgosGRA";
                    break;
                default:
                    break;
            }
            return result;
        }

        #region INSTRUMENTOS
        public static class INSTRUMENTOS
        {
            public const int IFC = 1;
            public const int ACC = 2;
            public const int PPP = 3;
            public const int GRA = 4;
            public const int LCFC = 5;
            public const int RC = 6;
            public const int RV = 7;
            public const int ARD = 8;
        }
        #endregion
    }
}