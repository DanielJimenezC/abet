﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class AccDiscoveriesController : BaseController
    {

        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }

        public ActionResult Consult(bool? isSearchRequest, int? IdSubModalidadPeriodoAcademico, int? IdSede, int? IdCarrera, string codigoNivelAceptacion)
        {
            var viewModel = new ConsultAccDiscoveriesViewModel();
            //Cargar ciclos
            viewModel.AvailablePeriodoAcademicos = GedServices.GetAllPeriodoAcademicosServices(Context)
                    .Select(o => new SelectListItem { Value = o.IdPeriodoAcademico.ToString(), Text = o.CicloAcademico }).OrderByDescending( o => o.Text);
            if (viewModel.AvailablePeriodoAcademicos == null)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudieronCargarLosPeriodosAcademicos);
            }
            //Cargar sedes
            viewModel.AvailableSedes = GedServices.GetAllSedesServices(Context).Select(s => new SelectListItem { Value = s.IdSede.ToString(), Text = s.Nombre });

            //Cargar carreras
            viewModel.AvailableCarreras = GedServices.GetAllCarrerasServices(Context, Session.GetEscuelaId()).Select(s => new SelectListItem { Value = s.IdCarrera.ToString(), Text = s.NombreEspanol });

            //Cargar nivelAceptacion
            viewModel.AvailableNivelAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices().Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                viewModel.Resultados = GenerarListaHallazgosAccResultViewModel(GedServices.GetHallazgosForAccServices(Context, IdSubModalidadPeriodoAcademico, IdSede, IdCarrera, codigoNivelAceptacion));
            }


            return View(viewModel);
        }

        private List<HallazgoAccViewModel> GenerarListaHallazgosAccResultViewModel(List<Hallazgo> hallazgos)
        {
            var lstviewModel = new List<HallazgoAccViewModel>();

            foreach (var hallazgo in hallazgos)
            {
                var vm = new HallazgoAccViewModel
                {
                    IdHallazgo = hallazgo.IdHallazgo,
                    PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Sede = hallazgo.Sede.Nombre,
                    Descripcion = hallazgo.DescripcionEspanol,
                    Codigo = hallazgo.Codigo,
                    NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad)
                };

                lstviewModel.Add(vm);
            }

            return lstviewModel;
        }

        public ActionResult Edit(int id)
        {
            var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == id);

            var viewModel = new EditHallazgoAccViewModel();

            viewModel.IdHallazgo = hallazgo.IdHallazgo;
            viewModel.Codigo = hallazgo.Codigo;
            viewModel.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            viewModel.Sede = hallazgo.Sede.Nombre;
            viewModel.Descripcion = hallazgo.DescripcionEspanol;
            viewModel.NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad);
            viewModel.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
            viewModel.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditHallazgoAccViewModel model)
        {
            bool success = GedServices.ActualizarHallazgoifcServices(Context, model.IdHallazgo, model.NivelAceptacion, model.Descripcion);

            if (success)
            {
                PostMessage(MessageType.Success, @"" + MessageResource.SeEditoExitosamenteHallazgoInformeDeFindDeCiclo);
            }
            else
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoEditarElInformeDeFinDeCiclo);
            }

            return RedirectToAction("Consult");
        }

        public ActionResult View(int id)
        {
            var hallazgo = (from h in Context.Hallazgo where h.IdHallazgo == id select h).FirstOrDefault();

            var viewModel = new EditHallazgoAccViewModel();

            viewModel.IdHallazgo = hallazgo.IdHallazgo;
            viewModel.Codigo = hallazgo.Codigo;
            viewModel.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            viewModel.Sede = hallazgo.Sede.Nombre;
            viewModel.Descripcion = hallazgo.DescripcionEspanol;
            viewModel.NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad);
            viewModel.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
            viewModel.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            return View(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var hallazgo = (from h in Context.Hallazgo where h.IdHallazgo == id select h).FirstOrDefault();

            var viewModel = new EditHallazgoAccViewModel();

            viewModel.IdHallazgo = hallazgo.IdHallazgo;
            viewModel.Codigo = hallazgo.Codigo;
            viewModel.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            viewModel.Sede = hallazgo.Sede.Nombre;
            viewModel.Descripcion = hallazgo.DescripcionEspanol;
            viewModel.NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad);
            viewModel.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
            viewModel.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(EditHallazgoAccViewModel model)
        {
            int idHallazgo = model.IdHallazgo;

            bool success = GedServices.EliminarHallazgoIfcServices(Context, idHallazgo);

            if (success)
            {
                PostMessage(MessageType.Success, @"" + MessageResource.SeEliminoExitosamenteHallazgoInformeDeFindDeCiclo);
            }
            else
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoEliminarElInformeDeFinDeCiclo);
            }

            return RedirectToAction("Consult");
        }


        public JsonResult GetAllCarrerasBydIdSede(int idSede)
        {
            var carreras = GedServices.GetCarreraBySedeServices(Context, idSede);
            var items = new List<SelectListItem>();

            foreach (var a in carreras)
            {
                items.Add(new SelectListItem { Value = a.IdCarrera.ToString(), Text = a.NombreEspanol });
            }
            return Json(new SelectList(items, "Value", "Text"));
        }

        public JsonResult GetSedesByPeriodo(int idPeriodoAcademico)
        {
            var sedes = GedServices.GetAllSedesByPeriodoServices(Context, idPeriodoAcademico);
            var items = new List<SelectListItem>();

            foreach (var a in sedes)
            {
                items.Add(new SelectListItem { Value = a.IdSede.ToString(), Text = a.Nombre });
            }

            return Json(new SelectList(items, "Value", "Text"));
        }

    }
}