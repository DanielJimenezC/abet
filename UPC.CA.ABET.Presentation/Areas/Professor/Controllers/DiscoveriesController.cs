﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.ActionPlan;
using System.IO;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.ActionPlan.Export;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.Shared;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using iTextSharp.text.pdf;
using iTextSharp.text;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.Docente)]
    public class DiscoveriesController : BaseController
    {
        // GET: Professor/Discoveries
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManagementHallazgo()
        {
            return View();
        }
        public ActionResult ManagementAccionesMejora()
        {
            return View();
        }

       /* public ActionResult ManagementMenuAccionesMejora()
        {
            return View();
        }*/


        public ActionResult Discoveries(DiscoveriesViewModel model)
        {
            //TryUpdateModel(model);

            int ModalidadId = Session.GetModalidadId();

           

            if (model.esModelFiltroParaAccionMejora ?? false)
            {
                TempData["filtroModel"] = model;
                return RedirectToAction("AddEditAccionMejora", "Discoveries");
            }


            
            model.fill(CargarDatosContext(),ModalidadId);
            if (model.LstAccionMejora.Count() == 0)
            {
                PostMessage(MessageType.Info, "No se encontraron registros para la busqueda.");
            }

            return View(model);
        }


        #region JsonResults Ajax
        public JsonResult GetCiclos()
        {
            var ciclos = Context.PeriodoAcademico.ToList();
            var LstSelect = new SelectList(ciclos, "IdPeriodoAcademico", "CicloAcademico");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCiclosxModalidad(Int32 ModalidadId)
        {
            var ciclos = (from pa in Context.PeriodoAcademico
                          join smpa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals smpa.IdPeriodoAcademico
                          join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                          join mo in Context.Modalidad on sm.IdModalidad equals mo.IdModalidad
                          where mo.IdModalidad == ModalidadId
                          select new
                          {
                              IdPeriodoAcademico = pa.IdPeriodoAcademico,
                              CicloAcademico = pa.CicloAcademico
                          }).ToList();

            var LstSelect = new SelectList(ciclos, "IdPeriodoAcademico", "CicloAcademico");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarreras(Int32 CicloId)
        {
            var idEscuela = Session.GetEscuelaId();
            var carreras = (from spa in Context.SubModalidadPeriodoAcademico
                            join cpa in Context.CarreraPeriodoAcademico on spa.IdSubModalidadPeriodoAcademico equals cpa.IdSubModalidadPeriodoAcademico
                            join c in Context.Carrera on cpa.IdCarrera equals c.IdCarrera
                            where spa.IdPeriodoAcademico == CicloId
                            select new { c.IdCarrera ,c.NombreEspanol }).ToList();

                
                
            var LstSelect = new SelectList(carreras, "IdCarrera", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetComision(Int32? CarreraId, Int32? PeriodoAcademicoId)
        {
            //Convertir Periodo en Submodapea

            int isdubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var comisiones = Context.Comision.Where(X => CarreraId.HasValue ? X.CarreraComision.Any(Y => Y.IdCarrera == CarreraId.Value) : true).ToList();
            if (PeriodoAcademicoId.HasValue)
                comisiones = comisiones.Where(X => (X.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico ?? 0) == isdubmoda).ToList();
            else
            {
                var periodoActivo = Context.PeriodoAcademico.FirstOrDefault(X => X.Estado == "ACT");
                if (periodoActivo != null)
                {
                    comisiones = comisiones.Where(X => (X.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico ?? 0) == isdubmoda).ToList();

                }
            }

            var LstSelect = new SelectList(comisiones, "IdComision", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOutcomes(Int32? ComisionId, Int32? PeriodoAcademicoId)
        {
            var outcomes = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(Context, ComisionId, PeriodoAcademicoId);
            var LstSelect = new SelectList(outcomes, "IdOutcome", "Outcome.Nombre");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetComisionForIFC(Int32? PeriodoAcademicoId)
        {
            var periodoAcademico = CargarDatosContext().session.GetPeriodoAcademicoId().Value;
            var comisiones = Context.Comision
                .Where(x => PeriodoAcademicoId.HasValue ? x.AcreditadoraPeriodoAcademico.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId.Value :
                    x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == periodoAcademico).ToList();
            var LstSelect = new SelectList(comisiones, "IdComision", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetArea(Int32? PeriodoAcademicoId)
        {
            var periodoAcademico = PeriodoAcademicoId;
            int IdSubModalidadPeriodoAcademico = (from a in Context.SubModalidadPeriodoAcademico
                                 join pa in Context.PeriodoAcademico on a.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                 where pa.IdPeriodoAcademico == periodoAcademico
                                 select a.IdSubModalidadPeriodoAcademico).FirstOrDefault();
            var areas = (from a in Context.UnidadAcademica
                         where a.Tipo == "AREA" && a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                         select new { a.IdUnidadAcademica, a.NombreEspanol }).ToList();

            var LstSelect = new SelectList(areas, "IdUnidadAcademica", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubArea(Int32? PeriodoAcademicoId, Int32? AreaUnidadAcademicaId)
        {
            var periodoAcademico = PeriodoAcademicoId;
            int IdSubModalidadPeriodoAcademico = (from a in Context.SubModalidadPeriodoAcademico
                                                  join pa in Context.PeriodoAcademico on a.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                  where pa.IdPeriodoAcademico == periodoAcademico
                                                  select a.IdSubModalidadPeriodoAcademico).FirstOrDefault();
            var subareas = (from a in Context.UnidadAcademica
                         where a.Tipo == "SUBAREA" && a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && a.IdUnidadAcademicaPadre == AreaUnidadAcademicaId
                            select new { a.IdUnidadAcademica, a.NombreEspanol }).ToList();

            var LstSelect = new SelectList(subareas, "IdUnidadAcademica", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCursoUnidadAcademica(Int32? PeriodoAcademicoId, Int32? AreaUnidadAcademicaId, Int32? SubAreaUnidadAcademicaId)
        {
            var periodoAcademico = PeriodoAcademicoId;
            int IdSubModalidadPeriodoAcademico = (from a in Context.SubModalidadPeriodoAcademico
                                                  join pa in Context.PeriodoAcademico on a.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                  where pa.IdPeriodoAcademico == periodoAcademico
                                                  select a.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            var cursos = (from a in Context.UnidadAcademica
                            where a.Tipo == "CURSO" && a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && a.IdUnidadAcademicaPadre == SubAreaUnidadAcademicaId
                          select new { a.IdUnidadAcademica, a.NombreEspanol }).ToList();

            var LstSelect = new SelectList(cursos, "IdUnidadAcademica", "NombreEspanol");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCursosConOutcomePorCiclo(Int32? PeriodoAcademicoId)
        {
            int? idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var query0 = (from mcd in CargarDatosContext().context.MallaCocosDetalle
                          join oc in CargarDatosContext().context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in CargarDatosContext().context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in CargarDatosContext().context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join ccpa in CargarDatosContext().context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in CargarDatosContext().context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          join curso in CargarDatosContext().context.Curso on cmc.IdCurso equals curso.IdCurso
                          where oc.IdSubModalidadPeriodoAcademico == idsubmoda && c.IdEscuela == EscuelaId
                          select new {
                              IdCurso = cmc.IdCurso,
                              Nombre = curso.Codigo+" - "+curso.NombreEspanol+ " - "+cmc.NombreMalla
                          }).AsQueryable();

            query0 = query0.Distinct();
            //var LstSelect = new SelectList(CargarDatosContext().context.Curso.Where(x => (query0).Contains(x.IdCurso)).ToList(), "IdCurso", "Nombre");
            var LstSelect = new SelectList(query0.ToList(), "IdCurso", "Nombre");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOutcomePorCurso(Int32? PeriodoAcademicoId, Int32? CursoId)
        {
            //var periodoAcademico = CargarDatosContext().session.GetPeriodoAcademicoId().Value;
            int? idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var queryMC = (from mc in Context.MallaCocos
                           join mcd in Context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                           where (mc.IdSubModalidadPeriodoAcademico == idsubmoda)
                           join oc in Context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                           join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                           where (cmc.IdCurso == CursoId)
                           select oc.IdOutcomeComision);

            var LstSelect = new SelectList(Context.OutcomeComision.Where(x => (queryMC).Contains(x.IdOutcomeComision))
                .Select(x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre }).ToList(), "IdOutcome", "Nombre");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCursos(Int32? PeriodoAcademicoId, Int32? CarreraId)
        {

            int? idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var cursosDisponibles = Context.MallaCurricularPeriodoAcademico
                .Where(mcpa => mcpa.MallaCurricular.IdCarrera == CarreraId && mcpa.IdSubModalidadPeriodoAcademico == idsubmoda)
                .SelectMany(mcpa => mcpa.MallaCurricular.CursoMallaCurricular.Select(cmc => cmc.Curso))
                .Distinct()
                .OrderBy(x => x.NombreEspanol)
                .ToList();

            var CursoHabiles = Context.MallaCocos.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.CarreraComision.IdCarrera == CarreraId.Value)
                .SelectMany(x => x.MallaCocosDetalle).Select(x => x.CursoMallaCurricular.Curso).Distinct().ToList();

            var existeCurso = CursoHabiles.Select(X => X.IdCurso).ToList();


            var cursos = cursosDisponibles.Where(X => existeCurso.Any(Y => (Y == X.IdCurso))).ToList();


            var LstSelect = new SelectList(cursos, "IdCurso", "NombreEspanol");

            return Json(LstSelect, JsonRequestBehavior.AllowGet);


        }

        #endregion

        public ActionResult ViewDiscovery(Int32 id, bool? AMSuccess = false)
        {
            var language = Session.GetCulture().ToString();
            ViewDiscoveryViewModel model = new ViewDiscoveryViewModel();
            model.fill(CargarDatosContext(), id);
            if(AMSuccess == true)
                if (language == "es-PE")
                    PostMessage(MessageType.Success, "Cambios guardados exitosamente.");
                else
                    PostMessage(MessageType.Success, "Changes saved successfully.");
                        
            return View(model);
        }

        public ActionResult AddEditNewAccionMejora(Int32? IdAccionMejora)
        {
            AddEditNewAccionMejoraViewModel model = new AddEditNewAccionMejoraViewModel();
            int idCiclo = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

            model.CargarDatos(CargarDatosContext(), IdAccionMejora, idCiclo, EscuelaId);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditNewAccionMejora(AddEditNewAccionMejoraViewModel model)
        {
            try
            {
                string ciclo = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").CicloAcademico;


                AccionMejora accionmejora = new AccionMejora();

                if (model.IdAccionMejora.HasValue)
                    accionmejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == model.IdAccionMejora);
                accionmejora.DescripcionEspanol = model.Descripcion;
                accionmejora.DescripcionIngles = model.Descripcion;
                accionmejora.Estado = "PENDI";

                if (model.IdAccionMejora.HasValue == false )
                    accionmejora.Codigo = ciclo + "-A-" + Context.AccionMejora.Max(x => x.IdAccionMejora) + 1;

                if (model.IdAccionMejora.HasValue == false )
                    Context.AccionMejora.Add(accionmejora);

                if (model.IdAccionMejora.HasValue)
                { 
                    List<HallazgoAccionMejora> lstham = Context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == model.IdAccionMejora).ToList();
                    Context.HallazgoAccionMejora.RemoveRange(lstham);
                }
                
                for (int i = 0; i<model.HallazgoId.Count; i++)
                {
                    HallazgoAccionMejora ham = new HallazgoAccionMejora();
                    ham.AccionMejora = accionmejora;
                    ham.IdHallazgo = model.HallazgoId[i];
                        
                    Context.HallazgoAccionMejora.Add(ham);
                }


                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }
            return RedirectToAction("LstAccionesMejora", "Discoveries");
        }
      
       

        public ActionResult AddEditAccionMejora(Int32? AccionMejoraId, Int32? HallazgoId, Int32? InstrumentoIdEditar, DiscoveriesViewModel filtroModel)
        {
            int ModalidadId = Session.GetModalidadId();
            AddEditAccionMejoraViewModel model = new AddEditAccionMejoraViewModel();
            filtroModel = (DiscoveriesViewModel)TempData["filtroModel"];
            model.fill(CargarDatosContext(), AccionMejoraId, filtroModel, InstrumentoIdEditar, ModalidadId);
            if (HallazgoId.HasValue)
            {
                var hallazgo = Context.Hallazgo.FirstOrDefault(X => X.IdHallazgo == HallazgoId.Value);
                if (hallazgo != null)
                {
                    var codigoMatch = hallazgo.Codigo.Replace("-F-", "$").Split('$').First();

                    var CantidadMatch = Context.AccionMejora.Where(X => X.Codigo.Contains(codigoMatch)).Count();
                    CantidadMatch++;
                    var nuevoCodigo = codigoMatch + "-A-" + CantidadMatch.ToString("000");
                    model.Codigo = nuevoCodigo;
                    model.HallazgoIdSeleccionado = HallazgoId.Value;
                    model.HallazgoId.Clear();
                    model.HallazgoId.Add(HallazgoId.Value);
                }
            }
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> AddEditAccionMejora(AddEditAccionMejoraViewModel model)
        {
            try
            {
                AccionMejora accionMejora = null;
                if (model.AccionMejoraId.HasValue)
                {
                    accionMejora = Context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == model.AccionMejoraId.Value);
                }
                if (accionMejora == null)
                {
                    accionMejora = new AccionMejora();
                    Context.AccionMejora.Add(accionMejora);
                }
                accionMejora.DescripcionEspanol = model.Descripcion;
                accionMejora.DescripcionIngles = model.Descripcion;
                accionMejora.Codigo = String.IsNullOrEmpty(model.Codigo) ? "Sin Codigo" : model.Codigo;
                accionMejora.Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO;

                await Context.SaveChangesAsync();

                var lstAntiguosHallazgos = Context.HallazgoAccionMejora.Where(X => X.IdAccionMejora == accionMejora.IdAccionMejora).ToList();
                if (lstAntiguosHallazgos != null && lstAntiguosHallazgos.Count > 0)
                    Context.HallazgoAccionMejora.RemoveRange(lstAntiguosHallazgos);


                if (model.HallazgoId != null && model.HallazgoId.Count > 0)
                {
                    List<String> Codigos = Context.Hallazgo.Where(X => model.HallazgoId.Any(Y => Y == X.IdHallazgo)).Select(X => X.Codigo).ToList();
                    model.HallazgoId = Context.Hallazgo.Where(X => Codigos.Any(Y => Y == X.Codigo)).Select(X => X.IdHallazgo).ToList();

                    Context.HallazgoAccionMejora.AddRange(model.HallazgoId.Select(X => new HallazgoAccionMejora { IdAccionMejora = accionMejora.IdAccionMejora, IdHallazgo = X }).ToList());
                    var hallazgo = Context.Hallazgo.FirstOrDefault(X => model.HallazgoId.Any(Y => Y == X.IdHallazgo));
                    if (hallazgo != null)
                    {
                        var codigoMatch = hallazgo.Codigo.Replace("-F-", "$").Split('$').First();

                        var CantidadMatch = Context.AccionMejora.Where(X => X.Codigo.Contains(codigoMatch)).Count();
                        CantidadMatch++;
                        var nuevoCodigo = codigoMatch + "-A-" + CantidadMatch.ToString("000");
                        accionMejora.Codigo = nuevoCodigo;
                    }
                }

                // await Context.SaveChangesAsync();

                if (model.HallazgoIdSeleccionado.HasValue)
                {
                    return RedirectToAction("ViewDiscovery", "Discoveries", new { id = model.HallazgoIdSeleccionado.Value , AMSuccess= true });
                }

                DiscoveriesViewModel m = new DiscoveriesViewModel();
                m.InstrumentoId = model.InstrumentoId;
                m.ResultadoId = 2;                
                //PostMessage(MessageType.Success, "Cambios Guardados exitosamente.");
                return RedirectToAction("Discoveries", "Discoveries", m);
            }
            catch (Exception)
            {

                PostMessage(MessageType.Success, "Error al editar la acción de mejora.");
                throw;
            }            
        }
       


       /* public ActionResult LstAccionesMejora(int? NumeroPagina, int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome)
        {
            var model = new LstAccionesMejoraViewModel();

            model.fill(CargarDatosContext(), escuelaId, NumeroPagina, IdConstituyente, IdInstrumento, IdCurso, IdOutcome);
            return View(model);
        }


        [HttpPost]
        public ActionResult LstAccionesMejora(LstAccionesMejoraViewModel model)
        {
            return RedirectToAction("LstAccionesMejora","Discoveries", new { NumeroPagina = model.NumeroPagina, IdConstituyente = model.IdConstituyente, IdInstrumento = model.IdInstrumento, IdCurso = model.IdCurso, IdOutcome = model.IdOutcome });
        }*/

        public ActionResult LstPlanAccion()
        {
            var model = new LstPlanAccionViewModel();
            model.fill(CargarDatosContext());
            return View(model);
        }
        [HttpPost]
        public ActionResult LstPlanAccion(LstPlanAccionViewModel model)
        {
            model.fill(CargarDatosContext());
            if(model.LstPlanAccion.Count() == 0)
            {
                ViewBag.Message = "No se encontraron registros.";
            }
            return PartialView("_TablaLstPlanAccion", model);
        }

        //public ActionResult DetallePlanAccion(DetallePlanAccionViewModel model)
        //{
        //    model.fill(CargarDatosContext());
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult DetallePlanAccion(DetallePlanAccionViewModel model, FormCollection formCollection)
        //{
        //    try
        //    {
        //        //Recoger los Id's de las acciones escogidas a partir del formCollection
        //        List<Int32> AccionesEscogidas = formCollection.AllKeys.Where(X => X.StartsWith("accionEscogida")).Select(X => X.Replace("accionEscogida", "").ToInteger()).ToList();

        //        if (model.Agregar)
        //        {
        //            //Agregar nuevas acciones al plan de acción
        //            var lstNuevas = new List<AccionMejoraPlanAccion>();
        //            foreach (var id in AccionesEscogidas)
        //            {
        //                AccionMejoraPlanAccion nueva = new AccionMejoraPlanAccion();
        //                nueva.IdPlanAccion = model.PlanAccionId;
        //                nueva.IdAccionMejora = id;
        //                lstNuevas.Add(nueva);
        //            }
        //            lstNuevas.ForEach(X => context.AccionMejoraPlanAccion.Add(X));


        //        }
        //        else
        //        {
        //            //Quitar las dichas acciones del plan de accion
        //            var lstquitar = new List<AccionMejoraPlanAccion>();
        //            var lstAccionesActuales = context.AccionMejoraPlanAccion.Where(X => X.IdPlanAccion == model.PlanAccionId).ToList();
        //            foreach (var id in AccionesEscogidas)
        //            {
        //                var quitar = lstAccionesActuales.FirstOrDefault(X => X.IdAccionMejora == id);
        //                if (quitar != null)
        //                    lstquitar.Add(quitar);
        //            }
        //            lstquitar.ForEach(X => context.AccionMejoraPlanAccion.Remove(X));
        //        }
        //        context.SaveChanges();
        //        PostMessage(MessageType.Success, "El plan de acción se modificó correctamente");


        //    }
        //    catch (Exception ex)
        //    {
        //        PostMessage(MessageType.Error, "Ocurrio un evento inesperado, porfavor intentar de nuevo mas tarde\n Evento: " + ex.Message);

        //    }
        //    return RedirectToAction("DetallePlanAccion", "Discoveries", new { PlanAccionId = model.PlanAccionId, Agregar = false });

        //}

        public ActionResult CerrarPlanAccion(Int32 PlanAccionId)
        {
            var planAccion = Context.PlanAccion.FirstOrDefault(X => X.IdPlanAccion == PlanAccionId && X.Estado == "ACT");
            if (planAccion != null)
            {
                planAccion.Estado = "CER";
                var accionesMejora = planAccion.AccionMejoraPlanAccion.Select(x => x.AccionMejora);
                foreach (var accion in accionesMejora)
                {
                    accion.Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PROCESADO.CODIGO;
                }
                try
                {
                    Context.SaveChanges();
                }
                catch (Exception ex)
                {
                    PostMessage(MessageType.Error, MessageResource.OcurrioUnErrorAlTratarDeActualizarElEstado + "\n" + MessageResource.Detalle + ":" + ex.Message);
                }
            }
            return RedirectToAction("LstPlanAccion", "Discoveries");
        }

       //LITTLE 



       /* public ActionResult AddEditNewPlanAccion(Int32? PlanAccionId)
        {
            AddEditNewPlanAccionViewModel model = new AddEditNewPlanAccionViewModel();
            model.fill(CargarDatosContext(), escuelaId, null, null, null, null);
            return View(model);
        }

*/


        public ActionResult AddEditPlanAccion(Int32? PlanAccionId)
        {
            AddEditPlanAccionViewModel model = new AddEditPlanAccionViewModel();
            model.PlanAccionId = PlanAccionId;
            model.fill(CargarDatosContext());
            return View(model);
        }        
        [HttpPost]
        public ActionResult AddEditPlanAccion(AddEditPlanAccionViewModel model)
        {

            try
            {
                PlanAccion planAccion;
                //PeriodoAcademico periodo;
                SubModalidadPeriodoAcademico sub_modalidad_periodo_academico;
                if (!model.PlanAccionId.HasValue)
                {

                    planAccion = Context.PlanAccion.FirstOrDefault(X => X.Anio == model.Anio && X.IdCarrera == model.CarreraId && (X.Estado == "ACT" || X.Estado == "CER"));
                    if (planAccion != null)
                    {
                        PostMessage(MessageType.Error, MessageResource.YaExisteUnPlanDeAccionConEstosDatos + "    <a href='LstPlanAccion?Anio=" + model.Anio + "&CarreraId=" + model.CarreraId + "' class='btn btn-dark'><i class='fa fa-arrow-right'></i>&nbsp;Navegar</a>");
                        return RedirectToAction("AddEditPlanAccion", "Discoveries", new { Anio = model.Anio, CarreraId = model.CarreraId });
                    }

                    planAccion = Context.PlanAccion.FirstOrDefault(X => X.Anio == model.Anio && X.IdCarrera == model.CarreraId);

                    if (planAccion == null)
                    {
                        planAccion = new PlanAccion();
                        Context.PlanAccion.Add(planAccion);
                    }
                }
                else
                {
                    planAccion = Context.PlanAccion.FirstOrDefault(X => X.IdPlanAccion == model.PlanAccionId.Value && X.Estado == "ACT");
                    if (planAccion == null)
                    {
                        PostMessage(MessageType.Error, MessageResource.NoSeEncontroPlanDeAccion);
                    }

                }
                //periodo = context.PeriodoAcademico.FirstOrDefault();
                sub_modalidad_periodo_academico = Context.SubModalidadPeriodoAcademico.FirstOrDefault();
                planAccion.IdCarrera = model.CarreraId.Value;
                planAccion.IdSubModalidadPeriodoAcademico = sub_modalidad_periodo_academico.IdSubModalidadPeriodoAcademico;
                planAccion.Estado = "ACT";
                planAccion.Anio = model.Anio;
                Context.SaveChanges();

                return RedirectToAction("DetallePlanAccion", "Discoveries", new { PlanAccionId = planAccion.IdPlanAccion, Agregar = true });
            }
            catch (Exception ex)
            {
                var a = ex;
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError +", "+MessageResource.Detalle.ToLower()+":" + ex.Message);
                return View(model);
            }
        }

        public ActionResult PrintPlanAccion(Int32? PlanAccionId)
        {
            PrintPlanAccionViewModel model = new PrintPlanAccionViewModel();
            model.PlanAccionId = PlanAccionId;

            //IDSUBMODALIDAD=1
            model.CargarDatos(CargarDatosContext(),1);
            return View(model);
        }

        [HttpPost]
        public ActionResult PrintPlanAccion(PrintPlanAccionViewModel model)
        {
            try
            {
                Boolean Spanish = CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL;
                ExportActionPlan pdfExport = new ExportActionPlan(CargarDatosContext().context, Spanish, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, model.PlanAccionId.Value, model.ComisionId.Value);
                if (model.esModelPDF ?? false)
                {
                    string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.PLAN_ACCION_DIRECTORY, "PDF");
                    Directory.CreateDirectory(directory);
                    string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                    string filename = Path.GetFileName(path);
                    return File(path, "application/pdf", filename);
                }
                else
                {
                    string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.PLAN_ACCION_DIRECTORY, "RTF");
                    Directory.CreateDirectory(directory);
                    string path = pdfExport.GetFile(ExportFileFormat.Rtf, directory);
                    string filename = Path.GetFileName(path);
                    return File(path, "application/rtf", filename);
                }

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoExportarSuPlanDeAccion + ex.Message + ex.InnerException);
                return RedirectToAction("LstPlanAccion");
            }
        }

        const string LogoUpc = "logo-upc-red.png";
        
        public ActionResult GetPlanAccionPdf(Int32? PlanAccionId, Int32? ComisionId)
        {
            try
            {
                Boolean Spanish = CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL;
                ExportImprovementPlan pdfExport = new ExportImprovementPlan(CargarDatosContext().context, Spanish, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc);
                pdfExport.SetDataTemplate(PlanAccionId.Value, ComisionId.Value);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.PLAN_ACCION_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                string filename = Path.GetFileName(path);
                return File(path, "application/rtf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoExportarSuPlanDeAccion);
                return RedirectToAction("LstPlanAccion");
            }
        }

        public ActionResult GetPlanAccionRtf(Int32? PlanAccionId, Int32? ComisionId)
        {
            return RedirectToAction("LstPlanAccion");
        }
        public ActionResult EditarResultadoAccionMejora(EditarResultadoAccionMejoraViewModel model)
        {
            try
            {
                var accionMejora = Context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == model.idAccionMejora);
                if (accionMejora != null)
                {
                    accionMejora.Resultado = model.Resultado;
                    accionMejora.Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.IMPLEMENTADO.CODIGO;
                    var basePath = AppSettingsHelper.GetAppSettings("TempPath");
                    if (!string.IsNullOrWhiteSpace(basePath))
                    {
                        var path = basePath + ConstantHelpers.PROFESSOR.EVIDENCE_DIRECTORY;
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        if (model.Archivos != null)
                            foreach (var evidencia in model.Archivos)
                            {
                                if (evidencia == null)
                                    continue;
                                var fileName = (Guid.NewGuid()).ToString().Substring(0, 6) + Path.GetExtension(evidencia.FileName);
                                evidencia.SaveAs(Path.Combine(path, fileName));
                                EvidenciaAccionMejora nuevaEvidencia = new EvidenciaAccionMejora();
                                nuevaEvidencia.IdAccionMejora = model.idAccionMejora;
                                nuevaEvidencia.Nombre = evidencia.FileName;
                                nuevaEvidencia.RutaArchivo = fileName;
                                Context.EvidenciaAccionMejora.Add(nuevaEvidencia);
                            }
                        Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError + ".");
            }
            return RedirectToAction("DetallePlanAccion", new { PlanAccionId = model.PlanAccionId, Agregar = model.Agregar });
        }
        public ActionResult EliminarPlanAccion(Int32 PlanAccionId)
        {
            var planAccion = Context.PlanAccion.FirstOrDefault(X => X.Estado == "ACT" && X.IdPlanAccion == PlanAccionId);
            if (planAccion == null)
            {
                PostMessage(MessageType.Error, MessageResource.NoSeEncontroPlanDeAccion);
                return RedirectToAction("LstPlanAccion", "Discoveries");
            }
            try
            {
                var accionesAsociadas = planAccion.AccionMejoraPlanAccion.ToList();
                Context.AccionMejoraPlanAccion.RemoveRange(accionesAsociadas);
                planAccion.Estado = "INA";
                Context.PlanAccion.Remove(planAccion);
                Context.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.SeEliminoCorrectamenteElPlanDeAccion);

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnErrorAlIntentarEliminarPlanDeAccion + "\n " + MessageResource.DetalleTecnico + ": " + ex.Message);
            }
            return RedirectToAction("LstPlanAccion", "Discoveries");
        }
        /*public ActionResult EliminarAccionMejora(Int32 AccionMejoraId)
        {
            var accionMejora = context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == AccionMejoraId) ?? new AccionMejora();

            var viewModel = new EliminarAccionMejoraViewModel
            {
                AccionMejoraId = AccionMejoraId,
                PuedeEliminar =
                    (accionMejora.Estado == ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO || accionMejora.Estado == ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.RECHAZADO.CODIGO)
                    && !accionMejora.AccionMejoraPlanAccion.Any()
            };

            return PartialView("_EliminarAccionMejora", viewModel);
        }
        [HttpPost]
        public async Task<ActionResult> EliminarAccionMejora(EliminarAccionMejoraViewModel viewModel)
        {
            var accionMejora = context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == viewModel.AccionMejoraId);

            if (accionMejora != null)
            {
                var listaRelacionesPlanAccion = accionMejora.AccionMejoraPlanAccion.ToList();
                if (listaRelacionesPlanAccion.Count != 0) context.AccionMejoraPlanAccion.RemoveRange(listaRelacionesPlanAccion);
                var listaRelacionesHallazgos = accionMejora.HallazgoAccionMejora.ToList();
                if (listaRelacionesHallazgos.Count != 0) context.HallazgoAccionMejora.RemoveRange(listaRelacionesHallazgos);
                var listaRelacionesEvidencias = accionMejora.EvidenciaAccionMejora.ToList();
                if (listaRelacionesEvidencias.Count != 0) context.EvidenciaAccionMejora.RemoveRange(listaRelacionesEvidencias);

                context.AccionMejora.Remove(accionMejora);
                await context.SaveChangesAsync();

                ViewBag.Elimino = true;
            }

            return PartialView("_EliminarAccionMejora", viewModel);
        }*/

        public JsonResult DatosResultadoAccionMejora(Int32 AccionMejoraId)
        {

            List<rutaArchivo> RutasArchivos = Context.EvidenciaAccionMejora.Where(X => X.IdAccionMejora == AccionMejoraId).Select(X => new rutaArchivo { ruta = X.RutaArchivo, nombre = X.Nombre }).ToList();
            String Res = Context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == AccionMejoraId).Resultado;
            return Json(new { r = Res, lista = RutasArchivos }, JsonRequestBehavior.AllowGet);
        }
        public class rutaArchivo
        {
            public String ruta { get; set; }
            public String nombre { get; set; }
        };

        public ActionResult DescargarEvidencia(String ruta, String nombre)
        {
            var basePath = AppSettingsHelper.GetAppSettings("TempPath");
            var path = basePath + ConstantHelpers.PROFESSOR.EVIDENCE_DIRECTORY + @"/" + ruta;
            try
            {
                var lines = System.IO.File.ReadAllLines(path);
                return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, nombre);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Info, MessageResource.NoExisteElDocumento);
                return Redirect(Request.UrlReferrer.ToString());
            }

        }


        public ActionResult AddActionPlan()
        {
            var viewModel = new AddEditPlanAccionViewModel();
            viewModel.fill(CargarDatosContext());
            return PartialView("_AddActionPlan",viewModel);
        }

        [HttpPost]
        public ActionResult AddActionPlan(AddEditPlanAccionViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var LstCarrera = Context.Carrera.ToList();
                    foreach (var carrera in LstCarrera)
                    {
                        var planAccion = new PlanAccion();
                        var periodo = Context.PeriodoAcademico.FirstOrDefault();
                        planAccion.IdCarrera = carrera.IdCarrera;
                        planAccion.IdSubModalidadPeriodoAcademico = periodo.IdPeriodoAcademico;
                        planAccion.Estado = "ACT";
                        planAccion.Anio = viewModel.Anio;
                        Context.PlanAccion.Add(planAccion);
                    }
                    Context.SaveChanges();

                    ViewBag.Guardo = true;
                }
            }
            catch (Exception)
            {
                ViewBag.Guardo = false;
            }
            
            
            viewModel.fill(CargarDatosContext());
            return PartialView("_AddActionPlan", viewModel);
        }

    }

}