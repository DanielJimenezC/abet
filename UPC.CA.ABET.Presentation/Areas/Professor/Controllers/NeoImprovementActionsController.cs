﻿using DocumentFormat.OpenXml.Drawing.Charts;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Professor.NeoImprovementActions;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions;
using UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.Docente)]
    public class NeoImprovementActionsController : BaseController
    {
       
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditImprovementAction(Int32? IdAccionMejora)
        {
            AddEditImprovementActionViewModel model = new AddEditImprovementActionViewModel();
            
            int ModalidadId = session.GetModalidadId();

            var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == ModalidadId).IdSubModalidadPeriodoAcademico;
            model.CargarDatos(CargarDatosContext(), IdAccionMejora, IdSubModalidadPeriodoAcademico, session.GetModalidadId(), EscuelaId);
            return View(model);
        }

        public ActionResult RemoveFinding(Int32 IdHallazgo, Int32 IdAccionMejora)
        {
            try
            {
                HallazgoAccionMejora ham = new HallazgoAccionMejora();
                ham = Context.HallazgoAccionMejora.FirstOrDefault(x => x.IdHallazgo == IdHallazgo && x.IdAccionMejora == IdAccionMejora);

                Context.HallazgoAccionMejora.Remove(ham);

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }

            return RedirectToAction("AddEditImprovementAction", "NeoImprovementActions", new { IdAccionMejora = IdAccionMejora });

        }
        public class AccioneMejoraVM
        {
            public string DescripcionIngles { get; set; }
            public string DescripcionEspanol { get; set; }
            public string Estado { get; set; }
            public string Codigo { get; set; }
            public int IdEscuela { get; set; }
            public int Anio { get; set; }
            public bool ParaPlan { get; set; }
            public int? IdSubModalidadPeriodoAcademico { get; set; } 
            public int Identificador { get; set; }
        }



        public JsonResult codigo(string check)
        {
            if( check != "")
            { 
            List<int> LstIdHall = check.Split(new char[] { ',' }).Select(int.Parse).ToList();
                //List<int> idHallazgos = result.Select(int.Parse).ToList();

                int n = LstIdHall[0];
            string codigoAM = "";
                codigoAM = (from a in Context.Hallazgos
                            where   a.IdHallazgo == n
                            select a.Codigo).FirstOrDefault().ToString();
                string Num= (from a in Context.Hallazgos
                             where a.IdHallazgo == n
                             select a.Identificador).FirstOrDefault().ToString();
                int max = codigoAM.Count();
                codigoAM = 'A' + codigoAM.Substring(1, max-1) +'-'+Num;
            

            return Json(codigoAM, JsonRequestBehavior.AllowGet);
            }
            return Json("-", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddEditImprovementAction(string Codigo, string check, string ComentarioEspanol, string ComentarioIngles, string PrimoStatus)
        {

            if (check != "")
            {
                try
                {
                    string Escuela = Context.Escuela.Where(x => x.IdEscuela == EscuelaId).FirstOrDefault().Codigo;
                    // List<int> LstIdHall = new List<int>();
                    List<int> LstIdHall = check.Split(new char[] { ',' }).Select(int.Parse).ToList();
                    // List<int> lstPreFijos= check.Split(new char[] { ',' }).Select(int.Parse).ToList();
                    List<Hallazgos> HallPrefijos = Context.Hallazgos.Where(x => LstIdHall.Contains(x.IdHallazgo)).ToList();
                    List<int> idCuantitavos = (from a in Context.Instrumento
                                               where a.Tipo == "Cuantitativo"
                                               select a.IdInstrumento).ToList();

                    // string inst = Codigo.Substring(1, 2);


                    foreach (var x in HallPrefijos)
                    {

                        if (idCuantitavos.Contains(x.ConstituyenteInstrumento.IdInstrumento) && PrimoStatus == "true" && Escuela == ConstantHelpers.ESCUELA.ESCUELA_INGENIERIA_SISTEMAS_COMPUTACION)
                        {
                            var LstIdHallPrim = Context.Usp_ListarHallazgosPrimosPorEquivalenciaDeComisiones(x.IdHallazgo).ToList();
                            if (LstIdHallPrim.Count() > 0)
                            {

                                foreach (var a in LstIdHallPrim)
                                {
                                    if (!LstIdHall.Contains(a.IdHallazgo))
                                        LstIdHall.Add(a.IdHallazgo);
                                }
                            }
                        }
                        else
                        {
                            if (!LstIdHall.Contains(x.IdHallazgo))
                                LstIdHall.Add(x.IdHallazgo);
                        }

                    }




                    int modalidad = Session.GetModalidadId().ToInteger();
                    var SMPA = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == modalidad).FirstOrDefault();

                    var codigohallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == LstIdHall.FirstOrDefault()).Codigo;

                    codigohallazgo = codigohallazgo.Remove(0, 1);

                    AccioneMejoraVM accionmejora = new AccioneMejoraVM();

                    accionmejora.DescripcionIngles = ComentarioIngles;
                    accionmejora.DescripcionEspanol = ComentarioEspanol;
                    if (ComentarioIngles.Length == 0)
                    {
                        YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                        accionmejora.DescripcionIngles = helper.Translate(ComentarioEspanol);
                    }




                    accionmejora.Estado = "PENDI";
                    accionmejora.Codigo = Codigo;//"A" + codigohallazgo;
                    accionmejora.IdEscuela = EscuelaId;
                    accionmejora.Anio = SMPA.PeriodoAcademico.CicloAcademico.Substring(0, 4).ToInteger();
                    accionmejora.ParaPlan = true;
                    accionmejora.IdSubModalidadPeriodoAcademico = SMPA.IdSubModalidadPeriodoAcademico;
                    FindLogic ofl = new FindLogic(CargarDatosContext(), 1, SMPA.IdSubModalidadPeriodoAcademico);
                    accionmejora.Identificador = ofl.ObtenerIndiceHallazgo();

                    var IdAccionMejora = RegistrarAccionMejora(accionmejora);

                    foreach (var item in LstIdHall)
                    {
                        if (item == 0) continue;
                        HallazgoAccionMejora ham = new HallazgoAccionMejora();
                        ham.IdHallazgo = item;
                        ham.IdAccionMejora = IdAccionMejora;
                        Context.HallazgoAccionMejora.Add(ham);

                    }
                    Context.SaveChanges();
                    //PostMessage(MessageType.Success);


                    return Json("True", JsonRequestBehavior.AllowGet);

                }
                catch (DbEntityValidationException ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("SeleccionarHall", JsonRequestBehavior.AllowGet);
        }


        public class PPP {
            public int? Npractica { get; set; }
            public int? idAlumno { get; set; }
        }

        public int RegistrarAccionMejora(AccioneMejoraVM viewmodel)
        {
            AccionMejora accionmejora = new AccionMejora();
            accionmejora.DescripcionIngles = viewmodel.DescripcionIngles;
            accionmejora.DescripcionEspanol = viewmodel.DescripcionEspanol;
            accionmejora.Estado = viewmodel.Estado;
            accionmejora.Codigo = viewmodel.Codigo;
            accionmejora.IdEscuela = viewmodel.IdEscuela;
            accionmejora.Anio = viewmodel.Anio;
            accionmejora.ParaPlan = true;
            accionmejora.IdSubModalidadPeriodoAcademico = viewmodel.IdSubModalidadPeriodoAcademico;
            accionmejora.Identificador = viewmodel.Identificador;
            Context.AccionMejora.Add(accionmejora);
            Context.SaveChanges();
            return accionmejora.IdAccionMejora;

        }


        //[HttpPost]
        public ActionResult ListarHallazgosPorBusqueda(int IdInstrumento, int IdCurso, int IdOutcome, int Pagina)
        {
            AddEditImprovementActionViewModel model = new AddEditImprovementActionViewModel();
            model.CargarDatosPartial(Context, Pagina, IdInstrumento, IdCurso, IdOutcome, EscuelaId);


            return PartialView(model);
        }

        //public JsonResult ListarHallazgosPorBusquedaJSON(int IdInstrumento, int IdCurso, string IdOutcome, int Pagina)
        //{
        //    List<int> hallazgosid = new List<int>();

        //    hallazgosid = context.HallazgoAccionMejora.Select(x => x.IdHallazgo).ToList();

        //   //int cantidadAsaltear = (Pagina - 1) * 5;

        //    var query = (from h in context.Hallazgo
        //                 join ci in context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
        //                 join ca in context.Carrera on h.IdCarrera equals ca.IdCarrera
        //                 join o in context.Outcome on h.IdOutcome equals o.IdOutcome
        //                 where (h.Estado != "INA" && ca.IdEscuela == escuelaId
        //                 && (IdInstrumento == 0 || h.IdInstrumento == IdInstrumento)
        //                 && (IdCurso == 0 || h.IdCurso == IdCurso)
        //                 && (IdOutcome == "0" || o.Nombre == IdOutcome)
        //                 && (hallazgosid.Contains(h.IdHallazgo) == false))
        //                 select new { IdHallazgo = h.IdHallazgo, Codigo = h.Codigo, IdCriticidad = h.IdCriticidad,
        //                     Criticidad = h.Criticidad.NombreEspanol, NiveldeAceptacion = h.NivelAceptacionHallazgo.NombreEspanol,
        //                     Instrumento = h.Instrumento.Acronimo,
        //                      Outcome = h.Comision.Codigo+" - "+h.Outcome.Nombre, Curso =  h.IdCurso.HasValue ? h.Curso.NombreEspanol : " - " , Descripcion = h.DescripcionEspanol}).OrderBy(x => x.IdCriticidad).ToList();

        //    for (int i = 1; i < query.Count; i++)
        //    {
        //        if (i == 0)
        //            i = 1;

        //        if (query[i].Codigo == query[i - 1].Codigo)
        //        {
        //            query.RemoveAt(i);
        //            i = i - 1;
        //        }
        //        else
        //        if (query[i].Descripcion == query[i - 1].Descripcion)
        //        {
        //            query.RemoveAt(i);
        //            i = i - 1;
        //        }
        //    }
        //    for (int i = 1; i < query.Count; i++)
        //    {
        //        if (i == 0)
        //            i = 1;

        //        if (query[i].Descripcion == query[i - 1].Descripcion)
        //        {
        //            query.RemoveAt(i);
        //            i = i - 1;
        //        }
        //    }
        //    // var LstHallazgos = query.Skip(cantidadAsaltear).Take(5).ToList();

        //    var LstHallazgos = query.ToList();

        //    /*  var data = context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora && x.AcreditadoraPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.Codigo != "CAC-CC")
        //          .Select(x => new SelectListItem()
        //          {
        //              Text = x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC",
        //              Value = x.Codigo
        //          }).Distinct().ToList();*/
        //    return Json(LstHallazgos, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult ListarHallazgosPorBusquedaJSON(int IdPeriodoAcademico, int IdCarrera, string IdOutcome, int Pagina, int ChkAM, int ChkCritico, int ChkCiclo, int Instrumento)
        {
            List<int> hallazgosid = new List<int>();///

            hallazgosid = Context.HallazgoAccionMejora.Select(x => x.IdHallazgo).ToList();
            string instr = "";
            //int cantidadAsaltear = (Pagina - 1) * 5;
            if (Instrumento == 1) instr = "IFC";
            if (Instrumento == 2) instr = "ARD";
            if (Instrumento == 3) instr = "RV";
            if (Instrumento == 4) instr = "RC";
            if (Instrumento == 5) instr="GRA";
            string codFiltro;
            int ModalidadId = Session.GetModalidadId();
            int idEscuela = Session.GetEscuelaId();
            var subPeriodAcad = session.GetPeriodoAcademicoId().ToInteger();
            if (ChkCiclo == 1)
            {
                ChkCiclo = subPeriodAcad;
                IdPeriodoAcademico = subPeriodAcad;
            }
            if (ChkAM == 1)
            {
                codFiltro = "conAM";
                var queryConAM = Context.ListaAccionMejora(IdPeriodoAcademico, IdCarrera, IdOutcome, ChkCritico, ChkCiclo, ModalidadId, codFiltro);
                var query = (from h in queryConAM
                             where h.Acronimo == instr
                             select new
                             {
                                 IdHallazgo = h.IdHallazgo,
                                 Codigo = h.Codigo,
                                 IdCriticidad = h.IdCriticidad,
                                 Criticidad = h.NombreCriticidad,
                                 NiveldeAceptacion = h.NivelAceptacion,
                                 Instrumento = h.Acronimo,
                                 Outcome = h.Outcome,
                                 Curso = (h.Curso == "" || h.Curso == null) ? " - ": h.Curso,
                                 Descripcion = h.DescripcionEspanol
                             }).OrderBy(x => x.IdCriticidad).ToList();
                for (int i = 1; i < query.Count; i++)
                {
                    if (i == 0)
                        i = 1;

                    if (query[i].Codigo == query[i - 1].Codigo)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                    else
                    if (query[i].Descripcion == query[i - 1].Descripcion)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                }
                for (int i = 1; i < query.Count; i++)
                {
                    if (i == 0)
                        i = 1;

                    if (query[i].Descripcion == query[i - 1].Descripcion)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                }
                var LstHallazgos = query.ToList();
                return Json(LstHallazgos, JsonRequestBehavior.AllowGet);

            }
            else
            {
                codFiltro = "sinAM";
                var querySinAM = Context.ListaAccionMejora(IdPeriodoAcademico, IdCarrera, IdOutcome, ChkCritico, ChkCiclo, ModalidadId, codFiltro);
                var query = (from h in querySinAM
                             where h.Acronimo == instr
                             select new
                             {
                                 IdHallazgo = h.IdHallazgo,
                                 Codigo = h.Codigo,
                                 IdCriticidad = h.IdCriticidad,
                                 Criticidad = h.NombreCriticidad,
                                 NiveldeAceptacion = h.NivelAceptacion,
                                 Instrumento = h.Acronimo,
                                 Outcome = h.Outcome,
                                 Curso = (h.Curso == "" || h.Curso == null) ? " - " : h.Curso,
                                 Descripcion = h.DescripcionEspanol
                             }).OrderBy(x => x.IdCriticidad).ToList();
                for (int i = 1; i < query.Count; i++)
                {
                    if (i == 0)
                        i = 1;

                    if (query[i].Codigo == query[i - 1].Codigo)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                    else
                    if (query[i].Descripcion == query[i - 1].Descripcion)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                }
                for (int i = 1; i < query.Count; i++)
                {
                    if (i == 0)
                        i = 1;

                    if (query[i].Descripcion == query[i - 1].Descripcion)
                    {
                        query.RemoveAt(i);
                        i = i - 1;
                    }
                }
                var LstHallazgos = query.ToList();
                return Json(LstHallazgos, JsonRequestBehavior.AllowGet);

            }
            //var query = (from h in context.Hallazgo
            //             join ci in context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
            //             join ca in context.Carrera on h.IdCarrera equals ca.IdCarrera
            //             join o in context.Outcome on h.IdOutcome equals o.IdOutcome
            //             join submodapa in context.SubModalidadPeriodoAcademico on h.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
            //             join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
            //             join pa in context.PeriodoAcademico on submodapa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
            //             join cri in context.Criticidad on h.IdCriticidad equals cri.IdCriticidad


            //             where (h.Estado != "INA" && ca.IdEscuela == escuelaId && submoda.IdModalidad==ModalidadId
            //             //&& (IdInstrumento == 0 || h.IdInstrumento == IdInstrumento)
            //             && (IdPeriodoAcademico == 0 || pa.IdPeriodoAcademico == IdPeriodoAcademico)
            //             //&& (IdCurso == 0 || h.IdCurso == IdCurso)
            //             && (IdCarrera == 0 || ca.IdCarrera == IdCarrera)
            //             && (IdOutcome == "0" || o.Nombre == IdOutcome)
            //             && (ChkCritico == 0 || h.IdCriticidad == ChkCritico)
            //             && (ChkCiclo == 0 || pa.IdPeriodoAcademico == ChkCiclo)

            //           && (hallazgosid.Contains(h.IdHallazgo) == false))
            //             select new
            //             {
            //                 IdHallazgo = h.IdHallazgo,
            //                 Codigo = h.Codigo,
            //                 IdCriticidad = h.IdCriticidad,
            //                 Criticidad = h.Criticidad.NombreEspanol,
            //                 NiveldeAceptacion = h.NivelAceptacionHallazgo.NombreEspanol,
            //                 Instrumento = h.Instrumento.Acronimo,
            //                 Outcome = h.Comision.Codigo + " - " + h.Outcome.Nombre,
            //                 Curso = h.IdCurso.HasValue ? h.Curso.NombreEspanol : " - ",
            //                 Descripcion = h.DescripcionEspanol
            //             }).OrderBy(x => x.IdCriticidad).ToList();

            //for (int i = 1; i < query.Count; i++)
            //{
            //    if (i == 0)
            //        i = 1;

            //    if (query[i].Codigo == query[i - 1].Codigo)
            //    {
            //        query.RemoveAt(i);
            //        i = i - 1;
            //    }
            //    else
            //    if (query[i].Descripcion == query[i - 1].Descripcion)
            //    {
            //        query.RemoveAt(i);
            //        i = i - 1;
            //    }
            //}
            //for (int i = 1; i < query.Count; i++)
            //{
            //    if (i == 0)
            //        i = 1;

            //    if (query[i].Descripcion == query[i - 1].Descripcion)
            //    {
            //        query.RemoveAt(i);
            //        i = i - 1;
            //    }
            //}
            // var LstHallazgos = query.Skip(cantidadAsaltear).Take(5).ToList();

            //var LstHallazgos = query.ToList();

            /*  var data = context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora && x.AcreditadoraPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.Codigo != "CAC-CC")
                  .Select(x => new SelectListItem()
                  {
                      Text = x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC",
                      Value = x.Codigo
                  }).Distinct().ToList();*/
            return Json(JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _EditHallazgo(int idHallazgo)
        {
            //var idAccionMejora = Convert.ToInt32(IdHallazgo);
            //var idHallazgo = context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == idAccionMejora).FirstOrDefault();
            //var vm = "Lo comenté porque no compilaba";
            var vm = new EditHallazgoAmViewModel()
            {
                Criticidades = Context.Criticidad.ToList(), //criticidades,
            };
            //int idhalla = idAccionMejora;
            var hallazgo = (Context.Hallazgos.Where(x => x.IdHallazgo == idHallazgo).FirstOrDefault());
            vm.descripcionHallazgo = hallazgo.DescripcionEspanol;
            int idcriticidad = Convert.ToInt32(hallazgo.IdCriticidad);
            vm.idhallazgo = hallazgo.IdHallazgo.ToString();
            vm.CodHallazgo = hallazgo.Codigo;
            vm.criticidad = (Context.Criticidad.Where(x => x.IdCriticidad == idcriticidad).FirstOrDefault().NombreEspanol);

            //vm.CargarData(Context);
            return View(vm);
        }

        public ActionResult AccionesMejora(string idHallazgo)
        {
            var HallazgoID = Convert.ToInt32(idHallazgo);

            var vm = new AccionesMejoraViewModel();

            vm.CargarData(HallazgoID, Context);
            return View(vm);
        }

        public JsonResult updateHallazgo(string Hallazgoid,string CodHallazgo, string descripcion, string criticidad,  Boolean? isEdit)
        {
            if (isEdit == true)
            {
                PostMessage(MessageType.Success); 
            }
           
            int hallazgoid = Convert.ToInt32(Hallazgoid);
            //int idhalla = Convert.ToInt32(IdHallazgo);
            //List<int> LstHall = (Context.Hallazgos.Where(x => x.Codigo == CodHallazgo).Select(x => x.IdHallazgo).ToList());

            var hallazgoupdate = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == hallazgoid);


            //for (int i = 0; i < LstHall.Count(); i++)
            //{
            //    int idh = LstHall[i];

            //var hallazgo = (Context.Hallazgos.Where(x => x.IdHallazgo == idh).FirstOrDefault());
            //hallazgo.DescripcionEspanol = descripcion;
            //var criti = (Context.Criticidad.Where(x => x.NombreEspanol == criticidad)).FirstOrDefault();
            //hallazgo.Criticidad = criti;

            //Context.SaveChanges();
            //}

            hallazgoupdate.DescripcionEspanol = descripcion;
            var criti = (Context.Criticidad.Where(x => x.NombreEspanol == criticidad)).FirstOrDefault();
            hallazgoupdate.Criticidad = criti;
            
            Context.SaveChanges();
           

            return Json(JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _SeeMoreHallazgo(string IdAccionMejora) {
            var idAccionMejora = Convert.ToInt32(IdAccionMejora);
            AddEditImprovementActionViewModel model = new AddEditImprovementActionViewModel();

            int ModalidadId = Session.GetModalidadId();

            var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado=="ACT" && x.SubModalidad.IdModalidad==ModalidadId).IdSubModalidadPeriodoAcademico;
            model.CargarDatos(CargarDatosContext(), idAccionMejora, IdSubModalidadPeriodoAcademico, session.GetModalidadId(), EscuelaId);
            return View(model);
        }

        [HttpPost]
        public ActionResult _SeeMoreHallazgo (string descriptionAccionMejora , string idAccionMejora, List<string> body)
        {
            var IdAccionMejora = Convert.ToInt32(idAccionMejora);
            var accionMejora = Context.AccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).FirstOrDefault();
            accionMejora.DescripcionEspanol = descriptionAccionMejora;
            var listHallazgo = Context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).OrderByDescending(x=>x.IdHallazgoAccionMejora);
            var countHallazgo = listHallazgo.Count()-1;
            foreach (var hallazgo in listHallazgo)
            {
                var hallazgoModel = Context.Hallazgo.Where(x => x.IdHallazgo == hallazgo.IdHallazgo).FirstOrDefault();
                hallazgoModel.DescripcionEspanol = body[countHallazgo];
                countHallazgo -= 1;
            }
            Context.SaveChanges();
     
            return RedirectToAction("LstAccionesParaPlan");
        }
        
        public ActionResult HabilitarCursoOutcomeJSON(int IdInstrumento)
        {
            var data = 2;

            if(IdInstrumento !=0)
            { 

             data = Context.Instrumento.FirstOrDefault(x => x.IdInstrumento == IdInstrumento).TieneCurso ? 1 : 0;
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LstAccionesMejora(int? NumeroPagina, int? IdConstituyente, int? IdInstrumento, int? IdCurso, string OutcomeNombre)
        {
            var model = new LstAccionesMejoraViewModel();

            int ModalidadId = Session.GetModalidadId();
            //IDSUBMODALIDAD=1
            model.fill(CargarDatosContext(), EscuelaId, NumeroPagina, IdConstituyente, IdInstrumento, IdCurso, OutcomeNombre, ModalidadId);
            return View(model);
        }

        [HttpPost]
        public ActionResult LstAccionesMejora(LstAccionesMejoraViewModel model)
        {
            return RedirectToAction("LstAccionesMejora", "NeoImprovementActions", new { NumeroPagina = model.NumeroPagina, IdConstituyente = model.IdConstituyente, IdInstrumento = model.IdInstrumento, IdCurso = model.IdCurso, OutcomeNombre = model.Outcomenombre });
        }

        public ActionResult DeleteImprovementAction(Int32 IdAccionMejora)
        {
            try
            {
                AccionMejora accionmejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                accionmejora.Estado = "INA";

                List<HallazgoAccionMejora> hams = Context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).ToList();

                Context.HallazgoAccionMejora.RemoveRange(hams);


                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }

            return RedirectToAction("MantenimientoAccionesMejora", "ImprovementPlans");
        }

        public ActionResult LstAccionesParaPlan(int? Anio, int? IdCarrera, int? IdComision , int? IdConstituyente, int? IdInstrumento, int? IdCurso, string IdOutcome)
        {
            /*List<AccionMejora> ams = context.AccionMejora.Where(x=>x.IdAccionMejora != 221).ToList();
                        for(int i = 0; i< ams.Count; i++)
                        {
                                 AccionMejora am = ams[i];
                            String codigo = am.Codigo;

                            if(codigo.Length>=20)
                            {
                                string[] partes = codigo.Split('-');
                               System.Diagnostics.Debug.WriteLine( partes[2].Substring(0, 4));
                                am.Anio = Int32.Parse( partes[2].Substring(0, 4));
                                context.SaveChanges();
                            }
                            else
                            {
                                am.Anio = Int32.Parse(codigo.Substring(0, 4));
                                context.SaveChanges();
                            }
                        }*/

            var model = new LstAccionesParaPlanViewModel();
            //SUBMODALIDAD=1
           List<int> result = null;

            int ModalidadId = Session.GetModalidadId();

            var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == ModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;


            model.fill(CargarDatosContext(), EscuelaId, Anio, IdCarrera, IdComision, IdSubModalidadPeriodoAcademico, IdConstituyente, IdInstrumento, IdCurso, IdOutcome, result);

            return View(model);

        }
        [HttpPost]
        public ActionResult _LstAccionesParaPlan(int? Anio, int? IdCarrera, int? IdComision,int? IdConstituyente, int? IdInstrumento, int? IdCurso, string IdOutcome, List<int> function_param)
        {
            //dynamic func_param = JsonConvert.DeserializeObject(function_param);

            var model = new LstAccionesParaPlanViewModel();
            model.fill(CargarDatosContext(), EscuelaId,Anio, IdCarrera, IdComision, 1, IdConstituyente, IdInstrumento, IdCurso, IdOutcome, function_param);

            return PartialView(model);
        }
        //[HttpPost]
        //public ActionResult LstAccionesParaPlan(LstAccionesParaPlanViewModel model)
        //{
        //    return RedirectToAction("LstAccionesParaPlan", "NeoImprovementActions", new {
        //        Anio = model.Anio,
        //        IdCarrera = model.IdCarrera,
        //        IdComision = model.IdComision,
        //        IdConstituyente = model.IdConstituyente,
        //        IdInstrumento = model.IdInstrumento,
        //        IdCurso = model.IdCurso,
        //        IdOutcome = model.IdOutcome
        //    });
        //}

        [HttpPost]
        public ActionResult ActualizarParaPlan(LstAccionesParaPlanViewModel model)
        {
            try
            {
                AccionMejora accionMejora = new AccionMejora();
                int idAccionMejora = 0;
                for (int i = 0; i < model.LstAccionMejora.Count; i++)
                {
                    idAccionMejora = model.LstAccionMejora[i].IdAccionMejora;

                    accionMejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == idAccionMejora);

                    accionMejora.ParaPlan = model.LstAccionMejora[i].ParaPlan;
                }


                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch(Exception e)
            {

                PostMessage(MessageType.Error);
            }
            return RedirectToAction("LstAccionesParaPlan", "NeoImprovementActions", new {
                Anio = model.Anio,
                IdCarrera = model.IdCarrera,
                IdComision = model.IdComision,
                IdConstituyente = model.IdConstituyente,
                IdInstrumento = model.IdInstrumento,
                IdCurso = model.IdCurso,
                IdOutcome = model.IdOutcome
            });

        }

        public ActionResult PrintPlanDeMejora(int? Anio, int? IdCarrera, int? IdComision)
        {

            Anio =  2018;
            IdCarrera = 1;
            IdComision = 33;
            var viewModel = new PrintPlanDeMejoraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), (int)Anio, (int)IdCarrera, (int)IdComision);
            Comision comi = (from c in Context.Comision
                             where c.IdComision == IdComision
                             select c).FirstOrDefault();
            Carrera carr = (from c in Context.Carrera
                            where c.IdCarrera == IdCarrera
                            select c).FirstOrDefault();
            List<AccionMejora> am = (from ame in Context.AccionMejora
                               join hac in Context.HallazgoAccionMejora on ame.IdAccionMejora equals hac.IdAccionMejora
                               join h in Context.Hallazgo on hac.IdHallazgo equals h.IdHallazgo
                               where h.IdCarrera == IdCarrera
                               select ame).ToList();
            List<Outcome> outcome = (from o in Context.Outcome
                                     join oc in Context.OutcomeComision on o.IdOutcome equals oc.IdOutcome
                                     where oc.IdComision == IdComision
                                     select o).ToList();

            string name;
            if (comi.Codigo.Substring(0, 3) == "EAC")
            {
                name = "Engineering Accreditation Commission";
            }
            else
                name = "Computing Accreditation Commission";

            Document document = new Document();
            document.SetMargins(75, 75, 40, 40);
            MemoryStream stream = new MemoryStream();
            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;
                document.Open();
                var fontNormal = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
                var fontEspecial = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
                #region PrimeraHoja
                /////////////////////////////////////////////////Titulo comision/////////////////////////////////////////////

                var fontfirst = FontFactory.GetFont("Arial", (float)19.5, Font.BOLD, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("\n" + name, fontfirst);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Año/////////////////////////////////////////////////
                var font = FontFactory.GetFont("Arial", 23, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\nPlan de Mejora " + viewModel.getAnio(), font);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                //////////////////////////////////////////////////////Para el/////////////////////////////////////////////
                var fontPequeño = FontFactory.GetFont("Arial", (float)10.5, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\nPara el", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                //////////////////////////////////////////////////////////Carera//////////////////////////////////////////////
                string carrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).NombreEspanol;
                titulo = new Paragraph("PROGRAMA DE " + carrera, font);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ////////////////////////////////////////////////////////de la////////////////////////////////////////////
                titulo = new Paragraph("\nDe la", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Universidad///////////////////////////////////////
                var fountUni = FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("Universidad Peruana de Ciencias Aplicadas", fountUni);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Pais///////////////////////////////////////////
                titulo = new Paragraph("Lima - Peru", fontfirst);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Logo///////////////////////////////////////////

                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                imagen.WidthPercentage = 30;
                document.Add(imagen);
                document.Add(new Paragraph("\n\n"));
                /////////////////////////////////////////////////////AÑO////////////////////////////////////////////
                var fontAnio = FontFactory.GetFont("Arial", 17, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph(viewModel.getAnio(), fontAnio);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(new Paragraph("\n"));
                document.Add(titulo);
                /////////////////////////////////////////////////////CONFI////////////////////////////////////////////
                
                titulo = new Paragraph("CONFIDENCIAL", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                //fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                //titulo = new Paragraph("La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.", fontNormal);
                //titulo.Alignment = Element.ALIGN_JUSTIFIED;
                //document.Add(titulo);
                #endregion 


                document.NewPage();
                #region PrimerTextoDeLaSegundaHoja

                //////////////////////////////////////////////////////////////////////////Otra HOJA////////////////////////////////////////
                ////////////////////////////////////////////////////////Titulo////////////////////////////////////////

                var fontTitlePageTwo = FontFactory.GetFont("Times New Roman", (float)13.5, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\nCARRERA DE " + carr.NombreEspanol + " PLAN DE MEJORA " + viewModel.getAnio(), fontTitlePageTwo);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ////////////////////////////////////////////////////////Texto////////////////////////////////////////
                var fontTextpageTwo = FontFactory.GetFont("Times New Roman", (float)11.5, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("\nSe muestran a continuación acciones de mejora para el periodo " + viewModel.getAnio()+".", fontTextpageTwo);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);
                ////////////////////////////////////////////////////////Texto2/////////////////////////////////////
                titulo = new Paragraph("\nDentro de cada clasificación, ordenadas por Constituyente e instrumento de evaluación.", fontTextpageTwo);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);

                #endregion


                #region ObjetivoEducacionales Aun falta definir
                //var TtileResultados = FontFactory.GetFont("Times New Roman", (float)14, Font.BOLD, BaseColor.BLACK);
                //titulo = new Paragraph("\n\nObjetivos Educacionales", TtileResultados);
                //titulo.Alignment = Element.ALIGN_LEFT;
                //document.Add(titulo);
                //int posconts = 1;
                //int posInst = 1;

                ////List<Constituyente> LstConstituyenteObjEdu = context.Constituyente.Where(x => x.IdConstituyente == 3).ToList();
                ////List<Instrumento> LstInstrumentos = (from ci in context.ConstituyenteInstrumento
                ////                                     join cons in context.Constituyente on ci.IdConstituyente equals cons.IdConstituyente
                ////                                     join inst in context.Instrumento on ci.IdInstrumento equals inst.IdInstrumento
                ////                                     where ci.IdConstituyente == 3
                ////                                     select inst).ToList();
                //List<Constituyente> LstConstituyenteObjEdu = context.Constituyente.Where(x => x.IdConstituyente == 3).ToList();

                //foreach (var consti in LstConstituyenteObjEdu)
                //{
                //    titulo = new Paragraph("\n"+ posconts + ". Constituyente: "+ consti.NombreEspanol, TtileResultados);
                //    titulo.Alignment = Element.ALIGN_LEFT;
                //    document.Add(titulo);
                //    foreach(var inst in consti.ConstituyenteInstrumento)
                //    {
                //        titulo = new Paragraph("\n      "+ posconts +"."+ posInst + ". Instrumento" + inst.Instrumento.Acronimo + " - "+inst.Instrumento.NombreEspanol, TtileResultados);
                //        titulo.Alignment = Element.ALIGN_LEFT;
                //        document.Add(titulo);
                //        posInst++;
                //    }
                //    posconts++;
                //}

                #endregion


                #region Resultado Del Estudiante
                var TtileResultados = FontFactory.GetFont("Times New Roman", (float)16, Font.BOLD, BaseColor.BLACK);
                var Ttileinst = FontFactory.GetFont("Times New Roman", (float)12, Font.NORMAL, BaseColor.BLACK);
                var formatOut = FontFactory.GetFont("Times New Roman", (float)10, Font.NORMAL, BaseColor.BLACK);
                var format = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.WHITE);
                var formathead = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.BLACK);
                var formatOuttitle = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\n\nResitado del Estudiante", TtileResultados);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);
                int posconts = 1;
                

                List<Constituyente> LstConstituyenteResulEst = Context.Constituyente.ToList();

                foreach (var consti in LstConstituyenteResulEst)
                {
                    int posInst = 1;
                    titulo = new Paragraph("\n"+ posconts + ". Constituyente: "+ consti.NombreEspanol, TtileResultados);
                    titulo.Alignment = Element.ALIGN_LEFT;
                    document.Add(titulo);
                    foreach(var inst in consti.ConstituyenteInstrumento)
                    {
                        
                        titulo = new Paragraph("   "+ posconts +"."+ posInst + ". Instrumento " + inst.Instrumento.Acronimo + " - "+inst.Instrumento.DescripcionEspanol, Ttileinst);
                        titulo.Alignment = Element.ALIGN_LEFT;
                        document.Add(titulo);
                        posInst++;

                        if(inst.Instrumento.Acronimo == "IFC")
                        {

                        }



                        foreach(var item in outcome)
                        {
                            document.Add(Chunk.NEWLINE);
                            PdfPTable table1 = new PdfPTable(1);
                            PdfPTable table2 = new PdfPTable(2);
                            List<HallazgoAccionMejoraPDF> am2 = (from ame in Context.AccionMejora
                                                                  join hac in Context.HallazgoAccionMejora on ame.IdAccionMejora equals hac.IdAccionMejora
                                                                  join h in Context.Hallazgo on hac.IdHallazgo equals h.IdHallazgo
                                                                  where h.IdCarrera == IdCarrera && h.IdInstrumento == inst.IdInstrumento && h.IdCarrera == IdCarrera && h.IdComision == IdComision 
                                                                  && h.IdOutcome == item.IdOutcome && h.IdSubModalidadPeriodoAcademico ==8
                                                                  select new HallazgoAccionMejoraPDF
                                                                  {
                                                                      amCodigo = ame.Codigo,
                                                                      amDescripcionEspanol = ame.DescripcionEspanol,
                                                                      hCodigo = h.Codigo,
                                                                      hDescripcionEspanol = h.DescripcionEspanol,
                                                                      IdInstrumento = h.IdInstrumento,

                                                                  }).Distinct().ToList();

                            PdfPCell cellhead = new PdfPCell();

                            cellhead.Phrase = new Phrase(item.Nombre+": " +item.DescripcionEspanol.ToLower() , formatOuttitle);

                            //cellhead.Colspan = 3;
                            cellhead.BackgroundColor = new BaseColor(184, 204, 228);
                            cellhead.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellhead.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellhead.PaddingBottom = 5;

                            table1.AddCell(cellhead);
                            document.Add(table1);

                            PdfPCell cellsubhead = new PdfPCell();
                            cellsubhead.BackgroundColor = new BaseColor(184, 204, 228);
                            cellsubhead.Phrase = new Phrase("Hallazgos", formathead);


                            table2.AddCell(cellsubhead);
                            
                            cellsubhead.Phrase = new Phrase("Acciones a Tomar", formathead);
                            table2.AddCell(cellsubhead);
                            document.Add(table2);


                            PdfPTable table3 = new PdfPTable(4);
                            PdfPCell cell3 = new PdfPCell();


                            cell3.BackgroundColor = new BaseColor(31, 73, 125);
                            cell3.Phrase = new Phrase("Código", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Descripción", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Código", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Descripción", format);
                            table3.AddCell(cell3);
                            document.Add(table3);

                            PdfPTable table = new PdfPTable(4);
                            PdfPCell cell = new PdfPCell();

                            foreach (var ac in am2)
                            {

                                //table2.HorizontalAlignment = Element.ALIGN_LEFT;
                                ////p2.Add(table2);
                                ////document.Add(p2);


                                //table2 = new PdfPTable(6);
                                //table2.WidthPercentage = 100;
                                cell.Phrase = new Phrase(ac.amCodigo, formatOut);
                                table.AddCell(cell);
                                cell.Phrase = new Phrase(ac.amDescripcionEspanol, formatOut);
                                table.AddCell(cell);
                                cell.Phrase = new Phrase(ac.hCodigo, formatOut);
                                table.AddCell(cell);
                                cell.Phrase = new Phrase(ac.hDescripcionEspanol, formatOut);
                                //cell.Colspan = 3;
                                //cell.BackgroundColor = new BaseColor(184, 204, 228);
                                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                //cell.PaddingBottom = 5;

                                table.AddCell(cell);

                                
                                //p2.Add(table2);
                                document.Add(table);
                                
                            }

                            if(am2.Count() == 0){
                                cell.Colspan = 2;
                                cell.Phrase = new Phrase("No se encontraron nuevos requerimientos", formatOut);
                                table.AddCell(cell);
                                cell.Phrase = new Phrase("No existen acciones a tomar", formatOut);
                                table.AddCell(cell);


                                table.AddCell(cell);


                                //p2.Add(table2);
                                document.Add(table);

                            }
                            document.Add(Chunk.NEWLINE);
                            document.Add(Chunk.NEWLINE);

                        }

                        
                        
                    }
                    document.NewPage();
                    posconts++;
                }

                #endregion

                #region Comentado
                //List<Constituyente> LstConstituyente = context.Constituyente.ToList();
                //List<ConstituyenteInstrumento> lstconstituyenteinstrumento = context.ConstituyenteInstrumento.Where(x => viewModel.LstInstrumentoID.Contains(x.IdInstrumento)).ToList();


                // List<HallazgoAccionMejoraPDF> lsthallazgosAccionesMejora = viewModel.Lsthams;

                //for (int i = 0; i < LstConstituyente.Count; i++)
                //{

                //    int constituyenteID = LstConstituyente[i].IdConstituyente;
                //    List<Instrumento> lstinstrumentos = context.ConstituyenteInstrumento.Where(x => x.IdConstituyente == constituyenteID && x.Instrumento.Acronimo != "LCFC").Select(x => x.Instrumento).ToList();

                //    List<HallazgoAccionMejoraPDF> lsthamsXInstrumento = new List<HallazgoAccionMejoraPDF>();

                //    if (lsthallazgosAccionesMejora.Where(x => lstinstrumentos.Select(y => y.IdInstrumento).Contains(x.IdInstrumento)).ToList().Count == 0)
                //    {
                //        continue;
                //    }


                //    string nombreOutcome = "";

                //    for (int j = 0; j < lstinstrumentos.Count; j++)
                //    {

                //        int idinstrumento = lstinstrumentos[j].IdInstrumento;
                //        lsthamsXInstrumento = lsthallazgosAccionesMejora.Where(x => x.IdInstrumento == idinstrumento).ToList();

                //        if (lsthamsXInstrumento.Count == 0)
                //            continue;

                //        bool esIFC = false;

                //        fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                //        titulo = new Paragraph("\n* Instrumento: " + lstinstrumentos[j].Acronimo, fontNormal);
                //        if (lstinstrumentos[j].Acronimo == "IFC")
                //            esIFC = true;

                //        titulo.IndentationLeft = 10;
                //        titulo.Alignment = Element.ALIGN_LEFT;
                //        document.Add(titulo);

                //        document.Add(Chunk.NEWLINE);


                //        PdfPTable table2 = new PdfPTable(6);
                //        PdfPCell cell = new PdfPCell(new Phrase("Hallazgos", fontNormal));
                //        PdfPCell cell2 = new PdfPCell(new Phrase("", fontNormal));
                //        Paragraph p2 = new Paragraph();

                //        for (int k = 0; k < lsthamsXInstrumento.Count; k++)
                //        {

                //            HallazgoAccionMejoraPDF hampdf = lsthamsXInstrumento[k];


                //            if (nombreOutcome != hampdf.hOutcome)
                //            {
                //                nombreOutcome = hampdf.hOutcome;

                //                p2 = new Paragraph();
                //                p2.IndentationLeft = 5;
                //                //p.IndentationRight = 5;
                //                table2.HorizontalAlignment = Element.ALIGN_LEFT;
                //                p2.Add(table2);
                //                document.Add(p2);

                //                if (k != 0)
                //                    document.NewPage();

                //                titulo = new Paragraph("\nResultado del estudiante '" + nombreOutcome + "'", FontFactory.GetFont("Arial", 13, Font.BOLD, BaseColor.BLACK));

                //                titulo.IndentationLeft = 15;
                //                titulo.Alignment = Element.ALIGN_LEFT;
                //                document.Add(titulo);

                //                document.Add(Chunk.NEWLINE);




                //                table2 = new PdfPTable(6);
                //                table2.WidthPercentage = 100;
                //                cell = new PdfPCell(new Phrase("Hallazgos " + Anio, fontNormal));
                //                cell.Colspan = 3;
                //                cell.BackgroundColor = new BaseColor(184, 204, 228);
                //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                cell.PaddingBottom = 5;

                //                table2.AddCell(cell);

                //                cell.Phrase = new Phrase("Acciones de Mejora " + (Anio + 1), fontNormal);
                //                table2.AddCell(cell);

                //                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.WHITE);
                //                cell2 = new PdfPCell(new Phrase("", fontNormal));
                //                cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                //                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                cell2.PaddingBottom = 5;
                //                cell2.Colspan = 1;
                //                cell2.Phrase = new Phrase("Código", fontNormal);
                //                cell2.BackgroundColor = new BaseColor(31, 73, 125);
                //                table2.AddCell(cell2);
                //                cell2.Colspan = 2;
                //                cell2.Phrase = new Phrase("Descripción", fontNormal);
                //                table2.AddCell(cell2);
                //                cell2.Phrase = new Phrase("Código", fontNormal);
                //                cell2.BackgroundColor = new BaseColor(31, 73, 125);
                //                cell2.Colspan = 1;
                //                table2.AddCell(cell2);
                //                cell2.Colspan = 2;
                //                cell2.Phrase = new Phrase("Descripción", fontNormal);
                //                table2.AddCell(cell2);

                //                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                //                cell2.BackgroundColor = BaseColor.WHITE;
                //            }







                //            cell2.Rowspan = 1;
                //            // Hallazgo h = lsthamsXInstrumento[k].Hallazgo;
                //            cell2.Phrase = new Phrase(hampdf.hCodigo, fontNormal);
                //            cell2.Colspan = 1;
                //            table2.AddCell(cell2);
                //            //Phrase frase = new Phrase();
                //            //cell2.Phrase = new Phrase(hampdf.hCodigo, fontEspecial);
                //            if (esIFC == false)
                //            {
                //                cell2.Phrase = new Phrase("OUTCOME " + hampdf.hOutcome + ": \n", fontEspecial);

                //                cell2.Phrase.Add(new Chunk(hampdf.hDescripcionEspanol, fontNormal));
                //            }
                //            else
                //            if (hampdf.CursoID.HasValue)
                //            {
                //                Curso c = context.Curso.FirstOrDefault(x => x.IdCurso == hampdf.CursoID);

                //                cell2.Phrase = new Phrase("OUTCOME " + hampdf.hOutcome + ". \n" + "CURSO: " + c.NombreEspanol + " \n", fontEspecial);
                //                cell2.Phrase.Add(new Chunk(hampdf.hDescripcionEspanol, fontNormal));

                //            }

                //            cell2.Colspan = 2;
                //            table2.AddCell(cell2);



                //            cell2.Rowspan = 1;
                //            cell2.Phrase = new Phrase(hampdf.amCodigo, fontNormal);
                //            cell2.Colspan = 1;
                //            table2.AddCell(cell2);


                //            cell2.Rowspan = 1;

                //            cell2.Colspan = 2;
                //            cell2.Phrase = new Phrase(hampdf.amDescripcionEspanol, fontNormal);
                //            table2.AddCell(cell2);

                //        }



                //        p2 = new Paragraph();
                //        p2.IndentationLeft = 5;
                //        //p.IndentationRight = 5;
                //        table2.HorizontalAlignment = Element.ALIGN_LEFT;
                //        p2.Add(table2);
                //        document.Add(p2);



                //        document.Add(new Paragraph("\n", fontNormal));

                //        document.NewPage();

                //    }


                //}
                #endregion

            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "Plan de Mejora.pdf");

        }

        public string PrintPlanDeMejoraTemp(int idcurso, int idplan,int IdCarrera, string codcurso, string periodoacademico,string NomCarrera)
        {

            try
            {
                string nombreReporte = ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + "PlanMejora_" + codcurso + "_" + periodoacademico + ".pdf";
                System.IO.File.Delete(nombreReporte);
                int modalidadid = session.GetModalidadId();
            

                    string name  = (from a in Context.Comision
                                     join b in Context.CarreraComision on a.IdComision equals b.IdComision
                                     join c in Context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                                     join d in Context.PeriodoAcademico on c.IdPeriodoAcademico equals d.IdPeriodoAcademico
                                     join e in Context.SubModalidad on c.IdSubModalidad equals e.IdSubModalidad
                                     where d.CicloAcademico == periodoacademico && e.IdModalidad==modalidadid && b.IdCarrera == IdCarrera
                                     select a.Codigo).FirstOrDefault();
               /* ReportParameter[] parameters = new ReportParameter[7];
                parameters[0] = new ReportParameter("par0", "es-PE");
                parameters[1] = new ReportParameter("par1", "CAC");
                parameters[2] = new ReportParameter("par2", "3");
                parameters[3] = new ReportParameter("par3", "2019");
                parameters[4] = new ReportParameter("par4", "1");
                parameters[5] = new ReportParameter("par5", "2");
                parameters[6] = new ReportParameter("par6", "8");*/

                  var queryString = string.Format("?{0}={1}&{2}={3}&{4}={5}&{6}={7}&{8}={9}&{10}={11}&{12}={13}",
                       ConstantHelpers.QueryStringParameters.LENGUAJEID, "es-PE",
                       ConstantHelpers.QueryStringParameters.CODIGOCOMISION, "CAC",
                       ConstantHelpers.QueryStringParameters.CARRERA_ID, "3",
                       ConstantHelpers.QueryStringParameters.ANIO, "2019",
                       ConstantHelpers.QueryStringParameters.MODALIDAD_ID, "1",
                       ConstantHelpers.QueryStringParameters.COSTITUYENTE_ID, "2",
                       ConstantHelpers.QueryStringParameters.INSTRUMENTO_ID, "8"    
                       );
                ReportViewer ReportViewer1 = new ReportViewer();
                //Html
               
                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/" + queryString);

                
               
               
                //string URL = Url.RouteUrl();


                /*  var basePage = new BasePage();
                  basePage.InitializeReportViewer(ReportViewer1, "ReportPlanMejora");
                  ReportViewer1.LocalReport.SetParameters(parameters);

                  Warning[] warnings;
                  string[] streamids;
                  string mimeType;
                  string encoding;
                  string extension;

                  byte[] bytes = ReportViewer1.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);


                  FileStream fs = new FileStream(nombreReporte,
                     FileMode.Create);
                  fs.Write(bytes, 0, bytes.Length);
                  fs.Close();
                  */

                return ("Documento Exitoso");
            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
                string message = de.Message;
                return (message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine();
                string message = ioe.Message;
                return (message);
            }
                
        }

        public ActionResult PrintPAM(int idplan, string Comision, int IdCarrera)
        {
            var PlanMejora = (from a in Context.PlanMejora
                              where a.IdPlanMejora == idplan
                              select a).FirstOrDefault();


            int modalidadid = session.GetModalidadId();
            var viewModel = new PrintPlanDeMejoraViewModel();

            //////////////////////////////////////////
            var amXcurso = Context.FuncionGetPlan(idplan, Comision.Substring(0,3), IdCarrera);
            //////////////////////////////////////////


            string name = (from a in Context.Comision
                           join b in Context.CarreraComision on a.IdComision equals b.IdComision
                           join c in Context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                           join d in Context.PeriodoAcademico on c.IdPeriodoAcademico equals d.IdPeriodoAcademico
                           join e in Context.SubModalidad on c.IdSubModalidad equals e.IdSubModalidad
                           where e.IdModalidad == modalidadid /*&& b.IdCarrera == IdCarrera && d.CicloAcademico == periodoacademico*/
                select a.Codigo).FirstOrDefault();

            string nameEsp;
            int? Anio = PlanMejora.Anio;

            if (name.Substring(0, 3) == "EAC")
            {
                name = "Engineering Accreditation Commission";
                nameEsp = "COMISIÓN DE ACREDITACIÓN DE INGENIERÍA";
            }
            else
            { 
            name = "Computing Accreditation Commission";
            nameEsp = "COMISIÓN DE ACREDITACIÓN DE COMPUTACIÓN";
            }
            Document document = new Document();
            document.SetMargins(75, 75, 40, 40);
            MemoryStream stream = new MemoryStream();
            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;
                document.Open();
                var fontNormal = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
                var fontEspecial = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
                #region PrimeraHoja
                /////////////////////////////////////////////////Titulo comision/////////////////////////////////////////////

                var fontfirst = FontFactory.GetFont("Arial", (float)19.5, Font.BOLD, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("\n" + name, fontfirst);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Año/////////////////////////////////////////////////
                var font = FontFactory.GetFont("Arial", 23, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\nPlan de Mejora " + Anio, font);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                //////////////////////////////////////////////////////Para el/////////////////////////////////////////////
                var fontPequeño = FontFactory.GetFont("Arial", (float)10.5, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\nPara el", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                //////////////////////////////////////////////////////////Carera//////////////////////////////////////////////
                titulo = new Paragraph("PROGRAMA DE " + nameEsp, font);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ////////////////////////////////////////////////////////de la////////////////////////////////////////////
                titulo = new Paragraph("\nDe la", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Universidad///////////////////////////////////////
                var fountUni = FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("Universidad Peruana de Ciencias Aplicadas", fountUni);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Pais///////////////////////////////////////////
                titulo = new Paragraph("Lima - Peru", fontfirst);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                ///////////////////////////////////////////////////////Logo///////////////////////////////////////////

                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                imagen.WidthPercentage = 30;
                document.Add(imagen);
                document.Add(new Paragraph("\n\n"));
                /////////////////////////////////////////////////////AÑO////////////////////////////////////////////
                var fontAnio = FontFactory.GetFont("Arial", 17, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph(viewModel.getAnio(), fontAnio);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(new Paragraph("\n"));
                document.Add(titulo);
                /////////////////////////////////////////////////////CONFI////////////////////////////////////////////

                titulo = new Paragraph("CONFIDENCIAL", fontPequeño);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                //fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                //titulo = new Paragraph("La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.", fontNormal);
                //titulo.Alignment = Element.ALIGN_JUSTIFIED;
                //document.Add(titulo);
                #endregion
                document.NewPage();
                #region PrimerTextoDeLaSegundaHoja

                //////////////////////////////////////////////////////////////////////////Otra HOJA////////////////////////////////////////
                ////////////////////////////////////////////////////////Titulo////////////////////////////////////////

                    ////var fontTitlePageTwo = FontFactory.GetFont("Times New Roman", (float)13.5, Font.BOLD, BaseColor.BLACK);
                    ////titulo = new Paragraph("\nCARRERA DE " + NomCarrera + " PLAN DE MEJORA " + viewModel.getAnio(), fontTitlePageTwo);
                    ////titulo.Alignment = Element.ALIGN_CENTER;
                    ////document.Add(titulo);
                ////////////////////////////////////////////////////////Texto////////////////////////////////////////
                var fontTextpageTwo = FontFactory.GetFont("Times New Roman", (float)11.5, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("\nSe muestran a continuación acciones de mejora para el periodo " + viewModel.getAnio() + ".", fontTextpageTwo);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);
                ////////////////////////////////////////////////////////Texto2/////////////////////////////////////
                titulo = new Paragraph("\nDentro de cada clasificación, ordenadas por Constituyente e instrumento de evaluación.", fontTextpageTwo);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);

                #endregion

                #region Resultado Del Estudiante

                var TtileResultados = FontFactory.GetFont("Times New Roman", (float)16, Font.BOLD, BaseColor.BLACK);
                var Ttileinst = FontFactory.GetFont("Times New Roman", (float)12, Font.NORMAL, BaseColor.BLACK);
                var formatOut = FontFactory.GetFont("Times New Roman", (float)10, Font.NORMAL, BaseColor.BLACK);
                var format = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.WHITE);
                var formathead = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.BLACK);
                var formatOuttitle = FontFactory.GetFont("Times New Roman", (float)10, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("\n\nResitado del Estudiante", TtileResultados);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);
                int posconts = 1;


                List<Constituyente> LstConstituyenteResulEst = Context.Constituyente.ToList();
                List<string> Const = (from a in amXcurso
                                      select a.Constituyente).Distinct().ToList();



                for (int i = 0; i < Const.Count(); i++)
                {
                    int posInst = 1;
                    titulo = new Paragraph("\n" + posconts + ". Constituyente: " + Const[i], TtileResultados);
                    string constituyentename = Const[i];
                    titulo.Alignment = Element.ALIGN_LEFT;
                    document.Add(titulo);
                    List<string> inst = (from a in amXcurso
                                         where a.Constituyente == constituyentename
                                         select a.Instrumento).Distinct().ToList();

                    for (int j = 0; j < inst.Count(); j++)
                    {
                        titulo = new Paragraph("   " + posconts + "." + posInst + ". Instrumento " + inst[j], Ttileinst);
                        string Instrumentoname = inst[j];
                        titulo.Alignment = Element.ALIGN_LEFT;
                        document.Add(titulo);
                        posInst++;
                        List<string> outc = (from a in amXcurso
                                             where a.Constituyente == constituyentename && a.Instrumento == Instrumentoname
                                             orderby a.Outcome descending
                                             select a.Outcome).Distinct().ToList();
                        for (int k = 0; k < outc.Count(); k++)
                        {
                            document.Add(Chunk.NEWLINE);
                            PdfPTable table1 = new PdfPTable(1);
                            PdfPTable table2 = new PdfPTable(2);
                            string outcomename = outc[k];

                            List<string> Hall = (from a in amXcurso
                                                 where a.Constituyente == constituyentename && a.Instrumento == Instrumentoname && a.Outcome == outcomename
                                                 orderby a.CodHallazgo descending
                                                 select a.CodHallazgo).Distinct().ToList();
                            PdfPCell cellhead = new PdfPCell();

                            cellhead.Phrase = new Phrase(outcomename, formatOuttitle);

                            //cellhead.Colspan = 3;
                            cellhead.BackgroundColor = new BaseColor(184, 204, 228);
                            cellhead.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellhead.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellhead.PaddingBottom = 5;

                            table1.AddCell(cellhead);
                            document.Add(table1);

                            PdfPCell cellsubhead = new PdfPCell();
                            cellsubhead.BackgroundColor = new BaseColor(184, 204, 228);
                            cellsubhead.Phrase = new Phrase("Hallazgos", formathead);


                            table2.AddCell(cellsubhead);

                            cellsubhead.Phrase = new Phrase("Acciones a Tomar", formathead);
                            table2.AddCell(cellsubhead);
                            document.Add(table2);


                            PdfPTable table3 = new PdfPTable(4);
                            PdfPCell cell3 = new PdfPCell();


                            cell3.BackgroundColor = new BaseColor(31, 73, 125);
                            cell3.Phrase = new Phrase("Código", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Descripción", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Código", format);
                            table3.AddCell(cell3);
                            cell3.Phrase = new Phrase("Descripción", format);
                            table3.AddCell(cell3);
                            document.Add(table3);


                            for (int y = 0; y < Hall.Count(); y++)
                            {
                                PdfPTable table = new PdfPTable(4);
                                PdfPCell cell4 = new PdfPCell();
                                string hallazgocod = Hall[y];
                                string nameHall = (from a in amXcurso
                                                   where a.Constituyente == constituyentename && a.Instrumento == Instrumentoname && a.Outcome == outcomename && a.CodHallazgo == hallazgocod
                                                   select a.Hallazgo).Distinct().FirstOrDefault();
                                List<string> Amcod = (from a in amXcurso
                                                      where a.Constituyente == constituyentename && a.Instrumento == Instrumentoname && a.Outcome == outcomename && a.CodHallazgo == hallazgocod
                                                      orderby a.CodAccion descending
                                                      select a.CodAccion).Distinct().ToList();

                                cell4.Phrase = new Phrase(hallazgocod, formatOut);
                                table.AddCell(cell4);
                                cell4.Phrase = new Phrase("-" + nameHall, formatOut);
                                table.AddCell(cell4);

                                for (int z = 0; z < Amcod.Count(); z++)
                                {
                                    PdfPCell cell5 = new PdfPCell();
                                    string AccionCod = Amcod[z];
                                    string AccionName = (from a in amXcurso
                                                         where a.Constituyente == constituyentename && a.Instrumento == Instrumentoname && a.Outcome == outcomename && a.CodHallazgo == hallazgocod && a.CodAccion == AccionCod
                                                         select a.AccionMejora).Distinct().FirstOrDefault();
                                    cell5.Phrase = new Phrase(AccionCod,formatOut);
                                    table.AddCell(cell5);

                                    cell5.Phrase = new Phrase("-" + AccionName, formatOut);
                                    table.AddCell(cell5);

                                }


                                document.Add(table);
                            }



                        }

                    }


                }


                #endregion


            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }
            document.Close();

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "Plan de Mejora.pdf");





        }


        //public ActionResult PrintPAR(int idplan, int IdCarrera)
        //{

        //    var amXcurso = context.FuncionGetPlanResultados(idplan, IdCarrera);
        //    Document document = new Document();
        //    document.SetMargins(75, 75, 40, 40);
        //    int Anio = (from a in context.PlanMejora
        //                where a.IdPlanMejora == idplan
        //                select a.Anio).FirstOrDefault().ToInteger();
        //    string carrera = (from a in context.Carrera
        //                      where a.IdCarrera == IdCarrera
        //                      select a.NombreEspanol).FirstOrDefault();

        //    MemoryStream stream = new MemoryStream();
        //    try
        //    {
        //        PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
        //        pdfWriter.CloseStream = false;
        //        document.Open();
        //        var fontNormal = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
        //        var fontEspecial = FontFactory.GetFont("Times New Roman", (float)11.5, Font.BOLD, BaseColor.BLACK);
        //        #region PrimeraHoja
        //        /////////////////////////////////////////////////Titulo comision/////////////////////////////////////////////
        //        var fontfirst = FontFactory.GetFont("Arial", (float)19.5, Font.BOLD, BaseColor.BLACK);
        //        ///////////////////////////////////////////////////////Año/////////////////////////////////////////////////
        //        var font = FontFactory.GetFont("Arial", 23, Font.BOLD, BaseColor.BLACK);
        //        Paragraph titulo = new Paragraph("\nPlan de Resultados " + Anio, font);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        //////////////////////////////////////////////////////Para el/////////////////////////////////////////////
        //        var fontPequeño = FontFactory.GetFont("Arial", (float)10.5, Font.BOLD, BaseColor.BLACK);
        //        titulo = new Paragraph("\nPara la", fontPequeño);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        //////////////////////////////////////////////////////////Carera//////////////////////////////////////////////
        //        titulo = new Paragraph("CARRERA DE " + carrera, font);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        ////////////////////////////////////////////////////////de la////////////////////////////////////////////
        //        titulo = new Paragraph("\nDe la", fontPequeño);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        ///////////////////////////////////////////////////////Universidad///////////////////////////////////////
        //        var fountUni = FontFactory.GetFont("Arial", 21, Font.BOLD, BaseColor.BLACK);
        //        titulo = new Paragraph("Universidad Peruana de Ciencias Aplicadas", fountUni);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        ///////////////////////////////////////////////////////Pais///////////////////////////////////////////
        //        titulo = new Paragraph("Lima - Peru", fontfirst);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);
        //        ///////////////////////////////////////////////////////Logo///////////////////////////////////////////

        //        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
        //        imagen.BorderWidth = 0;
        //        imagen.Alignment = Element.ALIGN_CENTER;
        //        imagen.WidthPercentage = 30;
        //        document.Add(imagen);
        //        document.Add(new Paragraph("\n\n"));
        //        /////////////////////////////////////////////////////AÑO////////////////////////////////////////////
        //        var fontAnio = FontFactory.GetFont("Arial", 17, Font.BOLD, BaseColor.BLACK);
        //        titulo = new Paragraph(Anio.ToString(), fontAnio);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(new Paragraph("\n"));
        //        document.Add(titulo);
        //        /////////////////////////////////////////////////////CONFI////////////////////////////////////////////

        //        titulo = new Paragraph("CONFIDENCIAL", fontPequeño);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        //fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
        //        //titulo = new Paragraph("La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.", fontNormal);
        //        //titulo.Alignment = Element.ALIGN_JUSTIFIED;
        //        //document.Add(titulo);
        //        #endregion
        //        document.NewPage();


        //    }
        //    catch (DocumentException de)
        //    {
        //        Console.Error.WriteLine(de.Message);
        //    }
        //    catch (IOException ioe)
        //    {
        //        Console.Error.WriteLine(ioe.Message);
        //    }
        //    document.Close();

        //    document.Close();

        //    stream.Flush(); //Always catches me out
        //    stream.Position = 0; //Not sure if this is required

        //    return File(stream, "application/pdf", "Plan de Mejora.pdf");

        //}

        [HttpPost]
        public string SendEmail(int idCurso,string correo, string codcurso,string Curso,string periodoacademico,int IdCarrera, int idplan)
        {
            //string Course,int idplan,int IdCarrera
            string Asunto = "Plan de Mejora " + Curso.ToUpper();
            string Mensaje = "<p style=\"text-align: justify; \">Estimado Coordinador,</p><p style=\"text-align: justify; \"><br></p><p style=\"text-align: justify; \">La Escuela de Ingeniería de Sistemas y Computación (EISC) Comparte con usted el plan de Mejora para el curso de <i> <b>" + Curso.ToUpper() + "</b></i>. Usted podrá visualizar el plan en el siguiente enlace: </p>";
            int anio = (from a in Context.PlanMejora
                        where a.IdPlanMejora == idplan
                        select a.Anio
                        ).FirstOrDefault().ToInteger();

            int modaid = session.GetModalidadId();

            int lstCur = (from pm in Context.PlanMejora
                          join pma in Context.PlanMejoraAccion on pm.IdPlanMejora equals pma.IdPlanMejora
                          join am in Context.AccionMejora on pma.IdAccionMejora equals am.IdAccionMejora
                          join ham in Context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                          join h in Context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                          join c in Context.Curso on h.IdCurso equals c.IdCurso
                          where c.IdCurso ==idCurso
                          select c.IdCurso).Distinct().Count();

            var queryString = string.Format("?{0}={1}&{2}={3}&{4}={5}",
                       ConstantHelpers.QueryStringParameters.LENGUAJEID, "es-PE",
                       ConstantHelpers.QueryStringParameters.CURSOID, idCurso.ToString(),
                       ConstantHelpers.QueryStringParameters.PLANMEJORA_ID, idplan
                       );
            

            if (lstCur > 0)
            {
                EmailGRALogic mailLogic = new EmailGRALogic();

                    try {

                        String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                        String Ruta = "Areas/Report/Resources/WebPages/ReportPlanMejoraCurso.aspx";
                        String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                                Request.Url.Scheme,
                                                                Request.Url.Authority,
                                                                vdirectory,
                                                                "/"+ Ruta + queryString);
                    String urlImage = String.Format("{0}://{1}{2}",
                                                    Request.Url.Scheme,
                                                    Request.Url.Authority, vdirectory);
                    
                        bool enviado = mailLogic.SendMailArc(Asunto, mailLogic.GetHtmlEmail(Mensaje, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVDPlanMejora.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), correo);
                        if (enviado == false)
                        {
                        return ("Error");
                        }
                        else
                        {
                            return ("Enviado!");
                        }
                        
                    }
                    catch (Exception e)
                    {
                        return (e.InnerException.Message);
                    }
                
            }
            return ("No Info");
        }


        public ActionResult SendPlanModal()
        {
            var vm = new SendPlanModalViewModel();
            int idplan =  Convert.ToInt32(Request.QueryString["IdPlan"]);

            int ModalidadId = Session.GetModalidadId();

            var SMPA = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT");

            
            vm.fill(CargarDatosContext(), session.GetEscuelaId(), session.GetSubModalidadPeriodoAcademicoId(),SMPA.IdSubModalidad,idplan, ModalidadId);
            

            return View(vm);

        }
        [HttpPost]
        public JsonResult SendMailCoord(string lstDoc,int IdSubModalidadPeridoAcademico, int IdCarrera, int IdPlan)
        {

            string PeriodoAcademico = (from a in Context.SubModalidadPeriodoAcademico
                                    join b in Context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                  where a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeridoAcademico
                                       select b.CicloAcademico).FirstOrDefault();

            string retorno="";
            var list = JsonConvert.DeserializeObject<List<CoordinadorNotificacionMail>>(lstDoc);

            foreach (var item in list)
            {
            string correo = item.CodCoor.ToLower() + "@upc.edu.pe";
            string idcurso = item.Idcurso.ToString();


                retorno = SendEmail(item.Idcurso, item.Correo, item.CodCurso, item.Curso.ToLower(), PeriodoAcademico, IdCarrera, IdPlan);
            }
          
            return Json(retorno);
        }
        [HttpPost]
        public JsonResult ActTabla(int IdCarrera, int IdPeridoAcademico , int IdPlan)
        {
            List<CoordinadorNotificacionMail> lstCoordinador = new List<CoordinadorNotificacionMail>();

            var GetCoordinador = CargarDatosContext().context.FuncionObtCursoCoordinador(IdPeridoAcademico, Convert.ToInt32(IdCarrera), IdPlan);

            foreach (var info in GetCoordinador)
            {

                CoordinadorNotificacionMail am = new CoordinadorNotificacionMail();
                am.Idcurso = info.idcurso;
                am.Carrera = info.Carrera;
                am.CodCurso = info.CodCurso;
                am.Curso = info.Curso;
                am.CodCoor = info.CodCoor;
                am.Correo = am.getEmail();
                am.Coordinador = info.Coordinador;
                am.IdSubModalidadPeriodoAcademico = (int)info.IdSubModalidadPeriodoAcademico;

                lstCoordinador.Add(am);
            }
            return Json(lstCoordinador);
        }


        public JsonResult ObtPlan()
        {
            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();
        
            var listplan = (from a in Context.PlanMejora
                            join b in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                            join c in Context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                            where c.IdModalidad == ModalidadId && a.IdEscuela==EscuelaId
                            select a.IdPlanMejora.ToString()).ToList();
            if (listplan.Count() > 0)
                return Json("ExistenPlanes");
            else
                return Json("NoExistenPlanes");

        }

    }
}