﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementActions;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.Docente)]
    public class ImprovementActionsController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditNewAccionMejora(Int32? IdAccionMejora)
        {
            AddEditNewAccionMejoraViewModel model = new AddEditNewAccionMejoraViewModel();
            int idModalidad = session.GetModalidadId();
            model.CargarDatos(CargarDatosContext(), IdAccionMejora, idModalidad, EscuelaId);
            return View(model);
        }

        [HttpPost]
        public JsonResult ReturntoeditIFC(Int32? IdAccionMejora)
        {
            try
            {
                bool escoordinador = false;
                var docenteid = Session.GetDocenteId();

                var IFC = (from a in Context.HallazgoAccionMejora
                           join b in Context.IFCHallazgo on a.IdHallazgo equals b.IdHallazgo
                           join c in Context.IFC on b.idIFC equals c.IdIFC
                           where a.IdAccionMejora == IdAccionMejora
                           select c).Distinct().FirstOrDefault();


                #region ES COORDINADOR

                var subarea = IFC.UnidadAcademica.UnidadAcademica2;
                var area = subarea.UnidadAcademica2;

                var DocenteArea = (from a in Context.SedeUnidadAcademica
                                   join b in Context.UnidadAcademicaResponsable on a.IdSedeUnidadAcademica equals b.IdSedeUnidadAcademica
                                   join c in Context.Docente on b.IdDocente equals c.IdDocente
                                   where a.IdUnidadAcademica == area.IdUnidadAcademica
                                   select c).Distinct().FirstOrDefault();

                var usuariodocente = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente);

                List<int> ListDocenteAreaId = new List<int>();

                ListDocenteAreaId.Add(Convert.ToInt32(DocenteArea.IdDocente));

                if (usuariodocente.IdUsuarioSecundario.HasValue)
                {
                    var DocenteId = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente).Usuario2.IdDocente;
                    ListDocenteAreaId.Add(Convert.ToInt32(DocenteId));
                }

                foreach (int iddocente in ListDocenteAreaId)
                {
                    if (iddocente == docenteid)
                    {
                        escoordinador = true;
                    }
                }
                #endregion

                if (escoordinador)
                {
                    var data = IFC.IdIFC;
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("nopertenece");
                }
            }
            catch (Exception ex)
            {
                return Json("error");
            }
        }

        public ActionResult AddNewEvidenceAM(Int32? IdAccionMejora, int Anio, string CodInstrumento)
        {
            AddNewEvidenceAMViewModel model = new AddNewEvidenceAMViewModel();
            model.CargarDatos(CargarDatosContext(), IdAccionMejora, Anio, CodInstrumento);
            return PartialView("_AddNewEvidenceAM", model);
        }

        public JsonResult AddEvidenciaPlan(List<EvidenciaForm> Listado, int IdAccionMejora)
        {
            try
            {
                foreach (var item in Listado)
                {
                    HallazgoAccionMejora ham = Context.HallazgoAccionMejora.FirstOrDefault(x => x.IdHallazgoAccionMejora == item.Id);              
                    ham.DescripcionEspanol = item.Descripcion;
                    Context.SaveChanges();
                }

                var query = (from ha in Context.HallazgoAccionMejora
                             join h in Context.Hallazgos on ha.IdHallazgo equals h.IdHallazgo
                             where ha.IdAccionMejora == IdAccionMejora
                             select ha).Distinct();

                var cantEvidencias = query.Count();
                var cantEvidenciasCargadas = query.Where(x => x.DescripcionEspanol != "").Count();

                if (cantEvidencias > 0 && cantEvidenciasCargadas == cantEvidencias)
                {
                    AccionMejora am = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);
                    am.Estado = "IMPLE";
                }

                Context.SaveChanges();

                var data = new { success = true};
                return Json(data, JsonRequestBehavior.AllowGet);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            }

            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddEvidencia(int IdHallazgoAccionMejora, string Descripcion)
        {
            try
            {
                HallazgoAccionMejora ham = Context.HallazgoAccionMejora.FirstOrDefault(x => x.IdHallazgoAccionMejora == IdHallazgoAccionMejora);
                var idAccionMejora = ham.IdAccionMejora;
                ham.DescripcionEspanol = Descripcion;
                Context.SaveChanges();                     

                var query = (from ha in Context.HallazgoAccionMejora
                             join h in Context.Hallazgos on ha.IdHallazgo equals h.IdHallazgo
                             where ha.IdHallazgoAccionMejora == IdHallazgoAccionMejora
                             select ha).Distinct();
                var cantEvidencias = query.Count();
                var cantEvidenciasCargadas = query.Where(x => x.DescripcionEspanol != "").Count();

                if (cantEvidencias > 0 && cantEvidenciasCargadas == cantEvidencias)
                {
                    AccionMejora am = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == idAccionMejora);
                    am.Estado = "IMPLE";
                }

                Context.SaveChanges();
                var data = new { success = true, texto = Descripcion };
                return Json(data, JsonRequestBehavior.AllowGet);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            }

            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddEditNewAccionMejora(AddEditNewAccionMejoraViewModel model)
        {
            try
            {
                
                string ciclo = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT").PeriodoAcademico.CicloAcademico;
                AccionMejora accionmejora = new AccionMejora();
                if (model.IdAccionMejora.HasValue)
                    accionmejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == model.IdAccionMejora);
                accionmejora.DescripcionEspanol = model.Descripcion;
                accionmejora.DescripcionIngles = model.Descripcion;
                accionmejora.IdEscuela = EscuelaId;
                accionmejora.Estado = "PENDI";
                accionmejora.ParaPlan = true;

                if (model.IdAccionMejora.HasValue == false)
                {
                    accionmejora.Anio = Int32.Parse(ciclo.Substring(0, 4)); 
                    accionmejora.Codigo = ciclo + "-A-" + (Context.AccionMejora.Max(x => x.IdAccionMejora) + 1);
                }

                if (model.IdAccionMejora.HasValue == false)
                    Context.AccionMejora.Add(accionmejora);

                if (model.IdAccionMejora.HasValue)
                {
                    List<HallazgoAccionMejora> lstham = Context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == model.IdAccionMejora).ToList();
                    Context.HallazgoAccionMejora.RemoveRange(lstham);
                }

                for (int i = 0; i < model.HallazgoId.Count; i++)
                {
                    HallazgoAccionMejora ham = new HallazgoAccionMejora();
                    ham.AccionMejora = accionmejora;
                    ham.IdHallazgo = model.HallazgoId[i];

                    Context.HallazgoAccionMejora.Add(ham);
                }


                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }
            return RedirectToAction("MantenimientoAccionesMejora", "ImprovementPlans");
        }

        public ActionResult LstAccionesParaPlan(int? Anio, int? IdCarrera, int? IdComision)
        {
            var model = new LstAccionesParaPlanViewModel();
            //SUBMODALIDADENDURO=1
            model.fill(CargarDatosContext(), EscuelaId, Anio, IdCarrera, IdComision,1);



            /* List<AccionMejora> ams = context.AccionMejora.ToList();

             for(int i = 0; i< ams.Count; i++)
             {
                 int IdAccionMejora = ams[i].IdAccionMejora;
                 AccionMejora am = context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                 String codigo = am.Codigo;


                 if(codigo.Length>=20)
                 {
                     string[] partes = codigo.Split('-');

                    System.Diagnostics.Debug.WriteLine( partes[2].Substring(0, 4));

                     am.Anio = Int32.Parse( partes[2].Substring(0, 4));

                     context.SaveChanges();

                 }
                 else
                 {
                     am.Anio = Int32.Parse(codigo.Substring(0, 4));

                     context.SaveChanges();
                 }

             }*/

            return View(model);
        }

        [HttpPost]
        public ActionResult LstAccionesParaPlan(LstAccionesParaPlanViewModel model)
        {
            return RedirectToAction("LstAccionesParaPlan", "ImprovementActions", new { Anio = model.Anio, IdCarrera = model.IdCarrera, IdComision = model.IdComision });
        }

        public ActionResult LstAccionesMejora(int? NumeroPagina, int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome)
        {
            var model = new LstAccionesMejoraViewModel();


            //IDSUBMODALIDAD=1
            model.fill(CargarDatosContext(), EscuelaId, NumeroPagina, IdConstituyente, IdInstrumento, IdCurso, IdOutcome,1);
            return View(model);
        }

        [HttpPost]
        public ActionResult LstAccionesMejora(LstAccionesMejoraViewModel model)
        {
            return RedirectToAction("LstAccionesMejora", "ImprovementActions", new { NumeroPagina = model.NumeroPagina, IdConstituyente = model.IdConstituyente, IdInstrumento = model.IdInstrumento, IdCurso = model.IdCurso, IdOutcome = model.IdOutcome });
        }
        
        public ActionResult EliminarAccionMejora(Int32 AccionMejoraId)
        {
            try
            {
                AccionMejora accionmejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == AccionMejoraId);

                accionmejora.Estado = "INA";

                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }

            return RedirectToAction("LstAccionesMejora", "ImprovementActions");
        }

        public ActionResult PrintPlanDeMejora(int Anio, int IdCarrera, int IdComision)
        {
            var viewModel = new PrintPlanDeMejoraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), Anio, IdCarrera, IdComision);

           

            Document document = new Document();
            document.SetMargins(75, 75, 40, 40);
            MemoryStream stream = new MemoryStream();
            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;
                document.Open();
                document.Add(new Paragraph("\n\n\n\n\n"));

                var fontNormal = FontFactory.GetFont("Arial", 24, Font.BOLD, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("Plan de Mejora", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                document.Add(new Paragraph("\n\n\n"));

                fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("Para la", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                string escuela = Context.Escuela.FirstOrDefault(x => x.IdEscuela == EscuelaId).Nombre;
                fontNormal = FontFactory.GetFont("Arial", 24, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph(escuela, fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                document.Add(new Paragraph("\n\n"));

                fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("De la", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);


                fontNormal = FontFactory.GetFont("Arial", 22, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("Universidad Peruana de Ciencias Aplicadas", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                fontNormal = FontFactory.GetFont("Arial", 20, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("Lima - Peru", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);


                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                imagen.WidthPercentage = 30;
                document.Add(imagen);

                document.Add(new Paragraph("\n\n\n"));

                fontNormal = FontFactory.GetFont("Arial", 18, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph(DateTime.Now.Year.ToString(), fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                document.Add(new Paragraph("\n\n"));

                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                titulo = new Paragraph("CONFIDENCIAL", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);

                document.Add(new Paragraph("\n\n"));

                fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.", fontNormal);
                titulo.Alignment = Element.ALIGN_JUSTIFIED;
                document.Add(titulo);


                document.NewPage();

                fontNormal = FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph(  "PLAN DE MEJORA " + viewModel.getAnio(), fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);


                fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("\nSe muestran a continuación las acciones de mejora dentro de los siguientes parametros:", fontNormal);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);

                fontNormal = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
                titulo = new Paragraph("\nCarrera: " +
                    viewModel.getCarreraNombre() + "\nComisión: " + viewModel.getComisionNombre(), fontNormal);
                titulo.Alignment = Element.ALIGN_LEFT;
                document.Add(titulo);

                document.Add(new Paragraph("\n", fontNormal));

                List<Constituyente> LstConstituyente = Context.Constituyente.ToList();
                //List<ConstituyenteInstrumento> lstconstituyenteinstrumento = context.ConstituyenteInstrumento.Where(x => viewModel.LstInstrumentoID.Contains(x.IdInstrumento)).ToList();


                List<HallazgoAccionMejoraPDF> lsthallazgosAccionesMejora = viewModel.Lsthams;




                for (int i = 0; i < LstConstituyente.Count; i++)
                {

                    int constituyenteID = LstConstituyente[i].IdConstituyente;
                     List<Instrumento> lstinstrumentos = Context.ConstituyenteInstrumento.Where(x => x.IdConstituyente == constituyenteID && x.Instrumento.Acronimo!="LCFC" ).Select(x => x.Instrumento).ToList();

                    List<HallazgoAccionMejoraPDF> lsthamsXInstrumento = new List<HallazgoAccionMejoraPDF>();

                    if (lsthallazgosAccionesMejora.Where(x => lstinstrumentos.Select(y => y.IdInstrumento).Contains(x.IdInstrumento)).ToList().Count == 0)
                    {
                        continue;
                    }


                    fontNormal = FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK);
                    titulo = new Paragraph("\nConstituyente: " + LstConstituyente[i].NombreEspanol, fontNormal);
                    titulo.IndentationLeft = 20;
                    titulo.Alignment = Element.ALIGN_LEFT;
                    document.Add(titulo);

                    for (int j = 0; j < lstinstrumentos.Count; j++)
                    {
                        int idinstrumento = lstinstrumentos[j].IdInstrumento;
                        lsthamsXInstrumento = lsthallazgosAccionesMejora.Where(x => x.IdInstrumento == idinstrumento).ToList();

                        if (lsthamsXInstrumento.Count == 0)
                            continue;

                        fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                        titulo = new Paragraph("\n*Instrumento: " + lstinstrumentos[j].NombreEspanol, fontNormal);
                        titulo.IndentationLeft = 30;
                        titulo.Alignment = Element.ALIGN_LEFT;
                        document.Add(titulo);

                        document.Add(Chunk.NEWLINE);


                        PdfPTable table2 = new PdfPTable(4);
                        table2.WidthPercentage = 100;
                        PdfPCell cell = new PdfPCell(new Phrase("Hallazgos", fontNormal));
                        cell.Colspan = 2;
                        cell.BackgroundColor = new BaseColor(184, 204, 228);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.PaddingBottom = 5;

                        table2.AddCell(cell);

                        cell.Phrase = new Phrase("Acciones de Mejora", fontNormal);
                        table2.AddCell(cell);

                        fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.WHITE);
                        PdfPCell cell2 = new PdfPCell(new Phrase("", fontNormal));
                        cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell2.PaddingBottom = 5;
                        cell2.Phrase = new Phrase("Código", fontNormal);
                        cell2.BackgroundColor = new BaseColor(31, 73, 125);
                        table2.AddCell(cell2);
                        cell2.Phrase = new Phrase("Descripción", fontNormal);
                        table2.AddCell(cell2);
                        cell2.Phrase = new Phrase("Código", fontNormal);
                        cell2.BackgroundColor = new BaseColor(31, 73, 125);
                        table2.AddCell(cell2);
                        cell2.Phrase = new Phrase("Descripción", fontNormal);
                        table2.AddCell(cell2);

                        fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
                        cell2.BackgroundColor = BaseColor.WHITE;

                        for (int k = 0; k < lsthamsXInstrumento.Count; k++)
                        {

                            HallazgoAccionMejoraPDF hampdf = lsthamsXInstrumento[k];
                            cell2.Rowspan = 1;
                           // Hallazgo h = lsthamsXInstrumento[k].Hallazgo;
                            cell2.Phrase = new Phrase(hampdf.hCodigo, fontNormal);
                            table2.AddCell(cell2);
                            cell2.Phrase = new Phrase(hampdf.hDescripcionEspanol, fontNormal);
                            table2.AddCell(cell2);




                            if (k > 0)
                            {
                                if (lsthamsXInstrumento[k - 1].amCodigo == hampdf.amCodigo)
                                    continue;
                            }

                            int amrowspan = lsthamsXInstrumento.Where(x => x.amCodigo == hampdf.amCodigo).ToList().Count;



                            cell2.Rowspan = amrowspan;
                            cell2.Phrase = new Phrase(hampdf.amCodigo, fontNormal);
                            table2.AddCell(cell2);
                            cell2.Phrase = new Phrase(hampdf.amDescripcionEspanol, fontNormal);
                            table2.AddCell(cell2);

                        }



                        Paragraph p2 = new Paragraph();
                        p2.IndentationLeft = 5;
                        //p.IndentationRight = 5;
                        table2.HorizontalAlignment = Element.ALIGN_LEFT;
                        p2.Add(table2);
                        document.Add(p2);



                        document.Add(new Paragraph("\n", fontNormal));

                        document.NewPage();

                    }


                }

            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "Plan de Mejora.pdf");

        }
    }
}