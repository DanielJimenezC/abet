﻿using System;
using System.Collections.Generic;
using System.Configuration.Internal;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Data.Entity;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using UPC.CA.ABET.Presentation.Controllers;
//using UPC.CA.ABET.Logic.Areas.Professor.IfcManagement.Export;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.IfcManagement.Export;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.IfcManagement;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;
using System.Transactions;
using System.Data;
using Excel;
using System.Data.Entity.Validation;
using System.Threading.Tasks;
using OfficeOpenXml;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.DataSet;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Threading;
using System.Data.SqlClient;
using UPC.CA.ABET.Logic.Areas.Admin;
using System.Text.RegularExpressions;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using System.Collections;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report;
using System.Net.Mail;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class IfcManagementController : BaseController
    {
        public string CurrentCulture { get; set; }


        public ActionResult Management()
        {
            return View();
        }

        // GET: Professor/IfcManagement
        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }

        [HttpPost]
        public ActionResult ReportStatusView(ReportStatusViewModel model)
        {
            try
            {
                string cicloacademico = model.LstCiclos.FirstOrDefault();
                int idModalidad = Session.GetModalidadId();

                int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.CicloAcademico == cicloacademico && x.SubModalidad.Modalidad.IdModalidad == idModalidad).
                     FirstOrDefault().IdSubModalidadPeriodoAcademico;


                String nombreArchivo = "ReporteEstadoIFC.xlsx";
                String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                FileInfo newFile = new FileInfo(nombreArchivo);
                ExcelPackage xlPackage = new ExcelPackage(newFile);
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");
                List<FilaReporteIFC> filas = new List<FilaReporteIFC>();

                string carrera, status;
                var languaje = model.LstCarreras.Count() > 0 ? "espanol" : "ingles";
                if (model.LstCarreras.Count() > 0)
                {
                    carrera = model.LstCarreras[0].ToString();
                    status = model.LstStatus[0].ToString().Substring(0, 3);
                }
                else
                {
                    carrera = model.LstCarrerasEn[0].ToString();
                    status = model.LstStatusEn[0].ToString().Substring(0, 3);
                }


                if (status == "PEN")
                    status = null;

                if (model.SelectedAnswer == "CARRERA" || model.SelectedAnswer == "CARRER")
                {
                    int IdCarrera = Context.Carrera.FirstOrDefault(x => x.NombreEspanol.Contains(carrera) || x.NombreIngles.Contains(carrera)).IdCarrera;


                    if (status == "TOD")
                    {
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.nombreEspanol as 'NOMBRE_CURSO' , (select distinct nombreespanol from UnidadAcademica where IdUnidadAcademica = c.IdUnidadAcademicaPadre) as 'AREA', CAR.NombreEspanol as 'CARRERA',b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + " and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera).ToList<FilaReporteIFC>();
                    }
                    else if (status == "ALL")
                    {
                        filas =
                       Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA', CAR.NombreIngles as 'CARRERA',b.Estado AS 'ESTADO'" +
                               " FROM[UnidadAcademica] a" +
                                 " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                 " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                 " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                 " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                 " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + " and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera).ToList<FilaReporteIFC>();
                    }
                    else if (status != null && languaje == "espanol")
                    {
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.nombreEspanol as 'NOMBRE_CURSO' , c.nombreespanol as 'AREA', CAR.NombreEspanol as 'CARRERA',b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera + "and B.Estado = '" + status + "'").ToList<FilaReporteIFC>();
                    }
                    else if (status != null && languaje == "ingles")
                    {
                        if (status == "APP") status = "APR";
                        if (status == "SEN") status = "ENV";
                        if (status == "WAI") status = "ESP";
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA', CAR.NombreIngles as 'CARRERA',b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera + "and B.Estado = '" + status + "'").ToList<FilaReporteIFC>();
                    }

                    else if (languaje == "espanol")
                    {
                        filas = Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.nombreEspanol as 'NOMBRE_CURSO' , d.nombreespanol as 'AREA', CAR.NombreEspanol as 'CARRERA',b.Estado AS 'ESTADO'" +
                                  " FROM[UnidadAcademica] a" +
                                    " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                    " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                    " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                    " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                    " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera + "and B.Estado is NULL").ToList<FilaReporteIFC>();
                    }
                    else
                    {
                        filas = Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA', CAR.NombreIngles as 'CARRERA',b.Estado AS 'ESTADO'" +
                                 " FROM[UnidadAcademica] a" +
                                   " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                   " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                   " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                   " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                   " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and car.IdCarrera = " + IdCarrera + "and B.Estado is NULL").ToList<FilaReporteIFC>();


                    }

                    if (languaje == "espanol")
                    {
                        worksheet.Cells["A1"].Value = "CURSO";
                    }
                    else
                    {
                        worksheet.Cells["A1"].Value = "COURSE";
                    }
                    worksheet.Cells["A1"].AutoFitColumns();
                    worksheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["A1"].Style.Font.Color.SetColor(Color.White);
                    worksheet.Cells["B1"].Value = "AREA";
                    worksheet.Cells["B1"].AutoFitColumns();
                    worksheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["B1"].Style.Font.Color.SetColor(Color.White);
                    if (languaje == "espanol")
                    {
                        worksheet.Cells["C1"].Value = "CARRERA";
                    }
                    else
                    {
                        worksheet.Cells["C1"].Value = "CARRER";

                    }
                    worksheet.Cells["C1"].AutoFitColumns();
                    worksheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["C1"].Style.Font.Color.SetColor(Color.White);
                    if (languaje == "espanol")
                    {
                        worksheet.Cells["D1"].Value = "ESTADO";
                    }
                    else
                    {
                        worksheet.Cells["D1"].Value = "STATUS";

                    }
                    worksheet.Cells["D1"].AutoFitColumns();
                    worksheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["D1"].Style.Font.Color.SetColor(Color.White);

                }
                if (model.SelectedAnswer == "AREA")
                {
                    if (status == "TOD")
                    {
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT distinct a.nombreEspanol as 'NOMBRE_CURSO' , d.nombreespanol as 'AREA', b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + " and a.nivel = 3 " + "and d.nombreespanol LIKE '%" + carrera + "%' COLLATE Latin1_General_CI_AI").ToList<FilaReporteIFC>();
                    }
                    else if (status == "ALL")
                    {
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT distinct a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA', b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + " and a.nivel = 3 " + "and CAR.NombreIngles LIKE '%" + carrera + "%'").ToList<FilaReporteIFC>();
                    }

                    else if (status != null && languaje == "espanol")
                    {
                        filas =
                         Context.Database.SqlQuery<FilaReporteIFC>("SELECT distinct a.nombreEspanol as 'NOMBRE_CURSO' , c.nombreespanol as 'AREA',b.Estado AS 'ESTADO'" +
                                   " FROM[UnidadAcademica] a" +
                                   " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                   " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                   " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                   " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                   " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                   " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and CAR.nombreespanol  LIKE '%" + carrera + "%' and B.Estado = '" + status + "' COLLATE Latin1_General_CI_AI").ToList<FilaReporteIFC>();
                    }
                    else if (status != null && languaje == "ingles")
                    {
                        if (status == "APP") status = "APR";
                        if (status == "SEN") status = "ENV";
                        if (status == "WAI") status = "ESP";
                        filas =
                        Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA',b.Estado AS 'ESTADO'" +
                                " FROM[UnidadAcademica] a" +
                                  " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                  " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                  " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                  " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                  " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                  " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and CAR.nombreespanol  LIKE '%" + carrera + "%' and B.Estado = '" + status + "'").ToList<FilaReporteIFC>();
                    }
                    else if (languaje == "espanol")
                    {
                        filas = Context.Database.SqlQuery<FilaReporteIFC>("SELECT distinct a.nombreEspanol as 'NOMBRE_CURSO' , c.nombreespanol as 'AREA', b.Estado AS 'ESTADO'" +
                                  " FROM[UnidadAcademica] a" +
                                    " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                    " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                    " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                    " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                    " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                    " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and CAR.nombreespanol  LIKE '%" + carrera + "%' COLLATE Latin1_General_CI_AI and B.Estado is NULL").ToList<FilaReporteIFC>();
                    }
                    else
                    {
                        filas = Context.Database.SqlQuery<FilaReporteIFC>("SELECT a.NombreIngles as 'NOMBRE_CURSO' , c.NombreIngles as 'AREA',b.Estado AS 'ESTADO'" +
                                 " FROM[UnidadAcademica] a" +
                                   " left join IFC b on a.idunidadacademica = b.idunidadacademica" +
                                   " left join[UnidadAcademica] c on a.idunidadacademicaPadre = c.idUnidadAcademica" +
                                   " left join[UnidadAcademica] d on c.idunidadacademicaPadre = d.idUnidadAcademica" +
                                   " left join CarreraCursoPeriodoAcademico ccpa on ccpa.IdCursoPeriodoAcademico = a.IdCursoPeriodoAcademico" +
                                   " left join Carrera CAR on CAR.IdCarrera = ccpa.IdCarrera" +
                                     " where a.IdSubModalidadPeriodoAcademico = " + IdSubModalidadPeriodoAcademico + "  and a.nivel = 3 " + "and CAR.nombreespanol  LIKE '%" + carrera + "%' and B.Estado is NULL").ToList<FilaReporteIFC>();


                    }


                    if (languaje == "espanol")
                    {
                        worksheet.Cells["A1"].Value = "CURSO";
                    }
                    else
                    {
                        worksheet.Cells["A1"].Value = "COURSE";
                    }
                    worksheet.Cells["A1"].AutoFitColumns();
                    worksheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["A1"].Style.Font.Color.SetColor(Color.White);
                    worksheet.Cells["B1"].Value = "AREA";
                    worksheet.Cells["B1"].AutoFitColumns();
                    worksheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["B1"].Style.Font.Color.SetColor(Color.White);
                    if (languaje == "espanol")
                    {
                        worksheet.Cells["C1"].Value = "ESTADO";
                    }
                    else
                    {
                        worksheet.Cells["C1"].Value = "STATUS";

                    }
                    worksheet.Cells["C1"].AutoFitColumns();
                    worksheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells["C1"].Style.Font.Color.SetColor(Color.White);


                }



                int indice = 1;
                if (filas.Count() > 0)
                {
                    foreach (FilaReporteIFC item in filas)
                    {
                        String estado = "Pendiente";
                        if (languaje == "espanol")
                        {
                            if (item.ESTADO == "ENV")
                                estado = "Enviado";
                            if (item.ESTADO == "APR")
                                estado = "Aprobado";
                            if (item.ESTADO == "ESP")
                                estado = "Espera";
                        }
                        else if (languaje == "ingles")
                        {
                            if (item.ESTADO == "ENV")
                                estado = "SENT";
                            if (item.ESTADO == "APR")
                                estado = "APPROVED";
                            if (item.ESTADO == "ESP")
                                estado = "WAIT";
                            if (item.ESTADO == "PEN")
                                estado = "PENDING";
                        }
                        indice = indice + 1;

                        if (model.SelectedAnswer == "CARRERA" || model.SelectedAnswer == "CARRER")
                        {
                            worksheet.Cells["A" + indice].Value = item.NOMBRE_CURSO;
                            worksheet.Cells["A" + indice].AutoFitColumns();
                            worksheet.Cells["A" + indice].Style.WrapText = true;
                            worksheet.Cells["B" + indice].Value = item.AREA;
                            worksheet.Cells["B" + indice].AutoFitColumns();
                            worksheet.Cells["B" + indice].Style.WrapText = true;
                            worksheet.Cells["C" + indice].Value = item.CARRERA;
                            worksheet.Cells["C" + indice].AutoFitColumns();
                            worksheet.Cells["C" + indice].Style.WrapText = true;
                            worksheet.Cells["D" + indice].Value = estado;
                            worksheet.Cells["D" + indice].AutoFitColumns();
                            worksheet.Cells["D" + indice].Style.WrapText = true;
                        }
                        else
                        {
                            worksheet.Cells["A" + indice].Value = item.NOMBRE_CURSO;
                            worksheet.Cells["A" + indice].AutoFitColumns();
                            worksheet.Cells["A" + indice].Style.WrapText = true;
                            worksheet.Cells["B" + indice].Value = item.AREA;
                            worksheet.Cells["B" + indice].AutoFitColumns();
                            worksheet.Cells["B" + indice].Style.WrapText = true;
                            worksheet.Cells["C" + indice].Value = estado;
                            worksheet.Cells["C" + indice].AutoFitColumns();
                            worksheet.Cells["C" + indice].Style.WrapText = true;
                        }

                    }
                }
                else
                {
                    indice = indice + 1;
                    if (model.SelectedAnswer == "CARRERA" || model.SelectedAnswer == "CARRER")
                    {
                        if (languaje == "espanol")
                        {
                            worksheet.Cells["A" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["A" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["A" + indice].AutoFitColumns();
                        worksheet.Cells["A" + indice].Style.WrapText = true;
                        if (languaje == "espanol")
                        {
                            worksheet.Cells["B" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["B" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["B" + indice].AutoFitColumns();
                        worksheet.Cells["B" + indice].Style.WrapText = true;

                        if (languaje == "espanol")
                        {
                            worksheet.Cells["C" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["C" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["C" + indice].AutoFitColumns();
                        worksheet.Cells["C" + indice].Style.WrapText = true;
                        worksheet.Cells["D" + indice].Value = "NO EXISTE INFORMACION"; if (languaje == "espanol")
                        {
                            worksheet.Cells["D" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["D" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["D" + indice].AutoFitColumns();
                        worksheet.Cells["D" + indice].Style.WrapText = true;
                    }
                    else
                    {
                        if (languaje == "espanol")
                        {
                            worksheet.Cells["A" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["A" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["A" + indice].AutoFitColumns();
                        worksheet.Cells["A" + indice].Style.WrapText = true;
                        if (languaje == "espanol")
                        {
                            worksheet.Cells["B" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["B" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["B" + indice].AutoFitColumns();
                        worksheet.Cells["B" + indice].Style.WrapText = true;
                        if (languaje == "espanol")
                        {
                            worksheet.Cells["C" + indice].Value = "NO EXISTE INFORMACION";
                        }
                        else
                        {
                            worksheet.Cells["C" + indice].Value = "THERE IS NO INFORMATION";
                        }
                        worksheet.Cells["C" + indice].AutoFitColumns();
                        worksheet.Cells["C" + indice].Style.WrapText = true;
                    }
                }


                var fileStream = new MemoryStream();
                xlPackage.SaveAs(fileStream);
                fileStream.Position = 0;

                var fsr = new FileStreamResult(fileStream, contentType);
                fsr.FileDownloadName = nombreArchivo;

                return fsr;
            }
            catch (IOException ioe)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("Management", "IfcManagement");
            }
        }

        private class AreaListAux {
            public string NombreIngles { get; set; }
            public string NombreEspanol { get; set; }
            public int? IdUnidadAcademica { get; set; }
        }

        public JsonResult GetAreasXAlumno(string periodoAcademicoID)
        {
            // var IdAlumnoMatriculado = context.AlumnoMatriculado.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdAlumnoMatriculado;

            /* var cursos = (from cpa in context.CursoPeriodoAcademico
                           join c in context.Curso on cpa.IdCurso equals c.IdCurso
                           where cpa.IdPeriodoAcademico == IdPeriodoAcademico && c.NombreEspanol.Trim() != ""
                           select c).OrderBy(x => x.NombreEspanol).ToList();*/

            int idEscuela = Session.GetEscuelaId();
            int idmoda = Session.GetModalidadId();
            int submodaactivo;

            if (String.IsNullOrEmpty(periodoAcademicoID))
                submodaactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == idmoda).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            else
                submodaactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.CicloAcademico == periodoAcademicoID && x.SubModalidad.Modalidad.IdModalidad == idmoda).FirstOrDefault().IdSubModalidadPeriodoAcademico;



            var data = new List<SelectListItem>();

            

            var language = Session.GetCulture().ToString();
            ArrayList arrAreas = new ArrayList();
            AreaListAux areasAux = new AreaListAux();
            if (language == "en-US")
            {
                var lstArea = (from u in Context.UnidadAcademica
                               where u.Tipo == "AREA" && u.IdSubModalidadPeriodoAcademico == submodaactivo && u.IdEscuela == idEscuela
                               select new { u.NombreIngles, u.IdUnidadAcademica }).Distinct().ToList();

                areasAux.NombreIngles = "There are no areas for the academic period";
                areasAux.IdUnidadAcademica = null;
                arrAreas.Add(areasAux);

                if (lstArea.Count > 0)
                    return Json(lstArea, JsonRequestBehavior.AllowGet);
                else
                    return Json(new ArrayList { areasAux }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstArea = (from u in Context.UnidadAcademica
                               where u.Tipo == "AREA" && u.IdSubModalidadPeriodoAcademico == submodaactivo && u.IdEscuela == idEscuela
                               select new { u.NombreEspanol, u.IdUnidadAcademica }).Distinct().ToList();

                areasAux.NombreEspanol = "No existe Areas para el periodo academico";
                areasAux.IdUnidadAcademica = null;
                arrAreas.Add(areasAux);

                if (lstArea.Count > 0)
                    return Json(lstArea, JsonRequestBehavior.AllowGet);
                else
                    return Json(new ArrayList {areasAux} , JsonRequestBehavior.AllowGet);
            }




        }
        public ActionResult ReportResultadosFDC(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            var model = new ReportResultadosFDCViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess);
            model.fileName = fileName ?? "";
            return View(model);
        }

        public ActionResult ListaNoIFCCorreo(Int32? PeriodoAcademico = 0) {
            var model = new ListaNoIFCCorreoViewModel();
            model.CargarDatos(CargarDatosContext(), PeriodoAcademico);
            model.IdPeriodoAcademico = PeriodoAcademico;
            return View(model);
        }

        [HttpPost]
        public JsonResult SendEmail(Int32? PeriodoAcademico) {
            var model = new ListaNoIFCCorreoViewModel();
            try
            {
                model.CargarDatos(CargarDatosContext(), PeriodoAcademico);
                CorreoOrganigrama correoOrganigrama = new CorreoOrganigrama();

                var IdSubModalidadPeriodoAcademico = CargarDatosContext().context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == PeriodoAcademico).IdSubModalidadPeriodoAcademico;

                //AREA
                var queryArea = CargarDatosContext().context.USP_SEL_Organigrama_Nivel(IdSubModalidadPeriodoAcademico, 0).ToList();

                correoOrganigrama.Email = queryArea.FirstOrDefault().CorreDocentePadre;
                correoOrganigrama.NombreDirector = queryArea.FirstOrDefault().Padre;
                correoOrganigrama.NombreResponsable = queryArea.FirstOrDefault().NombrePadre;
                foreach (var Area in queryArea)
                {
                    correoOrganigrama.Areas.Add(new AreaCorreoOrganigrama
                    {
                        Email = Area.CorreoDocenteHijo,
                        NombreArea = Area.Hijo,
                        NombreResponsable = Area.NombreHijo,
                        SubAreas = new List<SubAreaCorreoOrganigrama>()
                    });
                }

                //SUBAREA
                var querySubArea = CargarDatosContext().context.USP_SEL_Organigrama_Nivel(IdSubModalidadPeriodoAcademico, 1).ToList();
                foreach (var SubArea in querySubArea)
                {
                    foreach (var item in correoOrganigrama.Areas)
                    {
                        if (item.NombreArea == SubArea.Padre)
                        {
                            item.SubAreas.Add(new SubAreaCorreoOrganigrama
                            {
                                Email = SubArea.CorreoDocenteHijo,
                                NombreSubArea = SubArea.Hijo,
                                NombreResponsable = SubArea.NombreHijo,
                                Cursos = new List<CursoCorreoOrganigrama>()
                            });
                        }
                    }
                }

                //CURSO
                var queryCurso = CargarDatosContext().context.USP_SEL_Organigrama_Nivel(IdSubModalidadPeriodoAcademico, 2).ToList();
                foreach (var Curso in queryCurso)
                {
                    foreach (var area in correoOrganigrama.Areas)
                    {
                        foreach (var subarea in area.SubAreas)
                        {
                            if (subarea.NombreSubArea == Curso.Padre)
                            {
                                subarea.Cursos.Add(new CursoCorreoOrganigrama
                                {
                                    Email = Curso.CorreoDocenteHijo,
                                    NombreResponsable = Curso.NombreHijo,
                                    CodigoCurso = Curso.CodigoCurso,
                                    NombreCurso = Curso.Hijo
                                });
                            }
                        }
                    }
                }

                //var correoCursos = CargarDatosContext().context.USP_SEL_IFCNoRegistrado_v2(IdSubModalidadPeriodoAcademico).ToList();

                var fromAddress = new MailAddress("abetdev01@gmail.com");
                const string fromPassword = "ABET2018";
                var smtp = new SmtpClient
                {
                    //Host = "smtp.live.com",
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                var templateCorreoDirector = 
                    $"Estimada @NOM_DIR@<br/>" +
                    $"La siguiente lista muestra los cursos que no tienen IFC:<br/>" +
                    $"<ul>@LIST_AREAS@</ul>" +
                    $"Atentamente,<br/>" +
                    $"Equipo del Sistema ABET";

                var templateCorreoArea =
                    $"Estimado Coordinador de área de @NOM_AREA@<br/>" +
                    $"La siguiente lista muestra los cursos que no tienen IFC:<br/>" +
                    $"<ul>@LIST_SUBAREAS@</ul>" +
                    $"Atentamente,<br/>" +
                    $"Equipo del Sistema ABET<br/>";

                var templateCorreoSubArea = 
                    $"Estimado Coordinador de subárea de @NOM_SUBAREA@,<br/>" +
                    $"La siguiente lista muestra los cursos que no tienen IFC:<br/>" +
                    $"<ul>@LIST_CURSOS@</ul>" +
                    $"Atentamente,<br/>" +
                    $"Equipo del Sistema ABET<br/>";



                var templateCorreoCurso =
                    $"Estimado(a) Docente,<br/>" +
                    $"Se envía este correo para hacerle recordar registrar su Informe de Fin de Ciclo del siguiente Curso:<br/>" +
                    $"@COD_CURSO@ - @NOM_CURSO@<br/>" +                    
                    $"Atentamente,<br/>" +
                    $"Equipo del Sistema ABET";

                var body = "";
                var asunto = "";
                var correoDestino = "";

                var listaCursoDirector = "";
                foreach (var Area in correoOrganigrama.Areas)
                {
                    var listaCursoArea = "";
                    foreach (var SubArea in Area.SubAreas)
                    {                        
                        var listaCursoSubArea = "";
                        foreach (var Curso in SubArea.Cursos)
                        {
                            //ENVIO CORREO A COORDINADORES DE CURSO
                            body = templateCorreoCurso.Replace("@COD_CURSO@", Curso.CodigoCurso).Replace("@NOM_CURSO@", Curso.NombreCurso);
                            listaCursoSubArea += $"<li>{Curso.CodigoCurso} - {Curso.NombreCurso} - {Curso.NombreResponsable}</li>";
                            asunto = "Curso sin IFC - " + Curso.Email;
                            correoDestino = "abetdev01@outlook.com"; //Curso.Email;
                            SendEmailMethod(smtp, fromAddress, correoDestino, asunto, body);
                        }
                        if (SubArea.Cursos.Count() != 0)
                        {
                            listaCursoArea += $"<li>Subárea: {SubArea.NombreSubArea}</li><ul>@LIST_CURSOS@</ul>";
                            listaCursoArea = listaCursoArea.Replace("@LIST_CURSOS@", listaCursoSubArea);

                            //ENVIO CORREO A COORDINADOR DE SUBAREA
                            asunto = "Curso del subárea sin IFC - " + SubArea.Email;
                            correoDestino = "abetdev01@outlook.com"; //SubArea.Email;
                            body = templateCorreoSubArea.Replace("@LIST_CURSOS@", listaCursoSubArea).Replace("@NOM_SUBAREA@",SubArea.NombreSubArea); ;
                            SendEmailMethod(smtp, fromAddress, correoDestino, asunto, body);
                        }
                    }
                    if (listaCursoArea !="")
                    {
                        listaCursoDirector += $"<li>Área: {Area.NombreArea}</li><ul>@LIST_CURSOS@</ul>";
                        listaCursoDirector = listaCursoDirector.Replace("@LIST_CURSOS@", listaCursoArea);

                        //ENVIO CORREO A COORDINADOR DE AREA
                        asunto = "Curso del área sin IFC - " + Area.Email;
                        correoDestino = "abetdev01@outlook.com"; //Area.Email;
                        body = templateCorreoArea.Replace("@LIST_SUBAREAS@", listaCursoArea).Replace("@NOM_AREA@", Area.NombreArea); ;
                        SendEmailMethod(smtp, fromAddress, correoDestino, asunto, body);
                    }
                }

                if (listaCursoDirector !="")
                {
                    //ENVIO CORREO A DIRECTOR(A)
                    asunto = "Cursos EISC sin IFC - " + correoOrganigrama.Email;
                    correoDestino = "abetdev01@outlook.com"; //Area.Email;
                    body = templateCorreoDirector.Replace("@LIST_AREAS@", listaCursoDirector).Replace("@NOM_DIR@", correoOrganigrama.NombreDirector); ;
                    SendEmailMethod(smtp, fromAddress, correoDestino, asunto, body);
                }
                
            }
            catch (Exception ex)
            {
                return Json("No se pudo enviar el correo", JsonRequestBehavior.AllowGet);
                throw;
            }
            

            return Json("Correo Enviado", JsonRequestBehavior.AllowGet);
        }

        public bool SendEmailMethod(SmtpClient smtp, MailAddress fromAddress, string correoDestino, string Asunto, string cuerpoMensaje) {
            try
            {
                var toAddress = new MailAddress(correoDestino); //data.CorreoDocente;
                string subject = Asunto;
                string body = cuerpoMensaje;//  data.CodigoCurso + " - " + data.CursoEspanol;

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }            
        }


            public JsonResult GetCursosXAlumno()
        {
            // var IdAlumnoMatriculado = context.AlumnoMatriculado.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdAlumnoMatriculado;

            /* var cursos = (from cpa in context.CursoPeriodoAcademico
                           join c in context.Curso on cpa.IdCurso equals c.IdCurso
                           where cpa.IdPeriodoAcademico == IdPeriodoAcademico && c.NombreEspanol.Trim() != ""
                           select c).OrderBy(x => x.NombreEspanol).ToList();*/

            int idmoda = Session.GetModalidadId();

            var submodaactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == idmoda).
                FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var data = new List<SelectListItem>();




            var LstCarreras = (from cpp in Context.CarreraPeriodoAcademico
                               join c in Context.Carrera on cpp.IdCarrera equals c.IdCarrera
                               where cpp.IdSubModalidadPeriodoAcademico == submodaactivo
                               select new { c.NombreEspanol, c.IdCarrera }).Distinct().ToList();


            return Json(LstCarreras, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ReportStatusView()
        {
            var viewmodel = new ReportStatusViewModel();
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetPeriodoAcademicoId(), Session.GetEscuelaId());
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            int sessionIdEscuela = Session.GetEscuelaId();
            var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();

            int idmoda = Session.GetModalidadId();



            var submodaactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == idmoda).
                FirstOrDefault().IdSubModalidadPeriodoAcademico;



            viewmodel.CargarDatos(CargarDatosContext(), submodaactivo, idmoda,sessionIdEscuela);
            return View(viewmodel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult Consult(bool? isSearchRequest, string idAreaUnidadAcademica, int? IdPeriodoAcademico, string idCursoUnidadAcademica, string idSubareaUnidadAcademica, int? idSede, string estado)
        {

            //recibe idperiodoacademico correcto
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;




            var vm = new ConsultIfcViewModel();
            vm.Language = this.CurrentCulture;
            var modalityId = Session.GetModalidadId();
            var areasFromServices = GedServices.GetAllAreasServices(Context, EscuelaId, modalityId);

            //Filtros
            if (isSearchRequest == null)
            {
                var areas = areasFromServices.Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
                vm.AvailableAreas = areas.Select(o => new SelectListItem { Value = o, Text = o });     
                
                var subareas = GedServices.GetAllSubareasServices(Context, EscuelaId, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
                vm.AvailableSubareas = subareas.Select(o => new SelectListItem { Value = o, Text = o });

                var cursos = GedServices.GetAllCursosServices(Context, EscuelaId, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().OrderBy(x => x);
                vm.AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });
            } 
            else
            {
                int idsubmoda = 0;
                if (IdPeriodoAcademico != null)
                {
                    idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                }
                
                //Filtro de Areas
                if (IdPeriodoAcademico != null) {
                    var areas = GedServices.GetAllAreasForPeriodoAcademico(Context, idsubmoda, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper()).Distinct();
                    vm.AvailableAreas = areas.Select(o => new SelectListItem { Value = o, Text = o });
                }
                else {
                    var areas = areasFromServices.Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
                    vm.AvailableAreas = areas.Select(o => new SelectListItem { Value = o, Text = o });
                }

                //Filtros de SubAreas
                if (idAreaUnidadAcademica != null)
                {
                    var subareas = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.NombreIngles) == idAreaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 2)
                        .Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct();
                    vm.AvailableSubareas = subareas.Select(o => new SelectListItem { Value = o, Text = o });
                }
                else {
                    var subareas = GedServices.GetAllSubareasServices(Context, EscuelaId, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
                    vm.AvailableSubareas = subareas.Select(o => new SelectListItem { Value = o, Text = o });
                }

                //Filtros de Cursos
                if (idSubareaUnidadAcademica != null)
                {
                    var cursos = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.NombreIngles) == idSubareaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).
                        Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct().OrderBy(x => x);

                    vm.AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });
                }
                else {
                    //buscar cursos por area
                    if (idAreaUnidadAcademica != null)
                    {
                        var cursos = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.UnidadAcademica2.NombreIngles) == idAreaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).
                            Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct().OrderBy(x => x);
                        vm.AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });
                    }
                    else {
                        var cursos = GedServices.GetAllCursosServices(Context, EscuelaId, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().OrderBy(x => x);
                        vm.AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });
                    }
                }
            }
            //MODIFICADO GETALLMYPERIODOSACADEMICOS
            vm.AvailablePeriodoAcademicos = GedServices.GetAllMyPeriodosAcademicosServices(Context, @Session.GetModalidadId())
                .Select(o => new SelectListItem { Value = o.IdPeriodoAcademico.ToString(), Text = o.CicloAcademico }).OrderByDescending(x => x.Text);
            //*************

            vm.AvailableEstados = GedServices.GetAllEstadosIfcServices(CurrentCulture)
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                int idsubmoda = 0;

                if (IdPeriodoAcademico != null)
                {
                    idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                }

                //idsubmodalidadperiodoacademico incorrecto
                vm.Resultados = GenerarListaIfcResultViewModel(GedServices.GetIfcsServices(Context, idAreaUnidadAcademica, idSubareaUnidadAcademica, idsubmoda, idCursoUnidadAcademica, estado, EscuelaId));
                if (vm.Resultados == null || !vm.Resultados.Any())
                {
                    vm.Resultados = new List<IfcResultViewModel>();
                    PostMessage(MessageType.Info, ControllerIFCManagementResource.NoResultados);

                }

            }

            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Docente)]
        public ActionResult Create(int? idCursoUnidadAcademica, int? idPeriodoAcademico)
        {

            int idSubModalidadPeriodoAcademico = (int)Session.GetSubModalidadPeriodoAcademicoId();
            /////////////////////////////////////////////////////

            ////////////////////////////////////////////////////
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;



            if (SessionHelper.GetDocenteId(Session) == null)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoTienePrivilegios);
                return RedirectToAction("Consult");
            }

            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            //var DocentesNivel3;
            var docenteNivel3 = Context.SedeUnidadAcademica
                .Where(x => x.UnidadAcademica.Nivel == 3)
                .SelectMany(y => y.UnidadAcademicaResponsable)
                .Select(e => e.Docente)
                .Where(t => t.IdDocente == idDocenteSesion).ToList();

            if (docenteNivel3.Count == 0)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoEsCoordinadorDeCurso);
                return RedirectToAction("Consult");
            }

            //AQUI SE UTILIZA GETPERIODOFORIFC USARE LOS PERIODOS DEL GETMYALLPERIODOS

            var vm = new CreateIfcViewModel
            {
                AvailablePeriodoAcademicos = GedServices.GetAllMyPeriodosAcademicosServices(Context, @Session.GetModalidadId())
                            .Select(c => new SelectListItem { Value = c.IdPeriodoAcademico.ToString(), Text = c.CicloAcademico }).OrderByDescending(x => x.Text),

                /*
                AvailableAreas = GedServices.GetAllAreasServices(context, escuelaId).
                Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().Select(o => new SelectListItem { Value = o, Text = o }),

                AvailableSubareas =  GedServices.GetAllSubareasServices(context, escuelaId).
                Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().Select(o => new SelectListItem { Value = o, Text = o }),

                AvailableCursos = GedServices.GetAllCursosServices(context, escuelaId).
                Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().Select(o => new SelectListItem { Value = o, Text = o })
                */
            };
            if (idCursoUnidadAcademica != null)
            {
                var idSubModalidadPorObtener = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                int idSubModalidadPeriodoAcademicoActual = idSubModalidadPorObtener;
                var unidadAcademica = GedServices.GetUnidadAcademicaServices(Context, (int)idCursoUnidadAcademica);
                var ifc = GedServices.GetIfcByUnidadAcademicaServices(Context, unidadAcademica.IdUnidadAcademica);
                if (ifc != null)
                {
                    PostMessage(MessageType.Warning, @"" + ControllerIFCManagementResource.ExisteIFC + " <a href='" + Url.Action("View", new { id = ifc.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
                    return RedirectToAction("Consult");
                }
                var subarea = unidadAcademica.UnidadAcademica2;
                var area = subarea.UnidadAcademica2;
                var curso = unidadAcademica.CursoPeriodoAcademico.Curso;
                var currentPeriodoAcademico = Context.PeriodoAcademico.First(x => x.IdPeriodoAcademico == idPeriodoAcademico); //GedServices.GetCurrentPeriodoAcademicoServices(context);
                //var currentSubModalidadAcademica = context.SubModalidadPeriodoAcademico.First(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId);
                vm.NombreArea = area.NombreEspanol;
                vm.IdCurso = curso.IdCurso;
                vm.IdCursoUnidadAcademica = unidadAcademica.IdUnidadAcademica;


                var IdSubModalidadPeriodoAcademico = (from s in Context.SubModalidadPeriodoAcademico
                                                      where s.IdPeriodoAcademico == idPeriodoAcademico
                                                      select s.IdSubModalidadPeriodoAcademico).FirstOrDefault();

                var carreraComisionOutcome = CargarDatosContext().context.UnidadAcademicaResponsable
                         .Where(x => x.IdDocente == idDocenteSesion).Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                         .Where(x => x.Nivel == 3 && x.IdUnidadAcademica == idCursoUnidadAcademica).Select(x => x.CursoPeriodoAcademico)
                         .Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdCurso == curso.IdCurso)
                         .Select(x => x.Curso.CursoMallaCurricular).FirstOrDefault().SelectMany(x => x.MallaCocosDetalle)
                         .Where(x => x.MallaCocos.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).ToList()
                         .Select(c => new CarreraComisionOutcome
                         {
                             carrera = c.MallaCocos.CarreraComision.Carrera,
                             comision = c.OutcomeComision.Comision,
                             outcome = c.OutcomeComision.Outcome,
                         }).ToList();
                vm.ResultadoLogroCurso = GetResultadoLogrosCurso(carreraComisionOutcome);
                vm.ResultadoAlcanzado = GetResultadoAlcanzado(GedServices.GetCursoMallaCurricularForIfcServices(Context, curso.IdCurso, idSubModalidadPeriodoAcademicoActual).LogroFinCicloEspanol);
                vm.Sedes = GedServices.GetSedesForIfcServices(Context, idDocenteSesion, (int)idCursoUnidadAcademica, idSubModalidadPeriodoAcademicoActual, EscuelaId);

                string cicloAcademico = (from a in Context.PeriodoAcademico
                                         where a.IdPeriodoAcademico == idPeriodoAcademico
                                         select a.CicloAcademico).FirstOrDefault();


                vm.AccionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(Context, ModalidadId, (int)idCursoUnidadAcademica, cicloAcademico, EscuelaId);
                vm.CodigoCurso = curso.Codigo;

                vm.Idioma = this.CurrentCulture;
                vm.Criticidades = Context.Criticidad.ToList();

                vm.CicloAcademico = currentPeriodoAcademico.CicloAcademico;
                vm.Titulo = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                    "IFC - " + area.NombreEspanol + " - " + subarea.NombreEspanol + " - " + curso.NombreEspanol + " - " + currentPeriodoAcademico.CicloAcademico :
                    "IFC - " + area.NombreIngles + " - " + subarea.NombreIngles + " - " + curso.NombreIngles + " - " + currentPeriodoAcademico.CicloAcademico;
                vm.Hallazgos = new List<HallazgoViewModel>();
                vm.AccionesPropuestas = new List<AccionMejoraViewModel>();
                vm.IdDocente = idDocenteSesion;
                vm.Observaciones = null;
                vm.IdPeriodoAcademico = idPeriodoAcademico.Value;
                foreach (var accionp in vm.AccionesPrevias)
                {
                    accionp.Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accionp.Estado);
                }
                vm.IdSubModalidad = idSubModalidadPeriodoAcademico;
            }

            return View(vm);
        }

        public JsonResult UpdateTableEvidence(int? idCursoUnidadAcademica, int? idPeriodoAcademico)
        {
            string cicloAcademico = (from a in Context.PeriodoAcademico
                                     where a.IdPeriodoAcademico == idPeriodoAcademico
                                     select a.CicloAcademico).FirstOrDefault();

            var AccionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(Context, ModalidadId, (int)idCursoUnidadAcademica, cicloAcademico, EscuelaId);
            foreach (var accionp in AccionesPrevias)
            {
                accionp.Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accionp.Estado);
            }
            return Json(AccionesPrevias, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Docente)]
        public async Task<ActionResult> Create(CreateIfcViewModel vm)
        {
            try
            {
                int idPeriodoAcademicoActual = vm.IdPeriodoAcademico;
                //int SubModalidadPeriodoAcademicoId = vm.IdSubModalidadPeriodoAcademico;

                var ifc = CrearIfc(vm, idPeriodoAcademicoActual);

                // bool success = await GedServices.CrearIfcServices(Context, ifc);
                bool success = false;
                if (ifc.IdIFC != 0) success = true;
                //Thread.Sleep(30000);
                if (success)
                {
                    PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeCreoIFC + " <a href='" + Url.Action("View", new { id = ifc.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
                }
                else
                {
                    PostMessage(MessageType.Error, ControllerIFCManagementResource.ErrorCrearIFC);
                }

                var ruta = Url.Action("Consult", "IfcManagement");
                return Json(ruta);
            }
            catch (Exception ex) {
                
                throw new Exception(ex.Message);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Docente)]
        public ActionResult Edit(int id)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var ifc = GedServices.GetIfcServices(Context, id);


            int? idDocenteSesion = (int?)SessionHelper.GetDocenteId(Session);


            var unidadAcademicaX = ifc.UnidadAcademica;
            var subModalidadPeriodoAcademicoX = ifc.UnidadAcademica.CursoPeriodoAcademico.SubModalidadPeriodoAcademico;
            var idSubModalidadPeriodoAcademicoX = subModalidadPeriodoAcademicoX.IdSubModalidadPeriodoAcademico;
            var idSubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademicoX).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;
            var cursoX = GedServices.GetCursoServices(Context, ifc.UnidadAcademica.CursoPeriodoAcademico.IdCurso);
            var carreraX = GedServices.GetAreaForCursoServices(Context, ifc.UnidadAcademica.IdUnidadAcademica);
            var subareaX = unidadAcademicaX.UnidadAcademica2;
            var areaX = subareaX.UnidadAcademica2;
            bool esSuperiorSubarea = false;
            bool esSuperiorArea = false;

            //VALIDAR SI ES COORDINADOR DE AREA

            #region ES COORDINADOR

            var DocenteArea = (from a in Context.SedeUnidadAcademica
                               join b in Context.UnidadAcademicaResponsable on a.IdSedeUnidadAcademica equals b.IdSedeUnidadAcademica
                               join c in Context.Docente on b.IdDocente equals c.IdDocente
                               where a.IdUnidadAcademica == areaX.IdUnidadAcademica
                               select c).Distinct().FirstOrDefault();
            var usuariodocente = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente);

            List<int> ListDocenteAreaId = new List<int>();

            ListDocenteAreaId.Add(Convert.ToInt32(DocenteArea.IdDocente));

            if (usuariodocente.IdUsuarioSecundario.HasValue)
            {
                var DocenteId = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente).Usuario2.IdDocente;
                ListDocenteAreaId.Add(Convert.ToInt32(DocenteId));
            }
            foreach (int docenteid in ListDocenteAreaId)
            {
                if (idDocenteSesion == docenteid)
                {
                    esSuperiorArea = true;

                }
            }
            #endregion

            if (ifc.IdDocente != idDocenteSesion || true)
            {
                var sedesDocente = (from uar in Context.UnidadAcademicaResponsable
                                    join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                        sua.IdSedeUnidadAcademica
                                    join s in Context.Sede on sua.IdSede equals s.IdSede
                                    where sua.IdUnidadAcademica == ifc.IdUnidadAcademica
                                    && uar.IdDocente == ifc.IdDocente
                                    select s);
                var sedesSubarea = (from uar in Context.UnidadAcademicaResponsable
                                    join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                        sua.IdSedeUnidadAcademica
                                    join s in Context.Sede on sua.IdSede equals s.IdSede
                                    where sua.IdUnidadAcademica == subareaX.IdUnidadAcademica
                                          && uar.IdDocente == idDocenteSesion
                                    select s);
                var interseccionSubarea = sedesDocente.Intersect(sedesSubarea).ToList();
                if (interseccionSubarea.Count > 0)
                    esSuperiorSubarea = true;

                var sedesArea = (from uar in Context.UnidadAcademicaResponsable
                                 join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                     sua.IdSedeUnidadAcademica
                                 join s in Context.Sede on sua.IdSede equals s.IdSede
                                 where sua.IdUnidadAcademica == areaX.IdUnidadAcademica
                                     && uar.IdDocente == idDocenteSesion
                                 select s);
                var interseccionArea = sedesDocente.Intersect(sedesArea).ToList();
                if (interseccionArea.Count > 0)
                    esSuperiorArea = true;
            }

            if (ifc == null)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoSeEncontroIFC);
                return RedirectToAction("Consult");
            }

            var esCoordinador = false;

            if ((esSuperiorArea == false && esSuperiorSubarea == false) && ifc.IdDocente != idDocenteSesion)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoPuedeEditarIFCdeOTRO);
                return RedirectToAction("Consult");
            }
            else
            {
                esCoordinador = true;
            }

            if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO && esCoordinador == false)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoPuedeEditarIFCEnviado);
                return RedirectToAction("Consult");
            }
            int idCursoUnidadAcademica = ifc.IdUnidadAcademica;
            var unidadAcademica = ifc.UnidadAcademica;
            var subModalidadPeriodoAcademico = unidadAcademica.SubModalidadPeriodoAcademico;
            var currentDate = DateTime.Now;
            var fechaLimite = ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.FechaFinPeriodo;
            var idSubModalidadPeriodoAcademico = subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
            var curso = unidadAcademica.CursoPeriodoAcademico.Curso;
            var subarea = unidadAcademica.UnidadAcademica2;
            var area = subarea.UnidadAcademica2;
            var hallazgosZ = GedServices.GetHallazgosZForIfcServices(Context, ifc.IdIFC);
            var hallazgosVm = new List<HallazgoViewModel>();

            foreach (var hallazgoZZ in hallazgosZ)
            {
                bool puedeeditar = true;

                var IdSubModPerAcad = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == hallazgoZZ.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                var auxCodigo = hallazgoZZ.Codigo + '-' + hallazgoZZ.Identificador.ToString();

                var lsthallazgoaccionmejora = Context.HallazgoAccionMejora.Where(x => x.IdHallazgo == hallazgoZZ.IdHallazgo).ToList();

                foreach (var hallazgoaccionmejora in lsthallazgoaccionmejora)
                {
                    var listparaplan = Context.PlanMejoraAccion.Where(x => x.IdAccionMejora == hallazgoaccionmejora.IdAccionMejora).ToList();

                    if (listparaplan.Count() > 0)
                    {
                        puedeeditar = false;
                    }
                    else
                    {
                        puedeeditar = true;
                    }
                }

                var hallazgoVm = new HallazgoViewModel
                {
                    IdHallazgo = hallazgoZZ.IdHallazgo,
                    Codigo = auxCodigo,
                    Descripcion = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? hallazgoZZ.DescripcionEspanol : hallazgoZZ.DescripcionIngles,
                    IdPeriodoAcademico = IdSubModPerAcad,
                    IdSubmodalidadPeriodoAcademicoModulo = hallazgoZZ.IdSubModalidadPeriodoAcademicoModulo,
                    Criticidad = hallazgoZZ.Criticidad.NombreEspanol,
                    Identificador = hallazgoZZ.Identificador,
                    PuedeEditar = puedeeditar
                };

                var outcomestr = "";

                foreach (var ifcoutcome in hallazgoZZ.IFCHallazgoOutcome)
                {
                    var substring = "";
                    var outcome = Context.Outcome.Where(x => x.IdOutcome == ifcoutcome.IdOutcome).ToList().FirstOrDefault();
                    if (outcomestr == "")
                    {
                        substring = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? outcome.DescripcionEspanol : outcome.DescripcionIngles;
                        outcomestr += outcome.Nombre + " - " + substring.Substring(0, 40) + "...";
                    }   
                    else
                    {
                        substring = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? outcome.DescripcionEspanol : outcome.DescripcionIngles;
                        outcomestr += "\n" + outcome.Nombre + " - " + substring.Substring(0, 40) + "...";
                    }
                }
                hallazgoVm.Outcome = outcomestr;

                hallazgosVm.Add(hallazgoVm);
            }

            var accionesMejoraZ = GedServices.GetAccionMejorasForIfcServicesZ(Context, ifc.IdIFC);
            var accionesMejoraVm = new List<AccionMejoraViewModel>();
            foreach (var accionMejora in accionesMejoraZ)
            {
                var identificadorH = (from am in Context.AccionMejora
                                      join ham in Context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                                      join h in Context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                                      where am.IdAccionMejora == accionMejora.IdAccionMejora
                                      select h.Identificador).FirstOrDefault();

                bool perteceneaplan = false;

                var plan = Context.PlanMejoraAccion.Where(x => x.IdAccionMejora == accionMejora.IdAccionMejora).ToList();

                if (plan.Count() > 0)
                {
                    perteceneaplan = true;
                }
                else
                {
                    perteceneaplan = false;
                }
                var accionMejoraVm = new AccionMejoraViewModel
                {
                    Codigo = accionMejora.Codigo + '-' + accionMejora.Identificador.ToString(),
                    Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accionMejora.Estado),
                    Descripcion = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? accionMejora.DescripcionEspanol : accionMejora.DescripcionIngles,
                    CodigoHallazgo = accionMejora.HallazgoAccionMejora.First().Hallazgos.Codigo + '-' + accionMejora.HallazgoAccionMejora.First().Hallazgos.Identificador.ToString(),
                    IdAccionMejora = accionMejora.IdAccionMejora,
                    IdHallazgo = accionMejora.HallazgoAccionMejora.First().Hallazgos.IdHallazgo,
                    Enplan = perteceneaplan
                };

                accionesMejoraVm.Add(accionMejoraVm);
            }


            var vm = new CreateIfcViewModel
            {
                NombreArea = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? area.NombreEspanol : area.NombreIngles,
                IdCurso = curso.IdCurso,
                IdIfc = ifc.IdIFC,
                IdCursoUnidadAcademica = ifc.IdUnidadAcademica,
                Estado = GedServices.GetTextoForCodigoEstadoIfcServices(ifc.Estado, CurrentCulture),
                FechaCreacion = ifc.FechaCreacion,
                CodigoCurso = curso.Codigo,
                Sedes = GedServices.GetSedesForIfcServices(Context, (int)ifc.IdDocente, idCursoUnidadAcademica, idSubModalidadPeriodoAcademico, EscuelaId),
                CicloAcademico = subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                Titulo = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                "IFC - " + area.NombreEspanol + " - " + subarea.NombreEspanol + " - " + curso.NombreEspanol + " - " + subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico :
                "IFC - " + area.NombreIngles + " - " + subarea.NombreIngles + " - " + curso.NombreIngles + " - " + subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                AccionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(Context, ModalidadId, idCursoUnidadAcademica, subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, EscuelaId),
                Hallazgos = hallazgosVm,
                AccionesPropuestas = accionesMejoraVm,
                Infraestructura = ifc.Infraestructura,
                ApreciacionAlumnos = ifc.AprecioacionAlumnos,
                ApreciacionDelegado = ifc.ApreciacionDelegado,
                ComentarioEncuesta = ifc.ComentarioEncuesta,
                IdDocente = (int)ifc.IdDocente,
                IdAreaUnidadAcademica = area.IdUnidadAcademica,
                ResultadoLogroCurso = ifc.ResultadoLogroCurso,
                ResultadoAlcanzado = ifc.ResultadoAlcanzado,
                Observaciones = ifc.Observaciones,
                IdConstituyenteInstrumento = 1,
                IdSubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo,
                EsCoordinador = esSuperiorArea,
                IdSubModalidad = subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico
            };

            foreach (var accionp in vm.AccionesPrevias)
            {
                accionp.Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accionp.Estado);
            }

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Docente)]
        public ActionResult Edit(CreateIfcViewModel vm)
        {
            IFC ifcActual = Context.IFC.FirstOrDefault(x => x.IdIFC == vm.IdIfc);
            bool success = false;
            success = ActualizarIfc(ifcActual, vm);

            if (success && vm.TipoSubmit != "APROBAR" && vm.TipoSubmit != "GUARDARC")
            {
                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeEditoIFC + " <a href='" + Url.Action("View", new { id = ifcActual.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
            }
            else
                if (success && vm.TipoSubmit == "APROBAR")
            {
                #region TODAS ACCIONES DE MEJORA SE VAN PARA PLAN

                using (AbetEntities context = new AbetEntities())
                {
                    var data = (from a in context.IFC
                                join b in context.UnidadAcademica on a.IdUnidadAcademica equals b.IdUnidadAcademica
                                join c in context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
                                join d in context.MallaCurricularPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals d.IdSubModalidadPeriodoAcademico
                                join e in context.CursoMallaCurricular on c.IdCurso equals e.IdCurso
                                where a.IdIFC == vm.IdIfc && b.Nivel == 3
                                select e.EsElectivo).Distinct().ToList();

                    bool escarrera = data.Contains(false);

                    if (escarrera)
                    {
                        var listaccionesdemejoraIFC = (from a in context.IFC
                                                       join b in context.IFCHallazgo on a.IdIFC equals b.idIFC
                                                       join c in context.HallazgoAccionMejora on b.IdHallazgo equals c.IdHallazgo
                                                       join d in context.AccionMejora on c.IdAccionMejora equals d.IdAccionMejora
                                                       where a.IdIFC == vm.IdIfc
                                                       select d).ToList();

                        foreach (var accion in listaccionesdemejoraIFC)
                        {
                            accion.ParaPlan = true;
                        }

                        context.SaveChanges();
                    }
                }


                #endregion

                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeAproboIFC + " <a href='" + Url.Action("View", new { id = ifcActual.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
            }
            else
            if (success && vm.TipoSubmit == "GUARDARC")
            {
                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.Guardado + " <a href='" + Url.Action("View", new { id = ifcActual.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
            }
            else
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoPuedeEditarIFC);
            }

            return RedirectToAction("Consult");
        }

        private bool EditarHallazgoDescripcion(String codigoHallazgoActual, String Descripcion)
        {
            try
            {
                List<Hallazgo> hallazgos = Context.Hallazgo.Where(x => x.Codigo.Equals(codigoHallazgoActual)).ToList();
                foreach (var hallazgo in hallazgos)
                {
                    hallazgo.DescripcionEspanol = Descripcion;
                    hallazgo.DescripcionIngles = Descripcion;
                }

                Context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool EliminarHallazgo(String codigoHallazgoActual, Int32 IdSubmodalidadPeriodoAcademicoModulo)
        {
            try
            {
                int aux = 0;
                String varc = "";
                Char auxchar;
                int IdentificadorAux = 0;

                for (int i = 0; i < codigoHallazgoActual.Length; i++)
                {
                    auxchar = codigoHallazgoActual[i];
                    if (auxchar == '-')
                    {
                        aux++;
                    }
                    if (aux == 2)
                    {
                        varc = codigoHallazgoActual.Substring(i, codigoHallazgoActual.Length - i);
                        break;
                    }
                }

                String numc = Regex.Replace(varc, "[^.0-9]", "");

                IdentificadorAux = Convert.ToInt32(numc);

                var hallazgos = (from h in Context.Hallazgos
                                 where h.Identificador == IdentificadorAux
                                  && h.IdSubModalidadPeriodoAcademicoModulo == IdSubmodalidadPeriodoAcademicoModulo
                                 select h).ToList();

                List<IFCHallazgo> ifcHallazgos = hallazgos.SelectMany(x => x.IFCHallazgo).ToList();
                List<HallazgoAccionMejora> hallazgosAccionMejora = hallazgos.SelectMany(x => x.HallazgoAccionMejora).ToList();
                List<AccionMejora> accionesMejora = hallazgosAccionMejora.Select(x => x.AccionMejora).ToList();
                List<AccionMejoraPlanAccion> accionesMejoraPlanAccion = accionesMejora.SelectMany(x => x.AccionMejoraPlanAccion).ToList();
                List<IFCHallazgoOutcome> ifcHallazgosOutcome = new List<IFCHallazgoOutcome>();
                foreach (var item in hallazgos)
                {
                    ifcHallazgosOutcome.AddRange(Context.IFCHallazgoOutcome.Where(x => x.IdHallazgo == item.IdHallazgo).ToList());
                }

                var planmejoraaccion = (from am in accionesMejora
                                        join pma in Context.PlanMejoraAccion on am.IdAccionMejora equals pma.IdAccionMejora
                                        where am.ParaPlan == false
                                        select pma).ToList();

                if (accionesMejoraPlanAccion.Count > 0)
                {
                    return false;
                }

                Context.IFCHallazgoOutcome.RemoveRange(ifcHallazgosOutcome);
                Context.IFCHallazgo.RemoveRange(ifcHallazgos);
                Context.AccionMejoraPlanAccion.RemoveRange(accionesMejoraPlanAccion);
                Context.HallazgoAccionMejora.RemoveRange(hallazgosAccionMejora);
                Context.PlanMejoraAccion.RemoveRange(planmejoraaccion);
                Context.AccionMejora.RemoveRange(accionesMejora);
                Context.Hallazgos.RemoveRange(hallazgos);

                return true;
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoBorrarLosHallazgos);
                return false;
            }
        }

        private bool EliminarAccionMejora(Int32 IdAccionMejora)
        {
            try
            {
                var accionMejora = (from ham in Context.HallazgoAccionMejora
                                    join am in Context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                    join h in Context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                                    //         where (am.Codigo +"-"+ h.Identificador.ToString()) == codigoActualAccion
                                    where am.IdAccionMejora == IdAccionMejora
                                    // where am.Codigo == codigo
                                    select am).FirstOrDefault();

                //VALIDAR SI AÚN EXISTE LA ACCION DE MEJORA, PORQUE PUDO SER ELIMINADO PREVIAMENTE POR LA ELIMINACIÓN DEL HALLAZGO                       
                if(accionMejora == null)
                {
                    return true;
                }
                else
                {

                    List<HallazgoAccionMejora> hallazgoAccionMejora = accionMejora.HallazgoAccionMejora.ToList();
                    List<AccionMejoraPlanAccion> accionMejoraPlanAccion = accionMejora.AccionMejoraPlanAccion.ToList();

                    if (accionMejoraPlanAccion.Count > 0)
                    {
                        return false;
                    }

                    Context.AccionMejoraPlanAccion.RemoveRange(accionMejoraPlanAccion);
                    Context.HallazgoAccionMejora.RemoveRange(hallazgoAccionMejora);
                    Context.AccionMejora.Remove(accionMejora);
                    return true;
                }

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoBorrarLaAccionDeMejora);
                return false;
            }
        }

        private bool ActualizarIfc(IFC ifcActual, CreateIfcViewModel viewModel)
        {
            try
            {

                ifcActual.ApreciacionDelegado = viewModel.ApreciacionDelegado;
                ifcActual.AprecioacionAlumnos = viewModel.ApreciacionAlumnos;
                ifcActual.Infraestructura = viewModel.Infraestructura;
                ifcActual.ComentarioEncuesta = viewModel.ComentarioEncuesta;
                if (viewModel.TipoSubmit == "GUARDAR")
                    ifcActual.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.CODIGO;
                else
                    if (viewModel.TipoSubmit == "APROBAR")
                {
                    ifcActual.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO;
                    ifcActual.Observaciones = null;
                    ifcActual.IdDocenteObservador = null;
                    var docentesesionid = Session.GetDocenteId();
                    ifcActual.IdDocenteAprobador = docentesesionid;
                }
                else
                if (viewModel.TipoSubmit == "GUARDARC")
                {
                    //CUANDO GUARDA EL COORDINADOR                 
                }
                else
                {
                    ifcActual.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO;
                    ifcActual.IdDocenteAprobador = null;
                }

                int idSubModalidadPeriodoAcademico = ifcActual.UnidadAcademica.IdSubModalidadPeriodoAcademico.Value;
                var unidadAcademica = ifcActual.UnidadAcademica;
                var curso = unidadAcademica.CursoPeriodoAcademico.Curso;
                var IdSubmodalidadPeriodoAcademicoModulo = viewModel.IdSubmodalidadPeriodoAcademicoModulo;
                var IdConstituyenteInstrumento = viewModel.IdConstituyenteInstrumento;
                List<Hallazgos> hallazgosActuales = (from h in Context.Hallazgos
                                                     join ifch in Context.IFCHallazgo on h.IdHallazgo equals ifch.IdHallazgo
                                                     where ifch.idIFC == ifcActual.IdIFC
                                                     select h
                                    ).ToList();

                var diccionarioHallazgo = new Dictionary<String, String>();
                var diccionarioIdentificador = new Dictionary<String, int>();
                List<int> IdsHallazgosActuales = hallazgosActuales.Select(x => x.IdHallazgo).ToList();
                List<int> IdsAccionesMejoraActuales = hallazgosActuales.SelectMany(x => x.HallazgoAccionMejora)
                    .Select(x => x.AccionMejora.IdAccionMejora).ToList();

                if (viewModel.Hallazgos == null)
                    viewModel.Hallazgos = new List<HallazgoViewModel>();

                Context.SaveChanges();

                List<Hallazgos> hallazgosZ = new List<Hallazgos>();

                FindLogic ContIndicador = new FindLogic(CargarDatosContext(), 1, idSubModalidadPeriodoAcademico);
                List<String> codigosHallazgosActuales = hallazgosActuales.Select(x => x.Codigo + '-' + x.Identificador.ToString()).ToList();
                List<String> codigosAccionesMejoraActuales = hallazgosActuales.SelectMany(x => x.HallazgoAccionMejora).Select(x => x.AccionMejora.Codigo).ToList();
                List<HallazgoViewModel> LstHallazgosBasura = new List<HallazgoViewModel>();

                foreach (var hallazgosEnVM in viewModel.Hallazgos)
                {
                    if (hallazgosEnVM.Codigo == null || (hallazgosEnVM.Codigo.Contains("TEMP") && !hallazgosEnVM.Sede.Contains("CS")))
                    {
                        LstHallazgosBasura.Add(hallazgosEnVM);
                    }
                }


                //AGREGAR ACA ELIMINACION DE IFCHALLAZGOOUTCOME

                foreach (var hallazgosBasura in LstHallazgosBasura)
                {
                    viewModel.Hallazgos.Remove(hallazgosBasura);
                }

                Context.SaveChanges();

                // Revisar funcionamiento 
                Boolean encontrado = false;
                foreach (var codigoHallazgoActual in codigosHallazgosActuales)
                {
                    encontrado = false;
                    foreach (var hallazgosViewModel in viewModel.Hallazgos)
                    {
                        if (hallazgosViewModel.Codigo != null)
                        {
                            if (hallazgosViewModel.Codigo.Equals(codigoHallazgoActual))
                            {
                                encontrado = true;
                                break;
                            }
                        }
                        //EDITAMOS SI SON IGUALES la descripcion del hallazgo
                    }
                    if (!encontrado) // SI NO LO encontramos en el ViewModel el codigo, lo borramos
                    {
                        Boolean SepuedeEliminar = EliminarHallazgo(codigoHallazgoActual, IdSubmodalidadPeriodoAcademicoModulo);
                        if (!SepuedeEliminar)
                        {
                            PostMessage(MessageType.Error, MessageResource.NoSePuedeEliminarLaAccionDeMejoraQuePertenezcaaUnPlan);
                            return false;
                        }
                    }
                }

                //CREAR LISTA OUTCOMES
                var outcomes = new List<Outcome>();
                //HASTA ACA

                // Revisar funcionamiento
                if (viewModel.Hallazgos.Count > 0)
                {
                    var auxCodigoHallazgo = "";
                    foreach (var hallazgoViewModel in viewModel.Hallazgos)
                    {
                        //CARGAR LISTA OUTCOMES 
                        if (hallazgoViewModel.Sede != null)
                        {
                            var idOutcome = hallazgoViewModel.idOutcome;
                            var lst = idOutcome.Split('|');
                            var lstOutcome = new List<String>(lst);

                            foreach (var item in lstOutcome)
                            {
                                var temp = Context.Outcome.ToList().Where(o => o.IdOutcome == item.ToInteger()).FirstOrDefault();
                                outcomes.Add(temp);
                            }
                        }
                        //HASTA ACA

                        if (hallazgoViewModel.Sede == "CS")
                        {
                            if (hallazgoViewModel.Codigo != null)
                            {
                                var hallazgosDisponibles = Context.Hallazgos.Where(x => x.IdHallazgo.Equals(hallazgoViewModel.IdHallazgo)).ToList();
                                if (hallazgosDisponibles.Count == 0) // ES UN HALLAZGO NUEVO
                                {
                                    System.Diagnostics.Debug.WriteLine("EDITANDO IFC: HALLAZGO n CON CRITICIDAD" + hallazgoViewModel.Criticidad);
                                    if (hallazgoViewModel.Criticidad == null) // Esto ocurre cuando se borra un hallazgo temporal en la vista, por defecto se van a cambiar todas las criticidades a normal, corregir esto por el javascript
                                    {
                                        hallazgoViewModel.Criticidad = "Normal";
                                    }

                                    int? auxIDcriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == hallazgoViewModel.Criticidad).IdCriticidad;

                                    if (!auxIDcriticidad.HasValue)
                                        auxIDcriticidad = 3;

                                    var CodigoNuevoHallazgo = "HIFC-" + curso.Codigo;
                                    var Identificador = ContIndicador.ObtenerIndiceHallazgo();

                                    if (!hallazgoViewModel.Codigo.Equals(auxCodigoHallazgo))
                                    {
                                        auxCodigoHallazgo = hallazgoViewModel.Codigo;
                                        diccionarioHallazgo.Add(hallazgoViewModel.Codigo, CodigoNuevoHallazgo);
                                        diccionarioIdentificador.Add(hallazgoViewModel.Codigo, Identificador);
                                    }
                                    //hallazgosZ.Add
                                    var hallazgo_outcome = new Hallazgos
                                    {
                                        Codigo = CodigoNuevoHallazgo,
                                        DescripcionEspanol = hallazgoViewModel.Descripcion,
                                        DescripcionIngles = hallazgoViewModel.Descripcion,
                                        IdConstituyenteInstrumento = IdConstituyenteInstrumento,
                                        // IdOutcomeComision =
                                        IdCurso = curso.IdCurso,
                                        IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.Where(x => x.NombreEspanol.Contains("Necesita mejora")).FirstOrDefault().IdNivelAceptacionHallazgo,
                                        FechaRegistro = DateTime.Now,
                                        IdCriticidad = auxIDcriticidad.Value,
                                        IdSubModalidadPeriodoAcademicoModulo = IdSubmodalidadPeriodoAcademicoModulo,
                                        Estado = "ACT",
                                        // IdNumeroPractica =   
                                        Identificador = Identificador,
                                        HallazgoAccionMejora = new List<HallazgoAccionMejora>(),
                                        IFCHallazgo = new List<IFCHallazgo>(),
                                        IFCHallazgoOutcome = new List<IFCHallazgoOutcome>()
                                    };

                                    //Nuevo 
                                    hallazgosZ.Add(hallazgo_outcome);
                                    Context.Hallazgos.Add(hallazgo_outcome);
                                    Context.SaveChanges();

                                    foreach (var outcome in outcomes)
                                    {
                                        var ifcoutcome = new IFCHallazgoOutcome
                                        {
                                            IdHallazgo = hallazgo_outcome.IdHallazgo,
                                            IdOutcome = outcome.IdOutcome
                                        };
                                        Context.IFCHallazgoOutcome.Add(ifcoutcome);
                                    }
                                    Context.SaveChanges();
                                }
                                outcomes.Clear();
                            }
                        }
                    }
                }

                //Context.Hallazgos.AddRange(hallazgosZ);
                //Context.SaveChanges();

                if (viewModel.AccionesPropuestas == null)
                    viewModel.AccionesPropuestas = new List<AccionMejoraViewModel>();

                if (viewModel.AccionesPropuestas != null)
                {
                    /*
                    Boolean encontradoAccion = false;
                    foreach (var codigoActualAccion in codigosAccionesMejoraActuales)
                    {

                        encontradoAccion = false;
                        foreach (var accionViewModel in viewModel.AccionesPropuestas)
                        {

                            var codigomatch = accionViewModel.Codigo.Replace("HIFC", "AIFC");                      // Artificio para acciones de mejora viejas
                            if (codigoActualAccion.Equals(codigomatch))                                           
                            {
                                encontradoAccion = true;

                                break;
                            }
                        }

                        if (!encontradoAccion)
                        {
                            Boolean SePuedeEliminarAccion = EliminarAccionMejora(codigoActualAccion, 9887);
                            if (!SePuedeEliminarAccion)
                            {
                                PostMessage(MessageType.Warning, "No se puede Eliminar la acción de Mejora que pertenezca a un plan");
                                return false;
                            }
                        }                    
                    }
                    */


                    foreach (var idactualaccion in IdsAccionesMejoraActuales)
                    {
                        Boolean ExisteLaAccionMejoraEnViewModel = viewModel.AccionesPropuestas.Any(x => x.IdAccionMejora.Equals(idactualaccion));
                        if (ExisteLaAccionMejoraEnViewModel == false)
                        {
                            Boolean SePuedeEliminarAccion = EliminarAccionMejora(idactualaccion);
                            if (!SePuedeEliminarAccion)
                            {
                                PostMessage(MessageType.Warning, MessageResource.NoSePuedeEliminarLaAccionDeMejoraQuePertenezcaaUnPlan);
                                return false;
                            }
                        }

                    }



                    if (viewModel.AccionesPropuestas.Count > 0)
                    {
                        List<AccionMejora> AccionesMejoraNuevas = new List<AccionMejora>();
                        foreach (var accionViewModel in viewModel.AccionesPropuestas)
                        {
                            var accionesDisponibles = Context.AccionMejora.Where(x => x.IdAccionMejora == accionViewModel.IdAccionMejora).ToList();
                            if (accionesDisponibles.Count == 0 && accionViewModel.Codigo.Contains("TEMP")) // ES NUEVO
                            {
                                // AQUI DEBERIA IR EL STORED PROCEDURE
                                var Identificador = ContIndicador.ObtenerIndiceAccionMejora();
                                var CodigoNuevoAccionMejora = "AIFC-" + curso.Codigo;
                                // Aqui se deberia llamar al sp de identificadores
                                // var Identificador = 

                                var accion = new AccionMejora
                                {
                                    DescripcionEspanol = accionViewModel.Descripcion,
                                    DescripcionIngles = accionViewModel.Descripcion,
                                    Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO,
                                    // IdCurso = viewModel.IdCurso,
                                    IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico,
                                    Codigo = CodigoNuevoAccionMejora,
                                    Anio = ifcActual.FechaCreacion.Year,
                                    IdEscuela = EscuelaId,
                                    ParaPlan = false,
                                    Identificador = Identificador,
                                    HallazgoAccionMejora = new List<HallazgoAccionMejora>()
                                };

                                String auxCodigo = "";

                                foreach (var hallazgoVm in viewModel.Hallazgos)
                                {
                                    // Aqui falta hacer que el viewmodel tambien traiga el ID del hallazgo seleccionado y no solo su descripcion en la accionmejora     
                                    if (!accionViewModel.CodigoHallazgo.Contains("TEMP"))
                                    {
                                        if (accionViewModel.CodigoHallazgo.Equals(hallazgoVm.Codigo))
                                        {
                                            //           var asd = accionViewModel.CodigoHallazgo.Equals(hallazgoVm.Codigo);
                                            //           var LstHallazgosParaVm = context.Hallazgos.Where(x => x.Codigo.Equals(hallazgoVm.Codigo)).ToList();
                                            //           if (LstHallazgosParaVm.Count > 0)
                                            //           {
                                            // HALLAZGOS VIEJOS
                                            //  foreach (var hallazgoAux in LstHallazgosParaVm)
                                            //  {
                                            Hallazgos hallazgoAux = Context.Hallazgos.Where(x => x.IdHallazgo == hallazgoVm.IdHallazgo).FirstOrDefault();

                                            //      accion.Codigo = accion.Codigo + '-' + hallazgoAux.Identificador.ToString();                                                                   //  IDentificador accion mejora
                                            var hallazgoAccion = new HallazgoAccionMejora
                                            {
                                                AccionMejora = accion,
                                                Hallazgos = hallazgoAux
                                            };
                                            accion.HallazgoAccionMejora.Add(hallazgoAccion);                                                            // RML001 Revisar los insert
                                            hallazgoAux.HallazgoAccionMejora.Add(hallazgoAccion);                                                       // Rml001 Revisar los insert
                                            Context.SaveChanges();                                                                                                            //  }
                                        }
                                    }
                                    else if (accionViewModel.CodigoHallazgo.Equals(hallazgoVm.Codigo))
                                    // Aqui falta hacer que
                                    {
                                        if (hallazgoVm.Sede == "CS")
                                        {
                                            // HALLAZGOS NUEVOS
                                            if (!hallazgoVm.Codigo.Equals(auxCodigo))
                                            {
                                                var auxcodhallazgo = diccionarioHallazgo[hallazgoVm.Codigo];                                                 // ?? revisar RML001
                                                var auxIdent = diccionarioIdentificador[hallazgoVm.Codigo];
                                                foreach (var hallazgo in hallazgosZ)
                                                {
                                                    if (hallazgo.Codigo.Equals(auxcodhallazgo) && hallazgo.Identificador.Equals(auxIdent))
                                                    {
                                                        //  accion.Codigo = accion.Codigo + '-' + hallazgo.Identificador.ToString();
                                                        var hallazgoAccion = new HallazgoAccionMejora
                                                        {
                                                            AccionMejora = accion,
                                                            Hallazgos = hallazgo
                                                        };
                                                        accion.HallazgoAccionMejora.Add(hallazgoAccion);
                                                        hallazgo.HallazgoAccionMejora.Add(hallazgoAccion);
                                                        Context.SaveChanges();
                                                    }
                                                }
                                                auxCodigo = hallazgoVm.Codigo;

                                            }
                                        }
                                        break;
                                    }


                                }
                            }
                        }

                    }
                }
                Context.SaveChanges();
                foreach (var hallazgo in hallazgosZ)
                {
                    var ifcHallazgo = new IFCHallazgo
                    {
                        Hallazgos = hallazgo,
                        IFC = ifcActual
                    };
                    ifcActual.IFCHallazgo.Add(ifcHallazgo);
                    hallazgo.IFCHallazgo.Add(ifcHallazgo);
                }

                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult View(int id)
        {
            try
            {
                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

                var ifc = GedServices.GetIfcServices(Context, id);

                if (ifc == null)
                {
                    PostMessage(MessageType.Error, ControllerIFCManagementResource.NoSeEncontroIFC);
                    return RedirectToAction("Consult");
                }

                int idDocenteSesion = CargarDatosContext().session.GetDocenteId() ?? 0;

                int idCursoUnidadAcademica = ifc.IdUnidadAcademica;
                var unidadAcademica = ifc.UnidadAcademica;
                var subModalidadPeriodoAcademico = ifc.UnidadAcademica.CursoPeriodoAcademico.SubModalidadPeriodoAcademico;
                var idSubModalidadPeriodoAcademico = subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
                var idSubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;
                var curso = GedServices.GetCursoServices(Context, ifc.UnidadAcademica.CursoPeriodoAcademico.IdCurso);
                var carrera = GedServices.GetAreaForCursoServices(Context, ifc.UnidadAcademica.IdUnidadAcademica);
                var subarea = unidadAcademica.UnidadAcademica2;
                var area = subarea.UnidadAcademica2;
                JsonController jsoncontroller = new JsonController();
                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();    
                
                var vm = new ViewIfcViewModel
                {
                    IdIfc = ifc.IdIFC,
                    IdUnidadAcademica = ifc.IdUnidadAcademica,
                    CodigoCurso = curso.Codigo,
                    Estado = GedServices.GetTextoForCodigoEstadoIfcServices(ifc.Estado, CurrentCulture),
                    FechaCreacion = ifc.FechaCreacion,
                    AccionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(Context, ModalidadId, idCursoUnidadAcademica, subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, EscuelaId),
                    Hallazgos = GedServices.GetHallazgosZForIfcServices(Context, ifc.IdIFC),
                    AccionesPropuestas = GedServices.GetAccionMejorasForIfcServicesZ(Context, ifc.IdIFC),
                    CicloAcademico = subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Titulo = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                    "IFC - " + area.NombreEspanol + " - " + subarea.NombreEspanol + " - " + curso.NombreEspanol + " - " + subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico :
                    "IFC - " + area.NombreIngles + " - " + subarea.NombreIngles + " - " + curso.NombreIngles + " - " + subModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Infraestructura = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.Infraestructura : helper.Translate(ifc.Infraestructura),
                    ApreciacionAlumnos = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.AprecioacionAlumnos : helper.Translate(ifc.AprecioacionAlumnos),
                    ApreciacionDelegado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ApreciacionDelegado : helper.Translate(ifc.ApreciacionDelegado),
                    ComentarioEncuesta = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ComentarioEncuesta : helper.Translate(ifc.ComentarioEncuesta),
                    Docente = ifc.Docente,
                    ResultadoLogroCurso = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoLogroCurso : helper.Translate(ifc.ResultadoLogroCurso),
                    ResultadoAlcanzado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoAlcanzado : helper.Translate(ifc.ResultadoAlcanzado),
                    PuedeEditar = false,
                    PuedeObservar = false,
                    PuedeAprobar = false,
                    Observaciones = ifc.Observaciones,
                    AprobadoPor = null,
                    ObservadoPor = null,
                    PuedeExportar = false
                };

                //VALIDAR SI ES COORDINADOR DE AREA

                #region ES COORDINADOR
                bool esSuperiorSubarea = false;
                bool esSuperiorArea = false;

                var DocenteArea = (from a in Context.SedeUnidadAcademica
                                   join b in Context.UnidadAcademicaResponsable on a.IdSedeUnidadAcademica equals b.IdSedeUnidadAcademica
                                   join c in Context.Docente on b.IdDocente equals c.IdDocente
                                   where a.IdUnidadAcademica == area.IdUnidadAcademica
                                   select c).Distinct().FirstOrDefault();
                var usuariodocente = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente);

                List<int> ListDocenteAreaId = new List<int>();

                ListDocenteAreaId.Add(Convert.ToInt32(DocenteArea.IdDocente));

                if (usuariodocente.IdUsuarioSecundario.HasValue)
                {
                    var DocenteId = Context.Usuario.FirstOrDefault(x => x.IdDocente == DocenteArea.IdDocente).Usuario2.IdDocente;
                    ListDocenteAreaId.Add(Convert.ToInt32(DocenteId));
                }
                foreach (int docenteid in ListDocenteAreaId)
                {
                    if (idDocenteSesion == docenteid)
                    {
                        esSuperiorArea = true;
                        vm.Escoordinador = true;
                    }
                }
                #endregion


                for (int i = 0; i < vm.Hallazgos.Count(); i++)                                                                                                  // RML001
                {
                    vm.Hallazgos[i].Codigo = vm.Hallazgos[i].Codigo + '-' + vm.Hallazgos[i].Identificador.ToString();
                }


                for (int i = 0; i < vm.AccionesPropuestas.Count(); i++)                                                                                                  // RML001
                {
                    vm.AccionesPropuestas[i].Codigo = vm.AccionesPropuestas[i].Codigo + '-' + vm.AccionesPropuestas[i].Identificador.ToString();
                }



                if (ifc.Estado != ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO
                    && ifc.Estado != ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO
                    && ifc.IdDocente == idDocenteSesion)
                    vm.PuedeEditar = true;

                if (ifc.IdDocente != idDocenteSesion || true)
                {
                    var sedesDocente = (from uar in Context.UnidadAcademicaResponsable
                                        join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                            sua.IdSedeUnidadAcademica
                                        join s in Context.Sede on sua.IdSede equals s.IdSede
                                        where sua.IdUnidadAcademica == ifc.IdUnidadAcademica
                                        && uar.IdDocente == ifc.IdDocente
                                        select s);
                    var sedesSubarea = (from uar in Context.UnidadAcademicaResponsable
                                        join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                            sua.IdSedeUnidadAcademica
                                        join s in Context.Sede on sua.IdSede equals s.IdSede
                                        where sua.IdUnidadAcademica == subarea.IdUnidadAcademica
                                              && uar.IdDocente == idDocenteSesion
                                        select s);
                    var interseccionSubarea = sedesDocente.Intersect(sedesSubarea).ToList();
                    if (interseccionSubarea.Count > 0)
                        esSuperiorSubarea = true;

                    var sedesArea = (from uar in Context.UnidadAcademicaResponsable
                                     join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                         sua.IdSedeUnidadAcademica
                                     join s in Context.Sede on sua.IdSede equals s.IdSede
                                     where sua.IdUnidadAcademica == area.IdUnidadAcademica
                                         && uar.IdDocente == idDocenteSesion
                                     select s);
                    var interseccionArea = sedesDocente.Intersect(sedesArea).ToList();
                    if (interseccionArea.Count > 0)
                        esSuperiorArea = true;
                }

                // Si es coordinador
                if ((esSuperiorArea) && (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO || ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO))
                    vm.PuedeEditar = true;
                else
                if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO)
                    vm.PuedeEditar = false;

                // Observar
                if ((ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO)
                 && (esSuperiorArea || (esSuperiorSubarea && (int)ifc.IdDocente != idDocenteSesion)))
                {
                    // Enviado
                    if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO)
                    {
                        vm.PuedeObservar = true;
                    }
                    // Aprobado
                    else
                    {
                        if (esSuperiorArea && (int)ifc.IdDocenteAprobador != idDocenteSesion)
                        {
                            vm.PuedeObservar = true;
                        }
                    }
                }

                // Aprobar
                if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO && esSuperiorArea)
                {
                    vm.PuedeAprobar = true;
                }

                if (ifc.IdDocenteObservador != null)
                {
                    var docente = Context.Docente.First(x => x.IdDocente == (int)ifc.IdDocenteObservador);
                    vm.ObservadoPor = docente.Apellidos + ", " + docente.Nombres;
                }
                if (ifc.IdDocenteAprobador != null)
                {
                    var docente = Context.Docente.First(x => x.IdDocente == (int)ifc.IdDocenteAprobador);
                    vm.AprobadoPor = docente.Apellidos + ", " + docente.Nombres;
                }

                if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO)
                {
                    vm.PuedeExportar = true;
                }

                foreach (var accion in vm.AccionesPropuestas)
                {
                    accion.Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accion.Estado);
                }

                foreach (var accionp in vm.AccionesPrevias)
                {
                    accionp.Estado = GedServices.GetTextoForCodigoEstadoAccionMejoraIfcServices(accionp.Estado);
                }

                return View(vm);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError + ".");
                return RedirectToAction("Consult", "IfcManagement");
            }

        }

        private bool EsSuperiorArea(IFC ifc, UnidadAcademica area, int idDocente)
        {
            var sedesDocente = (from uar in Context.UnidadAcademicaResponsable
                                join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                    sua.IdSedeUnidadAcademica
                                join s in Context.Sede on sua.IdSede equals s.IdSede
                                where sua.IdUnidadAcademica == ifc.IdUnidadAcademica
                                && uar.IdDocente == ifc.IdDocente
                                select s);
            var sedesArea = (from uar in Context.UnidadAcademicaResponsable
                             join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                 sua.IdSedeUnidadAcademica
                             join s in Context.Sede on sua.IdSede equals s.IdSede
                             where sua.IdUnidadAcademica == area.IdUnidadAcademica
                                 && uar.IdDocente == idDocente
                             select s);
            var interseccionArea = sedesDocente.Intersect(sedesArea).ToList();

            return interseccionArea.Count > 0;
        }

        private bool EsSuperiorSubArea(IFC ifc, UnidadAcademica subarea, int idDocente)
        {
            var sedesDocente = (from uar in Context.UnidadAcademicaResponsable
                                join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                    sua.IdSedeUnidadAcademica
                                join s in Context.Sede on sua.IdSede equals s.IdSede
                                where sua.IdUnidadAcademica == ifc.IdUnidadAcademica
                                && uar.IdDocente == ifc.IdDocente
                                select s);
            var sedesSubarea = (from uar in Context.UnidadAcademicaResponsable
                                join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                    sua.IdSedeUnidadAcademica
                                join s in Context.Sede on sua.IdSede equals s.IdSede
                                where sua.IdUnidadAcademica == subarea.IdUnidadAcademica
                                      && uar.IdDocente == idDocente
                                select s);
            var interseccionSubarea = sedesDocente.Intersect(sedesSubarea).ToList();

            return interseccionSubarea.Count > 0;
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Docente)]
        public ActionResult CambiarEstado(ViewIfcViewModel vm)
        {
            if (SessionHelper.GetDocenteId(Session) == null)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoTienePrivilegiosAccion);
                return RedirectToAction("Consult");
            }
            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            var ifc = GedServices.GetIfcServices(Context, vm.IdIfc);
            if (vm.TipoSubmit == "OBSERVAR")
            {
                ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.CODIGO;
                ifc.Observaciones = vm.Observaciones;
                ifc.IdDocenteObservador = idDocenteSesion;
                ifc.IdDocenteAprobador = null;
                Context.SaveChanges();
                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeObservoIFC + " <a href='" + Url.Action("View", new { id = ifc.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
                return RedirectToAction("Consult");
            }
            if (vm.TipoSubmit == "APROBAR")
            {
                using (AbetEntities context = new AbetEntities())
                {
                    var data = (from a in context.IFC
                                join b in context.UnidadAcademica on a.IdUnidadAcademica equals b.IdUnidadAcademica
                                join c in context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
                                join d in context.MallaCurricularPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals d.IdSubModalidadPeriodoAcademico
                                join e in context.CursoMallaCurricular on c.IdCurso equals e.IdCurso
                                where a.IdIFC == vm.IdIfc && b.Nivel == 3
                                select e.EsElectivo).Distinct().ToList();

                    bool escarrera = data.Contains(false);

                    if (escarrera)
                    {
                        var listaccionesdemejoraIFC = (from a in context.IFC
                                                       join b in context.IFCHallazgo on a.IdIFC equals b.idIFC
                                                       join c in context.HallazgoAccionMejora on b.IdHallazgo equals c.IdHallazgo
                                                       join d in context.AccionMejora on c.IdAccionMejora equals d.IdAccionMejora
                                                       where a.IdIFC == ifc.IdIFC
                                                       select d).ToList();

                        foreach (var accion in listaccionesdemejoraIFC)
                        {
                            accion.ParaPlan = true;
                        }

                        context.SaveChanges();
                    }
                }

                ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO;
                ifc.Observaciones = null;
                ifc.IdDocenteObservador = null;
                ifc.IdDocenteAprobador = idDocenteSesion;
                Context.SaveChanges();
                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeAproboIFC + " <a href='" + Url.Action("View", new { id = ifc.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
                return RedirectToAction("Consult");
            }
            if (vm.TipoSubmit == "ENVIAR")
            {
                ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO;
                ifc.IdDocenteAprobador = null;
                Context.SaveChanges();
                PostMessage(MessageType.Success, @"" + ControllerIFCManagementResource.SeEnvioIFC + " <a href='" + Url.Action("View", new { id = ifc.IdIFC }) + @"'><b>" + ControllerIFCManagementResource.Aqui + "</b></a>.");
                return RedirectToAction("Consult");
            }
            PostMessage(MessageType.Error, @"" + ControllerIFCManagementResource.ErrorIntentarMasTarde);
            return RedirectToAction("Consult");
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Delete(int id)
        {
            var ifc = GedServices.GetIfcServices(Context, id);
            if (ifc == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (SessionHelper.GetDocenteId(Session) == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            if (ifc.IdDocente != idDocenteSesion)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ifc.Estado != ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.CODIGO)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // bool success = GedServices.DeleteIfcServices(context, id);                                                           // RML001 tabla hallazgos temporal
            bool success = GedServices.DeleteIfcServicesZ(Context, id);

            if (success)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult GetIfcPdf(int id)
        {
            try
            {
               
                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
                var ifc = GedServices.GetIfcServices(Context, id);
                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                var Infraestructura = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.Infraestructura : helper.Translate(ifc.Infraestructura);
                var ApreciacionAlumnos = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.AprecioacionAlumnos : helper.Translate(ifc.AprecioacionAlumnos);
                var ApreciacionDelegado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ApreciacionDelegado : helper.Translate(ifc.ApreciacionDelegado);
                var ComentarioEncuesta = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ComentarioEncuesta : helper.Translate(ifc.ComentarioEncuesta);
                var ResultadoLogroCurso = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoLogroCurso : helper.Translate(ifc.ResultadoLogroCurso);
                var ResultadoAlcanzado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoAlcanzado : helper.Translate(ifc.ResultadoAlcanzado);

                ExportWorkerIfc pdfExport = new ExportWorkerIfc(Context, id, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, CurrentCulture , Infraestructura, ApreciacionAlumnos, ApreciacionDelegado, ComentarioEncuesta);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.IFC_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                string filename = Path.GetFileName(path);
                //string filename = path.Split(new char[] { '/' }).Last();
                return File(path, "application/pdf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoExportarPDFIFC + " " + ex.ToString());
                return RedirectToAction("Consult");
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult GetIfcRtf(int id)
        {
            try
            {
                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
                var ifc = GedServices.GetIfcServices(Context, id);
                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                var Infraestructura = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.Infraestructura : helper.Translate(ifc.Infraestructura);
                var ApreciacionAlumnos = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.AprecioacionAlumnos : helper.Translate(ifc.AprecioacionAlumnos);
                var ApreciacionDelegado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ApreciacionDelegado : helper.Translate(ifc.ApreciacionDelegado);
                var ComentarioEncuesta = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ComentarioEncuesta : helper.Translate(ifc.ComentarioEncuesta);
                var ResultadoLogroCurso = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoLogroCurso : helper.Translate(ifc.ResultadoLogroCurso);
                var ResultadoAlcanzado = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? ifc.ResultadoAlcanzado : helper.Translate(ifc.ResultadoAlcanzado);
                ExportWorkerIfc rtfExport = new ExportWorkerIfc(Context, id, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, CurrentCulture, Infraestructura, ApreciacionAlumnos, ApreciacionDelegado, ComentarioEncuesta);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.IFC_DIRECTORY, "RTF");
                Directory.CreateDirectory(directory);
                string path = rtfExport.GetFile(ExportFileFormat.Rtf, directory);
                string filename = Path.GetFileName(path);
                //string filename = path.Split(new char[] { '/' }).Last();
                return File(path, "application/rtf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ControllerIFCManagementResource.NoExportarRTFIFC);
                return RedirectToAction("Consult");
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult AddEvidenciaModal(string IdAccionMejora, string vista)
        {
            var idAccionMejora = Convert.ToInt32(IdAccionMejora);

            var vm = new AddEvidenciaViewModel();

            vm.CargarData(idAccionMejora, Context, vista);
            return View(vm);
        }
        //FUNCION DE IDPERIODO A IDSUBMODALIDADPERIODOACADEMICO pero que ademas corresponda al docente actual
        [HttpPost]
        public JsonResult AddEditEvidence()
        {
            string Retornar;
            var evidenceDescripcion = Request.Form[0];
            var evidenceActionId = Request.Form[1];
            var evidenceViewType = Request.Form[3];
            HttpFileCollectionBase files = Request.Files;
            try
            {
                //  if (string.IsNullOrEmpty(evidenceDescripcion) || files.Count == 0)
                if (string.IsNullOrEmpty(evidenceDescripcion))
                {
                    return Json("Empty", JsonRequestBehavior.AllowGet);
                }

                EvidenciaAccionMejora evidencia = new EvidenciaAccionMejora();

                String timestamp = (DateTime.Now).ToString("yyyyMMddHHmmssffff");
                var uploadDir = "~/uploads/AccionMejora";
                var imageUrl = "";

                int FileExist = files.Count;

                if (FileExist != 0)
                {
                    HttpPostedFileBase file = files[0];
                    if (file != null)
                    {
                        if (Path.GetExtension(file.FileName).ToLower() != ".jpg" && Path.GetExtension(file.FileName).ToLower() != ".png"
                            && Path.GetExtension(file.FileName).ToLower() != ".gif" && Path.GetExtension(file.FileName).ToLower() != ".jpeg"
                            && Path.GetExtension(file.FileName).ToLower() != ".zip" && Path.GetExtension(file.FileName).ToLower() != ".doc"
                            && Path.GetExtension(file.FileName).ToLower() != ".docx" && Path.GetExtension(file.FileName).ToLower() != ".xlsx"
                            && Path.GetExtension(file.FileName).ToLower() != ".xls" && Path.GetExtension(file.FileName).ToLower() != ".pdf"
                            && Path.GetExtension(file.FileName).ToLower() != ".rar")
                        {
                            PostMessage(MessageType.Error, MessageResource.SoloSePermitenImagenesArchivosConExtension + " .jpg, .png, .gif, .jpeg, .doc, .docx, .xlsx, .xls, .pdf, .zip, .rar )");
                            return Json("False", JsonRequestBehavior.AllowGet);
                        }

                        var imagePath = Path.Combine(Server.MapPath(uploadDir), timestamp + Path.GetExtension(file.FileName));
                        imageUrl = "/uploads/ReunionDelegados/" + timestamp + Path.GetExtension(file.FileName);
                        file.SaveAs(imagePath);
                        evidencia.RutaArchivo = imageUrl;
                    }
                }
                evidencia.RutaArchivo = imageUrl;
                evidencia.Nombre = evidenceDescripcion;
                int idAccion = Convert.ToInt32(evidenceActionId);
                evidencia.IdAccionMejora = idAccion;

                Context.EvidenciaAccionMejora.Add(evidencia);

                AccionMejora Accion = new AccionMejora();
                Accion = (from ac in Context.AccionMejora
                          where ac.IdAccionMejora == idAccion
                          select ac).FirstOrDefault();

                Accion.Estado = "IMPLE";

                Context.SaveChanges();

            }
            catch (Exception e)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            Retornar = evidenceViewType + "_True";
            return Json(Retornar, JsonRequestBehavior.AllowGet);

        }

        public int GetIdSubmodalidadPeriodoAcByIdPeriodoAcademico(AbetEntities ctx, int idPeriodoAcademico)
        {

            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);

            /*
            var submodalidad = (from per in ctx.PeriodoAcademico
                                join
                                    submodaper in ctx.SubModalidadPeriodoAcademico on per.IdPeriodoAcademico
                                    equals submodaper.IdPeriodoAcademico
                                join curs in ctx.CursoPeriodoAcademico
                                    on submodaper.IdSubModalidadPeriodoAcademico equals curs.IdSubModalidadPeriodoAcademico
                                join seccion in ctx.Seccion on curs.IdCursoPeriodoAcademico equals seccion.IdSeccion
                                join
                                    docensecc in ctx.DocenteSeccion on seccion.IdSeccion equals docensecc.IdSeccion
                                join
                                    doc in ctx.Docente on docensecc.IdDocente equals doc.IdDocente
                                where submodaper.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId
                                where doc.IdDocente == idDocenteSesion
                                select submodaper).ToList();
            */

            var submodalidadperacam = (from per in ctx.PeriodoAcademico
                                       join
                                           submodaper in ctx.SubModalidadPeriodoAcademico on per.IdPeriodoAcademico
                                           equals submodaper.IdPeriodoAcademico
                                       where submodaper.IdPeriodoAcademico == idPeriodoAcademico
                                       select submodaper).ToList();

            int submodalidadperiodoacademico = submodalidadperacam[0].IdSubModalidadPeriodoAcademico;

            return submodalidadperiodoacademico;


        }
        ///*********************************

        #region JsonResults Ajax
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAreas(int idPeriodoAcademico)
        {
            int idSubModalidadPeriodoAcademico = GetIdSubmodalidadPeriodoAcByIdPeriodoAcademico(Context, idPeriodoAcademico);

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            if (SessionHelper.GetDocenteId(Session) == null)
            {
                return null;
            }

            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            var areas = GedServices.GetAreasForIfcServices(Context, idDocenteSesion, idSubModalidadPeriodoAcademico,
                EscuelaId);

            //var items = areas.Select(x => new SelectListItem { Value = x, Text = x.ToUpper() });
            var items = new List<SelectListItem>();
            //este era el error

            foreach (var a in areas)
            {
                items.Add(new SelectListItem { Value = a.IdUnidadAcademica.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? a.NombreEspanol : a.NombreIngles });
            }



            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllAreas()
        {
            var idEscuela = Session.GetEscuelaId();
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var areas = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper()).Distinct();
            var items = areas.Select(x => new SelectListItem { Value = x, Text = x.ToUpper() });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllAreasForPeriodoAcademico(int idPeriodoAcademico)
        {//IdPeriodoAcademico correcto de filtro
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var areas = GedServices.GetAllAreasForPeriodoAcademico(Context, idsubmoda, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper()).Distinct();
            var items = areas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllAreasByCarrera(int idCarrera, int idPeriodoAcademico)
        {
            int idsubmodalidadperiodoacademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var areas = GedServices.GetAllAreasByCarrera(Context, EscuelaId, idCarrera, idsubmodalidadperiodoacademico).Select(x => new { Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper(), Value = x.IdUnidadAcademica }).Distinct();
            var items = areas;

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);

        }
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSubareasForPeriodoAcademico(int idPeriodoAcademico)
        {
            //RECIBE IDPERIODOACADEMICO DEL FILTRO
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idEscuela = Session.GetEscuelaId();

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var subareas = Context.UnidadAcademica
                .Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 2 && x.IdEscuela == idEscuela).ToList()
                .Select(
                    x =>
                        CurrentCulture == ConstantHelpers.CULTURE.ESPANOL
                            ? x.NombreEspanol
                            : x.NombreIngles)
                .Distinct();
            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForPeriodoAcademico(int idPeriodoAcademico)
        {//RECUBE EL ID PERIODOACADEMICOACTIVO
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var cursos =
                Context.UnidadAcademica.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).ToList()
                    .Select(
                        x =>
                            CurrentCulture == ConstantHelpers.CULTURE.ESPANOL
                                ? x.NombreEspanol
                                : x.NombreIngles).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSubareasForArea(string idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var subareas = GedServices.GetAllSUbareasInAreaServices(Context, idAreaUnidadAcademica, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSubareasForAreaInt(int idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var subareas = GedServices.GetAllSUbareasInAreaServicesInt(Context, idAreaUnidadAcademica, EscuelaId).Select(x => new { Text = x.NombreEspanol, Value = x.IdUnidadAcademica }).Distinct();
            var items = subareas;

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForArea(string idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var cursos = GedServices.GetAllCursosInAreaServices(Context, idAreaUnidadAcademica, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursos()
        {
            var idEscuela = Session.GetEscuelaId();
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var cursos = GedServices.GetAllCursosServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForSubarea(string idSubareaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var cursos = GedServices.GetAllCursosInSubareaServices(Context, idSubareaUnidadAcademica, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSubareas()
        {
            var idEscuela = Session.GetEscuelaId();
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var subareas = GedServices.GetAllSubareasServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetSubareasInArea(int idAreaUnidadAcademica, int idPeriodoAcademico)
        {
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            if (SessionHelper.GetDocenteId(Session) == null)
            {
                return null;
            }
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            //var idPeriodoAcademicoActual = PeriodoAcademicoId;//GedServices.GetCurrentPeriodoAcademicoServices(context).IdPeriodoAcademico;
            var idSubModalidadPeriodoAcademicoActual = idsubmoda;


            /*
            var subareas = GedServices.GetSubareasForIfcServices(context, idDocenteSesion, idSubModalidadPeriodoAcademicoActual, 
                idAreaUnidadAcademica, escuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                    x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper()).Distinct();


            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x.ToUpper() });

            /*
            foreach (var s in subareas)
            {
                items.Add(new SelectListItem { Value = s.IdUnidadAcademica.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? s.NombreEspanol : s.NombreIngles });
            }*/
            ///
            var subareas = GedServices.GetSubareasForIfcServices(Context, idDocenteSesion, idSubModalidadPeriodoAcademicoActual,
                idAreaUnidadAcademica, EscuelaId);

            var items = new List<SelectListItem>();


            foreach (var s in subareas)
            {
                items.Add(new SelectListItem { Value = s.IdUnidadAcademica.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? s.NombreEspanol : s.NombreIngles });
            }



            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetCursosUnidadAcademicaInSubarea(int idSubareaUnidadAcademica, int idPeriodoAcademico)
        {

            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;


            if (SessionHelper.GetDocenteId(Session) == null)
            {
                return null;
            }
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            var idPeriodoAcademicoActual = idPeriodoAcademico;//GedServices.GetCurrentPeriodoAcademicoServices(context).IdPeriodoAcademico;
            var idSubModalidadPeriodoAcademicoActual = idsubmoda;
            var cursosUnidadAcademica = GedServices.GetCursosUnidadAcademicaForIfcServices(Context, idDocenteSesion,
                idSubModalidadPeriodoAcademicoActual, idSubareaUnidadAcademica, EscuelaId);
            var items = new List<SelectListItem>();

            foreach (var c in cursosUnidadAcademica)
            {
                items.Add(new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //GetAllSubareasForAreaForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSubareasForAreaForPeriodoAcademico(string NombreAreaUnidadAcademica, int idPeriodoAcademico)
        {
            //PERIODOACADEMICO CORRECTO DE FILTRO
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var subareas = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.NombreIngles) == NombreAreaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 2)
                .Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct();

            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        //GetAllCursosForAreaForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForAreaForPeriodoAcademico(string NombreAreaUnidadAcademica, int idPeriodoAcademico)
        {
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            //     var cursos = context.UnidadAcademica.Where(x => x.UnidadAcademica2.UnidadAcademica2.NombreEspanol == NombreAreaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).
            //     Select(x => x.NombreEspanol).Distinct();

            var cursos = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.UnidadAcademica2.NombreIngles) == NombreAreaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).
                Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        //GetAllCursosForSubareaForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForSubareaForPeriodoAcademico(string NombreSubareaUnidadAcademica, int idPeriodoAcademico)
        {
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var cursos = Context.UnidadAcademica.Where(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.UnidadAcademica2.NombreEspanol : x.UnidadAcademica2.NombreIngles) == NombreSubareaUnidadAcademica && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Nivel == 3).
                Select(x => (CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles)).Distinct().OrderBy(x => x);
            var items = cursos.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        #endregion

        private List<IfcResultViewModel> GenerarListaIfcResultViewModel(List<IFC> ifcs)
        {
            bool esAdmin = false;
            var roles = SessionHelper.GetRoles(Session);
            if (roles != null)
            {
                for (int i = 0; i < roles.Length; i++)
                {
                    if (roles[i] == AppRol.Administrador || roles[i] == AppRol.Acreditador)
                    {
                        esAdmin = true;
                        break;
                    }
                }
            }
            if (SessionHelper.GetDocenteId(Session) == null && !esAdmin)
            {
                return null;
            }
            int? idDocente = CargarDatosContext().session.GetDocenteId();
            Docente docenteSession = null;
            if (idDocente.HasValue)
            {
                docenteSession = CargarDatosContext().context.Docente.First(x => x.IdDocente == idDocente.Value);
                if (docenteSession.EsAdministrativo.Value)
                    esAdmin = true;
            }
            this.CurrentCulture = CargarDatosContext().currentCulture;
            int? idDocenteSesion = SessionHelper.GetDocenteId(Session) ?? 0;

            ifcs = ifcs.Distinct().ToList();
            var result = new List<IfcResultViewModel>();

            foreach (var ifc in ifcs)
            {
                if (!esAdmin)
                {
                    if (!IfcPertenec(ifc.IdIFC, (int)idDocenteSesion)) continue;
                }

                var ifcVm = new IfcResultViewModel();
                ifcVm.IdIfc = ifc.IdIFC;
                ifcVm.Codigo = ifc.UnidadAcademica.CursoPeriodoAcademico.Curso.Codigo;
                ifcVm.Carrera = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                    GedServices.GetAreaForCursoServices(Context, ifc.IdUnidadAcademica).NombreEspanol :
                    GedServices.GetAreaForCursoServices(Context, ifc.IdUnidadAcademica).NombreIngles;
                ifcVm.SubModalidadPeriodoAcademicoId = ifc.UnidadAcademica.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
                ifcVm.PeriodoAcademico = ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
                ifcVm.Curso = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                    ifc.UnidadAcademica.CursoPeriodoAcademico.Curso.NombreEspanol :
                    ifc.UnidadAcademica.CursoPeriodoAcademico.Curso.NombreIngles;
                ifcVm.Estado = GedServices.GetTextoForCodigoEstadoIfcServices(ifc.Estado, CurrentCulture);
                ifcVm.CoordinadorCurso = ifc.Docente.Apellidos + ", " + ifc.Docente.Nombres;
                ifcVm.DisableEditar = false;
                ifcVm.DisableEliminar = false;
                if (!esAdmin)
                {
                    if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO
                        || ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO
                        || ifc.IdDocente != idDocenteSesion)
                        ifcVm.DisableEditar = true;
                    ifcVm.DisableEliminar = false;
                    if (ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO
                        || ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO
                        || ifc.Estado == ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.CODIGO
                        || ifc.IdDocente != idDocenteSesion)
                        ifcVm.DisableEliminar = true;
                }
                result.Add(ifcVm);
            }

            result = result.OrderBy(x => x.Codigo).ToList();

            return result;
        }

        private bool IfcPertenec(int idIfc, int idDocenteSesion)
        {
            var ifc = Context.IFC.First(x => x.IdIFC == idIfc);

            if (ifc.IdDocente == idDocenteSesion)
                return true;



            var EsNivel2 = (from d in CargarDatosContext().context.Docente
                            join uar in CargarDatosContext().context.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                            join sua in CargarDatosContext().context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                            join ua in CargarDatosContext().context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                            where ua.IdSubModalidadPeriodoAcademico == ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico && d.IdDocente == idDocenteSesion && ua.Nivel == 2
                            select ua.IdUnidadAcademica).ToList();

            foreach (var item in EsNivel2)
            {
                if (item == ifc.UnidadAcademica.IdUnidadAcademicaPadre)
                    return true;
            }

            var EsNivel1 = (from d in CargarDatosContext().context.Docente
                            join uar in CargarDatosContext().context.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                            join sua in CargarDatosContext().context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                            join ua in CargarDatosContext().context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                            where ua.IdSubModalidadPeriodoAcademico == ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico && d.IdDocente == idDocenteSesion && ua.Nivel == 1
                            select ua.IdUnidadAcademica).ToList();

            foreach (var item in EsNivel1)
            {
                if (item == ifc.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre)
                    return true;
            }


            var EsNivel0 = (from d in CargarDatosContext().context.Docente
                            join uar in CargarDatosContext().context.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                            join sua in CargarDatosContext().context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                            join ua in CargarDatosContext().context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                            where ua.IdSubModalidadPeriodoAcademico == ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico && d.IdDocente == idDocenteSesion && ua.Nivel == 0
                            select ua.IdUnidadAcademica).ToList();

            foreach (var item in EsNivel0)
            {
                //  if (item == ifc.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre)
                return true;
            }

            /* var EsNivel3 = (from d in CargarDatosContext().context.Docente
                             join uar in CargarDatosContext().context.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                             join sua in CargarDatosContext().context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                             join ua in CargarDatosContext().context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                             where ua.IdPeriodoAcademico == ifc.UnidadAcademica.IdPeriodoAcademico && d.IdDocente == idDocenteSesion
                             select d.IdDocente).ToList();

             if (EsNivel3.Count > 0)
                 return true;*/



            var unidadAcademica = ifc.UnidadAcademica;
            var subarea = unidadAcademica.UnidadAcademica2;
            var area = subarea.UnidadAcademica2;

            var curso = unidadAcademica.CursoPeriodoAcademico.Curso;
            //var idPeriodo = unidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico;
            var idSubModalidadPeriodoAcademico = unidadAcademica.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;

            var esCoordinadorCarrera = CargarDatosContext().context
                .UnidadAcademicaResponsable.Where(x => x.IdDocente == idDocenteSesion)
                .Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                .Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && x.IdCarreraPeriodoAcademico.HasValue).ToList();

            if (esCoordinadorCarrera.Count > 0)
            {
                var idCarrera = esCoordinadorCarrera[0].CarreraPeriodoAcademico.IdCarrera;
                var perteneceCarrera = CargarDatosContext().context
                    .CursoMallaCurricular.Where(x => x.IdCurso == curso.IdCurso
                    && x.MallaCurricular.IdCarrera == idCarrera)
                    .SelectMany(x => x.MallaCurricular.MallaCurricularPeriodoAcademico)
                    .Where(cc => cc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).ToList();
                if (perteneceCarrera.Count > 0)
                    return true;
            }

            var sedesDocente = (from uar in Context.UnidadAcademicaResponsable
                                join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                    sua.IdSedeUnidadAcademica
                                join s in Context.Sede on sua.IdSede equals s.IdSede
                                where sua.IdUnidadAcademica == ifc.IdUnidadAcademica
                                && uar.IdDocente == ifc.IdDocente
                                select s);
            var sedesSubarea = (from uar in Context.UnidadAcademicaResponsable
                                join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                    sua.IdSedeUnidadAcademica
                                join s in Context.Sede on sua.IdSede equals s.IdSede
                                where sua.IdUnidadAcademica == subarea.IdUnidadAcademica
                                      && uar.IdDocente == idDocenteSesion
                                select s);
            var interseccionSubarea = sedesDocente.Intersect(sedesSubarea).ToList();
            if (interseccionSubarea.Count > 0)
                return true;

            var sedesArea = (from uar in Context.UnidadAcademicaResponsable
                             join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals
                                 sua.IdSedeUnidadAcademica
                             join s in Context.Sede on sua.IdSede equals s.IdSede
                             where sua.IdUnidadAcademica == area.IdUnidadAcademica
                                 && uar.IdDocente == idDocenteSesion
                             select s);
            var interseccionArea = sedesDocente.Intersect(sedesArea).ToList();
            if (interseccionArea.Count > 0)
                return true;

            return false;
        }

        private IFC CrearIfc(CreateIfcViewModel vm, int idPeriodoAcademico)
        {
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == vm.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var SMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico);
            var ifc = new IFC();

            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            var unidadAcademica = CargarDatosContext().context.UnidadAcademica.FirstOrDefault(x => x.IdUnidadAcademica == vm.IdCursoUnidadAcademica);
            var curso = unidadAcademica.CursoPeriodoAcademico.Curso;

            FindLogic ContIndicador = new FindLogic(CargarDatosContext(), 1, SubModalidadPeriodoAcademicoId);

            vm.ResultadoAlcanzado = GetResultadoAlcanzado(GedServices.GetCursoMallaCurricularForIfcServices(Context, vm.IdCurso, idsubmoda).LogroFinCicloEspanol);
            ifc.ApreciacionDelegado = vm.ApreciacionDelegado;
            ifc.AprecioacionAlumnos = vm.ApreciacionAlumnos;
            ifc.ComentarioEncuesta = vm.ComentarioEncuesta;
            ifc.ResultadoAlcanzado = vm.ResultadoAlcanzado;
            ifc.Infraestructura = vm.Infraestructura;
            ifc.IdDocente = idDocenteSesion;
            ifc.IdUnidadAcademica = vm.IdCursoUnidadAcademica;
            ifc.IdInstrumento = Context.Instrumento.First(x => x.Acronimo == "IFC").IdInstrumento;
            ifc.FechaCreacion = DateTime.Now;

            if (vm.TipoSubmit == "GUARDAR")
                ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.CODIGO;
            else
            {
                ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO;
                ifc.IdDocenteAprobador = null;
            }

            var diccionarioHallazgo = new Dictionary<String, String>();
            var diccionarioIdentificador = new Dictionary<String, int>();

            var carreraComisionOutcome = CargarDatosContext().context.UnidadAcademicaResponsable
                          .Where(x => x.IdDocente == ifc.IdDocente).Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                          .Where(x => x.Nivel == 3 && x.IdUnidadAcademica == ifc.IdUnidadAcademica).Select(x => x.CursoPeriodoAcademico)
                          .Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.IdCurso == curso.IdCurso)
                          .Select(x => x.Curso.CursoMallaCurricular).FirstOrDefault().SelectMany(x => x.MallaCocosDetalle)
                          .Where(x => x.MallaCocos.IdSubModalidadPeriodoAcademico == idsubmoda).ToList()
                          .Select(c => new CarreraComisionOutcome
                          {
                              carrera = c.MallaCocos.CarreraComision.Carrera,
                              comision = c.OutcomeComision.Comision,
                              outcome = c.OutcomeComision.Outcome,
                          }).ToList();

            vm.ResultadoLogroCurso = GetResultadoLogrosCurso(carreraComisionOutcome);
            ifc.ResultadoLogroCurso = vm.ResultadoLogroCurso;
            Context.IFC.Add(ifc);
            Context.SaveChanges();
            // Para borrar los hallazgos repetidos, Ya no se crearán hallazgos para cada sede, por lo que solo me quedo con uno
            List<HallazgoViewModel> LstHallazgosBasura = new List<HallazgoViewModel>();

            if (vm.Hallazgos != null)
            {
                foreach (var hallazgosEnVM in vm.Hallazgos)
                {
                    if (hallazgosEnVM.Sede != "MO")
                    {
                        LstHallazgosBasura.Add(hallazgosEnVM);
                    }
                }

                foreach (var hallazgosBasura in LstHallazgosBasura)
                {
                    vm.Hallazgos.Remove(hallazgosBasura);
                }
            }

         
            var ConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.IdInstrumento == ifc.IdInstrumento);
            YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
            var hallazgos = new List<Hallazgos>();

            //CREAR LISTA OUTCOMES
            var outcomes = new List<Outcome>();
            //HASTA ACA

            if (vm.Hallazgos != null)
            {
                foreach (var hallazgoVm in vm.Hallazgos)
                {
                    //CARGAR LISTA OUTCOMES 
                    var idOutcome = hallazgoVm.idOutcome;

                    var lst = idOutcome.Split('|');
                    var lstOutcome = new List<String>(lst);

                    foreach (var item in lstOutcome)
                    {
                        var temp = Context.Outcome.ToList().Where(o => o.IdOutcome == item.ToInteger()).FirstOrDefault();
                        outcomes.Add(temp);
                    }
                    //HASTA ACA

                    if (hallazgoVm.Sede == "MO")
                    {
                        var Codigo = "HIFC-" + curso.Codigo;
                        var Identificador = ContIndicador.ObtenerIndiceHallazgo();

                        diccionarioHallazgo.Add(hallazgoVm.Codigo, Codigo);
                        diccionarioIdentificador.Add(hallazgoVm.Codigo, Identificador);

                        var hallazgo = new Hallazgos
                        {
                            IdSubModalidadPeriodoAcademicoModulo = SMPAM.IdSubModalidadPeriodoAcademicoModulo,
                            DescripcionEspanol = hallazgoVm.Descripcion,
                            DescripcionIngles = helper.Translate(hallazgoVm.Descripcion),
                            Codigo = Codigo,
                            IdConstituyenteInstrumento = ConstituyenteInstrumento.IdConstituyenteInstrumento,
                            HallazgoAccionMejora = new List<HallazgoAccionMejora>(),
                            IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.Where(x => x.NombreEspanol.Contains("Necesita mejora")).FirstOrDefault().IdNivelAceptacionHallazgo,
                            IdCriticidad = Context.Criticidad.Where(x => x.NombreEspanol == hallazgoVm.Criticidad).FirstOrDefault().IdCriticidad,
                            IdCurso = curso.IdCurso,
                            Estado = "ACT",
                            FechaRegistro = DateTime.Now,
                            IFCHallazgo = new List<IFCHallazgo>(),
                            IFCHallazgoOutcome = new List<IFCHallazgoOutcome>(),
                            Identificador = Identificador
                        };

                        //NUEVO
                        hallazgos.Add(hallazgo);
                        Context.Hallazgos.Add(hallazgo);
                        Context.SaveChanges();

                        foreach (var outcome in outcomes)
                        {
                            var ifcoutcome = new IFCHallazgoOutcome
                            {
                                IdHallazgo = hallazgo.IdHallazgo,
                                IdOutcome = outcome.IdOutcome
                            };
                            Context.IFCHallazgoOutcome.Add(ifcoutcome);
                        }
                        Context.SaveChanges();
                        //--------------------------------------------------
                    }
                    outcomes.Clear();
                }
            }
            //Context.Hallazgos.AddRange(hallazgos);
            //Context.SaveChanges();

            if (vm.AccionesPropuestas != null)
            {
                foreach (var accionVm in vm.AccionesPropuestas)
                {
                    var Identificador = ContIndicador.ObtenerIndiceAccionMejora();
                    var Codigo = "AIFC-" + curso.Codigo;
                    var accion = new AccionMejora
                    {
                        DescripcionEspanol = accionVm.Descripcion,
                        DescripcionIngles = helper.Translate(accionVm.Descripcion),
                        Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO,
                        Codigo = Codigo,
                        Anio = ifc.FechaCreacion.Year,
                        IdSubModalidadPeriodoAcademico = idsubmoda,
                        IdEscuela = EscuelaId,
                        ParaPlan = false,
                        Identificador = Identificador,
                        HallazgoAccionMejora = new List<HallazgoAccionMejora>()

                    };

                    foreach (var hallazgo in hallazgos)
                    {
                        foreach (var hallazgoVM in vm.Hallazgos)
                        {
                            var auxIdentificador = diccionarioIdentificador[accionVm.CodigoHallazgo];
                            if (hallazgo.Identificador.Equals(auxIdentificador))
                            {
                                //  accion.Codigo = accion.Codigo + '-' + auxIdentificador.ToString();
                                var hallazgoAccion = new HallazgoAccionMejora
                                {
                                    AccionMejora = accion,
                                    Hallazgos = hallazgo
                                };

                                accion.HallazgoAccionMejora.Add(hallazgoAccion);
                                hallazgo.HallazgoAccionMejora.Add(hallazgoAccion);
                                Context.SaveChanges();
                                break;
                            }
                        }
                    }
                }
            }

            Context.SaveChanges();
            ifc.IFCHallazgo = new List<IFCHallazgo>();

            foreach (var hallazgo in hallazgos)
            {
                var ifcHallazgo = new IFCHallazgo
                {
                    Hallazgos = hallazgo,
                    IFC = ifc,
                };

                ifc.IFCHallazgo.Add(ifcHallazgo);
                hallazgo.IFCHallazgo.Add(ifcHallazgo);
            }

            //ifc.IFCOutcome = new List<IFCOutcome>();

            //foreach (var outcome in outcomes)
            //{
            //    var ifcoutcome = new IFCOutcome
            //    {
            //        IdIFC = ifc.IdIFC,
            //        IdOutcome = outcome.IdOutcome
            //    };
            //    Context.IFCOutcome.Add(ifcoutcome);
            //}

            Context.SaveChanges();

            return ifc;
        }

        private bool CrearIfcCARGAMASIVA(CreateIfcViewModel vm, int parIdSubModalidadPeriodoAcademico)
        {
                try
                {
                    var ifc = new IFC();
                    var unidadAcademica = CargarDatosContext().context.UnidadAcademica.FirstOrDefault(x => x.IdUnidadAcademica == vm.IdCursoUnidadAcademica);
                    var curso = unidadAcademica.CursoPeriodoAcademico.Curso;
                    int idDocenteSesion = vm.IdDocente;
                    FindLogic ContIndicador = new FindLogic(CargarDatosContext(), 1, parIdSubModalidadPeriodoAcademico);                                               // RML001

                    var SMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico);

                    vm.ResultadoAlcanzado = GetResultadoAlcanzado(GedServices.GetCursoMallaCurricularForIfcServices(Context, vm.IdCurso, parIdSubModalidadPeriodoAcademico).LogroFinCicloEspanol);
                    ifc.ApreciacionDelegado = vm.ApreciacionDelegado;
                    ifc.AprecioacionAlumnos = vm.ApreciacionAlumnos;
                    ifc.ComentarioEncuesta = vm.ComentarioEncuesta;
                    ifc.ResultadoAlcanzado = vm.ResultadoAlcanzado;
                    ifc.Infraestructura = vm.Infraestructura;
                    ifc.IdDocente = idDocenteSesion;
                    ifc.IdUnidadAcademica = vm.IdCursoUnidadAcademica;
                    ifc.IdInstrumento = Context.Instrumento.First(x => x.Acronimo == "IFC").IdInstrumento;
                    ifc.IdDocenteAprobador = Context.SedeUnidadAcademica
                                            .Where(x => x.UnidadAcademica.IdUnidadAcademica == vm.IdAreaUnidadAcademica)
                                            .SelectMany(x => x.UnidadAcademicaResponsable).Select(o => o.Docente).FirstOrDefault().IdDocente;

                    ifc.FechaCreacion = DateTime.Now;

                    if (vm.TipoSubmit == "GUARDAR")
                        ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO;
                    else
                    {
                        ifc.Estado = ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO;
                        ifc.IdDocenteAprobador = null;
                    }

                    var carreraComisionOutcome = CargarDatosContext().context.UnidadAcademicaResponsable
                                  .Where(x => x.IdDocente == ifc.IdDocente).Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                                  .Where(x => x.Nivel == 3 && x.IdUnidadAcademica == ifc.IdUnidadAcademica).Select(x => x.CursoPeriodoAcademico)
                                  .Where(x => x.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico && x.IdCurso == curso.IdCurso)
                                  .Select(x => x.Curso.CursoMallaCurricular).FirstOrDefault().SelectMany(x => x.MallaCocosDetalle)
                                  .Where(x => x.MallaCocos.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico).ToList()
                                  .Select(c => new CarreraComisionOutcome
                                  {
                                      carrera = c.MallaCocos.CarreraComision.Carrera,
                                      comision = c.OutcomeComision.Comision,
                                      outcome = c.OutcomeComision.Outcome,
                                  }).ToList();

                    vm.ResultadoLogroCurso = GetResultadoLogrosCurso(carreraComisionOutcome);
                    ifc.ResultadoLogroCurso = vm.ResultadoLogroCurso;


                    /*insert ifc*/
                    Context.IFC.Add(ifc);
                    Context.SaveChanges();

                    var ConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.IdInstrumento == ifc.IdInstrumento);
                    var hallazgos = new List<Hallazgos>();
                    if (vm.Hallazgos != null)
                    {
                        if (carreraComisionOutcome != null)
                        {
                            //     foreach (var cco in carreraComisionOutcome)
                            //     {
                            foreach (var hallazgoVm in vm.Hallazgos)
                            {
                                hallazgos.Add(new Hallazgos
                                {
                                    IdSubModalidadPeriodoAcademicoModulo = SMPAM.IdSubModalidadPeriodoAcademicoModulo,
                                    DescripcionEspanol = hallazgoVm.Descripcion,
                                    DescripcionIngles = hallazgoVm.Descripcion,
                                    Codigo = hallazgoVm.Codigo,
                                    IdConstituyenteInstrumento = ConstituyenteInstrumento.IdConstituyenteInstrumento,
                                    HallazgoAccionMejora = new List<HallazgoAccionMejora>(),
                                    IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.Where(x => x.NombreEspanol.Contains("Necesita mejora")).FirstOrDefault().IdNivelAceptacionHallazgo,
                                    IdCurso = curso.IdCurso,
                                    IdCriticidad = 3,
                                    Identificador = ContIndicador.ObtenerIndiceHallazgo(),
                                    FechaRegistro = DateTime.Now,
                                    IFCHallazgo = new List<IFCHallazgo>()
                                });
                            }
                            //   }
                        }
                        else
                        {
                            foreach (var hallazgoVm in vm.Hallazgos)
                            {
                                hallazgos.Add(new Hallazgos
                                {
                                    IdSubModalidadPeriodoAcademicoModulo = SMPAM.IdSubModalidadPeriodoAcademicoModulo,
                                    DescripcionEspanol = hallazgoVm.Descripcion,
                                    DescripcionIngles = hallazgoVm.Descripcion,
                                    Codigo = hallazgoVm.Codigo,
                                    IdConstituyenteInstrumento = ConstituyenteInstrumento.IdConstituyenteInstrumento,
                                    HallazgoAccionMejora = new List<HallazgoAccionMejora>(),
                                    IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.Where(x => x.NombreEspanol.Contains("Necesita mejora")).FirstOrDefault().IdNivelAceptacionHallazgo,
                                    IdCurso = curso.IdCurso,
                                    IdCriticidad = 3,
                                    Identificador = ContIndicador.ObtenerIndiceHallazgo(),
                                    FechaRegistro = DateTime.Now,
                                    IFCHallazgo = new List<IFCHallazgo>()
                                });
                            }
                        }
                    }

                    /*insert hallazgos*/
                    Context.Hallazgos.AddRange(hallazgos);
                    Context.SaveChanges();


                    if (vm.AccionesPropuestas != null)
                    {
                        foreach (var accionVm in vm.AccionesPropuestas)
                        {
                            var accion = new AccionMejora
                            {
                                DescripcionEspanol = accionVm.Descripcion,
                                DescripcionIngles = accionVm.Descripcion,
                                Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO,
                                Codigo = accionVm.Codigo,
                                Identificador = ContIndicador.ObtenerIndiceAccionMejora(),
                                Anio = ifc.FechaCreacion.Year,
                                IdSubModalidadPeriodoAcademico = parIdSubModalidadPeriodoAcademico,
                                ParaPlan = true,
                                IdEscuela = Session.GetEscuelaId(),

                                HallazgoAccionMejora = new List<HallazgoAccionMejora>()
                            };

                            foreach (var hallazgosVM in vm.Hallazgos)
                            {
                                if (hallazgosVM.CorrelativoHallazgoTemporal == accionVm.CorrelativoHallazgoTemporal)
                                {
                                    foreach (var hallazgo in hallazgos)
                                    {
                                        if (hallazgosVM.Descripcion == hallazgo.DescripcionEspanol)                        // Se asume que las descripciones del hallazgo son diferentes, siempre
                                        {
                                            var hallazgoAccion = new HallazgoAccionMejora
                                            {
                                                AccionMejora = accion,
                                                Hallazgos = hallazgo
                                            };

                                            accion.HallazgoAccionMejora.Add(hallazgoAccion);
                                            hallazgo.HallazgoAccionMejora.Add(hallazgoAccion);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    ifc.IFCHallazgo = new List<IFCHallazgo>();

                    foreach (var hallazgo in hallazgos)
                    {
                        var ifcHallazgo = new IFCHallazgo
                        {
                            Hallazgos = hallazgo,
                            IFC = ifc,
                        };

                        ifc.IFCHallazgo.Add(ifcHallazgo);
                        hallazgo.IFCHallazgo.Add(ifcHallazgo);
                    }

                    Context.SaveChanges();


                    return true;
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    return false;
                }
        }

        private string GetResultadoLogrosCurso(List<CarreraComisionOutcome> carreraComisionOutcome)
        {
            var masDeUno = false;
            if (carreraComisionOutcome.Count > 1)
                masDeUno = true;

            var parrafoLogroOutcomes = "<p style='text-align: justify;'>" + ControllerIFCManagementResource.LogroCursoParte1 + "</p>";


            if (masDeUno == false)
                parrafoLogroOutcomes +=
                    "<p style='text-align: justify;'>" + ControllerIFCManagementResource.LogroCursoParte2UnOutcome + "</p>";
            else
                parrafoLogroOutcomes +=
                    "<p style='text-align: justify;'>" + ControllerIFCManagementResource.LogroCursoParte2VariosOutcomes + "</p>";

            parrafoLogroOutcomes += "<p style = 'text-align: justify;'>";
            parrafoLogroOutcomes += "<ul style='text-align: justify;'>";

            foreach (var cco in carreraComisionOutcome)
            {
                if (CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL)
                {
                    parrafoLogroOutcomes += "<li style='text-align: justify;'>Student Outcome (" + cco.carrera.NombreEspanol.ToUpper() + " - " +
                                        cco.getComisionEspañol().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \"" + cco.outcome.DescripcionEspanol + "\"</li>";
                }
                else
                {
                    parrafoLogroOutcomes += "<li style='text-align: justify;'>Student Outcome (" + cco.carrera.NombreIngles.ToUpper() + " - " +
                                        cco.getComisionIngles().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \"" + cco.outcome.DescripcionIngles + "\"</li>";
                }
            }

            parrafoLogroOutcomes += "</ul>";
            parrafoLogroOutcomes += "</p>";

            return parrafoLogroOutcomes;
        }

        private string GetResultadoAlcanzado(string resultado)
        {
            var resultadoAlcanzado = String.Empty;

            resultadoAlcanzado += "<p style = 'text-align: justify;'>" + ControllerIFCManagementResource.ResultadoAlcanzado + "</p>";

            resultadoAlcanzado += "<p style = 'text-align: justify;'>" + resultado + "</p>";

            return resultadoAlcanzado;
        }

        [HttpPut]
        public ActionResult Edit()
        {
            return View();
        }

        const string LogoUpc = "logo-upc-red.png";

        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera)]
        public ActionResult CargaMasivaIFC()
        {
            return View();
        }

        public bool PerteneceaPlan(int accionmejoraid)
        {
            try
            {
                var planaccion = Context.PlanMejoraAccion.Where(x => x.IdAccionMejora == accionmejoraid).ToList();

                if (planaccion.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.CoordinadorCarrera)]
        [HttpPost]
        public ActionResult CargaMasivaIFC(HttpPostedFileBase file)
        {
            try
            {
                DataSet ds = new DataSet();
                if (file != null && file.ContentLength > 0)
                {
                    var nombreArchivoTemp = "CargaMasivaIFC-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + Path.GetExtension(file.FileName);
                    var rutaArchivoTemp = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.CARGA_MASIVA_IFC_DIRECTORY, nombreArchivoTemp);
                    string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.CARGA_MASIVA_IFC_DIRECTORY);
                    Directory.CreateDirectory(directory);

                    file.SaveAs(rutaArchivoTemp);

                    List<String> lstSedes = Context.Sede.Select(x => x.Codigo).ToList();

                    String fileExtension = System.IO.Path.GetExtension(file.FileName);
                    if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".xlsm")
                    {
                        file.InputStream.Position = 0;

                        IExcelDataReader excelReader = null;

                        if (fileExtension == ".xls")
                            excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                        else if (fileExtension == ".xlsx")
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                        else if (fileExtension == ".xlsm")
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);

                        ds = excelReader.AsDataSet();
                        var dataTable = ds.Tables[0]; // HOJA 0

                        //RECORRER DATA

                        int validacion = ExisteIFC(dataTable);
                        if (validacion == 1 || validacion == -1)
                        {
                            Response.TrySkipIisCustomErrors = true;
                            Response.StatusCode = 400;
                            return validacion == 1 ? Content(file.FileName + ": Ya se encuentra cargado el IFC. ")
                                : Content(file.FileName + ": Se encuentra vacio la plantilla. ");
                        }

                        //RECORREMOS TODA LA DATA VALIDANDO

                        CreateIfcViewModel ViewModelcreateIfc = new CreateIfcViewModel();
                        ///Sacar data de la seccion IFC del excel para pasarla al ViewModel
                        ///
                        int paridModalidad = Session.GetModalidadId();
                        int paridSubModalidadPeriodoAcademico = 0;
                        var cicloAcademico = "";
                        var codigoCurso = "";
                        int columnaInicio = 2;
                        int idPeriodoAcademico = 0;
                        List<HallazgoViewModel> lsthallazgos = new List<HallazgoViewModel>();
                        List<AccionMejoraViewModel> lstaccionMejora = new List<AccionMejoraViewModel>();

                        PeriodoAcademico periodoAcademico = null;
                        Curso curso = null;
                        CursoPeriodoAcademico cursoPeriodoAcademico = null;
                        UnidadAcademica unidadAcademica = null;
                        Docente docente = null;         

                        for (int i = 1; i < dataTable.Rows.Count; i++)
                        {

                            if (dataTable.Rows[i][0].ToString() == "IFC")
                            {
                                cicloAcademico = dataTable.Rows[i][3].ToString();
                                codigoCurso = dataTable.Rows[i + 1][3].ToString();

                                //PERIODO ACADEMICO QUE SE INDICA EN EL EXCEL EN LA MODALIDAD DE LA SESSION
                                periodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.CicloAcademico == cicloAcademico && x.SubModalidad.IdModalidad == paridModalidad).FirstOrDefault().PeriodoAcademico;
                                //SUBMODALIDADPERIODOACADEMICO DEL EXCEL
                                paridSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademico.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                                if (periodoAcademico == null)
                                {
                                    Response.TrySkipIisCustomErrors = true;

                                    Response.StatusCode = 400;
                                    return Content(file.FileName + ": Ingrese un Ciclo Academico Valido.");
                                }
                                idPeriodoAcademico = periodoAcademico.IdPeriodoAcademico;

                                curso = Context.Curso.Where(x => x.Codigo == codigoCurso).FirstOrDefault();
                                if (curso == null)
                                {
                                    Response.TrySkipIisCustomErrors = true;
                                    Response.StatusCode = 400;
                                    return Content(file.FileName + ": Ingrese un Codigo Curso Valido.");
                                }
                                cursoPeriodoAcademico = Context.CursoPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == paridSubModalidadPeriodoAcademico && x.IdCurso == curso.IdCurso).FirstOrDefault();

                                if (cursoPeriodoAcademico == null)
                                {
                                    Response.TrySkipIisCustomErrors = true;
                                    Response.StatusCode = 400;
                                    return Content(file.FileName + ": El codigo del curso no pertenece a ese Ciclo Academico.");
                                }

                                unidadAcademica = Context.UnidadAcademica.Where(x => x.IdSubModalidadPeriodoAcademico == paridSubModalidadPeriodoAcademico
                                                        && x.CursoPeriodoAcademico.IdCurso == curso.IdCurso).FirstOrDefault();

                                if (unidadAcademica == null)
                                {
                                    Response.TrySkipIisCustomErrors = true;
                                    Response.StatusCode = 400;
                                    return Content(file.FileName + ": No existe la unidad academica para ese curso en ese ciclo.");
                                }

                                docente = Context.UnidadAcademica.First(x => x.IdUnidadAcademica == unidadAcademica.IdUnidadAcademica)
                                                .SedeUnidadAcademica.SelectMany(x => x.UnidadAcademicaResponsable).Select(x => x.Docente).FirstOrDefault();


                                ViewModelcreateIfc.IdDocente = docente.IdDocente;
                                ViewModelcreateIfc.IdCursoUnidadAcademica = unidadAcademica.IdUnidadAcademica;
                                ViewModelcreateIfc.IdSubModalidadPeriodoAcademico = paridSubModalidadPeriodoAcademico;
                                ViewModelcreateIfc.CodigoCurso = codigoCurso.ToString();
                                ViewModelcreateIfc.IdCurso = curso.IdCurso;
                                ViewModelcreateIfc.TipoSubmit = "GUARDAR";

                                var subarea = unidadAcademica.UnidadAcademica2;
                                var area = subarea.UnidadAcademica2;
                                var resultadoAlcanzado = GetResultadoAlcanzado(GedServices.GetCursoMallaCurricularForIfcServices(Context, curso.IdCurso, paridSubModalidadPeriodoAcademico).LogroFinCicloEspanol);

                                ViewModelcreateIfc.NombreArea = area.NombreEspanol;
                                ViewModelcreateIfc.ResultadoAlcanzado = resultadoAlcanzado;
                                ViewModelcreateIfc.IdAreaUnidadAcademica = area.IdUnidadAcademica;
                            }

                            if (dataTable.Rows[i][0].ToString() == "HALLAZGOS")
                            {

                               // for (int sedesIterator = 0; sedesIterator < lstSedes.Count; sedesIterator++)
                               // {
                                 
                                    int fila = i + 1;
                                    while (dataTable.Rows[fila][columnaInicio].ToString() != "")
                                    {
                                        var codigoHallazgo = dataTable.Rows[fila][columnaInicio];
                                        var descripcion = dataTable.Rows[fila][columnaInicio + 1];

                                        var correlativoHallazgoTemp = dataTable.Rows[fila][columnaInicio];                                                                  // TEST 0001
 
                                    HallazgoViewModel hallazgoViewModel = new HallazgoViewModel();
                                    //  hallazgoViewModel.Codigo = "IFC-" + codigoCurso + "-" + cicloAcademico + "-" + "F-" + (codigoHallazgo.ToString()).PadLeft(3, '0');  // RML001
                                        hallazgoViewModel.Codigo = "HIFC-" + codigoCurso;
                                        hallazgoViewModel.Descripcion = descripcion.ToString();
                                    //   hallazgoViewModel.Sede = lstSedes[sedesIterator];                                                                                   // RML001
                                        hallazgoViewModel.CorrelativoHallazgoTemporal = correlativoHallazgoTemp.ToInteger();                                                    // TEST 0001
                                        lsthallazgos.Add(hallazgoViewModel);
                                        fila++;
                                    }
                               // }
                                ViewModelcreateIfc.Hallazgos = lsthallazgos;
                            }
                            //CODIGOCURSO-CICLOACADEMICO-F-001 number.ToString().PadLeft(6, '0');

                            if (dataTable.Rows[i][0].ToString() == "ACCION_MEJORA")
                            {
                                int fila = i + 1;

                                while (dataTable.Rows[fila][columnaInicio].ToString() != "")
                                {
                                    var codigoAccionMejora = dataTable.Rows[fila][columnaInicio];
                                    var descripcion = dataTable.Rows[fila][columnaInicio + 1];
                                    var codigoHallazgo = dataTable.Rows[fila][columnaInicio + 5];

                                    var correlativoHallazgoTemporal = dataTable.Rows[fila][columnaInicio + 5];                                                                      // TEST 0001

                                    AccionMejoraViewModel accionMejoraViewModel = new AccionMejoraViewModel();
                                   // accionMejoraViewModel.Codigo = "IFC-" + codigoCurso + "-" + cicloAcademico + "-" + "A-" + (codigoHallazgo.ToString()).PadLeft(3, '0');                // RML001
                                    accionMejoraViewModel.Codigo = "HIFC-" + codigoCurso;
                                    accionMejoraViewModel.Descripcion = descripcion.ToString();
                                  //  accionMejoraViewModel.CodigoHallazgo = "IFC-" + codigoCurso + "-" + cicloAcademico + "-" + "F-" + (codigoHallazgo.ToString()).PadLeft(3, '0');        // RML001
                                    accionMejoraViewModel.CodigoHallazgo = "AIFC-" + codigoCurso;
                                    accionMejoraViewModel.CorrelativoHallazgoTemporal = correlativoHallazgoTemporal.ToInteger();                                                        // TEST 0001
                                    lstaccionMejora.Add(accionMejoraViewModel);
                                    fila++;
                                }

                                ViewModelcreateIfc.AccionesPropuestas = lstaccionMejora;
                            }

                            if (dataTable.Rows[i][0].ToString() == "INFRAESTRUCTURA")
                            {
                                ViewModelcreateIfc.Infraestructura = dataTable.Rows[i + 1][columnaInicio].ToString();
                            }

                            if (dataTable.Rows[i][0].ToString() == "ALUMNOS")
                            {
                                ViewModelcreateIfc.ApreciacionAlumnos = dataTable.Rows[i + 1][columnaInicio].ToString();
                            }

                            if (dataTable.Rows[i][0].ToString() == "DELEGADO")
                            {
                                ViewModelcreateIfc.ApreciacionDelegado = dataTable.Rows[i + 1][columnaInicio].ToString();
                            }

                            if (dataTable.Rows[i][0].ToString() == "COMENTARIO")
                            {
                                ViewModelcreateIfc.ComentarioEncuesta = dataTable.Rows[i + 1][columnaInicio].ToString();
                            }
                        }
                        bool success = CrearIfcCARGAMASIVA(ViewModelcreateIfc, paridSubModalidadPeriodoAcademico);

                        if (success)
                        {
                            return Content("Éxito");
                        }
                        else
                        {
                            Response.TrySkipIisCustomErrors = true;
                            Response.StatusCode = 400;
                            return Content(file.FileName + ": No se pudo guardar el archivo. ");
                        }
                    }
                }
                else
                {
                    string directory = Server.MapPath(ConstantHelpers.PROFESSOR.PLANTILLA_IFC_DIRECTORY);
                    string filename = "IFC-Plantilla.xlsm";
                    string path = Path.Combine(directory, filename);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
                }
            }
            catch (Exception ex)
            {
                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 400;
                return Content(file.FileName + ": No se pudo guardar el archivo. " + ex.Message);

            }

            return Content("Éxito");

        }

        public int ExisteIFC(DataTable dataTable)
        {
            bool iterar = true;
            int i = 0;
            String cicloAcademico = "";
            String codigoCurso = "";
            while (iterar)
            {
                i++;
                if (dataTable.Rows[i][0].ToString() == "IFC")
                {
                    iterar = false;
                    cicloAcademico = dataTable.Rows[i][3].ToString();
                    codigoCurso = dataTable.Rows[i + 1][3].ToString();
                }
            }
            if (cicloAcademico == "" || codigoCurso == "")
                return -1;
            return ExisteIFC_Ciclo(cicloAcademico, codigoCurso);
        }

        public int ExisteIFC_Ciclo(String cicloAcademico, String codigoCurso)
        {
            //SUBMODALIDAD EN BASE AL CICLO DEL EXCEL Y LA SESSION DE LA MODALIDAD
            int parIdModalidad = Session.GetModalidadId();
            int parIdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.CicloAcademico == cicloAcademico && x.SubModalidad.IdModalidad == parIdModalidad).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            IFC ifc = Context.IFC.FirstOrDefault(x => x.UnidadAcademica.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico && x.UnidadAcademica.CursoPeriodoAcademico.Curso.Codigo == codigoCurso);

            if (ifc != null)
                return 1;
            return 0;
        }
    }

    public class CarreraComisionOutcome
    {
        public Carrera carrera { get; set; }
        public Comision comision { get; set; }
        public Outcome outcome { get; set; }

        public CarreraComisionOutcome()
        {

        }

        public CarreraComisionOutcome(Carrera _carrera, Comision _comision, Outcome _outcome)
        {
            this.carrera = _carrera;
            this.comision = _comision;
            this.outcome = _outcome;
        }

        public String getComisionEspañol()
        {
            String _nombreComision = "";
            switch (comision.Codigo)
            {
                case "CAC": case "CAC-CC": _nombreComision = "COMPUTACIÓN"; break;
                case "EAC": _nombreComision = "INGENIERÍA"; break;
                case "WASC": _nombreComision = "WASC"; break;
                default: _nombreComision = comision.NombreEspanol; break;
            }
            return _nombreComision;
        }

        public String getComisionIngles()
        {
            String _nombreComision = "";
            switch (comision.Codigo)
            {
                case "CAC":
                case "CAC-CC": _nombreComision = "COMPUTING"; break;
                case "EAC": _nombreComision = "ENGINEERING"; break;
                case "WASC": _nombreComision = "WASC"; break;
                default: _nombreComision = comision.NombreIngles; break;
            }
            return _nombreComision;
        }

    }

}