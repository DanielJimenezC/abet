﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.Docente)]
    public class PlanMejoraController : BaseController
    {
        // GET: Professor/AssessmentPlan
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditPlanMejora(Int32? IdPlanMejora)
        {

            return View();
        }


        public ActionResult MonitorearPlan(Int32 IdPlanAccion)
        {
            var viewModel = new MonitorearPlanViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdPlanAccion);
            return View(viewModel);
        }

        public ActionResult LstAccionesMejora(Int32 IdPlanAccion, Int32? IdCurso, Int32? IdSubModalidadPeriodoAcademico, Int32? IdSede, Int32? IdNivelAceptacion)
        {
            var viewModel = new LstAccionesMejoraViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdPlanAccion, EscuelaId, IdCurso, IdSede, IdNivelAceptacion, IdSubModalidadPeriodoAcademico);
            //CargarDatos No realiza ninguna acción
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LstAccionesMejora(LstAccionesMejoraViewModel model)
        {

            return RedirectToAction("LstAccionesMejora", "AssessmentPlan", new { IdPlanAccion = model.IdPlanAccion, IdCurso = model.IdCurso, model.IdSubModalidadPeriodoAcademico, IdSede = model.IdSede, IdNivelAceptacion = model.IdNivelAceptacion });
            // var viewModel = new LstAccionesMejoraViewModel();
            //viewModel.CargarDatos(CargarDatosContext(), model.IdPlanAccion, model, model.IdCurso, model.IdPeriodoAcademico, escuelaId);
            // return View(viewModel);
        }

        public ActionResult LstPlanesAssessment()
        {
            var viewModel = new LstPlanesAssessmentViewModel();
            //SUBMODALIDAD=1
            viewModel.CargarDatos(CargarDatosContext(), EscuelaId, null, null, null, null, 1);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult LstPlanesAssessment(LstPlanesAssessmentViewModel model)
        {
            // var viewModel = new LstPlanesAssessmentViewModel();
            return View();
            // return RedirectToAction("LstPlanesAssessment", "AssessmentPlan", new { NumeroPagina = model.NumeroPagina, Anio = model.Anio, IdCarrera = model.IdCarrera });
            /* viewModel.CargarDatos(CargarDatosContext(), model.NumeroPagina, Session.GetPeriodoAcademicoId().Value, escuelaId, model.Anio, model.IdCarrera);
             return View(viewModel);*/
        }

        public ActionResult AddEditAssessmentPlan(int? IdPlanAccion)
        {
            var viewmodel = new AddEditAssessmentPlanViewModel();
            if (IdPlanAccion.HasValue == false)
                IdPlanAccion = 0;

            viewmodel.Fill(CargarDatosContext(), Session.GetSubModalidadPeriodoAcademicoId().Value, EscuelaId, IdPlanAccion.Value);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddEditAssessmentPlan(AddEditAssessmentPlanViewModel model)
        {
            try
            {
                PlanAccion plan = new PlanAccion();
                Carrera carrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera);

                plan = Context.PlanAccion.FirstOrDefault(x => x.Anio == model.Anio && x.IdCarrera == model.IdCarrera && x.Estado != "INA");
                if (plan != null && plan.IdPlanAccion != model.IdPlanAccion)
                {
                    PostMessage(MessageType.Error, MessageResource.OperacionCanceladaYaExisteUnPlanDeAssessmentParaLaCarrera + " " + carrera.NombreEspanol + " " + MessageResource.EnElAnio + " " + model.Anio);
                    return RedirectToAction("AddEditAssessmentPlan", "AssessmentPlan", new { IdPlanAccion = model.IdPlanAccion });
                }

                plan = new PlanAccion
                {
                    IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico,
                    Anio = model.Anio,
                    IdCarrera = model.IdCarrera,
                    Estado = "ACT"
                };

                if (model.IdPlanAccion == 0)
                {
                    Context.PlanAccion.Add(plan);
                }

                Context.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.ProcesoRealizadoCorrectamente);

                return RedirectToAction("LstPlanesAssessment", "AssessmentPlan");

            }
            catch (DbEntityValidationException ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnEventoInesperadoPorFavorIntentarDeNuevoMasTarde + "\n " + MessageResource.Evento + ": " + ex.Message);
                return RedirectToAction("AddEditAssessmentPlan", "AssessmentPlan", new { IdPlanAccion = model.IdPlanAccion });
            }

        }

        public ActionResult EliminarPlanAssessment(Int32 IdPlanAccion)
        {
            var planAccion = Context.PlanAccion.FirstOrDefault(X => X.Estado != "INA" && X.IdPlanAccion == IdPlanAccion);
            if (planAccion == null)
            {
                PostMessage(MessageType.Error, MessageResource.NoSeEncontroElPlanDeAssessment);
                return RedirectToAction("LstPlanesAssessment", "AssessmentPlan");
            }
            try
            {
                var accionesAsociadas = planAccion.AccionMejoraPlanAccion.ToList();
                Context.AccionMejoraPlanAccion.RemoveRange(accionesAsociadas);
                planAccion.Estado = "INA";
                // context.PlanAccion.Remove(planAccion);
                Context.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.SeEliminoCorrectamenteElPlanDeAssessment);

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnErrorAlIntentarEliminarPlanDeAssessment + ".\n " + MessageResource.DetalleTecnico + ": " + ex.Message);
            }
            return RedirectToAction("LstPlanesAssessment", "AssessmentPlan");
        }

        public ActionResult CerrarPlanAssessment(Int32 IdPlanAccion)
        {
            var planAccion = Context.PlanAccion.FirstOrDefault(X => X.IdPlanAccion == IdPlanAccion && X.Estado == "ACT");
            if (planAccion != null)
            {
                planAccion.Estado = "CER";
                var accionesMejora = planAccion.AccionMejoraPlanAccion.Select(x => x.AccionMejora);
                foreach (var accion in accionesMejora)
                {
                    accion.Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PROCESADO.CODIGO;
                }
                try
                {
                    Context.SaveChanges();
                }
                catch (Exception ex)
                {
                    PostMessage(MessageType.Error, MessageResource.OcurrioUnErrorAlTratarDeActualizarElEstado + ".\n" + MessageResource.Detalle + ":" + ex.Message);
                }
            }
            return RedirectToAction("LstPlanesAssessment", "AssessmentPlan");
        }

        [HttpPost]
        public ActionResult RelacionarAccionesMejora(LstAccionesMejoraViewModel model)
        {

            try
            {
                // List<AccionMejoraPlanAccion> lstPrevia = new List<AccionMejoraPlanAccion>();
                // lstPrevia = context.AccionMejoraPlanAccion.Where(x => x.IdPlanAccion == model.IdPlanAccion).ToList();
                // context.AccionMejoraPlanAccion.RemoveRange(lstPrevia);

                for (int i = 0; i < model.LstAccionesMejora.Count; i++)
                {
                    AccionMejoraPlanAccion ampa = new AccionMejoraPlanAccion();
                    int idAccionMejora = model.LstAccionesMejora[i].IdAccionMejora;
                    ampa = Context.AccionMejoraPlanAccion.FirstOrDefault(x => x.IdPlanAccion == model.IdPlanAccion && x.IdAccionMejora == idAccionMejora);
                    if (ampa != null)
                        Context.AccionMejoraPlanAccion.Remove(ampa);

                    if (model.Respuestas[i])
                    {

                        ampa = new AccionMejoraPlanAccion();
                        ampa.IdPlanAccion = model.IdPlanAccion;
                        ampa.IdAccionMejora = idAccionMejora;

                        Context.AccionMejoraPlanAccion.Add(ampa);
                    }
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.OcurrioUnEventoInesperadoPorFavorIntentarDeNuevoMasTarde + "\n " + MessageResource.Evento + ": " + ex.Message);
                return RedirectToAction("LstAccionesMejora", "AssessmentPlan", new { IdPlanAccion = model.IdPlanAccion, IdCurso = model.IdCurso, model.IdSubModalidadPeriodoAcademico });
            }

            PostMessage(MessageType.Success, MessageResource.DatosGuardadosCorrectamente);
            return RedirectToAction("LstAccionesMejora", "AssessmentPlan", new { IdPlanAccion = model.IdPlanAccion, IdCurso = model.IdCurso, model.IdSubModalidadPeriodoAcademico });
        }
    }
}