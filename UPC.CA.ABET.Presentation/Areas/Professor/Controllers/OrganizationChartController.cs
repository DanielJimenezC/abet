﻿using System;
using System.Collections.Generic;
using System.Configuration.Internal;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.OrganizationChart;
using UPC.CA.ABET.Models;
using System.Transactions;
using System.Data;
using UPC.CA.ABET.Logic.Areas.Upload;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class OrganizationChartController : BaseController
    {
        protected UploadLogic uploadLogic;

        //
        // GET: /Professor/OrganizationChart/
        public ActionResult Index()
        {
            return View();
        }
        public OrganizationChartController()
        {
            uploadLogic = new UploadLogic(CargarDatosContext().context);
        }


        public ActionResult Management()
        {
            return View();
        }

        public ActionResult Organigrama(int? idperiodoacademico, string estado)
        {
            OrganigramaViewModel model = new OrganigramaViewModel();

            model.CargarDatos(CargarDatosContext(), idperiodoacademico ?? PeriodoAcademicoId, EscuelaId, ModalidadId);

            ViewBag.Paselected = model.IdPeriodoAcademico;

            if (estado != null)
                ViewBag.Estado = estado;

            return View(model);
        }

        public ActionResult AgregarOrganigrama(Int32 ParentNivel, Int32 PeriodoAcademicoId, Int32? UnidadAcademicaId)
        {

            int idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            CreateOrganigramaViewModel model = new CreateOrganigramaViewModel();
            model.cargarDatos(ParentNivel, idSubModalidadPeriodoAcademico, CargarDatosContext(), UnidadAcademicaId);
            return View(model);
        }

        [HttpPost]
        public ActionResult AgregarOrganigrama(CreateOrganigramaViewModel model)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    UnidadAcademica nuevaUnidadAcademica;
                    if (model.UnidadAcademicaId.HasValue)
                    {
                        nuevaUnidadAcademica = Context.UnidadAcademica.Find(model.UnidadAcademicaId);
                    }
                    else
                    {
                        nuevaUnidadAcademica = new UnidadAcademica();
                        Context.UnidadAcademica.Add(nuevaUnidadAcademica);
                        nuevaUnidadAcademica.IdUnidadAcademicaPadre = model.ParentId;
                        nuevaUnidadAcademica.IdEscuela = EscuelaId;
                    }


                    nuevaUnidadAcademica.NombreEspanol = model.Titulo;
                    nuevaUnidadAcademica.NombreIngles = model.Titulo;
                    nuevaUnidadAcademica.IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                    nuevaUnidadAcademica.EsVisible = true;
                    if (model.Nivel == 1)
                    {
                        nuevaUnidadAcademica.Nivel = 1;
                        nuevaUnidadAcademica.Tipo = "AREA";
                        if (model.IdCarreraPeriodoAcademico != null)
                        {
                            nuevaUnidadAcademica.IdCarreraPeriodoAcademico = model.IdCarreraPeriodoAcademico;
                        }
                    }
                    else if (model.Nivel == 2)
                    {
                        nuevaUnidadAcademica.Nivel = 2;
                        nuevaUnidadAcademica.Tipo = "SUBAREA";
                    }
                    else if (model.Nivel == 3)
                    {
                        nuevaUnidadAcademica.Nivel = 3;
                        nuevaUnidadAcademica.Tipo = "CURSO";

                        nuevaUnidadAcademica.IdCursoPeriodoAcademico = model.IdCursoPeriodoAcademico;

                    }
                    Context.SaveChanges();
                    if (model.UnidadAcademicaId.HasValue)
                    {
                        List<IFC> ifc = null;
                        ifc = Context.IFC.Where(x => x.IdUnidadAcademica == model.UnidadAcademicaId).ToList();

                        foreach (IFC i in ifc)
                        {
                            i.IdDocente = model.DocenteId;
                        }

                        Context.SaveChanges();

                        //if (ifc.Count() > 0)
                        //{
                        //    //Alerta ya que existe un IFC para la Unidad Academica y no se puede Eliminar

                        //    return RedirectToAction("Organigrama"/*, new { Estado = "Error2" }*/);
                        //}
                    }

                    List<SedeUnidadAcademica> listSedeUnidadAcademica = Context.SedeUnidadAcademica.Where(x => x.IdUnidadAcademica == nuevaUnidadAcademica.IdUnidadAcademica).ToList();
                    foreach (var item in listSedeUnidadAcademica)
                    {
                        UnidadAcademicaResponsable unidadAcademicaResponsable = Context.UnidadAcademicaResponsable.Where(x => x.IdSedeUnidadAcademica == item.IdSedeUnidadAcademica).First();
                        Context.UnidadAcademicaResponsable.Remove(unidadAcademicaResponsable);
                    }
                    Context.SedeUnidadAcademica.RemoveRange(listSedeUnidadAcademica);
                    foreach (var item in model.ListaSedesElegidas)
                    {
                        SedeUnidadAcademica sedeUnidadAcademica = new SedeUnidadAcademica();
                        sedeUnidadAcademica.IdSede = Convert.ToInt32(item);
                        sedeUnidadAcademica.IdUnidadAcademica = nuevaUnidadAcademica.IdUnidadAcademica;
                        Context.SedeUnidadAcademica.Add(sedeUnidadAcademica);
                        Context.SaveChanges();

                        UnidadAcademicaResponsable unidadAcademicaResponsable = new UnidadAcademicaResponsable();
                        unidadAcademicaResponsable.IdDocente = model.DocenteId;
                        unidadAcademicaResponsable.EsActivo = true;
                        unidadAcademicaResponsable.FechaCreacion = DateTime.Now;
                        unidadAcademicaResponsable.IdSedeUnidadAcademica = sedeUnidadAcademica.IdSedeUnidadAcademica;
                        Context.UnidadAcademicaResponsable.Add(unidadAcademicaResponsable);
                        Context.SaveChanges();
                    }
                    transactionScope.Complete();
                    return RedirectToAction("Organigrama");
                }
            }
            catch (Exception)
            {
                TryUpdateModel(model);
                model.cargarDatos(model.ParentId, model.IdSubModalidadPeriodoAcademico, CargarDatosContext(), model.UnidadAcademicaId);
                return View(model);
            }

        }

        public ActionResult DeleteOrganigrama(Int32 UnidadAcademicaId)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    List<IFC> ifc = null;
                    ifc = Context.IFC.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).ToList();
                    if (ifc.Count() > 0)
                    {
                        //Alerta ya que existe un IFC para la Unidad Academica y no se puede Eliminar

                        return RedirectToAction("Organigrama", new { Estado = "No se puede eliminar, ya que cuenta con  IFC" });
                    }


                    UnidadAcademica unidadAcademicaPadre = Context.UnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).FirstOrDefault();
                    List<UnidadAcademica> unidadAcademicaHijos = Context.UnidadAcademica.Where(x => x.IdUnidadAcademicaPadre == UnidadAcademicaId).ToList();
                    List<UnidadAcademicaResponsable> responsablesHijos = new List<UnidadAcademicaResponsable>();
                    List<SedeUnidadAcademica> sedesHijos = new List<SedeUnidadAcademica>();
                    List<IFC> ifcs = new List<IFC>();

                    foreach (var itemUA in unidadAcademicaHijos)
                    {
                        foreach (var itemSede in itemUA.SedeUnidadAcademica)
                        {
                            sedesHijos.Add(itemSede);
                        }
                    }

                    foreach(var itemUA in unidadAcademicaHijos)
                    {
                        foreach (var itemIFC in itemUA.IFC)
                        {
                            ifcs.Add(itemIFC);
                        }
                    }

                    foreach(var item in ifcs)
                    {
                        Context.IFCHallazgo.RemoveRange(item.IFCHallazgo);
                    }

                    Context.IFC.RemoveRange(ifcs);

                    foreach (var itemSede in sedesHijos)
                    {
                        foreach (var itemResponsable in itemSede.UnidadAcademicaResponsable)
                        {
                            responsablesHijos.Add(itemResponsable);
                        }
                    }

                    Context.UnidadAcademicaResponsable.RemoveRange(responsablesHijos);
                    Context.SedeUnidadAcademica.RemoveRange(sedesHijos);
                    Context.UnidadAcademica.RemoveRange(unidadAcademicaHijos);

                    List<SedeUnidadAcademica> sedesPadre = Context.SedeUnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).ToList();
                    foreach (var item in sedesPadre)
                    {
                        Context.UnidadAcademicaResponsable.RemoveRange(item.UnidadAcademicaResponsable);
                    }
                    Context.SedeUnidadAcademica.RemoveRange(sedesPadre);
                    Context.UnidadAcademica.Remove(unidadAcademicaPadre);

                /*    foreach (var unidadItem in unidadAcademicaHijos)
                    {
                        var listSedesAcademica = unidadItem.SedeUnidadAcademica;
                        foreach(var sedeItem in listSedesAcademica)
                        {
                            var listResponsableAcademica = sedeItem.UnidadAcademicaResponsable;
                            foreach(var responsableItem in listResponsableAcademica)
                            {
                                Context.UnidadAcademicaResponsable.Remove(responsableItem);
                            }
                            Context.SedeUnidadAcademica.Remove(sedeItem);
                        }
                        Context.UnidadAcademica.Remove(unidadItem);
                    }*/

                    //UnidadAcademica unidadAcademicaPadre = Context.UnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).FirstOrDefault();



                    //  List<SedeUnidadAcademica> listSedeUnidadAcademica = Context.SedeUnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).ToList();


                    //List<SedeUnidadAcademica> listSedeUnidadAcademica = Context.SedeUnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).ToList();
                    //foreach (var item in listSedeUnidadAcademica)
                    //{
                    //    List<UnidadAcademica> unidadAcademicaHijos = Context.UnidadAcademica.Where(x => x.IdUnidadAcademicaPadre == UnidadAcademicaId).ToList();
                    //    Context.UnidadAcademica.RemoveRange(unidadAcademicaHijos);

                    //    //UnidadAcademica unidadAcademicaPadre = Context.UnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).FirstOrDefault();
                    //    //Context.UnidadAcademica.Remove(unidadAcademicaPadre);
                    //    UnidadAcademicaResponsable unidadAcademicaResponsable = Context.UnidadAcademicaResponsable.Where(x => x.IdSedeUnidadAcademica == item.IdSedeUnidadAcademica).First();
                    //    Context.UnidadAcademicaResponsable.Remove(unidadAcademicaResponsable);
                    //}
                    //Context.SedeUnidadAcademica.RemoveRange(listSedeUnidadAcademica);



                    //UnidadAcademica unidadAcademica = Context.UnidadAcademica.Find(UnidadAcademicaId);
                    //Context.UnidadAcademica.Remove(unidadAcademica);

                    //UnidadAcademica unidadAcademicaPadre = Context.UnidadAcademica.Where(x => x.IdUnidadAcademicaPadre == UnidadAcademicaId).First(); ;
                    //Context.UnidadAcademica.Remove(unidadAcademicaPadre);


                    Context.SaveChanges();
                    transactionScope.Complete();
                    //Alerta de eliminacion Exitosa

                    return RedirectToAction("Organigrama");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Organigrama");
            }
        }
        public JsonResult GetCiclosxModalidad(Int32 ModalidadId)
        {
            var ciclos = (from pa in Context.PeriodoAcademico
                          join smpa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals smpa.IdPeriodoAcademico
                          join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                          join mo in Context.Modalidad on sm.IdModalidad equals mo.IdModalidad
                          where mo.IdModalidad == ModalidadId
                          select new
                          {
                              Value = pa.IdPeriodoAcademico,
                              Text = pa.CicloAcademico
                          }).ToList();

            var LstSelect = new SelectList(ciclos, "Value", "Text");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }

    }
}