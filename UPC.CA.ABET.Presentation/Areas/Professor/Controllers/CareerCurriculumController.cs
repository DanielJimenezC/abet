﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.CareerCurriculum;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using UPC.CA.ABET.Logic.Areas.Upload;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    

    [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
    public class CareerCurriculumController : BaseController
    {
        public ProfessorBaseModel professorBaseModel;
        protected UploadLogic uploadLogic;

        public CareerCurriculumController()
        {
            this.professorBaseModel = new ProfessorBaseModel(Context);
            uploadLogic = new UploadLogic(CargarDatosContext().context);
        }
        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }
        
        public ActionResult Management()
        {
            return View();
        }

        /// <summary>
        /// Método para realizar una búsqueda o consulta de mallas de cocos
        /// </summary>
        /// <param name="isSearchRequest">Especifica si la petición es para una búsqueda</param>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <param name="IdCarrera">Código identificador de la carrera</param>
        /// <param name="IdSubModalidadPeriodoAcademico"></param>
        /// <returns></returns>
        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult Consult(bool? isSearchRequest, string codigoComision, Int32? IdCarrera, Int32? IdSubModalidadPeriodoAcademico, Int32? IdAcreditadora)
        {
            //int? IdSubModa = null;

            //if (IdPeriodoAcademico != null)
            //{              
            //   IdSubModa = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            //}
            int ModalidadId = Session.GetModalidadId();

            var viewModel = new ConsultCareerCurriculumViewModel();

            //var culture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();

            // Cargar Comisiones
            viewModel.AvailableComisiones = GedServices.GetAllCodigosFromComisionesServices(Context, EscuelaId, IdCarrera, IdSubModalidadPeriodoAcademico, IdAcreditadora).Select(c => new SelectListItem { Value = c.Key, Text = c.Key + " - " + c.Value }).Distinct();
            //.Select(c => new SelectListItem { Value = c, Text = currentCulture == Culture.ESPANOL ? c == "EAC" ? "INGENIERÍA" : c == "CAC" || c=="CAC-CC" ? "COMPUTACIÓN" : "WASC" : c == "EAC" ? "Engineering" : c == "CAC" ? "Computing" : "Other Comission" }).Distinct();

            if (viewModel.AvailableComisiones == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudieronCargarComisiones);
            }

            // Cargar Carreras

            //viewModel.AvailableCarreras = GedServices.GetAllCarrerasServices(context, escuelaId).Select(c => new SelectListItem { Value = c.IdCarrera.ToString(), Text = currentCulture == Culture.ESPANOL ? c.NombreEspanol : c.NombreIngles });

            viewModel.AvailableCarreras = (from a in Context.Carrera
                                           join b in Context.SubModalidad on a.IdSubmodalidad equals b.IdSubModalidad
                                           where b.IdModalidad == ModalidadId
                                           select new
                                           {
                                               Value = a.IdCarrera.ToString(),
                                               Text = a.NombreEspanol.ToString()
                                           }).Select(o => new SelectListItem { Value = o.Value.ToString(), Text = o.Text });


            if (viewModel.AvailableCarreras == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudieronCargarCarreras);
            }

            // Cargar Periodos Academicos (Ciclos)

            viewModel.AvailablePeriodoAcademicos = (from a in Context.SubModalidadPeriodoAcademico
                                                    join b in Context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                                    join c in Context.SubModalidad on a.IdSubModalidad equals c.IdSubModalidad
                                                    where c.IdModalidad== ModalidadId
                                                    orderby a.IdPeriodoAcademico descending
                                                    select new
                                                    {
                                                        Value = a.IdPeriodoAcademico.ToString(),
                                                        Text = b.CicloAcademico.ToString()
                                                    }).Select(o => new SelectListItem { Value = o.Value.ToString(), Text = o.Text });


              //viewModel.AvailablePeriodoAcademicos = GedServices.GetAllPeriodoAcademicosServices(context)
              //     .Select(o => new SelectListItem { Value = o.IdPeriodoAcademico.ToString(), Text = o.CicloAcademico });


            viewModel.AvailableModulo = (from a in Context.Modalidad
                                          select new
                                         {
                                             Value = a.IdModalidad.ToString(),
                                             Text = a.NombreEspanol.ToString()
                                         }).Select(o => new SelectListItem { Value = o.Value.ToString(), Text = o.Text });

            if (viewModel.AvailablePeriodoAcademicos == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudieronCargarCiclos);
            }

            viewModel.ListAcreditadoras = Context.Acreditadora.Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdAcreditadora.ToString()
            }).ToList();

            // Si es request de búsqueda
            //int escuelaId = Session.GetEscuelaId();
            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                // Realizar búsqueda y guardar resultados
                viewModel.Resultados = GenerarListaMallaCocosViewModel(EscuelaId, codigoComision, IdCarrera, IdSubModalidadPeriodoAcademico, IdAcreditadora);
                if (viewModel.Resultados == null || !viewModel.Resultados.Any())
                {
                    viewModel.Resultados = new List<MallaCocosViewModel>();
                    PostMessage(MessageType.Info, MessagesCareerCurriculumResource.NoSeEncontraronResultados);
                }
            }
            SelectListItem inicio = new SelectListItem() { Text = "--"+LayoutResource.Seleccione+"--", Value = "0" };

            List<SelectListItem> Ciclos = uploadLogic.ListCyclesToForModality(ModalityId, 0, 0).Select(i => new SelectListItem() { Text = i.Value, Value = i.Key.ToString() }).ToList();
            Ciclos.Insert(0, inicio);
            viewModel.Modal = Ciclos;
            ViewBag.DropDownListModalities = professorBaseModel.DropDownListModalities();
           
            return View(viewModel);
        }

        /// <summary>
        /// Método para visualizar una malla de cocos
        /// </summary>
        /// <param name="id">Código identificador de la malla de cocos</param>
        /// <returns>Vista para la operación</returns>
        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult View(int id)
        {
            var mallaCocos = GedServices.GetMallaCocosByIdServices(Context, id);

            if (mallaCocos == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSeEncontroMallaCocosVisualizar);
                return RedirectToAction("Consult");
            }

            var carreraComision = GedServices.GetCarreraComisionServices(Context, mallaCocos.IdCarreraComision);
            var mallaCurricular = GedServices.GetMallaCurricularServices(Context, carreraComision.IdCarrera,
                mallaCocos.IdSubModalidadPeriodoAcademico.Value);

            ViewCareerCurriculumViewModel viewModel = null;
            if (CargarDatosContext().currentCulture == Culture.ESPANOL)
            {
                viewModel = new ViewCareerCurriculumViewModel
                {
                    IdMallaCocos = id,
                    Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value),

                    TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                        .Select(
                            o => new TipoOutcomeCursoViewModel
                            {
                                IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                                Icono = o.Icono,
                                Nombre = o.NombreEspanol
                            }).ToList(),

                    Titulo = carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreEspanol + " - " +
                             GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico,
                    Descripcion = mallaCocos.Detalle
                };
            }
            else
            {
                viewModel = new ViewCareerCurriculumViewModel
                {
                    IdMallaCocos = id,
                    Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value),

                    TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                        .Select(
                            o => new TipoOutcomeCursoViewModel
                            {
                                IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                                Icono = o.Icono,
                                Nombre = o.NombreIngles
                            }).ToList(),

                    Titulo = carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreIngles + " - " +
                             GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico,
                    Descripcion = mallaCocos.Detalle
                };
            }

            var mallaCocosDetalles = GedServices.GetMallaCocosDetallesInMallaCocosServices(Context, mallaCocos.IdMallaCocos);
            var dicMallaCocosDetalles = new Dictionary<Tuple<int, int>, MallaCocosDetalle>();
            mallaCocosDetalles.ForEach(x => dicMallaCocosDetalles.Add(new Tuple<int, int>((int)x.IdCursoMallaCurricular, x.IdOutcomeComision), x));
            var outcomeComisiones = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(Context, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico);
            var dicOutcomeComisiones = new Dictionary<Tuple<int, int>, OutcomeComision>();
            outcomeComisiones.ForEach(x => dicOutcomeComisiones.Add(new Tuple<int, int>(x.IdOutcome, x.IdComision), x));

            foreach (var nivel in viewModel.Niveles)
            {
                foreach (var cursoMallaCurricularViewModel in nivel.CursoMallaCurriculares)
                {
                    viewModel.CantidadOutcomes = cursoMallaCurricularViewModel.Outcomes.Count();
                    int IdCursoMallaCurricular = cursoMallaCurricularViewModel.IdCursoMallaCurricular;

                    foreach (var outcomeViewModel in cursoMallaCurricularViewModel.Outcomes)
                    {
                        int IdOutcomeComision = dicOutcomeComisiones[new Tuple<int, int>(outcomeViewModel.IdOutcome, carreraComision.IdComision)].IdOutcomeComision;

                        if (!dicMallaCocosDetalles.ContainsKey(new Tuple<int, int>(IdCursoMallaCurricular, IdOutcomeComision)))
                        {
                            continue;
                        }

                        var mallaCocosDetalle = dicMallaCocosDetalles[new Tuple<int, int>(IdCursoMallaCurricular, IdOutcomeComision)];

                        outcomeViewModel.IdTipoOutcomeCurso = (int)mallaCocosDetalle.IdTipoOutcomeCurso;
                       //uber
                        outcomeViewModel.Icono = mallaCocosDetalle.TipoOutcomeCurso.Icono;

                    }
                }
            }

            viewModel.IconoBlank = IconoBlank;
            viewModel.IconoFormacion = IconoFormacion;
            viewModel.IconoBorrar = IconoBorrar;

            return View(viewModel);
        }

       // [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult EditCalificado(int id)
        {
            var mallaCocos = GedServices.GetMallaCocosByIdServices(Context, id);

            if (mallaCocos == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSeEncontroMallaCocosVisualizar);
                return RedirectToAction("Consult");
            }

            var carreraComision = GedServices.GetCarreraComisionServices(Context, mallaCocos.IdCarreraComision);
            var mallaCurricular = GedServices.GetMallaCurricularServices(Context, carreraComision.IdCarrera,
                mallaCocos.IdSubModalidadPeriodoAcademico.Value);

            ViewCareerCurriculumViewModel viewModel = null;
            if (CargarDatosContext().currentCulture == Culture.ESPANOL)
            {
                viewModel = new ViewCareerCurriculumViewModel
                {
                    IdMallaCocos = id,
                    Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value),

                    TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                        .Select(
                            o => new TipoOutcomeCursoViewModel
                            {
                                IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                                Icono = o.Icono,
                                Nombre = o.NombreEspanol
                            }).ToList(),

                    Titulo = carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreEspanol + " - " +
                             GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico,
                    Descripcion = mallaCocos.Detalle
                };
            }
            else
            {
                viewModel = new ViewCareerCurriculumViewModel
                {
                    IdMallaCocos = id,
                    Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value),

                    TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                        .Select(
                            o => new TipoOutcomeCursoViewModel
                            {
                                IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                                Icono = o.Icono,
                                Nombre = o.NombreIngles
                            }).ToList(),

                    Titulo = carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreIngles + " - " +
                             GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico,
                    Descripcion = mallaCocos.Detalle
                };
            }


            var mallaCocosDetalles = GedServices.GetMallaCocosDetallesInMallaCocosServices(Context, mallaCocos.IdMallaCocos);
            var dicMallaCocosDetalles = new Dictionary<Tuple<int, int>, MallaCocosDetalle>();
            mallaCocosDetalles.ForEach(x => dicMallaCocosDetalles.Add(new Tuple<int, int>((int)x.IdCursoMallaCurricular, x.IdOutcomeComision), x));
            var outcomeComisiones = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(Context, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value);
            var dicOutcomeComisiones = new Dictionary<Tuple<int, int>, OutcomeComision>();
            outcomeComisiones.ForEach(x => dicOutcomeComisiones.Add(new Tuple<int, int>(x.IdOutcome, x.IdComision), x));

            foreach (var nivel in viewModel.Niveles)
            {
                foreach (var cursoMallaCurricularViewModel in nivel.CursoMallaCurriculares)
                {
                    viewModel.CantidadOutcomes = cursoMallaCurricularViewModel.Outcomes.Count();
                    int IdCursoMallaCurricular = cursoMallaCurricularViewModel.IdCursoMallaCurricular;

                    foreach (var outcomeViewModel in cursoMallaCurricularViewModel.Outcomes)
                    {
                        int IdOutcomeComision = dicOutcomeComisiones[new Tuple<int, int>(outcomeViewModel.IdOutcome, carreraComision.IdComision)].IdOutcomeComision;

                        if (!dicMallaCocosDetalles.ContainsKey(new Tuple<int, int>(IdCursoMallaCurricular, IdOutcomeComision)))
                        {
                            continue;
                        }

                        var mallaCocosDetalle = dicMallaCocosDetalles[new Tuple<int, int>(IdCursoMallaCurricular, IdOutcomeComision)];

                        outcomeViewModel.IdTipoOutcomeCurso = (int)mallaCocosDetalle.IdTipoOutcomeCurso;
                        outcomeViewModel.Icono = mallaCocosDetalle.TipoOutcomeCurso.Icono;
                        outcomeViewModel.mallaCocosDetalleId = mallaCocosDetalle.IdMallaCocosDetalle;
                        outcomeViewModel.esCalificado = mallaCocosDetalle.EsCalificado.HasValue ? mallaCocosDetalle.EsCalificado.Value : false;
                    }
                }
            }

            viewModel.IconoBlank = IconoBlank;
            viewModel.IconoFormacion = IconoFormacion;
            viewModel.IconoBorrar = IconoBorrar;

            return View(viewModel);
        }

        // [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        [HttpPost]
        public ActionResult  EditCalificado(FormCollection formCollection,Int32 id)
        {
            List<String> esCalificadoOutcomeEditados = formCollection.AllKeys.Where(X => X.Contains("outcome")).ToList();
            foreach (var outcomeEsCalificado in esCalificadoOutcomeEditados)
            {
                Boolean valor = (formCollection[outcomeEsCalificado]).ToString().Contains("true") || (formCollection[outcomeEsCalificado]).ToString().Contains("on");
                Int32 mallaCocosDetalleId = (outcomeEsCalificado.Remove(0,7)).ToInteger();
                var mallaCocosDetalle = Context.MallaCocosDetalle.FirstOrDefault(X => X.IdMallaCocosDetalle == mallaCocosDetalleId);
                if (mallaCocosDetalle == null)
                    continue;
                mallaCocosDetalle.EsCalificado = valor;
                Context.Entry(mallaCocosDetalle).State = System.Data.Entity.EntityState.Modified;

            }
            Context.SaveChanges();
            var a=Context.MallaCocosDetalle.FirstOrDefault(X => X.IdMallaCocosDetalle == 2);
            PostMessage(MessageType.Success, MessagesCareerCurriculumResource.SeActualizoMalla);
            return RedirectToAction("Consult");
            //return RedirectToAction("View", new { id=id });
        }

        /// <summary>
        /// Método para exportar una malla de cocos en formato pdf
        /// </summary>
        /// <param name="id">Código identificador de la malla de cocos</param>
        /// <returns>Archivo pdf de la malla de cocos</returns>
      //  [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult GetMallaCocosPdf(int id)
        {
            
            try
            {
                ExportResourcesMallaCocos exportResources = new ExportResourcesMallaCocos();

                foreach (var tipoOutcomeCurso in GedServices.GetAllTipoOutcomeCursosServices(Context))
                {
                    exportResources.PathsIconosTipoOutcome.Add
                        (
                            tipoOutcomeCurso.IdTipoOutcomeCurso,
                            Server.MapPath("~/Areas/Professor/Content/assets/images/") + tipoOutcomeCurso.Icono
                        );
                }

                exportResources.PathLogoUpc = Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc;
                exportResources.PathIconoFormacion = Server.MapPath("~/Areas/Professor/Content/assets/images/") + IconoFormacion;

                ExportWorkerMallaCocos pdfExport = new ExportWorkerMallaCocos(Context, id, exportResources, currentCulture);

        
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.MALLA_COCOS_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory, currentCulture);
                string filename = Path.GetFileName(path);
                //PostMessage(MessageType.Success, path + filename);
                return File(path, "application/pdf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.ErrorExportarPDF +"  " +ex.ToString());
                return RedirectToAction("Consult");
            }
        }

        /// <summary>
        /// Método para exportar una malla de cocos en formato rtf
        /// </summary>
        /// <param name="id">Código identificador de la malla de cocos</param>
        /// <returns>Archivo rtf de la malla de cocos</returns>
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult GetMallaCocosRtf(int id)
        {



            try
            {
                ExportResourcesMallaCocos exportResources = new ExportResourcesMallaCocos();

                foreach (var tipoOutcomeCurso in GedServices.GetAllTipoOutcomeCursosServices(Context))
                {
                    exportResources.PathsIconosTipoOutcome.Add
                        (
                            tipoOutcomeCurso.IdTipoOutcomeCurso,
                            Server.MapPath("~/Areas/Professor/Content/assets/images/") + tipoOutcomeCurso.Icono
                        );
                }

                exportResources.PathLogoUpc = Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc;
                exportResources.PathIconoFormacion = Server.MapPath("~/Areas/Professor/Content/assets/images/") + IconoFormacion;

                ExportWorkerMallaCocos pdfExport = new ExportWorkerMallaCocos(Context, id, exportResources, currentCulture);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.MALLA_COCOS_DIRECTORY, "RTF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Rtf, directory, currentCulture);
                string filename = Path.GetFileName(path);
                //PostMessage(MessageType.Success, path + filename);
                return File(path, "application/rtf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.ErrorExportarRTF + ex);
                return RedirectToAction("Consult");
            }
        }

        /// <summary>
        /// Método para la creación de una malla de cocos de acuerdo a los criterios ingresados
        /// </summary>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <param name="IdCarrera">Código identificador de la carrera</param>
        /// <param name="IdSubModalidadPeriodoAcademico"></param>
        /// <returns>Vista para la operación</returns>
       // [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Create(string codigoComision, int? IdCarrera, int? IdPeriodoAcademico, int? IdAcreditadora)
        {
            //Nueva Submoda 
            //int? IdSubModa=null;

            int parModalidadId = Session.GetModalidadId();
            var IdSubModalidadPeriodoAcademico = SubModalidadPeriodoAcademicoId;

            if(IdPeriodoAcademico!=null)
            { 
                IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }

            var viewModel = new CreateCareerCurriculumViewModel
            {
                AvailableComisiones = GedServices.GetAllCodigosFromComisionesServices(Context, EscuelaId, IdCarrera, IdSubModalidadPeriodoAcademico, IdAcreditadora)
                    .Select(c => new SelectListItem { Value = c.Key, Text = c.Key + " - " + c.Value }),

                ListPeriodoAcadmico = Context.SubModalidadPeriodoAcademico.Where(x=>x.SubModalidad.IdModalidad==parModalidadId).Select(x=> new SelectListItem() { Value=x.IdPeriodoAcademico.ToString(), Text=x.PeriodoAcademico.CicloAcademico }).ToList(),

                ListModalidad = Context.Modalidad.Select(x => new SelectListItem() { Value = x.IdModalidad.ToString(), Text = x.NombreEspanol }).ToList(),

            };

            if (!string.IsNullOrEmpty(codigoComision) && IdCarrera != null && IdSubModalidadPeriodoAcademico != null)
            {
                var buscarConCriterios = GedServices.GetMallaCocosFromCarreraComisionPeriodoAcademicoServices(Context, codigoComision, IdCarrera.Value,
                    IdSubModalidadPeriodoAcademico.Value);
                if (buscarConCriterios.Any())
                {
                    PostMessage(MessageType.Warning, @"" + MessagesCareerCurriculumResource.YaExisteMalla + " <a href='" + Url.Action("View", new { id = buscarConCriterios.First().IdMallaCocos }) + @"'><b>" + MessagesCareerCurriculumResource.aqui + "</b></a>.");
                    return RedirectToAction("Consult");
                }
                int idComision = GedServices.GetComisionFromCodigoAndPeriodoAcademicoServices(Context, codigoComision, (int)IdSubModalidadPeriodoAcademico).IdComision;
                viewModel.IdComision = idComision;
                viewModel.IdCarrera = IdCarrera.Value;
                viewModel.IdPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico.Value).FirstOrDefault().IdPeriodoAcademico;
                viewModel.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico.Value;
                viewModel.AvailableOutcomes = GedServices.GetOutcomesInComisionPeriodoAcademicoServices(Context, idComision, IdSubModalidadPeriodoAcademico.Value)
                    .Select(o => new SelectListItem { Value = o.IdOutcome.ToString(), Text = o.Nombre });
                if (viewModel.AvailableOutcomes == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudieronCargarOutcomes);
                    return RedirectToAction("Consult");
                }

                if (CargarDatosContext().currentCulture == Culture.ESPANOL)
                {
                    viewModel.TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                        .Select(o => new TipoOutcomeCursoViewModel
                        {
                            IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                            Icono = o.Icono,
                            Nombre = o.NombreEspanol 
                        }).ToList();
                }
                else
                {
                    viewModel.TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                       .Select(o => new TipoOutcomeCursoViewModel
                       {
                           IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                           Icono = o.Icono,
                           Nombre = o.NombreIngles
                       }).ToList();
                }
                if (viewModel.TipoOutcomeCursos == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudieronCargarTipoOutcomes);
                    return RedirectToAction("Consult");
                }

                var mallaCurricular = GedServices.GetMallaCurricularServices(Context, (int)IdCarrera, (int)IdSubModalidadPeriodoAcademico);
                if (mallaCurricular == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSePudoCargarMalla);
                    return RedirectToAction("Consult");
                }
                viewModel.IdMallaCurricular = mallaCurricular.IdMallaCurricular;
                viewModel.Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, (int)idComision, (int)IdSubModalidadPeriodoAcademico);

                var carrera = mallaCurricular.Carrera;
                var periodoAcademico = GedServices.GetPeriodoAcademicoServices(Context, (int)IdSubModalidadPeriodoAcademico);
                if (carrera == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoExisteRegistroCarrera);
                    return RedirectToAction("Consult");
                }
                if (periodoAcademico == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoExisteRegistroPeriodoAcademico);
                    return RedirectToAction("Consult");
                }
                var comision = GedServices.GetComisionServices(Context, idComision);
                viewModel.Titulo = CargarDatosContext().currentCulture == Culture.ESPANOL ?
                    (comision.Codigo + " - " + carrera.NombreEspanol + " - " + periodoAcademico.CicloAcademico) :
                    (comision.Codigo + " - " + carrera.NombreIngles + " - " + periodoAcademico.CicloAcademico);
            }

            viewModel.IconoBlank = IconoBlank;
            viewModel.IconoFormacion = IconoFormacion;
            viewModel.IconoBorrar = IconoBorrar;

            return View(viewModel);
        }
        public JsonResult GetCiclosxModalidad(Int32 ModalidadId)
        {
            var ciclos = (from pa in Context.PeriodoAcademico
                          join smpa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals smpa.IdPeriodoAcademico
                          join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                          join mo in Context.Modalidad on sm.IdModalidad equals mo.IdModalidad
                          where mo.IdModalidad == ModalidadId
                          select new
                          {
                              Value = pa.IdPeriodoAcademico,
                              Text = pa.CicloAcademico
                          }).ToList();

            var LstSelect = new SelectList(ciclos, "Value", "Text");
            return Json(LstSelect, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Método para la creación de una malla de cocos
        /// </summary>
        /// <param name="model">View model para la creación de la malla de cocos</param>
        /// <returns>Vista de redirección</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Create(CreateCareerCurriculumViewModel model)
        {
            try
            {
                int IdSubModa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var mallaCurricular = CrearMallaCurricular(model, model.IdComision, IdSubModa);

                int success = GedServices.CrearMallaCocosServices(Context, mallaCurricular, model.Descripcion,
                    model.IdPeriodoAcademico, model.IdComision, model.IdCarrera);

                if (success >= 0)
                {
                    PostMessage(MessageType.Success, MessagesCareerCurriculumResource.SeCreoMalla + model.Titulo + MessagesCareerCurriculumResource.PuedeVerla + "<a href='" + Url.Action("View", new { id = success }) + @"'><b>" + MessagesCareerCurriculumResource.aqui + "</b></a>.");
                }
                else
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSeCreoMalla + model.Titulo + MessagesCareerCurriculumResource.VuelvaIntentarlo);
                }

                return RedirectToAction("Consult");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Método para editar una malla de cocos
        /// </summary>
        /// <param name="id">Código identificador de la malla de cocos</param>
        /// <returns>Vista para la operación</returns>
        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Edit(int id)
        {
            
            var mallaCocos = GedServices.GetMallaCocosByIdServices(Context, id);

            if (mallaCocos == null)
            {
                PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoExisteRegistroMalla);
                return RedirectToAction("Consult");
            }

            var carreraComision = GedServices.GetCarreraComisionServices(Context, mallaCocos.IdCarreraComision);
            var mallaCurricular = GedServices.GetMallaCurricularServices(Context, carreraComision.IdCarrera,
                mallaCocos.IdSubModalidadPeriodoAcademico.Value);
            int idpa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == mallaCocos.IdSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;
            var viewModel = new CreateCareerCurriculumViewModel
            {
                //Obtener IdPeriodoAcademico

               

                IdMallaCocos = id,
                IdCarrera = carreraComision.IdCarrera,
                IdPeriodoAcademico = idpa,
                IdSubModalidadPeriodoAcademico = mallaCocos.IdSubModalidadPeriodoAcademico.Value,
                IdComision = carreraComision.IdComision,
                AvailableOutcomes = GedServices.GetOutcomesInComisionPeriodoAcademicoServices(Context, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value)
                    .Select(o => new SelectListItem { Value = o.IdOutcome.ToString(), Text = o.Nombre }),
                TipoOutcomeCursos = GedServices.GetAllTipoOutcomeCursosServices(Context)
                    .Select(
                        o =>
                            new TipoOutcomeCursoViewModel
                            {
                                IdTipoOutcomeCurso = o.IdTipoOutcomeCurso,
                                Icono = o.Icono,
                                Nombre = CargarDatosContext().currentCulture == Culture.ESPANOL ? o.NombreEspanol : o.NombreIngles
                            }).ToList(),
                IdMallaCurricular = GedServices
                    .GetMallaCurricularServices(Context, carreraComision.IdCarrera, mallaCocos.IdSubModalidadPeriodoAcademico.Value)
                    .IdMallaCurricular,
                Niveles = GenerarNivelesParaMallaCurricularViewModel(mallaCurricular, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico.Value),
                Titulo = CargarDatosContext().currentCulture == Culture.ESPANOL ?
                         (carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreEspanol + " - " +
                         GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico) :
                         (carreraComision.Comision.Codigo + " - " + carreraComision.Carrera.NombreIngles + " - " +
                         GedServices.GetPeriodoAcademicoServices(Context, mallaCocos.IdSubModalidadPeriodoAcademico.Value).CicloAcademico),
                Descripcion = mallaCocos.Detalle
            };


            var mallaCocosDetalles = GedServices.GetMallaCocosDetallesInMallaCocosServices(Context, mallaCocos.IdMallaCocos);
            var dicMallaCocosDetalle = new Dictionary<Tuple<int, int>, MallaCocosDetalle>();
            mallaCocosDetalles.ForEach(x => dicMallaCocosDetalle.Add(new Tuple<int, int>((int)x.IdCursoMallaCurricular, x.IdOutcomeComision), x));

            var outcomeComisiones = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(Context, carreraComision.IdComision, mallaCocos.IdSubModalidadPeriodoAcademico);
            var dicOutcomeComisiones = new Dictionary<Tuple<int, int>, OutcomeComision>();
            outcomeComisiones.ForEach(x => dicOutcomeComisiones.Add(new Tuple<int, int>(x.IdOutcome, x.IdComision), x));

            foreach (var nivel in viewModel.Niveles)
            {
                foreach (var cursoMallaCurricularViewModel in nivel.CursoMallaCurriculares)
                {
                    int idCursoMallaCurricular = cursoMallaCurricularViewModel.IdCursoMallaCurricular;

                    foreach (var outcomeViewModel in cursoMallaCurricularViewModel.Outcomes)
                    {
                        int idOutcomeComision = dicOutcomeComisiones[new Tuple<int, int>(outcomeViewModel.IdOutcome, carreraComision.IdComision)].IdOutcomeComision;

                        if (!dicMallaCocosDetalle.ContainsKey(new Tuple<int, int>(idCursoMallaCurricular, idOutcomeComision)))
                        {
                            outcomeViewModel.Icono = IconoBlank;
                            continue;
                        }
                        var mallaCocosDetalle = dicMallaCocosDetalle[new Tuple<int, int>(idCursoMallaCurricular, idOutcomeComision)];
                        outcomeViewModel.IdTipoOutcomeCurso = (int)mallaCocosDetalle.IdTipoOutcomeCurso;
                        outcomeViewModel.OriginalValue = (int)mallaCocosDetalle.IdTipoOutcomeCurso;
                        outcomeViewModel.Icono = mallaCocosDetalle.TipoOutcomeCurso.Icono;
                    }
                }
            }

            viewModel.IconoBlank = IconoBlank;
            viewModel.IconoFormacion = IconoFormacion;
            viewModel.IconoBorrar = IconoBorrar;

            return View(viewModel);
        }

        /// <summary>
        /// Método para editar una malla de cocos
        /// </summary>
        /// <param name="model">View model para la edición de la malla de cocos</param>
        /// <returns>Vista de redirección</returns>
        [HttpPost]
        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Edit(CreateCareerCurriculumViewModel model)
        {
            try
            {
                var mallaCocos = GedServices.GetMallaCocosByIdServices(Context, model.IdMallaCocos);

                if (mallaCocos == null)
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoExisteRegistroMalla);
                    return RedirectToAction("Consult");
                }
                int IdSubModa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var mallaCurricularCarrera = CrearMallaCurricular(model, model.IdComision, IdSubModa);

                bool success = GedServices.ActualizarMallaCocosServices(Context, mallaCurricularCarrera, model.Descripcion, model.IdMallaCocos);

                if (success)
                {
                    PostMessage(MessageType.Success, MessagesCareerCurriculumResource.SeActualizoMalla + model.Titulo + MessagesCareerCurriculumResource.PuedeVerla + " <a href='" + Url.Action("View", new { id = mallaCocos.IdMallaCocos }) + @"'><b>" + MessagesCareerCurriculumResource.aqui + "</b></a>.");
                    return RedirectToAction("Consult");
                }
                else
                {
                    PostMessage(MessageType.Error, MessagesCareerCurriculumResource.NoSeActualizoMalla +
                        model.Titulo + MessagesCareerCurriculumResource.VuelvaIntentarlo);
                    return RedirectToAction("Consult");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        /// <summary>
        /// Método para eliminar una malla de cocos
        /// </summary>
        /// <param name="id">Código identificador de la malla de cocos</param>
        /// <param name="collection"></param>
        /// <returns>Resultado de la operación</returns>
        [HttpPost]
        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite)]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var hasRubricaCalificada = false;
            var mensaje = String.Empty;

            var mallaCocos = Context.MallaCocos.FirstOrDefault(x => x.IdMallaCocos == id);
            var mallaCocosDetalle = mallaCocos.MallaCocosDetalle;
            var outcomeComision = mallaCocosDetalle.Select(x => x.IdOutcomeComision);

            var lstRubricas = Context.OutcomeRubrica.Where(x => outcomeComision
                .Contains(x.OutcomeComision.IdOutcomeComision)).Select(x => x.Rubrica).Distinct().ToList();

            if (lstRubricas.Count > 0)
            {
                foreach (var rubrica in lstRubricas)
                {
                    if (rubrica.RubricaCalificada.Count > 0)
                    {
                        hasRubricaCalificada = true;
                        break;
                    }
                }
            }

            var success = false;
            if (!hasRubricaCalificada)
            {
                foreach (var rubrica in lstRubricas)
                {
                    RubricLogic.DeleteRubric(CargarDatosContext(), rubrica.IdRubrica);
                }
                mensaje = MessageResource.SeEliminaronLasRubricasAsociadasaLaMalla;
                success = GedServices.DeleteMallaCocosServices(Context, id);
            }
            else
            {
                mensaje = MessageResource.ExistenRubricasCalificadasDependientesDeLaMallaSeleccionada;
            }

            if (success)
            {
                PostMessage(MessageType.Success, MessageResource.Success + "  " + mensaje);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                PostMessage(MessageType.Warning, MessageResource.Error + "  " + mensaje);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
        }

        /// <summary>
        /// Método para generar los niveles correspondientes a una malla curricular
        /// </summary>
        /// <param name="mallaCurricular">Malla curricular a partir de la cual se generarán los niveles para la malla de cocos</param>
        /// <param name="idComision">Código identificador de la malla de cocos</param>
        /// <param name="IdSubModalidadPeriodoAcademico"></param>
        /// <returns>Lista con los view models de nivel para la malla de cocos</returns>
        private List<NivelViewModel> GenerarNivelesParaMallaCurricularViewModel(MallaCurricular mallaCurricular, int idComision, int IdSubModalidadPeriodoAcademico)
        {
            
            var result = new List<NivelViewModel>();

            var aux = new Dictionary<int, NivelViewModel>();

            var outcomes = GedServices.GetOutcomesInComisionPeriodoAcademicoServices(Context, idComision, IdSubModalidadPeriodoAcademico);

            var tipoOutcomes = GedServices.GetAllTipoOutcomeCursosServices(Context);

            var nivelElectivos = new NivelViewModel
            {
                Outcomes = outcomes,
                CursoMallaCurriculares = new List<CursoMallaCurricularViewModel>(),
                Numero = currentCulture == Culture.ESPANOL ? "Cursos Electivos" : "Electives Courses",
                EsNivelDeElectivos = true
            };

            foreach (var cursoMallaCurricular in mallaCurricular.CursoMallaCurricular)
            {
                int nivelCmc = cursoMallaCurricular.NivelAcademico.Numero;
                bool esElectivo = cursoMallaCurricular.EsElectivo ?? false;

                if (!esElectivo && !aux.ContainsKey(nivelCmc))
                {
                    aux[nivelCmc] = new NivelViewModel
                    {
                        Numero = nivelCmc.ToString(),
                        Outcomes = outcomes,
                        CursoMallaCurriculares = new List<CursoMallaCurricularViewModel>()
                    };
                }

                var cmcViewModel = new CursoMallaCurricularViewModel
                {
                    IdCursoMallaCurricular = cursoMallaCurricular.IdCursoMallaCurricular,
                    CursoMallaCurricular = cursoMallaCurricular,
                    EsFormacion =
                        cursoMallaCurricular.EsFormacion == null ? false : (bool)cursoMallaCurricular.EsFormacion,
                    Outcomes = new List<OutcomeViewModel>()
                };

                if (!esElectivo)
                {
                    foreach (Outcome o in aux[nivelCmc].Outcomes)
                    {
                        var outcomeViewModel = new OutcomeViewModel { IdOutcome = o.IdOutcome };
                        cmcViewModel.Outcomes.Add(outcomeViewModel);
                    }
                }
                else
                {
                    foreach (Outcome o in nivelElectivos.Outcomes)
                    {
                        var outcomeViewModel = new OutcomeViewModel { IdOutcome = o.IdOutcome };
                        cmcViewModel.Outcomes.Add(outcomeViewModel);
                    }
                }

                if (!esElectivo)
                {
                    aux[nivelCmc].CursoMallaCurriculares.Add(cmcViewModel);
                }
                else
                {
                    nivelElectivos.CursoMallaCurriculares.Add(cmcViewModel);
                }
            }

            foreach (NivelViewModel nivelViewModel in aux.Values)
            {
                result.Add(nivelViewModel);
            }

            result = result.OrderBy(x => Convert.ToInt32(x.Numero)).ToList();

            if (nivelElectivos.CursoMallaCurriculares.Any())
            {
                result.Add(nivelElectivos);
            }

            return result;
        }

        /// <summary>
        /// Método para generar los detalles de una malla de cocos de acuerdo a los criterios ingresados
        /// </summary>
        /// <param name="model">View model de la malla de cocos a partir del cual se poblará la malla curricular</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <param name="IdSubModalidadPeriodoAcademico"></param>
        /// <returns>Malla curricular poblada con los detalles de la malla de cocos</returns>
        private MallaCurricular CrearMallaCurricular(CreateCareerCurriculumViewModel model, int idComision, int IdSubModalidadPeriodoAcademico)
        {

          

            var mallaCurricular = new MallaCurricular();
            mallaCurricular.IdMallaCurricular = model.IdMallaCurricular;
            mallaCurricular.CursoMallaCurricular = new List<CursoMallaCurricular>();
            var comision = GedServices.GetComisionServices(Context, idComision);
            var outcomeComisiones = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(Context, comision.IdComision, IdSubModalidadPeriodoAcademico);
            var dicOComisiones = new Dictionary<Tuple<int, int>, OutcomeComision>();
            outcomeComisiones.ForEach(x => dicOComisiones.Add(new Tuple<int, int>(x.IdOutcome, x.IdComision), x));

            foreach (var nivelVM in model.Niveles)
            {
                foreach (var cursoMallaCurricularVM in nivelVM.CursoMallaCurriculares)
                {
                    var cursoMallaCurricular = new CursoMallaCurricular();
                    cursoMallaCurricular.MallaCocosDetalle = new List<MallaCocosDetalle>();
                    cursoMallaCurricular.IdCursoMallaCurricular = cursoMallaCurricularVM.IdCursoMallaCurricular;
                    cursoMallaCurricular.EsFormacion = cursoMallaCurricularVM.EsFormacion;

                    foreach (var outcomeVM in cursoMallaCurricularVM.Outcomes)
                    {
                        if (outcomeVM.IdTipoOutcomeCurso == outcomeVM.OriginalValue) continue;
                        var mallaCocosDetalle = new MallaCocosDetalle
                        {
                            IdCursoMallaCurricular = cursoMallaCurricular.IdCursoMallaCurricular,
                            IdTipoOutcomeCurso = outcomeVM.IdTipoOutcomeCurso,
                            IdOutcomeComision =
                                dicOComisiones[new Tuple<int, int>(outcomeVM.IdOutcome, comision.IdComision)]
                                    .IdOutcomeComision
                        };
                        cursoMallaCurricular.MallaCocosDetalle.Add(mallaCocosDetalle);
                    }

                    mallaCurricular.CursoMallaCurricular.Add(cursoMallaCurricular);
                }
            }

            return mallaCurricular;
        }

        /// <summary>
        /// Método para generar los view models de los resultados de la búsqueda de mallas de cocos de acuerdo a los criterios ingresados
        /// </summary>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todos los resultados para la búsqueda</returns>
        public List<MallaCocosViewModel> GenerarListaMallaCocosViewModel(int idEscuela, string codigoComision, Int32? idCarrera, Int32? IdSubModalidadPeriodoAcademico, Int32? idAcreditadora)
        { 
            //ESTE SI RECIBE SUBMODALIDAD V

            var result = new List<MallaCocosViewModel>();
            var listaMallaCocos = GedServices.GetMallaCocosServices(CargarDatosContext(), idEscuela , codigoComision, idCarrera, IdSubModalidadPeriodoAcademico, idAcreditadora);

            foreach (var mallaCocos in listaMallaCocos)
            {
                var carreraComision = GedServices.GetCarreraComisionServices(Context, mallaCocos.IdCarreraComision);
                //if (carreraComision.Comision.Codigo == "WASC")
                //{
                //    continue;
                //}
                var carrera = GedServices.GetCarreraInCarreraComisionServices(Context, mallaCocos.IdCarreraComision);
                var mallaCocosViewModel = new MallaCocosViewModel
                {
                    IdMallaCocos = mallaCocos.IdMallaCocos,
                    Descripcion = mallaCocos.Detalle,
                    Ciclo = mallaCocos.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Carrera = CargarDatosContext().currentCulture == Culture.ESPANOL ? carrera.NombreEspanol : carrera.NombreIngles
                };
                result.Add(mallaCocosViewModel);
            }

            return result;
        }

        const string IconoBlank = "blank.png";
        const string IconoFormacion = "coco-form.png";
        const string NombreCursosElectivos = "Cursos Electivos";
        const string IconoBorrar = "x-borrar.png";
        const string LogoUpc = "logo-upc-red.png";
        
    }

    
}
