﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class IfcDiscoveryManagementController : BaseController
    {
        // GET: Professor/IfcDiscoveryManagement
        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }

        [AuthorizeUserAttribute(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult Consult(bool? isSearchRequest, int? idSubModalidadPeriodoAcademico, int? idSede, string codigoNivelAceptacion, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, string idCursoUnidadAcademica)
        {
            var idEscuela = Session.GetEscuelaId();

            var vm = new ConsultHallazgoIfcViewModel();

            var areas = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            vm.AvailableAreas = areas.Select(o => new SelectListItem { Value = o, Text = o });

            var subareas = GedServices.GetAllSubareasServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            vm.AvailableSubareas = subareas.Select(o => new SelectListItem { Value = o, Text = o });

            var cursos = GedServices.GetAllCursosServices(Context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            vm.AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });

            vm.AvailablePeriodoAcademicos = GedServices.GetAllPeriodoAcademicosServices(Context)
                .Select(o => new SelectListItem { Value = o.IdPeriodoAcademico.ToString(), Text = o.CicloAcademico }).OrderByDescending(o => o.Text);

            vm.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            vm.AvailableSedes = GedServices.GetAllSedesServices(Context)
                .Select(o => new SelectListItem { Value = o.IdSede.ToString(), Text = o.Nombre });

            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                //vm.Resultados = GenerarListaHallazgosIfcResultViewModel(HallazgoDaoImpl.GetHallazgosIfc(context, idAreaUnidadAcademica, idSubareaUnidadAcademica, idCursoUnidadAcademica, SubModalidadPeriodoAcademicoId, codigoNivelAceptacion, idSede));     // RML001
                vm.Resultados = GenerarListaHallazgosZIfcResultViewModel(HallazgoDaoImpl.GetHallazgosZIfc(Context, idAreaUnidadAcademica, idSubareaUnidadAcademica, idCursoUnidadAcademica, idSubModalidadPeriodoAcademico, codigoNivelAceptacion));

                if (vm.Resultados == null || !vm.Resultados.Any())
                {
                    vm.Resultados = new List<HallazgoViewModel>();
                    PostMessage(MessageType.Info, MessageResource.NoSeEcontraronResultadosParaBusqueda);
                }

            }

            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite)]
        public ActionResult Edit(int id)
        {
            //  var hallazgo = (from h in context.Hallazgo where h.IdHallazgo == id select h).FirstOrDefault();
            var hallazgo = (from h in Context.Hallazgos where h.IdHallazgo == id select h).FirstOrDefault();
            var viewModel = new EditHallazgoIfcViewModel();
            try
            {
                viewModel.IdHallazgo = hallazgo.IdHallazgo;
                //   viewModel.Codigo = hallazgo.Codigo;
                viewModel.Codigo = hallazgo.Codigo + '-' + hallazgo.Identificador.ToString();
                // viewModel.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
                viewModel.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
                //      viewModel.Sede = hallazgo.Sede == null ? "Sin Sede": hallazgo.Sede.Nombre;
                viewModel.Descripcion = hallazgo.DescripcionEspanol;
                //       viewModel.NivelAceptacion = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? hallazgo.NivelAceptacionHallazgo.NombreEspanol : hallazgo.NivelAceptacionHallazgo.NombreIngles;
                viewModel.NivelAceptacion = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? hallazgo.NivelAceptacionHallazgo.NombreEspanol : hallazgo.NivelAceptacionHallazgo.NombreIngles;
                viewModel.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
                viewModel.InstrumentoId = hallazgo.ConstituyenteInstrumento.IdInstrumento;
                viewModel.IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
                viewModel.NivelAceptacionList = Context.NivelAceptacionHallazgo.ToList();
                var instrumento = Context.Instrumento.FirstOrDefault(x => x.IdInstrumento == hallazgo.ConstituyenteInstrumento.IdConstituyenteInstrumento);
                viewModel.NombreInstrumento = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? instrumento.NombreEspanol : instrumento.NombreIngles;
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoProcesarSuConsulta);
                return RedirectToAction("ViewDiscovery", "Discoveries", new { id = viewModel.IdHallazgo });
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite)]
        public ActionResult Edit(EditHallazgoIfcViewModel model)
        {
            System.Diagnostics.Debug.WriteLine("modelIdHallazgo: " + model.IdHallazgo + ", modelNivelHallazgo: " + model.IdNivelAceptacionHallazgo);
            try
            {
                //  Hallazgo hallazgo = context.Hallazgo.Where(x => x.IdHallazgo == model.IdHallazgo).FirstOrDefault();
                Hallazgos hallazgo = Context.Hallazgos.Where(x => x.IdHallazgo == model.IdHallazgo).FirstOrDefault();
                hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;

                Context.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            if (!ModelState.IsValid)
            {
                //System.Diagnostics.Debug.WriteLine("-" + model.Criticidad);
                model.IdHallazgo = model.IdHallazgo;
                model.Codigo = model.Codigo;
                model.PeriodoAcademico = model.PeriodoAcademico;
                //  model.Sede = model.Sede ?? "Sin Sede";
                model.Descripcion = model.Descripcion;
                //  model.NivelAceptacion = model.;
                model.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                model.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, model.IdHallazgo);
                model.InstrumentoId = model.InstrumentoId;
                model.Identificador = model.Identificador;
                var instrumento = Context.Instrumento.FirstOrDefault(x => x.IdInstrumento == model.InstrumentoId);
                model.NombreInstrumento = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? instrumento.NombreEspanol : instrumento.NombreIngles;

                TryUpdateModel(model);
                return View(model);
            }
            model.Descripcion = model.Descripcion.TrimRemoveDiacritics();

            //    bool success = GedServices.ActualizarHallazgoifcServices(context, model.IdHallazgo, model.NivelAceptacion, model.Descripcion);

            bool success = GedServices.ActualizarHallazgoZifcServices(Context, model.IdHallazgo, model.IdNivelAceptacionHallazgo, model.Descripcion);

            if (success)
            {
                PostMessage(MessageType.Success, @""+MessageResource.Seeditosatisfactoriamente);
            }
            else
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoEditarElInformeDeFinDeCiclo);
            }

            return RedirectToAction("ViewDiscovery", "Discoveries", new { id = model.IdHallazgo });
        }

        [AuthorizeUserAttribute(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador)]
        public ActionResult View(int id)
        {
            // var hallazgo = (from h in context.Hallazgo where h.IdHallazgo == id select h).FirstOrDefault();                           // RML001
            var hallazgo = (from h in Context.Hallazgos where h.IdHallazgo == id select h).FirstOrDefault();

            var vm = new EditHallazgoIfcViewModel();

            vm.IdHallazgo = hallazgo.IdHallazgo;
            //     vm.Codigo = hallazgo.Codigo;
            vm.Codigo = hallazgo.Codigo + '-' + hallazgo.Identificador.ToString();
            vm.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            //     vm.Sede = hallazgo.Sede.Nombre;
            vm.Descripcion = hallazgo.DescripcionEspanol;
            //     vm.NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad);
            vm.NivelAceptacion = GedServices.GetTextoForIdNivelAceptacionHallazgoIfcServices(hallazgo.IdNivelAceptacionHallazgo);
            vm.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
            vm.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });

            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite)]
        public ActionResult Delete(int id)
        {
            //    var hallazgo = (from h in context.Hallazgo where h.IdHallazgo == id select h).FirstOrDefault();                             // RML001
            var hallazgo = (from h in Context.Hallazgos where h.IdHallazgo == id select h).FirstOrDefault();

            var vm = new EditHallazgoIfcViewModel();

            vm.IdHallazgo = hallazgo.IdHallazgo;
            //  vm.Codigo = hallazgo.Codigo;
            vm.Codigo = hallazgo.Codigo + '-' + hallazgo.Identificador.ToString();
            vm.PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            // vm.Sede = hallazgo.Sede == null ? "Sin Sede" : hallazgo.Sede.Nombre;
            vm.Descripcion = hallazgo.DescripcionEspanol;
            vm.NivelAceptacion = GedServices.GetTextoForIdNivelAceptacionHallazgoIfcServices(hallazgo.IdNivelAceptacionHallazgo);
            vm.AccionesMejora = GedServices.GetAccionesMejoraForHallazgoIfcServices(Context, hallazgo.IdHallazgo);
            vm.AvailableNivelesAceptacion = GedServices.GetAllNivelesAceptacionForHallazgosIfcServices()
                .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });
            vm.InstrumentoId = hallazgo.ConstituyenteInstrumento.IdInstrumento;
            var instrumento = Context.Instrumento.FirstOrDefault(x => x.IdInstrumento == hallazgo.ConstituyenteInstrumento.IdInstrumento);
            vm.NombreInstrumento = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? instrumento.NombreEspanol : instrumento.NombreIngles;
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite)]
        public ActionResult Delete(EditHallazgoIfcViewModel model)
        {
            int idHallazgo = model.IdHallazgo;

            // bool success = GedServices.EliminarHallazgoIfcServices(context, idHallazgo);

            bool success = GedServices.EliminarHallazgoZIfcServices(Context, idHallazgo);

            if (success)
            {
                PostMessage(MessageType.Success, @""+MessageResource.SeEliminoExitosamenteHallazgoInformeDeFindDeCiclo);
            }
            else
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoEliminarElHallazgoPorquePerteneceAunPlanDeAccion);
            }

            return RedirectToAction("Discoveries", "Discoveries", new { InstrumentoId = model.InstrumentoId, ResultadoId = 1 });
        }

        private List<HallazgoViewModel> GenerarListaHallazgosIfcResultViewModel(List<Hallazgo> hallazgos)
        {
            var vms = new List<HallazgoViewModel>();

            foreach (var hallazgo in hallazgos)
            {
                var vm = new HallazgoViewModel
                {
                    IdHallazgo = hallazgo.IdHallazgo,
                    PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Sede = hallazgo.Sede.Nombre,
                    Descripcion = hallazgo.DescripcionEspanol,
                    Codigo = hallazgo.Codigo,
                    NivelAceptacion = GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad)
                };

                vms.Add(vm);
            }

            return vms;
        }

        private List<HallazgoViewModel> GenerarListaHallazgosZIfcResultViewModel(List<Hallazgos> hallazgos)
        {
            var vms = new List<HallazgoViewModel>();

            foreach (var hallazgo in hallazgos)
            {
                var vm = new HallazgoViewModel
                {
                    IdHallazgo = hallazgo.IdHallazgo,
                    PeriodoAcademico = hallazgo.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    //    Sede = hallazgo.Sede.Nombre,
                    Descripcion = hallazgo.DescripcionEspanol,
                    Codigo = hallazgo.Codigo + '-' + hallazgo.Identificador.ToSafeString(),
                    NivelAceptacion = GedServices.GetTextoForIdNivelAceptacionHallazgoIfcServices(hallazgo.IdNivelAceptacionHallazgo)
                };

                vms.Add(vm);
            }

            return vms;
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _AddHallazgo(string codCurso, int idSubModalidad)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            //var vm = "Lo comenté porque no compilaba";
            var vm = new AddHallazgoViewModel()
            {
                Criticidades = Context.Criticidad.ToList(), //criticidades,
            };
            //-----------------------------------------------------------------------------------------
            //Se uso el codCurso="SI410" para las pruebas con multiselect
            //Se uso el codCurso="CC50" Para las pruebas con un solo outcome
            vm.CargarOutcomes(codCurso, idSubModalidad, Context);
            //-----------------------------------------------------------------------------------------
            // var hallazgo = (context.Hallazgo.Where(x => x.Codigo == codHallazgo).FirstOrDefault());
            // vm.descripcionHallazgo = hallazgo.DescripcionEspanol;
            vm.CargarData(codCurso, Context);
            vm.ListOutcomes = vm.ListaOutcomes.ToList().Select(o => new SelectListItem { Value = o.IdOutcome.ToString(), Text = o.Nombre + " - " + o.Codigo + " - " + (language == ConstantHelpers.CULTURE.ESPANOL ? o.DescripcionEspanol : o.DescripcionIngles).Substring(0, 50) + "..." });
            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _EditHallazgo(string codHallazgo, string codCurso, string criticidad, int pos)
        {

            //var vm = "Lo comenté porque no compilaba";
            var vm = new EditHallazgoViewModel()
            {
                Criticidades = Context.Criticidad.ToList(), //criticidades,
            };
            var hallazgo = (Context.Hallazgo.Where(x => x.Codigo == codHallazgo).FirstOrDefault());
            vm.descripcionHallazgo = hallazgo.DescripcionEspanol;
            vm.criticidad = criticidad;
            vm.pos = pos;
            vm.CargarData(codCurso, Context);
            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _AddAccionMejora(string codCurso)
        {
            var vm = new AddHallazgoViewModel()
            {
                Criticidades = Context.Criticidad.ToList(), //criticidades,
            };
            vm.CargarData(codCurso, Context);
            return View(vm);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _EditAccionMejora(int accionmejoraid, string codigocurso, int ifcid)
        {
            _EditAccViewModel viewmodel = new _EditAccViewModel();

            viewmodel.CargarData(Context, accionmejoraid, codigocurso, ifcid);

            return View(viewmodel);
        }
        [HttpPost]
        public JsonResult _EditAccionMejora(_EditAccViewModel viewmodel)
        {

            try
            {
                if (viewmodel.descripcionespanol == null || viewmodel.descripcioningles == null)
                {
                    return Json("nodescripcion", JsonRequestBehavior.AllowGet);
                }

                String descripcionespanol = viewmodel.descripcionespanol;

                for (int i = 0; i < descripcionespanol.Count(); i++)
                {
                    if (!String.Equals(descripcionespanol[i].ToString(), " "))
                    {
                        var accionmejoratoedit = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == viewmodel.accionmejoraid);

                        accionmejoratoedit.DescripcionEspanol = viewmodel.descripcionespanol;
                        accionmejoratoedit.DescripcionIngles = viewmodel.descripcioningles;
                        Context.SaveChanges();

                        return Json("trueedit", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("fullspace", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _EditHallazgo(int hallazgoid, string codigocurso)
        {
            _EditHallazgoViewModel viewmodel = new _EditHallazgoViewModel();

            viewmodel.CargarData(Context, hallazgoid, codigocurso);

            return View(viewmodel);
        }

        [HttpPost]
        public JsonResult _EditHallazgo(_EditHallazgoViewModel viewmodel)
        {

            try
            {
                if (viewmodel.descripcionespanol == null || viewmodel.descripcioningles == null)
                {
                    return Json("nodescripcion", JsonRequestBehavior.AllowGet);
                }

                String descripcionespanol = viewmodel.descripcionespanol;

                for (int i = 0; i < descripcionespanol.Count(); i++)
                {
                    if (!String.Equals(descripcionespanol[i].ToString(), " "))
                    {
                        var hallazgoedit = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == viewmodel.hallazgoid);

                        hallazgoedit.DescripcionEspanol = viewmodel.descripcionespanol;
                        hallazgoedit.DescripcionIngles = viewmodel.descripcioningles;
                        hallazgoedit.IdCriticidad = viewmodel.criticidadid;

                        Context.SaveChanges();

                        return Json("trueedit", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("fullspace", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult _EditIFCHallazgo(int idHallazgo, string codigocurso)
        {
            _EditIFCHallazgoViewModel viewmodel = new _EditIFCHallazgoViewModel();

            viewmodel.CargarData(Context, idHallazgo, codigocurso);

            return View(viewmodel);
        }

        [HttpPost]
        public JsonResult _EditIFCHallazgo(_EditAccViewModel viewmodel)
        {

            try
            {
                if (viewmodel.descripcionespanol == null || viewmodel.descripcioningles == null)
                {
                    return Json("nodescripcion", JsonRequestBehavior.AllowGet);
                }
                String descripcionespanol = viewmodel.descripcionespanol;

                for (int i = 0; i < descripcionespanol.Count(); i++)
                {
                    if (!String.Equals(descripcionespanol[i].ToString(), " "))
                    {
                        var hallazgoToEdit = Context.Hallazgos.Where(x => x.IdHallazgo == viewmodel.idhallazgo).FirstOrDefault();
                        var criticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == viewmodel.nombrecriticidad || x.NombreIngles == viewmodel.nombrecriticidad);
                        hallazgoToEdit.DescripcionEspanol = viewmodel.descripcionespanol;
                        hallazgoToEdit.DescripcionIngles = viewmodel.descripcioningles;
                        hallazgoToEdit.IdCriticidad = criticidad.IdCriticidad;
                        Context.SaveChanges();

                        return Json("trueedit", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("fullspace", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult AddHallazgoCoordinador(string codigocurso,int ifcid)
        {
            _EditIFCHallazgoViewModel viewmodel = new _EditIFCHallazgoViewModel();
            viewmodel.CargarAddHallazgo(Context, codigocurso);
            return View(viewmodel);
        }
        [HttpPost]
        public JsonResult AddHallazgoCoordinador(_EditAccViewModel viewmodel)
        {
            try
            {
                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                var ifc = Context.IFC.FirstOrDefault(x => x.IdIFC == viewmodel.ifcid);
                var unidadacademica = Context.UnidadAcademica.FirstOrDefault(x => x.IdUnidadAcademica == ifc.IdUnidadAcademica);
                var smpam = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == unidadacademica.IdSubModalidadPeriodoAcademico);
                var curso = Context.Curso.FirstOrDefault(x => x.Codigo == viewmodel.codigocurso);
                var criticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == viewmodel.nombrecriticidad);         
                Hallazgos newhallazgo = new Hallazgos();
                newhallazgo.DescripcionEspanol = viewmodel.descripcionespanol;
                if (viewmodel.descripcioningles.Count() <= 0)
                {
                    newhallazgo.DescripcionIngles = helper.Translate(newhallazgo.DescripcionEspanol);
                }
                else
                {
                    newhallazgo.DescripcionIngles = viewmodel.descripcioningles;
                }
                newhallazgo.IdCurso = curso.IdCurso;
                newhallazgo.IdSubModalidadPeriodoAcademicoModulo = smpam.IdSubModalidadPeriodoAcademicoModulo;
                newhallazgo.IdConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.Instrumento.NombreEspanol == "IFC").IdConstituyenteInstrumento;
                newhallazgo.Codigo = Convert.ToString(Context.Database.SqlQuery<string>("select[dbo].[FN_GetCodigoHallazgo](@p0, @p1, @p2,@p3)", newhallazgo.IdConstituyenteInstrumento,curso.IdCurso, 0,0).FirstOrDefault());
                newhallazgo.IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.Where(x => x.NombreEspanol.Contains("Necesita mejora")).FirstOrDefault().IdNivelAceptacionHallazgo;
                newhallazgo.IdCriticidad = criticidad.IdCriticidad;
                newhallazgo.Estado = "ACT";
                newhallazgo.FechaRegistro = DateTime.Now;
                FindLogic ofl = new FindLogic(CargarDatosContext(), newhallazgo.IdConstituyenteInstrumento,smpam.IdSubModalidadPeriodoAcademico);
                newhallazgo.Identificador = ofl.ObtenerIndiceHallazgo();

                Context.Hallazgos.Add(newhallazgo);

                IFCHallazgo newifchallazgo = new IFCHallazgo();
                newifchallazgo.idIFC = ifc.IdIFC;
                newifchallazgo.IdHallazgo = newhallazgo.IdHallazgo;

                Context.IFCHallazgo.Add(newifchallazgo);

                Context.SaveChanges();

                return Json("truecreate", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult AddAccionMejoraCoordinador(string codigocurso, int ifcid)
        {
            _EditAccViewModel viewmodel = new _EditAccViewModel();
            viewmodel.CargarData(Context, 0, codigocurso, ifcid);
            return View(viewmodel);
        }

        [HttpPost]
        public JsonResult AddAccionMejoraCoordinador(_EditAccViewModel viewmodel)
        {
            try
            {
                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                AccionMejora newaccionmejora = new AccionMejora();
                int modalidadid = Session.GetModalidadId();
                var hallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == viewmodel.idhallazgo);
                var smpam = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == hallazgo.IdSubModalidadPeriodoAcademicoModulo);
                var ifc = Context.IFC.FirstOrDefault(X => X.IdIFC == viewmodel.ifcid);
                var anio = (from a in Context.IFC
                            join b in Context.UnidadAcademica on a.IdUnidadAcademica equals b.IdUnidadAcademica
                            join c in Context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                            join d in Context.PeriodoAcademico on c.IdPeriodoAcademico equals d.IdPeriodoAcademico
                            where b.IdUnidadAcademica == ifc.IdUnidadAcademica
                            select new
                            {
                                anio = d.FechaInicioPeriodo
                            }).FirstOrDefault();
                FindLogic ContIndicador = new FindLogic(CargarDatosContext(), 1, smpam.IdSubModalidadPeriodoAcademico);
                newaccionmejora.DescripcionEspanol = viewmodel.descripcionespanol;
                if (viewmodel.descripcioningles.Count() > 0)
                {
                    newaccionmejora.DescripcionIngles = viewmodel.descripcioningles;
                }
                else
                {
                    newaccionmejora.DescripcionIngles = helper.Translate(newaccionmejora.DescripcionEspanol);
                }
                newaccionmejora.ParaPlan = false;
                newaccionmejora.Identificador = ContIndicador.ObtenerIndiceAccionMejora();
                newaccionmejora.Codigo = "A" + hallazgo.Codigo.Substring(1);
                newaccionmejora.Anio = Convert.ToDateTime(anio.anio).Year;
                newaccionmejora.IdEscuela = EscuelaId;
                newaccionmejora.IdSubModalidadPeriodoAcademico = smpam.IdSubModalidadPeriodoAcademico;
                newaccionmejora.Estado = ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO;

                Context.AccionMejora.Add(newaccionmejora);

                HallazgoAccionMejora newham = new HallazgoAccionMejora();
                newham.IdHallazgo = viewmodel.idhallazgo;
                newham.IdAccionMejora = newaccionmejora.IdAccionMejora;

                Context.HallazgoAccionMejora.Add(newham);

                Context.SaveChanges();

                return Json("truecreate", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDescripcion(int hallazgoid)
        {
            var descripcionhallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == hallazgoid);

            return Json(descripcionhallazgo.DescripcionEspanol, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteHallazgoIFC(int hallazgoid)
        {
            try
            {
                var hallazgodelete = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == hallazgoid);
                var ifchallazgodelete = Context.IFCHallazgo.FirstOrDefault(x => x.IdHallazgo == hallazgoid);
                var accionmejorahallazgo = Context.HallazgoAccionMejora.Where(x => x.IdHallazgo == hallazgoid).ToList();
                var accionmejora = (from a in accionmejorahallazgo
                                    join b in Context.AccionMejora on a.IdAccionMejora equals b.IdAccionMejora
                                    select b).ToList();

                Context.IFCHallazgo.Remove(ifchallazgodelete);

                if (accionmejorahallazgo.Count() > 0)
                {
                    foreach (var amh in accionmejorahallazgo)
                    {
                        Context.HallazgoAccionMejora.Remove(amh);
                    }
                }
                if (accionmejora.Count() > 0)
                {
                    foreach (var am in accionmejora)
                    {
                        Context.AccionMejora.Remove(am);
                    }
                }

                var ifcHallazgoOutcome = Context.IFCHallazgoOutcome.Where(x => x.IdHallazgo == hallazgodelete.IdHallazgo).ToList();

                foreach(var item in ifcHallazgoOutcome)
                {
                    Context.IFCHallazgoOutcome.Remove(item);
                }

                Context.Hallazgos.Remove(hallazgodelete);            
                Context.SaveChanges();

                return Json("deletetrue", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }          
        }

        public JsonResult DeleteAccionMejoraIFC(int accionmejoraid)
        {
            try
            {
                var deleteaccionmejora = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == accionmejoraid);
                var deleteaccionmejorahallazgo = Context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == accionmejoraid).ToList();

                foreach (var damh in deleteaccionmejorahallazgo)
                {
                    Context.HallazgoAccionMejora.Remove(damh);
                }

                Context.AccionMejora.Remove(deleteaccionmejora);
                Context.SaveChanges();             

                return Json("deletetrue", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ValidarIFC(int ifcid)
        {
            try
            {
                var hallazgoifcsinam = (from a in Context.IFC
                                        join b in Context.IFCHallazgo on a.IdIFC equals b.idIFC
                                        join c in Context.HallazgoAccionMejora on b.IdHallazgo equals c.IdHallazgo
                                        join d in Context.Hallazgos on c.IdHallazgo equals d.IdHallazgo
                                        where a.IdIFC == ifcid
                                        select d.IdHallazgo).ToList();

                var lstifchallazgotodelete = (from a in Context.IFCHallazgo
                                              join b in Context.Hallazgos on a.IdHallazgo equals b.IdHallazgo
                                              where a.idIFC == ifcid && !hallazgoifcsinam.Contains(a.IdHallazgo)
                                              select b).ToList();

                if (lstifchallazgotodelete.Count() > 0)
                {
                    return Json("hallazgoslibres", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }          
        }
    }
}
