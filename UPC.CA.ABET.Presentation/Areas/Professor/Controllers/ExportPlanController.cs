﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ExportPlan;
using UPC.CA.ABET.Presentation.Areas.Report.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class ExportPlanController : ReportBaseController
    {
        private ExportPlanModel model;

        public ExportPlanController()
        {
            this.model = new ExportPlanModel(reportBaseModel, this);
        }

        public ActionResult ImprovementPlan()
        {
            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();

            var ViewModel = new ImprovementPlanViewModel();
            ViewModel.Fill(CargarDatosContext(),ModalidadId,EscuelaId);

            return View(ViewModel);

        }
        [HttpPost]
        public ActionResult ImprovementPlan(ImprovementPlanViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.HasValue = true;
            }
            else {
                viewModel.HasValue = false;
            }
            return PartialView("_ImprovementPlan", viewModel);
        }
        public JsonResult IdModalidad(int IdModalidad)
        {
            int EscuelaId = Session.GetEscuelaId();

            var queryCar = (from c in Context.Carrera
                            join sm in Context.SubModalidad on c.IdSubmodalidad equals sm.IdSubModalidad
                            join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                            where c.IdEscuela==EscuelaId
                            select new
                            {
                                IdCarrera = c.IdCarrera,
                                Descripcion = c.NombreEspanol,
                                modalidad = m.IdModalidad
                            });

            var LstCarrera = new List<SelectListItem>();
             if (IdModalidad != 0)
                {
                    LstCarrera = (from c in queryCar
                                  where c.modalidad == IdModalidad 
                                  select new SelectListItem
                                  {
                                      Value = c.IdCarrera.ToString(),
                                      Text = c.Descripcion
                                  }
                                  ).Distinct().ToList();
                }
                else
                {
                    LstCarrera = (from c in queryCar
                                  select new SelectListItem
                                  {
                                      Value = c.IdCarrera.ToString(),
                                      Text = c.Descripcion
                                  }).Distinct().ToList();
                }
            
            return Json(new SelectList(LstCarrera, "Value","Text"), JsonRequestBehavior.AllowGet);

        }

        public JsonResult IdCarrera(int? anio , int? modalidad , int? carrera)
        {
            int EscuelaId = Session.GetEscuelaId();

            var queryCom = (from cc in Context.CarreraComision
                            join c in Context.Carrera on cc.IdCarrera equals c.IdCarrera
                            join co in Context.Comision on cc.IdComision equals co.IdComision
                            join spa in Context.SubModalidadPeriodoAcademico on cc.IdSubModalidadPeriodoAcademico equals spa.IdSubModalidadPeriodoAcademico
                            join pm in Context.PlanMejora on spa.IdSubModalidadPeriodoAcademico equals pm.IdSubModalidadPeriodoAcademico
                            join pa in Context.PeriodoAcademico on spa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                            join sm in Context.SubModalidad on spa.IdSubModalidad equals sm.IdSubModalidad
                            join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                            where c.IdEscuela==EscuelaId
                            select new {
                                IdComision = co.IdComision,
                                Codigo = co.Codigo,
                                IdCarrera = cc.IdCarrera,
                                IdModalidad = m.IdModalidad,
                                Anio = pm.IdPlanMejora,
                                Estado = pa.Estado
                            }
                            );

            var LstComision = new List<SelectListItem>();

            var lstcomision = (from a in Context.Comision
                               join b in Context.CarreraComision on a.IdComision equals b.IdComision
                               join c in Context.Carrera on b.IdCarrera equals c.IdCarrera
                               where c.IdCarrera == carrera && a.Codigo != "WASC" && c.IdEscuela==EscuelaId
                               select new
                               {
                                  codigo = a.Codigo
                               }).ToList();


     
            lstcomision = lstcomision.Distinct().ToList();

            List<string> filtradocomision = new List<string>();

            foreach(var item in lstcomision)
            {
                if (!filtradocomision.Contains(item.codigo.ToString().Substring(0,3)))
                {
                    filtradocomision.Add(item.codigo.ToString());

                }
            }

            foreach (var comision in filtradocomision)
            {
                LstComision.Add(new SelectListItem { Value = comision, Text = comision });
            }


            

            return Json(new SelectList(LstComision, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult IdInstrumento(int? anio, int? modalidad, int? carrera)
        {
            int EscuelaId = Session.GetEscuelaId();

            var queryIns= (from pm in Context.PlanMejora
                           join pma in Context.PlanMejoraAccion on pm.IdPlanMejora equals pma.IdPlanMejora
                           join am in Context.AccionMejora on pma.IdAccionMejora equals am.IdAccionMejora
                           join ham in Context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                           join h in Context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                           join ci in Context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                           join i in Context.Instrumento on ci.IdInstrumento equals i.IdInstrumento     
                           join smpam in Context.SubModalidadPeriodoAcademicoModulo on h.IdSubModalidadPeriodoAcademicoModulo  equals smpam.IdSubModalidadPeriodoAcademicoModulo
                           join smpa in Context.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico       
                           join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                           join ca in Context.Carrera on sm.IdSubModalidad equals ca.IdSubmodalidad
                           join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                           where pm.IdEscuela==EscuelaId
                           orderby i.IdInstrumento 
                           select new 
                           {
                               IdInstrumento = i.IdInstrumento.ToString(),
                               Acronimo = i.Acronimo,
                               IdModalidad = m.IdModalidad,
                               Anio = pm.IdPlanMejora,
                               Carrera = ca.IdCarrera
                           }).Distinct().ToList();


            var LstInstrumento = new List<SelectListItem>();
            if (carrera != 0)
            {
                if (modalidad != 0)
                {
                    LstInstrumento = (from i in queryIns
                                   where i.IdModalidad == modalidad && i.Anio == anio && i.Carrera== carrera
                                   select new SelectListItem
                                   {
                                       Value = i.IdInstrumento,
                                       Text = i.Acronimo
                                   }).Distinct().ToList();
                }
                else
                {
                    LstInstrumento = (from i in queryIns
                                      where  i.Anio == anio && i.Carrera == carrera
                                      select new SelectListItem
                                      {
                                          Value = i.IdInstrumento,
                                          Text = i.Acronimo
                                      }).Distinct().ToList();
                }

            }
            else
            {
                if (modalidad != 0)
                {
                    LstInstrumento = (from i in queryIns
                                      where i.IdModalidad == modalidad && i.Anio == anio
                                      select new SelectListItem
                                      {
                                          Value = i.IdInstrumento,
                                          Text = i.Acronimo
                                      }).Distinct().ToList();
                }
                else
                {
                    LstInstrumento = (from i in queryIns
                                      where i.Anio == anio
                                      select new SelectListItem
                                      {
                                          Value = i.IdInstrumento,
                                          Text = i.Acronimo
                                      }).Distinct().ToList();
                }

            }
            LstInstrumento.Insert(0, new SelectListItem { Value = "0", Text = "TODOS" });

            return Json(new SelectList(LstInstrumento.Distinct(), "Value", "Text"), JsonRequestBehavior.AllowGet);

        }

        public JsonResult Anio(int? planid)
        {
            try
            {
                int EscuelaId = Session.GetEscuelaId();
                var planmejora = Context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == planid && x.IdEscuela==EscuelaId).Anio;
                return Json(planmejora, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult IdConstituyente(int? anio, int? modalidad, int? carrera , int? instrumento)
        {
            var queryCons = (from con in Context.Constituyente
                             join ci in Context.ConstituyenteInstrumento on con.IdConstituyente equals ci.IdConstituyente
                            join h in Context.Hallazgos on ci.IdConstituyenteInstrumento equals h.IdConstituyenteInstrumento
                             join smpam in Context.SubModalidadPeriodoAcademicoModulo on h.IdSubModalidadPeriodoAcademicoModulo equals smpam.IdSubModalidadPeriodoAcademicoModulo
                             join smpa in Context.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                             join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                             join ca in Context.Carrera on smpa.IdSubModalidad equals ca.IdSubmodalidad
                            join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                            join spa in Context.SubModalidadPeriodoAcademico  on sm.IdSubModalidad equals spa.IdSubModalidad
                            join pm in Context.PlanMejora  on spa.IdSubModalidadPeriodoAcademico equals pm.IdSubModalidadPeriodoAcademico
                            join i in Context.Instrumento on ci.IdInstrumento equals i.IdInstrumento
                            orderby con.NombreEspanol
                            select new
                            {
                                IdInstrumento = i.IdInstrumento,
                                NombreEspanol = con.NombreEspanol,
                                IdModalidad = m.IdModalidad,
                                Anio = pm.IdPlanMejora,
                                Carrera=ca.IdCarrera,
                                IdConstituyente = con.IdConstituyente
                            }).Distinct().ToList();

            var queryConsSinCarr = ( from con in Context.Constituyente
                                     join ci in Context.ConstituyenteInstrumento on con.IdConstituyente equals ci.IdConstituyente
                                     join h in Context.Hallazgos on ci.IdConstituyenteInstrumento equals h.IdConstituyenteInstrumento
                                     join smpam in Context.SubModalidadPeriodoAcademicoModulo on h.IdSubModalidadPeriodoAcademicoModulo equals smpam.IdSubModalidadPeriodoAcademicoModulo
                                     join smpa in Context.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                     join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                                     join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                                     join spa in Context.SubModalidadPeriodoAcademico on sm.IdSubModalidad equals spa.IdSubModalidad
                                     join pm in Context.PlanMejora on spa.IdSubModalidadPeriodoAcademico equals pm.IdSubModalidadPeriodoAcademico
                                     join i in Context.Instrumento on ci.IdInstrumento equals i.IdInstrumento
                                     orderby con.NombreEspanol
                                     select new
                                     {
                                         IdInstrumento = i.IdInstrumento,
                                         NombreEspanol = con.NombreEspanol,                                     
                                         IdModalidad = m.IdModalidad,
                                         Anio = pm.IdPlanMejora,
                                         IdConstituyente = con.IdConstituyente
                                     }).Distinct().ToList();


            var LstConstituyente = new List<SelectListItem>();
            if (carrera != 0)
            {
                if (modalidad != 0)
                {
                    if (instrumento != 0)
                    {
                        LstConstituyente = (from con in queryCons
                                            where con.IdModalidad == modalidad && con.Anio == anio && con.IdInstrumento == instrumento
                                            && con.Carrera == carrera
                                          select new SelectListItem
                                          {
                                              Value = con.IdConstituyente.ToString(),
                                              Text = con.NombreEspanol
                                          }).Distinct().ToList();
                    }
                    else {
                        LstConstituyente = (from con in queryCons
                                            where con.IdModalidad == modalidad && con.Anio == anio && con.Carrera == carrera
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                }
                else
                {
                    if (instrumento != 0)
                    {
                        LstConstituyente = (from con in queryCons
                                            where  con.Anio == anio && con.IdInstrumento == instrumento && con.Carrera == carrera
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                    else
                    {
                        LstConstituyente = (from con in queryCons
                                            where con.Anio == anio && con.Carrera == carrera
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                }

            }
            else
            {
                if (modalidad != 0)
                {
                    if (instrumento != 0)
                    {
                        LstConstituyente = (from con in queryConsSinCarr
                                            where con.IdModalidad == modalidad && con.Anio == anio && con.IdInstrumento == instrumento
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                    else
                    {
                        LstConstituyente = (from con in queryConsSinCarr
                                            where con.IdModalidad == modalidad &&  con.Anio == anio
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                }
                else
                {
                    if (instrumento != 0)
                    {
                        LstConstituyente = (from con in queryConsSinCarr
                                            where con.Anio == anio && con.IdInstrumento == instrumento
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                    else
                    {
                        LstConstituyente = (from con in queryConsSinCarr
                                            where con.Anio == anio
                                            select new SelectListItem
                                            {
                                                Value = con.IdConstituyente.ToString(),
                                                Text = con.NombreEspanol
                                            }).Distinct().ToList();
                    }
                }

            }
            LstConstituyente.Insert(0, new SelectListItem { Value = "0", Text = LayoutResource.Todos });

            return Json(new SelectList(LstConstituyente, "Value", "Text"), JsonRequestBehavior.AllowGet);

        }

        public ActionResult PlanOfResults()
        {
            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();

            var ViewModel = new PlanOfResultsViewModel();
            ViewModel.Fill(CargarDatosContext(), ModalidadId,EscuelaId);
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult PlanOfResults(PlanOfResultsViewModel ViewModel)
        {

            if (ModelState.IsValid)
            {
                ViewModel.HasValue = true;
            }
            else
            {
                ViewModel.HasValue = false;
            }

            return PartialView("_PlanOfResults", ViewModel);
        }


   
    }
}