﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Professor;
//using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class DiscoveriesManagementController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }

        //public ActionResult Consult(bool? isSearchRequest, int? IdPeriodoAcademico, string codComision, int? IdCarrera, int? IdSede, int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdNivelAceptacion)
        //{
        //    var vm = new ConsultDiscoveriesViewModel();

        //    // Cargar Periodos Academicos (Ciclos)
        //    vm.AvailablePeriodoAcademicos = GedServices.GetAllPeriodoAcademicosServices(context)
        //            .Select(o => new SelectListItem { Value = o.IdPeriodoAcademico.ToString(), Text = o.CicloAcademico });
        //    if (vm.AvailablePeriodoAcademicos == null)
        //    {
        //        PostMessage(Helpers.MessageType.Error, "No se pudieron cargar los periodos académicos.");
        //    }
           
        //    // Cargar Comisiones
        //    vm.AvailableComisiones = GedServices.GetAllCodigosFromComisionesServices(context)
        //            .Select( c => new SelectListItem { Value = c, Text = c == "EAC" ? "Ingeniería" : "Computación" });
        //    if (vm.AvailableComisiones == null)
        //    {
        //        PostMessage(Helpers.MessageType.Error, "No se pudieron cargar las comisiones.");
        //    }

        //    // Cargar Carreras
        //    vm.AvailableCarreras = GedServices.GetAllCarrerasServices(context)
        //        .Select(c => new SelectListItem { Value = c.IdCarrera.ToString(), Text = c.NombreEspanol });
        //    if (vm.AvailableCarreras == null)
        //    {
        //        PostMessage(Helpers.MessageType.Error, "No se pudieron cargar las carreras.");
        //    }

            
        //    //Cargar sedes
        //    //vm.AvailableSedes = PmSharedLogic.GetAllSedes(context).Select(s => new SelectListItem { Value = s.IdSede.ToString(), Text = s.Nombre });
        //    //if ( vm.AvailableSedes == null)
        //    //{
        //    //    PostMessage(Helpers.MessageType.Error, "No se pudieron cargar las sedes");
        //    //}




        //        //public JsonResult GetInstrumentoByConstituyente(string codigoConstituyente)
        //        //{
        //        //    var
        //        //}
        

        //    //Cargar constituyente
        //    //vm.AvailableConstituyentes = PmSharedLogic.getallc


        //    //Cargar instrumentos
        //    //vm.AvailableInstrumentos = PmSharedLogic.GetAllInstrumentos(context)
        //    //    .Select(o => new SelectListItem { Value = o.IdInstrumento.ToString(), Text = o.NombreEspanol });
        //    //if (vm.AvailableInstrumentos == null)
        //    //{
        //    //    PostMessage(Helpers.MessageType.Error, "No se pudieron cargar los instrumentos");
        //    //}



        //    // Si es request de búsqueda
        //    //if (isSearchRequest != null && (bool)isSearchRequest)
        //    //{
        //    //    // Realizar búsqueda y guardar resultados
        //    //    vm.Resultados = GenerarListaMallaCocosViewModel(codigoComision, IdCarrera, IdPeriodoAcademico);
        //    //    if (vm.Resultados == null || !vm.Resultados.Any())
        //    //    {
        //    //        PostMessage(Helpers.MessageType.Info, "No se encontraron resultados para la búsqueda.");
        //    //    }
        //    //}


        //    return View(vm);
        //}

        //public ActionResult View()
        //{
        //    return Index();
        //}

        }
}