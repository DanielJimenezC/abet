﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Professor/Home
        public ActionResult Index()
        {
            return RedirectToAction("Consult", "CareerCurriculum");
        }
        public ActionResult Dashboard()
        {
            return RedirectToAction("Consult", "CareerCurriculum");
        }
    }
}