﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;


namespace CustomExtensions
{

    //Extension methods must be defined in a static class
    public static class StringExtension
    {
        // This is the extension method.
        // The first parameter takes the "this" modifier
        // and specifies the type for which the method is defined.
        public static string RenderPartialViewToString(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                // Find the partial view by its name and the current controller context.
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                // Create a view context.
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                // Render the view using the StringWriter object.
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
    using CustomExtensions;
    using System.Collections.Generic;

    [AuthorizeUser(AppRol.Administrador, AppRol.Docente)]
    public class ImprovementPlansController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManagementImprovementPlan()
        {
            ManagementImprovementPlanViewModel model = new ManagementImprovementPlanViewModel();
            var IdEscuela = session.GetEscuelaId();
            model.fill(CargarDatosContext(), EscuelaId, ModalidadId);
            return View(model);
        }
        public ActionResult CrearPlanMejora(int Anio, string Nombre)
        {
            try
            {
                ManagementImprovementPlanViewModel model = new ManagementImprovementPlanViewModel();
                int statuscode;
                int respuesta = Context.Database.SqlQuery<int>("EXEC Usp_InsertAccionMejoraPlan {0}, {1}, {2}, {3}, {4}, {5}", Anio, Nombre, EscuelaId, "Abierto", SubModalidadPeriodoAcademicoId, ModalidadId).Single();         
                if(respuesta!=0)
                    statuscode = 200; // OK
                else
                    statuscode = 201; //Created

                model.fill(CargarDatosContext(), EscuelaId, ModalidadId);
                //PartialViewResult LstImprovementPlans = PartialView("_LstImprovementPlans", model);

                return Json(new { statuscode = statuscode,
                    estado = respuesta,
                    message = this.RenderPartialViewToString("_LstImprovementPlans", model)
                    //message = LstImprovementPlans
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR:" + ex.Message);
                //Response.StatusCode = 400; // error
                return Json(new { status = 400});
                //return new EmptyResult();
            }

        }
        public ActionResult CerrarPlanMejora(int IdPlanMejora)
        {
            try
            {
                var IdEscuela = session.GetEscuelaId();

                bool existe = Context.PlanMejora.Any(x => x.IdPlanMejora == IdPlanMejora);
                if (existe)
                {
                    PlanMejora pm = new PlanMejora();
                    pm = Context.PlanMejora.First(x => x.IdPlanMejora == IdPlanMejora);
                    pm.Estado = "Cerrado";
                    Context.SaveChanges();

                    return Json(new { success = true });
                }
                return Json(new { success = false });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR:" + ex.Message);
                return Json(new { success = false });
            }

        }
        public ActionResult EditImprovementPlan(int IdPlanMejora)
        {
            try
            {
                EditImprovementPlanViewModel ViewModel = new EditImprovementPlanViewModel();
                ViewModel.fill(CargarDatosContext(), ModalidadId, EscuelaId, IdPlanMejora);
                return View(ViewModel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR:" + ex.Message);
            }
            return new EmptyResult();
        }
        public ActionResult MantenimientoAccionesMejora()
        {
            try
            {
                var language = Session.GetCulture();
                MantenimientoAccionesMejoraViewModel ViewModel = new MantenimientoAccionesMejoraViewModel();
                ViewModel.fill(CargarDatosContext(), ModalidadId, EscuelaId);
                return View(ViewModel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR:" + ex.Message);
            }
            return new EmptyResult();
        }

        public JsonResult DeleteImprovementPlan(int IdPlanMejora)
        {
            var status = false;
            try
            {
                int respuesta = Context.Database.SqlQuery<int>("EXEC Usp_DeleteAccionMejoraPlan {0}", IdPlanMejora).Single();
                if (respuesta != 0)
                    status = true; // OK
                else
                    status = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR:" + ex.Message);
            }
            
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GuardarEstadoAM(int IdAccionMejora)
        {
            var status = new { status = false };
            try
            {
                var ParaPlan = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora).ParaPlan;

                var queryAM = Context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                if(ParaPlan == true)
                {
                    queryAM.ParaPlan = false;
                    queryAM.Estado = "NOIMP";
                }
                else
                {
                    queryAM.ParaPlan = true;
                    queryAM.Estado = "PENDI";
                }

                Context.SaveChanges();

                status = new { status = true };
            }
            catch (Exception ex)
            {
                status = new { status = false };
                Debug.WriteLine("ERROR:" + ex.Message);
            }


            return Json(status, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllComisionesForPeriodoAcademico(List<int> LstIdPeriodoAcademico, List<int> LstIdCarrera)
        {
            if (LstIdCarrera.Count() == 1 && LstIdCarrera.First() == 0)
            {
                var query = (from com in Context.Comision
                             join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                             join ac in Context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
                             join smp in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
                             join pa in Context.PeriodoAcademico on smp.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                             join sm in Context.SubModalidad on smp.IdSubModalidad equals sm.IdSubModalidad
                             join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                             join cco in Context.CarreraComision on com.IdComision equals cco.IdComision
                             join ca in Context.Carrera on cco.IdCarrera equals ca.IdCarrera
                             where
                             ca.IdEscuela == EscuelaId
                              && com.Codigo != "WASC"
                              && m.IdModalidad == ModalidadId
                              && LstIdPeriodoAcademico.Contains(pa.IdPeriodoAcademico)
                             select new SelectListItem { Value = com.IdComision.ToString(), Text = com.Codigo }
                            ).Distinct().OrderByDescending(x => x.Text).ToList();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = (from com in Context.Comision
                             join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                             join ac in Context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
                             join smp in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
                             join pa in Context.PeriodoAcademico on smp.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                             join sm in Context.SubModalidad on smp.IdSubModalidad equals sm.IdSubModalidad
                             join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                             join cco in Context.CarreraComision on com.IdComision equals cco.IdComision
                             join ca in Context.Carrera on cco.IdCarrera equals ca.IdCarrera
                             where
                             ca.IdEscuela == EscuelaId
                              && com.Codigo != "WASC"
                              && m.IdModalidad == ModalidadId
                              && LstIdPeriodoAcademico.Contains(pa.IdPeriodoAcademico)
                              && LstIdCarrera.Contains(ca.IdCarrera)
                             select new SelectListItem { Value = com.IdComision.ToString(), Text = com.Codigo }
                       ).Distinct().OrderByDescending(x => x.Text).ToList();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllOutcomesForComision(int IdComision, List<int> LstIdPeriodoAcademico)
        {

            if (IdComision != 0 && IdComision >= 0)
            {
                var data = (from oc in Context.OutcomeComision
                            join smpa in Context.SubModalidadPeriodoAcademico on oc.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                            join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                            join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                            join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                            join o in Context.Outcome on oc.IdOutcome equals o.IdOutcome
                            join c in Context.Comision on oc.IdComision equals c.IdComision
                            where LstIdPeriodoAcademico.Contains(pa.IdPeriodoAcademico)
                            && c.Codigo != "WASC"
                            && c.IdComision == IdComision
                            && m.IdModalidad == ModalidadId
                            select new SelectListItem { Value = o.IdOutcome.ToString(), Text = o.Nombre})
                       .Distinct()
                       .ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
 
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult _LstImprovementActionsByPlan(int IdAccionMejora)
        //{
        //    ManagementImprovementPlanViewModel model = new ManagementImprovementPlanViewModel();
        //    var IdEscuela = session.GetEscuelaId();
        //    model.FillAmByPlan(CargarDatosContext(), IdAccionMejora);
        //    return PartialView(model);
        //}


        //public ActionResult LstAccionesDelPlan(int IdPlanMejora, int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome)
        //{
        //    var model = new LstAccionesDelPlanViewModel();
        //    //SUBMODALIDAD=1
        //    model.fill(CargarDatosContext(), escuelaId, IdPlanMejora, IdConstituyente, IdInstrumento, IdCurso, IdOutcome,1);

        //    return View(model);
        //}

        //public ActionResult LstPlanesMejora(int? NumeroPagina, int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome)
        //{
        //    var model = new LstPlanesMejoraViewModel();
        //    //SUBMODALIDAD=1
        //    model.fill(CargarDatosContext(), escuelaId, NumeroPagina, IdConstituyente, IdInstrumento, IdCurso, IdOutcome,1);
        //    return View(model);
        //}

        //public ActionResult EliminarPlanMejora(Int32 IdPlanMejora)
        //{
        //    try
        //    {
        //        PlanMejora planmejora = context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == IdPlanMejora);

        //        planmejora.Estado = "INA";

        //        context.SaveChanges();

        //        PostMessage(MessageType.Success);

        //    }
        //    catch (DbEntityValidationException ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex);
        //        PostMessage(MessageType.Error, ex.ToString());
        //    }

        //    return RedirectToAction("LstPlanesMejora", "ImprovementPlans");
        //}

        //public ActionResult AddEditPlanMejora(int? IdPlanMejora)
        //{
        //    var viewModel = new AddEditPlanMejoraViewModel();
        //    int SubModalidadPeriodoAcademicoId = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;
        //    viewModel.CargarDatos(CargarDatosContext(), IdPlanMejora, SubModalidadPeriodoAcademicoId, escuelaId);
        //    return View(viewModel);
        //}

        //[HttpPost]
        //public ActionResult AddEditPlanMejora(AddEditPlanMejoraViewModel model)
        //{
        //    try
        //    {
        //        string ciclo = context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").CicloAcademico;

        //        PlanMejora planmejora = new PlanMejora();
        //        if (model.IdPlanMejora.HasValue)
        //            planmejora = context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == model.IdPlanMejora);
        //        planmejora.Nombre = model.Nombre;
        //        planmejora.IdEscuela = escuelaId;
        //        planmejora.Estado = "ACT";
        //        planmejora.Anio = model.Anio;

        //        if (model.IdPlanMejora.HasValue)
        //        {
        //            List<PlanMejoraAccion> lstpma = context.PlanMejoraAccion.Where(x => x.IdPlanMejora == model.IdPlanMejora).ToList();
        //            context.PlanMejoraAccion.RemoveRange(lstpma);
        //        }

        //        for (int i = 0; i < model.AccionesMejoraId.Count; i++)
        //        {
        //            PlanMejoraAccion pma = new PlanMejoraAccion();
        //            pma.PlanMejora = planmejora;
        //            pma.IdAccionMejora = model.AccionesMejoraId[i];

        //            context.PlanMejoraAccion.Add(pma);
        //        }


        //        context.SaveChanges();

        //        PostMessage(MessageType.Success);

        //    }
        //    catch (DbEntityValidationException ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex);
        //        PostMessage(MessageType.Error, ex.ToString());
        //    }
        //    return RedirectToAction("LstPlanesMejora", "ImprovementPlans");
        //}

        //public ActionResult PrintPlanDeMejora(int IdPlanMejora , int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome)
        //{
        //    var viewModel = new PrintPlanDeMejoraViewModel();
        //    viewModel.CargarDatos(CargarDatosContext(), IdPlanMejora, IdConstituyente, IdInstrumento, IdCurso, IdOutcome);

        //    Document document = new Document();
        //    document.SetMargins(75, 75, 40, 40);
        //    MemoryStream stream = new MemoryStream();
        //    try
        //    {
        //        PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
        //        pdfWriter.CloseStream = false;
        //        document.Open();
        //        document.Add(new Paragraph("\n\n\n\n\n"));

        //        var fontNormal = FontFactory.GetFont("Arial", 24, Font.BOLD, BaseColor.BLACK);
        //        Paragraph titulo = new Paragraph("Plan de Mejora", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        document.Add(new Paragraph("\n\n\n"));

        //        fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("Para la", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        string escuela = context.Escuela.FirstOrDefault(x => x.IdEscuela == escuelaId).Nombre;
        //        fontNormal = FontFactory.GetFont("Arial", 24, Font.BOLD, BaseColor.BLACK);
        //        titulo = new Paragraph(escuela, fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        document.Add(new Paragraph("\n\n"));

        //        fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("De la", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);


        //        fontNormal = FontFactory.GetFont("Arial", 22, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("Universidad Peruana de Ciencias Aplicadas", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        fontNormal = FontFactory.GetFont("Arial", 20, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("Lima - Peru", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);


        //        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
        //        imagen.BorderWidth = 0;
        //        imagen.Alignment = Element.ALIGN_CENTER;
        //        imagen.WidthPercentage = 30;
        //        document.Add(imagen);

        //        document.Add(new Paragraph("\n\n\n"));

        //        fontNormal = FontFactory.GetFont("Arial", 18, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph(DateTime.Now.Year.ToString(), fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        document.Add(new Paragraph("\n\n"));

        //        fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
        //        titulo = new Paragraph("CONFIDENCIAL", fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);

        //        document.Add(new Paragraph("\n\n"));

        //        fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.", fontNormal);
        //        titulo.Alignment = Element.ALIGN_JUSTIFIED;
        //        document.Add(titulo);


        //        document.NewPage();

        //        fontNormal = FontFactory.GetFont("Arial", 16, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph(viewModel.planmejora.Nombre.TrimEnd()+" "+viewModel.planmejora.Anio, fontNormal);
        //        titulo.Alignment = Element.ALIGN_CENTER;
        //        document.Add(titulo);


        //        fontNormal = FontFactory.GetFont("Arial", 11, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("\nSe muestran a continuación las acciones de mejora dentro de los siguientes parametros:", fontNormal);
        //        titulo.Alignment = Element.ALIGN_LEFT;
        //        document.Add(titulo);

        //        fontNormal = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
        //        titulo = new Paragraph("\n\nConstituyente: " +
        //            viewModel.getConstituyenteNombre() + "\nInstrumento: " + viewModel.getInstrumentoNombre() +
        //            "\nCurso: " + viewModel.getCursoNombre() + "\nOutcome: " + viewModel.getOutcomeNombre(), fontNormal);
        //        titulo.Alignment = Element.ALIGN_LEFT;
        //        document.Add(titulo);

        //        document.Add(new Paragraph("\n", fontNormal));

        //        List<Constituyente> LstConstituyente = viewModel.LstConstituyente;
        //        List<ConstituyenteInstrumento> lstconstituyenteinstrumento = context.ConstituyenteInstrumento.Where(x =>  viewModel.LstInstrumentoID.Contains(x.IdInstrumento)).ToList();
        //       // List<PlanMejoraAccion> lstplanmejoraaccion = context.PlanMejoraAccion.Where(x => x.AccionMejora.IdEscuela == escuelaId && x.AccionMejora.Estado != "INA").ToList();
        //       // List<AccionMejora> accionesdelplan = context.PlanMejoraAccion.Where(x => x.AccionMejora.IdEscuela == escuelaId && x.AccionMejora.Estado != "INA" && x.IdPlanMejora == IdPlanMejora).Select(x => x.AccionMejora).ToList();

        //        /*List<HallazgoAccionMejora> lsthallazgosAccionesMejora = (from am in accionesdelplan
        //                                                   join ham in context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
        //                                                   join h in context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
        //                                                   where ((IdCurso.HasValue == false || h.IdCurso == IdCurso)
        //                                                      && (IdOutcome.HasValue == false || h.IdOutcome == IdOutcome))
        //                                                   select ham).Distinct().ToList();*/


        //        List<HallazgoAccionMejora> lsthallazgosAccionesMejora = (from pam in context.PlanMejoraAccion
        //                     join am in context.AccionMejora on pam.IdAccionMejora equals am.IdAccionMejora
        //                     join ham in context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
        //                     join h in context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
        //                     join ci in context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
        //                     where (pam.IdPlanMejora == IdPlanMejora && am.Estado != "INA"
        //                        && (IdCurso.HasValue == false || h.IdCurso == IdCurso)
        //                        && (IdOutcome.HasValue == false || h.IdOutcome == IdOutcome))
        //                     select ham).Distinct().ToList();





        //        for (int i = 0; i < LstConstituyente.Count; i++)
        //        {

        //            int constituyenteID = LstConstituyente[i].IdConstituyente;
        //            //  List<Instrumento> lstinstrumentos = context.ConstituyenteInstrumento.Where(x => x.IdConstituyente == constituyenteID && viewModel.LstInstrumentoID.Contains(x.IdInstrumento)).Select(x => x.Instrumento).ToList();
        //            List<Instrumento> lstinstrumentos = lstconstituyenteinstrumento.Where(x => x.IdConstituyente == constituyenteID).Select(x => x.Instrumento).ToList();
        //            List<HallazgoAccionMejora> lsthamsXInstrumento = new List<HallazgoAccionMejora>();

        //            if(lsthallazgosAccionesMejora.Where(x=>lstinstrumentos.Select(y=>y.IdInstrumento).Contains(x.Hallazgo.IdInstrumento)).ToList().Count==0)
        //            {
        //                continue;
        //            }


        //            fontNormal = FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK);
        //            titulo = new Paragraph("\nConstituyente: " + LstConstituyente[i].NombreEspanol, fontNormal);
        //            titulo.IndentationLeft = 20;
        //            titulo.Alignment = Element.ALIGN_LEFT;
        //            document.Add(titulo);


        //          /*  List<HallazgoAccionMejora> lsthams = (from ham in lsthallazgosAccionesMejora
        //                                                  join h in context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
        //                                                  join ci in lstconstituyenteinstrumento on h.IdInstrumento equals ci.IdInstrumento
        //                                                  where ((ci.IdConstituyente == constituyenteID))
        //                                                  select ham).Distinct().ToList();*/



        //            for (int j = 0; j < lstinstrumentos.Count; j++)
        //            {
        //                int idinstrumento = lstinstrumentos[j].IdInstrumento;
        //                lsthamsXInstrumento = lsthallazgosAccionesMejora.Where(x => x.Hallazgo.IdInstrumento == idinstrumento).ToList();

        //                if (lsthamsXInstrumento.Count == 0)
        //                    continue;

        //                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
        //                titulo = new Paragraph("\n*Instrumento: " + lstinstrumentos[j].NombreEspanol, fontNormal);
        //                titulo.IndentationLeft = 30;
        //                titulo.Alignment = Element.ALIGN_LEFT;
        //                document.Add(titulo);

        //                document.Add(Chunk.NEWLINE);

        //                /*List<HallazgoAccionMejora> lsthams = (from h in context.Hallazgo
        //                                                      join ham in context.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
        //                                                      join am in accionesdelplan on ham.IdAccionMejora equals am.IdAccionMejora
        //                                                      where h.IdInstrumento == idinstrumento
        //                                                      select ham).Take(5).ToList();*/




        //                PdfPTable table2 = new PdfPTable(4);
        //                table2.WidthPercentage = 100;
        //                PdfPCell cell = new PdfPCell(new Phrase("Hallazgos", fontNormal));
        //                cell.Colspan = 2;
        //                cell.BackgroundColor = new BaseColor(184, 204, 228);
        //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        //                cell.PaddingBottom = 5;

        //                table2.AddCell(cell);

        //                cell.Phrase = new Phrase("Acciones de Mejora", fontNormal);
        //                table2.AddCell(cell);

        //                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.WHITE);
        //                PdfPCell cell2 = new PdfPCell(new Phrase("", fontNormal));
        //                cell2.HorizontalAlignment = Element.ALIGN_CENTER;
        //                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
        //                cell2.PaddingBottom = 5;
        //                cell2.Phrase = new Phrase("Código", fontNormal);
        //                cell2.BackgroundColor = new BaseColor(31, 73, 125);
        //                table2.AddCell(cell2);
        //                cell2.Phrase = new Phrase("Descripción", fontNormal);
        //                table2.AddCell(cell2);
        //                cell2.Phrase = new Phrase("Código", fontNormal);
        //                cell2.BackgroundColor = new BaseColor(31, 73, 125);
        //                table2.AddCell(cell2);
        //                cell2.Phrase = new Phrase("Descripción", fontNormal);
        //                table2.AddCell(cell2);

        //                fontNormal = FontFactory.GetFont("Arial", 11, Font.BOLD, BaseColor.BLACK);
        //                cell2.BackgroundColor = BaseColor.WHITE;

        //                for (int k = 0; k < lsthamsXInstrumento.Count; k++)
        //                {
        //                    cell2.Rowspan = 1;
        //                    Hallazgo h = lsthamsXInstrumento[k].Hallazgo;
        //                    cell2.Phrase = new Phrase(h.Codigo, fontNormal);
        //                    table2.AddCell(cell2);
        //                    cell2.Phrase = new Phrase(h.DescripcionEspanol, fontNormal);
        //                    table2.AddCell(cell2);



        //                    AccionMejora am = lsthamsXInstrumento[k].AccionMejora;

        //                    if (k > 0)
        //                    {
        //                        if (lsthamsXInstrumento[k - 1].IdAccionMejora == am.IdAccionMejora)
        //                            continue;
        //                    }

        //                    int amrowspan = lsthamsXInstrumento.Where(x => x.IdAccionMejora == am.IdAccionMejora).ToList().Count;



        //                    cell2.Rowspan = amrowspan;
        //                    cell2.Phrase = new Phrase(am.Codigo, fontNormal);
        //                    table2.AddCell(cell2);
        //                    cell2.Phrase = new Phrase(am.DescripcionEspanol, fontNormal);
        //                    table2.AddCell(cell2);

        //                }



        //                Paragraph p2 = new Paragraph();
        //                p2.IndentationLeft = 5;
        //                //p.IndentationRight = 5;
        //                table2.HorizontalAlignment = Element.ALIGN_LEFT;
        //                p2.Add(table2);
        //                document.Add(p2);



        //                document.Add(new Paragraph("\n", fontNormal));

        //                document.NewPage();

        //            }


        //        }

        //    }
        //    catch (DocumentException de)
        //    {
        //        Console.Error.WriteLine(de.Message);
        //    }
        //    catch (IOException ioe)
        //    {
        //        Console.Error.WriteLine(ioe.Message);
        //    }

        //    document.Close();

        //    stream.Flush(); //Always catches me out
        //    stream.Position = 0; //Not sure if this is required

        //    return File(stream, "application/pdf", "Plan de Mejora.pdf");
        //}
    }
}