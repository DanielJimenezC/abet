﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;

namespace UPC.CA.ABET.Presentation.Areas.Professor.Controllers
{
   

    public class JsonController : BaseController
    {
        // GET: Professor/Json
        /// <summary>
        /// Método para obtener las carreras que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <returns>Carreras que cumplan con los criterios ingresados, en formato Json</returns>
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetCarrerasInComision(string codigoComision)
        {
            var carreras = GedServices.GetCarrerasInComisionServices(Context, codigoComision, EscuelaId);
            var items = new List<SelectListItem>();
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            currentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();

            foreach (var c in carreras)
            {
                items.Add(new SelectListItem { Value = c.IdCarrera.ToString(), Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"));
        }

        public JsonResult TraduccionActualizar(string textoatraducir)
        {
            YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();

            string textotraducido = helper.Translate(textoatraducir);

            var data = new { Text = textotraducido };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeStatus(int IdAccionMejora, string Estado)
        {
            AccionMejora pa = Context.AccionMejora.First(x => x.IdAccionMejora == IdAccionMejora);
            bool success;
            if (pa != null)
            {
                pa.Estado = Estado;
                Context.SaveChanges();
                success = true;
            }
            else
            {
                success = false;
            }


            var data = new { success = success };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAcreditacionPeriodoAcademico(Int32 IdperiodoAcademico)
        {
            //Nuevo IdPeriodoAcademico
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdperiodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var data = Context.AcreditadoraPeriodoAcademico.Include(x => x.Acreditadora).Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).Select(x => new SelectListItem()
            {
                Text = x.Acreditadora.Nombre,
                Value = x.IdAcreditadora.ToString()
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetCarreraAcreditacion(Int32 IdAcreditadora)
        {

            int parModalidadId = Session.GetModalidadId();

            var data = (from co in Context.Comision
                        join acrepa in Context.AcreditadoraPeriodoAcademico on co.IdAcreditadoraPeriodoAcademico equals acrepa.IdAcreditadoraPreiodoAcademico
                        join caco in Context.CarreraComision on co.IdComision equals caco.IdComision
                        join ca in Context.Carrera on caco.IdCarrera equals ca.IdCarrera
                        join submodapa in Context.SubModalidadPeriodoAcademico on acrepa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                        join submoda in Context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                        where (acrepa.IdAcreditadora == IdAcreditadora && submoda.IdModalidad == parModalidadId)
                        select new
                        {
                            ca.IdCarrera,
                            ca.NombreEspanol
                        }

                ).Distinct().Select(x => new SelectListItem() { Text = x.NombreEspanol, Value = x.IdCarrera.ToString() }).ToList();


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarPlanAccionMejora(int Anio, List<int> function_param)
        {
            try
            {
                bool existe = Context.PlanMejora.Any(x => x.Anio == Anio);
                if (!existe)
                {
                    PlanMejora pm = new PlanMejora();
                    pm.Anio = Anio;
                    pm.Nombre = Anio.ToString();
                    pm.IdEscuela = session.GetEscuelaId();
                    Context.PlanMejora.Add(pm);
                    Context.SaveChanges();
                    var LastId = pm.IdPlanMejora;
                    foreach (var item in function_param)
                    {
                        PlanMejoraAccion pma = new PlanMejoraAccion();
                        pma.IdPlanMejora = LastId;
                        pma.IdAccionMejora = item.ToInteger();
                        Context.PlanMejoraAccion.Add(pma);
                        Context.SaveChanges();
                    }

                    return Json("Se Guardo PM");
                }
                else
                {
                    return Json("Plan Generado para " + Anio);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Json("Error");
            }

        }

        public JsonResult GetImprovementAction(int PeAcad)
        {
            //if (IdPlanMejora != 0)
            //{
            int ModalidadId = Session.GetModalidadId();
            int idEscuela = Session.GetEscuelaId();
            var subPeriodAcad = session.GetPeriodoAcademicoId().ToInteger();
            PeAcad=subPeriodAcad;
            var cicloAcad = (from a in Context.PeriodoAcademico
                             join b in Context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                             where b.SubModalidad.IdModalidad == ModalidadId && a.Estado == "ACT"
                             select a).FirstOrDefault();
            var anio = cicloAcad.CicloAcademico.Substring(0, 4);
            var tableData = Context.ListaAccionMejoraTableView2(ModalidadId, anio,idEscuela).ToList();
            var table =(from h in tableData
                        join a in Context.Hallazgos on h.idHallazgo equals a.IdHallazgo
                        select new
                        {  
                            idHallazgo = h.idHallazgo,                         
                            Codigo = h.Codigo + "-"+a.Identificador,
                            IdCriticidad = h.IdCriticidad,
                            Criticidad = h.NombreCriticidad,
                            Instrumento = h.Acronimo,
                            Outcome = (h.Outcomes == "" || h.Outcomes == null) ? "-" :  h.Outcomes,
                            Curso = h.Curso,
                            Descripcion = h.DescripcionHallazgo,                      
                            IdPeriodoAcademico = h.IdPeriodoAcademico,
                            AccionMejora= (h.DescripcionAccionmejora == "" || h.DescripcionAccionmejora == null) ? " No existe accion de mejora " : ((h.DescripcionAccionmejora.Count() < 75) ? h.DescripcionAccionmejora : string.Concat(h.DescripcionAccionmejora.Substring(0,75),"...")) ,
                            Area =(h.Area == "" || h.Area == null) ? "-": h.Area,
                            CantCaract = (h.DescripcionAccionmejora == "" || h.DescripcionAccionmejora == null) ? 0 : h.DescripcionAccionmejora.Count(),
                            Comision = (h.Comision == "" || h.Comision == null) ? "-" : h.Comision,
                            SinAm = (h.ParaPlan == null || h.ParaPlan == 0) ? 0: 1
                        }).Distinct().OrderBy(x => x.IdCriticidad);

            var data = new
                {
                    //status = (table.Any()) ? table.Select(x => x.EstadoPlanMejora).First() : "vacio",
                    table = table,
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            //}

        }

        public JsonResult GetOutcome(string Com,string PeriodoAca)
        {
            int PeriodoA = Convert.ToInt32(PeriodoAca);
            if (Com != "0")
            {
                var data = (from com in Context.Comision
                            join outcom in Context.OutcomeComision on com.IdComision equals outcom.IdComision
                            join outc in Context.Outcome on outcom.IdOutcome equals outc.IdOutcome
                            join smpa in Context.SubModalidadPeriodoAcademico on outcom.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                            join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                            where com.Codigo == Com && pa.IdPeriodoAcademico == PeriodoA
                            select new SelectListItem
                            {
                                Value = outcom.IdOutcomeComision.ToString(),
                                Text = outc.Nombre
                            }).Distinct().OrderBy(x => x.Text).ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = (from com in Context.Comision
                            join outcom in Context.OutcomeComision on com.IdComision equals outcom.IdComision
                            join outc in Context.Outcome on outcom.IdOutcome equals outc.IdOutcome
                            join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                            join acre in Context.Acreditadora on apa.IdAcreditadora equals acre.IdAcreditadora
                            where acre.Nombre == "ABET"
                            select new SelectListItem
                            {
                                Value = outcom.IdOutcomeComision.ToString(),
                                Text = outc.Nombre
                            }).Distinct().OrderBy(x => x.Text).ToList();

                return Json(data, JsonRequestBehavior.AllowGet);
            }


             
        }

        public JsonResult GetComision( string PeriodoAca)
        {
            int PeriodoA = Convert.ToInt32(PeriodoAca);
            

                var data = (from co in Context.Comision
                            join apa in Context.AcreditadoraPeriodoAcademico on co.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                            join acre in Context.Acreditadora on apa.IdAcreditadora equals acre.IdAcreditadora
                            join spa in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals spa.IdSubModalidadPeriodoAcademico
                            join pa in Context.PeriodoAcademico on spa.IdPeriodoAcademico equals pa.IdPeriodoAcademico                        
                            where acre.Nombre == "ABET" && pa.IdPeriodoAcademico == PeriodoA
                            select new SelectListItem
                            {
                                Value = co.Codigo,
                                Text = co.Codigo
                            }).Distinct().OrderBy(x => x.Text).ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            
            



        }


        public class ResultadoAM
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Constituyente { get; set; }
        public string Estado { get; set; }
        public int IdCurso { get; set; }
        public string Instrumento { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int PeriodoAcademico { get; set; }
        public string EstadoPlanMejora { get; set; }
        public string Outcomes { get; set; }
        public int Carreras { get; set; }
    }

        public JsonResult GetImprovementActions(int? IdPlanMejora)
        {
            if (IdPlanMejora != 0)
            {
                //var tableData = context.fn_AccionesPlanMejora2(IdPlanMejora, 1).ToList();
                //var table = (from h in tableData
                //             select new AccionMejoraFiltrado()).Distinct();
                var table = Context.Database.SqlQuery<AccionMejoraFiltrado>("Select * from dbo.fn_AccionesPlanMejora2(@p0, @p1)", IdPlanMejora, 1).ToList();
                var data = new
                {
                    //status = (table.Any()) ? table.Select(x => x).First() : "vacio",
                    table = table,
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var table = Context.Database.SqlQuery<AccionMejoraFiltrado>("Select * from dbo.fn_ListadoAccionesMejora(@p0)", 1).ToList();
                var data = new
                {
                    table = table,
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }

        }
     
        //[HttpPost]
        public JsonResult GetAccionesMejora(List<int> function_param)
        {
            if (function_param != null)
            {
                var data = (from h in Context.Hallazgo
                            join ci in Context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
                            join ham in Context.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                            join am in Context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                            select new
                            {
                                Id = am.IdAccionMejora,
                                Codigo = am.Codigo,
                                Descripcion = am.DescripcionEspanol,
                                Instrumento = h.Instrumento.NombreEspanol,
                                Constituyente = h.Constituyente.NombreEspanol
                                //Carrera = h.Carrera.NombreEspanol
                            }).Where(item => function_param.Contains(item.Id)).Distinct().ToList();
                return Json(data, JsonRequestBehavior.AllowGet);

            }
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisiones()
        {
            var data = Context.Comision.Where(x=> x.Codigo != "CAC-CC").Select(x => new SelectListItem()
            {
                
                Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL? x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC"
                : x.Codigo == "EAC" ? "Engineering" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "Computing" : "WASC",
                Value = x.Codigo
            }).Distinct().ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetListComision(Int32 IdAcreditadora, Int32 IdperiodoAcademico, Int32 IdCarrera)
        {
            //Nuevo
            int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdperiodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var data = Context.CarreraComision.Include(x => x.Comision).Where(x => x.IdCarrera == IdCarrera && x.Carrera.IdEscuela == EscuelaId && x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora).Select(x => new SelectListItem()
            {
                /*Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.Comision.Codigo == "EAC" ? "Ingeniería" : x.Comision.Codigo == "CAC" || x.Comision.Codigo == "CAC-CC" ? "Computación" : "WASC"
                : x.Comision.Codigo == "EAC" ? "Engineering" : x.Comision.Codigo == "CAC" || x.Comision.Codigo == "CAC-CC" ? "Computing" : "WASC",               */
                Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.Comision.NombreEspanol : x.Comision.NombreIngles
                ,
                Value = x.Comision.Codigo
            }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método para obtener los periodos académicos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <returns>Periodos académicos que cumplan con los criterios ingresados, en formato Json</returns>
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetPeriodoAcademicosInComisionAndCarrera(string codigoComision, int idCarrera)
        {
            var pacademicos = GedServices.GetPeriodoAcademicosInComisionCarreraServices(Context, codigoComision, idCarrera);
            var items = new List<SelectListItem>();

            foreach (var p in pacademicos)
            {
                items.Add(new SelectListItem { Value = p.IdPeriodoAcademico.ToString(), Text = p.CicloAcademico });
            }

            return Json(new SelectList(items, "Value", "Text"));
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllPeriodosAcademicosForModalidad(Int32 IdModalidad)
        {
            //var data = context.Database.SqlQuery<SelectListItem>("ListarComboModalidad {0}", currentCulture).AsEnumerable();
            var data = (from smpa in Context.SubModalidadPeriodoAcademico
                        join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                        join moda in Context.Modalidad on sm.IdModalidad equals moda.IdModalidad
                        join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (moda.IdModalidad == IdModalidad)
                        select new { Value = pa.IdPeriodoAcademico.ToString(), Text = pa.CicloAcademico }
                        ).Distinct().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //GetAllAcreditadoras
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllAcreditadoras()
        {
            var data = Context.Acreditadora.Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdAcreditadora.ToString()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllAcreditadorasForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllAcreditadorasForPeriodoAcademico(Int32 IdPeriodoAcademico)
        {
            //int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var data = Context.AcreditadoraPeriodoAcademico.Include(x => x.Acreditadora).Where(x => x.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico).Select(x => new SelectListItem()
            {
                Text = x.Acreditadora.Nombre,
                Value = x.Acreditadora.IdAcreditadora.ToString()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllCarrerasForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCarrerasForPeriodoAcademico(Int32 IdPeriodoAcademico)
        {
            //int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var data = Context.CarreraPeriodoAcademico.Include(x=>x.Carrera).Where(x=>x.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && x.Carrera.IdEscuela == EscuelaId).Select(x => new SelectListItem()            
            {
                Text = x.Carrera.NombreEspanol,
                Value = x.Carrera.IdCarrera.ToString()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllComisionesForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisionesForPeriodoAcademico(Int32 IdPeriodoAcademico)
        {

            //int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            //var data = context.Comision.Where(x=>x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && x.Codigo != "CAC-CC").Select(x => new SelectListItem()            
            //{
            //    Text = x.Codigo + " - " + x.NombreEspanol
            //    /*Text = x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC"*/,
            //    Value = x.Codigo
            //}).Distinct().ToList();

            var data = (from com in Context.Comision
                        join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                        join ac in Context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
                        join sm in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals sm.IdSubModalidadPeriodoAcademico
                        join cco in Context.CarreraComision on com.IdComision equals cco.IdComision
                        join ca in Context.Carrera on cco.IdCarrera equals ca.IdCarrera
                        where apa.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && ca.IdEscuela == EscuelaId
                        select new
                        {
                            Value = ac.Nombre,
                            Text = com.Codigo
                        }).Distinct().Select(x => new SelectListItem { Value = x.Value, Text = x.Text });
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Método para obtener todas las carreras existentes
        /// </summary>
        /// <returns>Todas las carreras existentes, en formato Json</returns>
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCarreras()
        {
            var data = Context.Carrera.Where(c => c.IdEscuela == EscuelaId).Select(x => new SelectListItem()
            {
                Text = x.NombreEspanol,
                Value = x.IdCarrera.ToString()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllCarrerasForAcreditadora
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCarrerasForAcreditadora(Int32 IdAcreditadora)
        {
            var data = Context.CarreraComision.Where(x=> x.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora==IdAcreditadora && x.Carrera.IdEscuela == EscuelaId)
                .Select(x => new SelectListItem()
            {
                Text = x.Carrera.NombreEspanol,
                Value = x.IdCarrera.ToString()
            }).Distinct().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllCarrerasForAcreditadoraForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCarrerasForAcreditadoraForPeriodoAcademico(Int32 IdAcreditadora, Int32 IdPeriodoAcademico)
        {
            //int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
         
            var data = Context.CarreraComision.Where(x => x.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora && x.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && x.Carrera.IdEscuela == EscuelaId)
                .Select(x => new SelectListItem()
                {
                    Text = x.Carrera.NombreEspanol,
                    Value = x.IdCarrera.ToString()
                }).Distinct().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllComisionesForAcreditadora
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisionesForAcreditadora(Int32 IdAcreditadora)
        {
            var data = Context.Comision.Where(x=>x.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora && x.Codigo != "CAC-CC").Select(x => new SelectListItem()
            {
                    Text=x.Codigo + " - " + x.NombreEspanol
                    /*Text = x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC"*/,
                    Value = x.Codigo
            }).Distinct().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllComisionesForAcreditadoraForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisionesForAcreditadoraForPeriodoAcademico(Int32 IdAcreditadora, Int32 IdPeriodoAcademico)
        {
            //int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            //var data = context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdAcreditadora == IdAcreditadora && x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && x.Codigo != "CAC-CC")
            //    .Select(x => new SelectListItem()
            //    {
            //        Text = x.Codigo + " - " + x.NombreEspanol
            //        /*Text = x.Codigo == "EAC" ? "INGENIERÍA" : x.Codigo == "CAC" || x.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC"*/,
            //        Value = x.Codigo
            //    }).Distinct().ToList();
            var data = (from com in Context.Comision
                        join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                        join ac in Context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
                        join sm in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals sm.IdSubModalidadPeriodoAcademico
                        join cco in Context.CarreraComision on com.IdComision equals cco.IdComision
                        join ca in Context.Carrera on cco.IdCarrera equals ca.IdCarrera
                        where apa.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && ac.IdAcreditadora == IdAcreditadora && ca.IdEscuela == EscuelaId
                        select new
                        {
                            Value = ac.Nombre,
                            Text = com.Codigo + "-" + com.NombreEspanol
                        }).Select(x => new SelectListItem { Value = x.Value, Text = x.Text }).Distinct();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllComisionesForCarrera
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisionesForCarrera(Int32 IdCarrera)
        {
            var data = Context.CarreraComision.Where(x => x.IdCarrera == IdCarrera && x.Comision.Codigo != "CAC-CC" && x.Carrera.IdEscuela == EscuelaId).Select(x => new SelectListItem()
            {
                Text = x.Comision.Codigo + " - " + x.Comision.NombreEspanol
                /*Text = x.Comision.Codigo == "EAC" ? "INGENIERÍA" : x.Comision.Codigo == "CAC" || x.Comision.Codigo == "CAC-CC" ? "COMPUTACIÓN" : "WASC"*/,
                Value = x.Comision.Codigo
            }).Distinct().ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //GetAllComisionesForCarreraForPeriodoAcademico
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllComisionesForCarreraForPeriodoAcademico(/*Int32 IdAcreditadora,*/Int32 IdCarrera, Int32 IdPeriodoAcademico)
        {

       
            int parModalidadId = Session.GetModalidadId();

            int parIdAcreditadora = 0;
            if (parIdAcreditadora != 0)
            {
                var data = (from co in Context.Comision
                            join acrepa in Context.AcreditadoraPeriodoAcademico on co.IdAcreditadoraPeriodoAcademico equals acrepa.IdAcreditadoraPreiodoAcademico
                            join caco in Context.CarreraComision on co.IdComision equals caco.IdComision
                            join ca in Context.Carrera on caco.IdCarrera equals ca.IdCarrera
                            join submodapa in Context.SubModalidadPeriodoAcademico on acrepa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join submoda in Context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                            where (acrepa.IdAcreditadora == parIdAcreditadora && submoda.IdModalidad == parModalidadId && ca.IdCarrera == IdCarrera && acrepa.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico)
                            select new
                            {
                                co.Codigo,
                                co.NombreEspanol,

                            }
                    ).Distinct().Select(x => new SelectListItem() { Text = x.Codigo + " - " + x.NombreEspanol, Value = x.Codigo }).ToList();

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = Context.CarreraComision.Where(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico && x.Carrera.IdEscuela == EscuelaId).Select(x => new SelectListItem()
                {
                    Text = x.Comision.Codigo + " - " + x.Comision.NombreEspanol     ,             
                    Value = x.Comision.Codigo
                }).Distinct().ToList();

                return Json(data, JsonRequestBehavior.AllowGet);
            }
          
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetDocentes(String cadenaBuscar)
        {
            var query = Context.Docente.AsQueryable();
            if (!String.IsNullOrEmpty(cadenaBuscar))
                foreach (var token in cadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));

            var data = query.ToList().Select(x => new { id = x.IdDocente, text = x.Codigo });
            return Json(data);
        }

        //Little
        public JsonResult GetComisionPorCarrera(int? IdCarrera)
        {
            if (IdCarrera.HasValue == false)
                IdCarrera = 0;
            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.
                FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.NombreEspanol=="Regular").IdSubModalidadPeriodoAcademico;


            var data2 = (from cc in Context.CarreraComision 
                         join c in Context.Comision on cc.IdComision equals c.IdComision
                         where (cc.IdCarrera == IdCarrera && cc.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico)
                         select new { c.IdComision, c.Codigo });

            var data = data2.Distinct().ToList();

            return Json(data);

        }
        public JsonResult GetCursoPorCarrera(int? IdCarrera) {
            if (IdCarrera.HasValue == false)
                IdCarrera = 0;
            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.
                FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.NombreEspanol == "Regular").IdSubModalidadPeriodoAcademico;
            var data = (from cur in Context.Curso
                        join h in Context.Hallazgo on cur.IdCurso equals h.IdCurso
                        where h.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                        && h.IdCarrera == IdCarrera
                        select new {cur.IdCurso , cur.NombreEspanol}).Distinct().ToList();

            return Json(data);
        }
        public JsonResult GetInsturmentoConstituyente(int? IdConstituyente, int? IdCarrera) {
            if (IdConstituyente.HasValue == false)
                IdConstituyente = 0;

            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.
                FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.NombreEspanol == "Regular").IdSubModalidadPeriodoAcademico;

            var data = (from h in Context.Hallazgo
                        join ins in Context.Instrumento on h.IdInstrumento equals ins.IdInstrumento
                        join cons in Context.Constituyente on h.IdConstituyente equals cons.IdConstituyente
                        where cons.IdConstituyente == IdConstituyente && h.IdCarrera == IdCarrera && h.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                        select new { ins.IdInstrumento, ins.Acronimo }).Distinct().OrderBy(x=>x.Acronimo).ToList();
            return Json(data);
        }
        public JsonResult GetOutcomeCurso(int? IdCurso, int? IdCarrera) {
            if (!IdCurso.HasValue)
            {
                IdCurso = 0;
            }
            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.
                FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.NombreEspanol == "Regular").IdSubModalidadPeriodoAcademico;
            var data = (from cur in Context.Curso 
                        join h in Context.Hallazgo on cur.IdCurso equals h.IdCurso
                        join oc in Context.Outcome  on h.IdOutcome equals oc.IdOutcome                       
                        where h.IdCarrera == IdCarrera && h.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && cur.IdCurso == IdCurso
                        select new { oc.Nombre }).Distinct().ToList() ;
            return Json(data);
        }

        public JsonResult GetInstrumentos(int? IdConstituyente)
        {
            var data = Context.Instrumento.Select(x=> new { IdInstrumento = x.IdInstrumento, NombreEspanol = x.Acronimo }).ToList();

            if (IdConstituyente.HasValue == false)
            { 
                IdConstituyente = 0;
            }
            else
            { 

            var data2 = (from ci in Context.ConstituyenteInstrumento
                         where ci.IdConstituyente == IdConstituyente
                         select ci.Instrumento).Distinct().ToList();

             data = (from i in data2
                        select new { IdInstrumento = i.IdInstrumento, NombreEspanol = i.Acronimo }).ToList();
            }
            return Json(data);
        }

        public JsonResult GetOutcomesPorCurso(int? IdCurso)
        {
            if (IdCurso.HasValue == false)
                IdCurso = 0;
            int? idSubModalidadPeriodoAcademico = session.GetSubModalidadPeriodoAcademicoId();
            var data = (from mc in Context.MallaCocos
                         join mcd in Context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                         join oc in Context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                         join o in Context.Outcome on oc.IdOutcome equals o.IdOutcome
                         join c in Context.Comision on oc.IdComision equals c.IdComision
                         join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                         where (cmc.IdCurso == IdCurso 
                         && mc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                         )
                         select new { o.Nombre, NombreEspanol = c.Codigo + " | " + o.Nombre }).Distinct().ToList();

            return Json(data);
        }

        public ActionResult GetHallazgosAccionDeMejora(int? IdConstituyente, int? IdInstrumento, int? IdCurso, int? IdOutcome, int? IdCriticidad, int? Solo)
        {
            List<int> hallazgosid = new List<int>();

            if (Solo != 0)
                hallazgosid = Context.HallazgoAccionMejora.Select(x => x.IdHallazgo).ToList();

            var data = (from h in Context.Hallazgos
                        join ci in Context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                        //join oc in context.OutcomeComision on h.IdOutcomeComision equals oc.IdOutcomeComision
                        //join cc in context.CarreraComision on oc.IdComision equals cc.IdComision
                        //join ca in context.Carrera on cc.IdCarrera equals ca.IdCarrera
                        //where (IdConstituyente == 0 || ci.IdConstituyente == IdConstituyente) && h.Estado != "INA" && ca.IdEscuela== escuelaId
                        //&& (IdInstrumento == 0 || ci.IdInstrumento == IdInstrumento)
                        //&& (IdCriticidad == 0 || h.IdCriticidad == IdCriticidad)
                        //&& (IdCurso == 0 || h.IdCurso == IdCurso)
                        //&& (IdOutcome == 0 || oc.IdOutcome == IdOutcome)
                        //&& (Solo == 0 || hallazgosid.Contains(h.IdHallazgo) == false)                     
                        select new { IdHallazgo = h.IdHallazgo.ToString(), NombreEspanol = h.Codigo + " : " + h.DescripcionEspanol, IdCriticidad= h.IdCriticidad }
                        ).OrderBy(x=>x.IdCriticidad).ToList();

            //for(int i =1; i<data.Count; i++)
            //{
            //    if (i == 0)
            //        i = 1;

            //   if( data[i].NombreEspanol == data[i-1].NombreEspanol)
            //    {
            //        data.RemoveAt(i);
            //        i = i - 1;
            //    }
            //}

            // var data = data2.Distinct().ToList();

            //System.Diagnostics.Debug.WriteLine("" + data.Count);

            return Json(data);
        }

        public ActionResult GetAccionesDeMejoraPlan(int IdConstituyente, int IdInstrumento, int IdCurso, int IdOutcome)
        {

            var data = (from h in Context.Hallazgo
                        join ci in Context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
                        join ham in Context.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                        join am in Context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                        where (IdConstituyente == 0 || ci.IdConstituyente == IdConstituyente) && am.IdEscuela == EscuelaId && h.Estado !="INA" && am.Estado!="INA"
                        && (IdInstrumento == 0 || h.IdInstrumento == IdInstrumento)
                        && (IdCurso == 0 || h.IdCurso == IdCurso)
                        && (IdOutcome == 0 || h.IdOutcome == IdOutcome)
                        select new {IdAccionMejora = am.IdAccionMejora.ToString(), NombreEspanol = am.Codigo+" : "+am.DescripcionEspanol }
                        ).Distinct().ToList();
            //select new { IdHallazgo = h.IdHallazgo.ToString(), NombreEspanol = h.Codigo + " : " + h.DescripcionEspanol }
          

            System.Diagnostics.Debug.WriteLine("" + data.Count);

            return Json(data);
        }
    }
}