﻿
$(document).ready(function () {  
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });          
    $('#tableIFC thead tr th').each( function() {//////IFC
        var nTds = $(this);
        
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    $('#tableARD thead tr th').each( function() {/////////ARD
        var nTds = $(this);
      
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    $('#tableRV thead tr th').each( function() {/////////RV
        var nTds = $(this);
        
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    $('#tableRC thead tr th').each( function() {/////////RC
        var nTds = $(this);
        
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    $('#tableGRA thead tr th').each( function() {/////////GRA
        var nTds = $(this);
        
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    $('#tablePPP thead tr th').each( function() {/////////PPP
        var nTds = $(this);
        
        var sBrowser = $(nTds).text();
        this.setAttribute( 'title', text(sBrowser) );
    } );
    /////////////////////////////////////////////////////////////////////////ADD EL TOOLTIP
    $('#tableIFC thead th[title]').tooltip(
    {
        "container": 'body'
    });
    
    $('#tableARD thead th[title]').tooltip(
    {
        "container": 'body'
    });
    $('#tableRV thead th[title]').tooltip(
    {
        "container": 'body'
    });
    
    $('#tableRC thead th[title]').tooltip(
    {
        "container": 'body'
    });
    $('#tableGRA thead th[title]').tooltip(
    {
        "container": 'body'
    });
    
    $('#tablePPP thead th[title]').tooltip(
    {
        "container": 'body'
    });
    $('#TapIngles').on("click", function () {
        $('#ComentarioIngles').val("");
        ActualizarTraduccion();
    });
    function text(sBrowser) {
        var sTitle;
        if (sBrowser == " ")
            sTitle = 'Seleccionar un hallazgo para relaccionarlo a una Acción de mejora';
        else if (sBrowser == "Código")
            sTitle = 'El Código del Hallazgo';
        else if (sBrowser == "Criticidad")
            sTitle = 'Nivel de Criticidad:' + '-Normal' + '-Crítico';//
        else if (sBrowser == "Curso")
            sTitle = 'El curso a la que pertenece el Hallazgo';//
        else if (sBrowser == "Área")
            sTitle = 'La área a la que pertenece el Hallazgo';//
        else if (sBrowser == "Comisión")
            sTitle = 'La Comisión a la que pertenece un Hallazgo';//
        else if (sBrowser == "Editar")
            sTitle = 'Se puede Editar la descripción y la criticidad del Hallazgo';//
        else if (sBrowser == "Descripción Hallazgo")
            sTitle = 'Descripción del Hallazgo con el que fue Creado';//
        else if (sBrowser == "Acción Mejora Desde IFC")
            sTitle = 'La Descripción de una Acción de mejora Generado en el Módulo de IFC';//
        return sTitle;
    }
});
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}
function CargarTabla(url,idModalidad) {
    var dataTable = getDataTableAM(url);
    var headersTable = [
        { "title": " ", "data": "", "data-toggle": "tooltip" },//0
        { "title": "IdHallazgo", "data": "idHallazgo" },//1
        { "title": "Código", "data": "Codigo", "class": "col-md-2" },//2
        { "title": "IdCriticidad", "data": "IdCriticidad" },//3
        { "title": "Criticidad", "data": "Criticidad" },//4
        { "title": "Instrumento", "data": "Instrumento" },//5
        { "title": "Outcome", "data": "Outcome" },   //6
        { "title": "Curso", "data": "Curso", "class": "col-md-2" },//7
        { "title": "Descripción Hallazgo", "data": "Descripcion", "class": "col-md-4" },//8
        { "title": "Acción Mejora Desde IFC", "data": "AccionMejora", "class": "col-md-4" },//9
        { "title": "IdPeriodo", "data": "IdPeriodoAcademico" }, //10
        { "title": "AccionMejora", "data": "AccionMejora" }, //11
        { "title": "Área", "data": "Area" }, //12
        { "title": "Comisión", "data": "Comision", "class": "col-md-2" }, //13
        { "title": "Observaciones   ", "data": "" }, //14
        { "title": "Editar", "data": "IdHallazgo" }, //15
        { "title": "Caracteres", "data": "CantCaract" }, //16
        { "title": "SinAm", "data": "SinAm" }, //17

    ];
    var tableOptionsIFC = {
        "data": dataTable,
        "columns": headersTable,
        "columnDefs": [
            {
                "targets": [1, 5, 10, 11, 3, 16, 17, 6, 13], "visible": false,
            },
            {
                "targets": [0], "visible": true,
                'render':
                   function (data, type, row) {

                       return '<input type="checkbox" data-size="small" data-onstyle="success" class="toggle-btn" id="' + row["idHallazgo"] + '" value="' + row["idHallazgo"] + '"/>';

                   }
            },
            {
                "targets": [15], "visible": true,
                'render':
                   function (data, type, row) {
                       return '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'AddRecord("' + row["idHallazgo"] + '")\'> <i class="fa fa-edit" hidden></i></a> </button>'
                   } //ButtonsOptionsResource.Editar
            },
            {
                "targets": [14], "visible": false,
                'render':
                   function (data, type, row) {
                       return '<p> <strong>Código:</strong>' + row["Código"] + '</p><p><strong>Comision: </strong>' + row["Comision"] + '</p>'
                   }
            },
            {
                "targets": [9], "visible": true,
                'render':
                   function (data, type, row) {
                       if (row["CantCaract"] < 75)
                           return '<p>' + row["AccionMejora"] + '</p>'
                       else
                           return row["AccionMejora"] + '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'ViewDetalAM("' + row["idHallazgo"] + '")\'>Ver mas </button>'
                   }
            }
        ],
        "search": {
            "regex": true
        }

    };
    var tableOptionsRD = {
        "data": dataTable,
        "columns": headersTable,
        "columnDefs": [
            {
                "targets": [1, 5, 10, 11, 3, 16, 9, 6, 13, 17], "visible": false,
            },
            {
                "targets": [0], "visible": true,
                'render':
                   function (data, type, row) {

                       return '<input type="checkbox" data-size="small" data-onstyle="success" class="toggle-btn" id="' + row["idHallazgo"] + '" value="' + row["idHallazgo"] + '"/>';

                   }
            },
            {
                "targets": [15], "visible": true,
                'render':
                   function (data, type, row) {
                       return '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'AddRecord("' + row["idHallazgo"] + '")\'> <i class="fa fa-edit" hidden></i></a> </button>'
                   } //ButtonsOptionsResource.Editar
            },
            {
                "targets": [14], "visible": false,
                'render':
                   function (data, type, row) {
                       return '<p> <strong>Código:</strong>' + row["Código"] + '</p><p><strong>Comision: </strong>' + row["Comision"] + '</p>'
                   }
            },
            {
                "targets": [9], "visible": true,
                'render':
                   function (data, type, row) {
                       if (row["CantCaract"] < 75)
                           return '<p>' + row["AccionMejora"] + '</p>'
                       else
                           return row["AccionMejora"] + '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'ViewDetalAM("' + row["idHallazgo"] + '")\'>Ver mas </button>'
                   }
            }
        ],
        "search": {
            "regex": true
        }

    };
    var tableOptionsRVRC = {
        "data": dataTable,
        "columns": headersTable,
        "columnDefs": [
            {
                "targets": [1, 5, 10, 11, 3, 16, 17, 9, 6, 15], "visible": false,
            },
            {
                "targets": [0], "visible": true,
                'render':
                   function (data, type, row) {

                       return '<input type="checkbox" data-size="small" data-onstyle="success" class="toggle-btn" id="' + row["idHallazgo"] + '" value="' + row["idHallazgo"] + '"/>';

                   }
            },
            {
                "targets": [15], "visible": true,
                'render':
                   function (data, type, row) {
                       return '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'AddRecord("' + row["idHallazgo"] + '")\'> <i class="fa fa-edit" hidden></i></a> </button>'
                   } //ButtonsOptionsResource.Editar
            },
            {
                "targets": [14], "visible": false,
                'render':
                   function (data, type, row) {
                       return '<p> <strong>Código:</strong>' + row["Código"] + '</p><p><strong>Comision: </strong>' + row["Comision"] + '</p>'
                   }
            },
            {
                "targets": [9], "visible": true,
                'render':
                   function (data, type, row) {
                       if (row["CantCaract"] < 75)
                           return '<p>' + row["AccionMejora"] + '</p>'
                       else
                           return row["AccionMejora"] + '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'ViewDetalAM("' + row["idHallazgo"] + '")\'>Ver mas </button>'
                   }
            }
        ],
        "search": {
            "regex": true
        }

    };
    var tableOptionsGRAPPP = {
        "data": dataTable,
        "columns": headersTable,
        "columnDefs": [
            {
                "targets": [1, 5, 10, 11, 3, 16, 17, 7, 9, 12, 6, 15], "visible": false,
            },
            {
                "targets": [0], "visible": true,
                'render':
                   function (data, type, row) {

                       return '<input type="checkbox" data-size="small" data-onstyle="success" class="toggle-btn" id="' + row["idHallazgo"] + '" value="' + row["idHallazgo"] + '"/>';

                   }
            },
            {
                "targets": [15], "visible": true,
                'render':
                   function (data, type, row) {
                       return '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'AddRecord("' + row["idHallazgo"] + '")\'> <i class="fa fa-edit" hidden></i></a> </button>'
                   } //ButtonsOptionsResource.Editar
            },
            {
                "targets": [14], "visible": false,
                'render':
                   function (data, type, row) {
                       return '<p> <strong>Código:</strong>' + row["Código"] + '</p><p><strong>Comision: </strong>' + row["Comision"] + '</p>'
                   }
            },
            {
                "targets": [9], "visible": true,
                'render':
                   function (data, type, row) {
                       if (row["CantCaract"] < 75)
                           return '<p>' + row["AccionMejora"] + '</p>'
                       else
                           return row["AccionMejora"] + '<button type="button" class="btn btn-link" id = "btnEditLink" onclick= \'ViewDetalAM("' + row["idHallazgo"] + '")\'>Ver mas </button>'
                   }
            }
        ],
        "search": {
            "regex": true
        }

    };


    var language = '@Session.GetCulture()';
    if (language === '@ConstantHelpers.CULTURE.ESPANOL') {
        tableOptionsIFC.language = { url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" };
        tableOptionsRD.language = { url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" };
        tableOptionsRVRC.language = { url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" };
        tableOptionsGRAPPP.language = { url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" };

    }

    arrayTable[0] = $('#tableIFC').DataTable(tableOptionsIFC).columns(5).search('IFC').draw();//SI
    arrayTable[1] = $('#tableARD').DataTable(tableOptionsRD).columns(5).search('ARD').draw();//si
    arrayTable[2] = $('#tableRV').DataTable(tableOptionsRVRC).columns(5).search('RV').draw();
    arrayTable[3] = $('#tableRC').DataTable(tableOptionsRVRC).columns(5).search('RC').draw();
    arrayTable[4] = $('#tableGRA').DataTable(tableOptionsGRAPPP).columns(5).search('GRA').draw();

    //console.log(idModalidad);
    if (idModalidad == 1) {
        arrayTable[5] = $('#tablePPP').DataTable(tableOptionsGRAPPP).columns(5).search('PPP').draw();
    }
}
function filtrarTabla() {
    
    arrayTable.forEach(function (value, i) {
        ////console.log('Indice: ' + i)
        var columns = arrayTable[i].settings().init().columns;
        arrayTable[i].columns().every(function (index) {
            mapBusqueda.forEach(function (value, key) {
                if (columns[index].title == key) {
                    //console.log('Filtro busqueda por columna: ' + key + ' y con valor ' + value);
                    arrayTable[i].columns(index).search(value, true, false).draw();
                }
                else {
                    ////console.log('No hay filtro');
                }
            });
        });
    });
}
function limpiarCampos(IdAreasValoresIniciales, IdPeriodoAcademico) {
    console.log(IdAreasValoresIniciales)
    var $item1 = $("#periodoacademico").select2();
    $item1.val(IdPeriodoAcademico).trigger("change");
    var $item2 = $("#Area").select2();
    $item2.val(IdAreasValoresIniciales).trigger("change");
    var $item3 = $("#Comision");
    $item3.val(["0"]).trigger("change");
    var $item4 = $("#OC");
    $item4.val(["0"]).trigger("change");
}
function filtrarMultiople(){
    var mySelect = $('select[name="Area[]"]');
    if (mySelect.val()  != null) {
        if (mySelect.val().length > 1) {
            var regex = "";
            var array = mySelect.val();
            //console.log(array);
            array.forEach(function (value, i) {
                //console.log(value)
                if (i == 0)
                    regex = regex.concat("(?=.*" + value + ")");
                else
                    regex = regex.concat("|" + "(?=.*" + value + ")");
            })
            //console.log("El valor de regx es:" + regex)
            mapBusqueda.set('Área', regex)
        }
        else {
            mapBusqueda.set('Área', mySelect.val());
        }
    } else
        mapBusqueda.set('Área', "");           
}
function actChecks(ArrayChecked) {
    
    for (var i = 0; i < ArrayChecked.length; i += 1) {
        try {
            document.getElementById(ArrayChecked[i]).checked = true;
        }
        catch (error) {
            console.error(error);
        }
    }
}
function QuitChecks(ArrayChecked) {
   
    for (var i = 0; i < ArrayChecked.length; i += 1) {      
        if (ArrayChecked[i] != null)
        {
            console.log(ArrayChecked[i])
            document.getElementById(ArrayChecked[i]).checked = false;
        }
    }
}
function ActualizarTraduccion(url) {
    var comentario = $('#ComentarioEspañol').val();
    //console.log(comentario)
    $.ajax({
        //  type: "POST",
        url: url,
        data: { textoatraducir: comentario },
        dataType: "json",
        success: function (data) {
            $('#ComentarioIngles').show();
            $('#ComentarioIngles').val(data["Text"]);
            $("#btnGenerar").css("pointer-events", "visible");
            $("#btnGenerar").disabled = true;
        },
        beforeSend: function (xhr, opts) {
            if (comentario === "") {
                xhr.abort();
            }
            $('#ComentarioIngles').val('@AddEditImprovementActionResource.Translating');
            $("#btnGenerar").css("pointer-events", "none");
            $("#btnGenerar").disabled = false;
        }
    });
}
function getDataTableAM(url) {
    var item = document.getElementById('PeriodoAcademico')[0];
    var result = null;
    $.ajax({
        url: url,
        type: 'GET',
        data: { PeAcad: parseInt(item.value) },
        async: false,
        dataType: "json",
        success: function (data) {
            //wacky nested anonymous callbacks go here
            result = data.table;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // Empty most of the time...
        }, beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        }
    });

    return result;
}

