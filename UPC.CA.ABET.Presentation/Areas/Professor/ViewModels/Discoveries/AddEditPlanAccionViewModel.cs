﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class AddEditPlanAccionViewModel
    {
        public Int32? PlanAccionId { get; set; }
        public Int32? CarreraId { get; set; }
        public Int32 Anio { get; set; }
        public List<PlanAccion> LstPlanAccion { get; set; }
        public IEnumerable<SelectListItem> AvailableAnios { get; set; }
        public IEnumerable<SelectListItem> AvailableCarreras { get; set; }

        public void fill(CargarDatosContext datacontext)
        {
            var context = datacontext.context;
            var Session = datacontext.session;
            AvailableCarreras = context.Carrera.Select(o => new SelectListItem { Value = o.IdCarrera.ToString(), Text = o.NombreEspanol });
            var periodosExistentes = context.PlanAccion.Select(x => x.Anio).Distinct().ToList();
            var periodoAcademicos = context.PeriodoAcademico.ToList().Select(X => X.CicloAcademico.Substring(0, 4).ToInteger()).Distinct().ToList();
            periodoAcademicos.Add(DateTime.Now.Year + 1);
            periodoAcademicos = periodoAcademicos.Distinct().ToList();
            //var periodosFin = periodoAcademicos.Where(x => !periodosExistentes.Any(Y => Y == x)).ToList();
            //AvailableAnios = periodosFin.Select(X => new SelectListItem { Value = X.ToString(), Text = X.ToString() }).ToList();
            AvailableAnios = periodoAcademicos.Select(X => new SelectListItem { Value = X.ToString(), Text = X.ToString() }).ToList();

            if (PlanAccionId.HasValue)
            {
                var planAccion = context.PlanAccion.FirstOrDefault(X=>X.IdPlanAccion == PlanAccionId.Value);
                CarreraId = planAccion.IdCarrera;
                Anio = planAccion.Anio;
               
            }


        }
    }
}