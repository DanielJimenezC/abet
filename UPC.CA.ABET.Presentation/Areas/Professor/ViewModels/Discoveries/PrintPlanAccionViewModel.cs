﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class PrintPlanAccionViewModel
    {
        public Int32? PlanAccionId { get; set; }
        public String Anio { get; set; }
        public String Carrera { get; set; }
        public Int32? ComisionId { get; set; }
        public IEnumerable<SelectListItem> AvailableComisiones { get; set; }
        public Boolean? esModelPDF { get; set; }

        public void CargarDatos(CargarDatosContext dataContext,int IdSubModalidad)
        {
            //LUEGO MODIFICAR
            IdSubModalidad = 1;
            var ctx = dataContext.context;
            var session = dataContext.session;
            var culture = dataContext.currentCulture;
            esModelPDF = false;
            if (PlanAccionId.HasValue)
            {
                var planAccion = ctx.PlanAccion.FirstOrDefault(x => x.IdPlanAccion == PlanAccionId);
                Anio = planAccion.Anio.ToString();
                String cicloUltimo = (planAccion.Anio - 1).ToString();
                //var periodoAcademico = ctx.PeriodoAcademico.FirstOrDefault(x => x.CicloAcademico.Contains(cicloUltimo));
                var  submodalidadperiodoAcademico = ctx.SubModalidadPeriodoAcademico.FirstOrDefault(x=>x.PeriodoAcademico.CicloAcademico.Contains(cicloUltimo) && x.IdSubModalidad==IdSubModalidad);

                var carrera = ctx.Carrera.FirstOrDefault(x => x.IdCarrera == planAccion.IdCarrera);                
                this.Carrera = culture == ConstantHelpers.CULTURE.ESPANOL ? carrera.NombreEspanol : carrera.NombreIngles;
                AvailableComisiones = ctx.CarreraComision.Where(x => x.IdCarrera == carrera.IdCarrera && x.IdSubModalidadPeriodoAcademico == submodalidadperiodoAcademico.IdSubModalidadPeriodoAcademico).Select(y => y.Comision)
                    .Select(o => new SelectListItem { Value = o.IdComision.ToString(), Text = culture == ConstantHelpers.CULTURE.ESPANOL ? o.NombreEspanol : o.NombreIngles});
                
            }
        }

    }
}