﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class EliminarAccionMejoraViewModel
    {
        public int AccionMejoraId { get; set; }

        public int InstrumentoId { get; set; }

        public bool PuedeEliminar { get; set; }

        public string LanguageCulture { get; set; }
    }
}