﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class ViewDiscoveryViewModel
    {
     //   public Hallazgo Discovery { get; set; }                                                                                                                       // RML001
        public Hallazgos Discovery { get; set; }
        public List<AccionMejora> LstAccionMejora { get; set; }
        public Int32 ResultadoId { get; set; }
        public Int32 InstrumentoId { get; set; }
        public String NombreInstrumento { get; set; }

        public void fill(CargarDatosContext datacontext , Int32 HallazgoId)
        {
            //  Discovery = datacontext.context.Hallazgo.FirstOrDefault(X => X.IdHallazgo == HallazgoId);                                                             // RML001
            Discovery = datacontext.context.Hallazgos.FirstOrDefault(X => X.IdHallazgo == HallazgoId);
            Discovery.Codigo = Discovery.Codigo + '-' + Discovery.Identificador.ToString();
            LstAccionMejora = datacontext.context.AccionMejora.Where(X => X.HallazgoAccionMejora.Any(Y => Y.IdHallazgo == HallazgoId)).ToList();
            //    InstrumentoId = Discovery.IdInstrumento;                                                                                                            // RML001
            InstrumentoId = Discovery.ConstituyenteInstrumento.IdInstrumento;
            ResultadoId = 1;
            // NombreInstrumento = Discovery.Instrumento.NombreEspanol;                                                                                               // RML001
            NombreInstrumento = Discovery.ConstituyenteInstrumento.Instrumento.NombreEspanol;
        }
    }
}