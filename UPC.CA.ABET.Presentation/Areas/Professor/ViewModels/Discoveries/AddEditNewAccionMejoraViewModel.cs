﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class AddEditNewAccionMejoraViewModel
    {
        public Int32? IdAccionMejora { get; set; }
        public AccionMejora accionmejora { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public string DescripcionHallazgo { get; set; }

        public List<Constituyente> LstConstituyentes { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Curso> LstCursos { get; set; }
        public Int32? IdConstituyente { get; set; }
        public Int32? IdInstrumento { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdOutcome { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public IEnumerable<SelectListItem> LstHallazgos { get; set; }
        public List<Int32> HallazgoId { get; set; }

        public AddEditNewAccionMejoraViewModel()
        {
            Codigo = "";
            LstConstituyentes = new List<Constituyente>();
            LstCursos = new List<Curso>();
            LstInstrumentos = new List<Instrumento>();
            LstOutcome = new List<Outcome>();
            HallazgoId = new List<int>();
        }

        public void CargarDatos (CargarDatosContext dataContext, Int32? idAccionMejora, Int32 idCiclo, Int32 idEscuela)
        {
            //TRABAJAMOS CON EL MISMO CICLO YA QUE CADA SUBMODALIDAD TIENE 1 CICLO
            IdAccionMejora = idAccionMejora;

            LstConstituyentes = dataContext.context.Constituyente.ToList();

            var query0 = (from cpa in dataContext.context.CursoPeriodoAcademico
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Curso on cpa.IdCurso equals c.IdCurso
                          join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                          where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idCiclo && ca.IdEscuela == idEscuela
                          select c).Distinct().OrderBy(x => x.NombreEspanol);
            LstCursos = query0.ToList();

            List<Hallazgos> hallazgos = new List<Hallazgos>();

            if (IdAccionMejora.HasValue)
            {
                AccionMejora accionmejora = dataContext.context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                Codigo = accionmejora.Codigo;
                Descripcion = accionmejora.DescripcionEspanol;

                hallazgos = dataContext.context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).Select(x => x.Hallazgos).ToList();

                HallazgoId = hallazgos.Select(x => x.IdHallazgo).ToList();


            }





            LstHallazgos = hallazgos.Select(h => new SelectListItem
            {
                Text = h.Codigo + " : " + h.DescripcionEspanol,
                Value = h.IdHallazgo.ToString()
            });




        }

    }
}