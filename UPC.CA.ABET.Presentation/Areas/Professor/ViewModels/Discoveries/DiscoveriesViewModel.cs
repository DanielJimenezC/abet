﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class DiscoveriesViewModel
    {

        public Int32 InstrumentoId { get; set; }
        public String NombreInstrumento { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableModalidades { get; set; }
        public IEnumerable<SelectListItem> AvailableNivelesAceptacion { get; set; }
        public IEnumerable<SelectListItem> AvailableCriticidad { get; set; }
        public IEnumerable<SelectListItem> AvailableSedes { get; set; }
        public IEnumerable<SelectListItem> AvailableAreas { get; set; }
        public IEnumerable<SelectListItem> AvailableSubareas { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableCursosCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableCarreras { get; set; }
        public IEnumerable<SelectListItem> AvailableComision{ get; set; }
        public IEnumerable<SelectListItem> AvailableOutcome { get; set; }
        public Int32 ResultadoId { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdModalidad { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdAreaUnidadAcademica { get; set; }
        public Int32? IdSubareaUnidadAcademica { get; set; }
        public Int32? IdCursoUnidadAcademica { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdCriticidad { get; set; }
        public Boolean? esModelFiltroParaAccionMejora { get; set; }
        public Boolean? desdemenu { get; set; }
     //   public List<Hallazgo> LstHallazgos { get; set; }
        public List<Hallazgos> LstHallazgos { get; set; }                                                                                       // RML001
        public List<AccionMejora> LstAccionMejora { get; set; }
        public void fill (CargarDatosContext datacontext, int ModalidadId)
        {
            try
            {           
           
            
            var context = datacontext.context;
            var Session = datacontext.session;
            var idEscuela = Session.GetEscuelaId();

            Boolean Spanish = datacontext.currentCulture == ConstantHelpers.CULTURE.ESPANOL;
            esModelFiltroParaAccionMejora = false;

            IdModalidad = Session.GetModalidadId();



            if (InstrumentoId != 0)
                NombreInstrumento = context.Instrumento.First(X => X.IdInstrumento == InstrumentoId).NombreEspanol;
            else
                NombreInstrumento = "";


            var areas = GedServices.GetAllAreasServices(context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() ==
            ConstantHelpers.CULTURE.ESPANOL ? new SelectListItem { Value = x.IdUnidadAcademica.ToString(), Text = x.NombreEspanol } : 
            new SelectListItem { Value = x.IdUnidadAcademica.ToString(), Text = x.NombreIngles } /*x.NombreEspanol : x.NombreIngles*/).Distinct();  
            

            AvailableAreas = areas;//areas.Select(o => new SelectListItem { Value = o, Text = o });
            var subareas = GedServices.GetAllSubareasServices(context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() ==
            ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();

            AvailableSubareas = subareas.Select(o => new SelectListItem { Value = o, Text = o });
            
            var cursos = GedServices.GetAllCursosServices(context, idEscuela, @Session.GetModalidadId()).Select(x => SessionHelper.GetCulture(Session).ToString() == 
            ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            AvailableCursos = cursos.Select(o => new SelectListItem { Value = o, Text = o });


            //var periodoAcademicoActId = context.PeriodoAcademico.FirstOrDefault(x=>x.Estado == "ACT").IdPeriodoAcademico;

            //var periodoAcademicoActivoId = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT"
            //&& x.IdSubModalidad == idSubModalidad).IdPeriodoAcademico;

            var intSubModalidadPeriodoAcademicoActive = context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad==ModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
     
            var cursosDisponibles = GedServices.GetAllCursosServices(context, idEscuela, @Session.GetModalidadId()).Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico 
                == intSubModalidadPeriodoAcademicoActive && x.IdCursoPeriodoAcademico!= null)
                 .Select(x => x.CursoPeriodoAcademico.Curso);
                 

            AvailableCursosCursos = cursosDisponibles.Select(o => new SelectListItem
                {
                    Value = o.IdCurso.ToString(),
                    Text = Spanish ? o.NombreEspanol : o.NombreIngles
                });

  


            AvailablePeriodoAcademicos = (from a in context.PeriodoAcademico
                                          join sm in context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals sm.IdPeriodoAcademico
                                          join smo in context.SubModalidad on sm.IdSubModalidad equals smo.IdSubModalidad
                                          join mo in context.Modalidad on smo.IdModalidad equals mo.IdModalidad
                                          where mo.IdModalidad == IdModalidad
                                          select new SelectListItem { Value = a.IdPeriodoAcademico.ToString(), Text = a.CicloAcademico }).OrderByDescending(o => o.Text);



            AvailableModalidades = GedServices.GetAllModalidadServices(context)
               .Select(o => new SelectListItem { Value = o.IdModalidad.ToSafeString(), Text = o.NombreEspanol }).OrderByDescending(o => o.Text);
            AvailableNivelesAceptacion = context.NivelAceptacionHallazgo.ToList()
                .Select(o => new SelectListItem { Value = o.IdNivelAceptacionHallazgo.ToSafeString(), Text = o.NombreEspanol });
            AvailableCriticidad = context.Criticidad.ToList()
           .Select(o => new SelectListItem { Value = o.IdCriticidad.ToSafeString(), Text = o.NombreEspanol });

            AvailableSedes = GedServices.GetAllSedesServices(context)
                .Select(o => new SelectListItem { Value = o.IdSede.ToString(), Text = o.Nombre });
            
            AvailableCarreras = (from a in context.Carrera
                                 join b in context.SubModalidad on a.IdSubmodalidad equals b.IdSubModalidad
                                 where b.IdModalidad==ModalidadId && a.IdEscuela==idEscuela
                                 select a                                
            ).ToList().Select(o => new SelectListItem { Value = o.IdCarrera.ToString(), Text = o.NombreEspanol });

            AvailableComision = GedServices.GetAllComisionsServices(context).Select(o => new SelectListItem { Value = o.IdComision.ToString(), Text = o.NombreEspanol });
            AvailableOutcome = context.Outcome.Select(o => new SelectListItem { Value = o.IdOutcome.ToString(), Text = o.Nombre });
            //LstHallazgos = context.Hallazgo.Where(X =>
            //    X.IdInstrumento == InstrumentoId &&
            //    (IdPeriodoAcademico.HasValue ? (X.IdPeriodoAcademico == IdPeriodoAcademico.Value) : true) &&
            //    (IdSede.HasValue ? (X.IdSede == IdSede.Value) : true) &&
            //    (IdAreaUnidadAcademica.HasValue ? (X.IFCHallazgo.Any(Y => (Y.IFC.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre == IdAreaUnidadAcademica.Value))) : true) &&
            //    (IdSubareaUnidadAcademica.HasValue ? (X.IFCHallazgo.Any(Y => (Y.IFC.UnidadAcademica.IdUnidadAcademicaPadre == IdSubareaUnidadAcademica.Value))) : true) &&
            //    (IdCursoUnidadAcademica.HasValue ? (X.IFCHallazgo.Any(Y => (Y.IFC.IdUnidadAcademica == IdCursoUnidadAcademica.Value))) : true) &&
            //    (IdCarrera.HasValue ? ((X.IdCarrera ?? -1) == IdCarrera.Value) : true) &&
            //    (IdComision.HasValue ? ((X.IdComision ?? -1) == IdComision.Value) : true) &&
            //    (IdOutcome.HasValue ? ((X.IdOutcome ?? -1) == IdOutcome.Value) : true) &&
            //    (IdNivelAceptacionHallazgo.HasValue? ((X.IdNivelAceptacionHallazgo ?? -1) == IdNivelAceptacionHallazgo.Value):true)
            //    ).GroupBy(x => x.Codigo).Select(x=>x.FirstOrDefault()).ToList();


            if(InstrumentoId==0 && (desdemenu.HasValue))
            {
               
               
                IdPeriodoAcademico = (from a in context.SubModalidadPeriodoAcademico
                                      join b in context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                      join c in context.SubModalidad on a.IdSubModalidad equals c.IdSubModalidad
                                      where b.Estado == "ACT" && c.IdModalidad == ModalidadId
                                      select b).FirstOrDefault().IdPeriodoAcademico;
                    
            }


            
            var query = context.Hallazgos.Where(x => x.ConstituyenteInstrumento.IdInstrumento == InstrumentoId || InstrumentoId==0).AsQueryable();
            query = query.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.IdModalidad == ModalidadId).AsQueryable();
                                                                                                                                                                      


            //var query = (from a in context.Hallazgo
            //         join b in context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
            //         join c in context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
            //         where ((a.IdInstrumento == InstrumentoId || InstrumentoId == 0) && (c.IdModalidad == ModalidadId)) select a).AsQueryable();



            if (IdPeriodoAcademico.HasValue)
                query = query.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico.Value);

            
            //if(IdSede.HasValue)                                                                                                                           
            //    query = query.Where(x => x.IdSede == IdSede.Value);

            if (IdAreaUnidadAcademica.HasValue)
                query = query.Where(x => x.IFCHallazgo.Any(Y => (Y.IFC.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre == IdAreaUnidadAcademica.Value)));

            if (IdSubareaUnidadAcademica.HasValue)
                query = query.Where(x => x.IFCHallazgo.Any(Y => (Y.IFC.UnidadAcademica.IdUnidadAcademicaPadre == IdSubareaUnidadAcademica.Value)));

            if (IdCursoUnidadAcademica.HasValue)
                query = query.Where(x => x.IFCHallazgo.Any(Y => (Y.IFC.IdUnidadAcademica == IdCursoUnidadAcademica.Value)));
   
            //if (IdCarrera.HasValue)
            //    query = query.Where(x => x.IdCarrera == IdCarrera.Value);

            if (IdComision.HasValue)
                query = query.Where(x => x.OutcomeComision.IdComision == IdComision.Value);

            if (IdOutcome.HasValue)
                query = query.Where(x => x.OutcomeComision.IdOutcome == IdOutcome.Value);

            if (IdNivelAceptacionHallazgo.HasValue)
                query = query.Where(x => x.IdNivelAceptacionHallazgo == IdNivelAceptacionHallazgo.Value);

            if (IdCriticidad.HasValue)
                query = query.Where(x => x.IdCriticidad == IdCriticidad.Value);


            if (IdCurso.HasValue)
                query = query.Where(x => x.IdCurso == IdCurso);

            //query = query.Where(x => x.Carrera.IdEscuela == escuelaId);


            // LstHallazgos = query.GroupBy(x => x.Codigo).Select(x => x.FirstOrDefault()).OrderBy(x => x.IdCriticidad).ToList();                                          // RML001

            LstHallazgos = query.OrderBy(x => x.IdCriticidad).ToList();

            // Mostrar contador / identificador en el viewmodel
            for (int i=0; i<LstHallazgos.Count(); i++)
            {
                LstHallazgos[i].Codigo = LstHallazgos[i].Codigo + '-' + LstHallazgos[i].Identificador.ToString();
            }

            // Obtener los ids de los hallazgos filtrados
            List<Int32> HallazgosIds = LstHallazgos.Distinct().Select(X => X.IdHallazgo).ToList();                                                                          // RML001
            //Obtener los hallazgo/accionmejora que esten asociados a los hallazgosIds
            List<HallazgoAccionMejora> lstHallazgoAccionMejora = context.HallazgoAccionMejora.Where(X => HallazgosIds.Contains(X.IdHallazgo)).ToList();
            
            //obtener las accionmejora q le pertencen a los hallazgo/accionmejora encontrados anteriormente
            LstAccionMejora = lstHallazgoAccionMejora.Select(X => X.AccionMejora).Distinct().ToList();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
          public JsonResult DatosResultadoAccionMejora(Int32 AccionMejoraId)
        {
            //List<String> RutasArchivos = new List<string>();
            //String Res = context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == AccionMejoraId).Resultado;
            //return Json(new { r = Res, lista = RutasArchivos }, JsonRequestBehavior.AllowGet);
            return null;
        }
    }
}