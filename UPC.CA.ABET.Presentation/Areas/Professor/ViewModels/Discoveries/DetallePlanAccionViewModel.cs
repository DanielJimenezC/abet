﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using System.Data.Entity;
using UPC.CA.ABET.Helpers;
namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class DetallePlanAccionViewModel
    {
        public Int32 PlanAccionId { get; set; }
        public List<AccionMejora> AccionesPlan { get; set; }
        // Dictionary<AccionMejoraId,List de sus hallazgos>
        public Dictionary<Int32, List<Hallazgo>> Hallazgos { get; set; }
        public Boolean Agregar { get; set; }
        public Boolean Cerrado { get; set; }

        public IEnumerable<SelectListItem> AvailableNivelAceptacion { get; set; }
        public IEnumerable<SelectListItem> AvailableIntrumento { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademico { get; set; }
        public Int32? PeriodoAcademicoId { get; set; }
        public Int32? NivelAceptacionId { get; set; }
        public Int32? InstrumentoId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }

        //public void fill(CargarDatosContext datacontext)
        //{
        //    var planAccion = datacontext.context.PlanAccion.FirstOrDefault(X=>X.IdPlanAccion == PlanAccionId);
        //    if(planAccion == null)
        //        return;
        //    Int32 AnioBuscar = planAccion.Anio - 1;

        //    AvailableNivelAceptacion = datacontext.context.NivelAceptacionHallazgo.ToList().Select(X => new SelectListItem { Value = X.IdNivelAceptacionHallazgo.ToString(), Text = X.NombreEspanol }).ToList();
        //    AvailableIntrumento = datacontext.context.Instrumento.ToList().Select(X => new SelectListItem { Value = X.IdInstrumento.ToString(), Text = X.NombreEspanol }).ToList();
        //    AvailablePeriodoAcademico = datacontext.context.PeriodoAcademico.Where(x => x.CicloAcademico.Contains(AnioBuscar.ToString())).ToList().Select(X => new SelectListItem { Value = X.IdPeriodoAcademico.ToString(), Text= X.CicloAcademico }).ToList();

        //    Hallazgos = new Dictionary<int, List<Hallazgo>>();


        //    //Buscar las acciones que no tienen relacion con este plan de acción
        //    var accionesActuales = planAccion.AccionMejoraPlanAccion.Select(X => X.AccionMejora).ToList();

        //    //Acciones de mejora filtradas por instrumento y nivel de aceptacion (si aplica)

            
        //    var accionesDisponibles = datacontext.context.Hallazgo.Where(X =>
        //      ( InstrumentoId.HasValue ? X.IdInstrumento == InstrumentoId.Value : true) &&
        //      (  planAccion.IdCarrera == X.IdCarrera )&&
        //      (SubModalidadPeriodoAcademicoId.HasValue ?  X.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId.Value  : X.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico.Contains(AnioBuscar.ToString()) )&&
        //      (  NivelAceptacionId.HasValue && X.IdNivelAceptacionHallazgo.HasValue ? NivelAceptacionId.Value == X.IdNivelAceptacionHallazgo.Value : true))
        //      .SelectMany(X => X.HallazgoAccionMejora.Select(Y => Y.AccionMejora)).Distinct().ToList();

        //    if(Agregar)
        //    {
        //        //Ids Acciones en plan
        //         var existeAccion = accionesActuales.Select(X => X.IdAccionMejora).ToList();
                
        //        //Obtener todas las acciones que aun no estan en el plan
        //         AccionesPlan = accionesDisponibles.Where(X => !existeAccion.Any(Y => (Y == X.IdAccionMejora))).ToList();
        //    }
        //    else
        //    {
        //        var existeAccion = accionesActuales.Select(X => X.IdAccionMejora).ToList();

        //        //Obtener todas las acciones que aun no estan en el plan
        //        AccionesPlan = accionesDisponibles.Where(X => existeAccion.Any(Y => (Y == X.IdAccionMejora))).ToList();                
        //    }
        //    List<Int32> IdsAccionesMejora = AccionesPlan.Select(X => X.IdAccionMejora).ToList();
        //    var HallazgosAccionMejora = datacontext.context.HallazgoAccionMejora.Where(X => IdsAccionesMejora.Any(Y => Y == X.IdAccionMejora)).Include(X => X.Hallazgos).ToList();
        //    Cerrado = (planAccion.Estado == "CER");
        //    AccionesPlan.ForEach(X => Hallazgos[X.IdAccionMejora] = HallazgosAccionMejora.Where(Y=>Y.IdAccionMejora==X.IdAccionMejora).Select(Y => Y.Hallazgos).GroupBy(Y=>Y.Codigo).Select(x=>x.FirstOrDefault()).ToList());
          
        //}
    }
}