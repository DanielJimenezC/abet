﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class EditarResultadoAccionMejoraViewModel
    {
        public String Resultado { get; set; }
        public Int32 idAccionMejora { get; set; }
        public IEnumerable<HttpPostedFileBase> Archivos { get; set; }
        public Int32 PlanAccionId { get; set; }
        public Boolean Agregar { get; set; }
    }
    
}