﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.Discoveries
{
    public class AddEditAccionMejoraViewModel
    {
        public Int32? HallazgoIdSeleccionado { get; set; }
        public Int32? AccionMejoraId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]        
        public List<Int32> HallazgoId { get; set; }
      //  public List<Hallazgo> LstHallazgos { get; set; }                                                                                                      // RML001
        public List<Hallazgos> LstHallazgos { get; set; }
        public String Codigo { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]        
        public String Descripcion { get; set; }
        public String Estado { get; set; }
        public Int32 InstrumentoId { get; set; }
        public Int32 ResultadoId { get; set; }

        public void fill(CargarDatosContext datacontext, Int32? AccionMejoraId, DiscoveriesViewModel filtrosModel, Int32? InstrumentoIdEditar,int ModalidadId)
        {
            this.AccionMejoraId = AccionMejoraId;
            var instrumentoId = InstrumentoIdEditar ?? 0;
            //    LstHallazgos = datacontext.context.Hallazgo.Where(h => instrumentoId == 0 || instrumentoId == h.IdInstrumento).ToList();                          // RML001
            LstHallazgos = datacontext.context.Hallazgos.Where(h => instrumentoId == 0 || instrumentoId == h.ConstituyenteInstrumento.IdInstrumento).ToList();      // RML001
            
            //yyap
            InstrumentoId = instrumentoId;
            ResultadoId = 2;
            //porsiacso
            if(filtrosModel != null)
            {
                InstrumentoId = filtrosModel.InstrumentoId;
                ResultadoId = filtrosModel.ResultadoId;
                //SUBMODALIDAD=1
                filtrosModel.fill(datacontext, ModalidadId);
                if (filtrosModel.LstHallazgos != null && filtrosModel.LstHallazgos.Any())
                {
                    LstHallazgos = filtrosModel.LstHallazgos;   
                }
                //no e men q esta fallatando, hasta aca funca bien men???? mmm
            }

            HallazgoId = new List<int>();
            if (AccionMejoraId.HasValue)
            {
                var accionMejora = datacontext.context.AccionMejora.FirstOrDefault(X => X.IdAccionMejora == AccionMejoraId.Value);
                if (accionMejora == null)
                    return;
                HallazgoId = accionMejora.HallazgoAccionMejora
                    .Where(X => X.IdAccionMejora == AccionMejoraId.Value)
                    .Select(X => X.Hallazgos)
                    .GroupBy(x => x.Codigo)
                    .Select(x => x.Max(h => h.IdHallazgo))
                    .ToList();
                Codigo = accionMejora.Codigo;
                Descripcion = accionMejora.DescripcionEspanol;
                Estado = accionMejora.Estado;
            }
        }

    }
}