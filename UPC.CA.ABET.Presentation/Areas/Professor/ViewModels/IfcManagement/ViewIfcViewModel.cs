﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class ViewIfcViewModel
    {
        public List<AccionMejora> AccionesPrevias { get; set; }
      //  public List<Hallazgo> Hallazgos { get; set; }                                                                 // RML001
        public List<Hallazgos> Hallazgos { get; set; }                                                                  // RML001
        public List<AccionMejora> AccionesPropuestas { get; set; }
        public int IdIfc { get; set; }
        public string Estado { get; set; }
        public bool PuedeExportar { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Docente Docente { get; set; }
        public int IdUnidadAcademica { get; set; }
        public string ResultadoAlcanzado { get; set; }
        public string ResultadoLogroCurso { get; set; }
        public string Infraestructura { get; set; }
        public string ComentarioEncuesta { get; set; }
        public string ApreciacionDelegado { get; set; }
        public string ApreciacionAlumnos { get; set; }
        public string Titulo { get; set; }
        public int IdCarrera { get; set; }
        public int IdCurso { get; set; }
        public string CodigoCurso { get; set; }
        public string CicloAcademico { get; set; }
        public bool PuedeEditar { get; set; }
        public bool PuedeObservar { get; set; }
        public bool PuedeAprobar { get; set; }
        public string Observaciones { get; set; }
        public string TipoSubmit { get; set; }
        public string ObservadoPor { get; set; }
        public string AprobadoPor { get; set; }
        public bool Escoordinador { get; set; }
    }
}