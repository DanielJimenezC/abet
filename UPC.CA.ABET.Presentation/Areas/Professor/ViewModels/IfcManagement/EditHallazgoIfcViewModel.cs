﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class EditHallazgoIfcViewModel
    {
        public IEnumerable<SelectListItem> AvailableNivelesAceptacion { get; set; }
        public string NivelAceptacion { get; set; }
        public int IdHallazgo { get; set; }
        public string Codigo { get; set; }
        public string Sede { get; set; }
        public string PeriodoAcademico { get; set; }
        public string Criticidad { get; set; }
        public string Descripcion { get; set; }
        public string CodigoCurso { get; set; }
        public string CodigoHallazgo { get; set; }

        public List<NivelAceptacionHallazgo> NivelAceptacionList { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public List<AccionMejora> AccionesMejora { get; set; }
        public Int32 InstrumentoId { get; set; }
        public String NombreInstrumento { get; set; }
        public int Identificador { get; set; }                                                                              // RML001
    }
}