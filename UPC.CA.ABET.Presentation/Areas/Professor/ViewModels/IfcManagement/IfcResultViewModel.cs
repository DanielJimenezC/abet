namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class IfcResultViewModel
    {
        public int IdIfc { get; set; }
        public string Codigo { get; set; }
        public string Carrera { get; set; }
        public string PeriodoAcademico { get; set; }
        public int SubModalidadPeriodoAcademicoId { get; set; }
        public string Curso { get; set; }
        public string Estado { get; set; }
        public bool DisableEditar { get; set; }
        public bool DisableEliminar { get; set; }
        public string CoordinadorCurso { get; set; }
    }
}