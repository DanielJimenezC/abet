﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class ListaNoIFCCorreoViewModel
    {
        public Int32? IdPeriodoAcademico { get; set; }
        public List<USP_SEL_IFCNoRegistrado_v2_Result> ListaIFCNoregistrado { get; set; }


        public void CargarDatos(CargarDatosContext dataContext, Int32? idPeriodoAcademico) {
            var SubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == idPeriodoAcademico).IdSubModalidadPeriodoAcademico;
            var query = dataContext.context.USP_SEL_IFCNoRegistrado_v2(SubModalidadPeriodoAcademico);

            ListaIFCNoregistrado = query.ToList();
            

        }

    }
}