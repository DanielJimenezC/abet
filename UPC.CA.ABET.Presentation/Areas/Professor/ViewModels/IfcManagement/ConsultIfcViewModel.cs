using System.Collections.Generic;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class ConsultIfcViewModel
    {
        public IEnumerable<SelectListItem> AvailableAreas { get; set; }
        public IEnumerable<SelectListItem> AvailableSubareas { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableEstados { get; set; }
        
        public int IdAreaUnidadAcademica { get; set; }
        public int IdSubareaUnidadAcademica { get; set; }  
        public int IdPeriodoAcademico { get; set; }
        public int IdCursoUnidadAcademica { get; set; }
        public string Estado { get; set; }
        public string Language { get; set; }
        public List<IfcResultViewModel> Resultados { get; set; }
        public ConsultIfcViewModel()
        {
            Resultados = new List<IfcResultViewModel>();
        }
    }
}