﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class CreateIfcViewModel
    {
        public IEnumerable<SelectListItem> AvailableAreas { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableSubareas { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }


        public List<AccionMejora> AccionesPrevias { get; set; }
        public List<HallazgoViewModel> Hallazgos { get; set; }                                           
        public List<AccionMejoraViewModel> AccionesPropuestas { get; set; }
        public List<Sede> Sedes { get; set; }
        public List<Criticidad> Criticidades { get; set; }
        public string Idioma { get; set; }
        public int IdIfc { get; set; }
        public string Estado { get; set; }
        public string NombreArea { get; set; }
        public int IdCurso { get; set; }
        public int IdDocente { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string ResultadoAlcanzado { get; set; }
        public string ResultadoLogroCurso { get; set; }
        public string Infraestructura { get; set; }
        public string ComentarioEncuesta { get; set; }
        public string ApreciacionDelegado { get; set; }
        public string TipoSubmit { get; set; }
        public string ApreciacionAlumnos { get; set; }
        public string Titulo { get; set; }
        public int IdAreaUnidadAcademica { get; set; }
        public int IdSubareaUnidadAcademica { get; set; }
        public int IdCursoUnidadAcademica { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public string CodigoCurso { get; set; }
        public string CicloAcademico { get; set; }
        public string Observaciones { get; set; }

        public int IdSubmodalidadPeriodoAcademicoModulo { get; set; }                                                                         // RML001
        public int IdConstituyenteInstrumento { get; set; }                                                                                   // RML001

        public bool EsCoordinador { get; set; }
        //-------
        public int IdSubModalidad { get; set; }
    }
}