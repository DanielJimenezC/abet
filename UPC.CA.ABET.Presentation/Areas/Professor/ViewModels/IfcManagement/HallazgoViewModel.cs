﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class HallazgoViewModel
    {
        public int IdHallazgo { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionIngles { get; set; }
        public string Criticidad { get; set; }
        public int IdSede { get; set; }
        public int IdConstituyente { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdActa { get; set; }
        public string NivelAceptacion { get; set; }
        public string PeriodoAcademico { get; set; }
        public string Sede { get; set; }
        public int CorrelativoHallazgoTemporal { get; set; }                                                                // Correlativo temporal para cargas masivas
        
        public string idOutcome { get; set; }
        public string Outcome { get; set; }

        public Int32? IdCurso { get; set; }                                                                                 // RML0001    
        public int? IdNivelAceptacionHallazgo { get; set; }                                                               // RML0001    SI NO EXISTE POR DEFECTO ES 1
        public System.DateTime FechaRegistro { get; set; }                                                               // RML0001
        public int IdCriticidad { get; set; }                                                                            // RML0001
        public Int32? IdSubmodalidadPeriodoAcademicoModulo { get; set; }                                                 // RML0001
    //    public string Estado { get; set; }                                                                                  // RML0001
        public Int32? Identificador { get; set; }                                                                           // RML0001                                                                   
        public bool PuedeEditar { get; set; }

        public HallazgoViewModel()
        {
            PuedeEditar = false;
        }
    }
}