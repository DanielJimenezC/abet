﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class AccionMejoraViewModel
    {
        public int IdAccionMejora { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionIngles { get; set; }
        public string CodigoHallazgo { get; set; }
        public string Estado { get; set; }
        public int IdHallazgo { get; set; }
        public bool Enplan { get; set; }
        public int CorrelativoHallazgoTemporal { get; set; }                        // Para relacionar el hallazgo con la AM en las cargas masivas

        public AccionMejoraViewModel()
        {
            this.Enplan = false;
         }
    }
}