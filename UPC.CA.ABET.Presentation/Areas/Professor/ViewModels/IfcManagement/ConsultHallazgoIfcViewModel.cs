﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class ConsultHallazgoIfcViewModel
    {
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableNivelesAceptacion { get; set; }
        public IEnumerable<SelectListItem> AvailableSedes { get; set; }
        public IEnumerable<SelectListItem> AvailableAreas { get; set; }
        public IEnumerable<SelectListItem> AvailableSubareas { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public string CodigoNivelAceptacion { get; set; }
        public int IdSede { get; set; }
        public int IdAreaUnidadAcademica { get; set; }
        public int IdSubareaUnidadAcademica { get; set; }
        public int IdCursoUnidadAcademica { get; set; }
        public List<HallazgoViewModel> Resultados { get; set; }
        public ConsultHallazgoIfcViewModel()
        {
            Resultados = new List<HallazgoViewModel>();
        }
    }
}