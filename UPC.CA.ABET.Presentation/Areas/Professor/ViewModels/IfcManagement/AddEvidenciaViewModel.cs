﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class AddEvidenciaViewModel
    {
        public string AccionDeMejora { get; set; }
        public string CodAccionMejora { get; set; }


        [Required(ErrorMessage = "Debes ingresar una justificación.")]
        public string AddEvidenciaText { get; set; }

        public int idAccionDeMejora { get; set; }
        public string vistaF { get; set; }
        public HttpPostedFileBase file { get; set; }


        public void CargarData(int IDAccionMejora, AbetEntities context,string vista)
        {
            AccionMejora ame = (from am in context.AccionMejora
                                where am.IdAccionMejora == IDAccionMejora
                                select am).FirstOrDefault();

            idAccionDeMejora = IDAccionMejora;
            AccionDeMejora = ame.DescripcionEspanol;
            CodAccionMejora = ame.Codigo;
            vistaF = vista;


            return;
        }

    }
}