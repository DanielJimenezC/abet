﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement
{
    public class ReportStatusViewModel
    {

        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        //public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public List<string> LstStatus { get; set; }
        public List<string> LstStatusEn { get; set; }
        public List<string> LstCarreras { get; set; }
        public List<string> LstCarrerasEn { get; set; }
        public List<string> LstArea { get; set; }
        public List<string> LstCiclos { get; set; }
        public string SelectedAnswer { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public ReportStatusViewModel()
        {
            LstStatus = new List<string>();
            LstCarreras = new List<string>();
            LstArea = new List<string>();
            LstCiclos = new List<string>();

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idPeriodoAcademico, Int32? idmodalidad, int sessionIdEscuela)
        {
            
            IdPeriodoAcademico = idPeriodoAcademico;
            AbetEntities context = dataContext.context;
            LstStatus = (from a in context.IFC
                    select a.Estado).Distinct().ToList();

            LstStatusEn = (from a in context.IFC
                           select a.Estado).Distinct().ToList();

            LstCarreras = (from cpp in context.CarreraPeriodoAcademico
                           join c in context.Carrera on cpp.IdCarrera equals c.IdCarrera
                           where cpp.IdSubModalidadPeriodoAcademico == idPeriodoAcademico && c.IdEscuela == sessionIdEscuela
                           select c.NombreEspanol).Distinct().ToList();

            LstCarrerasEn = (from cpp in context.CarreraPeriodoAcademico
                             join c in context.Carrera on cpp.IdCarrera equals c.IdCarrera
                             where cpp.IdSubModalidadPeriodoAcademico == idPeriodoAcademico && c.IdEscuela == sessionIdEscuela
                             select c.NombreIngles).Distinct().ToList();

            LstCiclos = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == idmodalidad).Select(x => x.PeriodoAcademico.CicloAcademico).ToList();



            LstArea = (from u in context.UnidadAcademica
                       where u.Tipo == "AREA" && u.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico
                       select u.NombreEspanol).Distinct().ToList();
            for (int i = 0; i < LstStatus.Count(); i++)
            {

                if (LstStatus[i] == "ENV")
                {
                    LstStatus[i] = "ENVIADO";
                }
                else if (LstStatus[i] == "APR")
                {
                    LstStatus[i] = "APROBADO";
                }
                else if (LstStatus[i] == "ESP")
                {
                    LstStatus[i] = "ESPERA";
                }
            }
            for (int i = 0; i < LstStatusEn.Count; i++)
            {
                if (LstStatusEn[i] == "ENV")
                {
                    LstStatusEn[i] = "SENT";
                }
                else if (LstStatusEn[i] == "APR")
                {
                    LstStatusEn[i] = "APPROVED";
                }
                else if (LstStatusEn[i] == "ESP")
                {
                    LstStatusEn[i] = "WAIT";
                }
            }


            LstStatus.Insert(0, "TODOS LOS ESTADOS");
            LstStatus.Insert(LstStatus.Count(), "PENDIENTE");
            LstStatusEn.Insert(0, "ALL STATES");
            LstStatusEn.Insert(LstStatusEn.Count(), "PENDING");

            Ciclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == idPeriodoAcademico);
            
           
        }
    }
}