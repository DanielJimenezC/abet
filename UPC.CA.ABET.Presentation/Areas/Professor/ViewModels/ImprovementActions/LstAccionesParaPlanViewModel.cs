﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementActions
{
    public class LstAccionesParaPlanViewModel
    {
        public int? Anio { get; set; }
        public int? IdCarrera { get; set; }
        public int? IdComision { get; set; }

        public List<int> LstAnios { get; set; }

        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<AccionMejora> LstAccionMejora { get; set; }

        public LstAccionesParaPlanViewModel()
        {

            LstAccionMejora = new List<AccionMejora>();
            LstAnios = new List<int>();



            LstCarrera = new List<Carrera>();
            LstComision = new List<Comision>();

        }

        public void fill(CargarDatosContext dataContext,int idescuela, int? anio, int? idcarrera, int? idcomision, int IdSubModalidad)
        {
            //int IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;


            int IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == 1).IdSubModalidadPeriodoAcademico;

            Anio = anio;


            IdCarrera = idcarrera;
            IdComision = idcomision;

            int canio = 2016;
            int aniomax = DateTime.Now.Year;

            if(Anio.HasValue == false)
            {
                Anio = aniomax;
            }

            while (true)
            {
                LstAnios.Add(canio);
                canio = canio + 1;

                if (canio > aniomax)
                    break;
            }

            LstCarrera = dataContext.context.CarreraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Carrera.IdEscuela == idescuela).Select(x=>x.Carrera).ToList();

            
            if(IdCarrera.HasValue)
            {
                LstComision = dataContext.context.CarreraComision.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdCarrera == IdCarrera).Select(x => x.Comision).ToList();

            }


          var query = (from am in dataContext.context.AccionMejora 
                         join ham in dataContext.context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                         join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                         join oc in dataContext.context.OutcomeComision on  h.IdOutcome equals oc.IdOutcome 
                         where (am.Estado != "INA" && h.Estado != "INA" && h.IdCarrera == idcarrera && 
                                    am.Anio == Anio && oc.IdComision == IdComision && oc.IdSubModalidadPeriodoAcademico== IdSubModalidadPeriodoAcademico)
                         select am).Distinct().ToList();


            LstAccionMejora = query;



        }

    }
}