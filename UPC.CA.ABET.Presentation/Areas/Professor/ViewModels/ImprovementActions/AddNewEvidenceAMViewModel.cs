﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementActions
{
    public class AddNewEvidenceAMViewModel
    {
        public string CodInstrumento { get; set; }
        public Int32? IdAccionMejora { get; set; }
        public List<HallazgosPlanResultados> LstHallazgos { get; set; }
        public bool HayCuantitativo { get; set; }
        public AddNewEvidenceAMViewModel()
        {
            LstHallazgos = new List<HallazgosPlanResultados>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32? idAccionMejora, int anio, string codInstrumento)
        {
            CodInstrumento = codInstrumento;
            IdAccionMejora = idAccionMejora;
            LstHallazgos = (from ham in dataContext.context.HallazgoAccionMejora
                            join h in dataContext.context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                            join ci in dataContext.context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                            join i in dataContext.context.Instrumento on ci.IdInstrumento equals i.IdInstrumento
                            where ham.IdAccionMejora == idAccionMejora
                             select new HallazgosPlanResultados
                             {        
                                 IdHallazgo = ham.IdHallazgo,
                                 IdHallazgoAccionMejora = ham.IdHallazgoAccionMejora,
                                 Evidencia = ham.DescripcionEspanol,
                                 CodigoHallazgo = h.Codigo + "-" + h.Identificador,
                                 DescripcionHallazgo = h.DescripcionEspanol,
                                 Instrumento = i.Acronimo,
                                 Tipo = i.Tipo
                             }).Distinct().ToList();

            HayCuantitativo = LstHallazgos.Any(x => x.Tipo == "Cuantitativo");
            foreach (var item in LstHallazgos)
            {
                //Resultados L
                var objeto = dataContext.context.Database.SqlQuery<HallazgosFiltrado>("dbo.ReporteResut_B_L @p0, @p1", "es-PE", item.IdHallazgo).FirstOrDefault();
                if (objeto != null)
                {
                    item.HallazgoPorcentaje_1 = objeto.Porcentaje;
                    item.HallazgoColor_1 = objeto.Color;
                }

                //Resultados R
                var objeto2 = dataContext.context.Database.SqlQuery<HallazgosFiltrado>("dbo.ReporteResut_B_R {0}, {1}, {2}"
                    , "es-PE", anio, item.IdHallazgo).FirstOrDefault();
                if (objeto2 != null)
                {
                    item.HallazgoPorcentaje_2 = objeto2.Porcentaje;
                    item.HallazgoColor_2 = objeto2.ColorNivelAceptacion;
                }

            }
        }
    }
}