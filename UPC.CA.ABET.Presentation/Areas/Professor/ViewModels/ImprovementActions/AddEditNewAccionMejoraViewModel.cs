﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementActions
{
    public class AddEditNewAccionMejoraViewModel
    {
        public Int32? IdAccionMejora { get; set; }
        public AccionMejora accionmejora { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public string DescripcionHallazgo { get; set; }

        public List<Constituyente> LstConstituyentes { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Curso> LstCursos { get; set; }
        public Int32? IdConstituyente { get; set; }
        public Int32? IdInstrumento { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdOutcome { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }
        public IEnumerable<SelectListItem> LstHallazgos { get; set; }
        public List<Int32> HallazgoId { get; set; }

        public AddEditNewAccionMejoraViewModel()
        {
            Codigo = "";
            LstConstituyentes = new List<Constituyente>();
            LstCursos = new List<Curso>();
            LstInstrumentos = new List<Instrumento>();
            LstCriticidad = new List<Criticidad>();
            LstOutcome = new List<Outcome>();
            HallazgoId = new List<int>();
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idAccionMejora, Int32 idModalidad, Int32 idEscuela)
        {
            IdAccionMejora = idAccionMejora;
            List<Hallazgos> hallazgos = new List<Hallazgos>();
            if (IdAccionMejora.HasValue)
            {
                AccionMejora accionmejora = dataContext.context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                List<int?> LstSmpam = (from smpam in dataContext.context.SubModalidadPeriodoAcademicoModulo 
                                        join smpa in dataContext.context.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                        join sm in dataContext.context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                                        join m in dataContext.context.Modalidad on sm.IdModalidad equals m.IdModalidad
                                        join pa in dataContext.context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                        where pa.FechaInicioPeriodo.Value.Year == accionmejora.Anio && m.IdModalidad == idModalidad
                                        select (int?)smpam.IdSubModalidadPeriodoAcademicoModulo).Distinct().ToList();

                Codigo = accionmejora.Codigo+ "-"+accionmejora.Identificador;
                Descripcion = accionmejora.DescripcionEspanol;
                hallazgos = dataContext.context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).Select(x => x.Hallazgos).ToList();
                HallazgoId = hallazgos.Select(x => x.IdHallazgo).ToList();

                LstHallazgos = dataContext.context.Hallazgos.Where(x => LstSmpam.Contains(x.IdSubModalidadPeriodoAcademicoModulo)).Select(h => new SelectListItem
                {
                    Text = h.Codigo + "-" + h.Identificador + " : " + h.DescripcionEspanol,
                    Value = h.IdHallazgo.ToString()
                });
            }

        }
    }
}