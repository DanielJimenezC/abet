﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries
{
    public class ConsultAccDiscoveriesViewModel
    {
        public IEnumerable<SelectListItem> AvailableCarreras { get; set; }
        public IEnumerable<SelectListItem> AvailableSedes { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableNivelAceptacion { get; set; }

        public int IdCarrera { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public string CodigoNivelAceptacion { get; set; }
        public int IdSede { get; set; }
        public List<HallazgoAccViewModel> Resultados { get; set; }
        public ConsultAccDiscoveriesViewModel()
        {
            Resultados = new List<HallazgoAccViewModel>();
        }

    }
}