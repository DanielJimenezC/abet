﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries
{
    public class _EditAccViewModel
    {
        public int ifcid { get; set; }
        public string descripcionespanol { get; set; }
        public string descripcioningles { get; set; }
        public string codigo { get; set; }
        public int accionmejoraid { get; set; }
        public List<Hallazgos> lsthallazgos { get; set; }
        public List<Hallazgos> lsthallazgoswoacc { get; set; }
        public String Text { get; set; }
        public List<Outcome> lstoutcome { get; set; }
        public int idhallazgo { get; set; }
        public CursoMallaCurricular cursomallacurricular { get; set; }
        public Criticidad criticidad { get; set; }
        public List<Criticidad> lstcriticidad { get; set; }
        public string nombrecriticidad { get; set; }
        public string codigocurso { get; set; }
        public List<SelectListItem> lsthallazgosifc { get; set; }

        public _EditAccViewModel()
        {
            lsthallazgos = new List<Hallazgos>();
            lstoutcome = new List<Outcome>();
            lstcriticidad = new List<Criticidad>();
            lsthallazgoswoacc = new List<Hallazgos>();
            lsthallazgosifc = new List<SelectListItem>();
        }

        public void CargarData(AbetEntities context, int? paccionmejoraid, string pcodigocurso, int pifc)
        {
            var accionmejora = context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == paccionmejoraid);

            if (accionmejora != null)
            {
                this.codigo = accionmejora.Codigo + "-" + accionmejora.Identificador;
                this.descripcionespanol = accionmejora.DescripcionEspanol;
                this.descripcioningles = accionmejora.DescripcionIngles;
            }

          
            this.lstcriticidad = context.Criticidad.ToList();
            var curso = context.Curso.FirstOrDefault(x => x.Codigo == pcodigocurso);

            cursomallacurricular = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == curso.IdCurso);

            if (paccionmejoraid > 0)
            {
                lsthallazgos = (from a in context.HallazgoAccionMejora
                                join b in context.Hallazgos on a.IdHallazgo equals b.IdHallazgo
                                where a.IdAccionMejora == paccionmejoraid
                                select b).ToList();
            }

            lsthallazgoswoacc = (from a in context.Hallazgos
                                 join b in context.IFCHallazgo on a.IdHallazgo equals b.IdHallazgo
                                 where b.idIFC == pifc
                                 select a).ToList();

            lsthallazgosifc = lsthallazgoswoacc.Select(x => new SelectListItem { Value = x.IdHallazgo.ToString(), Text = x.Codigo + "-" + x.Identificador }).ToList();

            Outcome outcome;

            var listaoutcome = context.GetOutcomes(curso.IdCurso).ToList();

            for (int i = 0; i < listaoutcome.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaoutcome[i].IdOutcome;
                outcome.DescripcionEspanol = listaoutcome[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaoutcome[i].DescripcionIngles;
                lstoutcome.Add(outcome);
            }
        }
    }
}