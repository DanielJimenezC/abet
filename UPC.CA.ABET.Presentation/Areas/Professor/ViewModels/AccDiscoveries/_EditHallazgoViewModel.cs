﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries
{
    public class _EditHallazgoViewModel
    {
        public int hallazgoid { get; set; }
        public string descripcionespanol { get; set; }
        public string descripcioningles { get; set; }
        public string codigo { get; set; }
        public int criticidadid { get; set; }
        public List<Outcome> lstoutcome { get; set; }
        public List<Criticidad> lstcriticidad { get; set; }
        public CursoMallaCurricular cursomallacurricular { get; set; }

        public _EditHallazgoViewModel()
        {
            lstoutcome = new List<Outcome>();
            lstcriticidad = new List<Criticidad>();
        }

        public void CargarData(AbetEntities context, int hallazgoid, string codigocurso)
         {
            var curso = context.Curso.FirstOrDefault(x => x.Codigo == codigocurso);
            var hallazgo = context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == hallazgoid);

            Outcome outcome;
            var listaOutcomes = context.GetOutcomes(curso.IdCurso).ToList();
            for (int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionEspanol;
                lstoutcome.Add(outcome);
            }

            lstcriticidad = context.Criticidad.ToList();

            this.hallazgoid = hallazgo.IdHallazgo;
            this.descripcionespanol = hallazgo.DescripcionEspanol;
            this.descripcioningles = hallazgo.DescripcionIngles;


        }
    }
}