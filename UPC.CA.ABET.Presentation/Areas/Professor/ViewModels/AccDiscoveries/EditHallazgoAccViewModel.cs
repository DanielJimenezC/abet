﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AccDiscoveries
{
    public class EditHallazgoAccViewModel
    {
        public IEnumerable<SelectListItem> AvailableNivelesAceptacion { get; set; }
        public string NivelAceptacion { get; set; }
        public int IdHallazgo { get; set; }
        public string Codigo { get; set; }
        public string Sede { get; set; }
        public string PeriodoAcademico { get; set; }
        public string Descripcion { get; set; }
        public List<AccionMejora> AccionesMejora { get; set; }
    }
}