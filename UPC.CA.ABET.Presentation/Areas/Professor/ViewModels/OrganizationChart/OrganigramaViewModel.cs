﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Upload;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.OrganizationChart
{
    public class ItemOrganigrama
    {
        public Int32 IdUnidadAcademica { get; set; }
        public Int32? IdUnicdadAcademicaPadre { get; set; }
        public String Tipo { get; set; }
        public Int32 Nivel { get; set; }
        public String NombreUnidadAcademica { get; set; }
        public Int32? IdDocente { get; set; }
        public String NombreDocentes { get; set; }
        public String ApellDocentes { get; set; }
        public Int32? IdCurso { get; set; }
        public String NombreCurso { get; set; }
    }
    public class OrganigramaViewModel
    {
        public List<ItemOrganigrama> LstItemOrganigrama { get; set; }
        public Int32 Nivel { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdPeriodoAcademico { get; set; } 
        public Int32 IdModalidad { get; set; }
        

        public IEnumerable<SelectListItem> LstPeriodoAcademico { get; set; }
        public IEnumerable<SelectListItem> LstModulo { get; set; }

        public string TermsListUrl { get; set; }
        public string TermsListUrlRegular { get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }

        public OrganigramaViewModel()
        {
            LstItemOrganigrama = new List<ItemOrganigrama>();
        }
        public void CargarDatos(CargarDatosContext dataContext, int? periodoAcademicoId, int idEscuela, int modalidadId)
        {
            var entities = dataContext.context;

            IdPeriodoAcademico = periodoAcademicoId.Value;
            IdSubModalidadPeriodoAcademico = entities.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            IdModalidad = modalidadId;

            LstPeriodoAcademico = (from smpa in entities.SubModalidadPeriodoAcademico
                                   join pa in entities.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                   join sm in entities.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                                   where (sm.IdModalidad == IdModalidad)
                                   select pa).Select(pa => new SelectListItem
                                   {
                                       Text = pa.CicloAcademico,
                                       Value = pa.IdPeriodoAcademico.ToString()
                                   }).ToList();

            LstItemOrganigrama = entities.UnidadAcademica.Where(
                x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico)
                .Where(x => x.IdEscuela == idEscuela).Select(x => new ItemOrganigrama {
                IdUnidadAcademica = x.IdUnidadAcademica,
                IdUnicdadAcademicaPadre = x.IdUnidadAcademicaPadre,
                Tipo = x.Tipo,
                Nivel = x.Nivel,
                NombreUnidadAcademica = x.NombreEspanol,
                IdDocente = entities.SedeUnidadAcademica.Where(y => y.IdUnidadAcademica == x.IdUnidadAcademica).Select(
                    y => y.UnidadAcademicaResponsable.Where(j => j.IdSedeUnidadAcademica == y.IdSedeUnidadAcademica).Select(
                        k => k.Docente.IdDocente).FirstOrDefault()).FirstOrDefault(),
                NombreDocentes = entities.SedeUnidadAcademica.Where(y => y.IdUnidadAcademica == x.IdUnidadAcademica).Select(
                    y => y.UnidadAcademicaResponsable.Where(j => j.IdSedeUnidadAcademica == y.IdSedeUnidadAcademica).Select(
                        k => k.Docente.Nombres).FirstOrDefault()).FirstOrDefault(),
                ApellDocentes = entities.SedeUnidadAcademica.Where(y => y.IdUnidadAcademica == x.IdUnidadAcademica).Select(
                    y => y.UnidadAcademicaResponsable.Where(j => j.IdSedeUnidadAcademica == y.IdSedeUnidadAcademica).Select(
                        k => k.Docente.Apellidos).FirstOrDefault()).FirstOrDefault(),
                IdCurso = x.CursoPeriodoAcademico.IdCurso,
                NombreCurso = x.CursoPeriodoAcademico.Curso.NombreEspanol
            }).ToList();
            LstModulo = entities.Modalidad.Select(x => new SelectListItem
            {
                Text =  x.NombreEspanol,
                Value = x.IdModalidad.ToString()           
            }).ToList();

        }
    }

    
}