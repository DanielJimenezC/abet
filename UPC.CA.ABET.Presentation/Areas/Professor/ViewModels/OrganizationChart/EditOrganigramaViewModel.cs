﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.OrganizationChart
{
    public class EditOrganigramaViewModel
    {
        public Int32 IdUnidadAcademica { get; set; }
        public Int32? IdUnicdadAcademicaPadre { get; set; }
        public String Tipo { get; set; }
        public Int32 Nivel { get; set; }
        public String NombreUnidadAcademica { get; set; }
        public Int32? IdDocente { get; set; }
        public Int32? IdCurso { get; set; }
        public String NombreCurso { get; set; }
        public void CargarDatos(CargarDatosContext dataContext, Int32 idPeriodoAcademico)
        {

        }

    }
}