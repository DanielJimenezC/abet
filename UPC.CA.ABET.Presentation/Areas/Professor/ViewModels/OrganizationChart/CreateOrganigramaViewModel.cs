﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.OrganizationChart
{
    public class CreateOrganigramaViewModel
    {
        public Int32? UnidadAcademicaId { get; set; }
        public Int32 ParentId { get; set; }
        public String Titulo { get; set; }
        [Display(Name = "Docente:")]
        public Int32? DocenteId { get; set; }
        public List<String> ListaSedesElegidas { get; set; }
        public Int32? IdCursoPeriodoAcademico { get; set; }
        public IEnumerable<SelectListItem> ListaCursosPeriodoAcademico { get; set; }         
        public IEnumerable<SelectListItem> ListaCarreraPeriodoAcademico { get; set; }
        public IEnumerable<SelectListItem> ListaDocente { get; set; }
        public List<SelectListItem> ListaSede { get; set; }
        public Int32 PeriodoAcademicoId { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdCarreraPeriodoAcademico { get; set; }
        public Int32 Nivel { get; set; }
        public String Tipo { get; set; }

        public void cargarDatos(Int32 parentId, Int32 periodoAcademicoId, CargarDatosContext cargarDatos,Int32? unidadAcademicaId)
        {
            this.ParentId = parentId;
            this.PeriodoAcademicoId = cargarDatos.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == periodoAcademicoId).FirstOrDefault().IdPeriodoAcademico;
            int idsubmoda = periodoAcademicoId; //periodoAcademicoId es el valor de la submodalidad
            IdSubModalidadPeriodoAcademico = idsubmoda;
            ListaSedesElegidas = new List<String>();
            ListaCursosPeriodoAcademico = new List<SelectListItem>();
            ListaCarreraPeriodoAcademico = new List<SelectListItem>();
            ListaSede = new List<SelectListItem>();
            ListaDocente = new List<SelectListItem>();
            UnidadAcademicaId = unidadAcademicaId;
            ListaSede = cargarDatos.context.Sede.Select(x => new SelectListItem {
                Text = x.Nombre,
                Value = x.IdSede.ToString()
            }).ToList();
            if (UnidadAcademicaId.HasValue)
                Nivel = cargarDatos.context.UnidadAcademica.Where(x => x.IdUnidadAcademica == UnidadAcademicaId).FirstOrDefault().Nivel;
            else
                Nivel = cargarDatos.context.UnidadAcademica.Where(x => x.IdUnidadAcademica == ParentId).FirstOrDefault().Nivel + 1;
            if (Nivel == 1 )
            {
                ListaCarreraPeriodoAcademico = cargarDatos.context.CarreraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).Select(x => new SelectListItem
                {
                    Text = x.Carrera.NombreEspanol,
                    Value = x.IdCarreraPeriodoAcademico.ToString()
                }).ToList();
            }else if(Nivel ==3)
            {
                ListaCursosPeriodoAcademico = cargarDatos.context.CursoPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Curso.NombreEspanol != "").Select(x => new SelectListItem
                {
                    Text = x.Curso.NombreEspanol,
                    Value = x.IdCursoPeriodoAcademico.ToString()
                }).ToList();

            }
            if(UnidadAcademicaId.HasValue)
            {

                UnidadAcademica unidadAcademica = cargarDatos.context.UnidadAcademica.Find(UnidadAcademicaId);
                ListaSedesElegidas = cargarDatos.context.SedeUnidadAcademica.Where(x => x.IdUnidadAcademica == unidadAcademica.IdUnidadAcademica).Select(x => x.IdSede.ToString()).ToList();
                Titulo = unidadAcademica.NombreEspanol;

                if (unidadAcademica.Nivel == 1)
                {
                    IdCarreraPeriodoAcademico = unidadAcademica.IdCarreraPeriodoAcademico;
                    DocenteId = cargarDatos.context.SedeUnidadAcademica.Where(y => y.IdUnidadAcademica == UnidadAcademicaId).Select(
                    y => y.UnidadAcademicaResponsable.Where(j => j.IdSedeUnidadAcademica == y.IdSedeUnidadAcademica).Select(
                        k => k.Docente.IdDocente).FirstOrDefault()).FirstOrDefault();
                }

                if (unidadAcademica.Nivel == 3)
                {
                    IdCursoPeriodoAcademico = unidadAcademica.IdCursoPeriodoAcademico;
                }
            }
            
        }

    }
}