﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan
{
    public class MonitorearPlanViewModel
    {
        public Int32 IdPlanAccion { get; set; }
        public List<AccionMejora> LstAccionesMejora { get; set; }

        public MonitorearPlanViewModel()
        {
            //LstAccionesMejoraHallazgo = new List<HallazgoAccionMejora>();
            LstAccionesMejora = new List<AccionMejora>();
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idPlanAccion)
        {
            AbetEntities context = dataContext.context;

            IdPlanAccion = idPlanAccion;

            LstAccionesMejora = context.AccionMejoraPlanAccion.Where(x => x.IdPlanAccion == IdPlanAccion).Select(x => x.AccionMejora).ToList();

        }
    }
}