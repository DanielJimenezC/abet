﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan
{
    public class AccionMejoraDetalle
    {
        public Int32 IdAccionMejora { get; set; }
        public String Codigo { get; set; }
        public AccionMejora AccionMejora { get; set; }
        public Curso Curso { get; set; }
        public Int32 IdCurso { get; set; }
        public String codigoCurso { get; set; }
        public PeriodoAcademico periodoAcademico { get; set; }

        public AccionMejoraDetalle()
        {

        }
    }
}