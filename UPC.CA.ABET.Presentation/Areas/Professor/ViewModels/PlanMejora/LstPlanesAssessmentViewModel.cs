﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan
{
    public class LstPlanesAssessmentViewModel
    {

        public int? IdPlanAccion { get; set; }
        public int? IdConstituyente { get; set; }
        public int? IdInstrumento { get; set; }
        public int? IdOutcome { get; set; }
        public int? IdCurso { get; set; }
        public string Descripcion { get; set; }
        public List<Instrumento> LstInstrumento { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Constituyente> LstConstituyente { get; set; }
        public List<Hallazgo> LstHallazgo { get; set; }
        public List<AccionMejora> LstAccionesMejora { get; set; }


        public LstPlanesAssessmentViewModel()
        {
            LstInstrumento = new List<Instrumento>();
            LstOutcome = new List<Outcome>();
            LstCurso = new List<Curso>();
            LstConstituyente = new List<Constituyente>();
        }
        
        public void CargarDatos(CargarDatosContext dataContext, int idescuela, int? idConstituyente, int? idinstrumento, int? idCurso, int? idcurso,int? IdSubModalidad)
        {
            //TEMPORAL

            IdSubModalidad = 1;

            //IdPeriodoAcademico = idperiodoacademico;
            IdInstrumento = idinstrumento;
            //NombreOutcome = nombreoutcome;
            IdCurso = idcurso;

            LstConstituyente = dataContext.context.Constituyente.ToList();
            LstInstrumento = dataContext.context.Instrumento.ToList();

            //int IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

            int IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == IdSubModalidad).IdPeriodoAcademico;

            var query0 = (from cpa in dataContext.context.CursoPeriodoAcademico
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                          where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && ca.IdEscuela == idescuela
                          select cpa.IdCurso);

            LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x => x.NombreEspanol).ToList();


            LstAccionesMejora = dataContext.context.AccionMejora.Take(5).ToList();
           // LstPlanes = query.OrderBy(x => x.Anio).ThenByDescending(x => x.IdCarrera).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }

    }
}