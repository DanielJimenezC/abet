﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan
{
    public class LstAccionesMejoraViewModel
    {
        public Int32 IdPlanAccion { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdNivelAceptacion { get; set; }
        public PlanAccion planAccion { get; set; }
        //public List<AccionMejoraDetalle> LstAccionesMejora { get; set; }
        public List<AccionMejora> LstAccionesMejoraPorRellenar { get; set; }
       // public List<HallazgoAccionMejora> LstAccionesMejoraHallazgo { get; set; }
        public List<AccionMejora> LstAccionesMejora { get; set; }
        public List<Curso> LstCursos { get; set; }
        public List<Sede> LstSedes { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelesAceptacion { get; set; }
        public List<bool>  Respuestas { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public LstAccionesMejoraViewModel()
        {
            //LstAccionesMejoraHallazgo = new List<HallazgoAccionMejora>();
            LstAccionesMejora = new List<AccionMejora>();
            Respuestas = new List<bool>();
            LstCursos = new List<Curso>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idPlanAccion, Int32 idEscuela, Int32? idCurso, Int32?  idSede, Int32?  idNivelAceptacion, Int32? idSubModalidadPeriodoAcademico)
        {
         /* AbetEntities context =   dataContext.context;
            IdCurso = idCurso;
            IdPeriodoAcademico = PeriodoAcademicoId;
            IdPlanAccion = idPlanAccion;
            IdSede = idSede;
            IdNivelAceptacion = idNivelAceptacion;

            LstAccionesMejoraPorRellenar = dataContext.context.AccionMejora.Where( x => (x.Codigo.Substring(0, 3) == "IFC" && (x.IdPeriodoAcademico == null || x.IdCurso == null)) || (false)).ToList();

            for (int i = 0; i < LstAccionesMejoraPorRellenar.Count; i++)
            {
                String codigoCurso = LstAccionesMejoraPorRellenar[i].Codigo.Split('-')[1];
                String codigoPA = LstAccionesMejoraPorRellenar[i].Codigo.Split('-')[2];
                Curso curso = context.Curso.FirstOrDefault(x => x.Codigo == codigoCurso);
                PeriodoAcademico pa = context.PeriodoAcademico.FirstOrDefault(x => x.CicloAcademico == codigoPA);
                if (curso != null && pa != null)
                {
                    if(LstAccionesMejoraPorRellenar[i].IdCurso==null)
                        LstAccionesMejoraPorRellenar[i].IdCurso = curso.IdCurso;
                    if (LstAccionesMejoraPorRellenar[i].IdPeriodoAcademico == null)
                        LstAccionesMejoraPorRellenar[i].IdPeriodoAcademico = pa.IdPeriodoAcademico;

                    context.SaveChanges();
                }
            }

            //LstAccionesMejora = dataContext.context.AccionMejora.Where(x => x.Codigo.Substring(0, 3) == "IFC" && x.IdPeriodoAcademico.HasValue && x.IdCurso.HasValue ).ToList();
            //var LstAccionesMejoraHallazgo = dataContext.context.HallazgoAccionMejora.Where(x => x.AccionMejora.Codigo.Substring(0, 3) == "IFC").ToList();
            //var LstAccionesMejoraHallazgo = (from ham in context.HallazgoAccionMejora select ham).ToList();
            var query = (from ham in context.HallazgoAccionMejora
                         join am in context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                         join h in context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                         where am.IdCurso.HasValue
                         && (!IdCurso.HasValue || (IdCurso.HasValue && h.IdCurso == IdCurso) )
                         && (!IdSede.HasValue || (IdSede.HasValue && h.IdSede == IdSede))
                         && (!IdNivelAceptacion.HasValue || (IdNivelAceptacion.HasValue && h.IdNivelAceptacionHallazgo == IdNivelAceptacion))
                          && (!IdPeriodoAcademico.HasValue || (IdPeriodoAcademico.HasValue && h.IdPeriodoAcademico == IdPeriodoAcademico))
                         select am);

            

            List<AccionMejora> LstAccionesMejoraHallazgo = query.ToList();
            List<int> lst = new List<int>();
            lst = LstAccionesMejoraHallazgo.Select(x => x.IdAccionMejora).ToList();
            LstAccionesMejora = dataContext.context.AccionMejora.Where(x => lst.Contains(x.IdAccionMejora)).ToList();

         //  List<int> lst = new List<int>();
           lst = dataContext.context.AccionMejoraPlanAccion.Where(x => x.IdPlanAccion == IdPlanAccion).Select(x => x.IdAccionMejora.Value).ToList();



            for (int i=0; i<LstAccionesMejora.Count; i++)
            {
                int IdAccion = LstAccionesMejora[i].IdAccionMejora;
               

                if (lst.Contains(IdAccion))
                    Respuestas.Add(true);
                else
                    Respuestas.Add(false);

            }

            var query0 = (from cpa in dataContext.context.CursoPeriodoAcademico
                   join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                   join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                   where  ca.IdEscuela == escuelaId
                   select cpa.IdCurso);

            LstCursos = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x=>x.NombreEspanol).ToList();
            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            LstSedes = dataContext.context.Sede.ToList();
            LstNivelesAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();

   */

        }
    }
}