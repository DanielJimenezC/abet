﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.AssessmentPlan
{
    public class AddEditAssessmentPlanViewModel
    {
        public Int32 IdPlanAccion { get; set; }
        public List<int> anios { get; set; }
        public SelectList LstAnios { get; set; }
        public Int32 Anio { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public List<Carrera> LstCarreras { get; set; }
        
        public SubModalidadPeriodoAcademico SSubModalidadPeriodoAcademico { get; set; }
        public AddEditAssessmentPlanViewModel()
        {
            anios = new List<int>();
            LstCarreras = new List<Carrera>();
        }

        public void Fill(CargarDatosContext dataContext, Int32 SubModalidadPeriodoAcademicoId, Int32 EscuelaId, Int32 idPlanAccion)
        {
            IdSubModalidadPeriodoAcademico = SubModalidadPeriodoAcademicoId;
            SSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico);
            int anioActual = SSubModalidadPeriodoAcademico.PeriodoAcademico.FechaInicioPeriodo.Value.Year;
            IdPlanAccion = idPlanAccion;

            for (int i=0; i<=2; i++)
            {
                anios.Add(anioActual + i);
            }
            LstAnios = new SelectList(anios);

            LstCarreras = dataContext.context.Carrera.Where(x => x.IdEscuela == EscuelaId).ToList();
        }
    }
}