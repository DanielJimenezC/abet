using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class CreateCareerCurriculumViewModel
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string IconoFormacion { get; set; }
        public string IconoBlank { get; set; }
        public string IconoBorrar { get; set; }
        public Int32 IdAcreditadora { get; set; }
        public int IdMallaCocos { get; set; }
        public int IdComision { get; set; }
        public string CodigoComision { get; set; }
        public int IdCarrera { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdModalidad { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public int IdMallaCurricular { get; set; }
        public List<NivelViewModel> Niveles { get; set; }
        public List<TipoOutcomeCursoViewModel> TipoOutcomeCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableComisiones { get; set; }
        public IEnumerable<SelectListItem> AvailableOutcomes { get; set; }
        public IEnumerable<SelectListItem> ListPeriodoAcadmico { get; set; }
        public IEnumerable<SelectListItem> ListModalidad { get; set; }

    }
}