﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class ConfigOutcomeMallaCocosDetalleRubricaViewModel
    {
        public MallaCocos MallaCocos { get; set; }
        public Int32 IdMallaCocos { get; set; }
        public List<MallaCocosDetalle> ListMallaCocosDetalle { get; set; }
        public List<CursoMallaCurricular> ListCursoMallaCurricular { get; set; }
        public List<OutcomeComision> LstOutcomeComision { get; set; }

        public ConfigOutcomeMallaCocosDetalleRubricaViewModel()
        {
            ListMallaCocosDetalle = new List<MallaCocosDetalle>();
            ListCursoMallaCurricular = new List<CursoMallaCurricular>();
            LstOutcomeComision = new List<OutcomeComision>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32 IdMallaCocos)
        {
            this.MallaCocos = dataContext.context.MallaCocos.Include(x => x.MallaCocosDetalle).FirstOrDefault(x => x.IdMallaCocos == IdMallaCocos);
            var query = MallaCocos.MallaCocosDetalle.Where(x => x.CursoMallaCurricular.NivelAcademico.Numero == 9 || x.CursoMallaCurricular.NivelAcademico.Numero == 10 && !x.CursoMallaCurricular.EsElectivo.Value);
            this.ListMallaCocosDetalle = query.Where(x => x.CursoMallaCurricular.Curso.NombreEspanol.Contains("Taller de Proyecto")).ToList();

            var lstMallacocosDetalleId = ListMallaCocosDetalle.Select(x => x.IdMallaCocosDetalle).ToList();
            this.LstOutcomeComision = dataContext.context.OutcomeComision.Where(x => x.MallaCocosDetalle.Any(m => lstMallacocosDetalleId.Contains(m.IdMallaCocosDetalle))).Distinct().ToList();
        }
    }
}