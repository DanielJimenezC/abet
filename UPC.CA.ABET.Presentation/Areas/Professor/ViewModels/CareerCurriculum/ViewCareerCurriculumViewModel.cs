using System.Collections.Generic;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class ViewCareerCurriculumViewModel
    {
        public int IdMallaCocos { get; set; }
        public int CantidadOutcomes { get; set; }
        public string IconoFormacion { get; set; }
        public string IconoBlank { get; set; }
        public string IconoBorrar { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public List<NivelViewModel> Niveles { get; set; }
        public List<TipoOutcomeCursoViewModel> TipoOutcomeCursos { get; set; }
    }
}