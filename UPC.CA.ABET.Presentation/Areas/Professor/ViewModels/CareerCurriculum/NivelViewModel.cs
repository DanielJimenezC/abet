using System.Collections.Generic;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class NivelViewModel
    {
        public bool EsNivelDeElectivos { get; set; }
        public string Numero { get; set; }
        public List<Outcome> Outcomes { get; set; }
        public List<CursoMallaCurricularViewModel> CursoMallaCurriculares { get; set; }
    }
}