namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class MallaCocosViewModel
    {
        public int IdMallaCocos { get; set; }
        public string Descripcion { get; set; }
        public string Ciclo { get; set; }
        public string Carrera { get; set; }
    }
}