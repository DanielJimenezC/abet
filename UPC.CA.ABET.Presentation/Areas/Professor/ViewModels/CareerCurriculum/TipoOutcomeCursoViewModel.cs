namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class TipoOutcomeCursoViewModel
    {
        public int IdTipoOutcomeCurso { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
    }
}