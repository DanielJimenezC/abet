using System.Collections.Generic;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class CursoMallaCurricularViewModel
    {
        public CursoMallaCurricular CursoMallaCurricular { get; set; }
        public int IdCursoMallaCurricular { get; set; }
        public bool EsFormacion { get; set; }
        public List<OutcomeViewModel> Outcomes { get; set; }
    }
}