namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class OutcomeViewModel
    {
        public int IdOutcome { get; set; }
        public int IdTipoOutcomeCurso { get; set; }
        public string Icono { get; set; }
        public int OriginalValue { get; set; }
        public bool esCalificado;
        public int mallaCocosDetalleId { get; set; }
    }
}