using System.Collections.Generic;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.CareerCurriculum
{
    public class ConsultCareerCurriculumViewModel
    {
        public IEnumerable<SelectListItem> AvailableComisiones { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableModulo { get; set; }
        public IEnumerable<SelectListItem> AvailableCarreras { get; set; }
        public IEnumerable<SelectListItem> ListAcreditadoras { get; set; }

        public List<SelectListItem> Modal { get; set; }
        public int IdComision { get; set; }
        public int IdCarrera { get; set; }
        public string CodigoComision { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public int IdModalidad { get; set; }
        public int IdAcreditadora { get; set; }

        public int parConsIdPeriodoAcademico { get; set; }
        public int parConsIdAcreditadora { get; set; }
        public int parConsIdCarrera { get; set; }
        public int parConsIdComision { get; set; }

        public List<MallaCocosViewModel> Resultados { get; set; }
        public ConsultCareerCurriculumViewModel()
        {
            Resultados = new List<MallaCocosViewModel>();
        }
    }
}