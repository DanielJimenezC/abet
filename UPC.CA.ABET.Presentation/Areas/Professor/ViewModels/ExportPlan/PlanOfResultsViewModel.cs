﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Admin.Resources.Views.Report;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ExportPlan
{
    public class PlanOfResultsViewModel
    {

        public Int32 Anio { get; set; }
        public Int32 IdModalidad { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdInstrumento { get; set; }
        public string Comision { get; set; }
        public string Lenguaje { get; set; }
        public Int32 IdCostituyente { get; set; }
        public List<SelectListItem> LstCarrera { get; set; }
        public List<SelectListItem> LstAnio { get; set; }
        public List<SelectListItem> LstModalidad { get; set; }
        public List<SelectListItem> LstInstrumento { get; set; }
        public List<SelectListItem> LstComision { get; set; }
        public List<SelectListItem> LstConstituyente { get; set; }
        public List<SelectListItem> LstTipoCorte { get; set; }
        public bool HasValue { get; set; }
        public int Idtipocorte { get; set; }
        public int planid { get; set; }

        public PlanOfResultsViewModel()
        {
            LstCarrera = new List<SelectListItem>();
            LstAnio = new List<SelectListItem>();
            LstModalidad = new List<SelectListItem>();
            LstComision = new List<SelectListItem>();
            LstInstrumento = new List<SelectListItem>();
            LstConstituyente = new List<SelectListItem>();
            LstTipoCorte = new List<SelectListItem>();
        }

        public void Fill(CargarDatosContext dataContext,int ModalidadId,int EscuelaId)
        {
            AbetEntities context = dataContext.context;
            LstAnio = (from a in context.PlanMejora
                       join b in context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                       join c in context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                       where c.IdModalidad==ModalidadId && a.IdEscuela==EscuelaId
                       select new SelectListItem
                       {
                           Value = a.IdPlanMejora.ToString(),
                           Text = a.Anio.ToString()
                       }).Distinct().ToList();

            if (LstAnio.Count() > 0)
            {
                Anio = Convert.ToInt32(LstAnio.FirstOrDefault().Text);
            }

            LstTipoCorte.Insert(0, (new SelectListItem { Text = "Por Outcome", Value = "0", Selected = true }));
            LstTipoCorte.Insert(1, (new SelectListItem { Text = "Por Constituyente", Value = "1", Selected = false }));


            LstModalidad = (from m in context.Modalidad
                            select new SelectListItem
                            {
                                Value = m.IdModalidad.ToString(),
                                Text = m.NombreEspanol,
                            }).Distinct().ToList();

            LstCarrera = (from c in context.Carrera
                          join sm in context.SubModalidad on c.IdSubmodalidad equals sm.IdSubModalidad
                          join m in context.Modalidad on sm.IdModalidad equals m.IdModalidad
                          where m.IdModalidad==ModalidadId && c.IdEscuela==EscuelaId
                          select new SelectListItem
                          {
                              Value = c.IdCarrera.ToString(),
                              Text = c.NombreEspanol
                          }).Distinct().ToList();



            LstInstrumento = (
                                from pm in context.PlanMejora
                                join pma in context.PlanMejoraAccion on pm.IdPlanMejora equals pma.IdPlanMejora
                                join am in context.AccionMejora on pma.IdAccionMejora equals am.IdAccionMejora
                                join ham in context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                                join h in context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                                join ci in context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                                join i in context.Instrumento on ci.IdInstrumento equals i.IdInstrumento
                                where pm.IdEscuela==EscuelaId
                                select new SelectListItem
                                {
                                    Value = i.IdInstrumento.ToString(),
                                    Text = i.Acronimo
                                }).Distinct().ToList();

            LstConstituyente = (from pm in context.PlanMejora
                                join pma in context.PlanMejoraAccion on pm.IdPlanMejora equals pma.IdPlanMejora
                                join am in context.AccionMejora on pma.IdAccionMejora equals am.IdAccionMejora
                                join ham in context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                                join h in context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                                join ci in context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                                join i in context.Instrumento on ci.IdInstrumento equals i.IdInstrumento
                                join cons in context.Constituyente on ci.IdConstituyente equals cons.IdConstituyente
                                where pm.IdEscuela == EscuelaId
                                select new SelectListItem
                                {
                                    Value = cons.IdConstituyente.ToString(),
                                    Text = cons.NombreEspanol
                                }).Distinct().ToList();






            if (LstCarrera.Count() > 0)
            {
                int firstcarrera = Convert.ToInt32(LstCarrera[0].Value);

                var lstcomision = (from a in context.Comision
                                   join b in context.CarreraComision on a.IdComision equals b.IdComision
                                   join c in context.Carrera on b.IdCarrera equals c.IdCarrera
                                   where c.IdCarrera == firstcarrera && a.Codigo != "WASC" && c.IdEscuela==EscuelaId
                                   select new
                                   {
                                       codigo = a.Codigo
                                   }).ToList();
                lstcomision = lstcomision.Distinct().ToList();

                foreach (var comision in lstcomision)
                {
                    LstComision.Add(new SelectListItem { Value = comision.codigo, Text = comision.codigo });
                }
            }

            LstInstrumento.Insert(0, new SelectListItem { Value = "0", Text = "TODOS" });
            LstConstituyente.Insert(0, new SelectListItem { Value = "0", Text = "TODOS" });


        }



    }
}


