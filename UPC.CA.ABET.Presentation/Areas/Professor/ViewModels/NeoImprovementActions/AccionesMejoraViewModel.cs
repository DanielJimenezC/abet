﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class AccionesMejoraViewModel
    {
        public string descripcionHallazgo { get; set; }
        public string criticidad { get; set; }
        public string HallazgoID { get; set; }
        public string CodHallazgo { get; set; }
        public string AccionDeMejora { get; set; }
        public string CodAccionMejora { get; set; }

        public List<AccionMejora> Amejora { get; set; }

        public void CargarData(int idHllazgo, AbetEntities context)
        {
            Amejora = (from h in context.Hallazgos
                                join ham in context.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                                join am in context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                where h.IdHallazgo== idHllazgo
                                select am).Distinct().ToList();
            descripcionHallazgo = (from h in context.Hallazgos
                                   where h.IdHallazgo == idHllazgo
                                   select h.DescripcionEspanol).FirstOrDefault();


            return;
        }
    }
}