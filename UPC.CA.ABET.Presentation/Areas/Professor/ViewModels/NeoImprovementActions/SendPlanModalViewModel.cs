﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Professor.NeoImprovementActions;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class SendPlanModalViewModel
    {
       
        public List<SelectListItem> LstCarrera { get; set; }
        public int? IdCarrera { get; set; }
        public int? IdPeridoAcademico { get; set; }
        public int? IdSubModalidadPeriodoAcad { get; set; }
        public int? IdPlan { get; set; }
        public List<SelectListItem> PlanList { get; set; }
        public List<CoordinadorNotificacionMail> lstCoordinador { get; set; }
        public ConfiguracionNotificacion objConfiguracionNotificacion { get; set; }
        public List<SelectListItem> LstPeridoAcademico { get; set; }

        public SendPlanModalViewModel()
        {
            LstCarrera = new List<SelectListItem>();
            LstPeridoAcademico = new List<SelectListItem>();
            PlanList = new List<SelectListItem>();
        }

        public void fill(CargarDatosContext dataContext, int idescuela, int? IdSubModalidadPeriodoAcademico, int idsubmodalidad, int idplan,int modalidadid)
        {
            
            lstCoordinador = new List<CoordinadorNotificacionMail>();
            LstCarrera = (from a in dataContext.context.Carrera                       
                          where a.IdSubmodalidad == idsubmodalidad && a.IdEscuela == idescuela
                          select new SelectListItem { Text = a.NombreEspanol, Value = a.IdCarrera.ToString() }).OrderBy(x => x.Text).Distinct().ToList();
            
            IdSubModalidadPeriodoAcad = IdSubModalidadPeriodoAcademico;
            int? smpaid = null;
            dataContext.context.CarreraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Carrera.IdEscuela == idescuela).Select(x => x.Carrera).ToList();
            if (idplan == 0)
            { 
                PlanList = (from a in dataContext.context.PlanMejora
                            join smpa in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                            join sm in dataContext.context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                            join m in dataContext.context.Modalidad on sm.IdModalidad equals m.IdModalidad
                            where m.IdModalidad == modalidadid && a.IdEscuela == idescuela
                            select new SelectListItem { Text = a.Nombre, Value = a.IdPlanMejora.ToString() }).ToList();
                smpaid = IdSubModalidadPeriodoAcademico;
           
            }
            else
            {
                PlanList = (from a in dataContext.context.PlanMejora
                            where a.IdPlanMejora == idplan /*&& a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico*/
                            select new SelectListItem { Text = a.Nombre, Value = a.IdPlanMejora.ToString() }).ToList();
                
            }

            if(PlanList.Count() > 0)
            IdPlan = Convert.ToInt32(PlanList[0].Value);

            if (idplan > 0)
            {
                smpaid = dataContext.context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == idplan).SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
            }


            var TempCoor = dataContext.context.FuncionObtCursoCoordinador(smpaid, Convert.ToInt32(LstCarrera[0].Value), IdPlan);
            foreach (var info in TempCoor)
            {

                CoordinadorNotificacionMail am = new CoordinadorNotificacionMail();
                am.Idcurso = info.idcurso;
                am.Carrera = info.Carrera;
                am.CodCurso = info.CodCurso;
                am.Curso = info.Curso;
                am.CodCoor = info.CodCoor;
                am.Correo = am.getEmail();
                am.Coordinador = info.Coordinador;
                am.IdSubModalidadPeriodoAcademico = (int)info.IdSubModalidadPeriodoAcademico;

                lstCoordinador.Add(am);
            }


        }
    }
}