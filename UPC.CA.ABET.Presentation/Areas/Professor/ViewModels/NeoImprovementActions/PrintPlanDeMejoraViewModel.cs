﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class HallazgoAccionMejoraPDF
    {
        public string amCodigo { get; set; }
        public string amDescripcionEspanol { get; set; }
        public string hCodigo { get; set; }
        public string hDescripcionEspanol { get; set; }
        public int IdInstrumento { get; set; }
        public string hOutcome { get; set; }
        public int? CursoID { get; set; }

    }

    public class PrintPlanDeMejoraViewModel
    {
        public int? Anio { get; set; }
        public int? IdCarrera { get; set; }
        public int? IdComision { get; set; }
        public int IdPeriodoAcademico { get; set; }

        public List<HallazgoAccionMejoraPDF> Lsthams { get; set; }


        //public List<Constituyente> LstConstituyente { get; set; }
        //public List<Instrumento> LstInstrumento { get; set; }
        //public List<int> LstInstrumentoID { get; set; }

        public Carrera carrera { get; set; }
        public Comision comision { get; set; }

        public PrintPlanDeMejoraViewModel()
        {

            //LstConstituyente = new List<Constituyente>();
            //LstInstrumento = new List<Instrumento>();
            //LstInstrumentoID = new List<int>();
            carrera = new Carrera();
            comision = new Comision();
            Lsthams = new List<HallazgoAccionMejoraPDF>();
        }

        public void CargarDatos(CargarDatosContext dataContext, int anio, int idCarrera, int idComision)
        {
            IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

            Anio = anio + 1;
            IdCarrera = idCarrera;
            IdComision = idComision;

            carrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera);
            comision = dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision);

            string codigocomision = comision.Codigo;

            /* var query = (from ham in dataContext.context.HallazgoAccionMejora 
                          join am in dataContext.context.AccionMejora  on ham.IdAccionMejora equals am.IdAccionMejora
                          join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                          join oc in dataContext.context.OutcomeComision on h.IdOutcome equals oc.IdOutcome
                          join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                          where (am.Estado != "INA" && h.Estado != "INA" && h.IdCarrera == IdCarrera &&
                                     am.Anio == Anio && c.Codigo == codigocomision)
                          select ham).Distinct().ToList();*/

            var query = (from ham in dataContext.context.HallazgoAccionMejora
                         join am in dataContext.context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                         join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                         join oc in dataContext.context.OutcomeComision on h.IdOutcome equals oc.IdOutcome
                         join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                         where (am.Estado != "INA" && (h.Estado == null || h.Estado != "INA") && h.IdCarrera == IdCarrera
                            && am.Anio == anio && c.Codigo == codigocomision && am.ParaPlan)
                         select new HallazgoAccionMejoraPDF { amCodigo = am.Codigo, amDescripcionEspanol = am.DescripcionEspanol, hCodigo = h.Codigo, hDescripcionEspanol = h.DescripcionEspanol, IdInstrumento = h.IdInstrumento, hOutcome = h.Outcome.Nombre, CursoID = h.IdCurso}).Distinct().OrderBy(h=>h.hOutcome).ToList();

            Lsthams = query;



        }

        public string getAnio()
        {
            return Anio.ToString();
        }

        public string getCarreraNombre()
        {
            if (IdCarrera.HasValue)
                return carrera.NombreEspanol;
            else
                return "Sin especificar";
        }

        public string getComisionNombre()
        {
            if (IdComision.HasValue)
                return comision.NombreEspanol;
            else
                return "Sin especificar";
        }

    }
}