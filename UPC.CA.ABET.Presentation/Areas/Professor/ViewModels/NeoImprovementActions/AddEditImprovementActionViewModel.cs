﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class AddEditImprovementActionViewModel
    {
        public Int32? IdAccionMejora { get; set; }
        public AccionMejora accionmejora { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string ComentarioEspanol { get; set; }
        public string ComentarioIngles { get; set; }

        public string DescripcionHallazgo { get; set; }

        public List<Constituyente> LstConstituyentes { get; set; }
        public List<SelectListItem> LstOutcome { get; set; }
        public List<SelectListItem> LstArea { get; set; }
        public List<SelectListItem> LstPeriodoAcademico { get; set; }
        public List<SelectListItem> LstComision { get; set; }
        public string lstChecks { get; set; }

        public Int32? IdConstituyente { get; set; }
        public Int32? IdInstrumento { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdArea { get; set; }
        public Int32? IdComision { get; set; }


        public List<Instrumento> LstInstrumentos { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }
        //public IEnumerable<SelectListItem> LstHallazgos { get; set; }
        public List<Hallazgos> hallazgosRelacionados { get; set; }
       // public List<Int32> HallazgoId { get; set; }
        public int Anio { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public List<Hallazgo> LstHallazgos { get; set; }
        public List<bool> LstMarcados { get; set; }
        public int cantidadAsaltear { get; set; }

        public AddEditImprovementActionViewModel()
        {
            Codigo = "";
            LstConstituyentes = new List<Constituyente>();
            LstArea = new List<SelectListItem>();
            LstInstrumentos = new List<Instrumento>();
            LstCriticidad = new List<Criticidad>();
            LstOutcome = new List<SelectListItem>();
            LstComision = new List<SelectListItem>();
            //HallazgoId = new List<int>();
            LstHallazgos = new List<Hallazgo>();
            hallazgosRelacionados = new List<Hallazgos>();
            LstMarcados = new List<bool>();
            cantidadAsaltear = 0;
           
            LstPeriodoAcademico = new List<SelectListItem>();  
        }


        public void CargarDatos(CargarDatosContext dataContext, Int32? idAccionMejora, Int32 idSubModalidadPeriodoAcademico,Int32 idmodalidad, Int32 idEscuela)
        {
            AbetEntities context = dataContext.context;
            IdAccionMejora = idAccionMejora;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            LstConstituyentes = dataContext.context.Constituyente.ToList();
            string CodCom ="";
            //LstInstrumentos = dataContext.context.Instrumento.OrderBy(x => x.Acronimo).ToList();
            LstInstrumentos = dataContext.context.Instrumento.Where(x => x.ParaPlan == true).OrderBy(x => x.IdInstrumento).ToList();
            if (idmodalidad.ToString() == ConstantHelpers.MODALITY.PREGRADO_EPE)
            {
                LstInstrumentos.Remove(context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.INSTRUMENTOS_ACRONIMOS.PPP));
            }
            string anio = (from a in context.SubModalidadPeriodoAcademico
                          join b in context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                          where a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                          select b.CicloAcademico).FirstOrDefault();
                            //anio = anio.Substring(0, 4);

            //LstCursos = (from cpa in context.CursoPeriodoAcademico
            //              join ccpa in context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
            //              join c in context.Curso on cpa.IdCurso equals c.IdCurso
            //              join ca in context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
            //              where cpa.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && ca.IdEscuela == escuelaId && c.NombreEspanol != ""
            //              select new SelectListItem {Text = c.NombreEspanol, Value = c.IdCurso.ToString() }).Distinct().OrderBy(x => x.Text).ToList();

            IdPeriodoAcademico = (from a in context.PeriodoAcademico
                       join s in context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals s.IdPeriodoAcademico
                       where a.CicloAcademico.Contains(anio) && s.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                       select a).OrderByDescending(x=>x.IdPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;

            LstPeriodoAcademico = (from pa in context.PeriodoAcademico
                                   join spa in context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals spa.IdPeriodoAcademico
                                   join cpa in context.CarreraPeriodoAcademico on spa.IdSubModalidadPeriodoAcademico equals cpa.IdSubModalidadPeriodoAcademico
                                   join ca in context.Carrera on cpa.IdCarrera equals ca.IdCarrera
                                   join s in context.SubModalidad on spa.IdSubModalidad equals s.IdSubModalidad
                                   join m in context.Modalidad on s.IdModalidad equals m.IdModalidad
                                   where m.IdModalidad == idmodalidad && pa.CicloAcademico.Contains(anio)
                                   select new SelectListItem {
                                       Value = pa.IdPeriodoAcademico.ToString(),
                                       Text = pa.CicloAcademico,
                                       Selected = (pa.IdPeriodoAcademico == IdPeriodoAcademico)
                                   }).Distinct().OrderByDescending(x => x.Text).ToList();


            LstComision = (from cpa in context.CarreraPeriodoAcademico
                           join car in context.Carrera on cpa.IdCarrera equals car.IdCarrera
                           join es in context.Escuela on car.IdEscuela equals es.IdEscuela
                           join cc in context.CarreraComision on car.IdCarrera  equals cc.IdCarrera
                           join co in context.Comision on cc.IdComision equals co.IdComision
                           join apa in context.AcreditadoraPeriodoAcademico on co.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                           join acre in context.Acreditadora on apa.IdAcreditadora equals acre.IdAcreditadora
                           where apa.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico &&  acre.Nombre == "ABET" &&  es.IdEscuela == idEscuela && cpa.IdSubModalidadPeriodoAcademico == apa.IdSubModalidadPeriodoAcademico
                           select new SelectListItem { Value = co.Codigo, Text =  co.Codigo}).Distinct().ToList();
                          
            if(LstComision.Count() > 0)
            {
                 CodCom = LstComision[0].Value;
            }

            LstArea = (from a in context.UnidadAcademica                      
                       where a.Nivel == 1 && a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && a.IdEscuela == idEscuela
                       select new SelectListItem { Value = a.NombreEspanol, Text = a.NombreEspanol , Selected = true }).Distinct().ToList();


            //LstArea.Insert(0, new SelectListItem { Text = "Sin Área", Value = "-", Selected = true });

            LstOutcome = (from oc in context.OutcomeComision
                          join o in context.Outcome on oc.IdOutcome equals o.IdOutcome
                          join c in context.Comision on oc.IdComision equals c.IdComision
                          where oc.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.Codigo !="WASC" && c.Codigo == CodCom
                          select new SelectListItem {Value = oc.IdOutcomeComision.ToString(), Text = o.Nombre }).Distinct().OrderBy(x=>x.Text).ToList();

            LstOutcome.Insert(0, new SelectListItem { Text = "[- Todos -]", Value = "0" });
           // LstComision.Insert(0, new SelectListItem { Text = "[- Todos -]", Value = "0" });

            string cicloAcademico = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).PeriodoAcademico.CicloAcademico;

            Anio = Int32.Parse(cicloAcademico.Substring(0, 4));
            LstCriticidad = dataContext.context.Criticidad.ToList();
            //List<Hallazgo> hallazgos = new List<Hallazgo>();

            if (IdAccionMejora.HasValue)
            {
                AccionMejora accionmejora = dataContext.context.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == IdAccionMejora);

                Codigo = accionmejora.Codigo;
                Descripcion = accionmejora.DescripcionEspanol;

                hallazgosRelacionados = dataContext.context.HallazgoAccionMejora.Where(x => x.IdAccionMejora == IdAccionMejora).Select(x => x.Hallazgos).Where(y=>y.Estado!="INA").OrderBy(x=>x.IdCriticidad).ToList();

            }
        }
        public void CargarDatosPartial(AbetEntities context, int NumeroPagina, int IdInstrumento, int IdCurso, int IdOutcome, int idEscuela)
        {
            List<int> hallazgosid = new List<int>();

           // if (Solo != 0)
                hallazgosid = context.HallazgoAccionMejora.Select(x => x.IdHallazgo).ToList();

             cantidadAsaltear = (NumeroPagina - 1) * 5;




            var query = (from h in context.Hallazgo
                        join ci in context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
                        join ca in context.Carrera on h.IdCarrera equals ca.IdCarrera
                        where (h.Estado != "INA" && ca.IdEscuela == idEscuela
                        && (IdInstrumento == 0 || h.IdInstrumento == IdInstrumento)
                        && h.IdCurso.HasValue
                        && (IdCurso == 0 || h.IdCurso == IdCurso)
                        && (IdOutcome == 0 || h.IdOutcome == IdOutcome)
                        && (hallazgosid.Contains(h.IdHallazgo) == false))
                        select h).OrderBy(x => x.IdCriticidad).ToList();


            for (int i = 1; i < query.Count; i++)
            {
                if (i == 0)
                    i = 1;

                if (query[i].Codigo == query[i - 1].Codigo)
                {
                    query.RemoveAt(i);
                    i = i - 1;
                }
            }
            

            LstHallazgos = query.Skip(cantidadAsaltear).Take(5).ToList();

            for (int i = 0; i < cantidadAsaltear; i++)
            {
                LstHallazgos.Insert(0, new Hallazgo { IdHallazgo = 0 });
               
            }



            for (int i = 0; i< LstHallazgos.Count; i++)
            {
                LstMarcados.Add(false);
            }
        }

    }
}