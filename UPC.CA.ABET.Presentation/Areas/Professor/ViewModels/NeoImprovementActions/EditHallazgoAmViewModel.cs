﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class EditHallazgoAmViewModel
    {
        public String Text { get; set; }
        public List<Criticidad> Criticidades { get; set; }
        public List<Outcome> lstOutcomes { get; set; }
        public CursoMallaCurricular cursoMallaCurricular { get; set; }
        public string descripcionHallazgo { get; set; }
        public string criticidad { get; set; }
        public string idhallazgo { get; set; }
        public string CodHallazgo { get; set; }
        public int pos { get; set; }
        

        public void CargarData(string codCurso, AbetEntities context)
        {
            var Curso = (context.Curso.Where(x => x.Codigo == codCurso).FirstOrDefault());
            lstOutcomes = new List<Outcome>();
            cursoMallaCurricular = context.CursoMallaCurricular.Where(x => x.IdCurso == Curso.IdCurso).FirstOrDefault();
            Outcome outcome;
            var listaOutcomes = context.GetOutcomes(Curso.IdCurso).ToList();
            for (int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionEspanol;
                lstOutcomes.Add(outcome);
            }

            return;
        }
    }
}