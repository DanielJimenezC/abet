﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class LstAccionesParaPlanViewModel
    {
        public int? Anio { get; set; }
        public int? IdCarrera { get; set; }
        public int? IdComision { get; set; }
        public int? IdConstituyente { get; set; }
        public int? IdInstrumento { get; set; }
        public int? IdCurso { get; set; }
        public string IdOutcome { get; set; }
        public List<int> LstAnios { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<AccionMejora> LstAccionMejora { get; set; }
        public List<Constituyente> LstConstituyentes { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public List<Curso> LstCursos { get; set; }
        public List<SelectListItem> LstOutcomes { get; set; }
        public List<int> CheckBoxSeleccionado { get; set; }

        public LstAccionesParaPlanViewModel()
        {
            LstAccionMejora = new List<AccionMejora>();
            LstAnios = new List<int>();
            LstCarrera = new List<Carrera>();
            LstComision = new List<Comision>();
            LstConstituyentes = new List<Constituyente>();
            LstInstrumentos = new List<Instrumento>();
            LstCursos = new List<Curso>();
            LstOutcomes = new List<SelectListItem>();
        }

        public void fill(CargarDatosContext dataContext, int idescuela, int? anio, int? idcarrera, int? idcomision, int? IdSubModalidadPeriodoAcademico, int? idConstituyente , int? idInstrumento , int? idCurso , string idOutcome, 
            List<int> result)
        {
            //int IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
         
            //int IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico
            //                                     .FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == IdSubModalidad)
            //                                     .IdSubModalidadPeriodoAcademico;

            Anio = anio;
            IdCarrera = idcarrera;
            IdComision = idcomision;
            IdConstituyente = idConstituyente;
            IdInstrumento = idInstrumento;
            IdCurso = idCurso;
            IdOutcome = idOutcome;

            int canio = 2016;
            int aniomax = DateTime.Now.Year;

            if (Anio.HasValue == false)
            {
                Anio = aniomax;
            }

            while (true)
            {
                LstAnios.Add(canio);
                canio = canio + 1;

                if (canio > aniomax)
                    break;
            }

            LstCarrera = dataContext.context.CarreraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Carrera.IdEscuela == idescuela).Select(x => x.Carrera).ToList();
            LstConstituyentes = (from cons in dataContext.context.Constituyente select cons).OrderBy(x => x.NombreEspanol).ToList();
            if (IdConstituyente.HasValue)
            {
                //LstInstrumentos = (from ins in dataContext.context.Instrumento
                //                   join h in dataContext.context.Hallazgo on ins.IdInstrumento equals h.IdInstrumento
                //                   join cur in dataContext.context.Curso on h.IdCurso equals cur.IdCurso
                //                   select ins).Distinct().OrderBy(x => x.NombreEspanol).ToList();

                LstInstrumentos = (from h in dataContext.context.Hallazgo
                              join ins in dataContext.context.Instrumento on h.IdInstrumento equals ins.IdInstrumento
                              join cons in dataContext.context.Constituyente on h.IdConstituyente equals cons.IdConstituyente
                              where cons.IdConstituyente == IdConstituyente && h.IdCarrera == IdCarrera && h.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                              select ins).Distinct().OrderBy(x => x.Acronimo).ToList();
            }

            if (idcarrera.HasValue)
            {
                LstCursos = (from cur in dataContext.context.Curso
                             join h in dataContext.context.Hallazgo on cur.IdCurso equals h.IdCurso
                             where h.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                             && h.IdCarrera == idcarrera
                             select cur).Distinct().OrderBy(x => x.NombreEspanol).ToList();
            }

            LstOutcomes = (from oc in dataContext.context.OutcomeComision
                        join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
                        join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                        where oc.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.Codigo != "WASC"
                        select new SelectListItem { Value = o.Nombre, Text = o.Nombre }).Distinct().OrderBy(x => x.Text).ToList();
            if (IdCarrera.HasValue)
            {
                LstComision = dataContext.context.CarreraComision.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdCarrera == IdCarrera).Select(x => x.Comision).Where(y => y.Codigo != "WASC").ToList();
                Comision comision = dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision);
                string codigocomision;
                if (comision != null)
                    codigocomision = comision.Codigo;
                else
                    codigocomision = "";


                var globalList = from ham in dataContext.context.HallazgoAccionMejora
                                 join am in dataContext.context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                 join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                                 join oc in dataContext.context.OutcomeComision on h.IdOutcome equals oc.IdOutcome
                                 join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                                 join cons in dataContext.context.Constituyente on h.IdConstituyente equals cons.IdConstituyente
                                 join i in dataContext.context.Instrumento on h.IdInstrumento equals i.IdInstrumento
                                 join cur in dataContext.context.Curso on h.IdCurso equals cur.IdCurso
                                 join o in dataContext.context.Outcome on h.IdOutcome equals o.IdOutcome
                                 select new { ham = ham , am = am , h = h, oc = oc, c = c , cons = cons, i = i , cur = cur , o = o};
                
                if (!idConstituyente.HasValue && !idInstrumento.HasValue && !idCurso.HasValue && String.IsNullOrEmpty(idOutcome) )
                {
                    var query = (from ham in dataContext.context.HallazgoAccionMejora
                                 join am in dataContext.context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                 join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                                 join oc in dataContext.context.OutcomeComision on h.IdOutcome equals oc.IdOutcome
                                 join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                                 where (am.Estado != "INA" && (h.Estado == null || h.Estado != "INA") && h.IdCarrera == IdCarrera
                                    && am.Anio == anio && c.Codigo == codigocomision)
                                 select am).Distinct().ToList();
                


                    LstAccionMejora = query;
                }
                ////////////////1
                if (idConstituyente.HasValue && !idInstrumento.HasValue && !idCurso.HasValue && String.IsNullOrEmpty(idOutcome)) {
                    LstAccionMejora = (from g in globalList                                 
                                 where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                    && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente)
                                 select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && idInstrumento.HasValue && !idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                 where (g.am.Estado != "INA"  && g.h.IdCarrera == IdCarrera
                                    && g.am.Anio == anio && g.c.Codigo == codigocomision && g.i.IdInstrumento == IdInstrumento)
                                 select g.am).Distinct().ToList();                   
                }
                if (!idConstituyente.HasValue && !idInstrumento.HasValue && idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA"  && g.h.IdCarrera == IdCarrera
                                          &&  g.am.Anio == anio && g.c.Codigo == codigocomision && g.cur.IdCurso == IdCurso)
                                       select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && !idInstrumento.HasValue && !idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                          && g.am.Anio == anio && g.c.Codigo == codigocomision && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                ////////////////2
                if (idConstituyente.HasValue && idInstrumento.HasValue && !idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                          && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente  
                                          && g.i.IdInstrumento == IdInstrumento)
                                       select g.am).Distinct().ToList();
                }
                if (idConstituyente.HasValue && !idInstrumento.HasValue && idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.cur.IdCurso == IdCurso)
                                       select g.am).Distinct().ToList();
                }
                if (idConstituyente.HasValue && !idInstrumento.HasValue && !idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && idInstrumento.HasValue && idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cur.IdCurso== IdCurso
                                         && g.i.IdInstrumento == IdInstrumento)
                                       select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && idInstrumento.HasValue && !idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.o.Nombre == IdOutcome
                                         && g.i.IdInstrumento == IdInstrumento)
                                       select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && !idInstrumento.HasValue && idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cur.IdCurso == IdCurso
                                         && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                ////////////////3
                if (idConstituyente.HasValue && idInstrumento.HasValue && idCurso.HasValue && String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.i.IdInstrumento == IdInstrumento && g.cur.IdCurso == IdCurso)
                                       select g.am).Distinct().ToList();
                }
                if (idConstituyente.HasValue && idInstrumento.HasValue && !idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA"  && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.i.IdInstrumento == IdInstrumento && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                if (idConstituyente.HasValue && !idInstrumento.HasValue && idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.cur.IdCurso == IdCurso && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                if (!idConstituyente.HasValue && idInstrumento.HasValue && idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.i.IdInstrumento == IdInstrumento
                                         && g.cur.IdCurso == IdCurso && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }
                ////////////////4
                if (idConstituyente.HasValue && idInstrumento.HasValue && idCurso.HasValue && !String.IsNullOrEmpty(idOutcome))
                {
                    LstAccionMejora = (from g in globalList
                                       where (g.am.Estado != "INA" && g.h.IdCarrera == IdCarrera
                                         && g.am.Anio == anio && g.c.Codigo == codigocomision && g.cons.IdConstituyente == IdConstituyente
                                         && g.i.IdInstrumento == IdInstrumento && g.cur.IdCurso == IdCurso && g.o.Nombre == IdOutcome)
                                       select g.am).Distinct().ToList();
                }

            }

            if (result != null)
            {
                if (result.Count() > 0)
                {
                    CheckBoxSeleccionado = result;
                }
                else
                {
                    CheckBoxSeleccionado = LstAccionMejora.Select(x => x.IdAccionMejora).ToList();
                }
                
            }
            else
            {
                CheckBoxSeleccionado = LstAccionMejora.Select(x => x.IdAccionMejora).ToList(); ;
            }
        }
    }
}