﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.NeoImprovementActions
{
    public class LstAccionesMejoraViewModel
    {
        public int? IdConstituyente { get; set; }
        public int? IdInstrumento { get; set; }
        public int? IdOutcome { get; set; }
        public int? IdCurso { get; set; }
        public string Outcomenombre { get; set; }
        public List<Instrumento> LstInstrumento { get; set; }
        public List<SelectListItem> LstOutcome { get; set; }
        public List<SelectListItem> LstCursos { get; set; }
        public List<Constituyente> LstConstituyente { get; set; }
        public IPagedList<busquedaAM> LstAccionMejora { get; set; }
        public Int32? NumeroPagina { get; set; }

        
        public LstAccionesMejoraViewModel()
        {
            LstInstrumento = new List<Instrumento>();
            LstOutcome = new List<SelectListItem>();
            LstCursos = new List<SelectListItem>();
            LstConstituyente = new List<Constituyente>();
            Outcomenombre = "";

        }
        public void fill(CargarDatosContext dataContext, int idescuela, Int32? numeroPagina, int? idConstituyente, int? idinstrumento, int? idcurso, string outcomenombre,int ModalidadId)
        {
            //idSubModalidad = 1;

            AbetEntities context = dataContext.context;

            //int IdPeriodoAcademico = context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

            int IdPeriodoAcademicom = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad==ModalidadId).IdPeriodoAcademico;

            IdInstrumento = idinstrumento;
            Outcomenombre = outcomenombre;

            

            LstConstituyente = dataContext.context.Constituyente.ToList();
            LstInstrumento = dataContext.context.Instrumento.ToList();

            if (idConstituyente.HasValue)
            {
                IdConstituyente = idConstituyente;

                LstInstrumento = (from ci in dataContext.context.ConstituyenteInstrumento
                                  where ci.IdConstituyente == IdConstituyente
                                  select ci.Instrumento).Distinct().ToList();
            }

            /* if (idcurso.HasValue)
             {
                 IdCurso = idcurso;

                 List<OutcomeComision> LstOutcomeComision = (from mc in dataContext.context.MallaCocos
                                                             join mcd in dataContext.context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                                                             join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                                                             join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
                                                             join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                                                             join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                                                             where (cmc.IdCurso == IdCurso && mc.IdPeriodoAcademico == PeriodoAcademicoId)
                                                             select oc).ToList();
                 for (int i = 0; i < LstOutcomeComision.Count; i++)
                 {
                     Outcome outcome = LstOutcomeComision[i].Outcome;
                     outcome.Nombre = LstOutcomeComision[i].Comision.Codigo + " | " + outcome.Nombre;
                     LstOutcome.Add(outcome);
                 }
             }*/

            LstCursos = (from cpa in context.CursoPeriodoAcademico
                         join ccpa in context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                         join c in context.Curso on cpa.IdCurso equals c.IdCurso
                         join ca in context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                         where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicom && ca.IdEscuela == idescuela && c.NombreEspanol != ""
                         select new SelectListItem { Text = c.NombreEspanol, Value = c.IdCurso.ToString() }).Distinct().OrderBy(x => x.Text).ToList();


            LstCursos.Insert(0, new SelectListItem { Text = "[- Todos -]", Value = "" });

            LstOutcome = (from oc in context.OutcomeComision
                          join o in context.Outcome on oc.IdOutcome equals o.IdOutcome
                          join c in context.Comision on oc.IdComision equals c.IdComision
                          where oc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicom && c.Codigo != "WASC"
                          select new SelectListItem { Value = o.Nombre, Text = o.Nombre }).Distinct().OrderBy(x => x.Text).ToList();

            LstOutcome.Insert(0, new SelectListItem { Text = "[- Todos -]", Value = "" });


           // int IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

           /* var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join cur in dataContext.context.Curso on cpa.IdCurso equals cur.IdCurso
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          where oc.IdPeriodoAcademico == PeriodoAcademicoId && c.IdEscuela == idescuela
                          select cur).Distinct().OrderBy(x => x.NombreEspanol);*/

            // LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x => x.NombreEspanol).ToList();
           // LstCurso = query0.ToList();

            NumeroPagina = numeroPagina ?? 1;



            var query = (from am in context.AccionMejora
                         join ham in context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                         join h in context.Hallazgos on ham.IdHallazgo equals h.IdHallazgo
                         //join cmc in context.CursoMallaCurricular on h.IdCurso equals cmc.IdCurso
                         //join mccd in context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mccd.IdCursoMallaCurricular
                         //join a in context.OutcomeComision on mccd.IdOutcomeComision equals a.IdOutcomeComision
                         //join b in context.Outcome on a.IdOutcome equals b.IdOutcome
                         //join ca in context.Carrera on h.IdCarrera equals ca.IdCarrera                     
                         join ci in context.ConstituyenteInstrumento on h.IdConstituyenteInstrumento equals ci.IdConstituyenteInstrumento
                         join smpam in context.SubModalidadPeriodoAcademicoModulo on h.IdSubModalidadPeriodoAcademicoModulo equals smpam.IdSubModalidadPeriodoAcademicoModulo
                         join smpa in context.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                         join su in context.SubModalidad on smpa.IdSubModalidad equals su.IdSubModalidad
                         //join oc in context.OutcomeComision on h.IdOutcomeComision equals oc.IdOutcomeComision into joindt
                         //from x in joindt.DefaultIfEmpty()
                         //join o in context.Outcome on x.IdOutcome equals o.IdOutcome into js
                         //from so in js.DefaultIfEmpty()
                         where (
                            am.IdEscuela == idescuela && am.Estado != "INA"
                            && su.IdModalidad == ModalidadId
                            //&& h.IdCurso == idcurso
                            //&& (ci.IdInstrumento == IdInstrumento)
                            //&& (ci.IdConstituyente == IdConstituyente)
                            //&& (so.Nombre == Outcomenombre)
                         )
                         select new busquedaAM {
                             IdAccionMejora = am.IdAccionMejora,
                             DescripcionEspanol = am.DescripcionEspanol,
                             Codigo = am.Codigo,
                             IdInstrumento = ci.IdInstrumento,
                             IdConstituyente = ci.IdConstituyente,
                             IdCurso = h.IdCurso,
                         }).Distinct().ToList();

            if (idConstituyente != null)
            {
                query = query.Where(x => x.IdConstituyente == idConstituyente).Distinct().ToList();
            }

            if (idinstrumento != null)
            {
                query = query.Where(x => x.IdInstrumento == idinstrumento).Distinct().ToList();
            }

            if (idcurso != null)
            {
                query = query.Where(x => x.IdCurso == idcurso).Distinct().ToList();
            }

            if (Outcomenombre != null)
            {
                query = query.Where(x => x.Outcomenombre == Outcomenombre).Distinct().ToList();
            }



            LstAccionMejora = query.ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);



        }
    }
}