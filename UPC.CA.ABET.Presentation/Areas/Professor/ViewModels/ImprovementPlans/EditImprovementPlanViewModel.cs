﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class EditImprovementPlanViewModel
    {
        //public List<Carrera> LstCar { get; set; }
        public List<SelectListItem> LstComision { get; set; }
        public List<SelectListItem> LstOutcome { get; set; }
        public List<SelectListItem> LstCarrera { get; set; }
        public List<SelectListItem> LstPeriodoAcademico { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdPlanMejora { get; set; }
        public int Anio { get; set; }
        public List<int> LstIdSmpa { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public EditImprovementPlanViewModel()
        {
            LstIdSmpa = new List<int>();
            LstCarrera = new List<SelectListItem>();
            LstComision = new List<SelectListItem>();
            LstOutcome = new List<SelectListItem>();
            LstPeriodoAcademico = new List<SelectListItem>();
            LstInstrumentos = new List<Instrumento>();
        }

        public void fill(CargarDatosContext dataContext,Int32 idmodalidad, Int32 idescuela, Int32 idPlanMejora)
        {
            IdPlanMejora = idPlanMejora;
            Anio = (int)dataContext.context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == idPlanMejora).Anio;
            LstInstrumentos = dataContext.context.Instrumento.Where(x => x.ParaPlan == true).OrderBy(x => x.IdInstrumento).ToList();
            if (idmodalidad.ToString() == ConstantHelpers.MODALITY.PREGRADO_EPE)
            {
                LstInstrumentos.Remove(dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.INSTRUMENTOS_ACRONIMOS.PPP));
            }

            var query = (from pa in dataContext.context.PeriodoAcademico
                         join spa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals spa.IdPeriodoAcademico
                         join s in dataContext.context.SubModalidad on spa.IdSubModalidad equals s.IdSubModalidad
                         join m in dataContext.context.Modalidad on s.IdModalidad equals m.IdModalidad
                         where pa.FechaInicioPeriodo.Value.Year == (Anio - 1)
                         && m.IdModalidad == idmodalidad                        
                         select new
                         {
                             IdPeriodoAcademico = pa.IdPeriodoAcademico,
                             CicloAcademico = pa.CicloAcademico,
                             IdSubModalidadPeriodoAcademico = spa.IdSubModalidadPeriodoAcademico
                         }).ToList();

            IdPeriodoAcademico = query.Max(x => x.IdPeriodoAcademico);

            LstPeriodoAcademico = (from q in query
                                   where q.IdPeriodoAcademico == IdPeriodoAcademico
                                   select new SelectListItem {
                                       Value = q.IdPeriodoAcademico.ToString(),
                                       Text = q.CicloAcademico,
                                       Selected = (q.IdPeriodoAcademico == IdPeriodoAcademico)
                                   }).Distinct().ToList();

            LstCarrera = (from q in query
                          join cpa in dataContext.context.CarreraPeriodoAcademico on q.IdSubModalidadPeriodoAcademico equals cpa.IdSubModalidadPeriodoAcademico
                          join ca in dataContext.context.Carrera on cpa.IdCarrera equals ca.IdCarrera
                          where q.IdPeriodoAcademico == IdPeriodoAcademico && ca.IdEscuela == idescuela
                          select new SelectListItem { Value = ca.IdCarrera.ToString(), Text = ca.NombreEspanol, Selected = true }).Distinct().ToList();

            //LstComision = (from com in dataContext.context.Comision
            //               join apa in dataContext.context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
            //               join ac in dataContext.context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
            //               join smp in dataContext.context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
            //               join pa in dataContext.context.PeriodoAcademico on smp.IdPeriodoAcademico equals pa.IdPeriodoAcademico
            //               join sm in dataContext.context.SubModalidad on smp.IdSubModalidad equals sm.IdSubModalidad
            //               join m in dataContext.context.Modalidad on sm.IdModalidad equals m.IdModalidad
            //               join cco in dataContext.context.CarreraComision on com.IdComision equals cco.IdComision
            //               join ca in dataContext.context.Carrera on cco.IdCarrera equals ca.IdCarrera
            //               where
            //               ca.IdEscuela == idescuela
            //               && com.Codigo != "WASC"
            //               && m.IdModalidad == ModalidadId
            //               && pa.IdPeriodoAcademico == IdPeriodoAcademico
            //               select new SelectListItem { Value = com.IdComision.ToString(), Text = com.Codigo }
            //).Distinct().ToList();

            //LstComision.First().Selected = true;

            LstOutcome.Insert(0, new SelectListItem { Text = "Todos", Value = "0" , Selected = true});
        }
    }
}