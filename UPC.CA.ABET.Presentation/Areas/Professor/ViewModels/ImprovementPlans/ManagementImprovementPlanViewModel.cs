﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class carrxCom
    {
        public string comision { get; set; }
        public int idcarrera { get; set; }
    }
    public class ManagementImprovementPlanViewModel
    {
        public List<PlanMejoraModel> LstImprovementPlans { get; set;}

        public List<AccionMejora> LstImprovementActions { get; set; }
        public List<carrxCom> LstCom { get; set;}
        public List<Carrera> LstCar { get; set; }

        public List<int> LstAccionesMejora { get; set; }

        public ManagementImprovementPlanViewModel()
        {
            LstImprovementPlans = new List<PlanMejoraModel>();
            LstImprovementActions = new List<AccionMejora>();
            LstAccionesMejora = new List<int>();
            LstCom = new List<carrxCom>();
            LstCar = new List<Carrera>();
        }

        public void fill(CargarDatosContext dataContext, int IdEscuela, int idModalidad)
        {
            LstImprovementPlans = dataContext.context.PlanMejora.Where(x => x.IdEscuela == IdEscuela && x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad == idModalidad)
                                    .Select(x => new PlanMejoraModel {
                                        IdPlanMejora = x.IdPlanMejora,
                                        Anio = x.Anio,
                                        Nombre = x.Nombre,
                                        IdEscuela = x.IdEscuela,
                                        Estado = x.Estado,
                                        IdModalidad = x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad
                                    }).ToList();

            LstCom = (from a in dataContext.context.Comision
                      join b in dataContext.context.AcreditadoraPeriodoAcademico on a.IdAcreditadoraPeriodoAcademico equals b.IdAcreditadoraPreiodoAcademico
                      join c in dataContext.context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                      join d in dataContext.context.SubModalidad on c.IdSubModalidad equals d.IdSubModalidad
                      join e in dataContext.context.PeriodoAcademico on c.IdPeriodoAcademico equals e.IdPeriodoAcademico 
                      join f in dataContext.context.CarreraComision on a.IdComision equals f.IdComision
                      join g in dataContext.context.Carrera on f.IdCarrera equals g.IdCarrera
                      where d.IdModalidad == idModalidad && e.Estado =="ACT" && a.Codigo != "WASC"
                      select new carrxCom { comision = a.Codigo , idcarrera=g.IdCarrera }).ToList();

            LstCar = (from a in dataContext.context.Carrera
                      join b in dataContext.context.CarreraPeriodoAcademico on a.IdCarrera equals b.IdCarrera
                      join c in dataContext.context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                      join d in dataContext.context.PeriodoAcademico on c.IdPeriodoAcademico equals d.IdPeriodoAcademico
                      join e in dataContext.context.SubModalidad on c.IdSubModalidad equals e.IdSubModalidad
                      where e.IdModalidad == idModalidad && d.Estado == "ACT"
                      select a).Distinct().ToList();
        }

        public List<int> getLstAccionesMejora(CargarDatosContext dataContext, int Anio)
        {

            var LstAccionesMejora = (from ham in dataContext.context.HallazgoAccionMejora
                                     join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                                     join am in dataContext.context.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                     where h.FechaRegistro.Year == Anio - 1
                                     select new {am.IdAccionMejora }
                                    ).AsEnumerable().Select(x=>x.IdAccionMejora).Distinct().ToList();
            return LstAccionesMejora;
        }
    }
}