﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class AddEditPlanMejoraViewModel
    {
        public Int32? IdPlanMejora { get; set; }
        public PlanMejora planmejora { get; set; }
        public string Nombre { get; set; }
        public Int32? Anio { get; set; }

        public List<Constituyente> LstConstituyentes { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Curso> LstCursos { get; set; }
        public Int32? IdConstituyente { get; set; }
        public Int32? IdInstrumento { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdOutcome { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public IEnumerable<SelectListItem> LstAccionesMejora { get; set; }
        public List<Int32> AccionesMejoraId { get; set; }


        public AddEditPlanMejoraViewModel()
        {
            Nombre = "";
            LstConstituyentes = new List<Constituyente>();
            LstCursos = new List<Curso>();
            LstInstrumentos = new List<Instrumento>();
            LstOutcome = new List<Outcome>();
            AccionesMejoraId = new List<int>();
            Anio = DateTime.Now.Year+1;
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? idPlanMejora, Int32 idSubModalidadPeriodoAcademico, Int32 idEscuela)
        {
            IdPlanMejora = idPlanMejora;
            LstConstituyentes = dataContext.context.Constituyente.ToList();

            var query0 = (from cpa in dataContext.context.CursoPeriodoAcademico
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Curso on cpa.IdCurso equals c.IdCurso
                          join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                          where cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && ca.IdEscuela == idEscuela
                          select c).Distinct().OrderBy(x => x.NombreEspanol);
            LstCursos = query0.ToList();


            List<AccionMejora> accionesmejora = new List<AccionMejora>();

            if (IdPlanMejora.HasValue)
            {
                PlanMejora planmejora = dataContext.context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == IdPlanMejora);

                Nombre = planmejora.Nombre;
                Anio = planmejora.Anio;

                accionesmejora = dataContext.context.PlanMejoraAccion.Where(x => x.IdPlanMejora == IdPlanMejora).Where(x=> x.AccionMejora.Estado!="INA").Select(x => x.AccionMejora).ToList();

                AccionesMejoraId = accionesmejora.Select(x => x.IdAccionMejora).ToList();


            }

            LstAccionesMejora = accionesmejora.Select(h => new SelectListItem
            {
                Text = h.Codigo + " : " + h.DescripcionEspanol,
                Value = h.IdAccionMejora.ToString()
            });


        }
    }
}