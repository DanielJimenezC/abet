﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class LstPlanesMejoraViewModel
    {
        public int? IdConstituyente { get; set; }
        public int? IdInstrumento { get; set; }
        public int? IdOutcome { get; set; }
        public int? IdCurso { get; set; }
        public List<Instrumento> LstInstrumento { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Constituyente> LstConstituyente { get; set; }
        public IPagedList<PlanMejora> LstPlanMejora { get; set; }
        public Int32? NumeroPagina { get; set; }

        public LstPlanesMejoraViewModel()
        {

            LstInstrumento = new List<Instrumento>();
            LstOutcome = new List<Outcome>();
            LstCurso = new List<Curso>();
            LstConstituyente = new List<Constituyente>();


        }
        public void fill(CargarDatosContext dataContext, int idescuela, Int32? numeroPagina, int? idConstituyente, int? idinstrumento, int? idcurso, int? idoutcome,int? IdSubModalidad)
        {
            // int PeriodoAcademicoId = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
            //SOLO EN ESTE CASO YA QUE ES LA UNICA SUBMODALIDADQUEEXISTE
            IdSubModalidad = 1;

            int idPeriodoAcademicos = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == IdSubModalidad).IdPeriodoAcademico;

            IdInstrumento = idinstrumento;
            IdOutcome = idoutcome;

            LstConstituyente = dataContext.context.Constituyente.ToList();
          LstInstrumento = dataContext.context.Instrumento.ToList();

            if (idConstituyente.HasValue)
            {
                IdConstituyente = idConstituyente;

                LstInstrumento = (from ci in dataContext.context.ConstituyenteInstrumento
                                  where ci.IdConstituyente == IdConstituyente
                                  select ci.Instrumento).Distinct().ToList();
            }

            if (idcurso.HasValue)
            {
                IdCurso = idcurso;

                List<OutcomeComision> LstOutcomeComision = (from mc in dataContext.context.MallaCocos
                                                            join mcd in dataContext.context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                                                            join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                                                            join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
                                                            join c in dataContext.context.Comision on oc.IdComision equals c.IdComision
                                                            join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                                                            where (cmc.IdCurso == IdCurso && mc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademicos)
                                                            select oc).ToList();



                for (int i = 0; i < LstOutcomeComision.Count; i++)
                {
                    Outcome outcome = LstOutcomeComision[i].Outcome;
                    outcome.Nombre = LstOutcomeComision[i].Comision.Codigo + " | " + outcome.Nombre;
                    LstOutcome.Add(outcome);
                }

            }

            int IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;

            var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join cur in dataContext.context.Curso on cpa.IdCurso equals cur.IdCurso
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          where oc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademicos && c.IdEscuela == idescuela
                          select cur).Distinct().OrderBy(x => x.NombreEspanol);

            // LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x => x.NombreEspanol).ToList();
            LstCurso = query0.ToList();

            NumeroPagina = numeroPagina ?? 1;

            var query = (from  pm in dataContext.context.PlanMejora
                         join pma in dataContext.context.PlanMejoraAccion on pm.IdPlanMejora equals pma.IdPlanMejora
                         join am in dataContext.context.AccionMejora on pma.IdAccionMejora equals am.IdAccionMejora
                         join ham in dataContext.context.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                         join h in dataContext.context.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                         join ci in dataContext.context.ConstituyenteInstrumento on h.IdInstrumento equals ci.IdInstrumento
                         where (pm.IdEscuela == idescuela && pm.Estado != "INA"
                            && (IdCurso.HasValue == false || h.IdCurso == IdCurso)
                            && (IdInstrumento.HasValue == false || h.IdInstrumento == IdInstrumento)
                            && (IdConstituyente.HasValue == false || ci.IdConstituyente == IdConstituyente)
                            && (IdOutcome.HasValue == false || h.IdOutcome == IdOutcome))
                         select pm).Distinct().ToList();

            LstPlanMejora = query.ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);



        }

    }
}