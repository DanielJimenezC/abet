﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class MantenimientoAccionesMejoraViewModel
    {
        public List<SelectListItem> LstComision { get; set; }
        public List<SelectListItem> LstOutcome { get; set; }
        public List<SelectListItem> LstCarrera { get; set; }
        public List<SelectListItem> LstPeriodoAcademico { get; set; }
        public List<Instrumento> LstInstrumentos { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdPlanMejora { get; set; }
        public Boolean NoRelacionado { get; set; }
        public Boolean EsCritico { get; set; }
        public List<int> LstIdSmpa { get; set; }
        public MantenimientoAccionesMejoraViewModel()
        {
            LstIdSmpa = new List<int>();
            LstCarrera = new List<SelectListItem>();
            LstComision = new List<SelectListItem>();
            LstOutcome = new List<SelectListItem>();
            LstPeriodoAcademico = new List<SelectListItem>();
            LstInstrumentos = new List<Instrumento>();
        }

        public void fill(CargarDatosContext dataContext, Int32 idmodalidad, Int32 idescuela)
        {
            var query = (from pa in dataContext.context.PeriodoAcademico
                         join spa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals spa.IdPeriodoAcademico
                         join s in dataContext.context.SubModalidad on spa.IdSubModalidad equals s.IdSubModalidad
                         join m in dataContext.context.Modalidad on s.IdModalidad equals m.IdModalidad
                         where m.IdModalidad == idmodalidad
                        
                         select new
                         {
                             IdPeriodoAcademico = pa.IdPeriodoAcademico,
                             CicloAcademico = pa.CicloAcademico,
                             IdSubModalidadPeriodoAcademico = spa.IdSubModalidadPeriodoAcademico,
                             Estado = pa.Estado
                         }).Distinct().ToList();

            IdPeriodoAcademico = query.Where(x => x.Estado == "ACT").Max(x => x.IdPeriodoAcademico);

            LstPeriodoAcademico = (from q in query
                                   select new SelectListItem
                                   {
                                       Value = q.IdPeriodoAcademico.ToString(),
                                       Text = q.CicloAcademico,
                                       Selected = (q.IdPeriodoAcademico == IdPeriodoAcademico)
                                   }).Distinct().ToList();

            LstCarrera = (from q in dataContext.context.Carrera
                          join cpa in dataContext.context.CarreraPeriodoAcademico on q.IdCarrera equals cpa.IdCarrera
                          join smpa in dataContext.context.SubModalidadPeriodoAcademico on cpa.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                          join s in dataContext.context.SubModalidad on smpa.IdSubModalidad equals s.IdSubModalidad
                          join m in dataContext.context.Modalidad on s.IdModalidad equals m.IdModalidad
                          where m.IdModalidad == idmodalidad && q.IdEscuela == idescuela
                          select new SelectListItem { Value = q.IdCarrera.ToString(), Text = q.NombreEspanol, Selected = true }).Distinct().ToList();

            //LstComision = (from com in dataContext.context.Comision
            //               join apa in dataContext.context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
            //               join ac in dataContext.context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
            //               join smp in dataContext.context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
            //               join pa in dataContext.context.PeriodoAcademico on smp.IdPeriodoAcademico equals pa.IdPeriodoAcademico
            //               join sm in dataContext.context.SubModalidad on smp.IdSubModalidad equals sm.IdSubModalidad
            //               join m in dataContext.context.Modalidad on sm.IdModalidad equals m.IdModalidad
            //               join cco in dataContext.context.CarreraComision on com.IdComision equals cco.IdComision
            //               join ca in dataContext.context.Carrera on cco.IdCarrera equals ca.IdCarrera
            //               where
            //               ca.IdEscuela == idescuela
            //               && com.Codigo != "WASC"
            //               && m.IdModalidad == ModalidadId
            //               && pa.IdPeriodoAcademico == IdPeriodoAcademico
            //               select new SelectListItem { Value = com.IdComision.ToString(), Text = com.Codigo }
            //            ).Distinct().ToList();
            //LstComision.First().Selected = true;
            LstOutcome.Insert(0, new SelectListItem { Text = "Todos", Value = "0", Selected = true });
            LstInstrumentos = dataContext.context.Instrumento.Where(x => x.ParaPlan == true).OrderBy(x => x.IdInstrumento).ToList();
            if (idmodalidad.ToString() == ConstantHelpers.MODALITY.PREGRADO_EPE)
            {
                LstInstrumentos.Remove(dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.INSTRUMENTOS_ACRONIMOS.PPP));
            }
        }
    }
}