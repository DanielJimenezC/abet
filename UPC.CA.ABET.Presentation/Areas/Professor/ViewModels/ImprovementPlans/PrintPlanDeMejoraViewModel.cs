﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.ImprovementPlans
{
    public class PrintPlanDeMejoraViewModel
    {
        public int? IdConstituyente { get; set; }
        public int? IdInstrumento { get; set; }
        public int? IdCurso { get; set; }
        public int? IdOutcome { get; set; }
        public Constituyente constituyente { get;set;}
        public Instrumento instrumento { get; set; }
        public Curso curso { get; set; }
        public Outcome outcome { get; set; }
        private OutcomeComision oc { get; set; }
        public int? IdSubModalidadPeriodoAcademico { get; set; }

        public int IdPlanMejora { get; set; }
        public List<Constituyente> LstConstituyente { get; set; }
        public List<Instrumento> LstInstrumento { get; set; }
        public List<int> LstInstrumentoID { get; set; }
        public PlanMejora planmejora { get; set; }

        public PrintPlanDeMejoraViewModel()
        {
            constituyente = new Constituyente();
            instrumento = new Instrumento();
            curso = new Curso();
            outcome = new Outcome();
            oc = new OutcomeComision();
            LstConstituyente = new List<Constituyente>();
            LstInstrumento = new List<Instrumento>();
            LstInstrumentoID = new List<int>();
            planmejora = new PlanMejora();
        }
        public void CargarDatos(CargarDatosContext dataContext, int idPlanMejora, int? idConstituyente, int? idInstrumento, int? idCurso, int? idOutcome)
        {
            IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;
            IdPlanMejora = idPlanMejora;
            planmejora = dataContext.context.PlanMejora.FirstOrDefault(x => x.IdPlanMejora == IdPlanMejora);
            IdConstituyente = idConstituyente;
            IdInstrumento = idInstrumento;
            IdCurso = idCurso;
            IdOutcome = idOutcome;

            if (IdConstituyente.HasValue)
            {
                constituyente = dataContext.context.Constituyente.FirstOrDefault(x => x.IdConstituyente== IdConstituyente);
                LstConstituyente = dataContext.context.Constituyente.Where(x => x.IdConstituyente == IdConstituyente).ToList();

                LstInstrumento = dataContext.context.ConstituyenteInstrumento.Where(x => x.IdConstituyente == IdConstituyente).Select(x => x.Instrumento).ToList();
                LstInstrumentoID = dataContext.context.ConstituyenteInstrumento.Where(x => x.IdConstituyente == IdConstituyente).Select(x => x.IdInstrumento).ToList();
            }
            else
            {
                LstConstituyente = dataContext.context.Constituyente.ToList();
                LstInstrumento = dataContext.context.Instrumento.ToList();
                LstInstrumentoID = dataContext.context.Instrumento.Select(x => x.IdInstrumento).ToList();
            }
            if(IdInstrumento.HasValue)
            {
                instrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.IdInstrumento == IdInstrumento);
                LstInstrumento = dataContext.context.Instrumento.Where(x => x.IdInstrumento == IdInstrumento).ToList();
                LstInstrumentoID = dataContext.context.Instrumento.Where(x => x.IdInstrumento == IdInstrumento).Select(x=>x.IdInstrumento).ToList();
            }



            if (IdCurso.HasValue)
            {
                curso = dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == IdCurso);
            }
            if (IdOutcome.HasValue)
            {
                outcome = dataContext.context.Outcome.FirstOrDefault(x => x.IdOutcome == IdOutcome);

                oc = dataContext.context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == IdOutcome && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico);
            }

        }

        public string getConstituyenteNombre()
        {
            if (IdConstituyente.HasValue)
                return constituyente.NombreEspanol;
            else
                return "Sin especificar";
        }

        public string getInstrumentoNombre()
        {
            if (IdInstrumento.HasValue)
                return instrumento.NombreEspanol;
            else
                return "Sin especificar";
        }

        public string getCursoNombre()
        {
            if (IdCurso.HasValue)
                return curso.NombreEspanol;
            else
                return "Sin especificar";
        }

        public string getOutcomeNombre()
        {
            if (IdOutcome.HasValue)
            {
                
                return oc.Comision.Codigo+" | "+ outcome.Nombre;
            }
                
            else
                return "Sin especificar";
        }

    }
}