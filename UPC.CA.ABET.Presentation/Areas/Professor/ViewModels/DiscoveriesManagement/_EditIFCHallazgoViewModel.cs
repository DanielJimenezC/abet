﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement
{
    public class _EditIFCHallazgoViewModel
    {
        public String Text { get; set; }
        public List<Criticidad> Criticidades { get; set; }
        public List<Outcome> lstOutcomes { get; set; }
        public CursoMallaCurricular cursoMallaCurricular { get; set; }
        public string codigo { get; set; }
        public string descripcionespanol { get; set; }
        public string descripcioningles { get; set; }
        public int? idhallazgo { get; set; }
        public Criticidad criticidad { get; set; }
        public Int32 idCriticidad { get; set; }
        public String Criticidad { get; set; }
        public string nombrecriticidad { get; set; }
        public int ifcid { get; set; }
        public string codigocurso { get; set; }

        public _EditIFCHallazgoViewModel()
        {
            Criticidades = new List<Criticidad>();
            lstOutcomes = new List<Outcome>();
        }

        public void CargarData(AbetEntities context, int? idHallazgo, String codCurso)
        {
            var Curso = (context.Curso.Where(x => x.Codigo == codCurso).FirstOrDefault());
            var Hallazgo = (context.Hallazgos.Where(x => x.IdHallazgo == idHallazgo).FirstOrDefault());      
            this.idhallazgo = idHallazgo;          
            this.codigo = Hallazgo.Codigo + "-" + Hallazgo.Identificador.ToString();
            this.descripcionespanol = Hallazgo.DescripcionEspanol;
            this.descripcioningles = Hallazgo.DescripcionIngles;
            this.Criticidades = context.Criticidad.ToList();
            this.idCriticidad = Hallazgo.IdCriticidad;
            this.criticidad = context.Criticidad.Where(x => x.IdCriticidad == this.idCriticidad).FirstOrDefault();
            this.nombrecriticidad = criticidad.NombreEspanol;
            cursoMallaCurricular = context.CursoMallaCurricular.Where(x => x.IdCurso == Curso.IdCurso).FirstOrDefault();
            Outcome outcome;
            var listaOutcomes = context.GetOutcomes(Curso.IdCurso).ToList();
            for (int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionIngles;
                lstOutcomes.Add(outcome);
            }
        }
        public void CargarAddHallazgo(AbetEntities context, String codCurso)
        {
            var Curso = (context.Curso.Where(x => x.Codigo == codCurso).FirstOrDefault());
            this.codigocurso = codCurso;
            Criticidades = context.Criticidad.ToList();
            cursoMallaCurricular = context.CursoMallaCurricular.Where(x => x.IdCurso == Curso.IdCurso).FirstOrDefault();
            Outcome outcome;
            this.criticidad = context.Criticidad.FirstOrDefault();
            this.idCriticidad = criticidad.IdCriticidad;
            this.nombrecriticidad = criticidad.NombreEspanol;
            var listaOutcomes = context.GetOutcomes(Curso.IdCurso).ToList();
            for (int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionIngles;
                lstOutcomes.Add(outcome);
            }          
        }
    }
}
