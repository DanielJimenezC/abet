﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement
{
    public class AddHallazgoViewModel
    {
        public String Text { get; set; }
        public List<Criticidad> Criticidades { get; set; }
        public List<Outcome> lstOutcomes { get; set; }
        public CursoMallaCurricular cursoMallaCurricular { get; set; }

        //CARGAR OUTCOMES DE ACUERDO A LA SUBMODALIDAD DE PERIODO ACADEMICO
        public string HallazgoIdOutcome { get; set; }
        public IEnumerable<SelectListItem> ListOutcomes { get; set; }
        public List<OutcomeViewModel> ListaOutcomes { get; set; }

        public void CargarOutcomes(string codCurso, int idSubModalidadPeriodo, AbetEntities context)
        {
            var Curso = (context.Curso.Where(x => x.Codigo == codCurso).FirstOrDefault());
            ListaOutcomes = new List<OutcomeViewModel>();
            OutcomeViewModel outcome;
            var listaOutcomes = context.sp_get_outcomes(Curso.IdCurso, idSubModalidadPeriodo).ToList();
            for(int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new OutcomeViewModel();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.Nombre = listaOutcomes[i].Nombre;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionIngles;
                outcome.Codigo = listaOutcomes[i].Codigo;
                ListaOutcomes.Add(outcome);
            }
        }
        //--------------------------

        public void CargarData(string codCurso, AbetEntities context)
        {
            var Curso = (context.Curso.Where(x => x.Codigo ==  codCurso).FirstOrDefault());
            lstOutcomes = new List<Outcome>();
            cursoMallaCurricular = context.CursoMallaCurricular.Where(x => x.IdCurso== Curso.IdCurso).FirstOrDefault();
            Outcome outcome;
            var listaOutcomes = context.GetOutcomes(Curso.IdCurso).ToList();
            for(int i = 0; i < listaOutcomes.Count(); i++)
            {
                outcome = new Outcome();
                outcome.IdOutcome = listaOutcomes[i].IdOutcome;
                outcome.DescripcionEspanol = listaOutcomes[i].DescripcionEspanol;
                outcome.DescripcionIngles = listaOutcomes[i].DescripcionIngles;
                lstOutcomes.Add(outcome);
            }

            return;
        }
    }
}