﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement
{
    public class OutcomeViewModel
    {
        public int IdOutcome { get; set; }
        public string Nombre { get; set; }
        public string DescripcionEspanol { get; set; }
        public string DescripcionIngles { get; set; }
        public string Codigo { get; set; }
    }
}