﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.DiscoveriesManagement
{
    public class ConsultDiscoveriesViewModel
    {

        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> AvailableComisiones{ get; set; }
        public IEnumerable<SelectListItem> AvailableCarreras { get; set; }
        public IEnumerable<SelectListItem> AvailableSedes { get; set; }
        public IEnumerable<SelectListItem> AvailableConstituyentes { get; set; }
        public IEnumerable<SelectListItem> AvailableInstrumentos { get; set; }
        public IEnumerable<SelectListItem> AvailableStudentOutcomes { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableNivelAceptacion { get; set; }

    
        public int IdPeriodoAcademico { get; set; }
        public string CodigoComision { get; set; }
        public string CodigoConstituyente { get; set; }
        public int IdCarrera { get; set; }
        public int IdSede { get; set; }
        public int IdConstituyente { get; set; }
        public int IdInstrumento { get; set; }
        public int IdStudentOutcome { get; set; }
        public int IdCurso { get; set; }
        public int IdNivelAceptacion { get; set; }

        
    }
}