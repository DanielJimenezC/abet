﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using EstadosActas = UPC.CA.ABET.Helpers.ConstantHelpers.PROFESSOR.ACTAS.ESTADOS;
using TiposActas = UPC.CA.ABET.Helpers.ConstantHelpers.PROFESSOR.ACTAS.TIPOS;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingComitteMinute
{
    public class IndexViewModel
    {
        //public int? IdPeriodoAcademico { get; set; }
        public int? IdSubModalidadPeriodoAcademico { get; set; }

        public string Estado { get; set; }

        [Display(Name = "Fecha inicio:")]
        public string Desde { get; set; }

        [Display(Name = "Fecha final:")]
        public string Hasta { get; set; }

        public SelectList PeriodosAcademicos { get; set; }

        public SelectList Estados { get; set; }

        public IPagedList<Acta> Actas { get; set; }

        public void CargarDatos(CargarDatosContext dataContext)
        {
            PeriodosAcademicos = new SelectList(dataContext.context
                .PeriodoAcademico
                .OrderByDescending(pa => pa.CicloAcademico)
                .ToList(), "IdPeriodoAcademico", "CicloAcademico");

            Estados = new SelectList(new Dictionary<string, string> 
            {
                { EstadosActas.OK.CODIGO, EstadosActas.OK.TEXTO },
                { EstadosActas.OBSERVADA.CODIGO, EstadosActas.OBSERVADA.TEXTO }
            }, "Key", "Value");

            var culture = new CultureInfo(dataContext.currentCulture);
            var format = dataContext.currentCulture == ConstantHelpers.CULTURE.INGLES ? "MM/dd/yyyy" : "dd/MM/yyyy";
            Desde = DateTime.Now.AddMonths(-6).ToString(format, culture);
            Hasta = DateTime.Now.AddMonths(6).ToString(format, culture);

            Actas = ListarActas(dataContext);
        }

        public IPagedList<Acta> ListarActas(CargarDatosContext dataContext, int? page = null, IndexViewModel viewModel = null)
        {
            if (viewModel == null)
            {
                viewModel = new IndexViewModel();
            }

            //viewModel.IdPeriodoAcademico = viewModel.IdPeriodoAcademico ?? 0;
            viewModel.IdSubModalidadPeriodoAcademico = viewModel.IdSubModalidadPeriodoAcademico ?? 0;
            viewModel.Estado = viewModel.Estado ?? string.Empty;

            var culture = new CultureInfo("es-PE");

            var fechaDesde = Convert.ToDateTime(viewModel.Desde, culture);
            var fechaHasta = Convert.ToDateTime(viewModel.Hasta, culture);
            var usuarioId = dataContext.session.GetDocenteId();

            var actas = dataContext.context.Acta
                    .Where(a =>
                        (
                            a.Reunion.IdDocente == usuarioId
                            || a.Reunion.ParticipanteReunion.Any(pr => pr.IdDocente.HasValue && pr.IdDocente.Value == usuarioId)
                        )
                        && a.Reunion.Fecha >= fechaDesde
                        && a.Reunion.Fecha <= fechaHasta
                        //&& (viewModel.IdPeriodoAcademico == 0 || viewModel.IdPeriodoAcademico == a.IdPeriodoAcademico)
                        && (viewModel.IdSubModalidadPeriodoAcademico == 0 || viewModel.IdSubModalidadPeriodoAcademico == a.IdSubModalidadPeriodoAcademico)
                        && (viewModel.Estado == "" || viewModel.Estado == a.Estado)
                        && a.TipoActa == TiposActas.ACTA_COMITE.CODIGO)
                    .ToList();

            return actas.ToPagedList(page ?? 1, 10);
        }
    }
}