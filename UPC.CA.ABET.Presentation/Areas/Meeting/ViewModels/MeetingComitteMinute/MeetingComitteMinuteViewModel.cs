﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.MeetingComitteMinute;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingComitteMinute
{
    public class MeetingComitteMinuteViewModel
    {
        public bool CanEdit { get; set; }

        public bool IsEditing { get; set; }

        public int IdActa { get; set; }

        public int? IdDocente { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredSede")]
        public int IdSede { get; set; }

        public int IdReunion { get; set; }

        //public int IdPeriodoAcademico { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }

        public bool FechaModificada { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredUnidadAcademica")]
        public int IdUnidadAcademica { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredMotivo")]
        public string Motivo { get; set; }

        public string UnidadAcademica { get; set; }

        public int? Semana { get; set; }

        public string Sede { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredFechaReunion")]
        public string FechaReunion { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredLugarReunion")]
        public string LugarReunion { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredHoraInicioReunion")]
        public string HoraInicioReunion { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredHoraFinReunion")]
        public string HoraFinReunion { get; set; }

        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredObjetivo")]
        public string Objetivo { get; set; }

        public List<MeetingComitteMinuteOrganizatorViewModel> Organizadores { get; set; }

        public List<MeetingComitteMinuteParticipantViewModel> Participantes { get; set; }

        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredAgenda")]
        public string Agenda { get; set; }

        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredDetalleReunion")]
        public string DetalleReunion { get; set; }

        public List<MeetingComitteMinuteCommentViewModel> Comentarios { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Foto { get; set; }

        
        public string RutaFoto { get; set; }

        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string DescripcionFoto { get; set; }

        public SelectList Sedes { get; set; }

        public SelectList UnidadesAcademicas { get; set; }



        public MeetingComitteMinuteViewModel()
        {
            this.Organizadores = new List<MeetingComitteMinuteOrganizatorViewModel>();
            this.Participantes = new List<MeetingComitteMinuteParticipantViewModel>();
            this.Comentarios = new List<MeetingComitteMinuteCommentViewModel>();
        }

        public void CargarDatos(CargarDatosContext dataContext, int? idActa, int? idReunion, bool? isEditing)
        {
            this.IsEditing = isEditing ?? false;
            this.IdDocente = dataContext.session.GetDocenteId();

            var reunion = idReunion.HasValue ? dataContext.context.Reunion.Find(idReunion.Value) : dataContext.context.Acta.Where(a => a.IdActa == (idActa ?? 0)).Select(a => a.Reunion).FirstOrDefault();

            this.CanEdit = reunion.IdDocente == this.IdDocente;

            if (reunion != null && reunion.PreAgenda.TipoPreagenda == ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO)
            {
                this.IdReunion = reunion.IdReunion;
                this.Motivo = reunion.Motivo;
                this.FechaReunion = reunion.Fecha.ToString("dd/MM/yyyy");
                this.Semana = reunion.Semana ?? 0;
                this.LugarReunion = reunion.Lugar;
                this.HoraInicioReunion = reunion.HoraInicio.ToString("HH:mm");
                this.HoraFinReunion = reunion.HoraFin.ToString("HH:mm");
                this.Agenda = reunion.AgendaComiteConsultivo;
                this.Objetivo = reunion.ObjetivoComiteConsultivo;
                //this.IdPeriodoAcademico = reunion.IdPeriodoAcademico ?? 0;
                this.IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico ?? 0;


                var idSede = reunion.IdSede;
                if (idSede.HasValue)
                {
                    var sede = dataContext.context.Sede.Find(idSede.Value);
                    this.Sede = sede.Nombre;
                    this.IdSede = sede.IdSede;
                }

                var preAgenda = reunion.PreAgenda;
                if (preAgenda != null)
                {
                    this.UnidadAcademica = preAgenda.UnidadAcademica.NombreEspanol;
                    this.IdUnidadAcademica = preAgenda.UnidadAcademica.IdUnidadAcademica;
                }

                var acta = reunion.Acta.FirstOrDefault();
                if (acta != null)
                {
                    this.IdActa = acta.IdActa;
                    this.RutaFoto = acta.RutaFoto;
                    this.DescripcionFoto = acta.DescripcionFoto;
                    this.DetalleReunion = acta.DetalleReunionComiteConsultivo;
                    this.IdReunion = acta.IdReunion;
                }

                    
                reunion.ParticipanteReunion.Where(pr => pr.EsExterno == false).ToList().ForEach(pr =>     
                {
                    if (pr.IdDocente.HasValue)
                    {
                        var unidadAcademica = pr.Docente.UnidadAcademicaResponsable
                               .Where(uar => uar.SedeUnidadAcademica.IdSede == (idSede ?? 0))
                               .Select(uar => uar.SedeUnidadAcademica.UnidadAcademica)
                               .FirstOrDefault() ?? new UnidadAcademica();

                        this.Organizadores.Add(new MeetingComitteMinuteOrganizatorViewModel
                        {
                            Asistio = pr.Asistio,
                            IdPersona = pr.IdParticipanteReunion, //pr.IdDocente ?? 0,
                            Nombres = pr.NombreCompleto,
                            Descripcion = dataContext.currentCulture == "en-US" ? unidadAcademica.NombreIngles : unidadAcademica.NombreEspanol,
                            Seleccionar = (pr.IdDocente ?? 0) == IdDocente,
                            UsuarioEsOrganizador = (pr.IdDocente ?? 0) == IdDocente,
                            IdDocente = pr.IdDocente ?? 0,
                            IdUnidadAcademicaResponsable = pr.IdUnidadAcademicaResponsable ?? 0
                        });   
                    }
                });

                reunion.ParticipanteReunion.Where(pr => pr.EsExterno == true).ToList().ForEach(pr =>
                {
                    var empresa = pr.Empresa ?? new Empresa();

                    this.Participantes.Add(new MeetingComitteMinuteParticipantViewModel 
                    {
                        Asistio = pr.Asistio,
                        IdPersona = pr.IdParticipanteReunion,
                        IdEmpresa = pr.EmpresaId ?? 0,
                        Nombres = pr.NombreCompleto,
                        Cargo = pr.Cargo,
                        Correo = pr.Correo,
                        RazonSocial = empresa.RazonSocial,
                        RUC = empresa.RUC,
                        EmailContacto = empresa.EmailContacto
                    });

                    if (pr.ComentarioParticipanteReunion.Any())
                    {
                        pr.ComentarioParticipanteReunion.ToList().ForEach(cpr =>
                        {
                            this.Comentarios.Add(new MeetingComitteMinuteCommentViewModel
                            {
                                Descripcion = cpr.Texto,
                                IdComentario = cpr.IdComentario,
                                IdParticipante = cpr.IdParticipanteReunion,
                                Participante = pr.NombreCompleto
                            });
                        });
                    }
                });

            }
            
            if (this.IsEditing)
            {
                Sedes = new SelectList(dataContext.context.Sede.OrderBy(s => s.Nombre).ToList(), "IdSede", "Nombre");
                //UnidadesAcademicas = new SelectList(ListarUnidadesAcademicas(dataContext, IdSede, IdPeriodoAcademico), "IdUnidadAcademica", dataContext.currentCulture == "en-US" ? "NombreIngles" : "NombreEspanol");
                UnidadesAcademicas = new SelectList(ListarUnidadesAcademicas(dataContext, IdSede, IdSubModalidadPeriodoAcademico), "IdUnidadAcademica", dataContext.currentCulture == "en-US" ? "NombreIngles" : "NombreEspanol");
            }
        }

        //public List<MeetingComitteMinuteOrganizatorViewModel> ListarOrganizadores(CargarDatosContext dataContext, int idSede, int PeriodoAcademicoId)
        public List<MeetingComitteMinuteOrganizatorViewModel> ListarOrganizadores(CargarDatosContext dataContext, int idSede, int idSubModalidadPeriodoAcademico)
        {
            var idDocente = dataContext.session.GetDocenteId();
            var niveles = new int[] { 1 };
            return dataContext.context.UnidadAcademicaResponsable
                .Where(uar =>
                    uar.SedeUnidadAcademica.IdSede == idSede
                    //&& uar.SedeUnidadAcademica.UnidadAcademica.IdPeriodoAcademico == PeriodoAcademicoId
                    && uar.SedeUnidadAcademica.UnidadAcademica.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                    && niveles.Contains(uar.SedeUnidadAcademica.UnidadAcademica.Nivel)
                    //&& uar.Docente.Codigo != "PCSIVPAR"
                    && (uar.SedeUnidadAcademica.UnidadAcademica.IdCarreraPeriodoAcademico.HasValue || uar.SedeUnidadAcademica.UnidadAcademica.Nivel == 0))
                .Distinct()
                .ToList()
                .Select(uar => new MeetingComitteMinuteOrganizatorViewModel
                {
                    IdPersona = 0,
                    Nombres = string.Format("{0} {1}", uar.Docente.Nombres, uar.Docente.Apellidos),
                    Descripcion = dataContext.currentCulture == "en-US" ? uar.SedeUnidadAcademica.UnidadAcademica.NombreIngles : uar.SedeUnidadAcademica.UnidadAcademica.NombreEspanol,
                    Seleccionar = (uar.IdDocente ?? 0) == IdDocente,
                    UsuarioEsOrganizador = (uar.IdDocente ?? 0) == IdDocente,
                    IdDocente = uar.IdDocente ?? 0,
                    IdUnidadAcademicaResponsable = uar.IdUnidadAcademicaResponsable
                })
                .ToList();
        }

        //private List<UnidadAcademica> ListarUnidadesAcademicas(CargarDatosContext dataContext, int idSede, int PeriodoAcademicoId)
        private List<UnidadAcademica> ListarUnidadesAcademicas(CargarDatosContext dataContext, int idSede, int idSubModalidadPeriodoAcademico)
        {
            var docente = dataContext.context.Docente.Find(dataContext.session.GetDocenteId() ?? 0);
            var unidadesAcademicas = default(List<UnidadAcademica>);
            if (docente != null)
	        {
                unidadesAcademicas = docente.UnidadAcademicaResponsable
                    //.Where(uar => uar.SedeUnidadAcademica.IdSede == idSede && uar.SedeUnidadAcademica.UnidadAcademica.IdPeriodoAcademico == PeriodoAcademicoId)
                    .Where(uar => uar.SedeUnidadAcademica.IdSede == idSede && uar.SedeUnidadAcademica.UnidadAcademica.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico)
                    .Select(uar => uar.SedeUnidadAcademica.UnidadAcademica)
                    .ToList();
	        }

            return unidadesAcademicas ?? new List<UnidadAcademica>();
        }
    }

    public class MeetingComitteMinuteOrganizatorViewModel
    {
        public int IdPersona { get; set; }

        public string Nombres { get; set; }

        public string Descripcion { get; set; }

        public bool Asistio { get; set; }

        public bool Seleccionar { get; set; }

        public bool UsuarioEsOrganizador { get; set; }

        public int IdDocente { get; set; }

        public int IdUnidadAcademicaResponsable { get; set; }
    }

    public class MeetingComitteMinuteParticipantViewModel : Empresa
    {
        public int IdPersona { get; set; }

        public string TempId { get; set; }

        public int IdReunion { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredNombres")]
        public string Nombres { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredCargo")]
        public string Cargo { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredCorreo")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "EmailAddressCorreo")]
        public string Correo { get; set; }

        public bool Asistio { get; set; }

        public bool Seleccionar { get; set; }

        public SelectList Empresas { get; set; }

        public void CargarParticipanteReunion(AbetEntities context, int idReunion, int? idParticipanteReunion, string tempId)
        {
            this.IdReunion = IdReunion;
            this.TempId = tempId;

            if (idParticipanteReunion.HasValue)
            {
                var participante = context.ParticipanteReunion.Find(idParticipanteReunion.Value);
                if (participante != null)
                {
                    this.IdPersona = participante.IdParticipanteReunion;
                    this.Nombres = participante.NombreCompleto;
                    this.Cargo = participante.Cargo;
                    this.Correo = participante.Correo;
                }
            }
        }
    }

    public class MeetingComitteMinuteCommentViewModel
    {
        public int IdComentario { get; set; }

        public string TempId { get; set; }

        public int IdReunion { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredParticipante")]
        public string TempIdParticipante { get; set; }

        public int IdParticipante { get; set; }

        public string Participante { get; set; }

        public string Criticidad { get; set; }

        [Required(ErrorMessageResourceType = typeof(DetailResource), ErrorMessageResourceName = "RequiredComentario")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }

        public int IdCriticidad { get; set; }

        public SelectList Participantes { get; set; }

        public List<Criticidad> LstCriticidad { get; set; }

        public void CargarDatos(AbetEntities context, int idReunion, int? idComentario, string tempId)
        {
            LstCriticidad = new List<Criticidad>();
            LstCriticidad = context.Criticidad.ToList();
            this.IdReunion = IdReunion;
            this.TempId = tempId;
            //this.Participantes = new SelectList(context.ParticipanteReunion.Where(pr => pr.IdReunion == idReunion && pr.EsExterno == true).ToList(), "IdParticipanteReunion", "NombreCompleto");

            if (idComentario.HasValue)
            {
                var comentario = context.ComentarioParticipanteReunion.Find(idComentario.Value);
                if (comentario != null)
                {
                    this.Descripcion = comentario.Texto;
                }
            }
        }
    }
}