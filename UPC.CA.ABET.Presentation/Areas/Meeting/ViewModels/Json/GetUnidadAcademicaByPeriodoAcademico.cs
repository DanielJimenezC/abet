﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Json
{
    public class GetUnidadAcademicaByPeriodoAcademico
    {
        public Int32? IdPeriodoAcademico { get; set; }
        public List<UnidadAcademica> LstUnidadAcademica { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? IdPeriodoAcademico)
        {
            if (IdPeriodoAcademico == null)
                IdPeriodoAcademico = 5;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            this.IdPeriodoAcademico= IdPeriodoAcademico;

        }
    }
}