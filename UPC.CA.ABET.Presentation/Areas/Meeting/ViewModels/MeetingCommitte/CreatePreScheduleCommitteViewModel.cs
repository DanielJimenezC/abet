﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte
{
    public class CreatePreScheduleCommitteViewModel
    {
        public Int32? IdPreAgenda { get; set; }
        public Int32 IdSede { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<UnidadAcademica> lstUnidadAcademica { set; get; }
        public DateTime Fecha { get; set; }
        public String Motivo { get; set; }
        public List<ParticipanteNormal> listaParticipantesNormales { get; set; }
        public List<ParticipanteExterno> listaParticipantesExternos { get; set; }
        public string lstExternos { get; set; }
        public List<Empresa> Empresas { get; set; }
        public Int32 IdUsuarioCreador { get; set; }

        public void Fill(AbetEntities ctx, HttpSessionStateBase Session, Int32? _IdPreAgenda)
        {
            IdPreAgenda = _IdPreAgenda;
            lstSede = ctx.Sede.ToList();
            Empresas = ctx.Empresa.ToList();
            
            var participantesDisponibles = ctx.uspGetDocentesCargoNivel("CC", Session.GetPeriodoAcademicoId(),1, 0, 0).ToList();
            listaParticipantesNormales = new List<ParticipanteNormal>();
            listaParticipantesExternos = new List<ParticipanteExterno>();

            if (IdPreAgenda.HasValue)
            {
                var preAgenda = ctx.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == IdPreAgenda);
                IdSede = preAgenda.IdSede.Value;
                Fecha = preAgenda.FechaReunion.Value;
                Motivo = preAgenda.Motivo;
                IdUsuarioCreador = preAgenda.IdUsuarioCreador.Value;

                List<ParticipanteReunion> participantesInscritos;
                List<ParticipanteReunion> participantesInscritosExternos;
                participantesInscritos = ctx.ParticipanteReunion.Where(x => x.IdPreAgenda == IdPreAgenda && !x.EsExterno).ToList();
                participantesInscritosExternos = ctx.ParticipanteReunion.Where(x => x.IdPreAgenda == IdPreAgenda && x.EsExterno).ToList();

                foreach (var itemDisponible in participantesDisponibles)
                {
                    if (participantesInscritos != null)
                    {
                        ParticipanteNormal objParticipante = new ParticipanteNormal();
                        objParticipante.IdParticipante = itemDisponible.IdDocente;
                        objParticipante.Nivel = itemDisponible.Nivel.ToString();
                        objParticipante.Nombre = itemDisponible.NombreDocente;
                        objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                        objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                        objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                        if (participantesInscritos.Exists(x => x.IdDocente == itemDisponible.IdDocente))
                        {
                            objParticipante.IsSelected = true;
                        }
                        else
                        {
                            objParticipante.IsSelected = false;
                        }

                        listaParticipantesNormales.Add(objParticipante);
                    }
                }

                for (int i = 0; i < participantesInscritosExternos.Count; i++)
                {
                    ParticipanteExterno objParticipante = new ParticipanteExterno();
                    objParticipante.IdParticipante = participantesInscritosExternos[i].IdParticipanteReunion;
                    objParticipante.Nombre = participantesInscritosExternos[i].NombreCompleto;
                    objParticipante.Cargo = participantesInscritosExternos[i].Cargo;
                    objParticipante.Correo = participantesInscritosExternos[i].Correo;
                    objParticipante.IdEmpresa = participantesInscritosExternos[i].Empresa.IdEmpresa;
                    objParticipante.NombreEmpresa = participantesInscritosExternos[i].Empresa.RazonSocial;

                    listaParticipantesExternos.Add(objParticipante);

                    string externo = string.Format("{0}|{1}|{2}|{3}", objParticipante.Nombre, objParticipante.Cargo, objParticipante.Correo, objParticipante.IdEmpresa);
                    if (i != participantesInscritosExternos.Count - 1)
                        lstExternos += externo + "*";
                    else
                        lstExternos += externo;
                }
            }
            else
            {
                Fecha = DateTime.Now;
                IdUsuarioCreador = Session.GetDocenteId().Value;
                foreach (var itemDisponible in participantesDisponibles)
                {
                    listaParticipantesNormales.Add(new ParticipanteNormal()
                    {
                        IdParticipante = itemDisponible.IdDocente,
                        Nivel = itemDisponible.Nivel.ToString(),
                        Nombre = itemDisponible.NombreDocente,
                        IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable,
                        IdUnidadAcademica = itemDisponible.IdUnidadAcademica,
                        UnidadAcademica = itemDisponible.CargoEspanol,
                        IsSelected = false
                    });
                }
            }
            
            //EL USUARIO TIENE QUE ESTAR MARCADO
            if (IdUsuarioCreador != 0)
            {
                var user = listaParticipantesNormales.FirstOrDefault(x => x.IdParticipante == IdUsuarioCreador && !x.IsSelected);
                if (user != null)
                    user.IsSelected = true;
            }

        }

    }
}