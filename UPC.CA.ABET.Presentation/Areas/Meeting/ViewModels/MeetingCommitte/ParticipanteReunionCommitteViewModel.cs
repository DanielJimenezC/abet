﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte
{
    public class ParticipanteReunionCommitteViewModel
    {

        public ParticipanteReunion Participante { get; set; }
        public String Nivel { get; set; }
        public String UnidadAcademica { get; set; }        
        public bool EstaSeleccionado { get; set; }
    }
}