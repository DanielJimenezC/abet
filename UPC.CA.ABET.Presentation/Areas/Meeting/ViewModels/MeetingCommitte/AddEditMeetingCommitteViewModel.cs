﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte
{
    public class AddEditMeetingCommitteViewModel
    {
        public Int32 IdPreAgenda { get; set; }
        public Int32? IdReunion { get; set; }

        public Int32 IdUnidadAcademica { get; set; }
        public String Motivo { get; set; }
        public Int32 IdSede { get; set; }
        public List<Sede> lstSede { get; set; }

        public DateTime Fecha { get; set; }
        public String Lugar { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }

        [AllowHtml]
        public String Objetivo { get; set; }
        [AllowHtml]
        public String Agenda { get; set; }

        public Int32 IdUsuarioCreador { get; set; }
        public List<ParticipanteNormal> listaParticipantesNormales { get; set; }
        public List<ParticipanteExterno> listaParticipantesExternos { get; set; }
        public List<ParticipanteExterno> listaParticipantesExternosPreAgenda { get; set; }
        public string lstExternos { get; set; }
        public List<Empresa> Empresas { get; set; }

        public void Fill(AbetEntities ctx, HttpSessionStateBase Session, Int32 _IdPreAgenda, Int32? _IdReunion)
        {
            IdReunion = _IdReunion;
            IdPreAgenda = _IdPreAgenda;
            lstSede = ctx.Sede.ToList();
            Empresas = ctx.Empresa.ToList();

            var preAgenda = ctx.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == IdPreAgenda);
            IdUsuarioCreador = preAgenda.IdUsuarioCreador.Value;
            IdUnidadAcademica = preAgenda.IdUnidaAcademica;
            
            listaParticipantesNormales = new List<ParticipanteNormal>();
            listaParticipantesExternos = new List<ParticipanteExterno>();
            listaParticipantesExternosPreAgenda = new List<ParticipanteExterno>();

            //var participantesDisponibles = ctx.uspGetDocentesCargoNivel("CC", preAgenda.IdPeriodoAcademico, preAgenda.IdSede, 0, 0).ToList();
            var participantesDisponibles = ctx.uspGetDocentesCargoNivel("CC", preAgenda.IdSubModalidadPeriodoAcademico, preAgenda.IdSede, 0, 0).ToList();
            var participantesExternosPreAgenda = ctx.ParticipanteReunion.Where(x => x.IdPreAgenda == IdPreAgenda && x.EsExterno).ToList();

            for (int i = 0; i < participantesExternosPreAgenda.Count; i++)
            {
                ParticipanteExterno objParticipante = new ParticipanteExterno();
                objParticipante.IdParticipante = participantesExternosPreAgenda[i].IdParticipanteReunion;
                objParticipante.Nombre = participantesExternosPreAgenda[i].NombreCompleto;
                objParticipante.Cargo = participantesExternosPreAgenda[i].Cargo;
                objParticipante.Correo = participantesExternosPreAgenda[i].Correo;
                objParticipante.IdEmpresa = participantesExternosPreAgenda[i].Empresa.IdEmpresa;
                objParticipante.NombreEmpresa = participantesExternosPreAgenda[i].Empresa.RazonSocial;
                
                if(!IdReunion.HasValue)
                {
                    objParticipante.IsSelected = true;
                    listaParticipantesExternos.Add(objParticipante);
                    string externo = string.Format("{0}|{1}|{2}|{3}", objParticipante.Nombre, objParticipante.Cargo, objParticipante.Correo, objParticipante.IdEmpresa);
                    if (i != participantesExternosPreAgenda.Count - 1)
                        lstExternos += externo + "*";
                    else
                        lstExternos += externo;
                }

                listaParticipantesExternosPreAgenda.Add(objParticipante);
            }

            if (IdReunion.HasValue)
            {
                var reunion = ctx.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion);

                Motivo = reunion.Motivo;
                IdSede = preAgenda.IdSede ?? 0;
                Fecha = reunion.Fecha;
                Lugar = reunion.Lugar;
                HoraInicio = reunion.HoraInicio;
                HoraFin = reunion.HoraFin;
                Objetivo = reunion.ObjetivoComiteConsultivo;
                Agenda = reunion.AgendaComiteConsultivo;

                List<ParticipanteReunion> participantesInscritos;
                List<ParticipanteReunion> participantesInscritosExternos;

                participantesInscritos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && !x.EsExterno).ToList();
                participantesInscritosExternos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && x.EsExterno).ToList();


                foreach (var itemDisponible in participantesDisponibles)
                {
                    if (participantesInscritos != null)
                    {
                        ParticipanteNormal objParticipante = new ParticipanteNormal();
                        objParticipante.IdParticipante = itemDisponible.IdDocente;
                        objParticipante.Nivel = itemDisponible.Nivel.ToString();
                        objParticipante.Nombre = itemDisponible.NombreDocente;
                        objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                        objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                        objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                        if (participantesInscritos.Exists(x => x.IdDocente == itemDisponible.IdDocente))
                        {
                            objParticipante.IsSelected = true;
                        }
                        else
                        {
                            objParticipante.IsSelected = false;
                        }

                        listaParticipantesNormales.Add(objParticipante);
                    }
                }

                for (int i = 0; i < participantesInscritosExternos.Count; i++)
                {
                    ParticipanteExterno objParticipante = new ParticipanteExterno();
                    objParticipante.IdParticipante = participantesInscritosExternos[i].IdParticipanteReunion;
                    objParticipante.Nombre = participantesInscritosExternos[i].NombreCompleto;
                    objParticipante.Cargo = participantesInscritosExternos[i].Cargo;
                    objParticipante.Correo = participantesInscritosExternos[i].Correo;
                    objParticipante.IdEmpresa = participantesInscritosExternos[i].Empresa.IdEmpresa;
                    objParticipante.NombreEmpresa = participantesInscritosExternos[i].Empresa.RazonSocial;
                    listaParticipantesExternos.Add(objParticipante);

                    if (listaParticipantesExternosPreAgenda.Exists(x => x.Nombre == objParticipante.Nombre && x.Cargo == objParticipante.Cargo && x.NombreEmpresa == objParticipante.NombreEmpresa))
                    {
                        listaParticipantesExternosPreAgenda.FirstOrDefault(x => x.Nombre == objParticipante.Nombre && x.Cargo == objParticipante.Cargo && x.NombreEmpresa == objParticipante.NombreEmpresa).IsSelected = true;
                    }

                    string externo = string.Format("{0}|{1}|{2}|{3}", objParticipante.Nombre, objParticipante.Cargo, objParticipante.Correo, objParticipante.IdEmpresa);
                    if (i != participantesInscritosExternos.Count - 1)
                        lstExternos += externo + "*";
                    else
                        lstExternos += externo;
                }
            }
            else
            {
                Motivo = preAgenda.Motivo;
                Fecha = DateTime.Now;
                HoraInicio = DateTime.Now;
                HoraFin = DateTime.Now;
                //COJO LOS PARTICIPANTES DE LA PREAGENDA QUE FUERON SELECCIONADOS
                var participantesInscritosPreAgenda = ctx.ParticipanteReunion.Where(x => x.IdPreAgenda == IdPreAgenda && !x.EsExterno).ToList();

                foreach (var itemDisponible in participantesDisponibles)
                {

                    ParticipanteNormal objParticipante = new ParticipanteNormal();
                    objParticipante.IdParticipante = itemDisponible.IdDocente;
                    objParticipante.Nivel = itemDisponible.Nivel.ToString();
                    objParticipante.Nombre = itemDisponible.NombreDocente;
                    objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                    objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                    objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                    if (participantesInscritosPreAgenda.Exists(x => x.IdDocente == itemDisponible.IdDocente))
                        objParticipante.IsSelected = true;
                    else
                        objParticipante.IsSelected = false;

                    listaParticipantesNormales.Add(objParticipante);
                }
            }
            
            //EL USUARIO TIENE QUE ESTAR MARCADO
            if (IdUsuarioCreador != 0)
            {
                var user = listaParticipantesNormales.FirstOrDefault(x => x.IdParticipante == IdUsuarioCreador && !x.IsSelected);
                if (user != null)
                    user.IsSelected = true;
            }

        }
    }

    public class Participante
    {
        public int IdParticipante { get; set; }
        public string Nombre { get; set; }
        public bool IsSelected { get; set; }
    }

    public class ParticipanteNormal : Participante
    {
        public string Nivel { get; set; }
        public Int32 IdUnidadAcademicaResponsable { get; set; }
        public Int32 IdUnidadAcademica { get; set; }
        public string UnidadAcademica { get; set; }
    }

    public class ParticipanteExterno : Participante
    {
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public int IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
    }
}