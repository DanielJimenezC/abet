﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte
{
    public class ConsultPreScheduleCommitteViewModel
    {
        public int IdSede { get; set; }
        public int IdArea { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public IEnumerable<SelectListItem> Semanas { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }
        public List<PreAgenda> listaResultado { get; set; }


        public void CargarDatos(CargarDatosContext datacontext, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, bool? isSearchRequest)
        {
            var idDocente = datacontext.session.GetDocenteId();
            //var PeriodoAcademicoId = datacontext.session.GetPeriodoAcademicoId();
            var idSubModalidadPeriodoAcademico = datacontext.session.GetSubModalidadPeriodoAcademicoId();
            var idEscuela = datacontext.session.GetEscuelaId();
            listaResultado = new List<PreAgenda>();
            this.Sedes = GedServices.GetAllSedesServices(datacontext.context).Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            //var periodoAcademico = datacontext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == PeriodoAcademicoId);
            var subModalidadPeriodoAcademico = datacontext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == datacontext.context.SubModalidadPeriodoAcademico.FirstOrDefault
                                                                                    (y => y.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).IdPeriodoAcademico);
            //this.Areas = GedServices.GetAllCarrerasInPeriodoAcademico(datacontext.context, (int)PeriodoAcademicoId, escuelaId).Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = c.NombreEspanol });
            this.Areas = GedServices.GetAllCarrerasInPeriodoAcademico(datacontext.context, (int)idSubModalidadPeriodoAcademico, idEscuela).Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = c.NombreEspanol });
            //this.FechaInicio = periodoAcademico.FechaInicioCiclo;
            this.FechaInicio = subModalidadPeriodoAcademico.FechaInicioPeriodo;
            //this.FechaFin = periodoAcademico.FechaFinCiclo;
            this.FechaFin = subModalidadPeriodoAcademico.FechaFinPeriodo;
            listaResultado = datacontext.context.PreAgenda.Where(x=>x.TipoPreagenda== ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO).ToList();
            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                this.listaResultado = GedServices.GetPreAgendasBusquedaServices(datacontext.context, null, null, FechaInicio, FechaFin, idSede,null, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO);
            }
        }

    }
    
   
}