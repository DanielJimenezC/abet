﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AddCommentViewModel
    {
        public Int32 IdReunionDelegado { get; set; }
        public Int32? Identificador { get; set; }
        public String SedeNombre { get; set; }
        public String CicloAcademico { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        [Required(ErrorMessage = "Debes seleccionar un alumno.")]
        public Int32 IdAlumno { get; set; }
        public Int32 IdAlumnoInvitado { get; set; }
        public Int32 IdSeccion { get; set; }
        [Required(ErrorMessage = "Debes seleccionar un docente.")]
        public Int32 IdDocente { get; set; }
        [Required(ErrorMessage = "Debes seleccionar un curso.")]
        public Int32 IdCurso { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdSede { get; set; }
        // public List<OutcomeComision> LstOutcome { get; set; }
        // public List<string> LstOutcomeDescripcion { get; set; }
        public List<SelectListItem> LstAlumno { get; set; }
        public List<SelectListItem> LstSeccion { get; set; }
        public List<SelectListItem> LstDocentes { get; set; }
        public List<SelectListItem> LstAlumoInvitado { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdCriticidad { get; set; }

        public List<NivelAceptacionHallazgo> LstnivelesAceptacion { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }

        public Int32? IdOutcome { get; set; }
        public Int32? IdCarrera { get; set; }

        public List<ComentarioDelegado> LstHallazgo { get; set; }
        public List<SelectListItem> LstCurso { get; set; }

        public Int32 IdHallazgo { get; set; }
        [Required(ErrorMessage = "Debes escribir un comentario.")]
        public String DescripcionEspanol { get; set; }
        [Required(ErrorMessage = "You must add a comment.")]
        public String DescripcionIngles { get; set; }

        public Hallazgo hallazgo { get; set; }

        public AddCommentViewModel()
        {
            LstAlumno = new List<SelectListItem>();
            LstSeccion = new List<SelectListItem>();
            LstDocentes = new List<SelectListItem>();
            LstAlumoInvitado = new List<SelectListItem>();
            // LstOutcome = new List<OutcomeComision>();
            // LstOutcomeDescripcion = new List<string>();
            LstnivelesAceptacion = new List<NivelAceptacionHallazgo>();
            LstCriticidad = new List<Criticidad>();

            LstHallazgo = new List<ComentarioDelegado>();
            hallazgo = new Hallazgo();


            LstCurso = new List<SelectListItem>();
        }

        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32? IdSubModalidadPeriodoAcademico, string codalumnosJoin)
        {
            string[] arrCodAlumnos = codalumnosJoin.Split(new char[] { ';' });
    

            AbetEntities context = dataContext.context;
            IdReunionDelegado = idReunionDelegado;
            var ReunionDelegado = context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == IdReunionDelegado);

            LstHallazgo = context.ComentarioDelegado.Where(x => x.IdReunionDelegado == IdReunionDelegado).ToList();



            Identificador = ReunionDelegado.Identificador;
            IdSede = ReunionDelegado.IdSede;
            IdPeriodoAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            SedeNombre = ReunionDelegado.Sede.Nombre;
            Fecha = ReunionDelegado.Fecha;
            CicloAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;

            IdNivelAceptacionHallazgo = 1;
            IdCriticidad = 2;

            //  LstCurso = (from cpa in context.CursoPeriodoAcademico
            //             join c in context.Curso on cpa.IdCurso equals c.IdCurso
            //where cpa.IdPeriodoAcademico == IdPeriodoAcademico && c.NombreEspanol.Trim()!=""
            //            where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && c.NombreEspanol.Trim() != ""
            //           select new SelectListItem { Text = c.NombreEspanol, Value = c.IdCurso.ToString() }).OrderBy(x=>x.Text).ToList();

            LstAlumno = (from am in context.AlumnoMatriculado
                         join u in context.Alumno on am.IdAlumno equals u.IdAlumno
                         where am.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && am.IdSede == IdSede && arrCodAlumnos.Contains(am.IdAlumno.ToString())
                         select new SelectListItem { Text = u.Codigo + " | " + u.Nombres + " " + u.Apellidos, Value = u.IdAlumno.ToString() }).Distinct().ToList();


            //            LstDocentes = (from d in context.Docente
            //                         select new SelectListItem { Text = d.Nombres + " " + d.Apellidos, Value = d.IdDocente.ToString() }).Distinct().OrderBy(X => X.Text).ToList();

            //LstAlumoInvitado = (from d in context.AlumnoInvitado
            //                      where arrCodAlumnos.Contains(d.codigo)
            //                      select new SelectListItem { Text = d.codigo + " | " + d.nombres + " " + d.apellido, Value = d.idAlumnoInvitado.ToString() }).Distinct().ToList();
            LstAlumoInvitado = (from d in context.AlumnoInvitado where d.IdReunionDelegado == IdReunionDelegado
                                select new SelectListItem { Text = d.codigo + " | " + d.nombres + " " + d.apellido, Value = d.idAlumnoInvitado.ToString() }).Distinct().ToList();

            LstAlumno.Insert(0, new SelectListItem { Value = "", Text = "[- Sin seleccionar -]" });
            LstCurso.Insert(0, new SelectListItem { Value = "", Text = "[- Selecciona Un Alumno -]" });
            LstDocentes.Insert(0, new SelectListItem { Value = "", Text = "[- Seleccion Un Curso -]" });

            LstAlumoInvitado.Insert(0, new SelectListItem { Value = "", Text = "[- Sin seleccionar -]" });
            //LstnivelesAceptacion = context.NivelAceptacionHallazgo.ToList();
            //LstCriticidad = context.Criticidad.ToList();
        }
    }
}