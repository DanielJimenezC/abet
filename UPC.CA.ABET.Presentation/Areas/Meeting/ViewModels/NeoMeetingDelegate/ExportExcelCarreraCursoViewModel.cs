﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class ExportExcelCarreraCursoViewModel
    {
        public Int32 IdSede { get; set; }

        // public Int32? IdCurso { get; set; }
        //public PeriodoAcademico Ciclo { get; set; }
        //public Int32 IdCiclo { get; set; }
        //public Int32 IdEscuela { get; set; }
        //public Int32? IdReunionDelegado { get; set; }
        //public List<Curso> LstCurso { get; set; }
        //public List<Sede> LstSede { get; set; }
        public List<SelectListItem> LstCarrera { get; set; }
        public List<SelectListItem> LstArea { get; set; }
        public List<SelectListItem> LstSubArea { get; set; }
        public List<SelectListItem> LstPeriodoAcademico{ get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdArea { get; set; }
        public Int32 IdSubArea { get; set; }
        public Int32? IdPeriodo{ get; set; }
        //public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        //public List<Hallazgo> LstHallazgo { get; set; }

        //public String Comentario { get; set; }

        //public List<Int32> LstIdCursos { get; set; }

        public ExportExcelCarreraCursoViewModel()
        {
            //LstCurso = new List<Curso>();
            //LstSede = new List<Sede>();
            //LstPeriodoAcademico = new List<PeriodoAcademico>();
            //LstIdCursos = new List<Int32>();
            LstCarrera = new List<SelectListItem>();
            LstArea = new List<SelectListItem>();
            LstSubArea = new List<SelectListItem>();
            LstPeriodoAcademico = new List<SelectListItem>();
        }

        public void Fill(CargarDatosContext dataContext, Int32? IdSubModalidadPeriodoAcademico, Int32? EscuelaId, Int32? idsubmodalidad, Int32? idPeriodo, string language = "es-PE")
        {
            //IdReunionDelegado = idReunionDelegado;
            //IdCiclo = CicloId.Value;
            //IdEscuela = EscuelaId.Value;
            //string dateString = DateTime.Now.ToString("MM/dd/yyyy");


            //LstSede = (from b in dataContext.context.ReunionDelegado
            //           join a in dataContext.context.Asistencia on b.IdReunionDelegado equals a.idReunionDelegado
            //           select b.Sede).Distinct().ToList();
            IdPeriodo = idPeriodo;
            var Query = dataContext.context.Carrera;

            IdCarrera = Query.FirstOrDefault(x => x.IdEscuela == EscuelaId && x.IdSubmodalidad == idsubmodalidad).IdCarrera;

            LstCarrera = Query.Where(x => x.IdEscuela == EscuelaId && x.IdSubmodalidad == idsubmodalidad).Select(u => new SelectListItem
            {
                Text = u.NombreEspanol,
                Value = u.IdCarrera.ToString(),
                Selected = (u.IdCarrera == IdCarrera)
                
            }).ToList();


            int IdCarreraPeriodoAcademico = 0;
            if (dataContext.context.CarreraPeriodoAcademico.Any(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico))
            {
                IdCarreraPeriodoAcademico = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdCarreraPeriodoAcademico;
            }

            var query = dataContext.context.UnidadAcademica.Where( x => x.IdCarreraPeriodoAcademico == IdCarreraPeriodoAcademico && x.IdEscuela == EscuelaId);

            LstArea = (from c in query
                       where c.Nivel == 1 
                       select new SelectListItem
                          { Text = c.NombreEspanol, Value = c.IdUnidadAcademica.ToString() }
                          ).ToList();
           
            LstSubArea = (from u in query
                          where u.Nivel == 2
                          select new SelectListItem
                        { Text = u.NombreEspanol, Value = u.IdUnidadAcademica.ToString() }
                        ).ToList();
           
            LstPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == idsubmodalidad).Select(x => new SelectListItem() { Value = x.IdPeriodoAcademico.ToString(), Text = x.PeriodoAcademico.CicloAcademico, Selected = ( x.IdPeriodoAcademico == IdPeriodo) }).ToList();



            if (language == "es-PE")
            {
                LstArea.Add(new SelectListItem { Text = "Todos", Value = "0" });
                LstSubArea.Add(new SelectListItem { Text = "Todos", Value = "0" });
            }
            else {
                LstArea.Add(new SelectListItem { Text = "All", Value = "0" });
                LstSubArea.Add(new SelectListItem { Text = "All", Value = "0" });
            }

            LstArea = LstArea.OrderBy(x => x.Value).ToList();
            LstSubArea = LstSubArea.OrderBy(x => x.Value).ToList();
            //LstHallazgo = dataContext.context.Hallazgo.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado != "INA").OrderBy(x => x.IdCriticidad).ToList();

            //if (IdReunionDelegado.HasValue)
            //{
            // ReunionDelegado reunionDelegado = new ReunionDelegado();
            //var reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado.Value);

            //if (reunionDelegado == null)
            //    return;

            //IdCiclo = reunionDelegado.IdPeriodoAcademico;
            //IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            //IdEscuela = reunionDelegado.IdEscuela;
            //IdSede = reunionDelegado.IdSede;
            //Comentario = reunionDelegado.Comentario;
            //}
        }
    }
}