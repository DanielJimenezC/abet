﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class _ConfirmacionDeleteARDViewModel
    {
        public Int32 IdReunionDelegado { get; set; }

        public ReunionDelegado rd { get; set; }

        public List<String> lstNombreCursos { get; set; }

        public _ConfirmacionDeleteARDViewModel()
        {

        }
        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;


            IdReunionDelegado = idReunionDelegado;
            rd = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);



        }
    }
}