﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AddReuCommentDelegateViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public Int32 IdSede { get; set; }
        // public Int32? IdCurso { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public Int32? IdReunionDelegado { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Sede> LstSede { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public String Comentario { get; set; }

        public List<Int32> LstIdCursos { get; set; }

        public AddReuCommentDelegateViewModel()
        {
            LstCurso = new List<Curso>();
            LstSede = new List<Sede>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstIdCursos = new List<Int32>();
        }

        public void Fill(CargarDatosContext dataContext, Int32? idReunionDelegado, Int32? CicloId, Int32? EscuelaId)
        {
            IdReunionDelegado = idReunionDelegado;
            IdCiclo = CicloId.Value;
            IdEscuela = EscuelaId.Value;
            Fecha = DateTime.Now;

            LstSede = dataContext.context.Sede.ToList();

            if (IdReunionDelegado.HasValue)
            {
                // ReunionDelegado reunionDelegado = new ReunionDelegado();
                var reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado.Value);
                if (reunionDelegado == null)
                    return;

                //IdCiclo = reunionDelegado.IdPeriodoAcademico;
                IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                IdEscuela = reunionDelegado.IdEscuela;
                IdSede = reunionDelegado.IdSede;

                Fecha = reunionDelegado.Fecha.Value.Date;
                Comentario = reunionDelegado.Comentario;
            }
        }
    }
}