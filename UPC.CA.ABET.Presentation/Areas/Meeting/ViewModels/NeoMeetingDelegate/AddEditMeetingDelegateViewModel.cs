﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AddEditMeetingDelegateViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]


        public Int32 IdSede { get; set; }
        // public Int32? IdCurso { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public List<SelectListItem> ListFecha { get; set; }
        public Int32? IdReunionDelegado { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<SelectListItem> LstSede { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public String Comentario { get; set; }

        public List<Int32> LstIdCursos { get; set; }

        public AddEditMeetingDelegateViewModel()
        {
            LstCurso = new List<Curso>();
            LstSede = new List<SelectListItem>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstIdCursos = new List<Int32>();
            ListFecha = new List<SelectListItem> ();
        }

        public void Fill(CargarDatosContext dataContext, Int32? idReunionDelegado, Int32? CicloId, Int32? EscuelaId)
        {
            IdReunionDelegado = idReunionDelegado;
            IdCiclo = CicloId.Value;
            IdEscuela = EscuelaId.Value;

            LstSede = (from s in dataContext.context.Sede
                       select new SelectListItem { Value = s.IdSede.ToString() , Text = s.Nombre }).ToList();

            LstSede.Insert(0, new SelectListItem { Value = "0", Text = "[- Seleccion Una Sede -]", Selected = true });
            ListFecha.Insert(0, new SelectListItem { Value = "0", Text = "[- Primero Selecciona  una Sede -]", Selected = true });
            if (IdReunionDelegado.HasValue)
            {
               // ReunionDelegado reunionDelegado = new ReunionDelegado();
                var reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado.Value);
                if (reunionDelegado == null)
                    return;

                //IdCiclo = reunionDelegado.IdPeriodoAcademico;
                IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                IdEscuela = reunionDelegado.IdEscuela;
                IdSede = reunionDelegado.IdSede;

                Comentario = reunionDelegado.Comentario;
            }
        }
    }
}