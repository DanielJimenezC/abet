﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AlumnoAsistencia
    {
        public Int32 IdReunionDelegadoAlumno { get; set; }
        public Int32 IdAlumno { get; set; }
        public String AlumnoCodigo { get; set; }
        public String NombreCompleto { get; set; }
        public String Delegado { get; set; }
    }


    public class AddStudentViewModel
    {
         public Int32 IdReunionDelegado { get; set; }
         public String SedeNombre { get; set; }
        public String CicloAcademico { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }
        public Int32 IdAlumno { get; set; }
        //public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public List<SelectListItem> LstAlumno { get; set; }

        public List<AlumnoAsistencia> LstAlumnoAsistencia { get; set; }

        public AddStudentViewModel()
        {
            LstAlumno = new List<SelectListItem>();
            LstAlumnoAsistencia = new List<AlumnoAsistencia>();
        }

        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            AbetEntities context = dataContext.context;
            IdReunionDelegado = idReunionDelegado;
            var ReunionDelegado = context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == IdReunionDelegado);

            int IdSede = ReunionDelegado.IdSede;
            //IdPeriodoAcademico = ReunionDelegado.IdPeriodoAcademico;
            IdSubModalidadPeriodoAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            SedeNombre = ReunionDelegado.Sede.Nombre;
            Fecha = ReunionDelegado.Fecha;
            //CicloAcademico = ReunionDelegado.PeriodoAcademico.CicloAcademico;
            CicloAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;


            LstAlumno = (from a in context.Alumno
                          join am in context.AlumnoMatriculado on a.IdAlumno equals am.IdAlumno
                                join als in context.AlumnoSeccion on am.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                                join s in context.Seccion on als.IdSeccion equals s.IdSeccion
                                join cpa in context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                                //where s.IdSede == IdSede && cpa.IdPeriodoAcademico == IdPeriodoAcademico
                                where s.IdSede == IdSede && cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdSubModalidadPeriodoAcademico
                         select new SelectListItem { Value = a.IdAlumno.ToString(), Text = a.Codigo + " - " + a.Apellidos } ).Distinct().ToList();


            LstAlumnoAsistencia = dataContext.context.ReunionDelegadoAlumno.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => new AlumnoAsistencia { IdReunionDelegadoAlumno = x.IdReunionDelegadoAlumno, IdAlumno = x.IdAlumno }).ToList() ;


            for(int i = 0; i< LstAlumnoAsistencia.Count; i++)
            {
                int idalumno = LstAlumnoAsistencia[i].IdAlumno;
                Alumno alumno = dataContext.context.Alumno.FirstOrDefault(x => x.IdAlumno == idalumno);


                LstAlumnoAsistencia[i].AlumnoCodigo = alumno.Codigo;
                LstAlumnoAsistencia[i].NombreCompleto = alumno.Nombres+" "+alumno.Apellidos;

                //var alumnosecciones = context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == idalumno && x.Seccion.CursoPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).ToList();
                var alumnosecciones = context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == idalumno && x.Seccion.CursoPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdSubModalidadPeriodoAcademico).ToList();
                var alumnosecciondelegado = alumnosecciones.FirstOrDefault(x => x.EsDelegado);

                LstAlumnoAsistencia[i].Delegado = "-";
                if (alumnosecciondelegado != null)
                {

                    LstAlumnoAsistencia[i].Delegado =alumnosecciondelegado.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol + " | Sección " + alumnosecciondelegado.Seccion.Codigo;
                }



            }

        }



    }
}