﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;



namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AddEditFindingViewModel
    {
        public string CurrentCulture { get; set; }
        public Int32 IdReunionDelegado { get; set; }
        public Int32? Identificador { get; set; }
        public String SedeNombre { get; set; }
        public String CicloAcademico { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }
        public Int32 IdAlumno { get; set; }
        public Int32 IdAlumnoInvitado { get; set; }
        public Int32 IdSeccion { get; set; }
        public Int32 IdDocente { get; set; }
        public Int32 IdCurso { get; set; }
        public Int32 IdArea { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdSede { get; set; }
       // public List<OutcomeComision> LstOutcome { get; set; }
       // public List<string> LstOutcomeDescripcion { get; set; }
        public List<SelectListItem> LstAlumno { get; set; }
        public List<SelectListItem> LstSeccion { get; set; }
        public List<SelectListItem> LstDocentes { get; set; }
        public List<SelectListItem> LstAlumoInvitado { get; set; }
        public Int32? IdNivelAceptacionHallazgo {get; set;}
        public Int32? IdCriticidad { get; set; }

        public List<NivelAceptacionHallazgo> LstnivelesAceptacion { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }

        public Int32? IdOutcome { get; set; }
        public Int32? IdCarrera { get; set; }

        public List<Hallazgos> LstHallazgo { get; set; }
        public List<ObservacionDelegado> LstObservacion { get; set; }
        public List<SelectListItem> LstCurso { get; set; }
        public List<SelectListItem> LstArea { get; set; }

        public Int32 IdHallazgo { get; set; }
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }

        public Hallazgo hallazgo { get; set; }

        public AddEditFindingViewModel()
        {
            LstAlumno = new List<SelectListItem>();
            LstSeccion = new List<SelectListItem>();
            LstDocentes = new List<SelectListItem>();
            LstAlumoInvitado = new List<SelectListItem>();
            LstnivelesAceptacion = new List<NivelAceptacionHallazgo>();
            LstCriticidad = new List<Criticidad>();
            LstHallazgo = new List<Hallazgos>();
            hallazgo = new Hallazgo();
            LstObservacion = new List<ObservacionDelegado>();
            LstCurso = new List<SelectListItem>();
            LstArea = new List<SelectListItem>();
        }

        public void FillEdit(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32 idHallazgo)
        {
            IdReunionDelegado = idReunionDelegado;
            IdHallazgo = idHallazgo;
            hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
            IdOutcome = hallazgo.IdOutcome;
            IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
            IdCriticidad = hallazgo.IdCriticidad;
            IdCarrera = hallazgo.IdCarrera;
            DescripcionEspanol = hallazgo.DescripcionEspanol;
            LstnivelesAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();
        }

        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado, int IdEscuela)
        {
            AbetEntities context = dataContext.context;

            IdReunionDelegado = idReunionDelegado;
            var FechaReun = (from reu in context.ReunionDelegado
                                        where reu.IdReunionDelegado == idReunionDelegado
                                        select reu.Fecha).FirstOrDefault();
            var ReunionDelegado = context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado);

            Int32? IdSubModalidadPeriodoAcademico = ReunionDelegado.IdSubModalidadPeriodoAcademico;

            Identificador = ReunionDelegado.Identificador;
            IdSede = ReunionDelegado.IdSede;
            IdPeriodoAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            SedeNombre = ReunionDelegado.Sede.Nombre;
            Fecha = ReunionDelegado.Fecha;
            CicloAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            
            IdNivelAceptacionHallazgo = 1;
            IdCriticidad = 2;
        
            try
            {
               
                LstHallazgo = (from a in context.Hallazgos
                               join b in context.ComentarioHallazgo on a.IdHallazgo equals b.IdHallazgo
                               join c in context.ComentarioDelegado on b.IdComentarioDelegado equals c.IdComentarioDelegado
                               join d in context.ReunionDelegado on c.IdReunionDelegado equals d.IdReunionDelegado
                               where c.IdReunionDelegado == idReunionDelegado && d.Estado != "INA"
                               select a).Distinct().OrderBy(a => a.IdCriticidad).ToList();

            }
            catch (Exception ex)
            {
                var x = context.Hallazgo.Where(y => y.IdReunionDelegado == idReunionDelegado && y.Estado != "INA").OrderBy(y => y.IdCriticidad).ToList();
            }
        
            LstObservacion = context.ObservacionDelegado.Where(x => x.IdReunionDelegado == idReunionDelegado && x.Estado != "INA").OrderBy(x => x.IdCriticidad).ToList();

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            LstArea = GedServices.GetAllAreasForPeriodoAcademico(dataContext.context, (int)IdSubModalidadPeriodoAcademico, IdEscuela)
                .Select(x => new { Nombre = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol.ToUpper() : x.NombreIngles.ToUpper(), x.IdUnidadAcademica}).Distinct()
                .Select(x => new SelectListItem { Value = x.IdUnidadAcademica.ToString(), Text = x.Nombre}).ToList();

            LstArea.Insert(0, new SelectListItem { Value = "0", Text = "[- Seleccione un Área -]" });

            //LstCurso = (from cd in context.ComentarioDelegado
            //            join c in context.Curso on cd.IdCurso equals c.IdCurso
            //            where cd.IdReunionDelegado == ReunionDelegado.IdReunionDelegado
            //            select new SelectListItem { Text = c.NombreEspanol, Value = c.IdCurso.ToString() }).OrderBy(x => x.Text).Distinct().ToList();

            LstCurso.Insert(0, new SelectListItem { Value = "", Text= "[- Seleccione un Área -]" });

        }

        public void CargarDatosPartial(CargarDatosContext dataContext, Int32? idCurso, Int32 IdPeriodoAcademico)
        {
         
        }
    }
}