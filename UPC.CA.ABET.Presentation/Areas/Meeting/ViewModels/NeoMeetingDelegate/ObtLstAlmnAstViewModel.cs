﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class ObtLstAlmnAstViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public Int32 IdSede { get; set; }
    
        // public Int32? IdCurso { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public Int32? IdReunionDelegado { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<DateTime?> LstFecha { get; set; }
        public List<Sede> LstSede { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public String Comentario { get; set; }

        public List<Int32> LstIdCursos { get; set; }

        public ObtLstAlmnAstViewModel()
        {
            LstCurso = new List<Curso>();
            LstSede = new List<Sede>();
            LstFecha = new List<DateTime?>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstIdCursos = new List<Int32>();
        }

        public void Fill(CargarDatosContext dataContext, Int32? idReunionDelegado, Int32? CicloId, Int32? EscuelaId)
        {
            IdReunionDelegado = idReunionDelegado;
            IdCiclo = CicloId.Value;
            IdEscuela = EscuelaId.Value;
            Fecha = DateTime.Now;
            string dateString = DateTime.Now.ToString("MM/dd/yyyy");

            LstFecha = (from b in dataContext.context.ReunionDelegado
                                 join a in dataContext.context.Asistencia on b.IdReunionDelegado equals a.idReunionDelegado
                                  select   b.Fecha ).Distinct().ToList();

            LstSede = (from b in dataContext.context.ReunionDelegado
                                join a in dataContext.context.Asistencia on b.IdReunionDelegado equals a.idReunionDelegado
                                select  b.Sede ).Distinct().ToList();

            if (IdReunionDelegado.HasValue)
            {
                // ReunionDelegado reunionDelegado = new ReunionDelegado();
                var reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado.Value);
                if (reunionDelegado == null)
                    return;

                //IdCiclo = reunionDelegado.IdPeriodoAcademico;
                IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                IdEscuela = reunionDelegado.IdEscuela;
                IdSede = reunionDelegado.IdSede;
               

                Fecha = reunionDelegado.Fecha.Value.Date;
                Comentario = reunionDelegado.Comentario;
            }
        }
    }
}