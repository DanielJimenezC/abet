﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class PrintReunionDelegadoViewModel
    {
        public int IdReunionDelegado { get; set; }
        public ReunionDelegado reunion { get; set; }

        public List<Curso> LstCursos { get; set; }
        public List<int> LstCursoID { get; set; }
        public List<Hallazgos> LstHallazgos { get; set; }

        public List<ObservacionDelegado> LstObservaciones { get; set; }

        public List<AlumnoSeccion> LstAlumnosSeccion { get; set; }
        public List<bool> LstAsistencia { get; set; }
        public List<AlumnoAsistencia> LstAlumnoAsistencia { get; set; }

        public PrintReunionDelegadoViewModel()
        {
            reunion = new ReunionDelegado();
            LstCursos = new List<Curso>();
            LstHallazgos = new List<Hallazgos>();
            LstObservaciones = new List<ObservacionDelegado>();
            LstCursoID = new List<int>();
            LstAlumnosSeccion = new List<AlumnoSeccion>();
            LstAsistencia = new List<bool>();
            LstAlumnoAsistencia = new List<AlumnoAsistencia>();
        }

        public void CargarDatos(CargarDatosContext dataContext, int idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;

            reunion = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);

            LstCursos = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => x.Curso).ToList();
            LstCursoID = LstCursos.Select(x => x.IdCurso).ToList();

            Instrumento instrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "ARD");
            int idInstrumentoRD = 8;

            if (instrumento != null)
            {
                idInstrumentoRD = instrumento.IdInstrumento;
            }
            

            //int IdCiclo = reunion.IdPeriodoAcademico;
            int IdCiclo = reunion.SubModalidadPeriodoAcademico.IdPeriodoAcademico;

            //LstHallazgos = dataContext.context.Hallazgos.Where(x => x.ConstituyenteInstrumento.IdInstrumento == idInstrumentoRD && x.ComentarioHallazgo.First().ComentarioDelegado.IdReunionDelegado == IdReunionDelegado && x.Estado != "INA").OrderBy(x => x.IdCriticidad).ToList();

            LstHallazgos = (from a in dataContext.context.Hallazgos
                           join b in dataContext.context.ComentarioHallazgo on a.IdHallazgo equals b.IdHallazgo
                           join c in dataContext.context.ComentarioDelegado on b.IdComentarioDelegado equals c.IdComentarioDelegado
                           join d in dataContext.context.ReunionDelegado on c.IdReunionDelegado equals d.IdReunionDelegado
                           where c.IdReunionDelegado == idReunionDelegado && d.Estado != "INA"
                           select a).Distinct().OrderBy(a => a.IdCriticidad).ToList();




            LstObservaciones = dataContext.context.ObservacionDelegado.Where(x => x.IdReunionDelegado == IdReunionDelegado && x.Estado != "INA").OrderBy(x => x.IdCriticidad).ToList();
            LstAlumnoAsistencia = dataContext.context.ReunionDelegadoAlumno.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => new AlumnoAsistencia { IdReunionDelegadoAlumno = x.IdReunionDelegadoAlumno, IdAlumno = x.IdAlumno }).ToList();


            for (int i = 0; i < LstAlumnoAsistencia.Count; i++)
            {
                int idalumno = LstAlumnoAsistencia[i].IdAlumno;
                Alumno alumno = dataContext.context.Alumno.FirstOrDefault(x => x.IdAlumno == idalumno);


                LstAlumnoAsistencia[i].AlumnoCodigo = alumno.Codigo;
                LstAlumnoAsistencia[i].NombreCompleto = alumno.Nombres + " " + alumno.Apellidos;

                //var alumnosecciones = dataContext.context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == idalumno && x.Seccion.CursoPeriodoAcademico.IdPeriodoAcademico == IdCiclo).ToList();
                var alumnosecciones = dataContext.context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == idalumno && x.Seccion.CursoPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo).ToList();
                var alumnosecciondelegado = alumnosecciones.FirstOrDefault(x => x.EsDelegado);

                LstAlumnoAsistencia[i].Delegado = "-";
                if (alumnosecciondelegado != null)
                {

                    LstAlumnoAsistencia[i].Delegado = alumnosecciondelegado.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol + " | Sección " + alumnosecciondelegado.Seccion.Codigo;
                }



            }

        }
    }
}