﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class LstReunionDelegadosViewModel
    {
        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        //public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }

        public Int32? IdCurso { get; set; }

        public Int32? IdSede { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Sede> LstSede { get; set; }
        public IPagedList<ReunionDelegado> LstReunion { get; set; }
        public Int32? NumeroPagina { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public LstReunionDelegadosViewModel()
        {
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstCurso = new List<Curso>();
            LstSede = new List<Sede>();

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? numeroPagina, Int32 idPeriodoAcademico, Int32? idSede, DateTime? fecha, Int32 idEscuela)
        {
            IdPeriodoAcademico = idPeriodoAcademico;
            AbetEntities context = dataContext.context;
            IdSede = idSede;
            if (fecha.HasValue && fecha.Value.Year > 2000)
                Fecha = fecha;
            else
                Fecha = null;

            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            Ciclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == idPeriodoAcademico);
            

            LstSede = dataContext.context.Sede.ToList();

            NumeroPagina = numeroPagina ?? 1;
            // var query = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == 8 && x.Estado == "ACT").ToList();

            //var query = dataContext.context.ReunionDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == escuelaId && x.IdPeriodoAcademico == PeriodoAcademicoId).ToList();
            var query = dataContext.context.ReunionDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == idEscuela && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && x.ComentarioDelegado.Count > 0).ToList();

            if (IdSede.HasValue)
                query = query.Where(x => x.IdSede == IdSede).ToList();

            if (Fecha.HasValue)
                query = query.Where(x => x.Fecha == Fecha).ToList();

        /*    if (IdCurso.HasValue)
            {

                var reuniones = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdCurso == IdCurso).Select(x => x.IdReunionDelegado).ToList();
                query = query.Where(x => reuniones.Contains(x.IdReunionDelegado)).ToList();

            }*/




            LstReunion = query.OrderByDescending(x => x.Fecha).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

        }

    }
}