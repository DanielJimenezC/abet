﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class AddEditEvidenceViewModel
    {
        public Int32? IdEvidencia { get; set; }
        public Int32? Identificador { get; set; }
        public Int32 IdReunionDelegado { get; set; }
        public Int32 Orden { get; set; }
        public Evidencia evidencia { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase file { get; set; }
        public String SedeNombre { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }
        public String CicloAcademico { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }
        public Int32 IdAlumno { get; set; }
       //public Int32 IdSubModalidadPeriodoAcademico { get; set; }

        public List<Evidencia> LstEvidencias { get; set; }


        public AddEditEvidenceViewModel()
        {
            evidencia = new Evidencia();

            LstEvidencias = new List<Evidencia>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;
            var ReunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == IdReunionDelegado);

            LstEvidencias = dataContext.context.Evidencia.Where(x => x.IdReunionDelegado == IdReunionDelegado).OrderBy(x => x.Orden).ToList();


            int IdSede = ReunionDelegado.IdSede;
            Identificador = ReunionDelegado.Identificador;
            IdPeriodoAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            SedeNombre = ReunionDelegado.Sede.Nombre;
            Fecha = ReunionDelegado.Fecha;
            CicloAcademico = ReunionDelegado.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;




            List<Int32> lstOrden = (from e in dataContext.context.Evidencia
                                    where e.IdReunionDelegado == idReunionDelegado
                                    select e.Orden).ToList();
            if (lstOrden.Count == 0)
            {
                Orden = 0;
            }
            else
                Orden = lstOrden.Max() + 1;


        }

        public void CargarDatosEdit(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32 idEvidencia)
        {
            IdReunionDelegado = idReunionDelegado;
            var ReunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == IdReunionDelegado);
            IdEvidencia = idEvidencia;

                evidencia = dataContext.context.Evidencia.FirstOrDefault(x => x.IdEvidencia == IdEvidencia);
                Orden = evidencia.Orden;
            
        }

    }
}