﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate
{
    public class CallTheRollViewModel
    {
        #region Propiedades
        public List<SelectListItem> ListaAsistencia { get; set; }
        public Int32 IdReunionDelegado { get; set; }
        public List<Alumno> ListaAlumnos { get; set; }
        public List<AlumnoInvitado> ListaAlumnosInvitados { get; set; }
        public List<int> LstIdAlumno;
        public List<CheckboxModel> Checkboxes { get; set; }
        private AbetEntities context { get; set; }

        #endregion

        #region Metodos
        public CallTheRollViewModel()
        {
            Checkboxes = new List<CheckboxModel>();
            LstIdAlumno = new List<int>();
            //ListaAlumnos = new List<Alumno>();
        }
        public void CargarDatos(CargarDatosContext DataContext, int IdReunionDelegado, int? IdSubmoIdSubModalidadPeriodoAcademico)
        {
            this.IdReunionDelegado = IdReunionDelegado;
            int IdSede = DataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado).IdSede;
            List<AlumnoInvitado> auxListAlumnoInvitado = (from cu in DataContext.context.AlumnoInvitado where cu.IdReunionDelegado == this.IdReunionDelegado
                                                          select cu).ToList();
            List<Alumno> ListaAlumnos = (from am in DataContext.context.AlumnoMatriculado
                                         join u in DataContext.context.Alumno on am.IdAlumno equals u.IdAlumno
                                         join asa in DataContext.context.AlumnoSeccion on am.IdAlumnoMatriculado equals asa.IdAlumnoMatriculado
                                         join sc in DataContext.context.SeccionCurso on asa.IdSeccion equals sc.IdSeccion
                                         join cmc in DataContext.context.CursoMallaCurricular on sc.IdCurso equals cmc.IdCurso
                                         join mc in DataContext.context.MallaCurricular on cmc.IdMallaCurricular equals mc.IdMallaCurricular
                                         join mcpa in DataContext.context.MallaCurricularPeriodoAcademico on mc.IdMallaCurricular equals mcpa.IdMallaCurricular
                                            where am.IdSubModalidadPeriodoAcademico == IdSubmoIdSubModalidadPeriodoAcademico 
                                            && am.IdSede == IdSede 
                                            && asa.EsDelegado == true 
                                            //&& (cmc.EsFormacion == false || cmc.EsElectivo == true)
                                            && mcpa.IdSubModalidadPeriodoAcademico == IdSubmoIdSubModalidadPeriodoAcademico
                                            select u).Distinct().ToList();


            LstIdAlumno = DataContext.context.Asistencia.Where(x => x.idReunionDelegado == IdReunionDelegado).Select(x => x.idAlumno).ToList();


            for (int i = 0; i < ListaAlumnos.Count(); i++)
            {
                CheckboxModel NuevoAlumno = new CheckboxModel();
                NuevoAlumno.Value = ListaAlumnos[i].IdAlumno;
                NuevoAlumno.Text = ListaAlumnos[i].Nombres + " " + ListaAlumnos[i].Apellidos;
                NuevoAlumno.Codigo = ListaAlumnos[i].Codigo.ToString();
                if(LstIdAlumno.Find(x => x == ListaAlumnos[i].IdAlumno) != 0)
                {
                    NuevoAlumno.Checked = true;
                }else
                {
                    NuevoAlumno.Checked = false;
                }
                    

                this.Checkboxes.Add(NuevoAlumno);
            }

            ListaAlumnosInvitados = auxListAlumnoInvitado;
        }
        #endregion
    }
}