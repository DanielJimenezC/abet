﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Topic
{
    public class AddEditTemaSemanaViewModel
    {
    
        public Tema tema { get; set; }


        public Int32? IdUnidadAcademica { get; set; }
        //public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public List<UnidadAcademica> LstUnidadAcademica { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public AddEditTemaSemanaViewModel()
        {
            tema = new Tema();
            LstUnidadAcademica = new List<UnidadAcademica>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
        }

        //public void CargarDatos(CargarDatosContext dataContext,Int32? PeriodoAcademicoId, Int32? idUnidadAcademica, Int32? IdDocente, int escuelaId)
        public void CargarDatos(CargarDatosContext dataContext, Int32? idSubModalidadPeriodoAcademico, Int32? idUnidadAcademica, Int32? IdDocente, int idEscuela)
        {
            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   select pa).Distinct().ToList();

            //if (PeriodoAcademicoId.HasValue == false)
            if (idSubModalidadPeriodoAcademico.HasValue == false)
            {
                //IdPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
                idSubModalidadPeriodoAcademico = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
            }
            else
                //IdPeriodoAcademico = PeriodoAcademicoId;
                IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;

            IdUnidadAcademica = idUnidadAcademica;


            LstUnidadAcademica = (from uar in dataContext.context.UnidadAcademicaResponsable
                                  join sua in dataContext.context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                                  join ua in dataContext.context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                                  //where uar.IdDocente == IdDocente && ua.IdEscuela == escuelaId && ua.IdPeriodoAcademico == IdPeriodoAcademico
                                  where uar.IdDocente == IdDocente && ua.IdEscuela == idEscuela && ua.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdSubModalidadPeriodoAcademico
                                  select ua).Distinct().ToList();

            if(LstUnidadAcademica.Count>0)
            {
                if(IdUnidadAcademica.HasValue==false)
                IdUnidadAcademica = LstUnidadAcademica.FirstOrDefault().IdUnidadAcademica;


                //tema = dataContext.context.Tema.FirstOrDefault(x => x.IdUnidadAcademica == IdUnidadAcademica && x.IdPeriodoAcademico == IdPeriodoAcademico);
                tema = dataContext.context.Tema.FirstOrDefault(x => x.IdUnidadAcademica == IdUnidadAcademica && x.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico);


            }



        }

    }
}