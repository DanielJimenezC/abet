﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class EmployerVM
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string DNI { get; set; }
        public string Cargo { get; set; }
        public string Empresa { get; set; }
        public int Telefono { get; set; }
        public string RUC { get; set; }
        public string N_Empleados { get; set; }
        public string Evidencia { get; set; }
        public string ComentarioEspanol { get; set; }
        public string ComentarioIngles { get; set; }
        public List<ActaComiteConsultivoXEmpleador> lstACCxEmpleador { get; set; }

        public DateTime FechaACC { get; set; }
        public int userACC { get; set; }
        public int idACC { get; set; }


        public void Fill(CargarDatosContext dataContext,Int32? idACC)
        {
            lstACCxEmpleador = dataContext.context.ActaComiteConsultivoXEmpleador.Where(x=>x.IdActaReunion==idACC).ToList();
        }

    }
}