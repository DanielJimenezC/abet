﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.TeacherMeeting;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ConsultMeetingPreScheduleViewModel
    {
        public Int32 IdReunionProfesor { get; set; }
        public String Lugar { get; set; }
        public Int32 IdUnidadAcademica { get; set; }
        public String NombreReunion { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy h:ss}")]
        public DateTime Inicio { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy h:ss}")]
        public DateTime Fin { get; set; }
        public String Semana { get; set; }
        public String AreaEncargada { get; set; }
        public List<String> LstIdDocentesParticipantes { get; set; }
        public List<SelectListItem> DocentesDisponibles{ get; set; }
        public String Nivel { get; set; }

        public List<Docente> DocentesParticipantes { get; set; }

        public ConsultMeetingPreScheduleViewModel()
        {
            DocentesDisponibles = new List<SelectListItem>();
            DocentesParticipantes = new List<Docente>();
        }

        public void CargarDatos(AbetEntities ctx, int idReunionProfesor, string language)
        {
            IdReunionProfesor = idReunionProfesor;

            var reunion = (from a in ctx.ReunionProfesor
                           where a.IdReunionProfesor == idReunionProfesor
                           select a).FirstOrDefault();

            IdUnidadAcademica = reunion.IdUnidadAcademica;

            NombreReunion = (language == ConstantHelpers.CULTURE.ESPANOL ? reunion.NombreReunion : reunion.NombreIngles);
            Inicio = reunion.Inicio.Value;
            Fin = reunion.Fin.Value;
            Lugar = reunion.Lugar;

            foreach (var reunionProfesoresParticipantes in reunion.ReunionProfesorParticipante)
            {
                DocentesParticipantes.Add(reunionProfesoresParticipantes.Docente);
            }

            var idSubmodalidadPeriodoAcademicoModulo = reunion.IdSubModalidadPeriodoAcademicoModulo;

            var idSubModalidadPeriodoAcademico = ctx.SubModalidadPeriodoAcademicoModulo
                .FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == idSubmodalidadPeriodoAcademicoModulo).IdSubModalidadPeriodoAcademico;

            var dasd = (from ua in ctx.UnidadAcademica
                        join cpa in ctx.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                        join s in ctx.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                        join ds in ctx.DocenteSeccion on s.IdSeccion equals ds.IdSeccion
                        join d in ctx.Docente on ds.IdDocente equals d.IdDocente
                        join sua in ctx.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                        join uar in ctx.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                        where cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                        select d).Distinct().ToList()
                        .Concat
                        ((from ua in ctx.UnidadAcademica
                          join sua in ctx.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                          join uar in ctx.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                          join d in ctx.Docente on uar.IdDocente equals d.IdDocente
                          where ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                          select d).Distinct().ToList()).Distinct();


            var docentesSelecListQuery = (from d in dasd
                                          select new SelectListItem
                                          {
                                              Value = d.IdDocente.ToString(),
                                              Text = d.Apellidos + ", " + d.Nombres + ", (" + d.Codigo + ")",
                                              Selected = false
                                          })
                                          .ToList();
            Nivel = reunion.Nivel.ToString();
            Semana = TeacherMeetingResource.Semana.ToString() + " " + reunion.NumSemana;
            AreaEncargada = (language == ConstantHelpers.CULTURE.ESPANOL ? reunion.AreaEncargada.ToString() : reunion.AreaEncargadaIngles.ToString());
            DocentesDisponibles = ListaDocentes(reunion.ReunionProfesorParticipante, ctx, docentesSelecListQuery);
        }

        private List<SelectListItem> ListaDocentes(IEnumerable<ReunionProfesorParticipante> lstReunionDocentesParticipantes, AbetEntities ctx, List<SelectListItem> docentesSelecListQuery)
        {   
            foreach (var docenteParticipante in lstReunionDocentesParticipantes)
            {
                docentesSelecListQuery.Single(c => c.Value == docenteParticipante.IdDocente.ToString()).Selected = true;
            }
            return docentesSelecListQuery;
        }
    }
}