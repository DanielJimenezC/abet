﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ConsultMeetingExtraordinaryViewModel
    {
        public Int32? IdNivel { get; set; }
        public Int32? IdSemana { get; set; }
        public IEnumerable<SelectListItem> Niveles { get; set; }
        public IEnumerable<SelectListItem> Semanas { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public Int32? IdSede { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public IPagedList<Reunion> ListaReunionResultado { get; set; }
        public Int32? NumeroPagina { get; set; }

        public void Fill(AbetEntities ctx, HttpSessionStateBase Session, Int32? NumeroPagina, Int32? IdNivel, Int32? IdSemana, DateTime? FechaInicio, DateTime? FechaFin, Int32? IdSede)
        {
            this.NumeroPagina = NumeroPagina ?? 1;
            this.IdNivel = IdNivel;
            this.IdSemana = IdSemana;
            this.IdSede = IdSede;

            if (FechaInicio==null)
            {
                FechaFin = FechaInicio = DateTime.Now;
            }

            Niveles = ctx.UnidadAcademica.Where(x => x.Nivel > 0).Select(x => x.Nivel).Distinct().ToList().Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            Semanas = new List<int> { 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }.Select(c => new SelectListItem { Value = c.ToString().ToString(), Text = c.ToString() });
            Sedes = ctx.Sede.Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            
            //-----------------------------------------------------------------------
            var fechasSemana = GedServices.GetDateStartWeekCurrentPeriodoAcademicoBySemanaServices(ctx, Session, IdSemana ?? 0);
            //--------------------------------------------------------------

            try
            {
                this.FechaInicio = FechaInicio == DateTime.MinValue ? fechasSemana[0] : FechaInicio.Value;
                this.FechaFin = FechaFin == DateTime.MinValue ? fechasSemana[1] : FechaFin.Value;
            }
            catch { };

            var query = GedServices.GetReunionesBusquedaServices(ctx, IdNivel, IdSemana, FechaInicio, FechaFin, IdSede);
            if(query.Any())
            {
                query = query.Where(x => x.EsExtraordinaria == true).ToList();
                ListaReunionResultado = query.AsQueryable().ToPagedList(NumeroPagina ?? 1, 10);
            }
        }
    }
}