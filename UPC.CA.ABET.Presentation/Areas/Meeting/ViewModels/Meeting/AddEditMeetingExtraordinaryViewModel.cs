﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class AddEditMeetingExtraordinaryViewModel
    {
        public Int32? IdReunion { get; set; }

        public Int32 IdNivel { get; set; }
        public List<Int32> listaNiveles { get; set; }

        public Int32 IdUnidadAcademica { get; set; }
        public List<UnidadAcademica> listaUnidadesAcademicas { get; set; }
        
        public String Motivo { get; set; }
        public Int32 IdSede { get; set; }
        public List<Sede> lstSede { get; set; }
        public DateTime Fecha { get; set; }
        public String Lugar { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }

        public string lstTemas { get; set; }
        public List<string> listaTemas { get; set; }

        /*PARA ACTUALIZAR UNIDADES*/
        //public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdUsuarioCreador { get; set; }
        /*-----------------------*/
        public List<ParticipanteNormal> listaParticipantesNormales { get; set; }
        public List<ParticipanteExterno> listaParticipantesExternos { get; set; }
        public string lstExternos { get; set; }
        public List<Empresa> Empresas { get; set; }

        public void Fill(AbetEntities ctx, HttpSessionStateBase Session, Int32? _IdReunion)
        {
            var idEscuela = Session.GetEscuelaId();
            IdReunion = _IdReunion;
            lstSede = ctx.Sede.ToList();
            //----------------------------------
            Empresas = ctx.Empresa.ToList();
            
            listaParticipantesNormales = new List<ParticipanteNormal>();
            listaParticipantesExternos = new List<ParticipanteExterno>();

            if (IdReunion.HasValue)
            {
                var reunion = ctx.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion);
                //var participantesDisponibles = ctx.uspGetDocentesCargoNivel("EXT", reunion.IdPeriodoAcademico, 1, 0, 0).ToList();
                var participantesDisponibles = ctx.uspGetDocentesCargoNivel("EXT", reunion.IdSubModalidadPeriodoAcademico, 1, 0, 0).ToList();

                IdNivel = reunion.Nivel.Value;
                //listaNiveles = GedServices.GetNivelesUsuarioServices(ctx, reunion.IdDocente.Value, reunion.IdPeriodoAcademico.Value, escuelaId);
                listaNiveles = GedServices.GetNivelesUsuarioServices(ctx, reunion.IdDocente.Value, reunion.IdSubModalidadPeriodoAcademico.Value, idEscuela);
                IdUnidadAcademica = reunion.IdUnidadAcademica.Value;
                //listaUnidadesAcademicas = GedServices.GetUnidadAcademicaNivelServices(ctx, reunion.IdDocente.Value, reunion.Nivel.Value, reunion.IdPeriodoAcademico.Value, escuelaId);
                listaUnidadesAcademicas = GedServices.GetUnidadAcademicaNivelServices(ctx, reunion.IdDocente.Value, reunion.Nivel.Value, reunion.IdSubModalidadPeriodoAcademico.Value, idEscuela);

                Motivo = reunion.Motivo;
                IdSede = reunion.IdSede.Value;
                Fecha = reunion.Fecha;
                Lugar = reunion.Lugar;
                HoraInicio = reunion.HoraInicio;
                HoraFin = reunion.HoraFin;
                listaTemas = ctx.TemaReunion.Where(x => x.IdReunion == IdReunion).Select(c => c.DescripcionTemaReunion).ToList();
                //IdPeriodoAcademico = reunion.IdPeriodoAcademico.Value;
                IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico.Value;
                IdUsuarioCreador = reunion.IdDocente.Value;

                for (int i = 0; i < listaTemas.Count; i++)
                {
                    if(i != listaTemas.Count - 1)
                        lstTemas += listaTemas[i] + "*";
                    else
                        lstTemas += listaTemas[i];
                }

                List<ParticipanteReunion> participantesInscritos;
                List<ParticipanteReunion> participantesInscritosExternos;

                participantesInscritos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && !x.EsExterno).ToList();
                participantesInscritosExternos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && x.EsExterno).ToList();

                foreach (var itemDisponible in participantesDisponibles)
                {
                    if (participantesInscritos != null)
                    {
                        ParticipanteNormal objParticipante = new ParticipanteNormal();
                        objParticipante.IdParticipante = itemDisponible.IdDocente;
                        objParticipante.Nivel = itemDisponible.Nivel.ToString();
                        objParticipante.Nombre = itemDisponible.NombreDocente;
                        objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                        objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                        objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;
                        if (participantesInscritos.Exists(x => x.IdDocente == itemDisponible.IdDocente && x.Cargo == itemDisponible.CargoEspanol))
                        {
                            objParticipante.IsSelected = true;
                        }
                        else
                        {
                            objParticipante.IsSelected = false;
                        }

                        listaParticipantesNormales.Add(objParticipante);
                    }
                }

                for(int i = 0; i < participantesInscritosExternos.Count; i++)
                {
                    ParticipanteExterno objParticipante = new ParticipanteExterno();
                    objParticipante.IdParticipante = participantesInscritosExternos[i].IdParticipanteReunion;
                    objParticipante.Nombre = participantesInscritosExternos[i].NombreCompleto;
                    objParticipante.Cargo = participantesInscritosExternos[i].Cargo;
                    objParticipante.Correo = participantesInscritosExternos[i].Correo;
                    objParticipante.IdEmpresa = participantesInscritosExternos[i].Empresa.IdEmpresa;
                    objParticipante.NombreEmpresa = participantesInscritosExternos[i].Empresa.RazonSocial;
                    listaParticipantesExternos.Add(objParticipante);

                    string externo = string.Format("{0}|{1}|{2}|{3}", objParticipante.Nombre, objParticipante.Cargo, objParticipante.Correo, objParticipante.IdEmpresa);
                    if (i != participantesInscritosExternos.Count - 1)
                        lstExternos += externo + "*";
                    else
                        lstExternos += externo;
                }
            }
            else
            {
                //IdPeriodoAcademico = Session.GetPeriodoAcademicoId().Value;
                IdSubModalidadPeriodoAcademico = Session.GetPeriodoAcademicoId().Value;
                var docenteid = Session.GetDocenteId();
                //listaNiveles = GedServices.GetNivelesUsuarioServices(ctx, Session.GetDocenteId().Value, IdPeriodoAcademico, escuelaId);
                listaNiveles = GedServices.GetNivelesUsuarioServices(ctx, Session.GetDocenteId().Value, IdSubModalidadPeriodoAcademico, idEscuela);
                IdNivel = ConvertHelpers.ToInteger(listaNiveles.First());
                listaUnidadesAcademicas = GedServices.GetUnidadAcademicaNivelServices(ctx, Session.GetDocenteId().Value, IdNivel, Session.GetPeriodoAcademicoId().Value, idEscuela);
                IdUnidadAcademica = listaUnidadesAcademicas.First().IdUnidadAcademica;

                //var participantesDisponibles = ctx.uspGetDocentesCargoNivel("EXT", IdPeriodoAcademico,1, 0, 0).ToList();
                var participantesDisponibles = ctx.uspGetDocentesCargoNivel("EXT", IdSubModalidadPeriodoAcademico, 1, 0, 0).ToList();

                Fecha = DateTime.Now;
                HoraInicio = DateTime.Now;
                HoraFin = DateTime.Now;
                listaTemas = new List<String>();
                IdUsuarioCreador = Session.GetDocenteId().Value;

                foreach (var itemDisponible in participantesDisponibles)
                {
                    ParticipanteNormal objParticipante = new ParticipanteNormal();
                    objParticipante.IdParticipante = itemDisponible.IdDocente;
                    objParticipante.Nivel = itemDisponible.Nivel.ToString();
                    objParticipante.Nombre = itemDisponible.NombreDocente;
                    objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                    objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                    objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                    if (IdUsuarioCreador == itemDisponible.IdDocente && IdUnidadAcademica == itemDisponible.IdUnidadAcademica)
                        objParticipante.IsSelected = true;
                    else
                        objParticipante.IsSelected = false;

                    listaParticipantesNormales.Add(objParticipante);
                }
            }
        }
        
    }
}