﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class MeetingsAssociatedViewModel
    {
        public Reunion Reunion { get; set; }
        public PreAgenda PreAgenda { get; set; }  
        public Int32? IdPreAgenda { get; set; }
        public List<Reunion> ListaReunions { get; set; }
        public void CargarDatos(CargarDatosContext dataContext, Int32? idPreAgenda)
        {
            if (idPreAgenda.HasValue)
            {
                this.IdPreAgenda = idPreAgenda;
                var query = dataContext.context.Reunion.Where(x => x.IdPreAgenda == IdPreAgenda.Value);
                if (query.Any())
                    this.ListaReunions = query.ToList();
                else
                    ListaReunions = new List<Reunion>();
            }            
        }


    }
}