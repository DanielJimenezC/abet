﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ReunionPreScheduleViewModel
    {
        public PreAgenda PreAgenda { get; set; }
        public Reunion Reunion { get; set; }
        public List<TemaPreAgenda> ListaTemasPreAgenda { get; set; }
        public List<ParticipanteViewModel> Participantes { get; set; }

        //PARA LA FRECUENCIA
        public Int32 Frecuencia { get; set; }
        //------------------
        public List<Empresa> Empresas { get; set; }
        public IEnumerable<SelectListItem> Niveles { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public IEnumerable<SelectListItem> UnidadesAcademicas { get; set; }

        public IEnumerable<SelectListItem> AvailableFrecuenciaReunion { get; set; }
        public string TituloUnidadAcademica { get; set; }
        public string TituloReunion { get; set; }

        public string FrecuenciaReunion { get; set; }

        public string estado { get; set; }
    }
}