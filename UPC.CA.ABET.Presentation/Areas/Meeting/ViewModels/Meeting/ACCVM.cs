﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ACCVM
    {
        public int IdACC { get; set; }
        public string Codigo { get; set; }
        public string Fecha { get; set; }
        public string Usuario { get; set; }
        public string Evidencia { get; set; }
        public string Sede { get; set; }
        public string SubModalidadPeriodoAcademico { get; set; }
        public string Descripcion { get; set; }
        public bool IsDeleted { get; set; }
        public IPagedList<ACCVM> LstACC { get; set; }
        public Int32? NumeroPagina { get; set; }
        //public IPagedList<ReunionDocente> LstReunionDoc { get; set; }


        public void fill(CargarDatosContext dataContext, Int32? numeroPagina)
        {
            NumeroPagina = numeroPagina ?? 1;

            var query = (from acc in dataContext.context.ActaComiteConsultivo
                         join us in dataContext.context.Usuario on acc.IdUsuario equals us.IdUsuario
                         join se in dataContext.context.Sede on acc.Sede equals se.IdSede
                         join spa in dataContext.context.SubModalidadPeriodoAcademico on acc.IdSubModalidadPeriodoAcademico equals spa.IdSubModalidadPeriodoAcademico
                         join pa in dataContext.context.PeriodoAcademico on spa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                         where acc.IsDeleted == false
                         orderby acc.Fecha descending
                         select new ACCVM{
                             IdACC = acc.IdActaReunion,
                             Codigo = acc.Codigo,
                             Fecha = acc.Fecha.ToString(),
                             Usuario = us.Nombres + " "+us.Apellidos,
                             Evidencia = acc.Evidencia,
                             Sede = se.Nombre,
                             SubModalidadPeriodoAcademico = pa.CicloAcademico,
                             Descripcion = acc.Descripcion,
                             IsDeleted = acc.IsDeleted
                         }).ToList();
            LstACC = query.ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

            //var quer = dataContext.context.ReunionDocente.ToList();
            //if (query != null)
            //{

            //    LstReunionDoc = quer.AsQueryable().ToPagedList(NumeroPagina ?? 1, 10);
            //}
        }
        
    }

   
}