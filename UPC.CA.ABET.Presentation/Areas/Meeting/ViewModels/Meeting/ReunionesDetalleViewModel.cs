﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ReunionesDetalleViewModel
    {
        public string NombreReunion { get; set; }
        public string Semana { get; set; }
        public string Inicio { get; set; }
        public string Fin { get; set; }
        public string Lugar { get; set; }
        public string Nivel { get; set; }
        public string AreaEncargada { get; set; }
        public Int32? DocenteId { get; set; }
        public List<SelectListItem> lstProfesoresParticipantes { get; set; }

        public ReunionesDetalleViewModel() { }

        public static ReunionesDetalleViewModel ReunionDetalle(CargarDatosContext dataContext, int IdReunionProfesores)
        {
            var _context = dataContext.context;

            var reunionDetalleQuery = _context.Usp_GetReunionProfesor(IdReunionProfesores)
                .Select(c => new ReunionesDetalleViewModel
                {
                    NombreReunion = c.NombreReunion,
                    Semana = c.Semana,
                    Inicio = c.Inicio.Value.ToString("dd/MM/yyyy HH:mm"),
                    Fin = c.Fin.Value.ToString("dd/MM/yyyy HH:mm"),
                    Lugar = c.Lugar,
                    Nivel = c.Nivel.ToString(),
                    AreaEncargada = c.AreaEncargada
                })
                .FirstOrDefault();

            reunionDetalleQuery.lstProfesoresParticipantes = new List<SelectListItem>();
            reunionDetalleQuery.lstProfesoresParticipantes = _context.Usp_GetParticipantesReunionProfesor(IdReunionProfesores)
                .GroupBy(c => c.IdDocente)
                .Select(c => c.First())
                .Select(c => new SelectListItem
                {
                    Value = c.IdDocente.ToString(),
                    Text = c.Apellidos + ", " + c.Nombres,
                    Selected = true
                })
                .ToList();

            return reunionDetalleQuery;
        }
    }
}