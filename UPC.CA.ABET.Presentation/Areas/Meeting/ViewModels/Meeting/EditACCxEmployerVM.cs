﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class EditACCxEmployerVM
    {
        public int IdACC { get; set; }
        public int idEmpleador { get; set; }
        public int idHallazgo { get; set; }
        public string Empleado { get; set; }
        public string Empresa { get; set; }
        public string Descripcion { get; set; }
        public bool IsDeleted{ get; set; }
        public List<EditACCxEmployerVM> LstAccXEmployer { get; set; }



        public void Fill(CargarDatosContext dataContext, Int32? IdACC) {

            var context = dataContext.context;

            var data = (from ace in context.ActaComiteConsultivoXEmpleador
                        join emp in context.Empleador on ace.Id_Empleador equals emp.Id_Empleador
                        join ha in context.Hallazgo on ace.IdHallazgo equals ha.IdHallazgo
                        join acc in context.ActaComiteConsultivo on ace.IdActaReunion equals acc.IdActaReunion
                        where ace.IsDeleted == false && ace.IdActaReunion == IdACC
                        select new EditACCxEmployerVM {
                            IdACC = ace.IdActaReunion,
                            idEmpleador = emp.Id_Empleador,
                            Empleado = emp.Nombre + " "+ emp.Apellido,
                            Empresa = emp.Empresa,
                            idHallazgo = ha.IdHallazgo,
                            Descripcion = ha.DescripcionEspanol
                        }
                        ).ToList();
            LstAccXEmployer = data.ToList();
        }
    }
}