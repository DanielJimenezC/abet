﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.Validation;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class CreateACCVM
    {
        public String Fecha { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public PeriodoAcademico Ciclo { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public int idSede { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public String Comentario { get; set; }
        public int userACC { get; set; }
        public string CicloAcademico { get; set; }
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public string CodigoACC { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? CicloId, Int32? EscuelaId)
        {
            IdCiclo = CicloId.Value;
            Ciclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdCiclo);
            var sede = dataContext.context.Sede.ToList();
            IdEscuela = EscuelaId.Value;

        }
    }
}