﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ParticipanteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nivel { get; set; }
        public bool IsSelected { get; set; }
        public List<UnidadAcademica> UnidadesAcademicas { get; set; }

        public bool EsExterno { get; set; }
        public string Cargo { get; set; }
    }
}