﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class AddEditMeetingPreScheduleViewModel
    {
        //SIGUIENDO UN PATRÓN
        public Int32 IdPreAgenda { get; set; }
        public Int32? IdReunion { get; set; }
        public Boolean Editar { get; set; }

        public Int32 IdUnidadAcademica { get; set; }
        public String Motivo { get; set; }
        public Int32 IdSede { get; set; }

        public List<Sede> lstSede { get; set; }
        public DateTime Fecha { get; set; }
        public String Lugar { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
        public Int32 Frecuencia { get; set; }

        public string lstTemas { get; set; }
        public List<string> listaTemas { get; set; }
        public List<TemaPreAgenda> listaTemasPreAgenda { get; set; }

        public Int32 IdUsuarioCreador { get; set; }
        public List<ParticipanteNormal> listaParticipantesNormales { get; set; }
        public List<ParticipanteExterno> listaParticipantesExternos { get; set; }
        public string lstExternos { get; set; }
        public List<Empresa> Empresas { get; set; }

        public void Fill(AbetEntities ctx, HttpSessionStateBase Session, Int32 _IdPreAgenda, Int32? _IdReunion)
        {
            IdReunion = _IdReunion;
            IdPreAgenda = _IdPreAgenda;
            var preAgenda = ctx.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == IdPreAgenda);
            lstSede = ctx.Sede.ToList();
            Empresas = ctx.Empresa.ToList();
            listaTemasPreAgenda = ctx.TemaPreAgenda.Where(x => x.IdPreAgenda == IdPreAgenda).ToList();
            IdUsuarioCreador = preAgenda.IdUsuarioCreador.Value;
            IdUnidadAcademica = preAgenda.IdUnidaAcademica;

            //var participantesDisponibles = ctx.uspGetDocentesCargoNivel("PRE", preAgenda.IdPeriodoAcademico, preAgenda.IdSede,  preAgenda.Nivel.Value, preAgenda.IdUnidaAcademica).ToList();
            var participantesDisponibles = ctx.uspGetDocentesCargoNivel("PRE", preAgenda.IdSubModalidadPeriodoAcademico, preAgenda.IdSede, preAgenda.Nivel.Value, preAgenda.IdUnidaAcademica).ToList();
            listaParticipantesNormales = new List<ParticipanteNormal>();
            listaParticipantesExternos = new List<ParticipanteExterno>();
            
            if (IdReunion.HasValue)
            {
                var reunion = ctx.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion);
                Editar = true;
                IdUnidadAcademica = reunion.IdUnidadAcademica.Value;

                Motivo = reunion.Motivo;
                IdSede = preAgenda.IdSede ?? 0;
                Fecha = reunion.Fecha;
                Lugar = reunion.Lugar;
                HoraInicio = reunion.HoraInicio;
                HoraFin = reunion.HoraFin;
                Frecuencia = reunion.Frecuencia ?? 0;
                listaTemas = ctx.TemaReunion.Where(x => x.IdReunion == IdReunion).Select(c => c.DescripcionTemaReunion).ToList();

                for (int i = 0; i < listaTemas.Count; i++)
                {
                    if (i != listaTemas.Count - 1)
                        lstTemas += listaTemas[i] + "*";
                    else
                        lstTemas += listaTemas[i];
                }

                List<ParticipanteReunion> participantesInscritos;
                List<ParticipanteReunion> participantesInscritosExternos;

                participantesInscritos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && !x.EsExterno).ToList();
                participantesInscritosExternos = ctx.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && x.EsExterno).ToList();


                foreach (var itemDisponible in participantesDisponibles)
                {
                    if (participantesInscritos != null)
                    {
                        ParticipanteNormal objParticipante = new ParticipanteNormal();
                        objParticipante.IdParticipante = itemDisponible.IdDocente;
                        objParticipante.Nivel = itemDisponible.Nivel.ToString();
                        objParticipante.Nombre = itemDisponible.NombreDocente;
                        objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                        objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                        objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                        if (participantesInscritos.Exists(x => x.IdDocente == itemDisponible.IdDocente && x.Cargo == itemDisponible.CargoEspanol))
                        {
                            objParticipante.IsSelected = true;
                        }
                        else
                        {
                            objParticipante.IsSelected = false;
                        }

                        listaParticipantesNormales.Add(objParticipante);
                    }
                }

                for (int i = 0; i < participantesInscritosExternos.Count; i++)
                {
                    ParticipanteExterno objParticipante = new ParticipanteExterno();
                    objParticipante.IdParticipante = participantesInscritosExternos[i].IdParticipanteReunion;
                    objParticipante.Nombre = participantesInscritosExternos[i].NombreCompleto;
                    objParticipante.Cargo = participantesInscritosExternos[i].Cargo;
                    objParticipante.Correo = participantesInscritosExternos[i].Correo;
                    objParticipante.IdEmpresa = participantesInscritosExternos[i].Empresa.IdEmpresa;
                    objParticipante.NombreEmpresa = participantesInscritosExternos[i].Empresa.RazonSocial;
                    listaParticipantesExternos.Add(objParticipante);

                    string externo = string.Format("{0}|{1}|{2}|{3}", objParticipante.Nombre, objParticipante.Cargo, objParticipante.Correo, objParticipante.IdEmpresa);
                    if (i != participantesInscritosExternos.Count - 1)
                        lstExternos += externo + "*";
                    else
                        lstExternos += externo;
                }
            }
            else
            {
                Motivo = preAgenda.Motivo;
                Fecha = DateTime.Now;
                HoraInicio = DateTime.Now;
                HoraFin = DateTime.Now;
                listaTemas = new List<String>();

                for (int i = 0; i < listaTemasPreAgenda.Count; i++)
                {
                    if (i != listaTemasPreAgenda.Count - 1)
                        lstTemas += listaTemasPreAgenda[i].DescripcionTemaPreAgenda + "*";
                    else
                        lstTemas += listaTemasPreAgenda[i].DescripcionTemaPreAgenda;

                    listaTemas.Add(listaTemasPreAgenda[i].DescripcionTemaPreAgenda);
                }

                foreach (var itemDisponible in participantesDisponibles)
                {
                    ParticipanteNormal objParticipante = new ParticipanteNormal();
                    objParticipante.IdParticipante = itemDisponible.IdDocente;
                    objParticipante.Nivel = itemDisponible.Nivel.ToString();
                    objParticipante.Nombre = itemDisponible.NombreDocente;
                    objParticipante.IdUnidadAcademicaResponsable = itemDisponible.IdUnidadAcademicaResponsable;
                    objParticipante.IdUnidadAcademica = itemDisponible.IdUnidadAcademica;
                    objParticipante.UnidadAcademica = itemDisponible.CargoEspanol;

                    if (IdUsuarioCreador == itemDisponible.IdDocente && IdUnidadAcademica == itemDisponible.IdUnidadAcademica)
                        objParticipante.IsSelected = true;
                    else
                        objParticipante.IsSelected = false;

                    listaParticipantesNormales.Add(objParticipante);
                }
            }
        }
    }

    public class Participante
    {
        public Int32 IdParticipante { get; set; }
        public String Nombre { get; set; }
    }

    public class ParticipanteNormal : Participante
    {
        public String Nivel { get; set; }
        public String UnidadAcademica { get; set; }
        public Int32 IdUnidadAcademica { get; set; }
        public Int32 IdUnidadAcademicaResponsable { get; set; }
        public Boolean IsSelected { get; set; }
    }

    public class ParticipanteExterno : Participante
    {
        public String Cargo { get; set; }
        public String Correo { get; set; }
        public Int32 IdEmpresa { get; set; }
        public String NombreEmpresa { get; set; }
    }
}