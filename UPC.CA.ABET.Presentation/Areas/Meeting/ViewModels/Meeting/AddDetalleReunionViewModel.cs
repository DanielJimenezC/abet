﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class AddDetalleReunionViewModel
    {
        public IEnumerable<SelectListItem> ListTipDetalle { get; set; }
        public Int32? IdTipDetalle { get; set; }
        public int IdReunionProfesores { get; set; }
        public string NombreReunion { get; set; }
        public string AreaEncargada { get; set; }
        public string DocenteEncargado { get; set; }
        public DateTime? Fecha { get; set; }

        public class ReunionProfesores
        {

            public int IdReunionProfesores { get; set; }
            public string NombreReunion { get; set; }
            public string Semana { get; set; }
            public int NumSemana { get; set; }
            public DateTime? Inicio { get; set; }
            public DateTime? Fin { get; set; }
            public string Lugar { get; set; }
            public int Nivel { get; set; }
            public string AreaEncargada { get; set; }
            public int IdUnidadAcademica { get; set; }
            public int IdEscuela { get; set; }
            public int? IdSubModalidadPeriodoAcademicoModulo { get; set; }
            public string NombreEncargado { get; set; }
            public string CodigoEncargado { get; set; }
        }

        public void CargarDatos(AbetEntities context, int IdReunionProfesor)
        {
            this.IdReunionProfesores=IdReunionProfesor;

            var queryResultados = (from a in context.ReunionProfesor
                                   join b in context.ReunionProfesorParticipante on a.IdReunionProfesor equals b.IdReunionProfesor
                                   join c in context.UnidadAcademica on a.IdUnidadAcademica equals c.IdUnidadAcademica
                                   join d in context.SedeUnidadAcademica on c.IdUnidadAcademica equals d.IdUnidadAcademica
                                   join e in context.UnidadAcademicaResponsable on d.IdSedeUnidadAcademica equals e.IdSedeUnidadAcademica
                                   join f in context.Docente on e.IdDocente equals f.IdDocente
                                   where a.IdReunionProfesor == IdReunionProfesores
                                   select new ReunionProfesores
                                   {
                                       IdReunionProfesores = a.IdReunionProfesor,
                                       NombreReunion = a.NombreReunion,
                                       Semana = a.Semana,
                                       NumSemana = a.NumSemana,
                                       Inicio = a.Inicio,
                                       Fin = a.Fin,
                                       Lugar = (a.Lugar == "" || a.Lugar == null) ? LayoutResource.PorDefinir : a.Lugar,
                                       Nivel = a.Nivel,
                                       AreaEncargada = a.AreaEncargada,
                                       IdUnidadAcademica = a.IdUnidadAcademica,
                                       IdEscuela = a.IdEscuela,
                                       IdSubModalidadPeriodoAcademicoModulo = a.IdSubModalidadPeriodoAcademicoModulo,
                                       NombreEncargado = f.Nombres + " " + f.Apellidos,
                                       CodigoEncargado = f.Codigo
                                   }
                                ).Distinct().AsQueryable();

            string Nombre = queryResultados.Select(x => x.NombreEncargado).FirstOrDefault();
            string Codigo = queryResultados.Select(x => x.CodigoEncargado).FirstOrDefault();

            NombreReunion = queryResultados.Select(x => x.NombreReunion).FirstOrDefault();
            AreaEncargada = queryResultados.Select(x => x.AreaEncargada).FirstOrDefault();
            Fecha = queryResultados.Select(x => x.Inicio).FirstOrDefault();
            DocenteEncargado = Codigo + ' ' + Nombre;
        }
    } 
}