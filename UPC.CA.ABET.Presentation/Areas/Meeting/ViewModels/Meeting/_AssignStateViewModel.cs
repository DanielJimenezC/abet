﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class _AssignStateViewModel
    {
        public Int32 IdReunion { get; set; }
        public String FechaProgramacion { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
        public Boolean Extraordinaria { get; set; }

        public void Fill (AbetEntities ctx, Int32 idReunion)
        {
            IdReunion = idReunion;
            var reunion = ctx.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion);
            FechaProgramacion = String.Format("{0}-{1}-{2}", reunion.Fecha.Year, ((reunion.Fecha.Month < 10) ? "0" + reunion.Fecha.Month : reunion.Fecha.Month.ToString()), ((reunion.Fecha.Day < 10) ? "0" + reunion.Fecha.Day : reunion.Fecha.Day.ToString()));
            HoraInicio = reunion.HoraInicio;
            HoraFin = reunion.HoraFin;
            Extraordinaria = reunion.EsExtraordinaria;
        }
    }
}