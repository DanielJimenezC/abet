﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class _DeteleMeetingViewModel
    {
        public Int32 IdReunion { get; set; }
        public String Tipo { get; set; }

        public void Fill(AbetEntities ctx, Int32 idReunion)
        {
            IdReunion = idReunion;
            var reunion = ctx.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion);

            if(reunion.IdPreAgenda.HasValue)
            {
                Tipo = reunion.PreAgenda.TipoPreagenda;
            }
        }
    }
}