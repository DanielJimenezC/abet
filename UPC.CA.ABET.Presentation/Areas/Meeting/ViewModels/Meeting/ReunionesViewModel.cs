﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting
{
    public class ReunionesViewModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }

        public ReunionesViewModel() { }

        public static List<ReunionesViewModel> ConsultarReuniones(CargarDatosContext dataContext, int docenteId, string start, string end, int? idSubmodalidadPeriodoAcademico, int idEscuela)
        {
            DateTime startDt, endDt;

            DateTime.TryParse(start, out startDt);
            DateTime.TryParse(end, out endDt);

            var _context = dataContext.context;

            var reunionesQuery = _context.Usp_GetReunionesProfesorPorProfesor(docenteId).AsQueryable();

            reunionesQuery = reunionesQuery
                .Where(c => c.Inicio.Value.CompareTo(startDt) > 0 && c.Fin.Value.CompareTo(endDt) < 0);

            var reunionesLst = new List<ReunionesViewModel>();

            foreach (var reunion in reunionesQuery)
            {
                var r = new ReunionesViewModel();


                r.id = reunion.IdReunionProfesor.ToString();
                r.title = reunion.NombreReunion;
                r.start = FechaYHora(reunion.Inicio);
                r.end = FechaYHora(reunion.Fin);

                if (reunion.ActaCreada == true)
                {
                    r.color = "green";
                }
                else
                {
                    if (reunion.Notificada == true)
                    {
                        r.color = "red";
                    }
                    else
                    {
                        r.color = "grey";
                    }
                }

                reunionesLst.Add(r);

            }

            var lstCoordinadoresArea = _context.Usp_GetUnidadAcademicaResponsablesPorNivel(idSubmodalidadPeriodoAcademico, idEscuela, 1).AsQueryable();

            bool esCoordinadorDeArea = false;

            foreach (var item in lstCoordinadoresArea)
            {
                if (item.IdDocente == docenteId)
                {
                    esCoordinadorDeArea = true;
                }
            }

            if (esCoordinadorDeArea == true)
            {
                var lstReunionesSubArea = _context.Usp_GetReunionProfesorSubArea(idEscuela, idSubmodalidadPeriodoAcademico, docenteId, 1).AsQueryable();

                foreach (var reunion in lstReunionesSubArea)
                {
                    var r = new ReunionesViewModel();


                    r.id = reunion.IdReunionProfesor.ToString();
                    r.title = reunion.NombreReunion;
                    r.start = FechaYHora(reunion.Inicio);
                    r.end = FechaYHora(reunion.Fin);

                    if (reunion.ActaCreada == true)
                    {
                        r.color = "skyblue";
                    }
                    else
                    {
                        if (reunion.Notificada == true)
                        {
                            r.color = "gold";
                        }
                        else
                        {
                            r.color = "black";
                        }
                    }

                    reunionesLst.Add(r);
                }

            }

            return reunionesLst;
        }

        private static string FechaYHora(DateTime? fecha)
        {
            //string dateTime ISO8601
            var fechaStr = fecha.Value.ToString("yyyy-MM-dd");
            var horaStr = fecha.Value.ToString("HH:mm");

            return fechaStr + "T" + horaStr;
        }
    }
}