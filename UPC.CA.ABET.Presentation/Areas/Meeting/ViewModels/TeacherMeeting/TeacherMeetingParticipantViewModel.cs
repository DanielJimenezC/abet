﻿using System;
using System.Collections.Generic;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingParticipantViewModel
    {
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public TeacherMeetingParticipantViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }
        public int IdReunionProfesor { get; set; }
        public int IdReunionProfesorParticipante { get; set; }
        public ReunionProfesor ReunionProfesor { get; set; }
        public Docente Profesor { get; set; }
        public bool Obligatorio { get; set; }
        public bool Asistio { get; set; }
        public IEnumerable<ReunionProfesorParticipante> ReunionProfesorParticipantes { get; set; }
        public List<ReunionProfesorParticipanteInvitado> ProfesorParticipanteInvitados { get; set; }
        ///public List<ReunionProfesorParticipanteInvitado> AssitantVisitantProffesor { get; set; }
        public string ProfesorInvitadoNombre { get; set; }
        public string ProfesorInvitadoCodigo { get; set; }

        public bool ProfesorinvitadoAsistio { get; set; }

        public void LoadAttendance(int idReunionProfesor)
        {           
            ReunionProfesorParticipantes = TeacherMeetingLogic.GetAllTeacherMeetingParticipants(idReunionProfesor);
            ProfesorParticipanteInvitados = TeacherMeetingLogic.GetAllGuestTeacherMeetingParticipants(idReunionProfesor);           
            IdReunionProfesor = idReunionProfesor;
        }     
    }
}