﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class ObservacionesActaViewModel
    {
        public ObservacionesActa observacionesActa { get; set; }
        public string Numero { get; set; }        
        public string NomParticipante { get; set; }
        public string NomUnidadAcademica { get; set; }
        public IEnumerable<SelectListItem> AvailableEstadoObservacion { get; set; }
        public String Estado { get; set; }

        public ObservacionesActaViewModel()
        {
            this.observacionesActa = new ObservacionesActa();
        }

        public void CargarDatos(CargarDatosContext cargarDatosContext)
        {
            var culture = cargarDatosContext.currentCulture;
            List<Tuple<String, String>> estadosDisponibles = new List<Tuple<String, String>>();
            if(culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.PENDIENTE.CODIGO, ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.PENDIENTE.TEXTO));
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RECHAZADO.CODIGO, ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RECHAZADO.TEXTO));
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RESUELTO.CODIGO, ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RESUELTO.TEXTO));                
            }
            else
            {
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.PENDIENTE.CODIGO, ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.PENDIENTE.TEXTOINGLES));
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RECHAZADO.CODIGO, ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RECHAZADO.TEXTOINGLES));
                estadosDisponibles.Add(new Tuple<String, String>(ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RESUELTO.CODIGO,   ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.RESUELTO.TEXTOINGLES));                
            }

            AvailableEstadoObservacion = estadosDisponibles.Select(o=>new SelectListItem{ Value = o.Item1, Text = o.Item2});
            Estado = observacionesActa.Estado ?? "PEN";
        }
        
    }
}