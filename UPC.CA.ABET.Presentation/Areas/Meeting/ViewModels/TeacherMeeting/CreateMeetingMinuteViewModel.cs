﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class CreateMeetingMinuteViewModel
    {
        public Boolean EsEditar { get; set; }
        public IEnumerable<SelectListItem> AvailableTemaPreAgendaEstados { get; set; }
        public IEnumerable<SelectListItem> AvailableTareaEstados { get; set; }
        public IEnumerable<SelectListItem> AvailableParticipantes { get; set; }
        public Reunion Reunion { get; set; }
        public List<Tuple<string, string>> EstadosAgenda { get; set; }
        public List<Tuple<string, string>> EstadosTarea { get; set; }
        public List<TemaReunionCreateViewModel> TemasReunion { get; set; }
        public List<ParticipanteReunion> Participantes { get; set; }
        public List<DetalleReunionCreateViewModel> DetalleTemaReunion { get; set; }
        public List<AcuerdoCreateViewModel> Acuerdos { get; set; }
        public List<TareaCreateViewModel> Tareas { get; set; }
        public List<HttpPostedFileBase> Documentos { get; set; }
        public HttpPostedFileBase Foto { get; set; }
        public HttpPostedFileBase Firma { get; set; }
        public string RutaFirma { get; set; }
        public string Today { get; set; }
        public Usuario Usuario { get; set; }
        public string RutaFoto { get; set; }        
        public List<String> RutasDocumentos { get; set; }
        public List<ObservacionesActaViewModel> Observaciones { get; set; }
        
    }
}