﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Models;
using static UPC.CA.ABET.Helpers.ConstantHelpers;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingFindingViewModel : ReunionProfesorHallazgo
    {
        const int firstLevel = (int)ORGANIZATION_CHART_LEVELS.FIRST_LEVEL;
        const int secondLevel = (int)ORGANIZATION_CHART_LEVELS.SECOND_LEVEL;
        const int thirdLevel = (int)ORGANIZATION_CHART_LEVELS.THIRD_LEVEL;
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public IEnumerable<SelectListItem> AvailableAreas { get; set; }
        public IEnumerable<SelectListItem> AvailableSubAreas { get; set; }
        public IEnumerable<SelectListItem> AvailableCursos { get; set; }
        public IEnumerable<SelectListItem> AvailableSecciones { get; set; }
        public IEnumerable<SelectListItem> AvailableSedes { get; set; }
        public IEnumerable<ReunionProfesorHallazgo> ReunionProfesorHallazgos { get; set; }
        public ReunionProfesorHallazgo ReunionProfesorHallazgo { get; set; }

        public TeacherMeetingFindingViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }

        public void CargarData(AbetEntities context, int idReunionProfesor, int modalityId, int? idSubmodalidadPeriodoAcademico, int EscuelaId, string CurrentCulture)
        {
            ReunionProfesor = context.ReunionProfesor.Where(x => x.IdReunionProfesor == idReunionProfesor).FirstOrDefault();

            // Areas

            List<UnidadAcademica> areas = new List<UnidadAcademica>();

            if (ReunionProfesor.Nivel == firstLevel)
            {
                areas = (from ua in context.UnidadAcademica
                         join smpa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                         join smpam in context.SubModalidadPeriodoAcademicoModulo on smpa.IdSubModalidadPeriodoAcademico equals smpam.IdSubModalidadPeriodoAcademico
                         join m in context.Modulo on smpam.IdModulo equals m.IdModulo
                         where ua.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                         select ua
                           ).ToList();
                AvailableAreas = areas.Select(o => new SelectListItem { Value = o.IdUnidadAcademica.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? o.NombreIngles : o.NombreEspanol) });

            }

            if (ReunionProfesor.Nivel == secondLevel)
            {
                areas = (from ua in context.UnidadAcademica
                         join ua2 in context.UnidadAcademica on ua.IdUnidadAcademica equals ua2.IdUnidadAcademicaPadre
                         join smpa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                         join smpam in context.SubModalidadPeriodoAcademicoModulo on smpa.IdSubModalidadPeriodoAcademico equals smpam.IdSubModalidadPeriodoAcademico
                         join m in context.Modulo on smpam.IdModulo equals m.IdModulo
                         where ua2.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                         select ua
                           ).ToList();
                AvailableAreas = areas.Select(o => new SelectListItem { Value = o.IdUnidadAcademica.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? o.NombreIngles : o.NombreEspanol) });
            }

            this.IdAreaUnidadAcademica = areas.FirstOrDefault().IdUnidadAcademica;

            List<UnidadAcademica> subareas = new List<UnidadAcademica>();

            if (ReunionProfesor.Nivel == firstLevel)
            {
                subareas = (from ua in context.UnidadAcademica
                            join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                            where ua.Nivel == secondLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId && ua.IdUnidadAcademicaPadre == ReunionProfesor.IdUnidadAcademica
                            select ua).ToList();

                AvailableSubAreas = subareas.Select(x => new SelectListItem { Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol), Value = x.IdUnidadAcademica.ToString() });
            }

            if (ReunionProfesor.Nivel == secondLevel)
            {
                subareas = (from ua in context.UnidadAcademica
                            join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                            where ua.Nivel == secondLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId && ua.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                            select ua).ToList();

                AvailableSubAreas = subareas.Select(x => new SelectListItem { Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol), Value = x.IdUnidadAcademica.ToString() });
                this.IdSubAreaUnidadAcademica = subareas.FirstOrDefault().IdUnidadAcademica;
            }



            // Cursos 
            var cursosAux = (from ua in context.UnidadAcademica
                             join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                             join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                             where ua.Nivel == thirdLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId
                             select ua).Distinct().ToList();

            var cursos = (from ua in cursosAux
                          join ua2 in subareas on ua.IdUnidadAcademicaPadre equals ua2.IdUnidadAcademica
                          select ua).ToList();

            AvailableCursos = cursos.Select(x => new SelectListItem { Value = x.IdCursoPeriodoAcademico.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol) });


            // Secciones 

            var cursoSeccionAux = (from cpa in context.CursoPeriodoAcademico
                                   join ua in context.UnidadAcademica on cpa.IdCursoPeriodoAcademico equals ua.IdCursoPeriodoAcademico
                                   join s in context.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                                   where ua.Nivel == thirdLevel && ua.IdEscuela == EscuelaId && ua.IdSubModalidadPeriodoAcademico == idSubmodalidadPeriodoAcademico
                                   select s).ToList();

            var secciones = (from csa in cursoSeccionAux
                             join cu in cursos on csa.IdCursoPeriodoAcademico equals cu.IdCursoPeriodoAcademico
                             select csa).OrderBy(x => x.Sede.IdSede).ToList();
            
            AvailableSecciones = secciones.Select(x => new SelectListItem { Value = x.IdSeccion.ToString(), Text = x.Codigo + " : " + (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.CursoPeriodoAcademico.Curso.NombreIngles : x.CursoPeriodoAcademico.Curso.NombreEspanol) + " : " + x.Sede.Codigo });

            // Sedes

            var sedes = secciones.Select(x => x.Sede).Distinct().ToList();

            AvailableSedes = sedes.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre });

        }

        public void CargarDataForEdit(AbetEntities context, int teacherMeetingFindingId, int modalityId, int? idSubmodalidadPeriodoAcademico, int EscuelaId, string CurrentCulture )
        {
            ReunionProfesorHallazgo hallazgo = context.ReunionProfesorHallazgo.Find(teacherMeetingFindingId);

            IdReunionProfesorHallazgo = hallazgo.IdReunionProfesorHallazgo;
            IdReunionProfesor = hallazgo.IdReunionProfesor;
            IdAreaUnidadAcademica = hallazgo.IdAreaUnidadAcademica;
            IdCursoPeriodoAcademico = hallazgo.IdCursoPeriodoAcademico;
            IdSeccion = hallazgo.IdSeccion;
            IdSubAreaUnidadAcademica = hallazgo.IdSubAreaUnidadAcademica;
            IdSede = hallazgo.IdSede;
            DescripcionEspanol = hallazgo.DescripcionEspanol;
            DescripcionIngles = hallazgo.DescripcionIngles;

            ReunionProfesor = context.ReunionProfesor.Where(x => x.IdReunionProfesor == IdReunionProfesor).FirstOrDefault();

            var idModulo = ReunionProfesor.SubModalidadPeriodoAcademicoModulo.IdModulo;

            List<UnidadAcademica> areas = new List<UnidadAcademica>();

            if (ReunionProfesor.Nivel == firstLevel)
            {
                areas = (from ua in context.UnidadAcademica
                         join smpa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                         join smpam in context.SubModalidadPeriodoAcademicoModulo on smpa.IdSubModalidadPeriodoAcademico equals smpam.IdSubModalidadPeriodoAcademico
                         join m in context.Modulo on smpam.IdModulo equals m.IdModulo
                         where ua.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                         select ua
                           ).ToList();
            }

            if (ReunionProfesor.Nivel == secondLevel)
            {
                areas = (from ua in context.UnidadAcademica
                         join ua2 in context.UnidadAcademica on ua.IdUnidadAcademica equals ua2.IdUnidadAcademicaPadre
                         join smpa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                         join smpam in context.SubModalidadPeriodoAcademicoModulo on smpa.IdSubModalidadPeriodoAcademico equals smpam.IdSubModalidadPeriodoAcademico
                         join m in context.Modulo on smpam.IdModulo equals m.IdModulo
                         where ua2.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                         select ua
                           ).ToList();
            }


            List<UnidadAcademica> subareas = new List<UnidadAcademica>();

            if (ReunionProfesor.Nivel == firstLevel)
            {
                subareas = (from ua in context.UnidadAcademica
                            join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                            where ua.Nivel == secondLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId && ua.IdUnidadAcademicaPadre == ReunionProfesor.IdUnidadAcademica
                            select ua).ToList();
            }

            if (ReunionProfesor.Nivel == secondLevel)
            {
                subareas = (from ua in context.UnidadAcademica
                            join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                            where ua.Nivel == secondLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId && ua.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                            select ua).ToList();
            }

            // Cursos 
            var cursosAux = (from ua in context.UnidadAcademica
                             join submodapa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                             join submoda in context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                             where ua.Nivel == thirdLevel && ua.IdEscuela == EscuelaId && submoda.IdModalidad == modalityId
                             select ua).Distinct().ToList();

            var cursos = (from ua in cursosAux
                          join ua2 in subareas on ua.IdUnidadAcademicaPadre equals ua2.IdUnidadAcademica
                          select ua).ToList();


            // Secciones 

            var cursoSeccionAux = (from cpa in context.CursoPeriodoAcademico
                                   join ua in context.UnidadAcademica on cpa.IdCursoPeriodoAcademico equals ua.IdCursoPeriodoAcademico
                                   join s in context.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                                   where ua.Nivel == thirdLevel && ua.IdEscuela == EscuelaId && ua.IdSubModalidadPeriodoAcademico == idSubmodalidadPeriodoAcademico
                                   && s.IdModulo == idModulo
                                   select s).ToList();

            var secciones = (from csa in cursoSeccionAux
                             join cu in cursos on csa.IdCursoPeriodoAcademico equals cu.IdCursoPeriodoAcademico
                             select csa).OrderBy(x => x.Sede.IdSede).ToList();

            // Sedes

            var sedes = secciones.Select(x => x.Sede).Distinct().ToList();

            AvailableSedes = sedes.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre });

            //  Enlistar los seleccionados
            AvailableAreas = areas.Select(o => new SelectListItem { Value = o.IdUnidadAcademica.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? o.NombreIngles : o.NombreEspanol) });

            if (IdAreaUnidadAcademica != null)
            {
                AvailableAreas.Where(x => x.Value.Equals(IdAreaUnidadAcademica.ToString())).FirstOrDefault().Selected = true;
                //  Cargar los subareas del area
                var subAreasSelect = subareas.Where(x => x.UnidadAcademica2.IdUnidadAcademica == IdAreaUnidadAcademica).ToList();
                AvailableSubAreas = subAreasSelect.Select(x => new SelectListItem { Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol), Value = x.IdUnidadAcademica.ToString() });
            }
            else
            {
                AvailableSubAreas = subareas.Select(x => new SelectListItem { Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol), Value = x.IdUnidadAcademica.ToString() });
            }

            if (IdSubAreaUnidadAcademica != null)
            {
                AvailableSubAreas.Where(x => x.Value.Equals(IdSubAreaUnidadAcademica.ToString())).FirstOrDefault().Selected = true;
                //  Cargar los cursos del subarea
                var cursosSelect = cursos.Where(x => x.UnidadAcademica2.IdUnidadAcademica == IdSubAreaUnidadAcademica).ToList();
                AvailableCursos = cursosSelect.Select(x => new SelectListItem { Value = x.IdCursoPeriodoAcademico.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol) });
            }
            else
            {
                AvailableCursos = cursos.Select(x => new SelectListItem { Value = x.IdCursoPeriodoAcademico.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles : x.NombreEspanol) });
            }

            if (IdCursoPeriodoAcademico != null)
            {              
                AvailableCursos.Where(x => x.Value.Equals(IdCursoPeriodoAcademico.ToString())).FirstOrDefault().Selected = true;
                var sedesAux = secciones.Where(x => x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico).Select(y => y.Sede).Distinct().ToList();

                AvailableSedes = sedesAux.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre });
            }
            if(IdCursoPeriodoAcademico == null)
            {
                AvailableSedes = sedes.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre });
            }

            if (IdSede != null && IdCursoPeriodoAcademico != null)
            {
                AvailableSedes.Where(x => x.Value.Equals(IdSede.ToString())).FirstOrDefault().Selected = true;
                // cargar las secciones de la sede y curso
                var seccionesSelect = secciones.Where(x => x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico && x.IdSede == IdSede).ToList();
                AvailableSecciones = seccionesSelect.Select(x => new SelectListItem
                {
                    Value = x.IdSeccion.ToString(),
                    Text = x.Codigo
                + " : " +
                (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.CursoPeriodoAcademico.Curso.NombreIngles : x.CursoPeriodoAcademico.Curso.NombreEspanol)          
                });
            }
            else
            {
                AvailableSecciones = secciones.Select(x => new SelectListItem { Value = x.IdSeccion.ToString(), Text = x.Codigo });
            }

            if (IdSeccion != null)
            {
                AvailableSecciones.Where(x => x.Value.Equals(IdSeccion.ToString())).FirstOrDefault().Selected = true;
            }
        }


        public void LoadTeacherMeetingFindings(int teacherMeetingId)
        {
            ReunionProfesorHallazgos = TeacherMeetingLogic.GetTeacherMeetingFindingsByTeacherMeetingId(teacherMeetingId);
        }

        public ReunionProfesorHallazgo AddTeacherMeetingFinding(TeacherMeetingFindingViewModel viewModel)
        {
            return TeacherMeetingLogic.AddTeacherMeetingFinding(viewModel);
        }

        public bool UpdateTeacherMeetingFinding(TeacherMeetingFindingViewModel viewModel)
        {
            ReunionProfesorHallazgo = TeacherMeetingLogic.UpdateTeacherMeetingFinding(viewModel);
            if (ReunionProfesorHallazgo != null)
                return true;
            return false;
        }
    }
}
