﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class AccionMejoraCreateViewModel
    {
        public int IdAccionMejora { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string CodigoHallazgo { get; set; }
        public string Estado { get; set; }
    }
}