﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingAttendanceViewModel
    {
        public Int32 IdEscuela { get; set; }
        public Int32 IdSubmodalidadPeriodoAcademicoModulo { get; set; }
        public Int32 NumSemanaDesde { get; set; }
        public Int32 NumSemanaHasta { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy h:ss}")]
        public DateTime FechaDesde { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy h:ss}")]
        public DateTime FechaHasta { get; set; }

        public bool FilterWeeks { get; set; }
        public int GetDetalle { get; set; }

        public string IdIdioma { get; set; }
        public bool HasValue { get; set; }

        public List<SelectListItem> LstSubmodalidadPeriodoAcademicoModulo { get; set; }
        public List<SelectListItem> LstSemanasDesde { get; set; }
        public List<SelectListItem> LstSemanasHasta { get; set; }
        public List<SelectListItem> LstDetalle { get; set; }

        public bool ExistingData { get; set; }

        public TeacherMeetingAttendanceViewModel()
        {
            LstSubmodalidadPeriodoAcademicoModulo = new List<SelectListItem>();
            LstSemanasDesde = new List<SelectListItem>();
            LstSemanasHasta = new List<SelectListItem>();
            LstDetalle = new List<SelectListItem>();
        }

        public void ValidateIfDataExist(CargarDatosContext dataContext, TeacherMeetingAttendanceViewModel attendanceViewModel)
        {
            ExistingData = dataContext.context.Database.SqlQuery<ProcedureResult>("Usp_RP_ObtenerReunionProfesorAsistencia {0}, {1}, {2}, {3}, {4}, {5}",
                attendanceViewModel.IdEscuela,
                attendanceViewModel.IdSubmodalidadPeriodoAcademicoModulo,
                attendanceViewModel.NumSemanaDesde,
                attendanceViewModel.NumSemanaHasta,
                attendanceViewModel.FechaDesde,
                attendanceViewModel.FechaHasta).Any();
        }

        public void Fill(CargarDatosContext dataContext, int ModalidadId, int EscuelaId, string idioma)
        {
            AbetEntities context = dataContext.context;

            LstSubmodalidadPeriodoAcademicoModulo = (
                from a in context.SubModalidadPeriodoAcademico
                join b in context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
                join c in context.PeriodoAcademico on a.IdPeriodoAcademico equals c.IdPeriodoAcademico
                join d in context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademico equals d.IdSubModalidadPeriodoAcademico
                join e in context.Modulo on d.IdModulo equals e.IdModulo
                join f in context.ReunionProfesor on d.IdSubModalidadPeriodoAcademicoModulo equals f.IdSubModalidadPeriodoAcademicoModulo
                join g in context.Modalidad on b.IdModalidad equals g.IdModalidad
                where b.IdModalidad == ModalidadId
                select new SelectListItem
                {                
                    Value = d.IdSubModalidadPeriodoAcademicoModulo.ToString(),
                    Text = c.CicloAcademico.ToString() + " - " + e.IdentificadorSeccion.ToString()
                }).Distinct().ToList();

            List<int> itemsToDelete = new List<int>();

            switch (ModalidadId)
            {
                case 1:
                    itemsToDelete = (from a in context.SubModalidadPeriodoAcademicoModulo
                                    where a.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad != 1
                                    select a.IdSubModalidadPeriodoAcademicoModulo).ToList();
                    break;
                case 2:
                    itemsToDelete = (from a in context.SubModalidadPeriodoAcademicoModulo
                                     where a.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad != 2
                                     select a.IdSubModalidadPeriodoAcademicoModulo).ToList();
                    break;
            }

            foreach(var item in LstSubmodalidadPeriodoAcademicoModulo)
            {
                foreach(var deletes in itemsToDelete)
                {
                    if(item.Value == deletes.ToString())
                    {
                        LstSubmodalidadPeriodoAcademicoModulo.Remove(item);
                    }
                }
            }

            LstDetalle.Insert(0, new SelectListItem { Value = "0", Text = LayoutResource.No });
            LstDetalle.Insert(1, new SelectListItem { Value = "1", Text = LayoutResource.Si });

            IdEscuela = EscuelaId;
            IdIdioma = idioma;

            FechaDesde = DateTime.UtcNow;
            FechaHasta = DateTime.UtcNow.AddDays(1);
            FilterWeeks = true;
        }

        private class ProcedureResult
        {
            public string Nombres { get; set; }
            public int Asistencias { get; set; }
            public int Inasistencias { get; set; }
            public int Total { get; set; }
            public string Porcentaje { get; set; }
        }
    }
}