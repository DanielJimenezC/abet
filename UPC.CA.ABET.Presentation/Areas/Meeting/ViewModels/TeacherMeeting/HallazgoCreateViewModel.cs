﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class HallazgoCreateViewModel
    {
        public int IdHallazgo { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int IdSede { get; set; }
        public int IdConstituyente { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdActa { get; set; }
        public string NivelAceptacion { get; set; }
        public string PeriodoAcademico { get; set; }
        public string Sede { get; set; }
    }
}