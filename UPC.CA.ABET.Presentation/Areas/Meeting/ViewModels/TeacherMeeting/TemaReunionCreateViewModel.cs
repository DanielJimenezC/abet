﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TemaReunionCreateViewModel
    {
        public TemaReunion TemaReunion { get; set; }
        public string Numero { get; set; }
        public string Estado { get; set; }
    }
}