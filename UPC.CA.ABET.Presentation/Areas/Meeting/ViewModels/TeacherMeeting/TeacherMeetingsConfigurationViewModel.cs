﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingsConfigurationViewModel
    {
        public TeacherMeetingLogic TeacherMeetingLogic { get; set; }
        public SubModalityAcademicPeriodModuleLogic SubModalityAcademicPeriodModuleLogic { get; set; }
        public SubModalityAcademicPeriodLogic SubModalityAcademicPeriodLogic { get; set; }
        public TeacherMeetingsConfigurationViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
            SubModalityAcademicPeriodModuleLogic = new SubModalityAcademicPeriodModuleLogic();
            SubModalityAcademicPeriodLogic = new SubModalityAcademicPeriodLogic();
        }

        public ReunionProfesorConfiguracion ReunionProfesorConfiguracion { get; set; }
        public IEnumerable<ReunionProfesorConfiguracion> ReunionProfesorConfiguracionLista { get; set; }
        public int ModuleWeeks { get; set; }
        public int SubModalityAcademicPeriodId { get; set; }
        public SubModalidadPeriodoAcademico SubModalityAcademicPeriod { get; set; }

        #region FormValues
        public IEnumerable<SelectListItem> SubModalityAcademicPeriodModulesNotConfigured { get; set; }
        public int SubModalityAcademicPeriodModuleId { get; set; }
        public string FirstLevelWeeks { get; set; }
        public string SecondLevelWeeks { get; set; }
        public int FirstLevelDueDate { get; set; }
        public int SecondLevelDueDate { get; set; }
        #endregion

        public void LoadAllData(int subModalityAcademicPeriodId)
        {
            SubModalityAcademicPeriodId = subModalityAcademicPeriodId;
            SubModalityAcademicPeriod = SubModalityAcademicPeriodLogic.GetSubModalityAcademicPeriod(subModalityAcademicPeriodId);
            ReunionProfesorConfiguracionLista = GetSubModalityAcademicPeriodConfigurations(SubModalityAcademicPeriod.SubModalidad.IdModalidad);
            var notConfigured = TeacherMeetingLogic.GetSubModalityAcademicPeriodModulesNotConfiguredForTeacherMeetings(SubModalityAcademicPeriod.SubModalidad.IdModalidad);
            SubModalityAcademicPeriodModulesNotConfigured = notConfigured
            .Select(x => new SelectListItem
            {
                Text = x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreEspanol + " - " + x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico + " - " + x.Modulo.NombreEspanol,
                Value = x.IdSubModalidadPeriodoAcademicoModulo.ToString(),
            });
            
        }

        public void LoadData(int? subModalityAcademicPeriodModuleId)
        {
            ReunionProfesorConfiguracion = GetSubModalityAcademicPeriodConfiguration(subModalityAcademicPeriodModuleId);
            ModuleWeeks = GetModuleWeeks(subModalityAcademicPeriodModuleId);
        }

        private IEnumerable<ReunionProfesorConfiguracion> GetSubModalityAcademicPeriodConfigurations(int modalityId)
        {
            return TeacherMeetingLogic.GetTeacherMeetingConfigurationsByModalitydId(modalityId);
        }

        public async Task<bool> CreateTeacherMeetingConfiguration(int subModalityAcademicPeriodModuleId, string FirstLevelWeeks, string SecondLevelWeeks, int FirstLevelDueDate, int SecondLevelDueDate)
        {
            return await TeacherMeetingLogic.CreateTeacherMeetingConfigurationForAreaAndSubArea(subModalityAcademicPeriodModuleId, FirstLevelWeeks, SecondLevelWeeks, FirstLevelDueDate, SecondLevelDueDate);
        }

        private ReunionProfesorConfiguracion GetSubModalityAcademicPeriodConfiguration(int? subModalityAcademicPeriodModuleId)
        {
            if (!subModalityAcademicPeriodModuleId.HasValue) return null;
            return TeacherMeetingLogic.GetConfigurationForSubModalityAcademicPeriodModule(subModalityAcademicPeriodModuleId.Value);
        }

        private int GetModuleWeeks(int? subModalityAcademicPeriodModuleId)
        {
            if (!subModalityAcademicPeriodModuleId.HasValue) return 0;
            var subModalityAcademicPeriodModule = SubModalityAcademicPeriodModuleLogic.GetSubModalityAcademicPeriodModule(subModalityAcademicPeriodModuleId.Value);
            if (!subModalityAcademicPeriodModule.Modulo.Semanas.HasValue) return 0;
            return subModalityAcademicPeriodModule.Modulo.Semanas.Value;
        }
    }
}