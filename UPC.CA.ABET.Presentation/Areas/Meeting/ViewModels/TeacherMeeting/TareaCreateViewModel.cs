﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TareaCreateViewModel
    {
        public string Numero { get; set; }
        public Tarea Tarea { get; set; }
        public List<int> Responsables { get; set; }
        public string FechaPropuesta { get; set; }
        public string Estado { get; set; }
        public List<string> ListaResponsables { get; set; }
    }
}