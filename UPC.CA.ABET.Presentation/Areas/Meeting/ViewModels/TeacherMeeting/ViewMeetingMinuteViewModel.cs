﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class ViewMeetingMinuteViewModel
    {
        public int IdActa { get; set; }
        public Reunion Reunion { get; set; }
        public List<ParticipanteReunion> Participantes { get; set; }
        public List<TemaReunionCreateViewModel> TemasReunion { get; set; }
        public List<DetalleReunionCreateViewModel> DetallesReunion { get; set; }
        public List<AcuerdoCreateViewModel> Acuerdos { get; set; }
        public List<TareaCreateViewModel> Tareas { get; set; }
        public List<string> RutasDocumentos { get; set; }
        public string RutaFoto { get; set; }
        public string RutaFirma { get; set; }
        public Usuario Usuario { get; set; }                
        public List<ObservacionesActaViewModel> Observaciones { get; set; }
        public string Estado { get; set; }
        public bool PuedeObservar { get; set; }
    }
}