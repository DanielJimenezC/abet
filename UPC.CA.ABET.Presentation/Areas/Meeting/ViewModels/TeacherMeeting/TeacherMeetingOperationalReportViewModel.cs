﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Meeting;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingOperationalReportViewModel
    {
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public TeacherMeetingOperationalReportViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }

        public IEnumerable<Curso> CoursesToCreateOperationalReports { get; set; }

        public void LoadOperationalReports(int idReunionProfesor)
        {
            CoursesToCreateOperationalReports = TeacherMeetingLogic.GetAllCoursesToCreateOperationalReports(idReunionProfesor);
        }
    }
}