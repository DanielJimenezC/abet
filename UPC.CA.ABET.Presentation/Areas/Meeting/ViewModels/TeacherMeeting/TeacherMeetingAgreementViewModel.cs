﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.TeacherMeeting;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingAgreementViewModel : ReunionProfesorAcuerdo
    {
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public TeacherMeetingAgreementViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }

        [Display(Name = "FechaDeadline", ResourceType = typeof(TeacherMeetingResource))]
        public new DateTime? FechaDeadline { get; set; }
        [Display(Name = "SemanaDeadline", ResourceType = typeof(TeacherMeetingResource))]
        public new string SemanaDeadline { get; set; }
        [Display(Name = "FechaDeadline", ResourceType = typeof(TeacherMeetingResource))]
        public DateTime FechaDeadlineEditar { get; set; }
        [Display(Name = "SemanaDeadline", ResourceType = typeof(TeacherMeetingResource))]
        public string SemanaDeadlineEditar { get; set; }

        public string DateDeadLine { get; set; }
        public ReunionProfesorAcuerdo ReunionProfesorAcuerdo { get; set; }
        public IEnumerable<ReunionProfesorAcuerdo> ReunionProfesorAcuerdos { get; set; }
        public IEnumerable<ReunionProfesorParticipante> ReunionProfesorParticipantes { get; set; }
        public List<ReunionProfesorParticipanteInvitado> ReunionProfesorParticipanteInvitados { get; set; }
        public IEnumerable<ReunionProfesorHallazgo> ReunionProfesorHallazgos { get; set; }
        public IEnumerable<Docente> ReunionProfesorAcuerdoResponsables { get; set; }  
        public List<ReunionProfesorHallazgo> HallazgosSeleccionados { get; set; }

        public IEnumerable<ReunionProfesorParticipanteInvitado> ReunionProfesorInvitadoAcuerdoResponsables { get; set; }
        public IEnumerable<SelectListItem> WeeksLeft { get; set; }
        public int SubModalityAcademicPeriodModuleId { get; set; }

        public void FindTeacherMeetingAgreementForMeeting(int teacherMeetingAgreementId, int teacherMeetingId)
        {
            ReunionProfesorAcuerdo = TeacherMeetingLogic.GetTeacherMeetingAgreement(teacherMeetingAgreementId, teacherMeetingId);
        }
        public void LoadTeacherMeetingAgreementsByTeacherMeetingId(int teacherMeetingId)
        {
            ReunionProfesorAcuerdos = TeacherMeetingLogic.GetAllTeacherMeetingAgreements(teacherMeetingId);
        }
        
        public void LoadTeacherMeetingAttendanceAndFindingsByTeacherMeetingId(int teacherMeetingId)
        {
            ReunionProfesorParticipantes = TeacherMeetingLogic.GetAllTeacherMeetingParticipants(teacherMeetingId);
            ReunionProfesorParticipanteInvitados = TeacherMeetingLogic.GetAllGuestTeacherMeetingParticipants(teacherMeetingId);
            ReunionProfesorHallazgos = TeacherMeetingLogic.GetTeacherMeetingFindingsUnassignedByTeacherMeetingId(teacherMeetingId);
            var teacherMeeting = TeacherMeetingLogic.FindTeacherMeetingById(teacherMeetingId);
            var nextWeeksForTeacherMeeting = TeacherMeetingLogic.GetWeeksLeftForTheSameTypeOfTeacherMeeting(teacherMeeting);
            var module = teacherMeeting.SubModalidadPeriodoAcademicoModulo.Modulo;
            WeeksLeft = nextWeeksForTeacherMeeting.Select(x => new SelectListItem { Value = x.ToString(), Text = TeacherMeetingResource.Semana + " " + x.ToString() });
            SubModalityAcademicPeriodModuleId = teacherMeeting.IdSubModalidadPeriodoAcademicoModulo.Value;
        }

        public void LoadTeacherMeetingAgreementByTeacherMeetingAgreementId(int teacherMeetingAgreementId)
        {
            ReunionProfesorAcuerdo = TeacherMeetingLogic.GetTeacherMeetingAgreement(teacherMeetingAgreementId);
            ReunionProfesorParticipantes = TeacherMeetingLogic.GetAllTeacherMeetingParticipants(ReunionProfesorAcuerdo.IdReunionProfesor);
            ReunionProfesorParticipanteInvitados = TeacherMeetingLogic.GetAllGuestTeacherMeetingParticipants(ReunionProfesorAcuerdo.IdReunionProfesor);
            ReunionProfesorAcuerdoResponsables = TeacherMeetingLogic.GetTeacherMeetingAgreementResponsiblesByAgreementId(ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo);
            ReunionProfesorInvitadoAcuerdoResponsables = TeacherMeetingLogic.GetGuestTeacherMeetingAgreementResponsiblesByAgreementId(ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo);

            var hallazgosDelAcuerdo = TeacherMeetingLogic.GetTeacherMeetingFindingsByAgreementId(ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo);
            var hallazgosSinAsignar = TeacherMeetingLogic.GetTeacherMeetingFindingsUnassignedByTeacherMeetingId(ReunionProfesorAcuerdo.IdReunionProfesor);
            var teacherMeeting = TeacherMeetingLogic.FindTeacherMeetingById(ReunionProfesorAcuerdo.IdReunionProfesor);
            var nextWeeksForTeacherMeeting = TeacherMeetingLogic.GetWeeksLeftForTheSameTypeOfTeacherMeeting(teacherMeeting);
            var module = teacherMeeting.SubModalidadPeriodoAcademicoModulo.Modulo;
            WeeksLeft = nextWeeksForTeacherMeeting.Select(x => new SelectListItem { Value = x.ToString(), Text = TeacherMeetingResource.Semana + " " + x.ToString() });
            SubModalityAcademicPeriodModuleId = teacherMeeting.IdSubModalidadPeriodoAcademicoModulo.Value;

            HallazgosSeleccionados = hallazgosDelAcuerdo.ToList();
            ReunionProfesorHallazgos = hallazgosDelAcuerdo.Concat(hallazgosSinAsignar);
            FechaDeadlineEditar = ReunionProfesorAcuerdo.FechaDeadline.Value;
            SemanaDeadlineEditar = ReunionProfesorAcuerdo.SemanaDeadline.Value.ToString();
        }
    }
}