﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.TeacherMeeting;
using static UPC.CA.ABET.Helpers.ConstantHelpers.TEACHER_MEETING.ESTADO;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingTasksViewModel
    {
        public Int32 idEscuela { get; set; }
        public Int32 idSubmodalidadPeriodoAcademicoModulo { get; set; }
        public Int32 NumSemanaDesde { get; set; }
        public Int32 NumSemanaHasta { get; set; }
        public Int32 idAreaUnidadAcademica { get; set; }
        public Int32 idSubareaUnidadAcademica { get; set; }
        public Int32 idCursoPeriodoAcademico { get; set; }
        public Int32 idSede { get; set; }
        public Int32 idSeccion { get; set; }
        public Int32 Estado { get; set; }
        public string IdIdioma { get; set; }

        public bool HasValue { get; set; }

        public List<SelectListItem> LstSubmodalidadPeriodoAcademicoModulo { get; set; }
        public List<SelectListItem> LstSemanasDesde { get; set; }
        public List<SelectListItem> LstSemanasHasta { get; set; }
        public IEnumerable<SelectListItem> LstAreaUnidadAcademica { get; set; }
        public IEnumerable<SelectListItem> LstSubareaUnidadAcademica { get; set; }
        public List<SelectListItem> LstEstado { get; set; }

        public List<SelectListItem> LstCursoPeriodoAcademico { get; set; }
        public List<SelectListItem> LstSedes { get; set; }
        public List<SelectListItem> LstSeccion { get; set; }
        public bool ExistingData { get; set; }

        public TeacherMeetingTasksViewModel()
        {
            LstSubmodalidadPeriodoAcademicoModulo = new List<SelectListItem>();
            LstSemanasDesde = new List<SelectListItem>();
            LstSemanasHasta = new List<SelectListItem>();
            LstAreaUnidadAcademica = new List<SelectListItem>();
            LstSubareaUnidadAcademica = new List<SelectListItem>();
            LstEstado = new List<SelectListItem>();
            LstCursoPeriodoAcademico = new List<SelectListItem>();
            LstSedes = new List<SelectListItem>();
            LstSeccion = new List<SelectListItem>();
        }

        public void ValidateIfDataExist(CargarDatosContext dataContext, TeacherMeetingTasksViewModel attendanceViewModel)
        {
            ExistingData = dataContext.context.Database.SqlQuery<ProcedureResult>("Usp_GetReunionProfesorTareasByHallazgo {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                attendanceViewModel.idSubmodalidadPeriodoAcademicoModulo,
                attendanceViewModel.idEscuela,
                attendanceViewModel.NumSemanaDesde,
                attendanceViewModel.NumSemanaHasta,
                attendanceViewModel.idAreaUnidadAcademica,
                attendanceViewModel.idSubareaUnidadAcademica,
                attendanceViewModel.idCursoPeriodoAcademico,
                attendanceViewModel.idSede,
                attendanceViewModel.idSeccion,
                attendanceViewModel.Estado
                ).Any();
        }

        public void Fill(CargarDatosContext dataContext, int ModalidadId, int EscuelaId, string idioma, int? idsubmodalidadperiodoacademicoid)
        {
            AbetEntities context = dataContext.context;

            LstSubmodalidadPeriodoAcademicoModulo = (
                from a in context.SubModalidadPeriodoAcademico
                join b in context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
                join c in context.PeriodoAcademico on a.IdPeriodoAcademico equals c.IdPeriodoAcademico
                join d in context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademico equals d.IdSubModalidadPeriodoAcademico
                join e in context.Modulo on d.IdModulo equals e.IdModulo
                join f in context.ReunionProfesor on d.IdSubModalidadPeriodoAcademicoModulo equals f.IdSubModalidadPeriodoAcademicoModulo
                join g in context.Modalidad on b.IdModalidad equals g.IdModalidad
                where b.IdModalidad == ModalidadId
                select new SelectListItem
                {
                    Value = d.IdSubModalidadPeriodoAcademicoModulo.ToString(),
                    Text = c.CicloAcademico.ToString() + " - " + e.IdentificadorSeccion.ToString()
                }).Distinct().ToList();

            List<int> itemsToDelete = new List<int>();

            var ModalidadPregradoRegular = Int32.Parse(TeacherMeetingResource.IdPregradoRegularValue);
            var ModalidadPregradoEPE = Int32.Parse(TeacherMeetingResource.IdPregradoEPEValue);

            switch (ModalidadId)
            {
                case 1:
                    itemsToDelete = (from a in context.SubModalidadPeriodoAcademicoModulo
                                     where a.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad != ModalidadPregradoRegular
                                     select a.IdSubModalidadPeriodoAcademicoModulo).ToList();
                    break;
                case 2:
                    itemsToDelete = (from a in context.SubModalidadPeriodoAcademicoModulo
                                     where a.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad != ModalidadPregradoEPE
                                     select a.IdSubModalidadPeriodoAcademicoModulo).ToList();
                    break;
            }

            foreach (var item in LstSubmodalidadPeriodoAcademicoModulo)
            {
                foreach (var deletes in itemsToDelete)
                {
                    if (item.Value == deletes.ToString())
                    {
                        LstSubmodalidadPeriodoAcademicoModulo.Remove(item);
                    }
                }
            }
        
            List<UnidadAcademica> areas = new List<UnidadAcademica>();

            var NivelArea = Int32.Parse(TeacherMeetingResource.AreaNumberValue);

            areas = (from ua in context.UnidadAcademica
                             join smpa in context.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                             join smpam in context.SubModalidadPeriodoAcademicoModulo on smpa.IdSubModalidadPeriodoAcademico equals smpam.IdSubModalidadPeriodoAcademico
                             join m in context.Modulo on smpam.IdModulo equals m.IdModulo
                             where ua.IdSubModalidadPeriodoAcademico == idsubmodalidadperiodoacademicoid
                                    && ua.Nivel == NivelArea
                     select ua
                           ).Distinct().ToList();

            
                
            LstAreaUnidadAcademica = areas.Select(o => new SelectListItem { Value = o.IdUnidadAcademica.ToString(), Text = (idioma == ConstantHelpers.CULTURE.INGLES ? o.NombreIngles : o.NombreEspanol) });

            LstEstado.Insert(0, new SelectListItem { Value = TeacherMeetingResource.TodosEstado, Text = TeacherMeetingResource.ParaTodos });
            LstEstado.Insert(1, new SelectListItem { Value = ((int)TAREA.PENDIENTE).ToString(),  Text = TeacherMeetingResource.Pendiente });
            LstEstado.Insert(2, new SelectListItem { Value = ((int)TAREA.COMPLETADA).ToString(), Text = TeacherMeetingResource.Completado });
            LstEstado.Insert(3, new SelectListItem { Value = ((int)TAREA.NO_COMPLETADA).ToString(), Text = TeacherMeetingResource.NoCompletado });

            idEscuela = EscuelaId;
            IdIdioma = idioma;         

        }
        private class ProcedureResult
        {
            public int IdReunionProfesorTarea { get; set; }
        }
    }
}