﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Meeting;
using PagedList;
using UPC.CA.ABET.Helpers;
using static UPC.CA.ABET.Helpers.ConstantHelpers;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingManagementViewModel
    {
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public bool MeetingsAlreadyCreated { get; set; }
        public bool OrganizationChartAlreadyUploaded { get; set; }


        public IPagedList<TeacherMeetingViewModel> TeacherMeetingsByResponsible { get; set; }
        public TeacherMeetingViewModel TeacherMeeting { get; set; }

        public TeacherMeetingManagementViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }

        public IEnumerable<SelectListItem> Modulos { get; set; }

        public IEnumerable<Modulo> ModulosList { get; set; }
        public IEnumerable<SelectListItem> AvailableAcademicPeriods { get; set; }
        public IEnumerable<SelectListItem> Levels { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }
        public IEnumerable<SelectListItem> Weeks { get; set; }

        public string Week { get; set; }
        public int Level { get; set; }
        public int Area { get; set; }
        public int AcademicPeriodId { get; set; }

        public int IdModulo { get; set; }
        public int DefaultModuleId { get; set; }

        public bool OrganizationChartUploaded(CargarDatosContext dataContext, int? idSubModalidadPeriodoAcademico)
        {
            return dataContext.context.UnidadAcademica.Any(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico.Value);
        }
        public bool MeetingsCreated(CargarDatosContext datosContext, int? idSubModalidadPeriodoAcademico, int? idModulo)
        {
            var subModalidadPeriodoAcademicoModulo = datosContext.context.SubModalidadPeriodoAcademicoModulo
                .FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico.Value
                && x.IdModulo == idModulo);
            if (subModalidadPeriodoAcademicoModulo == null) return false;
            return datosContext.context.ReunionProfesor.Any(x => x.IdSubModalidadPeriodoAcademicoModulo == subModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademicoModulo);
        }

        public void LoadFilters(int idSubModalidadPeriodoAcademico, IQueryable<TeacherMeetingViewModel> queryResultados)
        {
            var academicPeriods = TeacherMeetingLogic.GetAcademicPeriodsBySubModalityPeriod(idSubModalidadPeriodoAcademico);
            var periods = academicPeriods.Select(x => new SelectListItem { Value = x.IdPeriodoAcademico.ToString(), Text = x.CicloAcademico }).ToList();
            periods.Add(new SelectListItem { Value = "0", Text = Filtros.Todos });
            AvailableAcademicPeriods = periods.OrderBy(x => x.Value).ToList();

            var levels = queryResultados.Select
                (o => new SelectListItem { Value = o.Nivel.ToString(), Text = o.Nivel.ToString() }).Distinct().ToList();
            levels.Add(new SelectListItem { Value = "0", Text = Filtros.Todos });
            Levels = levels.OrderBy(x => x.Value).ToList();

            var areas = queryResultados.Select
                (o => new SelectListItem { Value = o.IdUnidadAcademica.ToString(), Text = o.AreaEncargada.ToString() }).Distinct().ToList();
            areas.Add(new SelectListItem { Value = "0", Text = Filtros.Todas });
            Areas = areas.OrderBy(x => x.Value).ToList();

            var querysemanas = (from a in queryResultados
                                select new
                                {
                                    NumeroSemana = a.NumSemana,
                                    a.Semana
                                }).Distinct().OrderBy(x => x.NumeroSemana);

            var weeks = querysemanas.Select
                (o => new SelectListItem { Value = o.NumeroSemana.ToString(), Text = o.Semana.ToString() }).OrderBy(x => x.Value).ToList();
            var _weeks = new List<SelectListItem> { new SelectListItem{ Value = "All", Text = Filtros.Todas }};
            _weeks.AddRange(weeks);
            Weeks = _weeks.ToList();
        }

        public void LoadModules(AbetEntities abetEntities, int parsedModalityId, int? subModalityAcademicPeriodId, int? subModalityAcademicPeriodIdSession)
        {
            var activeSubModalityAcademicPeriod = abetEntities.SubModalidadPeriodoAcademico
                    .FirstOrDefault(x => x.SubModalidad.Modalidad.IdModalidad == parsedModalityId
                        && x.PeriodoAcademico.Estado == ESTADO.ACTIVO);
            var submodalityAcademicPeriodIdDefault = subModalityAcademicPeriodId ?? subModalityAcademicPeriodIdSession;
            if (submodalityAcademicPeriodIdDefault.HasValue)
            {
                ModulosList = abetEntities.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == submodalityAcademicPeriodIdDefault.Value)
                    .Select(x => x.Modulo).ToList();
            }
            else
            {
                ModulosList = abetEntities.SubModalidadPeriodoAcademicoModulo
                    .Where(x => x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad == parsedModalityId)
                    .Select(x => x.Modulo).ToList();
            }
            Modulos = ModulosList.Select(x => new SelectListItem
            {
                Text = x.SubModalidadPeriodoAcademicoModulo.FirstOrDefault().SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreEspanol + " - " + x.SubModalidadPeriodoAcademicoModulo.FirstOrDefault().SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico + " - " + x.NombreEspanol,
                Value = x.IdModulo.ToString(),
            });
            if (Modulos.ToList().Count > 0)
                IdModulo = DefaultModuleId = int.Parse(Modulos.FirstOrDefault().Value);
        }
        public void LoadMeetingsByTeacher(CargarDatosContext datosContext, int p, int? idDocente, int idSubModalidadPeriodoAcademico)
        {
            var IdUnidadesAcademicas = (from uar in datosContext.context.UnidadAcademicaResponsable
                                        join sua in datosContext.context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                                        join d in datosContext.context.Docente on uar.IdDocente equals d.IdDocente
                                        join ua in datosContext.context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                                        where ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && d.IdDocente == idDocente
                                        select ua.IdUnidadAcademica).Distinct().ToList();

            var queryResultados = (from a in datosContext.context.ReunionProfesor
                                   join aa in datosContext.context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademicoModulo equals aa.IdSubModalidadPeriodoAcademicoModulo
                                   join bb in datosContext.context.SubModalidadPeriodoAcademico on aa.IdSubModalidadPeriodoAcademico equals bb.IdSubModalidadPeriodoAcademico
                                   join b in datosContext.context.ReunionProfesorParticipante on a.IdReunionProfesor equals b.IdReunionProfesor
                                   join c in datosContext.context.UnidadAcademica on a.IdUnidadAcademica equals c.IdUnidadAcademica
                                   join d in datosContext.context.SedeUnidadAcademica on c.IdUnidadAcademica equals d.IdUnidadAcademica
                                   join e in datosContext.context.UnidadAcademicaResponsable on d.IdSedeUnidadAcademica equals e.IdSedeUnidadAcademica
                                   join f in datosContext.context.Docente on e.IdDocente equals f.IdDocente
                                   where aa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && b.IdDocente == idDocente
                                   select new TeacherMeetingViewModel
                                   {
                                       IdReunionProfesor = a.IdReunionProfesor,
                                       NombreReunion = a.NombreReunion,
                                       NombreIngles = a.NombreIngles,
                                       Semana = a.Semana,
                                       NumSemana = a.NumSemana,
                                       Inicio = a.Inicio,
                                       Fin = a.Fin,
                                       PlazoHasta = a.PlazoHasta,
                                       Lugar = a.Lugar,
                                       Nivel = a.Nivel,
                                       AreaEncargada = a.AreaEncargada,
                                       AreaEncargadaIngles = a.AreaEncargadaIngles,
                                       IdUnidadAcademica = a.IdUnidadAcademica,
                                       IdPeriodoAcademico = bb.IdPeriodoAcademico,
                                       IdEscuela = a.IdEscuela,
                                       IdSubModalidadPeriodoAcademicoModulo = a.IdSubModalidadPeriodoAcademicoModulo,
                                       IdSubModalidadPeriodoAcademico = aa.IdSubModalidadPeriodoAcademico,
                                       IdModulo = aa.IdModulo,
                                       NombreEncargado = f.Nombres + " " + f.Apellidos,
                                       CodigoEncargado = f.Codigo,
                                       Notificada = a.Notificada,
                                       ActaCreada = a.ActaCreada
                                   }
                                   ).Distinct().AsQueryable();

            queryResultados = queryResultados.OrderBy(x => x.IdReunionProfesor);

            LoadFilters(idSubModalidadPeriodoAcademico, queryResultados);
            if (AcademicPeriodId == 0 && Level == 0 && Area == 0 && (Week == null || Week == "All"))
            {
                var ReunionesResponsable = (from a in queryResultados
                                            where IdUnidadesAcademicas.Contains(a.IdUnidadAcademica)
                                            select a).Distinct().OrderBy(x => x.IdReunionProfesor);


                TeacherMeetingsByResponsible = ReunionesResponsable.ToPagedList(p, ConstantHelpers.DEFAULT_PAGE_SIZE);
            }
            else
            {
                var ReunionesResponsable = (from a in queryResultados
                                            where IdUnidadesAcademicas.Contains(a.IdUnidadAcademica)
                                            select a);

                if (AcademicPeriodId != 0)
                    ReunionesResponsable = ReunionesResponsable.Where(x => x.IdPeriodoAcademico == AcademicPeriodId);
                if (Level != 0)
                    ReunionesResponsable = ReunionesResponsable.Where(x => x.Nivel == Level);
                if (Week != "All")
                {
                    var _week = short.Parse(Week);
                    ReunionesResponsable = ReunionesResponsable.Where(x => x.NumSemana == _week);
                }


                var result = ReunionesResponsable.Distinct().OrderBy(x => x.IdReunionProfesor);
                TeacherMeetingsByResponsible = result.ToPagedList(p, ConstantHelpers.DEFAULT_PAGE_SIZE);
            }
        }

        public void LoadAllTeacherMeetings(CargarDatosContext datosContext, int p, int idSubModalidadPeriodoAcademico)
        {

            var queryResultados = (from a in datosContext.context.ReunionProfesor
                                   join aa in datosContext.context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademicoModulo equals aa.IdSubModalidadPeriodoAcademicoModulo
                                   join b in datosContext.context.ReunionProfesorParticipante on a.IdReunionProfesor equals b.IdReunionProfesor
                                   join c in datosContext.context.UnidadAcademica on a.IdUnidadAcademica equals c.IdUnidadAcademica
                                   join d in datosContext.context.SedeUnidadAcademica on c.IdUnidadAcademica equals d.IdUnidadAcademica
                                   join e in datosContext.context.UnidadAcademicaResponsable on d.IdSedeUnidadAcademica equals e.IdSedeUnidadAcademica
                                   join f in datosContext.context.Docente on e.IdDocente equals f.IdDocente
                                   where aa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                                   select new TeacherMeetingViewModel
                                   {
                                       IdReunionProfesor = a.IdReunionProfesor,
                                       NombreReunion = a.NombreReunion,
                                       Semana = a.Semana,
                                       NumSemana = a.NumSemana,
                                       Inicio = a.Inicio,
                                       Fin = a.Fin,
                                       Lugar = a.Lugar,
                                       Nivel = a.Nivel,
                                       AreaEncargada = a.AreaEncargada,
                                       IdUnidadAcademica = a.IdUnidadAcademica,
                                       IdEscuela = a.IdEscuela,
                                       IdSubModalidadPeriodoAcademicoModulo = a.IdSubModalidadPeriodoAcademicoModulo,
                                       IdSubModalidadPeriodoAcademico = aa.IdSubModalidadPeriodoAcademico,
                                       IdModulo = aa.IdModulo,
                                       NombreEncargado = f.Nombres + " " + f.Apellidos,
                                       CodigoEncargado = f.Codigo,
                                       Notificada = a.Notificada,
                                       ActaCreada = a.ActaCreada
                                   }
                                   ).Distinct().AsQueryable();

            queryResultados = queryResultados.OrderBy(x => x.IdReunionProfesor);

            var ReunionesResponsable = (from a in queryResultados
                                        select a).Distinct().OrderBy(x => x.IdReunionProfesor);

            LoadFilters(idSubModalidadPeriodoAcademico, queryResultados);

            TeacherMeetingsByResponsible = ReunionesResponsable.ToPagedList(p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}