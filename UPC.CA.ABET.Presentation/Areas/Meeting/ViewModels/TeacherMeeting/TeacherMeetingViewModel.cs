﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingViewModel
{
        public TeacherMeetingLogic TeacherMeetingLogic { get; set; }
        public TeacherMeetingViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }
        public int IdPeriodoAcademico { get; set; }
        public int IdReunionProfesor { get; set; }
        public string NombreReunion { get; set; }
        public string NombreIngles { get; set; }
        public string Semana { get; set; }
        public short NumSemana { get; set; }
        public DateTime? Inicio { get; set; }
        public DateTime? Fin { get; set; }
        public DateTime? PlazoHasta { get; set; }
        public string Lugar { get; set; }
        public int Nivel { get; set; }
        public string AreaEncargada { get; set; }
        public string AreaEncargadaIngles { get; set; }
        public int IdUnidadAcademica { get; set; }
        public int IdEscuela { get; set; }
        public int? IdSubModalidadPeriodoAcademico { get; set; }
        public int? IdSubModalidadPeriodoAcademicoModulo { get; set; }
        public int? IdModulo { get; set; }
        public bool Notificada { get; set; }
        public bool ActaCreada { get; set; }
        public DateTime FechaActaFinaliza { get; set; }
        public string NombreEncargado { get; set; }
        public string CodigoEncargado { get; set; }

        public ReunionProfesor ReunionProfesor { get; set; }

        public void LoadData(int teacherMeetingId)
        {
            ReunionProfesor = FindTeacherMeeting(teacherMeetingId);
        }

        private ReunionProfesor FindTeacherMeeting(int teacherMeetingId)
        {
            return TeacherMeetingLogic.TeacherMeetingById(teacherMeetingId);
        }

        internal bool IsResponsibleOrAdmin(int teacherMeetingId, int subModalityAcademicPeriodId, AppRol[] roles, int teacherId = 0)
        {
            if (roles.Count() > 0 && roles.HasRole(AppRol.Administrador)) return true;
            return TeacherMeetingLogic.IsResponsibleForTheMeeting(teacherMeetingId, teacherId, subModalityAcademicPeriodId);
        }

        public IEnumerable<ReunionProfesorAcuerdoHistorico> GetPendingTeacherMeetingAgreements(int statusId, int weekNumber, int level, int subModalityAcademicPeriodModuleId, int academicUnitId)
        {
            var result = TeacherMeetingLogic.GetPendingTeacherMeetingAgreements(statusId, weekNumber, level, subModalityAcademicPeriodModuleId, academicUnitId);
            var historicTeacherMeetingAgreements = new List<ReunionProfesorAcuerdoHistorico>();
            foreach (var item in result)
            {
                historicTeacherMeetingAgreements.Add(new ReunionProfesorAcuerdoHistorico
                {
                    IdReunionProfesorAcuerdoHistorico = item.IdReunionProfesorAcuerdoHistorico,
                    IdReunionProfesorAcuerdo = item.IdReunionProfesorAcuerdo,
                    ComentarioCierreEspanol = item.ComentarioCierreEspanol,
                    ComentarioCierreIngles = item.ComentarioCierreIngles,
                    FechaCierre = item.FechaCierre,
                    IdEstado = item.IdEstado,
                });
            }
            return historicTeacherMeetingAgreements;
        }

        
    }
}