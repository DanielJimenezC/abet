﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using static UPC.CA.ABET.Helpers.ConstantHelpers;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class ChooseModuleViewModel
    {
        public IEnumerable<Modulo> Modulos { get; set; }
        public IEnumerable<SelectListItem> ModuleList { get; set; }
        public int IdModulo { get; set; }

        public void LoadModules(AbetEntities abetEntities, int defaultModalityId, int? subModalityAcademicPeriodId, int? subModalityAcademicPeriodIdSession)
        {
            var activeSubModalityAcademicPeriod = abetEntities.SubModalidadPeriodoAcademico
                    .FirstOrDefault(x => x.SubModalidad.Modalidad.IdModalidad == defaultModalityId
                        && x.PeriodoAcademico.Estado == ESTADO.ACTIVO);
            var submodalityAcademicPeriodIdDefault = subModalityAcademicPeriodId ?? subModalityAcademicPeriodIdSession;
            if (submodalityAcademicPeriodIdDefault.HasValue)
            {
                Modulos = abetEntities.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == submodalityAcademicPeriodIdDefault.Value)
                    .Select(x => x.Modulo).ToList();
            }
            else
            {
                Modulos = abetEntities.SubModalidadPeriodoAcademicoModulo
                    .Where(x => x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad == defaultModalityId)
                    .Select(x => x.Modulo).ToList();
            }

            ModuleList = Modulos.Select(x => new SelectListItem
            {
                Text = x.NombreEspanol.Trim(),
                Value = x.IdModulo.ToString(),
            });
        }
    }
}