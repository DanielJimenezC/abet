﻿using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Meeting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting
{
    public class TeacherMeetingPreviousTaskViewModel
    {
        private TeacherMeetingLogic TeacherMeetingLogic { get; set; }

        public TeacherMeetingPreviousTaskViewModel()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }
        public int IdReunionProfesorAccionMejora { get; set; }
        public int IdReunionProfesor { get; set; }
        public int IdAccionMejora { get; set; }
        public string AvanceEspanol { get; set; }
        public string AvanceIngles { get; set; }
        public ReunionProfesor ReunionProfesor { get; set; }
        public AccionMejora AccionMejora { get; set; }

        public IEnumerable<ReunionProfesorAcuerdo> ReunionProfesorAcuerdosPrevios { get; set; }

        public IEnumerable<ReunionProfesorAccionMejora> ReunionProfesorAccionesMejora { get; set; }

        public void LoadData(int teacherMeetingId)
        {
            IdReunionProfesor = teacherMeetingId;
            LoadImprovementActions(teacherMeetingId);
            PreviousAgreements(teacherMeetingId);
        }
        private void LoadImprovementActions(int teacherMeetingId)
        {
            ReunionProfesorAccionesMejora = TeacherMeetingLogic.GetAllTeacherMeetingImprovementActions(teacherMeetingId);
        }

        private void PreviousAgreements(int teacherMeetingId)
        {
            ReunionProfesorAcuerdosPrevios = TeacherMeetingLogic.GetPreviousTeacherMeetingAgreements(teacherMeetingId);
        }
    }
}