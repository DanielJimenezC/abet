﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class AgregarEditarImagenViewModel
    {
        public Int32? IdEvidencia { get; set; }
        public Int32 IdReunionDelegado { get; set; }
        public Int32 Orden { get; set; }
        public Evidencia evidencia { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase file { get; set; }
   

        public AgregarEditarImagenViewModel()
        {
            evidencia = new Evidencia();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32? idEvidencia)
        {
            IdReunionDelegado = idReunionDelegado;
            IdEvidencia = idEvidencia;

            List<Int32> lstOrden = (from e in dataContext.context.Evidencia
                                    where e.IdReunionDelegado == idReunionDelegado
                                    select e.Orden).ToList();
            if (lstOrden.Count == 0)
            {
                Orden = 1;
            }
            else
                Orden = lstOrden.Max()+1;

            if (IdEvidencia.HasValue)
            {
                evidencia = dataContext.context.Evidencia.FirstOrDefault(x => x.IdEvidencia == IdEvidencia);
                Orden = evidencia.Orden;
            }
        }
    }
}