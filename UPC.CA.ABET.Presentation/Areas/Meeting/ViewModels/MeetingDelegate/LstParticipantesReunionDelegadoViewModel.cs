﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class LstParticipantesReunionDelegadoViewModel
    {
        public int IdReunionDelegado { get; set; }
        public int? IdInstrumento { get; set; }
        public ReunionDelegado rd;

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public List<AlumnoSeccion> LstDelegados { get; set; }
        public List<bool> LstDelegadosAsistentes { get; set; }
        public List<int> LstIdAlumnoSeccion { get; set; }
        public List<int> LstCursoID { get; set; }

        public LstParticipantesReunionDelegadoViewModel()
        {
            rd = new ReunionDelegado();
            LstDelegados = new List<AlumnoSeccion>();
            LstDelegadosAsistentes = new List<bool>();
            LstIdAlumnoSeccion = new List<int>();
            LstCursoID = new List<int>();
        }

        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;
            rd = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
            Fecha = rd.Fecha.Value;
            //int IdCiclo = rd.IdPeriodoAcademico;
            int IdCiclo = rd.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            IdInstrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
            if (IdInstrumento.HasValue == false)
            {
                IdInstrumento = 8;
            }

            //List<Seccion> secciones = dataContext.context.Seccion.Where(x => x.CursoPeriodoAcademico.IdCurso == rd.IdCurso && x.CursoPeriodoAcademico.IdPeriodoAcademico == rd.IdPeriodoAcademico).ToList();
            LstCursoID = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => x.IdCurso).ToList();

            var query0 = (from als in dataContext.context.AlumnoSeccion
                          join am in dataContext.context.AlumnoMatriculado on als.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                          join a in dataContext.context.Alumno on am.IdAlumno equals a.IdAlumno
                          join s in dataContext.context.Seccion on als.IdSeccion equals s.IdSeccion
                          join cpa in dataContext.context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                          //cpa.IdCurso == IdCurso
                          //where cpa.IdPeriodoAcademico == IdCiclo && LstCursoID.Contains(cpa.IdCurso) && als.EsDelegado == true
                          where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && LstCursoID.Contains(cpa.IdCurso) && als.EsDelegado == true
                          select als).Distinct().OrderBy(x => x.AlumnoMatriculado.Alumno.Apellidos);

            LstDelegados = query0.ToList();

            for(int i =0; i<LstDelegados.Count; i++)
            {
                int IdAlumnoSeccion = LstDelegados[i].IdAlumnoSeccion;
                ReunionDelegadoAlumnoSeccion rdas = dataContext.context.ReunionDelegadoAlumnoSeccion.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado && x.IdAlumnoSeccion == IdAlumnoSeccion);

                if(rdas==null)
                    LstDelegadosAsistentes.Add(false);
                else
                    LstDelegadosAsistentes.Add(true);


                LstIdAlumnoSeccion.Add(LstDelegados[i].IdAlumnoSeccion);
            }

        }
    }
}