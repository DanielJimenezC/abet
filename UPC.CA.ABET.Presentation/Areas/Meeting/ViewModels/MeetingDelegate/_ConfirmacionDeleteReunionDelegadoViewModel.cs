﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class _ConfirmacionDeleteReunionDelegadoViewModel
    {

        public Int32 IdReunionDelegado { get; set; }

        public ReunionDelegado rd { get; set; }

        public List<String> lstNombreCursos { get; set; }

        public _ConfirmacionDeleteReunionDelegadoViewModel()
        {

        }
        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            lstNombreCursos = (from rdc in dataContext.context.ReunionDelegadoCurso
                               join rde in dataContext.context.ReunionDelegado on rdc.IdReunionDelegado equals rde.IdReunionDelegado
                               where rdc.IdReunionDelegado == idReunionDelegado
                               select rdc.Curso.NombreEspanol).ToList();


            IdReunionDelegado = idReunionDelegado;
            rd = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);



        }
    }
}