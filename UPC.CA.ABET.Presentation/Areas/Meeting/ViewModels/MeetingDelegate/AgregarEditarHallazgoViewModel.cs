﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class AgregarEditarHallazgoViewModel
    {
        public List<Curso> LstCursos { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }
        public ReunionDelegado reunion { get; set; }
        public Int32 IdReunionDelegado { get; set; }
        public ConstituyenteInstrumento constituyenteinstrumento { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdCriticidad { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdConstituyente { get; set; }
        public Int32? IdInstrumento { get; set; }
        public Int32? IdSede { get; set; }
        //public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public List<OutcomeComision> LstOutcome { get; set; }
        public List<string> LstOutcomeDescripcion { get; set; }
        public Int32? IdHallazgo { get; set; }
        public String descripcionEspanol { get; set; }
        public Hallazgo hallazgo { get; set; }


        public AgregarEditarHallazgoViewModel()
        {
            reunion = new ReunionDelegado();
            LstCursos = new List<Curso>();
            constituyenteinstrumento = new ConstituyenteInstrumento();
            LstNivelAceptacion = new List<NivelAceptacionHallazgo>();
            LstCriticidad = new List<Criticidad>();
            LstOutcome = new List<OutcomeComision>();
            LstOutcomeDescripcion = new List<string>();
            descripcionEspanol = "";
            hallazgo = new Hallazgo();
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32? idHallazgo)
        {
            IdReunionDelegado = idReunionDelegado;
            reunion = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);

            LstCursos = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => x.Curso).ToList();

            Int32? IdInstrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
            if (IdInstrumento.HasValue == false)
                IdInstrumento = 8;

            constituyenteinstrumento = dataContext.context.ConstituyenteInstrumento.FirstOrDefault(x => x.IdInstrumento == IdInstrumento);
            IdInstrumento = constituyenteinstrumento.IdInstrumento;
            IdConstituyente = constituyenteinstrumento.IdConstituyente;
            IdSede = reunion.IdSede;
            //IdPeriodoAcademico = reunion.IdPeriodoAcademico;
            IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico;
            LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();


            if(idHallazgo.HasValue)
            {
                IdHallazgo = idHallazgo;
                hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
                IdCriticidad = hallazgo.IdCriticidad;
                descripcionEspanol = hallazgo.DescripcionEspanol;

            }

        }


        public void CargarDatosPartial(CargarDatosContext dataContext, Int32? idCurso, Int32 idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;
            reunion = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);

            var queryMC = (from mc in dataContext.context.MallaCocos
                           join mcd in dataContext.context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                           join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                           join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                           //where (cmc.IdCurso == idCurso && mc.IdPeriodoAcademico == reunion.IdPeriodoAcademico)
                           where (cmc.IdCurso == idCurso && mc.IdSubModalidadPeriodoAcademico == reunion.IdSubModalidadPeriodoAcademico)
                           select oc );

            LstOutcome = queryMC.Distinct().ToList();



            for(int i=0; i< LstOutcome.Count; i++)
            {
                LstOutcomeDescripcion.Add("");

                OutcomeComision oc = LstOutcome[i];

                LstOutcome[i].Outcome.Nombre = oc.Comision.Codigo + " | " + oc.Outcome.Nombre;

            }
        }
    }
}