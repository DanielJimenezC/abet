﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class _AddEditHallazgoRDViewModel
    {
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        //public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdAcreditadora { get; set; }
        public Int32? IdCriticidad { get; set; }
        public Int32? IdReunionDelegado { get; set; }

        public Int32? IdNivelAceptacionHallazgo { get; set; }
        //public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        //public Int32? IdNumeroPractica { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdHallazgo { get; set; }

        //public List<OutcomeEncuestaConfig> LstOutcome { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Acreditadora> LstAcreditadora { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<SelectListItem> LstCustomOutcomes { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }

        public String NombreCarrera { get; set; }
        public String NombreComision { get; set; }
        public String NombreCurso { get; set; }
        public String NombreAcreditadora { get; set; }

        public String NombreOutcome { get; set; }
        public String NombreNivelAceptacion { get; set; }
        public String NombreSede { get; set; }

        public Acreditadora acreditadora { get; set; }
        public Carrera carrera { get; set; }

        //  public Boolean Search { get; set; }

        public _AddEditHallazgoRDViewModel()
        {
            LstNivelAceptacion = new List<NivelAceptacionHallazgo>();
            LstCriticidad = new List<Criticidad>();
            LstCustomOutcomes = new List<SelectListItem>();

        }

        //public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32? idCurso, Int32? PeriodoAcademicoId, Int32? idSede, Int32 escuelaId, Int32 idHallazgo)
        public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado, Int32? idCurso, Int32? idSubModalidadPeriodoAcademico, Int32? idSede, Int32 idEscuela, Int32 idHallazgo)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            
            IdReunionDelegado = idReunionDelegado;
            //IdPeriodoAcademico = PeriodoAcademicoId;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            IdSede = idSede;
            NombreSede = dataContext.context.Sede.FirstOrDefault(x => x.IdSede == idSede).Nombre;
            IdCurso = idCurso;
            NombreCurso = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == idCurso).NombreEspanol : dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == idCurso).NombreIngles;

            acreditadora = dataContext.context.Acreditadora.FirstOrDefault(x => x.Nombre=="ABET");

            NombreAcreditadora = acreditadora.Nombre;

            carrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdEscuela == idEscuela);
            IdCarrera = carrera.IdCarrera;
            NombreCarrera = language == ConstantHelpers.CULTURE.ESPANOL ? carrera.NombreEspanol : carrera.NombreIngles;


            LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();
            LstCustomOutcomes = (from cc in dataContext.context.CarreraComision
                                 join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
                                 join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
                                 join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
                                 join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
                                 //where (c.IdEscuela == escuelaId && cc.IdPeriodoAcademico == PeriodoAcademicoId && co.Codigo!="WASC")
                                 where (c.IdEscuela == idEscuela && cc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && co.Codigo != "WASC")
                                 orderby oc.Comision.Codigo
                                 select new SelectListItem
                                 {
                                     Value = oc.IdOutcome.ToString(),
                                     Text = oc.Comision.Codigo + " | " + oc.Outcome.Nombre
                                 }).Distinct().ToList();


            IdHallazgo = idHallazgo;

            if(IdHallazgo!=0)
            {
                Hallazgo hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                IdOutcome = hallazgo.IdOutcome;
                IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
                IdCriticidad = hallazgo.IdCriticidad;
                DescripcionEspanol = hallazgo.DescripcionEspanol;
                DescripcionIngles = hallazgo.DescripcionIngles;
            }

        }
    }
}