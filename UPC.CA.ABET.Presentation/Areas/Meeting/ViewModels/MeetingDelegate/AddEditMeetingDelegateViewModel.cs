﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.Validation;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class AddEditMeetingDelegateViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
       // public Int32? IdCurso { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public Int32? IdReunionDelegado { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Sede> LstSede { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }

        public String Comentario { get; set; }

        public List<Int32> LstIdCursos { get; set; }

        public AddEditMeetingDelegateViewModel()
        {
            LstCurso = new List<Curso>();
            LstSede = new List<Sede>();
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstIdCursos = new List<Int32>();
        }

        public void Fill(CargarDatosContext dataContext, Int32? idReunionDelegado, Int32? CicloId, Int32? EscuelaId)
        {
            // IdReunionDelegado = 0;
            this.IdReunionDelegado = idReunionDelegado;
            IdCiclo = CicloId.Value;
            Ciclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdCiclo);
            IdEscuela = EscuelaId.Value;
            //   IdCurso = null;
           // IdCurso = null;
            Fecha = DateTime.Now;
            var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join cur in dataContext.context.Curso on cpa.IdCurso equals cur.IdCurso
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          //
                          join smpa in dataContext.context.SubModalidadPeriodoAcademico on oc.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                          //join pa in dataContext.context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                          //
                          //where oc.IdPeriodoAcademico == IdCiclo && c.IdEscuela == IdEscuela
                          where smpa.IdPeriodoAcademico == IdCiclo && c.IdEscuela == IdEscuela
                          select cur).Distinct().OrderBy(x => x.NombreEspanol) ;
                /*var query0 = (from cpa in dataContext.context.CursoPeriodoAcademico
                              join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                              join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                              where cpa.IdPeriodoAcademico == IdCiclo && ca.IdEscuela == IdEscuela
                              select cpa.IdCurso);*/

          /*  var query0 = (from als in dataContext.context.AlumnoSeccion
                          join s in dataContext.context.Seccion on als.IdSeccion equals s.IdSeccion
                          join cpa in dataContext.context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Curso on cpa.IdCurso equals c.IdCurso
                          join ca in dataContext.context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                          where cpa.IdPeriodoAcademico == IdCiclo && ca.IdEscuela == IdEscuela && als.EsDelegado == true
                          select c).Distinct().OrderBy(x => x.NombreEspanol);*/
            LstCurso = query0.ToList();


            LstSede = dataContext.context.Sede.ToList();

            if (IdReunionDelegado.HasValue)
            {
                ReunionDelegado reunionDelegado = new ReunionDelegado();
                reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(X => X.IdReunionDelegado == idReunionDelegado.Value);
                if (reunionDelegado == null)
                    return;

                LstIdCursos = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => x.IdCurso).ToList();


                /*      ReunionDelegado rd = new ReunionDelegado();
                       rd = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado.Value);*/
                //IdCiclo = reunionDelegado.IdPeriodoAcademico;
                IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                IdEscuela = reunionDelegado.IdEscuela;
                IdSede = reunionDelegado.IdSede;
             //   IdCurso = reunionDelegado.IdCurso;
                Fecha = reunionDelegado.Fecha.Value.Date;
                Comentario = reunionDelegado.Comentario;
            }
        }
    }
}