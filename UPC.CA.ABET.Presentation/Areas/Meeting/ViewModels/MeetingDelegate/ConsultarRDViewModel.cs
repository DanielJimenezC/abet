﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class ConsultarRDViewModel
    {
       // public Int32 IdCurso { get; set; }
        public ReunionDelegado reunionDelegado { get; set; }
        public Int32 IdCiclo { get; set; }
        public Int32 IdEscuela { get; set; }
        public Int32 IdReunionDelegado { get; set; }

        public List<AlumnoSeccion> LstAlumnosSeccion { get; set; }

        public List<bool> LstAsistencia { get; set; }
        public List<int> LstCursoID { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Hallazgo> LstHallazgos { get; set; }

        public  List<Evidencia> LstEvidencias { get; set; }

        public ConsultarRDViewModel()
        {
            LstAlumnosSeccion = new List<AlumnoSeccion>();
            LstAsistencia = new List<bool>();
            LstCursoID = new List<int>();
            LstCurso = new List<Curso>();
            LstHallazgos = new List<Hallazgo>();
            LstEvidencias = new List<Evidencia>();
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;
            reunionDelegado = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
            //IdCurso = reunionDelegado.IdCurso;
            //IdCiclo = reunionDelegado.IdPeriodoAcademico;
            IdCiclo = reunionDelegado.SubModalidadPeriodoAcademico.IdPeriodoAcademico;

            LstCurso = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x=>x.Curso).ToList();
            LstCursoID = LstCurso.Select(x => x.IdCurso).ToList();

            int? idInstrumentoRD = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
            if(idInstrumentoRD.HasValue==false)
            {
                idInstrumentoRD = 8;
            }


            LstHallazgos = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == idInstrumentoRD && x.IdReunionDelegado == IdReunionDelegado && x.Estado == "ACT").OrderBy(x=>x.IdCriticidad).ToList();








            var query0 = (from als in dataContext.context.AlumnoSeccion
                          join am in dataContext.context.AlumnoMatriculado on als.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                          join a in dataContext.context.Alumno on am.IdAlumno equals a.IdAlumno
                          join s in dataContext.context.Seccion on als.IdSeccion equals s.IdSeccion
                          join cpa in dataContext.context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico

                          //where cpa.IdPeriodoAcademico == IdCiclo && LstCursoID.Contains(cpa.IdCurso) && als.EsDelegado == true
                          where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && LstCursoID.Contains(cpa.IdCurso) && als.EsDelegado == true
                          select als).Distinct().OrderBy(x=>x.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol).ThenBy(x => x.AlumnoMatriculado.Alumno.Apellidos);

            LstAlumnosSeccion = query0.ToList();

            foreach(var item in LstAlumnosSeccion)
            {
                ReunionDelegadoAlumnoSeccion rdas = dataContext.context.ReunionDelegadoAlumnoSeccion.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado && x.IdAlumnoSeccion == item.IdAlumnoSeccion);

                if (rdas == null)
                    LstAsistencia.Add(false);
                else
                    LstAsistencia.Add(true);
            }


            LstEvidencias = dataContext.context.Evidencia.Where(x => x.IdReunionDelegado == IdReunionDelegado).OrderBy(x => x.Orden).ToList();



        }
    }
}