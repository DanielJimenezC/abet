﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class LstReunionDelegadosViewModel
    {
        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdPeriodoAcademico { get; set; }
        
        public Int32? IdCurso { get; set; }

        public Int32? IdSede { get; set; }
        public PeriodoAcademico Ciclo { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Sede> LstSede { get; set; }
        public IPagedList<ReunionDelegado> LstReunion { get; set; }
        public Int32? NumeroPagina { get; set; }

        public LstReunionDelegadosViewModel()
        {
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstCurso = new List<Curso>();
            LstSede = new List<Sede>();

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? numeroPagina, Int32 idPeriodoAcademico, Int32? idCurso, Int32? idSede, Int32 idEscuela)
        {


           int  IdSubModalidadPeriodoAcademico = (from s in dataContext.context.SubModalidadPeriodoAcademico
                                              where s.IdPeriodoAcademico == idPeriodoAcademico
                                              select s.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            IdPeriodoAcademico = idPeriodoAcademico;
            IdCurso = idCurso;
            IdSede = idSede;

            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            Ciclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == idPeriodoAcademico);
            /*var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          where oc.IdPeriodoAcademico == PeriodoAcademicoId && c.IdEscuela == escuelaId
                          select cmc.IdCurso);
            LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x=>x.NombreEspanol).ToList();*/

            LstCurso = (from mcd in dataContext.context.MallaCocosDetalle
                        join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                        join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                        join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                        join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                        join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                        join cur in dataContext.context.Curso on cmc.IdCurso equals cur.IdCurso
                        //where oc.IdPeriodoAcademico == PeriodoAcademicoId && c.IdEscuela == escuelaId
                        where oc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && c.IdEscuela == idEscuela
                        select cur).Distinct().OrderBy(x => x.NombreEspanol).ToList();
           // LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).OrderBy(x => x.NombreEspanol).ToList();

            LstSede = dataContext.context.Sede.ToList();



            NumeroPagina = numeroPagina ?? 1;
            // var query = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == 8 && x.Estado == "ACT").ToList();

            var query = dataContext.context.ReunionDelegado.Where(x =>x.Estado == "ACT" && x.IdEscuela == idEscuela && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).ToList();

            if (IdSede.HasValue)
                query = query.Where(x => x.IdSede == IdSede).ToList();

            if (IdCurso.HasValue)
            {

                var reuniones = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdCurso == IdCurso).Select(x=>x.IdReunionDelegado).ToList();
                query = query.Where(x => reuniones.Contains(x.IdReunionDelegado)).ToList();

            }

                


            LstReunion = query.OrderBy(x => x.Fecha).ThenByDescending(x => x.IdReunionDelegado).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

        }
    }
}