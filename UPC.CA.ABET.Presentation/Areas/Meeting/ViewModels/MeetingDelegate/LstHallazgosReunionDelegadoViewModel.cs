﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class LstHallazgosReunionDelegadoViewModel
    {
        public int IdReunionDelegado;
        public int? IdInstrumento;
        public ReunionDelegado rd;

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public List<Hallazgo> LstHallazgos { get; set; }

        public LstHallazgosReunionDelegadoViewModel()
        {
            rd = new ReunionDelegado();
            LstHallazgos = new List<Hallazgo>();
        }

        public void Fill(CargarDatosContext dataContext, Int32 idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;
            rd = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
            Fecha = rd.Fecha.Value;
            IdInstrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
            if(IdInstrumento.HasValue==false)
            {
                IdInstrumento = 8;
            }
            LstHallazgos  = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == IdInstrumento && x.IdReunionDelegado == idReunionDelegado && x.Estado =="ACT" ).OrderBy(x => x.IdCriticidad).ToList();

        }
    }
}