﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate
{
    public class PrintReunionDelegadoViewModel
    {
        public int IdReunionDelegado { get; set; }
        public ReunionDelegado reunion { get; set; }

        public List<Curso> LstCursos { get; set; }
        public List<int> LstCursoID { get; set; }
        public List<Hallazgo> LstHallazgos { get; set; }

        public List<AlumnoSeccion> LstAlumnosSeccion { get; set; }
        public List<bool> LstAsistencia { get; set; }

        public PrintReunionDelegadoViewModel()
        {
            reunion = new ReunionDelegado();
            LstCursos = new List<Curso>();
            LstHallazgos = new List<Hallazgo>();
            LstCursoID = new List<int>();
            LstAlumnosSeccion = new List<AlumnoSeccion>();
            LstAsistencia = new List<bool>();
        }

        public void CargarDatos(CargarDatosContext dataContext, int idReunionDelegado)
        {
            IdReunionDelegado = idReunionDelegado;

            reunion = dataContext.context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);

            LstCursos = dataContext.context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == IdReunionDelegado).Select(x => x.Curso).ToList();
            LstCursoID = LstCursos.Select(x => x.IdCurso).ToList();

            int? idInstrumentoRD = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
            if (idInstrumentoRD.HasValue == false)
            {
                idInstrumentoRD = 8;
            }

            //int IdCiclo = reunion.IdPeriodoAcademico;
            int IdCiclo = reunion.SubModalidadPeriodoAcademico.IdPeriodoAcademico;

            LstHallazgos = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == idInstrumentoRD && x.IdReunionDelegado == IdReunionDelegado && x.Estado == "ACT").ToList();

            var query0 = (from als in dataContext.context.AlumnoSeccion
                          join am in dataContext.context.AlumnoMatriculado on als.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                          join a in dataContext.context.Alumno on am.IdAlumno equals a.IdAlumno
                          join s in dataContext.context.Seccion on als.IdSeccion equals s.IdSeccion
                          join cpa in dataContext.context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico

                          where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && LstCursoID.Contains(cpa.IdCurso) && als.EsDelegado == true
                          select als).Distinct().OrderBy(x => x.AlumnoMatriculado.Alumno.Apellidos);

            LstAlumnosSeccion = query0.ToList();

            foreach (var item in LstAlumnosSeccion)
            {
                ReunionDelegadoAlumnoSeccion rdas = dataContext.context.ReunionDelegadoAlumnoSeccion.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado && x.IdAlumnoSeccion == item.IdAlumnoSeccion);

                if (rdas == null)
                    LstAsistencia.Add(false);
                else
                    LstAsistencia.Add(true);
            }

        }

    }
}