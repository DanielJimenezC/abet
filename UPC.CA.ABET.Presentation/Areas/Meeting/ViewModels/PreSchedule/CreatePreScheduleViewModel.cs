﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class CreatePreScheduleViewModel
    {
        public IEnumerable<SelectListItem> Niveles { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public IEnumerable<SelectListItem> UnidadesAcademicas { get; set; }
        public string TituloUnidadAcademica { get; set; }
        public PreAgenda PreAgenda { get; set; }
    }
}