﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class _DeletePreScheduleCommitteViewModel
    {
        public Int32 IdPreAgenda { get; set; }

        public void Fill(AbetEntities ctx, Int32 idPreAgenda)
        {
            IdPreAgenda = idPreAgenda;
        }
    }
}