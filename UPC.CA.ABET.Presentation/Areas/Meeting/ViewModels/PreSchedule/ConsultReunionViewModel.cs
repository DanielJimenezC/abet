﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class ConsultReunionViewModel
    {
        public IEnumerable<SelectListItem> Niveles { get; set; }
        public int IdNivel { get; set; }
        public IEnumerable<SelectListItem> Semanas { get; set; }
        public int IdSemana { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public int IdSede { get; set; }
        public int IdArea { get; set; }
        public IPagedList<Reunion> ListaReunionResultado { get; set; }
    }
}