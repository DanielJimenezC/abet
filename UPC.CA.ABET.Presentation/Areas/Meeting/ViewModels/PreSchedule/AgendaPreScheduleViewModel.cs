﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class AgendaPreScheduleViewModel
    {
        public int IdPreAgenda { get; set; }
        public string Motivo { get; set; }
        public List<TemaPreAgenda> ListaTemasPreAgenda { get; set; }
    }
}