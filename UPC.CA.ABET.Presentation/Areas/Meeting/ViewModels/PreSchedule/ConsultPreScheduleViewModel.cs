﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class ConsultPreScheduleViewModel
    {
        public IEnumerable<SelectListItem> Niveles { get; set; }
        public int? IdNivel { get; set; }
        public IEnumerable<SelectListItem> Semanas { get; set; }
        public int? IdSemana { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }
        public int? IdSede { get; set; }
        public IPagedList<PreAgenda> ListaPreAgendaResultado { get; set; }
        public int? IdArea { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }

        public ConsultPreScheduleViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext datacontext, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, bool? isSearchRequest, int? idArea, String tipoCoordinacion, Int32 p)
        {
            IdNivel = idNivel;
            IdSemana = IdSemana;
            IdSede = idSede;
            IdArea = idArea;

            var idDocente = datacontext.session.GetDocenteId();
            //var goodEq = Expression.Equal(Expression.Convert(someIntExpr, typeof(int?)), someNubIntExpr);
            var idPeriodoAcademico = datacontext.session.GetPeriodoAcademicoId();
            var lstNivelesPermitidos = new List<Int32>();
            //int idPeriodo = Expression.Equal(Expression.Convert(idPeriodo, typeof(int?), PeriodoAcademicoId);
            var idPeriodoAcad = (idPeriodoAcademico.HasValue) ? idPeriodoAcademico.Value : -1;
            var lstNivelesDocente = datacontext.context.UnidadAcademicaResponsable.Include(x => x.SedeUnidadAcademica).Include(x => x.SedeUnidadAcademica.UnidadAcademica).
                //Where(x => x.IdDocente == idDocente && x.SedeUnidadAcademica.UnidadAcademica.IdPeriodoAcademico == PeriodoAcademicoId).ToList();
                Where(x => x.IdDocente == idDocente && x.SedeUnidadAcademica.UnidadAcademica.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).ToList();

            var menorNivel = 4;
            foreach (var item in lstNivelesDocente) {
                if (menorNivel > item.SedeUnidadAcademica.UnidadAcademica.Nivel)
                    menorNivel = item.SedeUnidadAcademica.UnidadAcademica.Nivel;
            }
            if (menorNivel != 4)
            {
                this.Niveles = datacontext.context.UnidadAcademica.Where(x => x.Nivel >= menorNivel).Select(x => new SelectListItem() { Value = x.Nivel.ToString(), Text = x.Nivel.ToString() }).Distinct().ToList();
                lstNivelesPermitidos = datacontext.context.UnidadAcademica.Where(x => x.Nivel >= menorNivel).Select(x=>x.Nivel).Distinct().ToList();
            }
                
            
            var idEscuela = datacontext.session.GetEscuelaId();

            this.Semanas = GedServices.GetSemanasDisponiblesServices(datacontext.context).Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            this.Sedes = GedServices.GetAllSedesServices(datacontext.context).Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            this.Areas = GedServices.GetAreasByPeriodoAcademico(datacontext.context, idPeriodoAcad, idEscuela).Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = c.NombreEspanol });

            //var periodoAcademico = datacontext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == PeriodoAcademicoId);
            var periodoAcademico = datacontext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == datacontext.context.SubModalidadPeriodoAcademico.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == idPeriodoAcademico).IdPeriodoAcademico);
            //if ((!FechaInicio.HasValue || FechaInicio.Value == DateTime.MinValue) && periodoAcademico != null && periodoAcademico.FechaInicioCiclo.HasValue)
            if ((!FechaInicio.HasValue || FechaInicio.Value == DateTime.MinValue) && periodoAcademico != null && periodoAcademico.FechaInicioPeriodo.HasValue)
                FechaInicio = periodoAcademico.FechaInicioPeriodo.Value;

            if ((!FechaFin.HasValue || FechaFin.Value == DateTime.MinValue) && periodoAcademico != null && periodoAcademico.FechaInicioPeriodo.HasValue)
                FechaFin = periodoAcademico.FechaFinPeriodo.Value;

            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                //Temporalmente se agrego 1 OJO REVISAR
                if (tipoCoordinacion == ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO)
                {

                    this.ListaPreAgendaResultado = GedServices.GetPreAgendasBusquedaServices(datacontext.context, idNivel, idSemana, FechaInicio, FechaFin, idSede,idArea, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO).Where(x=>x.Nivel.HasValue && lstNivelesPermitidos.Contains(x.Nivel.Value)).ToPagedList(p,ConstantHelpers.DEFAULT_PAGE_SIZE);
                }
                else
                {
                    this.ListaPreAgendaResultado = GedServices.GetPreAgendasBusquedaServices(datacontext.context, idNivel, idSemana, FechaInicio, FechaFin, idSede, idArea, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO).Where(x => x.Nivel.HasValue && lstNivelesPermitidos.Contains(x.Nivel.Value)).ToPagedList(p, ConstantHelpers.DEFAULT_PAGE_SIZE);
                }
            }

            this.FechaInicio = FechaInicio;
            this.FechaFin = FechaFin;
        }
    }
}