﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class AgendaPreScheduleCommitteViewModel
    {
        public int IdPreAgenda { get; set; }
        public string Motivo { get; set; }
        public List<TemaPreAgenda> ListaTemaPreAgendaComite { get; set; }
    }
}