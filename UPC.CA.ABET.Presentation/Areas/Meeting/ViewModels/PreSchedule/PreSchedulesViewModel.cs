﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule
{
    public class PreSchedulesViewModel
    {
        public Int32? IdNivel { get; set; }
        public IEnumerable<SelectListItem> Niveles { get; set; }


        public Int32? IdSemana { get; set; }
        public IEnumerable<SelectListItem> Semanas { get; set; }

        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }

        public Int32? IdSede { get; set; }
        public IEnumerable<SelectListItem> Sedes { get; set; }

        public Int32? IdArea { get; set; }
        public String Area { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }

        public Int32 idPeriodoAcad { get; set; }

        public List<PreAgenda> ListaPreAgendaResultado { get; set; }

        public void CargarDatos(CargarDatosContext datacontext)
        {
            var idEscuela = datacontext.session.GetEscuelaId();
                
            idPeriodoAcad = datacontext.context.PeriodoAcademico.FirstOrDefault(X =>X.Estado.Equals(Helpers.ConstantHelpers.ESTADO.ACTIVO) || (X.FechaInicioPeriodo <= DateTime.Now && X.FechaFinPeriodo >= DateTime.Now)).IdPeriodoAcademico;
            this.Semanas = GedServices.GetSemanasDisponiblesServices(datacontext.context).Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            this.Sedes   = GedServices.GetAllSedesServices(datacontext.context).Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            this.Areas   = GedServices.GetAreasByPeriodoAcademico(datacontext.context, idPeriodoAcad, idEscuela)
                .Select(c => new SelectListItem 
                {
                    Value = c.IdUnidadAcademica.ToString(), 
                    Text = datacontext.currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles 
                });

            this.Niveles = GedServices.GetNivelesUsuarioLogueadoServices(datacontext.context, datacontext.session).Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() }); ;
            var AreaNombre = this.Areas.FirstOrDefault(X=>X.Value.Equals((IdArea ?? -1).ToString()));
            Area = AreaNombre != null ? AreaNombre.Text : "";
            FechaInicio = FechaInicio ?? DateTime.Now.AddMonths(-6);
            FechaFin = FechaFin  ?? DateTime.Now.AddMonths(6);
                        
            var query = datacontext.context.PreAgenda.Where(x => x.TipoPreagenda == ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO
                        && x.FechaReunion.Value >= FechaInicio && x.FechaReunion.Value <= FechaFin
                        && x.UnidadAcademica.NombreEspanol.Contains(Area));      

            if (this.IdNivel.HasValue)
                query = query.Where(x => x.Nivel.Value == IdNivel.Value);
            if (this.IdSede.HasValue)
                query = query.Where(x => x.IdSede == IdSede.Value);
            if (this.IdSemana.HasValue)
                query = query.Where(x => x.Semana == IdSemana.Value);

            if (query != null)
            {
                ListaPreAgendaResultado = query.ToList();
            }          
        }


    }
}