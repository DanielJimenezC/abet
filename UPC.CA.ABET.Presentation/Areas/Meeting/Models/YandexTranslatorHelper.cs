﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Models
{
    public class YandexTranslatorHelper
    {
        public YandexTranslatorHelper()
        {
            key = "trnsl.1.1.20180609T041203Z.57b0f738d560afd7.22725a9f4f05b9c776358e5c1295c76c34b40270";
            direccion = "es-en";
        }

        public string key { get; set; }

        public string direccion { get; set; }
        
        public string textoEntrada { get; set; }
        
        public string textoSalida { get; set; }
    }
}