﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Models
{
    public class EmailHelper
    {
        public static string MeetingRequestString(string mailOrganizador, string mailRespuesta, string nombreEvento, string descripcionEvento, string ubicacion, DateTime fechaInicio, DateTime fechaFin, int? eventoId = null, bool esCancelacion = false)
        {
            StringBuilder str = new StringBuilder();

            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN");
            str.AppendLine("VERSION:2.0");
            str.AppendLine(string.Format("METHOD:{0}", (esCancelacion ? "CANCEL" : "REQUEST")));
            str.AppendLine("BEGIN:VEVENT");

            int gapHours = 0; //GetTimeZoneOffsetHours(timezone);

            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", fechaInicio.AddHours(gapHours).ToUniversalTime()));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmss}", DateTime.Now));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", fechaFin.AddHours(gapHours).ToUniversalTime()));
            str.AppendLine(string.Format("LOCATION: {0}", ubicacion));
            str.AppendLine(string.Format("UID:{0}", (eventoId.HasValue ? "GEDREUNION" + eventoId : Guid.NewGuid().ToString())));
            str.AppendLine(string.Format("DESCRIPTION:{0}", descripcionEvento.Replace("\n", "<br>")));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", descripcionEvento.Replace("\n", "<br>")));
            str.AppendLine(string.Format("SUMMARY:{0}", nombreEvento));

            str.AppendLine(string.Format("ORGANIZER;CN=\"{0}\":MAILTO:{1}", mailOrganizador, mailOrganizador));
            str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", mailRespuesta, mailRespuesta));

            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");

            return str.ToString();
        }
    }
}