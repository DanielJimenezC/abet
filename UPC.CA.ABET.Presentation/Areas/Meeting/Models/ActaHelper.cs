﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Models
{
    public class ActaHelper
    {
        public static CreateMeetingMinuteViewModel CreateViewModelFor(CargarDatosContext cargarDatosContext, int idReunion) // LLENADO del VIEW MODEL PARA EL CREATE
        {
            AbetEntities ctx = cargarDatosContext.context;
            var acta = ctx.Acta.FirstOrDefault(x => x.IdReunion == idReunion);
            var esEditar = acta != null;

            var vm = new CreateMeetingMinuteViewModel();
            vm.EsEditar = esEditar;
            var reunion = GedServices.GetReunionServices(ctx, idReunion);            

            vm.AvailableTemaPreAgendaEstados =
                GedServices.GetAllEstadosTemaPreAgendaServices(ctx)
                    .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });
            vm.AvailableTareaEstados =
                GedServices.GetAllEstadosTareaServices(ctx)
                    .Select(o => new SelectListItem { Value = o.Item1, Text = o.Item2 });
            vm.AvailableParticipantes = reunion.ParticipanteReunion
                .Select(
                    o =>
                        new SelectListItem
                        {
                            Value = o.IdParticipanteReunion.ToString(),
                            Text = o.EsExterno ? o.NombreCompleto : o.Docente.Apellidos + ", " + o.Docente.Nombres
                        });
            var esExtraorinaria = reunion.EsExtraordinaria;

            //var nombreArea = reunion.PreAgenda.UnidadAcademica.NombreEspanol.ToUpper();
            //var anio = reunion.PreAgenda.PeriodoAcademico.FechaInicioCiclo.ToDateTime().Year;
            vm.RutaFoto = esEditar?acta.RutaFoto:"";
            //vm.AccionesPrevias = GedServices.GetAccionesMejoraPreviasForActaServices(ctx, nombreArea, anio);
            vm.EstadosAgenda = GedServices.GetAllEstadosTemaPreAgendaServices(ctx);
            vm.EstadosTarea = GedServices.GetAllEstadosTareaServices(ctx);
            vm.Reunion = reunion;
            vm.Participantes = reunion.ParticipanteReunion.ToList();
            var usuario = ctx.Usuario.First(x => x.IdDocente == reunion.IdDocente);
            vm.RutaFirma = usuario.Firma;
            vm.Usuario = usuario;            
            vm.Acuerdos = new List<AcuerdoCreateViewModel>();
            vm.Tareas = new List<TareaCreateViewModel>();
            
            vm.Documentos = new List<HttpPostedFileBase>();
            vm.RutasDocumentos = new List<String>();
            vm.Today = string.Format("{0:dd/MM/yyyy}", DateTime.Today);

            vm.TemasReunion = new List<TemaReunionCreateViewModel>();

            if (!esEditar)
            {
                for (int i = 0; i < reunion.TemaReunion.Count; i++)
                {
                    var temaReunion = new TemaReunionCreateViewModel();
                    temaReunion.TemaReunion = reunion.TemaReunion.ToList()[i];
                    temaReunion.Numero = (i+1).ToString();
                    vm.TemasReunion.Add(temaReunion);
                }
            }
            else
            {
                for (int i = 0; i < reunion.TemaReunion.Count; i++)
                {
                    var temaReunion = new TemaReunionCreateViewModel();
                    temaReunion.TemaReunion = reunion.TemaReunion.ToList()[i];
                    temaReunion.Numero = (i + 1).ToString();                    
                    vm.TemasReunion.Add(temaReunion);
                }
            }

            vm.DetalleTemaReunion = new List<DetalleReunionCreateViewModel>();            

            int j = 1;            
            foreach (var temasReunion in vm.TemasReunion)
            {
                foreach(var detalleTema in temasReunion.TemaReunion.DetalleTemaReunion)
                {
                    DetalleReunionCreateViewModel detalleTemaReunionVm = new DetalleReunionCreateViewModel();
                    detalleTemaReunionVm.DetalleTemaReunion = detalleTema;
                    detalleTemaReunionVm.Numero = (j).ToString();
                    detalleTemaReunionVm.NumeroTemaReunion = temasReunion.Numero;
                    vm.DetalleTemaReunion.Add(detalleTemaReunionVm);
                    j++;
                }
            }
            
            if (esEditar)
            {
                vm.Acuerdos = new List<AcuerdoCreateViewModel>();
                foreach (var a in acta.Acuerdo)
                {
                    var acuerdoVm = new AcuerdoCreateViewModel();
                    acuerdoVm.Acuerdo = a;
                    acuerdoVm.Numero = a.IdAcuerdo.ToString();

                    vm.Acuerdos.Add(acuerdoVm);
                }

                vm.Tareas = new List<TareaCreateViewModel>();
                foreach (var t in acta.Tarea)
                {
                    var tareaVm = new TareaCreateViewModel();
                    tareaVm.FechaPropuesta = t.FechaLimite.ToShortDateString();
                    var estadosTarea = GedServices.GetAllEstadosTareaServices(ctx);
                    foreach (var e in estadosTarea)
                    {
                        if (t.Estado == e.Item1)
                        {
                            tareaVm.Estado = e.Item2;
                            break;
                        }
                    }
                    tareaVm.Numero = t.IdTarea.ToString();
                    tareaVm.Tarea = t;
                    tareaVm.ListaResponsables = new List<string>();                    
                    foreach (var r in t.ResponsableTarea)
                    {
                        var participante = r.ParticipanteReunion;
                        string nombre = "";
                        if (participante.EsExterno)
                        {
                            nombre = participante.NombreCompleto;
                        }
                        else
                        {
                            nombre = participante.Docente.Apellidos + ", " + participante.Docente.Nombres;
                        }
                        tareaVm.ListaResponsables.Add(nombre);
                    }

                    vm.Tareas.Add(tareaVm);
                }
            }
            

            if (esEditar)
            {
                foreach (var d in acta.Documento)
                {
                    vm.RutasDocumentos.Add(d.RutaArchivo);
                }
            }

            if(esEditar)
            {
                vm.Observaciones = new List<ObservacionesActaViewModel>();
                
                var observacionesActa = acta.ObservacionesActa;
                foreach (var o in observacionesActa)
                {
                    var observacion = new ObservacionesActaViewModel();
                    observacion.observacionesActa = o;
                    var participante = ctx.ParticipanteReunion.First(x => x.IdParticipanteReunion == o.idParticipante);
                    observacion.NomParticipante = participante.NombreCompleto;
                    if (participante.EsExterno)
                        observacion.NomUnidadAcademica = "Externo";
                    else
                        observacion.NomUnidadAcademica = ctx.Docente.First(x => x.IdDocente == participante.IdDocente)
                            .UnidadAcademicaResponsable.Select(X => X.SedeUnidadAcademica).Select(X => X.UnidadAcademica)
                            .Select(X => X.NombreEspanol).ToString();
                    observacion.CargarDatos(cargarDatosContext);
                    vm.Observaciones.Add(observacion);
                }
            }

            /*
            if (acta.Estado != ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.FINALIZADO.CODIGO)
            {
                vm.Observaciones = new List<ObservacionesActaViewModel>();
                var observacionesActa = acta.ObservacionesActa;
                foreach (var o in observacionesActa)
                {
                    var observacion = new ObservacionesActaViewModel();
                    observacion.observacionesActa = o;
                    var participante = ctx.ParticipanteReunion.First(x => x.IdParticipanteReunion == o.idParticipante);
                    observacion.NomParticipante = participante.NombreCompleto;
                    if (participante.EsExterno)
                        observacion.NomUnidadAcademica = "Externo";
                    else
                        observacion.NomUnidadAcademica = ctx.Docente.First(x => x.IdDocente == participante.IdDocente)
                            .UnidadAcademicaResponsable.Select(X => X.SedeUnidadAcademica).Select(X => X.UnidadAcademica)
                            .Select(X => X.NombreEspanol).ToString();
                    vm.Observaciones.Add(observacion);
                }
            }

            vm.Estado = acta.Estado;

            vm.RutasDocumentos = new List<string>();
            foreach (var d in acta.Documento)
            {
                vm.RutasDocumentos.Add(d.RutaArchivo);
            }*/

            return vm;
        }

        public static ViewMeetingMinuteViewModel CreateViewFor(CargarDatosContext cargarDatos, int idActa)
        {
            var vm = new ViewMeetingMinuteViewModel();
            var acta = cargarDatos.context.Acta.First(x => x.IdActa == idActa);
            var reunion = acta.Reunion;                                   
            vm.IdActa = idActa;
            vm.Reunion = reunion;
            vm.Participantes = reunion.ParticipanteReunion.ToList();
            vm.RutaFoto = acta.RutaFoto ?? String.Empty;
            var usuario = cargarDatos.context.Usuario.First(x => x.IdDocente == reunion.IdDocente);
            vm.RutaFirma = usuario.Firma;
            vm.Usuario = usuario;
            var unidadAcademicaArea = cargarDatos.context.UnidadAcademica.FirstOrDefault(x=>x.IdUnidadAcademica == reunion.IdUnidadAcademica);
            var nombreArea = unidadAcademicaArea.NombreEspanol.ToUpper();
            var periodo = cargarDatos.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == reunion.IdSubModalidadPeriodoAcademico);
            var anio = periodo.FechaInicioPeriodo.ToDateTime().Year;            
            vm.TemasReunion = new List<TemaReunionCreateViewModel>();
            vm.PuedeObservar = false;
            
            foreach(var participante in vm.Participantes)
            {
                if (participante.IdDocente == cargarDatos.session.GetDocenteId())
                    vm.PuedeObservar = true;
            }

            if (usuario.IdUsuario == cargarDatos.session.GetUsuarioId() && vm.PuedeObservar)
                vm.PuedeObservar = false;
            

            foreach(var temaReunion in reunion.TemaReunion)
            {
                var temaReunionVm = new TemaReunionCreateViewModel();
                temaReunionVm.TemaReunion = temaReunion;
            }

            for (int i = 0; i < reunion.TemaReunion.Count; i++)
            {
                var temaReunion = new TemaReunionCreateViewModel();
                temaReunion.TemaReunion = reunion.TemaReunion.ToList()[i];
                temaReunion.Numero = (i + 1).ToString();
                vm.TemasReunion.Add(temaReunion);
            }
            vm.DetallesReunion = new List<DetalleReunionCreateViewModel>();

            var numeroDetalle = 1;
            foreach(var temaReunion in vm.TemasReunion)
            {
                foreach(var detalleReunion in temaReunion.TemaReunion.DetalleTemaReunion)
                {
                    var detalleReunionVm = new DetalleReunionCreateViewModel();
                    detalleReunionVm.DetalleTemaReunion = detalleReunion;
                    detalleReunionVm.Numero = numeroDetalle.ToString();
                    detalleReunionVm.NumeroTemaReunion = temaReunion.Numero;
                    numeroDetalle++;
                    vm.DetallesReunion.Add(detalleReunionVm);
                }
            }
             
                vm.Acuerdos = new List<AcuerdoCreateViewModel>();
                foreach (var a in acta.Acuerdo)
                {
                    var acuerdoVm = new AcuerdoCreateViewModel();
                    acuerdoVm.Acuerdo = a;
                    acuerdoVm.Numero = a.IdAcuerdo.ToString();

                    vm.Acuerdos.Add(acuerdoVm);
                }

                vm.Tareas = new List<TareaCreateViewModel>();
                foreach (var t in acta.Tarea)
                {
                    var tareaVm = new TareaCreateViewModel();
                    tareaVm.FechaPropuesta = t.FechaLimite.ToShortDateString();
                    var estadosTarea = GedServices.GetAllEstadosTareaServices(cargarDatos.context);
                    foreach (var e in estadosTarea)
                    {
                        if (t.Estado == e.Item1)
                        {
                            tareaVm.Estado = e.Item2;
                            break;
                        }
                    }
                    tareaVm.Numero = t.IdTarea.ToString();
                    tareaVm.Tarea = t;
                    tareaVm.ListaResponsables = new List<string>();
                    foreach (var r in t.ResponsableTarea)
                    {
                        var participante = r.ParticipanteReunion;
                        string nombre = "";
                        if (participante.EsExterno)
                        {
                            nombre = participante.NombreCompleto;
                        }
                        else
                        {
                            nombre = participante.Docente.Apellidos + ", " + participante.Docente.Nombres;
                        }
                        tareaVm.ListaResponsables.Add(nombre);
                    }

                    vm.Tareas.Add(tareaVm);
                }
            

            if (acta.Estado != ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.FINALIZADO.CODIGO)
            {
                vm.Observaciones = new List<ObservacionesActaViewModel>();
                var observacionesActa = acta.ObservacionesActa;
                foreach (var obs in observacionesActa)
                {
                    var observacion = new ObservacionesActaViewModel();
                    observacion.observacionesActa = obs;
                    observacion.Numero = obs.idObservacionesActa.ToString();
                    var participante = cargarDatos.context.ParticipanteReunion.First(x => x.IdParticipanteReunion == obs.idParticipante);
                    observacion.NomParticipante = participante.NombreCompleto;
                    if (participante.EsExterno)
                        observacion.NomUnidadAcademica = "Externo";
                    else
                        observacion.NomUnidadAcademica = cargarDatos.context.Docente.First(x => x.IdDocente == participante.IdDocente)
                            .UnidadAcademicaResponsable.Select(X => X.SedeUnidadAcademica).Select(X => X.UnidadAcademica)
                            .Select(X => X.NombreEspanol).ToString();
                    vm.Observaciones.Add(observacion);
                }
            }

            vm.Estado = acta.Estado;

            vm.RutasDocumentos = new List<string>();
            foreach (var d in acta.Documento)
            {
                vm.RutasDocumentos.Add(d.RutaArchivo);
            }

            return vm;
        }

        public static int CreateModelForComentariosActas(AbetEntities ctx, ViewMeetingMinuteViewModel vmVistaActa, HttpServerUtilityBase server)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    foreach (var observacionesVm in vmVistaActa.Observaciones ?? new List<ObservacionesActaViewModel>())
                    {
                        int idObservacionDummy;
                        // Si no se puede hacer parse es porque es una observación nueva ("O<número>")
                        if (!int.TryParse(observacionesVm.Numero.Trim(), out idObservacionDummy))
                        {
                            var observacion = new ObservacionesActa();
                            observacion.idActa = vmVistaActa.IdActa;
                            observacion.idParticipante = observacionesVm.observacionesActa.idParticipante;
                            observacion.SeccionActa = observacionesVm.observacionesActa.SeccionActa;
                            observacion.Comentario = observacionesVm.observacionesActa.Comentario;
                            observacion.Estado = observacionesVm.observacionesActa.Estado;
                            observacion.FechaRegistro = DateTime.Now;
                            var acta = ctx.Acta.Where(x => x.IdActa == vmVistaActa.IdActa).FirstOrDefault();
                            acta.Estado = ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.OBSERVADA.CODIGO;
                            ctx.ObservacionesActa.Add(observacion);
                            ctx.SaveChanges();
                        }
                    }
                    transaction.Commit();
                    return vmVistaActa.IdActa;
                }
                catch (DbEntityValidationException ex)
                {
                    // TODO: log exception
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    transaction.Rollback();
                    return -1;
                }

            }
        }

        public static int CreateModelFor(AbetEntities ctx, CreateMeetingMinuteViewModel vm, HttpServerUtilityBase server)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    // Registrar acta
                    var reunion = ctx.Reunion.FirstOrDefault(r => r.IdReunion == vm.Reunion.IdReunion);
                    var acta = ctx.Acta.FirstOrDefault(r => r.IdReunion == vm.Reunion.IdReunion);                    

                    var esEditar = acta != null;
                    if (acta == null)
                    {
                        acta = new Acta();
                        acta.IdReunion = vm.Reunion.IdReunion;
                        acta.FechaRegistro = DateTime.Now;
                        //acta.IdInstrumento = ctx.Instrumento.First(x => x.Acronimo == "ACC").IdInstrumento;
                        acta.TipoActa = ConstantHelpers.PROFESSOR.ACTAS.TIPOS.ACTA_NORMAL.CODIGO;
                        acta.IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico ?? 4;                        
                        ctx.Acta.Add(acta);
                    }
                    acta.Estado = ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.OK.CODIGO;
                    ctx.SaveChanges();

                    string prePath = Path.Combine(server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY);

                    // Crear el directorio para la gestión de actas
                    Directory.CreateDirectory(prePath);

                    // Crear directorio para la nueva acta
                    string pathActa = Path.Combine(prePath, acta.IdActa.ToString());
                    Directory.CreateDirectory(pathActa);

                    // Crear directorio para los documentos del acta
                    string pathDocumentosActa = Path.Combine(pathActa, "Documentos");
                    Directory.CreateDirectory(pathDocumentosActa);

                    // Guardar foto
                    if (vm.Foto != null)
                    {
                        string filenameFoto = vm.Foto.FileName;
                        string fullPathFoto = Path.Combine(pathActa, filenameFoto);
                        vm.Foto.SaveAs(fullPathFoto);
                        acta.RutaFoto = Path.Combine(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY, ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, acta.IdActa.ToString()) + "\\" + vm.Foto.FileName;
                    }

                    // Actualizar estado de reunión
                    reunion.FueEjecutada = true;

                    // Actualizar asistencia de participantes
                    foreach (var participanteReunion in reunion.ParticipanteReunion)
                    {
                        participanteReunion.Asistio =
                            vm.Participantes
                                .FirstOrDefault(o => o.IdParticipanteReunion == participanteReunion.IdParticipanteReunion)
                                    .Asistio;
                        ctx.SaveChanges();
                    }

                    
                        if (esEditar) //Eliminar acuerdos antiguos
                            ctx.Acuerdo.RemoveRange(ctx.Acuerdo.Where(x => x.IdActa == acta.IdActa));

                        // Registrar acuerdos
                        foreach (var acuerdoVm in vm.Acuerdos ?? new List<AcuerdoCreateViewModel>())
                        {
                            var acuerdo = new Acuerdo();
                            acuerdo.IdActa = acta.IdActa;
                            acuerdo.DescripcionEspanol = acuerdoVm.Acuerdo.DescripcionEspanol;
                            acuerdo.DescripcionIngles = acuerdo.DescripcionEspanol;
                            acuerdo.FechaCreacion = DateTime.Now;

                            ctx.Acuerdo.Add(acuerdo);
                            ctx.SaveChanges();
                        }

                        if (esEditar) //Eliminar tareas antiguos
                        {
                            var tareasRegistradas = ctx.Tarea.Where(x => x.IdActa == acta.IdActa);
                            foreach (var tareaRegistrada in tareasRegistradas)
                            {
                                ctx.ResponsableTarea.RemoveRange(tareaRegistrada.ResponsableTarea);
                                ctx.Tarea.Remove(tareaRegistrada);
                            }
                        }

                        // Registrar tareas y responsables-tareas
                        foreach (var tareaVm in vm.Tareas ?? new List<TareaCreateViewModel>())
                        {
                            var tarea = new Tarea();
                            tarea.IdActa = acta.IdActa;
                            tarea.FechaInicio = DateTime.Now;
                            try
                            {
                                tarea.FechaLimite = DateTime.Parse(tareaVm.FechaPropuesta);
                            }
                            catch (Exception)
                            {
                                tarea.FechaLimite = DateTime.Today;
                            }
                            tarea.Estado = tareaVm.Tarea.Estado;
                            tarea.DescripcionEspanol = tareaVm.Tarea.DescripcionEspanol;
                            tarea.DescripcionIngles = tarea.DescripcionEspanol;

                            ctx.Tarea.Add(tarea);
                            ctx.SaveChanges();
                            foreach (var idParticipante in tareaVm.Responsables ?? new List<int>())
                            {
                                var responsableTarea = new ResponsableTarea();
                                responsableTarea.IdParticipanteReunion = idParticipante;
                                responsableTarea.IdTarea = tarea.IdTarea;

                                ctx.ResponsableTarea.Add(responsableTarea);
                            }
                        }
                    
                    //PARA LOS COMENTARIOS
                    if(esEditar)
                    {
                        Boolean Observado = false;
                        foreach (var observacionesVm in vm.Observaciones ?? new List<ObservacionesActaViewModel>())
                        {                            
                            var observacion = ctx.ObservacionesActa.FirstOrDefault(x=>x.idObservacionesActa == observacionesVm.observacionesActa.idObservacionesActa);
                            observacion.Estado = observacionesVm.observacionesActa.Estado;
                            ctx.SaveChanges();                          
                            if(!Observado)
                            {
                                Observado = observacionesVm.observacionesActa.Estado == ConstantHelpers.PROFESSOR.ACTAS.OBSERVACIONES.ESTADOS.PENDIENTE.CODIGO;
                            }
                        }
                        if (Observado)
                            acta.Estado = ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.OBSERVADA.CODIGO;
                        else
                            acta.Estado = ConstantHelpers.PROFESSOR.ACTAS.ESTADOS.OK.CODIGO;
                        
                    }

                    
                    /* LOGICA ANTERIOR: HACE DIFERENCIA PARA AGREGAR ELIMINAR. EL PROBLEMA ES QUE EL ID NO VIAJA BIEN 
                    // Verificar la eliminación de temas de preagenda
                    var temasPreagendaExistentes = reunion.PreAgenda.TemaPreAgenda;
                    var temasAEliminar = new List<int>();
                    foreach (var tema in temasPreagendaExistentes)
                    {
                        bool encontrado = false;
                        foreach (var temaVm in vm.TemasPreAgenda ?? new List<TemaPreAgendaCreateViewModel>())
                        {
                            int idTema;
                            if (int.TryParse(temaVm.Numero.Trim(), out idTema))
                            {
                                if (idTema == tema.IdTemaAgenda)
                                {
                                    encontrado = true;
                                    tema.Estado = temaVm.TemaPreAgenda.Estado;
                                    tema.Nombre = temaVm.TemaPreAgenda.Nombre;
                                }
                            }
                        }
                        if (!encontrado)
                        {
                            temasAEliminar.Add(tema.IdTemaAgenda);
                        }
                        else
                        {
                            // Si el tema aún sigue existiendo, ver si hay detalles asociados y registrarlos
                            foreach (var detalleVm in vm.DetallesReunion ?? new List<DetalleReunionCreateViewModel>())
                            {
                                if (char.IsNumber(detalleVm.NumeroTemaPreAgenda.Trim()[0]))
                                {
                                    int idTema;
                                    int.TryParse(detalleVm.NumeroTemaPreAgenda.Trim(), out idTema);
                                    if (idTema == tema.IdTemaAgenda)
                                    {
                                        var detalle = new DetalleReunion();
                                        detalle.IdReunion = reunion.IdReunion;
                                        detalle.Detalles = detalleVm.DetalleReunion.Detalles;
                                        detalle.FechaRegistro = DateTime.Now;
                                        detalle.IdTemaAgenda = tema.IdTemaAgenda;
                                        ctx.DetalleReunion.Add(detalle);
                                    }
                                }
                            }
                        }
                    }

                    // Eliminar temas no encontrados
                    foreach (var idTema in temasAEliminar)
                    {
                        // Eliminar detalles asociados
                        var detallesAEliminar = ctx.DetalleReunion.Where(d => d.IdTemaAgenda == idTema).ToList();
                        ctx.DetalleReunion.RemoveRange(detallesAEliminar);
                        ctx.TemaPreAgenda.Remove(ctx.TemaPreAgenda.FirstOrDefault(t => t.IdTemaAgenda == idTema));
                    }

                    // Registrar nuevos temas con sus respectivos detalles
                    foreach (var temaVm in vm.TemasPreAgenda ?? new List<TemaPreAgendaCreateViewModel>())
                    {
                        int idTemaDummy;
                        // Si no se puede hacer parse es porque es tema nuevo ("G<número>")
                        if (!int.TryParse(temaVm.Numero.Trim(), out idTemaDummy))
                        {
                            var tema = new TemaPreAgenda();
                            tema.IdPreAgenda = reunion.IdPreAgenda;
                            tema.Estado = temaVm.TemaPreAgenda.Estado;
                            tema.Nombre = temaVm.TemaPreAgenda.Nombre;
                            ctx.TemaPreAgenda.Add(tema);
                            ctx.SaveChanges();
                            foreach (var detalleVm in vm.DetallesReunion ?? new List<DetalleReunionCreateViewModel>())
                            {
                                if (detalleVm.NumeroTemaPreAgenda == temaVm.Numero)
                                {
                                    var detalle = new DetalleReunion();
                                    detalle.IdReunion = reunion.IdReunion;
                                    detalle.Detalles = detalleVm.DetalleReunion.Detalles;
                                    detalle.FechaRegistro = DateTime.Now;
                                    detalle.IdTemaAgenda = tema.IdTemaAgenda;
                                    ctx.DetalleReunion.Add(detalle);
                                }
                            }
                        }
                    }*/

                    foreach (var temaReunion in ctx.TemaReunion.Where(x => x.IdReunion == reunion.IdReunion))
                    {
                        ctx.DetalleTemaReunion.RemoveRange(temaReunion.DetalleTemaReunion);
                        ctx.TemaReunion.Remove(temaReunion);                        
                    }

                    foreach (var temaVm in vm.TemasReunion ?? new List<TemaReunionCreateViewModel>())
                    {
                        var tema = new TemaReunion();
                        tema.IdReunion = reunion.IdReunion;                        
                        tema.DetalleTemaReunion = temaVm.TemaReunion.DetalleTemaReunion;
                        tema.FechaRegistro = DateTime.Now;
                        tema.DescripcionTemaReunion = temaVm.TemaReunion.DescripcionTemaReunion;
                        ctx.TemaReunion.Add(tema);
                        ctx.SaveChanges();
                        foreach (var detalleVm in vm.DetalleTemaReunion ?? new List<DetalleReunionCreateViewModel>())
                        {
                            if (detalleVm.NumeroTemaReunion.Trim() == temaVm.Numero.Trim())
                            {
                                var detalle = new DetalleTemaReunion();                                
                                detalle.DetalleTema = detalleVm.DetalleTemaReunion.DetalleTema;
                                detalle.FechaRegistro = DateTime.Now;
                                detalle.IdTemaReunion = tema.IdTemaReunion;
                                ctx.DetalleTemaReunion.Add(detalle);
                            }
                        }
                    }

                    // Registrar documentos
                    foreach (var doc in vm.Documentos ?? new List<HttpPostedFileBase>())
                    {
                        if (doc == null || doc.ContentLength == 0) continue;
                        var documento = new Documento();
                        documento.IdActa = acta.IdActa;
                        documento.Estado = null;
                        documento.FechaSubida = DateTime.Now;
                        string filenameArchivo = doc.FileName;
                        string fullPathArchivo = Path.Combine(pathDocumentosActa, filenameArchivo);
                        doc.SaveAs(fullPathArchivo);
                        documento.RutaArchivo = Path.Combine(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY, ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, acta.IdActa.ToString(), "Documentos") + "/" + doc.FileName;
                        ctx.Documento.Add(documento);
                    }

                    ctx.SaveChanges();

                    transaction.Commit();

                    return acta.IdActa;
                }
                catch (DbEntityValidationException ex)
                {
                    // TODO: log exception
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    transaction.Rollback();
                    return -1;
                }
            }
        }
    }
}