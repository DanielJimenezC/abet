﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.Meeting {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class _AddEditEmployerResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public _AddEditEmployerResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.Meeting._AddEditEmployerRe" +
                            "source", typeof(_AddEditEmployerResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Agree.
        /// </summary>
        public static string Aceptar {
            get {
                return ResourceManager.GetString("Aceptar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Last name.
        /// </summary>
        public static string Apellidos {
            get {
                return ResourceManager.GetString("Apellidos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a DNI.
        /// </summary>
        public static string DNI {
            get {
                return ResourceManager.GetString("DNI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Number of employees.
        /// </summary>
        public static string NdeEmpleados {
            get {
                return ResourceManager.GetString("NdeEmpleados", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Names.
        /// </summary>
        public static string Nombres {
            get {
                return ResourceManager.GetString("Nombres", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a RUC.
        /// </summary>
        public static string RUC {
            get {
                return ResourceManager.GetString("RUC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select the number of employees.
        /// </summary>
        public static string SeleccioneNumeroDeEmpleados {
            get {
                return ResourceManager.GetString("SeleccioneNumeroDeEmpleados", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Phone.
        /// </summary>
        public static string Telefono {
            get {
                return ResourceManager.GetString("Telefono", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Employer Registration.
        /// </summary>
        public static string Titulo {
            get {
                return ResourceManager.GetString("Titulo", resourceCulture);
            }
        }
    }
}
