﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AgendaResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AgendaResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule.AgendaResource" +
                            "", typeof(AgendaResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Accept.
        /// </summary>
        public static string Aceptar {
            get {
                return ResourceManager.GetString("Aceptar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Update.
        /// </summary>
        public static string Actualizar {
            get {
                return ResourceManager.GetString("Actualizar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Agenda.
        /// </summary>
        public static string Agenda {
            get {
                return ResourceManager.GetString("Agenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Add.
        /// </summary>
        public static string Agregar {
            get {
                return ResourceManager.GetString("Agregar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Add Meeting Detail.
        /// </summary>
        public static string AgregarDetalleDeReunion {
            get {
                return ResourceManager.GetString("AgregarDetalleDeReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a There was a problem saving the topic. Please, try later..
        /// </summary>
        public static string Alerta {
            get {
                return ResourceManager.GetString("Alerta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assigned Area.
        /// </summary>
        public static string AreaEncargada {
            get {
                return ResourceManager.GetString("AreaEncargada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Meeting configuration.
        /// </summary>
        public static string ConfiguracionDeLaReunion {
            get {
                return ResourceManager.GetString("ConfiguracionDeLaReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Do you want to delete this PreAgenda?.
        /// </summary>
        public static string DeseaEliminarEstaPreAgenda {
            get {
                return ResourceManager.GetString("DeseaEliminarEstaPreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Meeting details.
        /// </summary>
        public static string DetallesDeReunion {
            get {
                return ResourceManager.GetString("DetallesDeReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Remove.
        /// </summary>
        public static string Eliminar {
            get {
                return ResourceManager.GetString("Eliminar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a End of Meeting.
        /// </summary>
        public static string FinDeReunion {
            get {
                return ResourceManager.GetString("FinDeReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Enter the Agenda.
        /// </summary>
        public static string IngresaLaAgenda {
            get {
                return ResourceManager.GetString("IngresaLaAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Enter the detail.
        /// </summary>
        public static string IngresarElDetalle {
            get {
                return ResourceManager.GetString("IngresarElDetalle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Enter the detail here.
        /// </summary>
        public static string IngreseElDetalleAqui {
            get {
                return ResourceManager.GetString("IngreseElDetalleAqui", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Start of Meeting.
        /// </summary>
        public static string InicioDeReunion {
            get {
                return ResourceManager.GetString("InicioDeReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Place.
        /// </summary>
        public static string Lugar {
            get {
                return ResourceManager.GetString("Lugar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Place of the meeting.
        /// </summary>
        public static string LugarDeLaReunion {
            get {
                return ResourceManager.GetString("LugarDeLaReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Level.
        /// </summary>
        public static string Nivel {
            get {
                return ResourceManager.GetString("Nivel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a There are no topics in this PreAgenda.
        /// </summary>
        public static string NoHayTemasEnEstaPreAgenda {
            get {
                return ResourceManager.GetString("NoHayTemasEnEstaPreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name.
        /// </summary>
        public static string Nombre {
            get {
                return ResourceManager.GetString("Nombre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name of the meeting.
        /// </summary>
        public static string NombreDeLaReunion {
            get {
                return ResourceManager.GetString("NombreDeLaReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Participants.
        /// </summary>
        public static string Participantes {
            get {
                return ResourceManager.GetString("Participantes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Participants of the meeting.
        /// </summary>
        public static string ParticipantesDeLaReunion {
            get {
                return ResourceManager.GetString("ParticipantesDeLaReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Pre-Agenda.
        /// </summary>
        public static string PreAgenda {
            get {
                return ResourceManager.GetString("PreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Return.
        /// </summary>
        public static string Regresar {
            get {
                return ResourceManager.GetString("Regresar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Teacher Meetings.
        /// </summary>
        public static string ReunionesDeDocente {
            get {
                return ResourceManager.GetString("ReunionesDeDocente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select the associated agenda topic.
        /// </summary>
        public static string SeleccionarElTemaDeAgendaAsociado {
            get {
                return ResourceManager.GetString("SeleccionarElTemaDeAgendaAsociado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Week.
        /// </summary>
        public static string Semana {
            get {
                return ResourceManager.GetString("Semana", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Theme.
        /// </summary>
        public static string Tema {
            get {
                return ResourceManager.GetString("Tema", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Themes.
        /// </summary>
        public static string Temas {
            get {
                return ResourceManager.GetString("Temas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Agenda.
        /// </summary>
        public static string Titulo {
            get {
                return ResourceManager.GetString("Titulo", resourceCulture);
            }
        }
    }
}
