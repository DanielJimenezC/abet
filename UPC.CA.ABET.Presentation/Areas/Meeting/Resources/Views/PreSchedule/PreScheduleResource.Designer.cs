﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PreScheduleResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PreScheduleResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule.PreScheduleRes" +
                            "ource", typeof(PreScheduleResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Schedule.
        /// </summary>
        public static string Agenda {
            get {
                return ResourceManager.GetString("Agenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consult Pre-Schedule.
        /// </summary>
        public static string ConsultarPreAgenda {
            get {
                return ResourceManager.GetString("ConsultarPreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Meeting.
        /// </summary>
        public static string CrearReunion {
            get {
                return ResourceManager.GetString("CrearReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data of PreSchedule.
        /// </summary>
        public static string DatosPreAgenda {
            get {
                return ResourceManager.GetString("DatosPreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Meeting PreSchedule.
        /// </summary>
        public static string EditarReunion {
            get {
                return ResourceManager.GetString("EditarReunion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The date does not belong to any academic year..
        /// </summary>
        public static string FechaNoPerteneceCiclo {
            get {
                return ResourceManager.GetString("FechaNoPerteneceCiclo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter data to PreSchedule.
        /// </summary>
        public static string IngresePreAgenda {
            get {
                return ResourceManager.GetString("IngresePreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reason.
        /// </summary>
        public static string Motivo {
            get {
                return ResourceManager.GetString("Motivo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The PreSchedule was created successfully..
        /// </summary>
        public static string PreAgendaCreada {
            get {
                return ResourceManager.GetString("PreAgendaCreada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The PreSchedule was edited successfully..
        /// </summary>
        public static string PreAgendaEditada {
            get {
                return ResourceManager.GetString("PreAgendaEditada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The PreSchedule was deleted successfully..
        /// </summary>
        public static string PreAgendaEliminada {
            get {
                return ResourceManager.GetString("PreAgendaEliminada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not delete the PreSchedule. Check if a meeting is associated to the PreSchedule..
        /// </summary>
        public static string PreAgendaEliminadaMensajeError {
            get {
                return ResourceManager.GetString("PreAgendaEliminadaMensajeError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No results found for the search..
        /// </summary>
        public static string SinResultados {
            get {
                return ResourceManager.GetString("SinResultados", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register PreSchedule Meeting.
        /// </summary>
        public static string TituloCrearPreAgenda {
            get {
                return ResourceManager.GetString("TituloCrearPreAgenda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Display Meeting PreSchedule.
        /// </summary>
        public static string VisualizarReunion {
            get {
                return ResourceManager.GetString("VisualizarReunion", resourceCulture);
            }
        }
    }
}
