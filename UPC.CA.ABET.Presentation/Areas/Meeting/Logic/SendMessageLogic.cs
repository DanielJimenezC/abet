﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Text;
using System.Web;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.Logic;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Logic
{
    public class SendMessageLogic
    {
        public String FromAddress="";
        public String toAddress = "";
        public List<String> lstCC = new List<String>();
        public String Section = "";
        public String Plantilla = "";
        public String Topic = "";
        public String Messagesend = "";
        public String nombreEvento = "";
        public String descripcionEvento = "";
        public String ubicacion = "";
        public DateTime fechaInicio;
        public DateTime fechaFin;
        public int? eventoId = null;
        public bool esCancelacion = false;

         public Tuple<List<String>,bool> validateFields()
        {

            
            List<String> messages = new List<string>();
            
            if (FromAddress=="")
            {
                messages.Add("No FromAddress were found.");
                return new Tuple<List<string>, bool>(messages, false);
            }
            if (toAddress == "")
            {
                messages.Add("No toAddress were found.");
                return new Tuple<List<string>, bool>(messages, false);
            }
            if (nombreEvento == "")
            {
                messages.Add("No nombreEvento were found.");
                return new Tuple<List<string>, bool>(messages, false);
            }
            if (ubicacion == "")
            {
                messages.Add("No ubicacion were found.");
                return new Tuple<List<string>, bool>(messages, false);

            }
            if (fechaInicio == null)
            {
                messages.Add("No fechaInicio were found.");
                return new Tuple<List<string>, bool>(messages, false);
            }
            if (fechaFin == null)
            {
                messages.Add("No fechaFina were found.");
                return new Tuple<List<string>, bool>(messages, false);
            }
            return new Tuple<List<string>, bool>(messages,true);
        }
        public Tuple<List<String>, bool> SendEmail()
        {
            Tuple<List<String>, bool> validatedata = validateFields();
            try
            {
                
                if (validatedata.Item2!=true)
                {
                    return validatedata;
                }

                using (var SecurePassword = new SecureString())
                {

                    //Array.ForEach(ConfigurationManager.AppSettings["ClaveCorreoUPC"].ToArray(), SecurePassword.AppendChar);

                    TemplateRender templateRender = new TemplateRender();
                    

                    using (var smtpClient = new SmtpClient
                    {
                        Host = ConfigurationManager.AppSettings["HostAbet"],
                        Port = (int) (int.Parse( ConfigurationManager.AppSettings["HostAbetPort"])),
                        EnableSsl = false,
                        Timeout = 60000,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["AbetEmailCredentialUser"], ConfigurationManager.AppSettings["AbetEmailCredentialPassword"])
                    })
                    {


                        MailAddress from = new MailAddress(FromAddress);
                        MailAddress to = new MailAddress(toAddress);
                        using (var mailMessage = new MailMessage(from, to))
                        {    
                            for (int i = 0; i < lstCC.Count; i++)
                            {
                                mailMessage.To.Add(new MailAddress(lstCC[i]));
                            }                         

                            if (true)
                            {
                                System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType("text/calendar");
                                contentType.Parameters.Add("method", "REQUEST");
                                contentType.Parameters.Add("name", "meeting.ics");
                                string meetingInfo = EmailHelper.MeetingRequestString(FromAddress, toAddress,nombreEvento,descripcionEvento,ubicacion,fechaInicio,fechaFin,eventoId,esCancelacion);
                                AlternateView avCal = AlternateView.CreateAlternateViewFromString(meetingInfo, contentType);
                                mailMessage.AlternateViews.Add(avCal);
                            }
                            mailMessage.IsBodyHtml = true;
                            mailMessage.Subject = Topic;
                            mailMessage.Sender = from;
                            ServicePointManager.ServerCertificateValidationCallback =
                            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                     System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
                            { return true; };

                            smtpClient.Send(mailMessage);
                        }
                    }
                }

            }
            catch(Exception ex)
            {

                
                return validatedata;
            }

            return validatedata;
        }

        
    }
}