﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.MeetingMinuteCC.Export;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingComitteMinute;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using ActasHelper = UPC.CA.ABET.Helpers.ConstantHelpers.PROFESSOR.ACTAS;


namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite,  AppRol.Acreditador)]
    public class MeetingComitteMinuteController : BaseController
    {
        public ActionResult Index()
        {
            CheckTempData();
            var viewModel = new IndexViewModel();
            viewModel.CargarDatos(CargarDatosContext());
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(int? page, IndexViewModel viewModel)
        {
            return PartialView("_ListMinutes", viewModel.ListarActas(CargarDatosContext(), page, viewModel));
        }

        public ActionResult Detail(int? idActa, int? idReunion, bool? isEditing)
        {
            CheckTempData();
            var viewModel = new MeetingComitteMinuteViewModel();
            viewModel.CargarDatos(CargarDatosContext(), idActa, idReunion, isEditing);

            if (!viewModel.CanEdit && isEditing.HasValue && isEditing.Value)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Detail(MeetingComitteMinuteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var errorEnFoto = false;
                var mensaje = "";
                var tipoMensage = default(MessageType);

              /*  foreach(MeetingComitteMinuteCommentViewModel item in viewModel.Comentarios)
                {
                    System.Diagnostics.Debug.WriteLine("-"+item.Criticidad+"-");
                }

                return RedirectToAction("Detail", new { idActa = viewModel.IdActa, idReunion = viewModel.IdReunion, isEditing = viewModel.IsEditing });

*/

                try
                {
                    var acta = viewModel.IdActa == 0 ? new Acta() : Context.Acta.Find(viewModel.IdActa);
                    if (acta != null)
                    {
                        #region Crear / Editar acta

                        if (acta.IdActa == 0)
                        {
                            acta.FechaRegistro = DateTime.Now;
                        }
                        //acta.FechaRegistro = viewModel
                        acta.DescripcionFoto = viewModel.DescripcionFoto;
                        acta.DetalleReunionComiteConsultivo = viewModel.DetalleReunion;
                        acta.Estado = ActasHelper.ESTADOS.OK.CODIGO;
                        acta.TipoActa = ActasHelper.TIPOS.ACTA_COMITE.CODIGO;
                        acta.Instrumento = Context.Instrumento.Where(i => i.Acronimo.ToUpper() == "ACC").FirstOrDefault();
                        var fechaActa = DateTime.ParseExact(viewModel.FechaReunion, currentCulture == "en-US" ? "MM/dd/yyyy" : "dd/MM/yyyy", null);
                        acta.FechaRegistro = fechaActa;

                        if (viewModel.IdReunion > 0)
                        {
                            #region Reunión

                            var reunion = default(Reunion);
                            if (viewModel.IdReunion > 0)
                            {
                                reunion = Context.Reunion.Find(viewModel.IdReunion);
                                acta.Reunion = reunion;
                            }
                            else
                            {
                                reunion = acta.Reunion;
                            }

                            acta.IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico ?? 0;

                            if (viewModel.FechaModificada)
                            {
                                reunion.EstadoReunion = Context.EstadoReunion.Where(er => er.Estado.ToUpper() == "REPROGRAMADO").FirstOrDefault();
                                reunion.FueEjecutada = true;
                            }
                            var fecha = DateTime.ParseExact(viewModel.FechaReunion, currentCulture == "en-US" ? "MM/dd/yyyy" : "dd/MM/yyyy", null);
                            reunion.Fecha = fecha;
                            reunion.Lugar = viewModel.LugarReunion;

                            var horaInicio = viewModel.HoraInicioReunion.Split(':');
                            if (horaInicio.Length == 2)
                            {
                                reunion.HoraInicio = fecha.Date.Add(new TimeSpan(horaInicio[0].ToInteger(), horaInicio[1].ToInteger(), 0));    
                            }

                            var horaFin = viewModel.HoraFinReunion.Split(':');
                            if (horaFin.Length == 2)
                            {
                                reunion.HoraFin = fecha.Date.Add(new TimeSpan(horaFin[0].ToInteger(), horaFin[1].ToInteger(), 0));                                
                            }


                            reunion.IdSede = viewModel.IdSede;
                            reunion.Motivo = viewModel.Motivo;
                            reunion.Semana = viewModel.Semana;
                            reunion.AgendaComiteConsultivo = viewModel.Agenda;
                            reunion.ObjetivoComiteConsultivo = viewModel.Objetivo;

                            // organizadores
                            AddOrganizators(viewModel, reunion);


                            // externos
                            AddExternalParticipants(viewModel, reunion, acta);

                            #endregion

                            if (viewModel.IdActa == 0)
                            {
                                Context.Acta.Add(acta);
                            }

                            await Context.SaveChangesAsync();

                            mensaje = MessageResource.SeGuardaronLosCambiosSatisfactoriamente;
                            tipoMensage = MessageType.Success;
                        }
                        else
                        {
                            mensaje = MessageResource.NoExisteReunionParaEstaActaDeComiteConsultivo;
                            tipoMensage = MessageType.Warning;
                        }

                        if (viewModel.Foto != null && acta.IdActa > 0)
                        {
                            #region Foto

                            try
                            {
                                string prePath = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY);

                                // Crear el directorio para la gestión de actas
                                Directory.CreateDirectory(prePath);

                                // Crear directorio para la nueva acta
                                string pathActa = Path.Combine(prePath, acta.IdActa.ToString());
                                Directory.CreateDirectory(pathActa);

                                // Crear directorio para los documentos del acta
                                string pathDocumentosActa = Path.Combine(pathActa, "Documentos");
                                Directory.CreateDirectory(pathDocumentosActa);

                                // Guardar foto
                                if (viewModel.Foto != null)
                                {
                                    string filenameFoto = viewModel.Foto.FileName;
                                    string fullPathFoto = Path.Combine(pathActa, filenameFoto);
                                    viewModel.Foto.SaveAs(fullPathFoto);
                                    acta.RutaFoto = Path.Combine(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY, ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, acta.IdActa.ToString()) + "\\" + viewModel.Foto.FileName;

                                    await Context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                errorEnFoto = true;
                                mensaje = MessageResource.NoSePudoGuardarSuFoto;
                                tipoMensage = MessageType.Error;
                            }

                            #endregion
                        }

                        #endregion
                    }
                    else
                    {
                        mensaje = MessageResource.NoExisteActaComiteConsultivoNoSePudieronGuardarSusCambios;
                        tipoMensage = MessageType.Warning;
                    }

                }
                catch (Exception ex)
                {
                    mensaje = ex.Message;
                    tipoMensage = MessageType.Error;

                    var entityException = (System.Data.Entity.Validation.DbEntityValidationException)ex;

                    if (entityException != null)
                    {
                        foreach (var item in entityException.EntityValidationErrors)
                        {
                            object revisa = item;
                        }
                    }

                    //throw;
                }

                TempData["MessageType"] = tipoMensage;
                TempData["FlashMessage"] = mensaje;

                if (tipoMensage == MessageType.Success)
                {
                    return RedirectToAction("Index");   
                }

                TempData["KeepTempData"] = true;
            }

            return RedirectToAction("Detail", new { idActa = viewModel.IdActa, idReunion = viewModel.IdReunion, isEditing = viewModel.IsEditing });
        }

        private void AddOrganizators(MeetingComitteMinuteViewModel viewModel, Reunion reunion)
        {
            var idsActualesOrganizadores = viewModel.Organizadores.Where(o => o.IdDocente > 0).Select(o => o.IdDocente).Distinct().ToList();
            var organizadoresEliminados = reunion.ParticipanteReunion.Where(pr => !pr.EsExterno && (pr.IdDocente.HasValue && !idsActualesOrganizadores.Contains(pr.IdDocente.Value))).ToList();
            for (int i = organizadoresEliminados.Count - 1; i >= 0; i--)
            {
                Context.ParticipanteReunion.Remove(organizadoresEliminados[i]);
            }
            viewModel.Organizadores.Where(o => o.IdPersona == 0).ToList().ForEach(o =>
            {
                reunion.ParticipanteReunion.Add(new ParticipanteReunion
                {
                    NombreCompleto = o.Nombres,
                    IdDocente = o.IdDocente,
                    Asistio = o.Asistio,
                    Cargo = o.Descripcion,
                    IdUnidadAcademicaResponsable = o.IdUnidadAcademicaResponsable
                });
            });

            reunion.ParticipanteReunion.Where(pr => !pr.EsExterno && (pr.IdDocente.HasValue && idsActualesOrganizadores.Contains(pr.IdDocente.Value))).ToList().ForEach(pr =>
            {
                pr.Asistio = viewModel.Organizadores.First(o => o.IdDocente == pr.IdDocente).Asistio;
            });
        }

        private void AddExternalParticipants(MeetingComitteMinuteViewModel viewModel, Reunion reunion, Acta acta)
        {
            var idsActualesParticipantes = viewModel.Participantes.Where(o => o.IdPersona > 0).Select(o => o.IdPersona).Distinct().ToList();
            var participantesEliminados = reunion.ParticipanteReunion.Where(pr => pr.EsExterno && !idsActualesParticipantes.Contains(pr.IdParticipanteReunion)).ToList();
            for (int i = participantesEliminados.Count - 1; i >= 0; i--)
            {
                var item = participantesEliminados[i];
                for (int j = item.ComentarioParticipanteReunion.Count - 1; j >= 0; j--)
                {
                    Context.ComentarioParticipanteReunion.Remove(item.ComentarioParticipanteReunion.ElementAt(j));
                }

                Context.ParticipanteReunion.Remove(participantesEliminados[i]);
            }

            viewModel.Participantes.Where(o => o.IdPersona == 0).ToList().ForEach(o =>
            {
                var participanteReunion = new ParticipanteReunion
                {
                    NombreCompleto = o.Nombres,
                    IdDocente = viewModel.IdDocente,
                    Cargo = o.Cargo,
                    Correo = o.Correo,
                    EmpresaId = o.IdEmpresa,
                    Asistio = o.Asistio,
                    EsExterno = true
                };
                var comentario = viewModel.Comentarios.Where(c => c.TempIdParticipante == o.TempId).FirstOrDefault();
                if (comentario != null)
                {
                    participanteReunion.ComentarioParticipanteReunion.Add(new ComentarioParticipanteReunion
                    {
                        Texto = comentario.Descripcion
                    });
                }
                reunion.ParticipanteReunion.Add(participanteReunion);
            });

            reunion.ParticipanteReunion.Where(pr => pr.EsExterno && idsActualesParticipantes.Contains(pr.IdParticipanteReunion)).ToList().ForEach(pr =>
            {
                var participante = viewModel.Participantes.First(o => o.IdPersona == pr.IdParticipanteReunion);
                pr.Asistio = participante.Asistio;
                pr.Cargo = participante.Cargo;
                pr.Correo = participante.Correo;
                pr.EmpresaId = participante.IdEmpresa;


                var comentariosParticipante = viewModel.Comentarios.Where(c => c.TempIdParticipante == participante.TempId);

                foreach (var item in comentariosParticipante.Where(c => c.IdComentario > 0))
                {
                    var comentario = pr.ComentarioParticipanteReunion.FirstOrDefault(cpr => cpr.IdComentario == item.IdComentario);
                    if (comentario != null)
                    {
                        comentario.Texto = item.Descripcion;
                    }
                }
            });


            var instrumento = Context.Instrumento.Where(i => i.Acronimo == "ACC").FirstOrDefault() ?? new Instrumento();
            var idConstituyente = (Context.ConstituyenteInstrumento.FirstOrDefault(ci => ci.IdInstrumento == instrumento.IdInstrumento) ?? new ConstituyenteInstrumento()).IdConstituyente;
            var idCarrera = 0;
            var codigoCarrera = "";
            var correlativo = GetCorrelativeNumber(viewModel, instrumento.IdInstrumento, ref idCarrera, ref codigoCarrera);
            var ciclo = Context.PeriodoAcademico.Find(viewModel.IdSubModalidadPeriodoAcademico).CicloAcademico;

            foreach (var item in viewModel.Comentarios.Where(c => c.IdComentario == 0))
            {
               int? auxIDcriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == item.Criticidad).IdCriticidad;
                if (!auxIDcriticidad.HasValue)
                    auxIDcriticidad = 3;
               // int  auxIDcriticidad = 3;
                Context.Hallazgo.Add(new Hallazgo
                {
                    IdCarrera = idCarrera,
                    IdConstituyente = idConstituyente,
                    IdInstrumento = instrumento.IdInstrumento,
                    IdSubModalidadPeriodoAcademico = viewModel.IdSubModalidadPeriodoAcademico,
                    IdNivelAceptacionHallazgo = 1,
                    IdCriticidad = auxIDcriticidad.Value,
                    DescripcionEspanol = item.Descripcion,
                    DescripcionIngles = item.Descripcion,
                    FechaRegistro = DateTime.Now,
                    Codigo = string.Format("ACC-{0}-{1}-F-{2}", codigoCarrera, ciclo, correlativo)
                });
                correlativo++;
            }
        }

        private int GetCorrelativeNumber(MeetingComitteMinuteViewModel viewModel, int idInstumento, ref int idCarrera, ref string codigoCarrera)
        {
            var unidadAcademica = Context.UnidadAcademica.Find(viewModel.IdUnidadAcademica);
            idCarrera = unidadAcademica != null ? unidadAcademica.CarreraPeriodoAcademico.IdCarrera : 0;
            codigoCarrera = unidadAcademica != null ? unidadAcademica.CarreraPeriodoAcademico.Carrera.Codigo : string.Empty;
            var _idCarrera = idCarrera;
            return Context.Hallazgo
                .Where(h => h.IdInstrumento == idInstumento && h.IdCarrera == _idCarrera)
                .Select(h => h.Codigo)
                .Distinct()
                .Count() + 1;
        }

        public ActionResult AddOrganizator(int idSede, int idPeriodoAcademico)
        {
            var viewModel = new MeetingComitteMinuteViewModel();
            return PartialView("_AddOrganizator", viewModel.ListarOrganizadores(CargarDatosContext(), idSede, idPeriodoAcademico));
        }

        [HttpPost]
        public ActionResult AddOrganizator(List<MeetingComitteMinuteOrganizatorViewModel> viewModel)
        {
            var newOrganizators = new List<MeetingComitteMinuteOrganizatorViewModel>();

            if (ModelState.IsValid)
            {
                newOrganizators.AddRange(viewModel.Where(vm => vm.Seleccionar).Select(vm => vm));
            }

            return PartialView("_ListOrganizators", new MeetingComitteMinuteViewModel 
            { 
                IsEditing = true,
                Organizadores = newOrganizators 
            });
        }

        public ActionResult RemoveOrganizator(int id)
        {
            return PartialView("_RemoveOrganizator", new MeetingComitteMinuteOrganizatorViewModel { IdPersona = id });
        }

        [HttpPost]
        public ActionResult RemoveOrganizator(MeetingComitteMinuteOrganizatorViewModel viewModel)
        {
            if (viewModel.IdPersona > 0)
            {
                ViewBag.Quitar = true;
            }

            return PartialView("_RemoveOrganizator", viewModel);
        }

        public ActionResult AddParticipant(int idReunion, int? idParticipanteReunion, string tempId)
        {
            var viewModel = new MeetingComitteMinuteParticipantViewModel();
            viewModel.CargarParticipanteReunion(Context, idReunion, idParticipanteReunion, tempId);
            viewModel.Empresas = new SelectList(Context.Empresa.ToList(), "IdEmpresa", "RazonSocial");

            return PartialView("_AddParticipant", viewModel);
        }

        [HttpPost]
        public ActionResult AddParticipant(MeetingComitteMinuteParticipantViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var empresa = Context.Empresa.Find(viewModel.IdEmpresa);

                if (empresa != null)
                {
                    viewModel.RazonSocial = empresa.RazonSocial;
                }
            }

            return PartialView("_ListParticipants", new MeetingComitteMinuteViewModel 
            {
                IsEditing = true,
                IdReunion = viewModel.IdReunion,
                Participantes = new List<MeetingComitteMinuteParticipantViewModel> { viewModel }
            });
        }

        public ActionResult RemoveParticipant(string id)
        {
            return PartialView("_RemoveParticipant", new MeetingComitteMinuteParticipantViewModel { TempId = id });
        }

        [HttpPost]
        public ActionResult RemoveParticipant(MeetingComitteMinuteParticipantViewModel viewModel)
        {
            if (!string.IsNullOrWhiteSpace(viewModel.TempId))
            {
                ViewBag.Quitar = true;
            }

            return PartialView("_RemoveParticipant", viewModel);
        }


        public ActionResult AddComment(int idReunion, int? idComentario, string tempId)
        {
            var viewModel = new MeetingComitteMinuteCommentViewModel();
            viewModel.CargarDatos(Context, idReunion, idComentario, tempId);
            return PartialView("_AddComment", viewModel);
        }

        [HttpPost]
        public ActionResult AddComment(MeetingComitteMinuteCommentViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var participanteReunion = Context.ParticipanteReunion.Find(viewModel.IdParticipante);
                if (participanteReunion != null)
                {
                    viewModel.Participante = participanteReunion.NombreCompleto; 
                }
            }

            return PartialView("_ListComments", new MeetingComitteMinuteViewModel
            {
                IsEditing = true,
                IdReunion = viewModel.IdReunion,
                Comentarios = new List<MeetingComitteMinuteCommentViewModel> { viewModel }
            });
        }

        public ActionResult RemoveComment(string id)
        {
            return PartialView("_RemoveComment", new MeetingComitteMinuteCommentViewModel { TempId = id });
        }

        [HttpPost]
        public ActionResult RemoveComment(MeetingComitteMinuteCommentViewModel viewModel)
        {
            if (!string.IsNullOrWhiteSpace(viewModel.TempId))
            {
                ViewBag.Quitar = true;
            }

            return PartialView("_RemoveComment", viewModel);
        }

        private void CheckTempData()
        {
            var keep = TempData.ContainsKey("KeepTempData");

            if (keep)
            {
                if (TempData.ContainsKey("FlashMessage"))
                {
                    TempData.Keep("FlashMessage");
                }
                if (TempData.ContainsKey("MessageType"))
                {
                    TempData.Keep("MessageType");
                }   
            }
        }

        public ActionResult GetActaPdf(int id)
        {
            try
            {
                string pathRutaFotoReunion = Server.MapPath(Context.Acta.First(x => x.IdActa == id).RutaFoto);
                ExportMeetingMinuteCC pdfExport = new ExportMeetingMinuteCC(Context, id, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, pathRutaFotoReunion);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                string filename = Path.GetFileName(path);
                //string filename = path.Split(new char[] { '/' }).Last();
                return File(path, "application/pdf", filename);
            }
            catch (Exception ex)
            {
                PostMessage(Helpers.MessageType.Error, MessageResource.OcurrioUnErrorExportarActaReunion +"   " + ex.ToString());
                return RedirectToAction("Index");
            }
        }

        
        public ActionResult GetActaRtf(int id)
        {
            try
            {
                string pathRutaFotoReunion = Server.MapPath(Context.Acta.First(x => x.IdActa == id).RutaFoto);
                ExportMeetingMinuteCC rtfExport = new ExportMeetingMinuteCC(Context, id, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, pathRutaFotoReunion);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, "RTF");
                Directory.CreateDirectory(directory);
                string path = rtfExport.GetFile(ExportFileFormat.Rtf, directory);
                string filename = Path.GetFileName(path);
                //string filename = path.Split(new char[] { '/' }).Last();
                return File(path, "application/pdf", filename);
                
            }
            catch (Exception ex)
            {
                PostMessage(Helpers.MessageType.Error, MessageResource.OcurrioUnErrorExportarActaReunion + "   " + ex.ToString());
                return RedirectToAction("Consult");
            }
        }
        
        const string LogoUpc = "logo-upc-red.png";
    }
}