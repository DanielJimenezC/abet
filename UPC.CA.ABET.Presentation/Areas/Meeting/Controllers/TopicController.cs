﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Topic;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.Docente)]
    public class TopicController : BaseController
    {

        public ActionResult GetUnidadAcademicaByPeriodo(Int32? idSubModalidadPeriodoAcademico)
        {
            var model = new AddEditTemaSemanaViewModel();
            if (!idSubModalidadPeriodoAcademico.HasValue)
                //SubModalidadPeriodoAcademicoId = Session.GetPeriodoAcademicoId();
                idSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();

            var docenteid = Session.GetDocenteId();
            var idescuela = Session.GetEscuelaId();

            model.LstUnidadAcademica = (from uar in Context.UnidadAcademicaResponsable
                                        join sua in Context.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                                        join ua in Context.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                                        where uar.IdDocente == docenteid && ua.IdEscuela == idescuela && ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                                        select ua).Distinct().ToList();

            return PartialView(model);
        }
        //public ActionResult AddEditTemaSemana(Int32? IdPeriodoAcademico, Int32? IdUnidadAcademica)
        public ActionResult AddEditTemaSemana(Int32? IdSubModalidadPeriodoAcademico, Int32? IdUnidadAcademica)
        {
            var addedittemasemanaViewModel = new AddEditTemaSemanaViewModel();
            //addedittemasemanaViewModel.CargarDatos(CargarDatosContext(), IdPeriodoAcademico, IdUnidadAcademica, Session.GetDocenteId(), escuelaId);
            addedittemasemanaViewModel.CargarDatos(CargarDatosContext(), IdSubModalidadPeriodoAcademico, IdUnidadAcademica, Session.GetDocenteId(), EscuelaId);
            return View(addedittemasemanaViewModel);
        }
        [HttpPost]
        public ActionResult GoTheme(AddEditTemaSemanaViewModel model)
        {
            //return RedirectToAction("AddEditTemaSemana", new { IdUnidadAcademica = model.IdUnidadAcademica});
            //return RedirectToAction("AddEditTemaSemana", new { IdUnidadAcademica = model.IdUnidadAcademica, IdSubModalidadPeriodoAcademico = model.IdPeriodoAcademico });
            return RedirectToAction("AddEditTemaSemana", new { IdUnidadAcademica = model.IdUnidadAcademica, IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico });
        }

        [HttpPost]
        public ActionResult AddEditTemaSemana(AddEditTemaSemanaViewModel model)
        {
            try
            {
                {
                   Tema tema = new Tema();
                    int idtema = model.tema.IdTema;
                    if (idtema!=0)
                    {
                        tema = Context.Tema.First(x => x.IdTema == idtema);
                    }
                    tema.IdUnidadAcademica = model.IdUnidadAcademica;
                    //tema.IdSubModalidadPeriodoAcademicoModulo = model.IdPeriodoAcademico;                    
                    tema.IdSubModalidadPeriodoAcademicoModulo = Convert.ToInt32((from smpam in Context.SubModalidadPeriodoAcademicoModulo
                                                                                 where smpam.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico
                                                                                 select new
                                                                                 {
                                                                                     IdSubModalidadPeriodoAcademicoModulo = smpam.IdSubModalidadPeriodoAcademicoModulo
                                                                                 }).FirstOrDefault());

                    tema.Semana1 = model.tema.Semana1;
                    tema.Semana2 = model.tema.Semana2;
                    tema.Semana3 = model.tema.Semana3;
                    tema.Semana4 = model.tema.Semana4;
                    tema.Semana5 = model.tema.Semana5;
                    tema.Semana6 = model.tema.Semana6;
                    tema.Semana7 = model.tema.Semana7;
                    tema.Semana8 = model.tema.Semana8;
                    tema.Semana9 = model.tema.Semana9;
                    tema.Semana10 = model.tema.Semana10;
                    tema.Semana11 = model.tema.Semana11;
                    tema.Semana12 = model.tema.Semana12;
                    tema.Semana13 = model.tema.Semana13;
                    tema.Semana14 = model.tema.Semana14;
                    tema.Semana15 = model.tema.Semana15;
                    tema.Semana16 = model.tema.Semana16;

                    if (idtema == 0)
                    {
                        Context.Tema.Add(tema);
                    }


                    Context.SaveChanges();


                    PostMessage(MessageType.Success);
                  //  return RedirectToAction("LstConsultarTema");
                }
            }
            catch (Exception ex)
            {
               // InvalidarContext();
                PostMessage(MessageType.Error);
               // model.CargarDatos(CargarDatosContext(), model.IdTema);
               // TryUpdateModel(model);
               // return View(model);
            }

            //return RedirectToAction("AddEditTemaSemana", new { IdPeriodoAcademico = model.IdPeriodoAcademico, IdUnidadAcademica = model.IdUnidadAcademica });
            return RedirectToAction("AddEditTemaSemana", new { IdPeriodoAcademico = model.IdSubModalidadPeriodoAcademico, IdUnidadAcademica = model.IdUnidadAcademica });
        }
    }
}