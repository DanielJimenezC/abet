﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingDelegate;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
// comentario 2 borrar
namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.Docente)]
    public class MeetingDelegateController : BaseController
    { //asdasdasdasdas borrar
        // GET: Meeting/MeetingDelegate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditMeetingDelegate(int? IdReunionDelegado)
        {
            var viewmodel = new AddEditMeetingDelegateViewModel();
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetPeriodoAcademicoId(), Session.GetEscuelaId());
            viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            return View(viewmodel); 
            
        }
        
        [HttpPost]
        public ActionResult AddEditMeetingDelegate(AddEditMeetingDelegateViewModel model)
        {
            try
            {
                ReunionDelegado reunion = new ReunionDelegado();
                if (model.IdReunionDelegado.HasValue)
                { 
                    reunion = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == model.IdReunionDelegado);
                }
                else
                {
                    reunion.IdSubModalidadPeriodoAcademico = model.IdCiclo;
                    reunion.IdEscuela = model.IdEscuela;

                }

                reunion.IdSede = model.IdSede;
                reunion.Fecha = model.Fecha;
                //reunion.IdCurso = model.IdCurso.Value;
                reunion.Estado = "ACT";
                Sede sede = new Sede();
                sede = Context.Sede.FirstOrDefault(x => x.IdSede == reunion.IdSede);

                reunion.Comentario = "RD-" + sede.Codigo + "-" + (Context.ReunionDelegado.Max(x => x.IdReunionDelegado) + 1) + "-" + model.Fecha.Value.ToShortDateString();

                if (model.IdReunionDelegado.HasValue == false || model.IdReunionDelegado.Value == 0)
                    Context.ReunionDelegado.Add(reunion);


                if (model.IdReunionDelegado.HasValue)
                {
                    List<ReunionDelegadoCurso> lstrdc = Context.ReunionDelegadoCurso.Where(x => x.IdReunionDelegado == model.IdReunionDelegado).ToList();
                    Context.ReunionDelegadoCurso.RemoveRange(lstrdc);
                }


                for (int i = 0; i < model.LstIdCursos.Count; i++)
                {
                    ReunionDelegadoCurso rdc = new ReunionDelegadoCurso();
                    rdc.ReunionDelegado = reunion;
                    rdc.IdCurso = model.LstIdCursos[i];

                    Context.ReunionDelegadoCurso.Add(rdc);
                }




                Context.SaveChanges();

                PostMessage(MessageType.Success);

                return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = reunion.IdReunionDelegado });
            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return RedirectToAction("LstReunionDelegados", "MeetingDelegate");

            }


        }


        public ActionResult ConsultarRD(int IdReunionDelegado)
        {
            var model = new ConsultarRDViewModel();

            model.CargarDatos(CargarDatosContext(), IdReunionDelegado);
            return View(model);
        }

        public ActionResult LstReunionDelegados(Int32? NumeroPagina, Int32? IdPeriodoAcademico, Int32? IdCurso, Int32? IdSede)
        {
            var viewModel = new LstReunionDelegadosViewModel();
            if (IdPeriodoAcademico.HasValue == false)
                IdPeriodoAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
            

            viewModel.CargarDatos(CargarDatosContext(), NumeroPagina, IdPeriodoAcademico.Value, IdCurso, IdSede, EscuelaId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LstReunionDelegados(LstReunionDelegadosViewModel model)
        {
            return RedirectToAction("LstReunionDelegados", new { NumeroPagina = model.NumeroPagina, IdPeriodoAcademico = model.IdPeriodoAcademico,IdCurso = model.IdCurso, IdSede = model.IdSede });
            /* var viewModel = new LstReunionDelegadosViewModel();
             if (model.NumeroPagina.HasValue == false) model.NumeroPagina = 1;
             viewModel.CargarDatos(CargarDatosContext(), model.NumeroPagina, model.IdPeriodoAcademico.Value, model.IdCurso, model.IdSede, escuelaId);
             return View(viewModel);*/
        }

        public ActionResult _LstParticipantesReunionDelegado(int IdReunionDelegado)
        {
            var viewModel = new LstParticipantesReunionDelegadoViewModel();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado);
            return View(viewModel);
        }

        
        public ActionResult AgregarEditarImagen(int IdReunionDelegado, int? IdEvidencia)
        {
            var viewModel = new AgregarEditarImagenViewModel();
            //viewModel.IdReunionDelegado = IdReunionDelegado;
            viewModel.CargarDatos(CargarDatosContext(), IdReunionDelegado,IdEvidencia);
            return View(viewModel);
        }

        public ActionResult EliminarImagen(int IdReunionDelegado, int IdEvidencia)
        {

            try
            {
                Evidencia evidencia = new Evidencia();
                evidencia = Context.Evidencia.FirstOrDefault(x => x.IdEvidencia == IdEvidencia);
                Context.Evidencia.Remove(evidencia);

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch(Exception e)
            {
                PostMessage(MessageType.Error, e.InnerException.ToString());
            }

            return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = IdReunionDelegado });
        }


        [HttpPost]
        public ActionResult AgregarEditarImagen(AgregarEditarImagenViewModel viewmodel)
        {
            try
            {
               

                Evidencia evidencia = new Evidencia();

                if(viewmodel.IdEvidencia!=0 && viewmodel.IdEvidencia.HasValue)
                {
                    evidencia = Context.Evidencia.FirstOrDefault(x => x.IdEvidencia == viewmodel.IdEvidencia);
                }

                String timestamp = (DateTime.Now).ToString("yyyyMMddHHmmssffff");
                var uploadDir = "~/uploads/ReunionDelegados";
                var imageUrl = "";

                if(viewmodel.file!= null)
                {
                    if(Path.GetExtension(viewmodel.file.FileName).ToLower() != ".jpg"
                        && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".png"
                        && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".gif"
                        && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".jpeg")
                    {
                        PostMessage(MessageType.Error, MessageResource.SoloSePermitenImagenesArchivosConExtensionJPGpngGIFjpeg);
                        return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado });
                    }
                    /*var oldpath = ath.Combine(Server.MapPath(uploadDir), evidencia.ubicacion);
                    if (System.IO.File.Exists(oldpath) && viewmodel.IdEvidencia != 0)
                    {
                        System.IO.File.Delete(oldpath);
                    }*/

                    var imagePath = Path.Combine(Server.MapPath(uploadDir), timestamp + Path.GetExtension(viewmodel.file.FileName));
                    // imageUrl = Path.Combine("/uploads/ReunionDelegados", timestamp + Path.GetExtension(viewmodel.file.FileName));
                    imageUrl = "/uploads/ReunionDelegados/"+ timestamp + Path.GetExtension(viewmodel.file.FileName);
                    viewmodel.file.SaveAs(imagePath);
                    evidencia.ubicacion = imageUrl;
                }
// Order validate lines
                Int32 MaxValue;

                List<Int32> lstOrden = (from e in Context.Evidencia
                                        where e.IdReunionDelegado == viewmodel.IdReunionDelegado
                                        select e.Orden).ToList();
                if( lstOrden.Count==0)
                {
                    MaxValue = 0;
                    evidencia.Orden = 1;
                }
                else
                    MaxValue = lstOrden.Max();

                for(int i=0; i<lstOrden.Count; i++)
                {
                    if(viewmodel.Orden.Equals(lstOrden[i]))
                    {
                        evidencia.Orden = MaxValue + 1;                // Si se repite el orden, se cambia el valor a la cantidad de evidencias + 1
                        break;                        
                    }
                    else
                    {
                        evidencia.Orden = viewmodel.Orden;
                    }
                }
// End Order valide lines

                evidencia.IdReunionDelegado = viewmodel.IdReunionDelegado;

                if (viewmodel.IdEvidencia == 0 || viewmodel.IdEvidencia.HasValue == false)
                {  
                   Context.Evidencia.Add(evidencia);
                }
                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error, e.InnerException.ToString());
            }
            return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado });
        }

        public ActionResult _AgregarEditarHallazgo(int IdReunionDelegado, int? IdHallazgo )
        {
            var viewModel = new AgregarEditarHallazgoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdReunionDelegado, IdHallazgo);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult _AgregarEditarHallazgo(AgregarEditarHallazgoViewModel model)
        {
           
            try
            {
                Hallazgo hallazgo; 
                //  var encuesta = context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);
                if (model.IdHallazgo.HasValue)
                {
                    hallazgo = new Hallazgo();
                    hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo);

                    hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                    hallazgo.IdCriticidad = model.IdCriticidad.Value;
                    hallazgo.DescripcionEspanol = model.descripcionEspanol;
                    Context.SaveChanges();

                }
                else
                {
                    hallazgo = new Hallazgo();

                    hallazgo.FechaRegistro = DateTime.Now;
                    hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;



                    hallazgo.IdAcreditadora = 1;
                    hallazgo.IdCurso = model.IdCurso;
                    hallazgo.IdReunionDelegado = model.IdReunionDelegado;
                    hallazgo.IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "IRD").IdInstrumento;
                    hallazgo.IdConstituyente = model.IdConstituyente.Value;
                    hallazgo.IdSede = model.IdSede;
                    //hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;
                    hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;

                    hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                    hallazgo.IdCriticidad = model.IdCriticidad.Value;

                    //var cicloacademico = context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;
                    //falta
                    var cicloacademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdSubModalidadPeriodoAcademico).CicloAcademico;

                   
                    for(int i=0; i<model.LstOutcome.Count; i++)
                    {
                        Hallazgo newhallazgo = hallazgo;
                        newhallazgo.DescripcionEspanol = model.LstOutcomeDescripcion[i];
                        newhallazgo.DescripcionIngles = "";

                        if (newhallazgo.DescripcionEspanol == "") continue;

                        int idcomision = model.LstOutcome[i].IdComision;
                        newhallazgo.IdOutcome = model.LstOutcome[i].IdOutcome;
                        newhallazgo.IdComision = idcomision;

                        newhallazgo.Codigo = String.Format("{0}-{1}-{2}-F-{3}", ConstantHelpers.ENCUESTA.ARD, cicloacademico, model.IdCurso, Context.Hallazgo.Max(x => x.IdHallazgo) + 1);

                        newhallazgo.IdCarrera = Context.CarreraComision.FirstOrDefault(x => x.IdComision == idcomision && x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico).IdCarrera;

                        Context.Hallazgo.Add(newhallazgo);
                        Context.SaveChanges();
                    }

                    
                    PostMessage(MessageType.Success);

                    //return RedirectToAction("ConsultaRD", "MeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });
                }
            }
            catch(Exception e)
            {
                PostMessage(MessageType.Error,e.InnerException.ToString());
               // return  RedirectToAction("ConsultaRD", "MeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });
            }
          return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });
        }



        //Esta funcion es invocada por Ajax desde agregareditarhallazgo, muestra textbox descripcion por cada comision 
        //de la carrera seleccionada
        [HttpPost]
        public ActionResult ListarDescripcionPorCarrera(int? idCurso, int idReunionDelegado)
        {
            AgregarEditarHallazgoViewModel model = new AgregarEditarHallazgoViewModel();
            model.CargarDatosPartial(CargarDatosContext(), idCurso, idReunionDelegado);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult _LstParticipantesReunionDelegado(LstParticipantesReunionDelegadoViewModel model)
        {
            var ruta = "";
            try
            {
                int idReunionDelegado = model.IdReunionDelegado;

               // context.ReunionDelegadoAlumnoSeccion.RemoveRange(context.ReunionDelegadoAlumnoSeccion.Where(x => x.IdReunionDelegado == idReunionDelegado));

              for (int i = 0; i < model.LstDelegadosAsistentes.Count; i++)
                {
                    if (model.LstDelegadosAsistentes[i]==true)
                    {
                        int idalumnoseccion = model.LstIdAlumnoSeccion[i]; 
                        ReunionDelegadoAlumnoSeccion rdas = Context.ReunionDelegadoAlumnoSeccion.FirstOrDefault(x => x.IdReunionDelegado == idReunionDelegado && x.IdAlumnoSeccion == idalumnoseccion);

                        if(rdas!=null)
                            continue;
                        else
                        {
                            rdas = new ReunionDelegadoAlumnoSeccion();
                        }
                           
                        rdas.IdReunionDelegado = idReunionDelegado;
                        rdas.IdAlumnoSeccion = model.LstIdAlumnoSeccion[i];

                        Context.ReunionDelegadoAlumnoSeccion.Add(rdas);
                    }
                    else
                    {
                        int ideliminar = model.LstIdAlumnoSeccion[i];
                        ReunionDelegadoAlumnoSeccion rdas = Context.ReunionDelegadoAlumnoSeccion.FirstOrDefault(x => x.IdReunionDelegado == idReunionDelegado && x.IdAlumnoSeccion == ideliminar);

                        if (rdas != null)
                        {
                            Context.ReunionDelegadoAlumnoSeccion.Remove(rdas);
                        }


                    }

                }

                Context.SaveChanges();
                PostMessage(MessageType.Success);

             //   return RedirectToAction("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = idReunionDelegado });

                 ruta = Url.Action("ConsultarRD", "MeetingDelegate", new { IdReunionDelegado = idReunionDelegado });


              

            }
            catch (Exception e)
            {
                 ruta = Url.Action("LstReunionDelegados", "MeetingDelegate");
            }
            return Json(ruta);
         
        }

        public ActionResult LstHallazgosReunionDelegado(int IdReunionDelegado)
        {
            var viewModel = new LstHallazgosReunionDelegadoViewModel();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado);
            return View(viewModel);
        }

        public ActionResult _AddEditHallazgoRD(int IdReunionDelegado, int IdHallazgo)
        {
            var viewModel = new _AddEditHallazgoRDViewModel();
            ReunionDelegado rd = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
            viewModel.CargarDatos(CargarDatosContext(), rd.IdReunionDelegado, rd.IdCurso, rd.IdSubModalidadPeriodoAcademico, rd.IdSede, EscuelaId, IdHallazgo);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult _AddEditHallazgoRD(_AddEditHallazgoRDViewModel model)
        {
            try
            {
                Hallazgo hallazgo;
                if (model.IdHallazgo != 0)
                    hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo);
                else
                    hallazgo = new Hallazgo();
                //var cicloacademico = context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;
                var cicloacademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdSubModalidadPeriodoAcademico).CicloAcademico;
                var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.ARD).IdInstrumento;
                // System.Diagnostics.Debug.WriteLine("IdInstrumento: " + IdInstrumento);

                hallazgo.IdOutcome = model.IdOutcome;
                hallazgo.IdReunionDelegado = model.IdReunionDelegado;
                //hallazgo.IdComision = context.OutcomeComision.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.IdPeriodoAcademico && x.IdOutcome == model.IdOutcome).IdComision;
                hallazgo.IdComision = Context.OutcomeComision.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico && x.IdOutcome == model.IdOutcome).IdComision;
                // System.Diagnostics.Debug.WriteLine("IdComision: " + model.IdComision);

                hallazgo.FechaRegistro = DateTime.Now;
                hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;

               

                hallazgo.IdConstituyente = 1;
                hallazgo.IdInstrumento = IdInstrumento;
                hallazgo.IdCurso = model.IdCurso;
                hallazgo.IdCarrera = model.IdCarrera;
                hallazgo.IdSede = model.IdSede;
                hallazgo.IdAcreditadora = Context.Acreditadora.FirstOrDefault(x => x.Nombre == "ABET").IdAcreditadora;
                hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                hallazgo.IdCriticidad = model.IdCriticidad.Value;

                hallazgo.Codigo = String.Format("{0}-{1}-{2}-F-{3}", ConstantHelpers.ENCUESTA.ARD, cicloacademico, model.IdCurso, Context.Hallazgo.Max(x => x.IdHallazgo) + 1);
                hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                //hallazgo.IdSubModalidadPeriodoAcademico = model.IdPeriodoAcademico.Value;
                hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;

                if (model.IdHallazgo==0)
                    Context.Hallazgo.Add(hallazgo);

                Context.SaveChanges();


                PostMessage(MessageType.Success);

                var ruta = Url.Action("LstHallazgosReunionDelegado", new { IdReunionDelegado = model.IdReunionDelegado });
               // var ruta = Url.Action("ConsultarRD", new { IdReunionDelegado = model.IdReunionDelegado });

                
                return Json(ruta);
            }
            catch (DbEntityValidationException ex)
            {

                foreach (var eve in ex.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {


                System.Diagnostics.Debug.WriteLine(ex);

                throw;
            }
        }

        public ActionResult DeleteHallazgoRD(int IdReunionDelegado, int IdHallazgo)
        {
            var viewModel = new _AddEditHallazgoRDViewModel();
            ReunionDelegado rd = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
            viewModel.CargarDatos(CargarDatosContext(), rd.IdReunionDelegado, rd.IdCurso, rd.IdSubModalidadPeriodoAcademico, rd.IdSede, EscuelaId, IdHallazgo);
            return View(viewModel);
        }


        public ActionResult _ConfirmacionDeleteReunionDelegado(Int32 IdReunionDelegado)
        {
            var viewModel = new _ConfirmacionDeleteReunionDelegadoViewModel();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult _ConfirmacionDeleteReunionDelegado(_ConfirmacionDeleteReunionDelegadoViewModel model)
        {
            try
            {
                var reunion = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == model.IdReunionDelegado);
                //int idReunionDelegado = hallazgo.IdReunionDelegado.Value;
                reunion.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                var ruta = Url.Action("LstHallazgosReunionDelegado", "MeetingDelegate");
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult _ConfirmacionDeleteHallazgoRD(Int32 IdHallazgo)
        {
            var viewModel = new _ConfirmacionDeleteHallazgoRDViewModel();
            viewModel.Fill(CargarDatosContext(), IdHallazgo);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult _ConfirmacionDeleteHallazgoRD(_ConfirmacionDeleteHallazgoRDViewModel model)
        {
            try
            {
                var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);
                int idReunionDelegado = hallazgo.IdReunionDelegado.Value;
                hallazgo.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                var ruta = Url.Action("ConsultarRD", new { IdReunionDelegado = idReunionDelegado });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult PrintReunionDelegado(int IdReunionDelegado)
        {
            var viewModel = new PrintReunionDelegadoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdReunionDelegado);

            Document document = new Document();

            MemoryStream stream = new MemoryStream();



            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;

                document.Open();


                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_RIGHT;

                document.Add(imagen);



                var fontNormal = FontFactory.GetFont("Arial", 16, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("Acta de Reunión de Delegados", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                document.Add(Chunk.NEWLINE);
                var fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);

                var escuela = viewModel.reunion.Escuela.Nombre;
                Paragraph titulo2 = new Paragraph(escuela, fontsub);
                titulo2.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo2);

                document.Add(Chunk.NEWLINE);
                document.Add(Chunk.NEWLINE);


                var fontNormal2 = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);
                document.Add(new Paragraph("Fecha: " + viewModel.reunion.Fecha.Value.ToString("dd/MM/yyyy"), fontNormal2));

                String cursos = "";

                for(int i=0; i<viewModel.LstCursos.Count; i++)
                {
                    if(i==0)
                    {
                        cursos = viewModel.LstCursos[i].NombreEspanol;
                    }
                    else
                    cursos = cursos + ", " + viewModel.LstCursos[i].NombreEspanol;
                }

                document.Add(new Paragraph("Cursos: " + cursos, fontNormal2));

                document.Add(Chunk.NEWLINE);

                fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph tituloHallazgos = new Paragraph("Hallazgos:", fontsub);
                tituloHallazgos.Alignment = Element.ALIGN_LEFT;
                document.Add(tituloHallazgos);

                document.Add(Chunk.NEWLINE);

                PdfPTable table = new PdfPTable(6);
              
                table.WidthPercentage = 100;
                table.SetWidthPercentage(new float[] { 70, 80, 110, 100, 70, 170 },document.PageSize);

                var fontNormal3 = FontFactory.GetFont("Arial", 10,Font.BOLD, BaseColor.WHITE);

                PdfPCell cell = new PdfPCell(new Phrase("Criticidad", fontNormal3));
                cell.BackgroundColor = new BaseColor(237, 28, 36);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.PaddingBottom = 5;
                table.AddCell(cell);

                cell.Phrase = new Phrase("Nivel de Aceptación", fontNormal3);
                table.AddCell(cell);

                cell.Phrase = new Phrase("Código", fontNormal3);
                table.AddCell(cell);

                cell.Phrase =  new Phrase("Curso", fontNormal3);
                table.AddCell(cell);

                cell.Phrase = new Phrase("Outcome", fontNormal3);
                table.AddCell(cell);

                cell.Phrase = new Phrase("Descripción", fontNormal3);
                table.AddCell(cell);

                fontNormal3 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
                cell.BackgroundColor = BaseColor.WHITE;
                for (int i = 0; i<viewModel.LstHallazgos.Count; i++)
                {
                    Hallazgo hallazgo = viewModel.LstHallazgos[i];

                    cell.Phrase = new Phrase(hallazgo.Criticidad.NombreEspanol, fontNormal3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(hallazgo.NivelAceptacionHallazgo.NombreEspanol, fontNormal3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(hallazgo.Codigo, fontNormal3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(hallazgo.Curso.NombreEspanol, fontNormal3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(hallazgo.Comision.Codigo + " | " + hallazgo.Outcome.Nombre, fontNormal3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(hallazgo.DescripcionEspanol, fontNormal3);
                    table.AddCell(cell);
                }


                Paragraph p = new Paragraph();
                p.IndentationLeft = 5;
                //p.IndentationRight = 5;
                table.HorizontalAlignment = Element.ALIGN_LEFT;
                p.Add(table);
                document.Add(p);


                document.Add(Chunk.NEWLINE);
                document.Add(Chunk.NEWLINE);
                document.Add(Chunk.NEWLINE);

                fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph tituloAsistencia = new Paragraph("Asistencia:", fontsub);
                tituloAsistencia.Alignment = Element.ALIGN_LEFT;
                document.Add(tituloAsistencia);

                document.Add(Chunk.NEWLINE);

                PdfPTable  table2 = new PdfPTable(4);

                table2.WidthPercentage = 100;
                //table2.SetWidthPercentage(new float[] {100, 100, 100, 100 }, document.PageSize);

                fontNormal3 = FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.WHITE);

                cell = new PdfPCell(new Phrase("Curso", fontNormal3));
                cell.BackgroundColor = new BaseColor(237, 28, 36);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.PaddingBottom = 5;
                table2.AddCell(cell);

                cell.Phrase = new Phrase("Sección", fontNormal3);
                table2.AddCell(cell);

                cell.Phrase = new Phrase("Nombre del delegado", fontNormal3);
                table2.AddCell(cell);
                cell.Phrase = new Phrase("Asistencia", fontNormal3);
                table2.AddCell(cell);

                fontNormal3 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
                cell.BackgroundColor = BaseColor.WHITE;
                for (int i = 0; i < viewModel.LstAlumnosSeccion.Count; i++)
                {
                    AlumnoSeccion alumnoseccion = viewModel.LstAlumnosSeccion[i];

                    cell.Phrase = new Phrase(alumnoseccion.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol, fontNormal3);
                    table2.AddCell(cell);
                    cell.Phrase = new Phrase(alumnoseccion.Seccion.Codigo, fontNormal3);
                    table2.AddCell(cell);
                    var alumno = alumnoseccion.AlumnoMatriculado.Alumno;
                    cell.Phrase = new Phrase(alumno.Nombres+" "+alumno.Apellidos, fontNormal3);
                    table2.AddCell(cell);
                    String marca = "";
                    if (viewModel.LstAsistencia[i])
                        marca = "X";
                    cell.Phrase = new Phrase(marca, fontNormal3);

                    table2.AddCell(cell);
                }


                Paragraph p2 = new Paragraph();
                p2.IndentationLeft = 5;
                //p.IndentationRight = 5;
                table2.HorizontalAlignment = Element.ALIGN_LEFT;
                p2.Add(table2);
                document.Add(p2);

                document.NewPage();

               /* fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph tituloEvidencia = new Paragraph("Evidencia:", fontsub);
                tituloEvidencia.Alignment = Element.ALIGN_LEFT;
                document.Add(tituloEvidencia);*/

                List<Evidencia> LstEvidencias = Context.Evidencia.Where(x => x.IdReunionDelegado == IdReunionDelegado).OrderBy(x=>x.Orden).ToList();
               
                for(int i =0; i<LstEvidencias.Count; i++)
                {
                    try
                    {
                        var primeraparte = Request.Url.GetLeftPart(UriPartial.Authority);
                        var direccion = primeraparte + "/" + Url.Content("~/" + LstEvidencias[i].ubicacion);
                        imagen = iTextSharp.text.Image.GetInstance(direccion);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_CENTER;
                        //imagen.ScaleAbsoluteHeight(document.PageSize.Height * 0.9f);
                        imagen.ScaleToFit(new Rectangle(document.PageSize.Width * 1.2f, document.PageSize.Width * 1.2f));
                        document.Add(imagen);

                        document.NewPage();
                    }
                    catch(Exception e)
                    {
                        continue;
                    }
                }

            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "Acta_Reunión_Delegados_" + viewModel.reunion.Comentario + ".pdf");
        }



    }

}