﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    public class JsonController : BaseController
    {
        //Little
        public JsonResult GetDatosAlumno(int? IdAlumno, int idSubModalidadPeriodoAcademico)
        {
            if (IdAlumno.HasValue == false) IdAlumno = 0;

            var Alumno = Context.Alumno.FirstOrDefault(x => x.IdAlumno==IdAlumno);

            var alumnosecciones = Context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == IdAlumno && x.Seccion.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).ToList();

            var Delegado = "";

            var alumnosecciondelegado = alumnosecciones.FirstOrDefault(x => x.EsDelegado);

            if(alumnosecciondelegado != null)
            {
                var seccion = alumnosecciondelegado.Seccion;

                Delegado = "<br/> " + MessageResource.DELEGADO + ": '" + seccion.CursoPeriodoAcademico.Curso.NombreEspanol + "'," + MessageResource.Seccion + " " + seccion.Codigo;
            }

            var CarreraNombre = "";

            var alumnomatriculado = Context.AlumnoMatriculado.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && x.IdAlumno == Alumno.IdAlumno);

            if(alumnomatriculado!= null)
            {
                CarreraNombre = alumnomatriculado.Carrera.NombreEspanol;
            }


            var data =  new { NombreCompleto = Alumno.Nombres +" "+Alumno.Apellidos, Carrera= CarreraNombre, Delegado = Delegado };

            return Json(data);

        }

        public JsonResult GetDatosDocente(int? IdDocente, int IdPeriodoAcademico)
        {
            if (IdDocente.HasValue == false) IdDocente = 0;

            var Docente = Context.Docente.FirstOrDefault(x => x.IdDocente == IdDocente);

            var data = new { NombreCompleto = Docente.Nombres + " " + Docente.Apellidos };

            return Json(data);

        }
        public JsonResult GetComentariosHallazgo(Int32 HallazgoId)
        {
           
            var lstComentario = (from ch in Context.ComentarioHallazgo
                                 join cod in Context.ComentarioDelegado on ch.IdComentarioDelegado equals cod.IdComentarioDelegado
                                 where ch.IdHallazgo == HallazgoId
                                 select cod).ToList();

            List<ComentarioDelegado> lstcomentariodelegado = new List<ComentarioDelegado>();

            foreach (var comentario in lstComentario)
            {
           

                ComentarioDelegado ocd = new ComentarioDelegado();
           
                    ocd.DescripcionEspanol = comentario.DescripcionEspanol;
                    ocd.CodigoComentario = comentario.CodigoComentario;
                    ocd.Alumno = comentario.Alumno;
                   lstcomentariodelegado.Add(ocd);
              
            }

            var data = (from a in lstcomentariodelegado
                        select new { CodComentario = a.CodigoComentario, Alumno = a.Alumno.Nombres + " " + a.Alumno.Apellidos, Comentario = a.DescripcionEspanol }).ToList();


            return Json(data, JsonRequestBehavior.AllowGet);





        }

        public JsonResult NewAlumnReg(string nombres, string apellidos, string correo, string carrera, string codigo , int idReunionDelegado)
        {
        try
         { 
            
            AbetEntities context = CargarDatosContext().context;

          
            var AlmInv =( from cust in context.AlumnoInvitado
                               where cust.codigo == codigo 
                               select cust);

            //retornar mensaje de alumno ya registradoo!!!! =) 
            

            if(AlmInv.Count() <= 0) { 
            AlumnoInvitado alumin = new AlumnoInvitado();
            alumin.nombres = nombres;
            alumin.apellido = apellidos;
            alumin.correo = correo;
            alumin.carrera = carrera;
            alumin.codigo = codigo;
                    alumin.IdReunionDelegado = idReunionDelegado;
            context.AlumnoInvitado.Add(alumin);
            context.SaveChanges();

              string menssage = "Success";

                return Json(menssage, JsonRequestBehavior.AllowGet);
            }
            else
            {
                    string menssage = "Warning";
                    return Json(menssage, JsonRequestBehavior.AllowGet);

             }

                

            }
            catch(Exception e)
            {
                string menssage = "Error";
                return Json(menssage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RefreshAlumninvt()
        {
            var LstAlumoInvitado = (from d in Context.AlumnoInvitado 
                                    select new SelectListItem { Text = d.codigo + " | " + d.nombres + " " + d.apellido, Value = d.idAlumnoInvitado.ToString() }).Distinct().ToList();

            LstAlumoInvitado.Insert(0, new SelectListItem { Value = "", Text = "[- " + LayoutResource.SinSeleccionar + " -]" });
            return Json(LstAlumoInvitado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RefreshAlumn(int IdPeriodoAcademico, int IdSede)
        {
            var LstAlumo = (from am in Context.AlumnoMatriculado
                            join u in Context.Alumno on am.IdAlumno equals u.IdAlumno
                            where am.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && am.IdSede == IdSede
                            select new SelectListItem { Text = u.Codigo + " | " + u.Nombres + " " + u.Apellidos, Value = u.IdAlumno.ToString() }).Distinct().ToList();

            LstAlumo.Insert(0, new SelectListItem { Value = "", Text = "[- " + LayoutResource.SinSeleccionar + " -]" });
            return Json(LstAlumo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTheLastAlumn()
        {
            //var last = (from d in context.AlumnoInvitado
            //                        select new  { Text = d.codigo  , Nombre =  d.nombres + " " + d.apellido }).FirstOrDefault();


            //return Json(last, JsonRequestBehavior.AllowGet);

            var ls = (from d in Context.AlumnoInvitado select d).ToList().OrderByDescending(c => c.idAlumnoInvitado);

            var LstAlumoInvitado = (from d in ls
                                    select new SelectListItem { Text = d.codigo   , Value = d.nombres + " " + d.apellido }).FirstOrDefault();

      
            return Json(LstAlumoInvitado, JsonRequestBehavior.AllowGet);


        }

        public JsonResult TraduccionActualizar(string textoatraducir)
        {
            YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();

            string textotraducido = helper.Translate(textoatraducir);

            var data = new { Text = textotraducido};

            return Json(data, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetCursosXAlumno(int IdAlumno, int IdPeriodoAcademico, int IdSede)
        {
            // var IdAlumnoMatriculado = context.AlumnoMatriculado.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdAlumnoMatriculado;

            /* var cursos = (from cpa in context.CursoPeriodoAcademico
                           join c in context.Curso on cpa.IdCurso equals c.IdCurso
                           where cpa.IdPeriodoAcademico == IdPeriodoAcademico && c.NombreEspanol.Trim() != ""
                           select c).OrderBy(x => x.NombreEspanol).ToList();*/

            //var data = new List<SelectListItem>();
            int idModalidad = Session.GetModalidadId();
            int idSubModalidadPeriodoAcademico = (from s in Context.SubModalidadPeriodoAcademico
                                                  join pa in Context.PeriodoAcademico on s.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                  join smpa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals smpa.IdPeriodoAcademico
                                                  join  sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad  
                                                  join m in Context.Modalidad on sm.IdSubModalidad equals m.IdModalidad
                                                  where s.IdPeriodoAcademico == IdPeriodoAcademico && m.IdModalidad == ModalidadId
                                                  select s.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            var data = (from cpa in Context.CursoPeriodoAcademico
                        join c in Context.Curso on cpa.IdCurso equals c.IdCurso
                        join cmc in Context.CursoMallaCurricular on c.IdCurso equals cmc.IdCurso
                        join s in Context.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                        join mc in Context.MallaCurricular on cmc.IdMallaCurricular equals mc.IdMallaCurricular
                        join mpa in Context.MallaCurricularPeriodoAcademico on mc.IdMallaCurricular equals mpa.IdMallaCurricular
                        where cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && c.NombreEspanol != ""
                         && mpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                         //&& s.IdSede == IdSede
                        //&& cmc.EsFormacion == false
                        select new { Text = c.NombreEspanol, IdCurso = c.IdCurso, Value= c.IdCurso.ToString() }).OrderBy(x => x.Text).Distinct().ToList();


            var cursosmatriculados = (from s in Context.Seccion
                                      join als in Context.AlumnoSeccion on s.IdSeccion equals als.IdSeccion
                                      join cpa in Context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                                      join c in Context.Curso on cpa.IdCurso equals c.IdCurso
                                      join am in Context.AlumnoMatriculado  on als.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                                      where am.IdAlumno == IdAlumno 
                                      && am.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && c.NombreEspanol.Trim() != ""
                                      && als.EsDelegado == true
                                      && am.IdSede == IdSede
                                      select s).Distinct().ToList();

            for (int i=0; i< cursosmatriculados.Count; i++)
            {
                int idcursomatriculado = cursosmatriculados[i].CursoPeriodoAcademico.IdCurso;

                var curso = data.FirstOrDefault(x => x.IdCurso == idcursomatriculado);

                if(curso !=null)
                {

                    data.Remove(curso);

                    curso = new { Text = "[" + cursosmatriculados[i].Codigo + "] " + curso.Text  , IdCurso = curso.IdCurso, Value= curso.IdCurso.ToString() };

                    data.Insert(0, curso);
                }
            }
         
            if (IdAlumno == 0)
                data.Insert(0, new { Text = "[" + LayoutResource.SinSeleccionar + "] ", IdCurso = 0, Value = "" });


            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetSeccionesXAlumno(int? IdAlumno, int idSubModalidadPeriodoAcademico)
        {
            if (IdAlumno.HasValue == false) IdAlumno = 0;

            var Alumno = Context.Alumno.FirstOrDefault(x => x.IdAlumno == IdAlumno);

            var alumnosecciones = Context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdAlumno == IdAlumno && x.Seccion.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).ToList();

            
            var data = new List<SelectListItem>();

            for(int i = 0; i< alumnosecciones.Count; i++)
            {

                string customtext = "";
                Seccion seccion = alumnosecciones[i].Seccion;
                Int32 idCurso = seccion.CursoPeriodoAcademico.IdCurso;

                if (seccion.CursoPeriodoAcademico.Curso.NombreEspanol == "")
                    continue;
                 
                customtext = seccion.CursoPeriodoAcademico.Curso.NombreEspanol+" - [" + seccion.Codigo+"]";

                var queryMC = (from mc in Context.MallaCocos
                               join mcd in Context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                               join oc in Context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                               join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                               where (cmc.IdCurso == idCurso && mc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico)
                               select oc);


                if (alumnosecciones[i].EsDelegado)
                {
                    customtext = customtext + " ("+MessageResource.EsDelegado+")";
                }

                data.Add(new SelectListItem {Value = seccion.IdSeccion.ToString(), Text = customtext });
       

            }

           

            return Json(data);

        }

        public JsonResult GetDocentesXCurso(int? IdAlumno, int? IdCurso, int IdPeriodoAcademico)
        {
            if (IdAlumno.HasValue == false) IdAlumno = 0;
            if (IdCurso.HasValue == false) IdCurso = 0;

            //var Curso = context.Curso.FirstOrDefault(x => x.IdCurso == IdCurso);

            //var CursoDocentes = context.CursoSeccion.Where(x => x.CursoMatriculado.IdCurso == IdCurso && x.Seccion.CursoPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).ToList();
            int idSubModalidadPeriodoAcademico = (from s in Context.SubModalidadPeriodoAcademico
                                                  where s.IdPeriodoAcademico == IdPeriodoAcademico
                                                  select s.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            var data = new List<SelectListItem>();


            data = (from ds in Context.DocenteSeccion
                    join d in Context.Docente on ds.IdDocente equals d.IdDocente
                               join s in Context.Seccion on ds.IdSeccion equals s.IdSeccion
                               join cpa in Context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                               join c in Context.Curso on cpa.IdCurso equals c.IdCurso
                               where cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && c.IdCurso== IdCurso
                               select new SelectListItem { Value = ds.IdSeccion.ToString(), Text = "[" + s.Codigo + "] " + d.Nombres+" "+d.Apellidos  }).Distinct().OrderBy(x=>x.Text).ToList();


            //for (int i = 0; i < CursoDocentes.Count; i++)
            //{

            //    string customtext = "";
            //    Seccion seccion = CursoDocentes[i].Seccion;
            //    Int32 idCurso = seccion.CursoPeriodoAcademico.IdCurso;

            //    if (seccion.CursoPeriodoAcademico.Curso.NombreEspanol == "")
            //        continue;

            //    customtext = seccion.CursoPeriodoAcademico.Curso.NombreEspanol + " - [" + seccion.Codigo + "]";

            //    var queryMC = (from mc in context.MallaCocos
            //                   join mcd in context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
            //                   join oc in context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
            //                   join cmc in context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
            //                   where (cmc.IdCurso == idCurso && mc.IdPeriodoAcademico == IdPeriodoAcademico)
            //                   select oc);


            //    if (CursoDocentes[i].EsDelegado)
            //    {
            //        customtext = customtext + " (Es delegado)";
            //    }

            //    data.Add(new SelectListItem { Value = seccion.IdSeccion.ToString(), Text = customtext });


            //}

            if (IdAlumno == 0)
                data.Insert(0, new SelectListItem { Value = "", Text = "[" + LayoutResource.SinSeleccionar + "] " });

            return Json(data, JsonRequestBehavior.AllowGet);

        }





    }
}