﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Professor;
using System.Data.Entity.Validation;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera)]
    public class PreScheduleCommitteController : BaseController
    {   
        public ActionResult Index()
        {
            return RedirectToAction("ConsultPreScheduleCommitte");
        }
        
        public ActionResult ConsultPreScheduleCommitte(DateTime? FechaInicio, DateTime? FechaFin, int? idSede, bool? isSearchRequest)
        {
            var viewModel = new ConsultPreScheduleCommitteViewModel();
            viewModel.CargarDatos(CargarDatosContext(), null, null, FechaInicio, FechaFin, idSede, isSearchRequest);
            if (viewModel != null)
            {
                if (viewModel.listaResultado.Count == 0 && isSearchRequest != null && (bool)isSearchRequest)
                {
                    PostMessage(MessageType.Info, PreScheduleResource.SinResultados);
                }
            }
            return View(viewModel);
        }
        
        public ActionResult CreatePreScheduleCommitte(Int32? IdPreAgenda)
        {
            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            //int idPeriodo = (int)SessionHelper.GetPeriodoAcademicoId(Session);
            int idSubModalidadPeriodo = (int)SessionHelper.GetSubModalidadPeriodoAcademicoId(Session);
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            //if (!GedServices.DocentePerteneceOrganigrama(context, idDocenteSesion, idPeriodo, escuelaId))
            if (!GedServices.DocentePerteneceOrganigrama(Context, idDocenteSesion, idSubModalidadPeriodo, idEscuela))
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoTienePermisosSuficientesParaCrearUnaPreagendaDeComiteConsultivo);
                return RedirectToAction("ConsultPreScheduleCommitte");
            }

            var ResponsableCarrera = Context.UnidadAcademicaResponsable.Where(x => x.IdDocente == idDocenteSesion
                && x.SedeUnidadAcademica.UnidadAcademica.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodo
                && x.SedeUnidadAcademica.UnidadAcademica.IdCarreraPeriodoAcademico.HasValue).ToList();
                
            if(ResponsableCarrera.Count == 0)
            {
                PostMessage(MessageType.Warning, MessageResource.IElUsuarioNoEsCoordinadorCarreraYnoPodraCrearPreagendaDeComiteConsultivoI);
                return RedirectToAction("ConsultPreScheduleCommitte");
            }

            var model = new CreatePreScheduleCommitteViewModel();
            model.Fill(Context, Session, IdPreAgenda);
            return View(model);
        }

        [HttpPost]
        public ActionResult CreatePreScheduleCommitte(CreatePreScheduleCommitteViewModel model)
        {
            try
            {
                var idEscuela = Session.GetEscuelaId();
                //VALIDACIONES PARA EL REGISTRO DE LA PREAGENDA
                if (model.Fecha.Date < DateTime.Now.Date)
                    PostMessage(MessageType.Error, MessageResource.LaFechaNoDebeSerAnterioraLaActual);
                else if (String.IsNullOrEmpty(model.Motivo))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarUnMotivo);
                else if (model.listaParticipantesNormales.Where(x => x.IsSelected == true).Count() == 1 && String.IsNullOrEmpty(model.lstExternos))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosParticipantes);
                else
                {
                    //INSTANCIO EL OBJETO Y REVISO SI LO TENGO O NO
                    PreAgenda preAgenda;
                    if (model.IdPreAgenda.HasValue)
                    {
                        //SI VOY A EDITAR ELIMINO LOS TEMAS Y PARTICIPANTES PARA PODER ACTUALIZAR
                        preAgenda = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == model.IdPreAgenda);
                        var participantes = Context.ParticipanteReunion.Where(x => x.IdPreAgenda == preAgenda.IdPreAgenda).ToList();
                        Context.ParticipanteReunion.RemoveRange(participantes);
                        Context.SaveChanges();
                    }
                    else
                    {
                        preAgenda = new PreAgenda();
                    }
                    //---------------------------------------------
                    //ASIGNO LOS CAMPOS DE INFORMACIÓN GENERAL
                    preAgenda.IdSede = model.IdSede;
                    //preAgenda.IdUnidaAcademica = GedServices.GetUnidadesAcademicasDocenteServices(context, Session.GetDocenteId().Value, Session.GetPeriodoAcademicoId().Value, escuelaId).First().IdUnidadAcademica;
                    preAgenda.IdUnidaAcademica = GedServices.GetUnidadesAcademicasDocenteServices(Context, Session.GetDocenteId().Value, Session.GetSubModalidadPeriodoAcademicoId().Value, idEscuela).First().IdUnidadAcademica;
                    //preAgenda.IdSubModalidadPeriodoAcademico = Session.GetPeriodoAcademicoId() ?? 0;
                    preAgenda.IdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId() ?? 0;
                    preAgenda.FechaReunion = model.Fecha;
                    preAgenda.Motivo = model.Motivo;
                    preAgenda.TipoPreagenda = "RCC";
                    preAgenda.Nivel = 1;
                    preAgenda.IdUsuarioCreador = Session.GetDocenteId();
                    //---------------------------------------------
                    //SIN IdPreAgenda: REGISTRO LA PreAgenda; CON IdPreAgenda: ACTUALIZO LA PREAGENDA
                    if (!model.IdPreAgenda.HasValue)
                    {
                        preAgenda.FechaCreacion = DateTime.Now;
                        Context.PreAgenda.Add(preAgenda);
                        Context.SaveChanges();
                    }
                    else
                    {
                        var auxPreAgenda = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == preAgenda.IdPreAgenda);
                        auxPreAgenda.IdSede = preAgenda.IdSede;
                        auxPreAgenda.FechaReunion = preAgenda.FechaReunion;
                        auxPreAgenda.Motivo = preAgenda.Motivo;
                        auxPreAgenda.IdUsuarioCreador = preAgenda.IdUsuarioCreador;
                        auxPreAgenda.Semana = preAgenda.Semana;
                        Context.Entry(auxPreAgenda).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                    }
                    //---------------------------------------------
                    
                    var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true).ToList();
                    
                    List<ParticipanteExterno> lstAuxExternos = new List<ParticipanteExterno>();
                    if (!String.IsNullOrEmpty(model.lstExternos))
                    {
                        var firstEncode = model.lstExternos.Split('*');
                        for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                        {
                            var data = firstEncode[i].Split('|');
                            lstAuxExternos.Add(new ParticipanteExterno
                            {
                                Nombre = data[j],
                                Cargo = data[j + 1],
                                Correo = data[j + 2],
                                IdEmpresa = ConvertHelpers.ToInteger(data[j + 3])
                            });
                        }
                    }
                    
                    foreach (var participante in participantesNormales)
                    {
                        ParticipanteReunion objParticipante = new ParticipanteReunion();
                        objParticipante.IdPreAgenda = preAgenda.IdPreAgenda;
                        objParticipante.IdDocente = participante.IdParticipante;
                        objParticipante.Asistio = false;
                        objParticipante.NombreCompleto = participante.Nombre;
                        objParticipante.IdUnidadAcademicaResponsable = participante.IdUnidadAcademicaResponsable;
                        objParticipante.Cargo = participante.UnidadAcademica;
                        Context.ParticipanteReunion.Add(objParticipante);
                        Context.SaveChanges();
                    }
                    
                    foreach (var item in lstAuxExternos)
                    {
                        ParticipanteReunion objParticipanteExterno = new ParticipanteReunion();
                        objParticipanteExterno.IdPreAgenda = preAgenda.IdPreAgenda;
                        objParticipanteExterno.Asistio = false;
                        objParticipanteExterno.EsExterno = true;
                        objParticipanteExterno.NombreCompleto = item.Nombre;
                        objParticipanteExterno.Cargo = item.Cargo;
                        objParticipanteExterno.Correo = item.Correo;
                        objParticipanteExterno.EmpresaId = item.IdEmpresa;
                        Context.ParticipanteReunion.Add(objParticipanteExterno);
                        Context.SaveChanges();
                    }
                    //---------------------------------------------
                    PostMessage(MessageType.Success);
                    return RedirectToAction("ConsultPreScheduleCommitte", "PreScheduleCommitte");
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("CreatePreScheduleCommitte", "PreScheduleCommitte", new { IdPreAgenda = model.IdPreAgenda });
        }
        
        public ActionResult DetailsPreScheduleCommitte(Int32 IdPreAgenda)
        {
            var model = new CreatePreScheduleCommitteViewModel();
            model.Fill(Context, Session, IdPreAgenda);
            return View(model);
        }
        
        public ActionResult _DeletePreScheduleCommitte(Int32 IdPreAgenda)
        {
            var model = new _DeletePreScheduleCommitteViewModel();
            model.Fill(Context, IdPreAgenda);
            return View(model);
        }
        
        [HttpPost]
        public ActionResult _DeletePreScheduleCommitte(_DeletePreScheduleCommitteViewModel model)
        {
            try
            {
                var preAgenda = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == model.IdPreAgenda);
                var participantesPreAgenda = Context.ParticipanteReunion.Where(x => x.IdPreAgenda == model.IdPreAgenda).ToList();
                foreach(var item in participantesPreAgenda)
                {
                    Context.ParticipanteReunion.Remove(item);
                    Context.SaveChanges();
                }
                Context.PreAgenda.Remove(preAgenda);
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("ConsultPreScheduleCommitte", "PreScheduleCommitte");
            }
            catch(Exception)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("ConsultPreScheduleCommitte", "PreScheduleCommitte");
            }
        }
    }
}