﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.MeetingCommitte;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.CoordinadorCarrera)]
    public class MeetingCommitteController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("ConsultMeetingCommitte");
        }
        
        public ActionResult ConsultMeetingCommitte(int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, bool? isSearchRequest, Int32 p = 1)
        {
            var viewModel = new ConsultReunionViewModel();

            viewModel.Niveles = Context.UnidadAcademica.Where(X => X.Nivel > 0).Select(X => X.Nivel).Distinct().ToList().Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            viewModel.Semanas = GedServices.GetSemanasDisponiblesServices(Context)
                    .Select(c => new SelectListItem { Value = c.ToString().ToString(), Text = c.ToString() });
            viewModel.Sedes = GedServices.GetAllSedesServices(Context)
                    .Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });

            // viewModel.IdArea = GedServices.GetAllAreasForPeriodoAcademico(context, );
            var fechasSemana = GedServices.GetDateStartWeekCurrentPeriodoAcademicoBySemanaServices(Context, Session, idSemana.GetValueOrDefault(0));

            try
            {
                viewModel.FechaInicio = FechaInicio == DateTime.MinValue ? fechasSemana[0] : FechaInicio.Value;
                viewModel.FechaFin = FechaFin == DateTime.MinValue ? fechasSemana[1] : FechaFin.Value;
            }
            catch { };

            if (isSearchRequest != null && (bool)isSearchRequest)
            {
                var query = GedServices.GetReunionesBusquedaServices(Context, idNivel, idSemana, FechaInicio, FechaFin, idSede).Where(x => x.IdPreAgenda != null);
                query = query.Where(x => x.PreAgenda.TipoPreagenda.Equals(ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO));
                if (query.Count() > 0)
                {
                    viewModel.ListaReunionResultado = query.ToPagedList(p, ConstantHelpers.DEFAULT_PAGE_SIZE);
                }

                if (viewModel.ListaReunionResultado == null || !viewModel.ListaReunionResultado.Any())
                {
                    PostMessage(Helpers.MessageType.Info, MessageResource.NoSeEcontraronResultadosParaBusqueda);
                }
            }

            return View(viewModel);
        }

        public ActionResult AddEditMeetingCommitte(Int32 IdPreAgenda, Int32? IdReunion)
        {
            var model = new AddEditMeetingCommitteViewModel();
            try
            {
                if (IdReunion.HasValue)
                {
                    Int32 IdDocenteCreador = Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdDocente.Value;
                    if (Session.GetDocenteId() != IdDocenteCreador)
                    {
                        PostMessage(MessageType.Warning, MessageResource.LaEdicionEstaPermitidaAlDocenteCreadorDeLaReunion);
                        return RedirectToAction("Index");
                    }
                    var acta = Context.Reunion.Find(IdReunion.Value).Acta;
                    if (acta.FirstOrDefault() != null)
                    {
                        PostMessage(MessageType.Warning, " " + MessageResource.NoSePuedeEditarLaReunionYaTieneUnaActaAsociada);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    var reunion = Context.Reunion.FirstOrDefault(x => x.IdPreAgenda == IdPreAgenda);
                    if (reunion != null)
                    {
                        PostMessage(MessageType.Error, MessageResource.YaExisteUnaReunionRegistradaParaEstaPreagenda);
                        return RedirectToAction("Index");
                    }
                }
                model.Fill(Context, Session, IdPreAgenda, IdReunion);
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditMCommitte(AddEditMeetingCommitteViewModel model)
        {
            try
            {
                //VALIDACIONES PARA EL REGISTRO DE LA REUNIÓN
                if (String.IsNullOrEmpty(model.Motivo))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarUnMotivo);
                else if (model.Fecha.Date < DateTime.Now.Date)
                    PostMessage(MessageType.Error, MessageResource.LaFechaNoDebeSerAnterioraLaActual);
                else if (model.HoraInicio.Hour > model.HoraFin.Hour)
                    PostMessage(MessageType.Error, MessageResource.LaHoraFinDeReunionDebeSerSuperiorALaHoraInicio);
                else if (model.HoraInicio.Hour == model.HoraFin.Hour && model.HoraInicio.Minute >= model.HoraFin.Minute)
                    PostMessage(MessageType.Error, MessageResource.LaHoraFinDeReunionDebeSerSuperiorALaHoraInicio);
                else if (String.IsNullOrEmpty(model.Objetivo))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarElObjetivo);
                else if (model.listaParticipantesNormales.Where(x => x.IsSelected == true).Count() == 1 && String.IsNullOrEmpty(model.lstExternos))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosParticipantes);
                else if (String.IsNullOrEmpty(model.Agenda))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarUnaAgenda);
                else
                {
                    //LÓGICA PARA EL REGISTRO Y/O EDICIÓN DE LA REUNIÓN
                    //INSTANCIO EL OBJETO Y REVISO SI LO TENGO O NO
                    Reunion reunion;
                    PreAgenda preagenda;
                    preagenda = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == model.IdPreAgenda);
                    
                    if (model.IdReunion.HasValue)
                    {
                        //SI VOY A EDITAR ELIMINO LOS TEMAS Y PARTICIPANTES PARA PODER ACTUALIZAR
                        reunion = Context.Reunion.FirstOrDefault(x => x.IdReunion == model.IdReunion);
                        GedServices.EliminarParticipantesReunionServices(Context, reunion.IdReunion);
                    }
                    else
                    {
                        reunion = new Reunion();
                        reunion.IdPreAgenda = preagenda.IdPreAgenda;
                        reunion.IdUnidadAcademica = preagenda.IdUnidaAcademica;
                        reunion.Nivel = preagenda.Nivel;
                    }
                    //---------------------------------------------
                    //ASIGNO LOS CAMPOS DE INFORMACIÓN GENERAL
                    reunion.Motivo = model.Motivo;
                    reunion.IdSede = model.IdSede;
                    reunion.Fecha = model.Fecha;
                    reunion.Lugar = model.Lugar;
                    reunion.HoraInicio = model.HoraInicio;
                    reunion.HoraFin = model.HoraFin;
                    reunion.ObjetivoComiteConsultivo = model.Objetivo;
                    reunion.AgendaComiteConsultivo = model.Agenda;
                    reunion.Semana = ConvertHelpers.GetNumeroSemanaOfReunion(preagenda.SubModalidadPeriodoAcademico.PeriodoAcademico.FechaInicioPeriodo.Value, reunion.Fecha);
                    reunion.IdDocente = preagenda.IdUsuarioCreador;
                    reunion.IdEstadoReunion = 1;
                    reunion.IdSubModalidadPeriodoAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
                    reunion.Estado = "PROGRAMADO";
                    //---------------------------------------------
                    //SIN IdReunion: REGISTRO LA REUNIÓN; CON IdReunion: ACTUALIZO LA REUNIÓN
                    if (!model.IdReunion.HasValue)
                    {
                        reunion.FechaRegistro = DateTime.Now.Date;
                        Context.Reunion.Add(reunion);
                    }
                    /*
                    else
                    {
                        context.Entry(reunion).State = System.Data.Entity.EntityState.Modified;
                    }
                    */
                    Context.SaveChanges();
                    //---------------------------------------------
                    //REGISTRO LOS PARTICIPANTES NORMALES SELECCIONADOS
                    var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true).ToList();
                    foreach (var participante in participantesNormales)
                    {
                        ParticipanteReunion objParticipante = new ParticipanteReunion();
                        objParticipante.IdReunion = reunion.IdReunion;
                        objParticipante.IdDocente = participante.IdParticipante;
                        objParticipante.Asistio = false;
                        objParticipante.NombreCompleto = participante.Nombre;
                        objParticipante.IdUnidadAcademicaResponsable = participante.IdUnidadAcademicaResponsable;
                        objParticipante.Cargo = participante.UnidadAcademica;
                        Context.ParticipanteReunion.Add(objParticipante);
                        Context.SaveChanges();
                    }
                    //---------------------------------------------
                    
                    //REGISTRO LOS PARTICIPANTES EXTERNOS
                    if (!String.IsNullOrEmpty(model.lstExternos))
                    {
                        var firstEncode = model.lstExternos.Split('*');
                        List<ParticipanteExterno> lstAuxExternos = new List<ParticipanteExterno>();
                        for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                        {
                            var data = firstEncode[i].Split('|');
                            lstAuxExternos.Add(new ParticipanteExterno
                            {
                                Nombre = data[j],
                                Cargo = data[j + 1],
                                Correo = data[j + 2],
                                IdEmpresa = ConvertHelpers.ToInteger(data[j + 3])
                            });
                        }
                    
                        foreach (var item in lstAuxExternos)
                        {
                            ParticipanteReunion objParticipanteExterno = new ParticipanteReunion();
                            objParticipanteExterno.IdReunion = reunion.IdReunion;
                            objParticipanteExterno.Asistio = false;
                            objParticipanteExterno.EsExterno = true;
                            objParticipanteExterno.NombreCompleto = item.Nombre;
                            objParticipanteExterno.Cargo = item.Cargo;
                            objParticipanteExterno.Correo = item.Correo;
                            objParticipanteExterno.EmpresaId = item.IdEmpresa;
                            Context.ParticipanteReunion.Add(objParticipanteExterno);
                            Context.SaveChanges();
                        }
                    }
                    //---------------------------------------------
                    PostMessage(MessageType.Success);
                    return RedirectToAction("ConsultMeetingCommitte", "MeetingCommitte");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}' ", e);
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddEditMeetingCommitte", "MeetingCommitte", new { IdPreAgenda = model.IdPreAgenda, IdReunion = model.IdReunion });
        }

        public ActionResult DetailsMeetingCommitte(int IdReunion)
        {
            var viewmodel = new AddEditMeetingCommitteViewModel();
            viewmodel.Fill(Context, Session, Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdPreAgenda.Value, IdReunion);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult DeleteReunion(int id)
        {
            if (GedServices.EliminarReunionServices(Context, id))
                return Json(true);
            else
                return Json(false);
        }

        [HttpPost]
        public ActionResult CrearEmpresa(string razon, string ruc, string email)
        {
            var empresa = new Empresa();
            empresa.RazonSocial = razon;
            empresa.RUC = ruc;
            empresa.EmailContacto = email;
            GedServices.CrearEmpresaServices(Context, empresa);
            return Json(new SelectList(GedServices.GetAllEmpresasServices(Context), "IdEmpresa", "RazonSocial"));
        }
    }
}