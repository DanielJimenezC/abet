﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using System.Data;

using System.Configuration;
using UPC.CA.ABET.Presentation.Areas.Meeting.Logic;
using System.Data.Entity.Validation;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
// comentario 1 borrar
namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.Docente)]
    public class MeetingController : BaseController
    {
        public string CurrentCulture { get; set; }

        public ActionResult Index()
        {
            return RedirectToAction("MaintenanceMeetingExtraordinary");
        }
        public ActionResult Management()
        {
            return View();
        }
        public ActionResult ManagementACC()
        {
            return View();
        }

        public ActionResult ManagementRD()
        {
            return View();
        }

        public ActionResult IndexExtraordinary()
        {
            return RedirectToAction("MaintenanceMeetingExtraordinary");
        }

        public ActionResult IndexPreSchedule()
        {
            return RedirectToAction("MaintenanceMeetingPreSchedule");
        }
        
        public ActionResult _AssignState(Int32 idReunion)
        {
            var model = new _AssignStateViewModel();
            model.Fill(Context, idReunion);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult _AssignState(_AssignStateViewModel model)
        {
            try
            {
                var reunion = Context.Reunion.FirstOrDefault(x => x.IdReunion == model.IdReunion);
                DateTime fecha = ConvertHelpers.ToDateTime(model.FechaProgramacion);
                reunion.Fecha = fecha;
                reunion.HoraInicio = new DateTime(fecha.Year, fecha.Month, fecha.Day, model.HoraInicio.Hour, model.HoraInicio.Minute, model.HoraInicio.Second);
                reunion.HoraFin = new DateTime(fecha.Year, fecha.Month, fecha.Day, model.HoraFin.Hour, model.HoraFin.Minute, model.HoraFin.Second);
                reunion.Estado = "REPROGRAMADO";
                reunion.IdEstadoReunion = 2;
                Context.SaveChanges();
                PostMessage(MessageType.Success);

                if(reunion.EsExtraordinaria)
                    return RedirectToAction("MaintenanceMeetingExtraordinary", "Meeting");
                else
                    return RedirectToAction("MaintenanceMeetingPreSchedule", "Meeting");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult _CancelState(Int32 idReunion)
        {
            var model = new _CancelStateViewModel();
            model.Fill(Context, idReunion);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult _CancelState(_CancelStateViewModel model)
        {
            try
            {
                var reunion = Context.Reunion.FirstOrDefault(x => x.IdReunion == model.IdReunion);
                reunion.Estado = "CANCELADO";
                reunion.IdEstadoReunion = 3;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                if (reunion.EsExtraordinaria)
                    return RedirectToAction("MaintenanceMeetingExtraordinary", "Meeting");
                else
                    return RedirectToAction("MaintenanceMeetingPreSchedule", "Meeting");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult _DeleteMeeting(Int32 IdReunion)
        {
            var model = new _DeteleMeetingViewModel();
            model.Fill(Context, IdReunion);
            return View(model);
        }

        [HttpPost]
        public ActionResult _DeleteMeeting(_DeteleMeetingViewModel model)
        {
            var acta = Context.Reunion.Find(model.IdReunion).Acta;
            if (acta.FirstOrDefault() != null)
            {
                PostMessage(MessageType.Warning, " " + MessageResource.NoSePuedeEliminarLaReunionYaTieneUnActaAsociada);
                return RedirectToAction("Index");
            }

            if (GedServices.EliminarReunionServices(Context, model.IdReunion))
                PostMessage(MessageType.Success);
            else
                PostMessage(MessageType.Error);

            switch (model.Tipo)
            {
                case "RCO":
                    return RedirectToAction("MaintenanceMeetingPreSchedule", "Register");
                case "RCC":
                    return RedirectToAction("MaintenanceMeetingCommitte", "Register");
                default:
                    return RedirectToAction("MaintenanceMeetingExtraordinary", "Register");
            }
        }
        
        public ActionResult MaintenanceMeetingExtraordinary(Int32? NumeroPagina, Int32? IdNivel, Int32? IdSemana, DateTime? FechaInicio, DateTime? FechaFin, Int32? IdSede)
        {
            ConsultMeetingExtraordinaryViewModel model = new ConsultMeetingExtraordinaryViewModel();
            model.Fill(Context, Session, NumeroPagina, IdNivel, IdSemana, FechaInicio, FechaFin, IdSede);
            return View(model);
        }
        
        public ActionResult MaintenanceMeetingPreSchedule(Int32? NumeroPagina, Int32? IdNivel, Int32? IdSemana, DateTime? FechaInicio, DateTime? FechaFin, Int32? IdSede)
        {
            ConsultMeetingPreScheduleViewModel model = new ConsultMeetingPreScheduleViewModel();
            //if (!NumeroPagina.HasValue)
            //{
            //    model.Fill(Context, Session, NumeroPagina, IdNivel, IdSemana, FechaInicio, FechaFin, IdSede);
            //}
            //else {
            //    model.Fill_2(Context,  NumeroPagina);
            //}

            return View(model);
        }

        public ActionResult AddEditMeetingPreSchedule(int IdPreAgenda, int? IdReunion)
        {
            if (IdReunion.HasValue)
            {
                Int32 IdDocenteCreador = Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdDocente.Value;
                if (Session.GetDocenteId() != IdDocenteCreador)
                {
                    PostMessage(MessageType.Warning, MessageResource.LaEdicionEstaPermitidaAlDocenteCreadorDeLaReunion);
                    return RedirectToAction("IndexPreSchedule");
                }
                var acta = Context.Reunion.Find(IdReunion.Value).Acta;
                var preagenda = Context.Reunion.Find(IdReunion.Value).IdPreAgenda;

                if (acta.FirstOrDefault()!=null)
                {
                    PostMessage(MessageType.Warning, " " + MessageResource.NoSePuedeEditarLaReunionYaTieneUnaActaAsociada);
                    if (preagenda!=null)
                    {
                        return RedirectToAction("IndexPreSchedule");
                    }
                    else
                    {
                        return RedirectToAction("IndexExtraordinary");
                    }

                    
                }
            }
            Int32 IdPreAgendaCreador = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == IdPreAgenda).IdUsuarioCreador.Value;
            if ( Session.GetDocenteId()!= IdPreAgendaCreador)
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoPuedeCrearReunionSobreUnaPreagendaDeOtroUsuario);
                return RedirectToAction("IndexPreSchedule");
            }
            var viewmodel = new AddEditMeetingPreScheduleViewModel();
            viewmodel.Fill(Context, Session, IdPreAgenda, IdReunion);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddEditMPreSchedule(AddEditMeetingPreScheduleViewModel model)
        {
            try
            {
                //VALIDACIONES PARA EL REGISTRO DE LA REUNIÓN
                if (String.IsNullOrEmpty(model.Motivo))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarUnMotivo);
                else if (model.Fecha.Date < DateTime.Now.Date)
                    PostMessage(MessageType.Error, MessageResource.LaFechaNoDebeSerAnterioraLaActual);
                else if (model.HoraInicio.Hour > model.HoraFin.Hour || (model.HoraInicio.Hour == model.HoraFin.Hour && model.HoraInicio.Minute >= model.HoraFin.Minute))
                    PostMessage(MessageType.Error, MessageResource.LasHorasDeInicioYfinNoConcuerdenEntreSi);
                else if (String.IsNullOrEmpty(model.lstTemas))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosTemas);
                else if (model.listaParticipantesNormales.Where(x => x.IsSelected == true).Count() == 1 && String.IsNullOrEmpty(model.lstExternos))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosParticipantes);
                else
                {
                    //LÓGICA PARA EL REGISTRO Y/O EDICIÓN DE LA REUNIÓN
                    //INSTANCIO EL OBJETO Y REVISO SI LO TENGO O NO
                    Reunion reunion;
                    PreAgenda preagenda;
                    preagenda = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == model.IdPreAgenda);
                    if (model.IdReunion.HasValue)
                    {
                        //SI VOY A EDITAR ELIMINO LOS TEMAS Y PARTICIPANTES PARA PODER ACTUALIZAR
                        reunion = Context.Reunion.FirstOrDefault(x => x.IdReunion == model.IdReunion);
                        GedServices.EliminarTemasDeReunionService(Context, reunion.IdReunion);
                        GedServices.EliminarParticipantesReunionServices(Context, reunion.IdReunion);
                    }
                    else
                    {
                        reunion = new Reunion();
                        reunion.IdPreAgenda = model.IdPreAgenda;
                        reunion.Nivel = preagenda.Nivel;
                        reunion.IdUnidadAcademica = preagenda.IdUnidaAcademica;
                        reunion.IdSubModalidadPeriodoAcademico = preagenda.IdSubModalidadPeriodoAcademico;
                    }
                    //---------------------------------------------
                    //ASIGNO LOS CAMPOS DE INFORMACIÓN GENERAL
                    reunion.IdSede = model.IdSede;
                    reunion.Fecha = model.Fecha;
                    reunion.Lugar = model.Lugar;
                    reunion.HoraInicio = new DateTime(model.Fecha.Year, model.Fecha.Month, model.Fecha.Day, model.HoraInicio.Hour, model.HoraInicio.Minute, model.HoraInicio.Second);
                    reunion.HoraFin = new DateTime(model.Fecha.Year, model.Fecha.Month, model.Fecha.Day, model.HoraFin.Hour, model.HoraFin.Minute, model.HoraFin.Second);
                    reunion.Motivo = model.Motivo;
                    reunion.Frecuencia = model.Frecuencia;
                    reunion.IdDocente = preagenda.IdUsuarioCreador;
                    reunion.Semana = ConvertHelpers.GetNumeroSemanaOfReunion(preagenda.SubModalidadPeriodoAcademico.PeriodoAcademico.FechaInicioPeriodo.Value, reunion.Fecha);
                    reunion.EsExtraordinaria = false;
                    reunion.IdEstadoReunion = 1;
                    reunion.Estado = "PROGRAMADO";
                    //---------------------------------------------
                    //SIN IdReunion: REGISTRO LA REUNIÓN; CON IdReunion: ACTUALIZO LA REUNIÓN
                    if (!model.IdReunion.HasValue)
                    {
                        GedServices.CrearReunionServices(Context, reunion);
                    }
                    else
                    {
                        Context.Entry(reunion).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                    }
                    //---------------------------------------------
                    if(model.Editar || model.Frecuencia == 0)
                    {
                        if (!String.IsNullOrEmpty(model.lstTemas))
                        {
                            var auxlstaTemas = model.lstTemas.Split('*');
                            List<String> lstAuxTemas = new List<string>();
                            foreach (string item in auxlstaTemas)
                            {
                                lstAuxTemas.Add(item);
                            }
                            model.listaTemas = lstAuxTemas;
                        }
                    
                        List<ParticipanteExterno> lstAuxExternos = new List<ParticipanteExterno>();
                        if (!String.IsNullOrEmpty(model.lstExternos))
                        {
                            var firstEncode = model.lstExternos.Split('*');
                            for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                            {
                                var data = firstEncode[i].Split('|');
                                lstAuxExternos.Add(new ParticipanteExterno
                                {
                                    Nombre = data[j],
                                    Cargo = data[j + 1],
                                    Correo = data[j + 2],
                                    IdEmpresa = ConvertHelpers.ToInteger(data[j + 3])
                                });
                            }
                        }
                    
                        //REGISTRO LOS TEMAS UNA VEZ TENGA UNA REUNIÓN REGISTRADA O ID ASOCIADO
                        foreach (var item in model.listaTemas)
                        {
                            GedServices.CrearTemaDeReunionService(Context, item, reunion.IdReunion);
                        }
                        //---------------------------------------------
                        var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true).ToList();
                        //REGISTRO LOS PARTICIPANTES NORMALES SELECCIONADOS
                    
                    
                        foreach (var participante in participantesNormales)
                        {
                            ParticipanteReunion objParticipante = new ParticipanteReunion();
                            objParticipante.IdReunion = reunion.IdReunion;
                            objParticipante.IdDocente = participante.IdParticipante;
                            objParticipante.Asistio = false;
                            objParticipante.NombreCompleto = participante.Nombre;
                            objParticipante.IdUnidadAcademicaResponsable = participante.IdUnidadAcademicaResponsable;
                            objParticipante.Cargo = participante.UnidadAcademica;
                            Context.ParticipanteReunion.Add(objParticipante);
                            Context.SaveChanges();
                        }
                        //---------------------------------------------
                        //REGISTRO LOS PARTICIPANTES EXTERNOS
                        foreach (var item in lstAuxExternos)
                        {
                            ParticipanteReunion objParticipanteExterno = new ParticipanteReunion();
                            objParticipanteExterno.IdReunion = reunion.IdReunion;
                            objParticipanteExterno.Asistio = false;
                            objParticipanteExterno.EsExterno = true;
                            objParticipanteExterno.NombreCompleto = item.Nombre;
                            objParticipanteExterno.Cargo = item.Cargo;
                            objParticipanteExterno.Correo = item.Correo;
                            objParticipanteExterno.EmpresaId = item.IdEmpresa;
                            Context.ParticipanteReunion.Add(objParticipanteExterno);
                            Context.SaveChanges();
                        }
                    }
                    else
                    {
                        //PARA SABER EL ID DE CADA REUNIÓN REGISTRADA A LA PREAGENDA
                        var lstaReunionesPreAgenda = Context.Reunion.Where(x => x.IdPreAgenda == model.IdPreAgenda).ToList();
                    
                        if (lstaReunionesPreAgenda.Count > 0)
                        {
                            if (!String.IsNullOrEmpty(model.lstTemas))
                            {
                                var auxlstaTemas = model.lstTemas.Split('*');
                                List<String> lstAuxTemas = new List<string>();
                                foreach (string item in auxlstaTemas)
                                {
                                    lstAuxTemas.Add(item);
                                }
                                model.listaTemas = lstAuxTemas;
                            }
                    
                            var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true).ToList();
                    
                            List<ParticipanteExterno> lstAuxExternos = new List<ParticipanteExterno>();
                            if (!String.IsNullOrEmpty(model.lstExternos))
                            {
                                var firstEncode = model.lstExternos.Split('*');
                                for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                                {
                                    var data = firstEncode[i].Split('|');
                                    lstAuxExternos.Add(new ParticipanteExterno
                                    {
                                        Nombre = data[j],
                                        Cargo = data[j + 1],
                                        Correo = data[j + 2],
                                        IdEmpresa = ConvertHelpers.ToInteger(data[j + 3])
                                    });
                                }
                            }
                    
                            //---------------------------------------------
                    
                            foreach (var reu in lstaReunionesPreAgenda)
                            {
                                //REGISTRO LOS TEMAS UNA VEZ TENGA UNA REUNIÓN REGISTRADA O ID ASOCIADO
                                foreach (var item in model.listaTemas)
                                {
                                    GedServices.CrearTemaDeReunionService(Context, item, reu.IdReunion);
                                }
                                //---------------------------------------------
                                //REGISTRO LOS PARTICIPANTES NORMALES SELECCIONADOS
                                foreach (var participante in participantesNormales)
                                {
                                    ParticipanteReunion objParticipante = new ParticipanteReunion();
                                    objParticipante.IdReunion = reunion.IdReunion;
                                    objParticipante.IdDocente = participante.IdParticipante;
                                    objParticipante.Asistio = false;
                                    objParticipante.NombreCompleto = participante.Nombre;
                                    objParticipante.IdUnidadAcademicaResponsable = participante.IdUnidadAcademicaResponsable;
                                    objParticipante.Cargo = participante.UnidadAcademica;
                                    Context.ParticipanteReunion.Add(objParticipante);
                                    Context.SaveChanges();
                                }
                                //---------------------------------------------
                                //REGISTRO LOS PARTICIPANTES EXTERNOS
                                foreach (var item in lstAuxExternos)
                                {
                                    ParticipanteReunion objParticipanteExterno = new ParticipanteReunion();
                                    objParticipanteExterno.IdReunion = reu.IdReunion;
                                    objParticipanteExterno.Asistio = false;
                                    objParticipanteExterno.EsExterno = true;
                                    objParticipanteExterno.NombreCompleto = item.Nombre;
                                    objParticipanteExterno.Cargo = item.Cargo;
                                    objParticipanteExterno.Correo = item.Correo;
                                    objParticipanteExterno.EmpresaId = item.IdEmpresa;
                                    Context.ParticipanteReunion.Add(objParticipanteExterno);
                                    Context.SaveChanges();
                                }
                            }
                            //---------------------------------------------
                        }
                    }
                    PostMessage(MessageType.Success);
                    return RedirectToAction("MeetingsAssociated", "Meeting", new { IdPreAgenda = model.IdPreAgenda });
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddEditMeetingPreSchedule", "Meeting", new { IdPreAgenda = model.IdPreAgenda, IdReunion = model.IdReunion });
        }

        public ActionResult AddEditMeetingExtraordinary(int? IdReunion)
        {
            var idDocenteSesion = Session.GetUsuarioId();
            //int? idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            //int idPeriodo = (int)SessionHelper.GetPeriodoAcademicoId(Session);
            int idSubModalidadPeriodo = (int)SessionHelper.GetSubModalidadPeriodoAcademicoId(Session);
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);

            //if (!GedServices.DocentePerteneceOrganigrama(context, idDocenteSesion, idPeriodo, escuelaId))
            if (!GedServices.DocentePerteneceOrganigrama(Context, idDocenteSesion, idSubModalidadPeriodo, idEscuela))
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoTienePermisosSuficientesParaCrearUnaReunionExtraordinaria);
                return RedirectToAction("MaintenanceMeetingExtraordinary");
            }

            Reunion reunion = null;
            if (IdReunion != null)
            {
                reunion = Context.Reunion.Find(IdReunion.Value);
            }
           
            if (reunion != null)
            {
                if (reunion.Acta.FirstOrDefault() != null)
                {
                    PostMessage(MessageType.Warning, " " + MessageResource.NoSePuedeEditarLaReunionYaTieneUnaActaAsociada);
                    return RedirectToAction("Index");
                }
                //acta = context.Reunion.Find(IdReunion.Value).Acta;
            }

            

            if (IdReunion.HasValue)
            {
                Int32 IdDocenteCreador = Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdDocente.Value;
                if (Session.GetDocenteId() != IdDocenteCreador)
                {
                    PostMessage(MessageType.Warning, MessageResource.LaEdicionEstaPermitidaAlDocenteCreadorDeLaReunion);
                    return RedirectToAction("Index");
                }
            }

            var viewmodel = new AddEditMeetingExtraordinaryViewModel();
            viewmodel.Fill(Context, Session, IdReunion);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddEditMExtraordinary(AddEditMeetingExtraordinaryViewModel model)
        {
            try
            {
                //VALIDACIONES PARA EL REGISTRO DE LA REUNIÓN
                if (String.IsNullOrEmpty(model.Motivo))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarUnMotivo);
                else if (model.Fecha.Date < DateTime.Now.Date)
                    PostMessage(MessageType.Error, MessageResource.LaFechaNoDebeSerAnterioraLaActual);
                else if (model.HoraInicio.Hour > model.HoraFin.Hour || (model.HoraInicio.Hour == model.HoraFin.Hour && model.HoraInicio.Minute >= model.HoraFin.Minute))
                    PostMessage(MessageType.Error, MessageResource.LasHorasDeInicioYfinNoConcuerdenEntreSi);
                else if (String.IsNullOrEmpty(model.lstTemas))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosTemas);
                else if (model.listaParticipantesNormales.Where(x => x.IsSelected == true).Count() == 1 && String.IsNullOrEmpty(model.lstExternos))
                    PostMessage(MessageType.Error, MessageResource.SeDebeIngresarLosParticipantes);
                else
                {
                    //LÓGICA PARA EL REGISTRO Y/O EDICIÓN DE LA REUNIÓN
                    //INSTANCIO EL OBJETO Y REVISO SI LO TENGO O NO
                    Reunion reunion;
                    if (model.IdReunion.HasValue)
                    {
                        //SI VOY A EDITAR ELIMINO LOS TEMAS Y PARTICIPANTES PARA PODER ACTUALIZAR
                        reunion = Context.Reunion.FirstOrDefault(x => x.IdReunion == model.IdReunion);
                        GedServices.EliminarTemasDeReunionService(Context, reunion.IdReunion);
                        GedServices.EliminarParticipantesReunionServices(Context, reunion.IdReunion);
                    }
                    else
                    {
                        reunion = new Reunion();
                        reunion.IdDocente = Session.GetDocenteId();
                        reunion.FechaRegistro = DateTime.Today.Date;
                    }
                    //---------------------------------------------
                    //ASIGNO LOS CAMPOS DE INFORMACIÓN GENERAL
                    reunion.Nivel = model.IdNivel;
                    reunion.IdUnidadAcademica = model.IdUnidadAcademica;
                    reunion.IdSede = model.IdSede;
                    reunion.Lugar = model.Lugar;
                    reunion.Fecha = model.Fecha;
                    reunion.HoraInicio = new DateTime(model.Fecha.Year, model.Fecha.Month, model.Fecha.Day, model.HoraInicio.Hour, model.HoraInicio.Minute, model.HoraInicio.Second);
                    reunion.HoraFin = new DateTime(model.Fecha.Year, model.Fecha.Month, model.Fecha.Day, model.HoraFin.Hour, model.HoraFin.Minute, model.HoraFin.Second);
                    reunion.Motivo = model.Motivo;
                    //reunion.IdSubModalidadPeriodoAcademico = Session.GetPeriodoAcademicoId();
                    reunion.IdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
                    var fechaInicioPeriodoAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == reunion.IdSubModalidadPeriodoAcademico).FechaInicioPeriodo;
                    reunion.Semana = ConvertHelpers.GetNumeroSemanaOfReunion(fechaInicioPeriodoAcademico.Value, reunion.Fecha);
                 
                    reunion.EsExtraordinaria = true;
                    reunion.IdEstadoReunion = 1;
                    reunion.Estado = "PROGRAMADO";
                    //---------------------------------------------
                    //SIN IdReunion: REGISTRO LA REUNIÓN; CON IdReunion: ACTUALIZO LA REUNIÓN
                    if (!model.IdReunion.HasValue)
                    {
                        Context.Reunion.Add(reunion);
                    }
                    else
                    {
                        Context.Entry(reunion).State = System.Data.Entity.EntityState.Modified;
                    }
                    Context.SaveChanges();
                    //---------------------------------------------
                    //REGISTRO LOS TEMAS UNA VEZ TENGA UNA REUNIÓN REGISTRADA O ID ASOCIADO
                    var auxlstaTemas = model.lstTemas.Split('*');
                    List<String> lstAuxTemas = new List<string>();
                    foreach (string item in auxlstaTemas)
                    {
                        lstAuxTemas.Add(item);
                    }
                    model.listaTemas = lstAuxTemas;
                    foreach (var item in model.listaTemas)
                    {
                        GedServices.CrearTemaDeReunionService(Context, item, reunion.IdReunion);
                    }
                    //---------------------------------------------
                    //REGISTRO LOS PARTICIPANTES NORMALES SELECCIONADOS
                    var usuario = model.listaParticipantesNormales.FirstOrDefault(x => x.IdParticipante == Session.GetDocenteId() && x.IdUnidadAcademica == model.IdUnidadAcademica);
                    var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true && x.IdParticipante != Session.GetDocenteId()).ToList();
                    //INCLUYO AL PARCITICIPANTE DE LA REUNION
                    //var participantesNormales = model.listaParticipantesNormales.Where(x => x.IsSelected == true).ToList();
                    
                    participantesNormales.Add(usuario);
                    
                    foreach (var participante in participantesNormales)
                    {
                        ParticipanteReunion objParticipante = new ParticipanteReunion();
                        objParticipante.IdReunion = reunion.IdReunion;
                        objParticipante.IdDocente = participante.IdParticipante;
                        objParticipante.Asistio = false;
                        objParticipante.EsExterno = false;
                        objParticipante.NombreCompleto = participante.Nombre;
                        objParticipante.IdUnidadAcademicaResponsable = participante.IdUnidadAcademicaResponsable;
                        objParticipante.Cargo = participante.UnidadAcademica;
                        Context.ParticipanteReunion.Add(objParticipante);
                        Context.SaveChanges();
                    }
                    //---------------------------------------------
                    //REGISTRO LOS PARTICIPANTES EXTERNOS
                    if (!String.IsNullOrEmpty(model.lstExternos))
                    {
                        var firstEncode = model.lstExternos.Split('*');
                        List<ParticipanteExterno> lstAuxExternos = new List<ParticipanteExterno>();
                        for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                        {
                            var data = firstEncode[i].Split('|');
                            lstAuxExternos.Add(new ParticipanteExterno
                            {
                                Nombre = data[j],
                                Cargo = data[j + 1],
                                Correo = data[j + 2],
                                IdEmpresa = ConvertHelpers.ToInteger(data[j + 3])
                            });
                        }
                    
                        foreach (var item in lstAuxExternos)
                        {
                            ParticipanteReunion objParticipanteExterno = new ParticipanteReunion();
                            objParticipanteExterno.IdReunion = reunion.IdReunion;
                            objParticipanteExterno.Asistio = false;
                            objParticipanteExterno.EsExterno = true;
                            objParticipanteExterno.NombreCompleto = item.Nombre;
                            objParticipanteExterno.Cargo = item.Cargo;
                            objParticipanteExterno.Correo = item.Correo;
                            objParticipanteExterno.EmpresaId = item.IdEmpresa;
                            Context.ParticipanteReunion.Add(objParticipanteExterno);
                            Context.SaveChanges();
                        }
                    }
                    //---------------------------------------------
                    PostMessage(MessageType.Success);
                    return RedirectToAction("Index", "Meeting");
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddEditMeetingExtraordinary", "Meeting", new { IdReunion = model.IdReunion });
        }
        
        public ActionResult DetailsMeetingExtraordinary(int IdReunion)
        {
            Int32 IdDocenteCreador = Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdDocente.Value;
            Int32 nivelDocenteCreador = GedServices.GetNivelMaximoDocenteServices(Context, IdDocenteCreador, EscuelaId);
            Int32 nivelDocenteLogueado = GedServices.GetNivelMaximoDocenteServices(Context, Session.GetDocenteId().Value, EscuelaId);
            var lstaParticipantes = Context.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && !x.EsExterno).ToList();
            if(IdDocenteCreador == Session.GetDocenteId() || lstaParticipantes.Exists(x => x.IdDocente == Session.GetDocenteId()) || nivelDocenteCreador > nivelDocenteLogueado)
            {
                var model = new AddEditMeetingExtraordinaryViewModel();
                model.Fill(Context, Session, IdReunion);
                return View(model);
            }
            else
            {
                PostMessage(MessageType.Warning, MessageResource.NoTienePermisoParaConsultarEstaReunion);
                return RedirectToAction("Index");
            }
        }
        
        public ActionResult DetailsMeetingPreSchedule(int IdPreAgenda, int IdReunion)
        {
            Int32 IdDocenteCreador = Context.Reunion.FirstOrDefault(x => x.IdReunion == IdReunion).IdDocente.Value;
            Int32 nivelDocenteCreador = GedServices.GetNivelMaximoDocenteServices(Context, IdDocenteCreador, EscuelaId);
            Int32 nivelDocenteLogueado = GedServices.GetNivelMaximoDocenteServices(Context, Session.GetDocenteId().Value, EscuelaId);
            var lstaParticipantes = Context.ParticipanteReunion.Where(x => x.IdReunion == IdReunion && !x.EsExterno).ToList();

            if (IdDocenteCreador == Session.GetDocenteId() || lstaParticipantes.Exists(x => x.IdDocente == Session.GetDocenteId()) || nivelDocenteCreador > nivelDocenteLogueado)
            {
                var viewmodel = new AddEditMeetingPreScheduleViewModel();
                viewmodel.Fill(Context, Session, IdPreAgenda, IdReunion);
                return View(viewmodel);
            }
            else
            {
                PostMessage(MessageType.Warning, MessageResource.NoTienePermisoParaConsultarEstaReunion);
                return RedirectToAction("IndexPreSchedule");
            }
        }

        [HttpPost]
        public ActionResult DeleteReunion(int id)
        {
            if (GedServices.EliminarReunionServices(Context, id))
                return Json(true);
            else
                return Json(false);
        }

        [HttpPost]
        public ActionResult CrearEmpresa(string razon, string ruc, string email)
        {
            bool rucRepetido = (Context.Empresa.FirstOrDefault(x => x.RUC == ruc) == null) ? false : true;
            var empresa = new Empresa();

            if (!rucRepetido)
            {
                empresa.RazonSocial = razon;
                empresa.RUC = ruc;
                empresa.EmailContacto = email;
                GedServices.CrearEmpresaServices(Context, empresa);
            }

            return Json(new { lista = new SelectList(GedServices.GetAllEmpresasServices(Context), "IdEmpresa", "RazonSocial"), flag = rucRepetido, idEmp = empresa.IdEmpresa });
        }

        [HttpPost]
        public ActionResult DeleteEmpresa(int id)
        {
            try
            {
                var empresa = Context.Empresa.FirstOrDefault(x => x.IdEmpresa == id);
                Context.Empresa.Remove(empresa);
                Context.SaveChanges();
                
                return Json(new SelectList(GedServices.GetAllEmpresasServices(Context), "IdEmpresa", "RazonSocial"));
            }
            catch (Exception)
            {

            }
            return Json(false);
        }

        public ActionResult MeetingsAssociated(Int32? idPreAgenda)
        {
            MeetingsAssociatedViewModel ViewModel = new MeetingsAssociatedViewModel();
            Int32 IdPreAgendaCreador = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == idPreAgenda.Value).IdUsuarioCreador.Value;

            if (Session.GetDocenteId() != IdPreAgendaCreador)
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoPuedeCrearReunionSobreUnaPreagendaDeOtroUsuario + " ");
                return RedirectToAction("IndexPreSchedule");
            }
            ViewModel.PreAgenda = GedServices.GetPreAgendaServices(Context, idPreAgenda.Value);
            ViewModel.CargarDatos(CargarDatosContext(), idPreAgenda);

            return View(ViewModel);
        }

        public ActionResult Create()
        {

            var viewmodel = new CreateACCVM();
            viewmodel.Fill(CargarDatosContext(), Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            var sede = Context.Sede.ToList();

            ViewBag.Sede = sede.Select(x => new SelectListItem()
            {
                Value = x.IdSede.ToString(),
                Text = x.Nombre
            }).ToList();

            var sessionIdEscuela = Session.GetEscuelaId();
            var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();
            return View(viewmodel);
        }
        public ActionResult RemoveFinding(Int32  IdAcc, Int32 IdEmployer, Int32 IdHallazgo) {
            try
            {
                ActaComiteConsultivoXEmpleador axe = Context.ActaComiteConsultivoXEmpleador
                                                    .FirstOrDefault(x => x.IdActaReunion == IdAcc &&
                                                                    x.Id_Empleador == IdEmployer &&
                                                                    x.IdHallazgo == IdHallazgo);

                axe.IsDeleted = true;
                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }

            return RedirectToAction("EditACC", "Meeting", new { IdACC = IdAcc });
        }
        public ActionResult DeleteACC(Int32 IdACC)
        {
            try
            {
                ActaComiteConsultivo acc = Context.ActaComiteConsultivo.FirstOrDefault(x => x.IdActaReunion == IdACC);
                acc.IsDeleted = true;                               
                Context.SaveChanges();
                PostMessage(MessageType.Success);

            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }

            return RedirectToAction("LstActaComiteConsultivo", "Meeting");
        }

        [HttpGet]
        public ActionResult _AddEditEmployer(string idACC , string FechaACC, string userACC)
        {
            
            //var viewmodel = new CreateACCVM();
            //viewmodel.Fill(CargarDatosContext(), Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            //var sede = context.Sede.ToList();

            //ViewBag.Sede = sede.Select(x => new SelectListItem()
            //{
            //    Value = x.IdSede.ToString(),
            //    Text = x.Nombre
            //}).ToList();

            //var sessionIdEscuela = Session.GetEscuelaId();
            //var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();

            var sl = new List<SelectListEmployer>();
            sl.Add(new SelectListEmployer() { Value = "0 - 20", Text = "0 - 20" });
            sl.Add(new SelectListEmployer() { Value = "20 - 50", Text = "20 - 50" });
            sl.Add(new SelectListEmployer() { Value = "50 - 100", Text = "50 - 100" });
            sl.Add(new SelectListEmployer() { Value = "100 - más", Text = "100 - más" });


            ViewBag.Employers = sl.Select(x => new SelectListItem()
            {
                Value = x.Value,
                Text = x.Text
            }).ToList();
            return PartialView("_AddEditEmployer");
        }
        [HttpPost]
        public ActionResult _AddEditEmployer(EmployerVM vm)
        {
            //var viewmodel = new CreateACCVM();
            //viewmodel.Fill(CargarDatosContext(), Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            //var sede = context.Sede.ToList();

            //ViewBag.Sede = sede.Select(x => new SelectListItem()
            //{
            //    Value = x.IdSede.ToString(),
            //    Text = x.Nombre
            //}).ToList();

            //var sessionIdEscuela = Session.GetEscuelaId();
            //var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();

            var sl = new List<SelectListEmployer>();
            sl.Add(new SelectListEmployer() { Value = "0 - 20", Text = "0 - 20" });
            sl.Add(new SelectListEmployer() { Value = "20 - 50", Text = "20 - 50" });
            sl.Add(new SelectListEmployer() { Value = "50 - 100", Text = "50 - 100" });
            sl.Add(new SelectListEmployer() { Value = "100 - más", Text = "100 - más" });
            

            ViewBag.Employers = sl.Select(x => new SelectListItem()
            {
                Value = x.Value,
                Text = x.Text
            }).ToList();
            
            vm.Fill(CargarDatosContext(),vm.idACC);
            return RedirectToAction("AddViewEmployer",vm);
            //return View();
        }
        [HttpPost]
        public ActionResult Create(CreateACCVM model)
        {
            var user = Session.GetUsuarioId();
            var acc = new ActaComiteConsultivo();
            model.CicloAcademico = Session.GetPeriodoAcademico();
            acc.Codigo = "ACT-CC-" + model.CicloAcademico;
            model.CodigoACC = acc.Codigo;
            //var time = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
            //var date = model.Fecha + " " + time;
            //model.Fecha = date;

            acc.Fecha = model.Fecha.ToDateTime();
            model.userACC = user.ToInteger();
            acc.IdUsuario = model.userACC;
            acc.Sede = model.idSede;
            acc.Escuela = model.IdEscuela;
            model.IdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId().ToInteger();
            acc.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico;


            var IsNull = Context.ActaComiteConsultivo.ToList();
            if (IsNull.Count == 0)
            {
                Context.ActaComiteConsultivo.Add(acc);
                Context.SaveChanges();
            }
            else
            {


                var ExistAcc = Context.ActaComiteConsultivo.Select(x => x.Fecha == acc.Fecha.Date
                                                                && x.Codigo == acc.Codigo
                                                                && x.IdUsuario == acc.IdUsuario
                                                                && x.Sede == acc.Sede
                                                                && x.Escuela == acc.Escuela
                                                                && x.IdSubModalidadPeriodoAcademico == acc.IdSubModalidadPeriodoAcademico);
                if (ExistAcc.First() == false)
                {
                    Context.ActaComiteConsultivo.Add(acc);
                    Context.SaveChanges();
                }
            }


            return RedirectToAction("AddViewEmployer", model);
        }
        private class SelectListEmployer
        {
            public string Value { get; set; }
            public string Text { get; set; }
        }
        [HttpGet]
        public ActionResult LstActaComiteConsultivo(int? NumeroPagina) {
            var model = new ACCVM();
            model.fill(CargarDatosContext(), NumeroPagina);
            return View(model);
        }

        public ActionResult EditACC(Int32? IdACC)
        {
            EditACCxEmployerVM model = new EditACCxEmployerVM();

            int ModalidadId = session.GetModalidadId();

            var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == ModalidadId).IdSubModalidadPeriodoAcademico;

            model.Fill(CargarDatosContext(), IdACC);
            return View(model);
        }

        [HttpGet]
        public ActionResult AddViewEmployer(CreateACCVM model, EmployerVM vm)
        {
            var employer = new EmployerVM();
            if (vm.Nombre == null)
            {
                var accID = Context.ActaComiteConsultivo.ToList().Where(x => x.Fecha == model.Fecha.ToDateTime()
                                                                 && x.Codigo == model.CodigoACC
                                                                 && x.IdUsuario == model.userACC
                                                                 && x.Sede == model.idSede
                                                                 && x.Escuela == model.IdEscuela
                                                                 && x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico
                                                                 ).FirstOrDefault().IdActaReunion;
                vm.idACC = accID;
                vm.FechaACC = model.Fecha.ToDateTime();
                vm.userACC = model.userACC;
            }

            vm.Fill(CargarDatosContext(), vm.idACC);

            var sl = new List<SelectListEmployer>();
            sl.Add(new SelectListEmployer() { Value = "0 - 20", Text = "0 - 20" });
            sl.Add(new SelectListEmployer() { Value = "20 - 50", Text = "20 - 50" });
            sl.Add(new SelectListEmployer() { Value = "50 - 100", Text = "50 - 100" });
            sl.Add(new SelectListEmployer() { Value = "100 - más", Text = "100 - más" });


            ViewBag.Employers = sl.Select(x => new SelectListItem()
            {
                Value = x.Value,
                Text = x.Text
            }).ToList();
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddViewEmployer(EmployerVM viewmodel)
        {
            if (ModelState.IsValid)
            {
               int idEmployer=  RegistrarEmpleador(viewmodel);
                int IdHallazgo = RegistrarHallazgo(viewmodel);
                var idsede = Context.ActaComiteConsultivo.Where(x => x.IdActaReunion == viewmodel.idACC).FirstOrDefault().Sede;



                var accE = new ActaComiteConsultivoXEmpleador();

                
                accE.IdActaReunion = viewmodel.idACC;
                accE.Id_Empleador = idEmployer;
                accE.IdHallazgo = IdHallazgo;
                Context.ActaComiteConsultivoXEmpleador.Add(accE);
                Context.SaveChanges();
                viewmodel.Fill(CargarDatosContext(), viewmodel.idACC);
                /////
                var sl = new List<SelectListEmployer>();
                sl.Add(new SelectListEmployer() { Value = "0 - 20", Text = "0 - 20" });
                sl.Add(new SelectListEmployer() { Value = "20 - 50", Text = "20 - 50" });
                sl.Add(new SelectListEmployer() { Value = "50 - 100", Text = "50 - 100" });
                sl.Add(new SelectListEmployer() { Value = "100 - más", Text = "100 - más" });


                ViewBag.Employers = sl.Select(x => new SelectListItem()
                {
                    Value = x.Value,
                    Text = x.Text
                }).ToList();

            }
            return View(viewmodel);
        }
        public int RegistrarEmpleador(EmployerVM model)
        {

            var em = new Empleador();
            em.Nombre = model.Nombre;
            em.Apellido = model.Apellido;
            em.DNI = model.DNI;
            em.Cargo = model.Cargo;
            em.Empresa = model.Empresa;
            if (!String.IsNullOrEmpty(model.Telefono.ToString()))
            { em.Telefono = model.Telefono; }
            if (!String.IsNullOrEmpty(model.RUC))
            { em.RUC = model.RUC; }
            em.N_Empleados = model.N_Empleados;

            Context.Empleador.Add(em);
            Context.SaveChanges();
            return em.Id_Empleador;

        }
        public int RegistrarHallazgo(EmployerVM viewmodel)
        {

            var idsede = Context.ActaComiteConsultivo.Where(x => x.IdActaReunion == viewmodel.idACC).FirstOrDefault().Sede;
            var ha = new Hallazgo();
            ha.Codigo = "ACT-CC-" + Session.GetPeriodoAcademico();
            ha.DescripcionEspanol = viewmodel.ComentarioEspanol;
            ha.DescripcionIngles = viewmodel.ComentarioIngles;
            ha.IdConstituyente = 3;
            ha.IdInstrumento = 2;
            ha.IdSede = idsede;
            ha.FechaRegistro = (DateTime.Now.ToDateTime());
            ha.IdCriticidad = 3;
            Context.Hallazgo.Add(ha);
            Context.SaveChanges();
            return ha.IdHallazgo;

        }

        public JsonResult GetAllUnidadPorNivel(int IdDocente, int IdNivel, int IdPeriodoAcademico)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var unidades = GedServices.GetUnidadAcademicaNivelServices(Context, IdDocente, IdNivel, IdPeriodoAcademico, EscuelaId);
            var items = unidades.Select(x => new SelectListItem { Value = x.IdUnidadAcademica.ToString(), Text = x.NombreEspanol });
            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PRUEBACORREO()
        {
            bool temp = false;
            try
            {
                SendMessageLogic sc = new SendMessageLogic();
                var PlantillaEmail = Context.Plantilla.FirstOrDefault();
                sc.FromAddress = ConfigurationManager.AppSettings["AbetEmailCredentialUser"];
                sc.toAddress = "vjrojasb@gmail.com";
                sc.Topic = "GED: Reunión de Coordinación";
                sc.nombreEvento = "Reunión de Nivel 1";
                sc.descripcionEvento = "Se hablarán de los siguientes Temas: - Tema 1: Tema 2:";
                sc.ubicacion = "Sotano del E";
                sc.fechaInicio = DateTime.Now.AddHours(1);
                sc.fechaFin = DateTime.Now.AddHours(3);
                sc.lstCC.Add("vjrojasb@gmail.com");
                sc.lstCC.Add("u201112105@upc.edu.pe");
                sc.lstCC.Add("u201210391@upc.edu.pe");
                sc.lstCC.Add("rondanrichard@gmail.com");
                sc.lstCC.Add("victor.parasi@upc.edu.pe");
                var temporal = sc.SendEmail();
                temp = temporal.Item2;

            }
            catch (Exception ex)
            {
                return View(temp);
            }
            return View(temp);
        }
    }
}