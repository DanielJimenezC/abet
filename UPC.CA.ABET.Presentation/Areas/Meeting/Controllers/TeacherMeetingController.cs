﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Meeting;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.TeacherMeeting;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using static UPC.CA.ABET.Helpers.ConstantHelpers.TEACHER_MEETING.ESTADO;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.TeacherMeeting;
using System.Globalization;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using System.Transactions;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Ionic.Zip;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [RouteArea("Meeting")]
    [RoutePrefix("TeacherMeeting")]
    public class TeacherMeetingController : BaseController
    {
        public TeacherMeetingLogic TeacherMeetingLogic { get; set; }
        const string LogoUpc = "logo-upc-red.png";
        public string CurrentCulture { get; set; }
        public TeacherMeetingController()
        {
            TeacherMeetingLogic = new TeacherMeetingLogic();
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult Index(TeacherMeetingManagementViewModel viewModel, int p = 1, int teacherMeetingId = 0)
        {
            viewModel.LoadMeetingsByTeacher(CargarDatosContext(), p, DocenteId, SubModalidadPeriodoAcademicoId.Value);
            if (teacherMeetingId != 0)
                ViewBag.IdReunionActaCompletada = teacherMeetingId;
            return View(viewModel);
        }

        //[AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult Management(int? subModalityAcademicPeriodId)
        {
            var viewModel = new TeacherMeetingManagementViewModel();
            var parsedModalityId = int.Parse(ModalityId);
            viewModel.LoadModules(Context, parsedModalityId, subModalityAcademicPeriodId, SubModalidadPeriodoAcademicoId);
            return View(viewModel);
        }

        public JsonResult CheckIfMeetingsAreAlreadyCreated(int? moduleId)
        {
            var viewModel = new TeacherMeetingManagementViewModel();
            var OrganizationChartAlreadyUploaded = viewModel.OrganizationChartUploaded(CargarDatosContext(), SubModalidadPeriodoAcademicoId);
            var MeetingsAlreadyCreated = viewModel.MeetingsCreated(CargarDatosContext(), SubModalidadPeriodoAcademicoId, moduleId);
            return Json(new { OrganizationChartAlreadyUploaded, MeetingsAlreadyCreated }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> CrearReunionProfesores(int moduleId)
        {
            var result = await TeacherMeetingLogic.CreateTeacherMeetingWithGuests(SubModalidadPeriodoAcademicoId, moduleId, EscuelaId);
            if (!result.Item1)
            {
                var message = result.Item2 + " " + MessageResource.NoSeRealizoNingunCambio;
                return Json(new { status = "FAILED", message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "OK", message = result.Item2 }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult Configuration()
        {
            if (!SubModalidadPeriodoAcademicoId.HasValue)
                return RedirectToAction("Index", "Home");

            var viewModel = new TeacherMeetingsConfigurationViewModel();
            viewModel.LoadAllData(SubModalidadPeriodoAcademicoId.Value);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult RemoveConfiguration(int smpamId)
        {
            var configurationsToDelete = Context.ReunionProfesorConfiguracion.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == smpamId);
            Context.ReunionProfesorConfiguracion.RemoveRange(configurationsToDelete);
            return Json(new { success = true, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public async Task<ActionResult> Configuration(TeacherMeetingsConfigurationViewModel viewModel)
        {
            if (!SubModalidadPeriodoAcademicoId.HasValue)
                return RedirectToAction("Index", "Home");

            var success = await viewModel.CreateTeacherMeetingConfiguration(viewModel.SubModalityAcademicPeriodModuleId, viewModel.FirstLevelWeeks, viewModel.SecondLevelWeeks, viewModel.FirstLevelDueDate, viewModel.SecondLevelDueDate);
            if (success)
                PostMessage(MessageType.Success, MessageResource.SeGuardaronLosCambiosSatisfactoriamente);
            else
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError);
            return Json(new { redirectTo = Url.Action("Configuration") }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWeeksForSubModalityAcademicPeriodModule(int smpamId)
        {
            var viewModel = new TeacherMeetingsConfigurationViewModel();
            viewModel.LoadData(smpamId);
            var weeks = viewModel.ModuleWeeks;
            return Json(new { weeks }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        [Route("{teacherMeetingId:int}/Minute")]
        public ActionResult ActaReunionProfesor(int teacherMeetingId = 0, string culture = "es-PE")
        {
            var viewModel = new TeacherMeetingViewModel();
            viewModel.LoadData(teacherMeetingId);
            if (viewModel.ReunionProfesor == null)
                return RedirectToAction("NotFound", "Error");

            if (!viewModel.ReunionProfesor.ActaCreada)
            {
                PostMessage(MessageType.Error, MessageResource.ElActaDeReunionDeProfesoresAunNoFueCreada);
                return RedirectToAction("Index");
            }
            return RedirectToAction("GetActaPdf", new { teacherMeetingId, culture });
        }

        [AuthorizeUser(AppRol.Administrador)]
        public ActionResult DescargaMasivaActas(string culture = "es-PE")
        {
            var teachersMeetingsId = Context.ReunionProfesor.Where(x => x.ActaCreada == true).ToList();
            string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, "PDF");
            Directory.CreateDirectory(directory);
            var carpeta = "";
            var ZipFile = (culture == "es-PE" ? "DescargaMasivaActas.zip" : "MassDownloadMinutes.zip");
            using (var memoryStream = new MemoryStream())
            {
                using (var archive = new ZipFile())
                {
                    for(int i = 0; i < teachersMeetingsId.Count(); i++)
                    {
                        var now = DateTime.Now;
                        var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
                        try
                        {
                            ExportTeacherMeetingLogic pdfExport = new ExportTeacherMeetingLogic(Context, teachersMeetingsId[i].IdReunionProfesor, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, culture);
                            string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                            
                            if(path.Contains("EPE"))
                                carpeta = (culture == "es-PE" ? "PregradoEPE" : "UndergraduateEPE");
                            else
                                carpeta = (culture == "es-PE" ? "PregradoRegular" : "UndergraduateRegular");

                            archive.AddFile(path, carpeta + "/" + pdfExport.Area);
                        }
                        catch(Exception ex)
                        {
                            var newErrorLog = new LogErrores
                            {
                                Detail = ex.Message,
                                FechaCreacion = formatDateTimeNow.ToDateTime()
                            };
                            Context.LogErrores.Add(newErrorLog);
                            Context.SaveChanges();
                            PostMessage(Helpers.MessageType.Error, MessageResource.OcurrioUnErrorExportarActaReunion + "   " + ex.ToString());
                            return RedirectToAction("Consult");
                        }
                    }
                    archive.Save(Response.OutputStream);
                    Directory.Delete(directory, true);
                }
                return File(memoryStream.ToArray(), "application/zip", ZipFile);
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        [Route("{teacherMeetingId:int}")]
        public ActionResult NewTeacherMeetingMinute(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingViewModel();
            viewModel.LoadData(teacherMeetingId);
            if (viewModel.ReunionProfesor == null)
                return RedirectToAction("NotFound", "Error");

            if (!viewModel.ReunionProfesor.Notificada)
            {
                PostMessage(MessageType.Info, TeacherMeetingResource.EsNecesarioNotificarPrimero);
                return RedirectToAction("Index");
            }

            if (!viewModel.IsResponsibleOrAdmin(teacherMeetingId, SubModalidadPeriodoAcademicoId.Value, Session.GetRoles(), DocenteId.Value))
            {
                PostMessage(MessageType.Info, MessageResource.EsNecesarioSerEncargadoDeLaReunionParaPoderVisualizarla);
                return RedirectToAction("Index");
            }

            if (viewModel.ReunionProfesor.ActaCreada)
            {
                PostMessage(MessageType.Error, MessageResource.UnaVezActaEsCreadaNoEstanPermitidosLosCambios);
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        [HttpGet]
        public ActionResult _AttendanceTab(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingParticipantViewModel();
            viewModel.LoadAttendance(teacherMeetingId);
            return PartialView(viewModel);
        }
        
        public ActionResult UpdateTeachers(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingParticipantViewModel();
            viewModel.LoadAttendance(teacherMeetingId);
            return PartialView("_TableAttendanceTab", viewModel);
        }
        public JsonResult UpdateDataTableTeacher(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingParticipantViewModel();
            viewModel.LoadAttendance(teacherMeetingId);
            return Json(viewModel.ProfesorParticipanteInvitados);
        }

        public ActionResult DeleteTeacher(int guestTeacherId = 0)
        {
            var language = Session.GetCulture().ToString();
            try
            {
                var guestTeacher = Context.ReunionProfesorParticipanteInvitado.Find(guestTeacherId);
                Context.ReunionProfesorParticipanteInvitado.Attach(guestTeacher);
                Context.ReunionProfesorParticipanteInvitado.Remove(guestTeacher);
                Context.SaveChanges();
                if (language == "es-PE")
                    return Json(new { success = true, title = "¡Éxito!", message = "Profesor invitado elimnado exitosamente." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, title = "¡Success!", message = "Guest teachers successfully removed" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                if (language == "es-PE")
                    return Json(new { success = false, title = "¡Error!", message = "Ocurrio un Error: " + e.Message }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, title = "¡Error!", message = "Ocurrio un Error: " + e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult _OperationalReportTab(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingOperationalReportViewModel();
            viewModel.LoadOperationalReports(teacherMeetingId);
            return PartialView(viewModel);
        }

        public ActionResult _PreviousTasksTab(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingPreviousTaskViewModel();
            viewModel.LoadData(teacherMeetingId);
            return PartialView(viewModel);
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult MarkAttendance(int teacherMeetingParticipantId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var teacherMeetingParticipant = Context.ReunionProfesorParticipante.FirstOrDefault(x => x.IdReunionProfesorParticipante == teacherMeetingParticipantId);
                teacherMeetingParticipant.Asistio = !teacherMeetingParticipant.Asistio;
                teacherMeetingParticipant.FechaActualizacion = dateFormatted.ToDateTime();
                Context.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime(),
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult UpdateSilabus(int teacherMeetingOperationalReportId, string newValue, string language)
        {
            YandexTranslatorAPIHelper yandexTranslatorAPIHelper = new YandexTranslatorAPIHelper();
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var teacherMeetingOperationalReport = Context.ReunionProfesorReporteOperativo.FirstOrDefault(x => x.IdReunionProfesorReporteOperativo == teacherMeetingOperationalReportId);
                var translation = "";
                if (language == "es-PE")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue);
                    teacherMeetingOperationalReport.AvanceSilabo = newValue;
                    teacherMeetingOperationalReport.AvanceSilaboIngles = translation;

                } else if (language == "en-US")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue, "en-es");
                    teacherMeetingOperationalReport.AvanceSilaboIngles = newValue;
                    teacherMeetingOperationalReport.AvanceSilabo = translation;

                }
                teacherMeetingOperationalReport.FechaActualizacion = dateFormatted.ToDateTime();
                Context.SaveChanges();
                return Json(new { translation }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult UpdateVirtualUse(int teacherMeetingOperationalReportId, string newValue, string language)
        {
            YandexTranslatorAPIHelper yandexTranslatorAPIHelper = new YandexTranslatorAPIHelper();
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var teacherMeetingOperationalReport = Context.ReunionProfesorReporteOperativo.FirstOrDefault(x => x.IdReunionProfesorReporteOperativo == teacherMeetingOperationalReportId);
                var translation = "";
                if (language == "es-PE")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue);
                    teacherMeetingOperationalReport.UsoAulaVirtual = newValue;
                    teacherMeetingOperationalReport.UsoAulaVirtualIngles = translation;

                }
                else if (language == "en-US")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue, "en-es");
                    teacherMeetingOperationalReport.UsoAulaVirtualIngles = newValue;
                    teacherMeetingOperationalReport.UsoAulaVirtual = translation;

                }
                teacherMeetingOperationalReport.FechaActualizacion = dateFormatted.ToDateTime();
                Context.SaveChanges();
                return Json(new { translation }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult UpdateImprovementActionProgress(int tmpId, string newValue, int teacherMeetingId, string language, bool targetOnly = true)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            YandexTranslatorAPIHelper yandexTranslatorAPIHelper = new YandexTranslatorAPIHelper();
            try
            {
                var improvementActionTeacherMeeting = Context.ReunionProfesorAccionMejora.FirstOrDefault(x => x.IdReunionProfesorAccionMejora == tmpId);
                var translation = "";
                if (language == "spanish")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue);
                    improvementActionTeacherMeeting.ComentarioCierreEspanol = newValue;
                    if (!targetOnly)
                        improvementActionTeacherMeeting.ComentarioCierreIngles = translation;

                } else if (language == "english")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue, "en-es");
                    improvementActionTeacherMeeting.ComentarioCierreIngles = newValue;
                    if (!targetOnly)
                        improvementActionTeacherMeeting.ComentarioCierreEspanol = translation;
                }
                improvementActionTeacherMeeting.IdReunionProfesor = teacherMeetingId;
                improvementActionTeacherMeeting.FechaActualizacion = dateFormatted.ToDateTime();

                Context.SaveChanges();
                return Json(new { translation }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult ReleaseImprovementAction(int tmpId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var improvementActionTeacherMeeting = Context.ReunionProfesorAccionMejora.FirstOrDefault(x => x.IdReunionProfesorAccionMejora == tmpId);
                improvementActionTeacherMeeting.ComentarioCierreEspanol = null;
                improvementActionTeacherMeeting.ComentarioCierreIngles = null;
                improvementActionTeacherMeeting.IdReunionProfesor = null;
                improvementActionTeacherMeeting.FechaActualizacion = dateFormatted.ToDateTime();
                Context.SaveChanges();
                return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult UpdatePreviousAgreementProgress(int tmpId, string newValue, int teacherMeetingId, string language, bool targetOnly = true)
        {
            YandexTranslatorAPIHelper yandexTranslatorAPIHelper = new YandexTranslatorAPIHelper();
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var historicAgreement = Context.ReunionProfesorAcuerdoHistorico.FirstOrDefault(x => x.IdReunionProfesorAcuerdo == tmpId && x.IdUltimaReunionProfesor == teacherMeetingId);
                var agreement = Context.ReunionProfesorAcuerdo.FirstOrDefault(x => x.IdReunionProfesorAcuerdo == tmpId);
                var translation = "";

                if (language == "spanish")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue);
                    agreement.ComentarioCierreEspanol = newValue;
                    if (!targetOnly)
                        agreement.ComentarioCierreIngles = translation;
                    if (historicAgreement != null)
                    {
                        historicAgreement.ComentarioCierreEspanol = newValue;
                        if (!targetOnly)
                            historicAgreement.ComentarioCierreIngles = translation;
                        historicAgreement.FechaActualizacion = dateFormatted.ToDateTime();
                    }
                } else if (language == "english")
                {
                    translation = yandexTranslatorAPIHelper.Translate(newValue, "en-es");
                    agreement.ComentarioCierreIngles = newValue;
                    if (!targetOnly)
                        agreement.ComentarioCierreEspanol = translation;
                    if (historicAgreement != null)
                    {
                        historicAgreement.ComentarioCierreIngles = newValue;
                        if (!targetOnly)
                            historicAgreement.ComentarioCierreEspanol = translation;
                        historicAgreement.FechaActualizacion = dateFormatted.ToDateTime();
                    }
                }
                agreement.FechaActualizacion = dateFormatted.ToDateTime();
                Context.SaveChanges();
                return Json(new { translation }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public JsonResult CompleteTask(int tmpId, int teacherMeetingId)
        {
            var dateOfCompletion = DateTime.Now;
            var dateFormatted = dateOfCompletion.ToString("yyyy-MM-dd HH:mm:ss");
            //var dateOfCompletion = new DateTime(2019, 4, 10, 15, 0, 0);
            try
            {
                var teacherMeeting = Context.ReunionProfesor.FirstOrDefault(x => x.IdReunionProfesor == teacherMeetingId);
                if (teacherMeeting == null) return Json(new { status = false, message = MessageResource.LaReunionNoExiste });

                var historicTask = Context.ReunionProfesorTareaHistorico.FirstOrDefault(x => x.IdReunionProfesorTarea == tmpId && x.IdUltimaReunionProfesor == teacherMeetingId);
                var task = Context.ReunionProfesorTarea.FirstOrDefault(x => x.IdReunionProfesorTarea == tmpId);

                if (!task.FechaDeadline.HasValue) return Json(new { status = false, message = TeacherMeetingResource.FaltaFechaDeadline });

                if (task.FechaDeadline.Value < dateOfCompletion) return Json(new { status = false, message = TeacherMeetingResource.NoPuedeCerrarTareaVencida });

                
                var updateToStatusId = (int)TAREA.COMPLETADA;
                if (historicTask != null)
                {
                    historicTask.FechaCierre = dateFormatted.ToDateTime();
                    historicTask.IdEstado = updateToStatusId;
                }
                task.IdEstado = updateToStatusId;
                task.FechaCierre = dateOfCompletion.ToDateTime();
                task.IdReunionProfesorCompletada = historicTask.IdUltimaReunionProfesor;
                task.FechaActualizacion = dateFormatted.ToDateTime();

                Context.SaveChanges();
                return Json(new { status = true, message = TeacherMeetingResource.TareaMarcadaComoCompletadad }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public JsonResult UncompleteTask(int tmpId, int teacherMeetingId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            //var now = new DateTime(2019, 4, 10, 15, 0, 0);
            try
            {
                var teacherMeeting = Context.ReunionProfesor.FirstOrDefault(x => x.IdReunionProfesor == teacherMeetingId);
                if (teacherMeeting == null) return Json(new { status = false, message = MessageResource.LaReunionNoExiste });

                var historicTask = Context.ReunionProfesorTareaHistorico.FirstOrDefault(x => x.IdReunionProfesorTarea == tmpId && x.IdUltimaReunionProfesor == teacherMeetingId);
                var task = Context.ReunionProfesorTarea.FirstOrDefault(x => x.IdReunionProfesorTarea == tmpId);

                if (!task.FechaDeadline.HasValue) return Json(new { status = false, message = TeacherMeetingResource.FaltaFechaDeadline });

                if (task.FechaDeadline.Value < now) return Json(new { status = false, message = TeacherMeetingResource.NoPuedeCerrarTareaVencida });

                var taskCompletedId = (int)TAREA.PENDIENTE;
                if (historicTask != null)
                {
                    historicTask.FechaCierre = null;
                    historicTask.IdEstado = taskCompletedId;
                }
                task.IdEstado = taskCompletedId;
                task.FechaCierre = null;
                task.IdReunionProfesorCompletada = null;
                task.FechaActualizacion = dateFormatted.ToDateTime();

                Context.SaveChanges();
                return Json(new { status = true, message = TeacherMeetingResource.VolverTareaAPendiente }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _FindingsTab(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingFindingViewModel
            {
                IdReunionProfesor = teacherMeetingId
            };
            viewModel.LoadTeacherMeetingFindings(teacherMeetingId);
            return PartialView(viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _AddTeacherMeetingFinding(int teacherMeetingId)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var viewModel = new TeacherMeetingFindingViewModel
            {
                IdReunionProfesor = teacherMeetingId
            };

            viewModel.CargarData(Context, teacherMeetingId, ModalidadId, SubModalidadPeriodoAcademicoId, EscuelaId, CurrentCulture);

            return PartialView(viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _EditTeacherMeetingFinding(int teacherMeetingFindingId)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var viewModel = new TeacherMeetingFindingViewModel();

            var modalityId = Session.GetModalidadId();
            var idSubmodalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();

            viewModel.CargarDataForEdit(Context, teacherMeetingFindingId, modalityId, idSubmodalidadPeriodoAcademico, EscuelaId, CurrentCulture);

            return PartialView(viewModel);
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _EditTeacherMeetingFinding(TeacherMeetingFindingViewModel viewModel)
        {
            var result = viewModel.UpdateTeacherMeetingFinding(viewModel);
            if (!result)
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError);
            PostMessage(MessageType.Success, MessageResource.HallazgoActualizadoCorrectamente);
            return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId = viewModel.IdReunionProfesor });
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _AddTeacherMeetingFinding(TeacherMeetingFindingViewModel viewModel)
        {
            var newFinding = viewModel.AddTeacherMeetingFinding(viewModel);
            if (newFinding == null)
                PostMessage(MessageType.Error, MessageResource.OcurrioUnError);
            PostMessage(MessageType.Success, MessageResource.HallazgoCreadoCorrectamente);
            return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId = viewModel.IdReunionProfesor });
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _AgreementsTab(int teacherMeetingId = 0)
        {
            var viewModel = new TeacherMeetingAgreementViewModel
            {
                IdReunionProfesor = teacherMeetingId
            };
            viewModel.LoadTeacherMeetingAgreementsByTeacherMeetingId(teacherMeetingId);
            return PartialView(viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _AddTeacherMeetingAgreement(int teacherMeetingId)
        {
            var viewModel = new TeacherMeetingAgreementViewModel
            {
                IdReunionProfesor = teacherMeetingId
            };
            viewModel.LoadTeacherMeetingAttendanceAndFindingsByTeacherMeetingId(teacherMeetingId);
            return PartialView(viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _TeacherMeetingAgreementDetail(int tmaId, int tmId)
        {
            var viewModel = new TeacherMeetingAgreementViewModel
            {
                IdReunionProfesor = tmId,
                IdReunionProfesorAcuerdo = tmaId,
            };
            viewModel.FindTeacherMeetingAgreementForMeeting(tmaId, tmId);
            return PartialView(viewModel);
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _TeacherMeetingAgreementDetail(TeacherMeetingAgreementViewModel viewModel)
        {
            return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId = viewModel.IdReunionProfesor });
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _AddTeacherMeetingAgreement(FormCollection formCollection)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var IdReunionProfesor = formCollection["IdReunionProfesor"];

            var dateDeadLine = formCollection["DateDeadLine"];
            var fechaDeadline = dateDeadLine;
            var semanaDeadline = formCollection["SemanaDeadline"];
            var SubModalityAcademicPeriodModuleId = formCollection["SubModalityAcademicPeriodModuleId"];
            if (IdReunionProfesor == null)
            {
                PostMessage(MessageType.Error, MessageResource.LaReunionNoExiste);
                return RedirectToAction("Index");
            }
            var teacherMeetingId = int.Parse(IdReunionProfesor);
            if (fechaDeadline == null || semanaDeadline == null || SubModalityAcademicPeriodModuleId == null)
            {
                PostMessage(MessageType.Error, MessageResource.FaltanDatos);
                return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
            }
            try
            {
                var parsedDate = DateTime.ParseExact(fechaDeadline, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var acuerdoId = TeacherMeetingLogic
                    .AddTeacherMeetingAgreement(teacherMeetingId,
                        formCollection["descripcionEspanol"],
                        formCollection["descripcionIngles"],
                        semanaDeadline.Trim(),
                        parsedDate.ToString("yyyy-MM-dd HH:mm:ss")
                    ); //tabla ReunionProfesorAcuerdo

                var hallazgos = new List<int>();
                var tareas = new List<int>();
                var docentes = new List<int>();
                var docentesInvitados = new List<int>();

                foreach (var item in formCollection.AllKeys)
                {
                    if (item != "IdReunionProfesor" && item != "newAgreementTask" && item != "descripcionEspanol" && item != "descripcionIngles" && item != "selectAll" && item != "FechaDeadline" && item != "SemanaDeadline")
                    {
                        if (item.Contains("hallazgo"))
                        {
                            hallazgos.Add(Int32.Parse(formCollection[item]));
                        }
                        else if (item.Contains("tarea"))
                        {
                            string taskDescription = formCollection[item].ToString();
                            if (taskDescription == "") throw new Exception(ErrorResource.DatosIncorrectos);
                            var splitted = taskDescription.Split('|');
                            if (splitted.Length != 2) throw new Exception(ErrorResource.DatosIncorrectos);
                            int taskId = TeacherMeetingLogic.AddTeacherMeetingTask(splitted[0], splitted[1], acuerdoId, semanaDeadline.Trim(),
                        parsedDate.ToString("yyyy-MM-dd HH:mm:ss"));
                            tareas.Add(taskId);
                        }
                        else if (item.Contains("docente"))
                        {
                            docentes.Add(Int32.Parse(formCollection[item]));
                        }
                        else if (item.Contains("invitado"))
                        {
                            docentesInvitados.Add(Int32.Parse(formCollection[item]));
                        }
                    }
                }

                var teacherMeetingFindingTaskAgreement = new List<int>(); //tabla ReunionProfesorHallazgoTareaAcuerdo

                foreach (int hallazgoId in hallazgos)
                {
                    foreach (int tareaId in tareas)
                    {
                        teacherMeetingFindingTaskAgreement
                            .Add(TeacherMeetingLogic.AddTeacherMeetingFindingTaskAgreement(hallazgoId, tareaId, acuerdoId).IdReunionProfesorHallazgoTarea);
                    }
               
                }

                foreach (int rphtaId in teacherMeetingFindingTaskAgreement)
                {
                    foreach (int docenteId in docentes)
                    {
                        TeacherMeetingLogic.AddTeacherMeetingFindingTaskAgreementResponsable(rphtaId, docenteId);
                    }
                    foreach (int docenteinvitadoId in docentesInvitados)
                    {
                        TeacherMeetingLogic.AddGuestTeacherMeetingFindingTaskAgreementResponsable(rphtaId, docenteinvitadoId);
                    }
                }

                PostMessage(MessageType.Success, MessageResource.AcuerdoCreadoCorrectamente);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
            }

            return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult DeleteTeacherMeetingAgreement(int teacherMeetingAgreementId)
        {
            try
            {
                var rpa = Context.ReunionProfesorAcuerdo.Where(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).FirstOrDefault();
                if (rpa == null) return Json(new { success = false, message = MessageResource.ElAcuerdoNoExiste });
                var lstTareasHistoricas = Context.ReunionProfesorTareaHistorico.Where(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId);
                var lstTareas = Context.ReunionProfesorTarea.Where(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).ToList();
                var lstHallazgoTarea = Context.ReunionProfesorHallazgoTarea.Where(x => x.ReunionProfesorTarea.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).ToList();
                var lstHallazgoTareaReponsables = Context.ReunionProfesorHallazgoTareaResponsable.Select(x => x).Where(y => y.ReunionProfesorHallazgoTarea.ReunionProfesorTarea.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).ToList();
                var lstHallazgoTareaRealizadores = Context.ReunionProfesorHallazgoTareaRealizador.Select(x => x).Where(y => y.ReunionProfesorHallazgoTarea.ReunionProfesorTarea.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).ToList();
                var lstAcuerdosHistoricos = Context.ReunionProfesorAcuerdoHistorico.Where(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId).ToList();

                Context.ReunionProfesorHallazgoTareaRealizador.RemoveRange(lstHallazgoTareaRealizadores);
                Context.ReunionProfesorHallazgoTareaResponsable.RemoveRange(lstHallazgoTareaReponsables);
                Context.ReunionProfesorHallazgoTarea.RemoveRange(lstHallazgoTarea);
                Context.ReunionProfesorTareaHistorico.RemoveRange(lstTareasHistoricas);
                Context.ReunionProfesorTarea.RemoveRange(lstTareas);
                Context.ReunionProfesorAcuerdoHistorico.RemoveRange(lstAcuerdosHistoricos);
                Context.ReunionProfesorAcuerdo.Remove(rpa);

                Context.SaveChanges();

                return Json(new { success = true, message = MessageResource.ElAcuerdoFueBorradoCorrectamente }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message, error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _EditTeacherMeetingAgreement(int teacherMeetingAgreementId)
        {
            var viewModel = new TeacherMeetingAgreementViewModel();

            viewModel.LoadTeacherMeetingAgreementByTeacherMeetingAgreementId(teacherMeetingAgreementId);

            return PartialView(viewModel);
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult _EditTeacherMeetingAgreement(FormCollection formCollection)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var IdReunionProfesor = formCollection["ReunionProfesorAcuerdo.IdReunionProfesor"];
            var IdReunionProfesorAcuerdo = formCollection["ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo"];
            var SubModalityAcademicPeriodModuleId = formCollection["SubModalityAcademicPeriodModuleId"];

            var newDescripcionEspanol = formCollection["editarDescripcionEspanol"].ToString();
            var newDescripcionIngles = formCollection["editarDescripcionIngles"].ToString();

            if (IdReunionProfesor == null || IdReunionProfesorAcuerdo == null || SubModalityAcademicPeriodModuleId == null)
            {
                PostMessage(MessageType.Error, MessageResource.LaReunionNoExiste);
                return RedirectToAction("Index");
            }

            var teacherMeetingId = int.Parse(IdReunionProfesor);
            var teacherMeetingAgreementId = int.Parse(IdReunionProfesorAcuerdo);
            var fechaDeadline = formCollection["FechaDeadline"];
            var semanaDeadline = formCollection["SemanaDeadlineEditar"];

            if (fechaDeadline == null || semanaDeadline == null)
            {
                PostMessage(MessageType.Error, MessageResource.FaltanDatos);
                return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
            }
            try
            {
                var parsedDate = DateTime.ParseExact(fechaDeadline, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                TeacherMeetingLogic.UpdateTeacherMeetingAgreement(teacherMeetingAgreementId,
                    newDescripcionEspanol,
                    newDescripcionIngles,
                    semanaDeadline.Trim(),
                        parsedDate.ToString("yyyy-MM-dd HH:mm:ss"));

                var hallazgos = new Dictionary<int, bool>();
                var tareas = new Dictionary<int, bool>();
                var docentes = new Dictionary<int, bool>();
                var docenteInvitado = new Dictionary<int, bool>();

                foreach (var item in formCollection.AllKeys)
                {
                    if (item != "ReunionProfesorAcuerdo.IdReunionProfesor"
                        && item != "ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo"
                        && item != "newEditAgreementTaskEspanol"
                        && item != "newEditAgreementTaskIngles"
                        && item != "editarDescripcionEspanol"
                        && item != "editarDescripcionIngles"
                        && item != "selectAllEditar"
                        && item != "SemanaDeadlineEditar"
                        && item != "FechaDeadline"
                        && item != "SubModalityAcademicPeriodModuleId"
                        )
                    {
                        var estaSeleccionado = formCollection[item.ToString()].Contains("true");
                        if (item.Contains("hallazgoEditar"))
                        {
                            hallazgos.Add(Int32.Parse(item.Split('-')[1]), estaSeleccionado);
                        }
                        else if (item.Contains("tareaEditar"))
                        {
                            if (item.Contains('|'))
                            {
                                var splitted = item.Split('-');
                                var first = splitted[0];
                                var second = splitted[1];
                                var data = second.Split('|');
                                int taskId = int.Parse(data[0]);
                                if (!tareas.Keys.Contains(taskId))
                                {
                                    if (item.Contains("eddited") && estaSeleccionado)
                                    {
                                        string taskDescEspanol = data[2];
                                        string taskDescIngles = data[3];
                                        TeacherMeetingLogic.UpdateTeacherMeetingTask(taskId, taskDescEspanol, taskDescIngles);
                                    }
                                    tareas.Add(Int32.Parse(data[0]), estaSeleccionado);
                                }
                            } else
                            {
                                var splitted = item.Split('-');
                                int taskId = int.Parse(splitted[1]);
                                if (!tareas.Keys.Contains(taskId))
                                {
                                    tareas.Add(Int32.Parse(splitted[1]), estaSeleccionado);
                                }
                            }
                        }
                        else if (item.Contains("docenteEditar"))
                        {
                            docentes.Add(Int32.Parse(item.Split('-')[1]), estaSeleccionado);
                        }
                        else if (item.Contains("invitadoEditar"))
                        {
                            docenteInvitado.Add(Int32.Parse(item.Split('-')[1]), estaSeleccionado);
                        }
                        else
                        {
                            //Para crear las nuevas tareas newtarea-
                            string taskDescription = formCollection[item].ToString();
                            if (taskDescription == "") throw new Exception(ErrorResource.DatosIncorrectos);
                            var splitted = taskDescription.Split('|');
                            if (splitted.Length != 2) throw new Exception(ErrorResource.DatosIncorrectos);
                            int taskId = TeacherMeetingLogic
                                .AddTeacherMeetingTask(splitted[0], splitted[1],
                                    teacherMeetingAgreementId,
                                    semanaDeadline.Trim(),
                                    parsedDate.ToString("yyyy-MM-dd HH:mm:ss"));
                            tareas.Add(taskId, true);
                        }
                    }
                }

                foreach (var hallazgo in hallazgos)
                {
                    foreach (var tarea in tareas)
                    {
                        var hallazgoId = Int32.Parse(hallazgo.Key.ToString());
                        var tareaId = Int32.Parse(tarea.Key.ToString());
                        var hallazgoTarea = TeacherMeetingLogic.GetTeacherMeetingFindingsTaskByFindingAndTask(hallazgoId, tareaId);

                        if (hallazgo.Value && tarea.Value)
                        {
                            if (hallazgoTarea == null)
                            {
                                hallazgoTarea = TeacherMeetingLogic.AddTeacherMeetingFindingTaskAgreement(hallazgoId, tareaId, teacherMeetingAgreementId);
                            }

                            foreach (var docente in docentes)
                            {
                                var docenteId = Int32.Parse(docente.Key.ToString());
                                if (docente.Value)
                                {
                                    TeacherMeetingLogic.AddTeacherMeetingFindingTaskAgreementResponsable(hallazgoTarea.IdReunionProfesorHallazgoTarea, docenteId);
                                }
                                else
                                {
                                    TeacherMeetingLogic.DeleteTeacherMeetingFindingTaskResponsableByTaskAndTeacher(hallazgoTarea.IdReunionProfesorHallazgoTarea, docenteId);
                                }
                            }

                            foreach (var invitado in docenteInvitado)
                            {
                                var docenteInvitadoId = Int32.Parse(invitado.Key.ToString());
                                if (invitado.Value)
                                {
                                    TeacherMeetingLogic.AddGuestTeacherMeetingFindingTaskAgreementResponsable(hallazgoTarea.IdReunionProfesorHallazgoTarea, docenteInvitadoId);
                                }
                                else
                                {
                                    TeacherMeetingLogic.DeleteGuestTeacherMeetingFindingTaskResponsableByTaskAndTeacher(hallazgoTarea.IdReunionProfesorHallazgoTarea, docenteInvitadoId);
                                }
                            }
                        }
                        else
                        {
                            if (hallazgoTarea != null)
                            {
                                TeacherMeetingLogic.DeleteTeacherMeetingFindingsTask(hallazgoTarea.IdReunionProfesorHallazgoTarea, teacherMeetingAgreementId);
                            }

                            if (!tarea.Value)
                            {
                                TeacherMeetingLogic.DeleteTeacherMeetingTaskByTaskId(tareaId);
                            }
                        }
                    }
                }

                PostMessage(MessageType.Success, MessageResource.AcuerdoEditadoCorrectamente);
            } catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                PostMessage(MessageType.Error, ex.Message);
            }
            return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult DeleteTeacherMeetingFinding(int teacherMeetingFindingId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var hallazgo = Context.ReunionProfesorHallazgo.Where(x => x.IdReunionProfesorHallazgo == teacherMeetingFindingId).FirstOrDefault();

                var lstHallazgoTareas = Context.ReunionProfesorHallazgoTarea.Where(x => x.IdReunionProfesorHallazgo == teacherMeetingFindingId).ToList();

                if (lstHallazgoTareas.Count() > 0)
                {
                    var lstAcuerdos = (from rpa in Context.ReunionProfesorAcuerdo
                                       join rpt in Context.ReunionProfesorTarea on rpa.IdReunionProfesorAcuerdo equals rpt.IdReunionProfesorAcuerdo
                                       join rpht in Context.ReunionProfesorHallazgoTarea on rpt.IdReunionProfesorTarea equals rpht.IdReunionProfesorTarea
                                       join rph in Context.ReunionProfesorHallazgo on rpht.IdReunionProfesorHallazgo equals rph.IdReunionProfesorHallazgo
                                       where rph.IdReunionProfesorHallazgo == teacherMeetingFindingId
                                       select rpa).ToList();



                    foreach (var item in lstAcuerdos)
                    {
                        var rpa = Context.ReunionProfesorAcuerdo.Where(x => x.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).FirstOrDefault();
                        var lstTareasHistoricas = Context.ReunionProfesorTareaHistorico.Where(x => x.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).ToList();
                        var lstTareas = Context.ReunionProfesorTarea.Where(x => x.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).ToList();
                        var lstHallazgoTarea = Context.ReunionProfesorHallazgoTarea.Where(x => x.ReunionProfesorTarea.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).ToList();
                        var lstHallazgoTareaReponsables = Context.ReunionProfesorHallazgoTareaResponsable.Where(y => y.ReunionProfesorHallazgoTarea.ReunionProfesorTarea.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).ToList();
                        var lstHallazgoTareaRealizadores = Context.ReunionProfesorHallazgoTareaRealizador.Where(y => y.ReunionProfesorHallazgoTarea.ReunionProfesorTarea.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo).ToList();
                        var lstAcuerdosHistoricos = Context.ReunionProfesorAcuerdoHistorico.Where(x => x.IdReunionProfesorAcuerdo == item.IdReunionProfesorAcuerdo);

                        Context.ReunionProfesorHallazgoTareaRealizador.RemoveRange(lstHallazgoTareaRealizadores);
                        Context.ReunionProfesorHallazgoTareaResponsable.RemoveRange(lstHallazgoTareaReponsables);
                        Context.ReunionProfesorHallazgoTarea.RemoveRange(lstHallazgoTarea);
                        Context.ReunionProfesorTareaHistorico.RemoveRange(lstTareasHistoricas);
                        Context.ReunionProfesorTarea.RemoveRange(lstTareas);
                        Context.ReunionProfesorAcuerdoHistorico.RemoveRange(lstAcuerdosHistoricos);
                        Context.ReunionProfesorAcuerdo.Remove(rpa);
                    }
                    Context.SaveChanges();
                }

                if (hallazgo != null)
                {
                    Context.ReunionProfesorHallazgo.Remove(hallazgo);
                    Context.SaveChanges();
                }

                return Json(new { success = true, message = MessageResource.ElHallazgoFueBorradoCorrectamente }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = e.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return Json(new { success = false, message = e.Message, error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExisteTeacher(int idReunion, string cod) {
            var reu = Context.ReunionProfesorParticipanteInvitado.Where(x => x.IdReunionProfesor == idReunion && x.codigo == cod).FirstOrDefault();
            var reuPro = Context.ReunionProfesorParticipante.Where(x => x.IdReunionProfesor == idReunion && x.Docente.Codigo == cod).FirstOrDefault();
            if (reu == null && reuPro == null) {
                return Json(false,JsonRequestBehavior.AllowGet);
            }else return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveTeacher(TeacherMeetingParticipantViewModel viewModel, int teacherMeetingId)
        {
            try
            {
                var profesorInvitado = new ReunionProfesorParticipanteInvitado
                {
                    IdReunionProfesor = teacherMeetingId,
                    NombreCompleto = viewModel.ProfesorInvitadoNombre,
                    Asistio = viewModel.ProfesorinvitadoAsistio,
                    codigo = viewModel.ProfesorInvitadoCodigo
                };
                Context.ReunionProfesorParticipanteInvitado.Add(profesorInvitado);

                Context.SaveChanges();
                
                return Json(new { success = true, title = "¡Éxito!", message = "Profesor Invitado agregado exitosamente." }, JsonRequestBehavior.AllowGet); ;

            }catch (Exception e)
            {
                return Json(new { success = false, title = "Ocurrió un error", message = "Error al agregar el profesor invitado: " + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult CloseTeacherMeetingMinute(int teacherMeetingId, FormCollection formCollection)
        {
            var actualTeacherMeeting = Context.ReunionProfesor.FirstOrDefault(x => x.IdReunionProfesor == teacherMeetingId);
            var onHoldTaskStatudId = (int)TAREA.PENDIENTE;
            var completedTaskStatudId = (int)TAREA.COMPLETADA;
            var notCompletedTaskStatudId = (int)TAREA.NO_COMPLETADA;

            var onHoldAgreementStatusId = (int)ACUERDO.PENDIENTE;
            var completedAgreementStatudId = (int)ACUERDO.COMPLETADA;
            var notCompletedAgreementStatudId = (int)ACUERDO.NO_COMPLETADA;
            if (actualTeacherMeeting == null)
            {
                PostMessage(MessageType.Error, MessageResource.LaReunionNoExiste);
                return RedirectToAction("Index");
            }

            var teacherMeetingConfiguration = Context.ReunionProfesorConfiguracion
                .FirstOrDefault(x => x.Nivel == actualTeacherMeeting.Nivel
                && x.IdSubModalidadPeriodoAcademicoModulo == actualTeacherMeeting.IdSubModalidadPeriodoAcademicoModulo);

            var nextTeacherMeetings = Context.ReunionProfesor.Where(x =>
                   x.IdSubModalidadPeriodoAcademicoModulo == actualTeacherMeeting.IdSubModalidadPeriodoAcademicoModulo &&
                   x.IdUnidadAcademica == actualTeacherMeeting.IdUnidadAcademica &&
                   x.Nivel == actualTeacherMeeting.Nivel &&
                   x.NumSemana > actualTeacherMeeting.NumSemana
                   ).OrderBy(x => x.NumSemana);
            var nextTeacherMeeting = nextTeacherMeetings.Count() > 0 ? nextTeacherMeetings.FirstOrDefault() : null;

            var deadline = teacherMeetingConfiguration.PlazoEnDias;
            var now = DateTime.Now;
            //var now = new DateTime(2019, 4, 10, 15, 0, 0);
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");

            #region Validation: All the findings are already related
            var teacherMeetingFindings = Context.ReunionProfesorHallazgo.Where(x => x.IdReunionProfesor == actualTeacherMeeting.IdReunionProfesor).ToList();
            var allTheFindingsMatchTasks = true;
            for (int i = 0; i < teacherMeetingFindings.Count; i++)
            {
                var findingId = teacherMeetingFindings[i].IdReunionProfesorHallazgo;
                bool hasMatch = Context.ReunionProfesorHallazgoTarea
                    .Any(x => x.IdReunionProfesorHallazgo == findingId);
                if (!hasMatch)
                    allTheFindingsMatchTasks = false;

            }
            if (!allTheFindingsMatchTasks)
            {
                PostMessage(MessageType.Warning, TeacherMeetingResource.HallazgosNecesitanRelacionarse);
                return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
            }
            #endregion

            #region Validation: If is the last meeting, you must complete all the previous tasks. Just if they are available to complete
            var previousTasksOnHoldInThisMeeting = Context.ReunionProfesorTareaHistorico
                       .Any(x => x.IdUltimaReunionProfesor == teacherMeetingId && x.IdEstado == onHoldTaskStatudId && now < x.ReunionProfesorTarea.FechaDeadline.Value);
            if (nextTeacherMeeting == null && previousTasksOnHoldInThisMeeting)
            {
                PostMessage(MessageType.Warning, TeacherMeetingResource.TareasNecesitanCompletarse);
                return RedirectToAction("NewTeacherMeetingMinute", new { teacherMeetingId });
            }
            #endregion

            try
            {
                #region Checking if tasks are already completed
                var agreementsInvolved = Context.ReunionProfesorTareaHistorico
                    .Where(x => x.IdUltimaReunionProfesor == teacherMeetingId).Select(x => x.ReunionProfesorAcuerdo).Distinct().ToList();

                foreach (var agreementInvolved in agreementsInvolved)
                {
                    var teacherMeetingHistoricAgreement = agreementInvolved.ReunionProfesorAcuerdoHistorico.FirstOrDefault(x => x.IdUltimaReunionProfesor == teacherMeetingId);
                    if (agreementInvolved.ReunionProfesorTarea.All(x => x.IdEstado == completedTaskStatudId))
                    { //Si ya estan completadas todas las tareas de estos acuerdos
                        agreementInvolved.IdEstado = completedAgreementStatudId;
                        agreementInvolved.IdReunionProfesorCompletada = teacherMeetingId;
                        agreementInvolved.FechaCierre = formatDateTimeNow.ToDateTime();
                        agreementInvolved.FechaActualizacion = formatDateTimeNow.ToDateTime();

                        if (teacherMeetingHistoricAgreement != null)
                        {
                            teacherMeetingHistoricAgreement.IdEstado = completedAgreementStatudId;
                            teacherMeetingHistoricAgreement.FechaCierre = formatDateTimeNow.ToDateTime();
                            teacherMeetingHistoricAgreement.FechaActualizacion = formatDateTimeNow.ToDateTime();
                        }
                    } else if (agreementInvolved.ReunionProfesorTarea.Any(x => x.IdEstado == onHoldTaskStatudId && now > x.FechaDeadline.Value)) // si no estan completadas y estan fuera del deadline
                    {
                        agreementInvolved.IdEstado = notCompletedAgreementStatudId;
                        agreementInvolved.IdReunionProfesorCompletada = null;
                        agreementInvolved.FechaCierre = null;
                        agreementInvolved.FechaActualizacion = formatDateTimeNow.ToDateTime();
                        if (teacherMeetingHistoricAgreement != null)
                        {
                            teacherMeetingHistoricAgreement.IdEstado = notCompletedAgreementStatudId;
                            teacherMeetingHistoricAgreement.FechaCierre = null;
                            teacherMeetingHistoricAgreement.FechaActualizacion = formatDateTimeNow.ToDateTime();
                        }
                        var tareas = agreementInvolved.ReunionProfesorTarea.Where(x => x.IdEstado == onHoldTaskStatudId);
                        foreach (var tarea in tareas)
                        {
                            tarea.IdEstado = notCompletedTaskStatudId;
                            tarea.FechaCierre = null;
                            tarea.FechaActualizacion = formatDateTimeNow.ToDateTime();
                            var tareaHistorico = tarea.ReunionProfesorTareaHistorico.FirstOrDefault(x => x.IdUltimaReunionProfesor == teacherMeetingId);
                            if (tareaHistorico != null)
                            {
                                tareaHistorico.IdEstado = notCompletedTaskStatudId;
                                tareaHistorico.FechaCierre = null;
                                teacherMeetingHistoricAgreement.FechaActualizacion = formatDateTimeNow.ToDateTime();
                            }
                        }
                    }
                    Context.SaveChanges();
                }
                #endregion

                #region Creating agreements for the next meeting if they are not already completed
                var teacherMeetingAgreementsNotCompleted = Context.ReunionProfesorAcuerdoHistorico
                    .Where(x => x.IdUltimaReunionProfesor == teacherMeetingId
                    && x.IdEstado == onHoldAgreementStatusId).ToList();
                var previousTasksInvolved = Context.ReunionProfesorTareaHistorico
                        .Where(x => x.IdUltimaReunionProfesor == teacherMeetingId
                        && x.ReunionProfesorAcuerdo.IdEstado == onHoldAgreementStatusId).ToList();
                if (nextTeacherMeeting != null)
                {
                    //var rPAHList = new List<ReunionProfesorAcuerdoHistorico>();
                    //var rPTHList = new List<ReunionProfesorTareaHistorico>();
                    foreach (var item in teacherMeetingAgreementsNotCompleted)
                    {
                        if (nextTeacherMeeting.NumSemana > item.ReunionProfesorAcuerdo.SemanaDeadline.Value) //SOLO SE AGREGAN LAS QUE NO HAN VENCIDO
                        {                           
                            ReunionProfesorAcuerdoHistorico newRPAH = new ReunionProfesorAcuerdoHistorico
                            {
                                IdReunionProfesorAcuerdo = item.IdReunionProfesorAcuerdo,
                                IdUltimaReunionProfesor = nextTeacherMeeting.IdReunionProfesor,
                                ComentarioCierreEspanol = item.ComentarioCierreEspanol,
                                ComentarioCierreIngles = item.ComentarioCierreIngles,
                                FechaCierre = item.FechaCierre,
                                IdEstado = item.IdEstado,
                                FechaCreacion = item.FechaCreacion,
                                FechaActualizacion = item.FechaActualizacion
                            };
                            Context.ReunionProfesorAcuerdoHistorico.Add(newRPAH);
                            Context.SaveChanges();
                            //var rPAHExistance = Context.ReunionProfesorAcuerdoHistorico
                            //    .Any(x => x.IdReunionProfesorAcuerdo == newRPAH.IdReunionProfesorAcuerdo
                            //    && x.IdUltimaReunionProfesor == newRPAH.IdUltimaReunionProfesor
                            //    && x.ComentarioCierreEspanol == newRPAH.ComentarioCierreEspanol
                            //    && x.IdEstado == newRPAH.IdEstado
                            //    && x.FechaActualizacion == newRPAH.FechaActualizacion);
                            //if (!rPAHExistance)
                            //    rPAHList.Add(newRPAH);
                        }
                    }

                    foreach (var item in previousTasksInvolved)
                    {
                        if (nextTeacherMeeting.NumSemana > item.ReunionProfesorAcuerdo.SemanaDeadline.Value)
                        {
                            ReunionProfesorTareaHistorico newRPTH = new ReunionProfesorTareaHistorico
                            {
                                IdReunionProfesorTarea = item.IdReunionProfesorTarea,
                                IdReunionProfesorAcuerdo = item.IdReunionProfesorAcuerdo,
                                IdUltimaReunionProfesor = nextTeacherMeeting.IdReunionProfesor,
                                ComentarioCierreEspanol = item.ComentarioCierreEspanol,
                                ComentarioCierreIngles = item.ComentarioCierreIngles,
                                FechaCierre = item.FechaCierre,
                                IdEstado = item.IdEstado,
                                FechaCreacion = item.FechaCreacion,
                                FechaActualizacion = item.FechaActualizacion
                            };
                            //var rPTHExistance = Context.ReunionProfesorTareaHistorico
                            //    .Any(x => x.IdReunionProfesorTarea == newRPTH.IdReunionProfesorTarea
                            //    && x.IdReunionProfesorAcuerdo == newRPTH.IdReunionProfesorAcuerdo
                            //    && x.IdUltimaReunionProfesor == newRPTH.IdUltimaReunionProfesor
                            //    && x.ComentarioCierreEspanol == newRPTH.ComentarioCierreEspanol
                            //    && x.IdEstado == newRPTH.IdEstado
                            //    && x.FechaActualizacion == newRPTH.FechaActualizacion);
                            //if (!rPTHExistance)
                            //    rPTHList.Add(newRPTH);

                            Context.ReunionProfesorTareaHistorico.Add(newRPTH);
                            Context.SaveChanges();
                        }
                    }                  
                }
                #endregion

                actualTeacherMeeting.ActaCreada = true;
                actualTeacherMeeting.FechaActualizacion = formatDateTimeNow.ToDateTime();
                actualTeacherMeeting.FechaActaFinalizada = formatDateTimeNow.ToDateTime();

                if (!now.IsBetween(actualTeacherMeeting.Inicio.Value, actualTeacherMeeting.Fin.Value.AddDays(deadline)))
                {
                    PostMessage(MessageType.Info, MessageResource.ElActaFueCreadaYentregadaAdesatiempo);
                    actualTeacherMeeting.IdEstadoActa = (int)ACTA.CERRADA_CON_RETRASO;
                }
                else
                {
                    PostMessage(MessageType.Success, MessageResource.ElActaFueCreadaYentregadaAtiempo);
                    actualTeacherMeeting.IdEstadoActa = (int)ACTA.CERRADA_A_TIEMPO;
                }

                Context.SaveChanges();
                ViewBag.IdReunionActaCompletada = teacherMeetingId;
                return RedirectToAction("Index", new { teacherMeetingId });
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }



        [HttpPost]
        public JsonResult NotificarReunionProfesor()
        {
            var onHoldAgreementStatusId = (int)ACUERDO.PENDIENTE;
            var now = DateTime.Now;
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
            var teacherMeetingViewModel = new TeacherMeetingViewModel();
            try
            {
                var idreunionprofesor = Request.Form[0].ToInteger();


                var teacherMeeting = Context.ReunionProfesor.Where(x => x.IdReunionProfesor == idreunionprofesor).FirstOrDefault();

                var fechainicio = teacherMeeting.Inicio;
                var fechafin = teacherMeeting.Fin;
                var lugar = teacherMeeting.Lugar;

                if (fechainicio == null)
                {
                    return Json("StartDateUnallowed", JsonRequestBehavior.AllowGet);
                }

                if (fechafin == null)
                {
                    return Json("EndDateUnallowed", JsonRequestBehavior.AllowGet);
                }

                if (lugar == "" || lugar == null)
                {
                    return Json("PlaceUnallowed", JsonRequestBehavior.AllowGet);
                }

                var previousTeacherMeetingNotNotified = Context.ReunionProfesor.Where(x => x.Nivel == teacherMeeting.Nivel
                    && x.IdSubModalidadPeriodoAcademicoModulo == teacherMeeting.IdSubModalidadPeriodoAcademicoModulo
                    && x.IdUnidadAcademica == teacherMeeting.IdUnidadAcademica
                    && x.NumSemana < teacherMeeting.NumSemana
                    && !x.Notificada).OrderByDescending(x => x.NumSemana).FirstOrDefault();


                if (previousTeacherMeetingNotNotified != null) return Json("PreviousMeetingNotNotified", JsonRequestBehavior.AllowGet);

                var firstPreviousTeacherMeeting = Context.ReunionProfesor.Where(x => x.Nivel == teacherMeeting.Nivel
                    && x.IdSubModalidadPeriodoAcademicoModulo == teacherMeeting.IdSubModalidadPeriodoAcademicoModulo
                    && x.IdUnidadAcademica == teacherMeeting.IdUnidadAcademica
                    && x.NumSemana < teacherMeeting.NumSemana).OrderByDescending(x => x.NumSemana).FirstOrDefault();
                if (firstPreviousTeacherMeeting != null && firstPreviousTeacherMeeting.Inicio.Value > now)
                    return Json("WaitCompletionPrevious", JsonRequestBehavior.AllowGet);
                // Correo

                
                var isTesting = AppSettingsHelper.GetAppSettings("ENV_TESTING", "false").ToBoolean(); ;
                var teacherMeetingParticipants = Context.ReunionProfesorParticipante.Where(x => x.IdReunionProfesor == idreunionprofesor).ToList();
                var mailsForTest = new List<string>(new string[] {
                    "u201319829", //Javier Valverde
                    "u201313378", // Roberto Miranda
                });
                if (!isTesting)
                {
                    foreach (var item in teacherMeetingParticipants)
                    {
                        string correo = item.Docente.Codigo.ToLower() + "@upc.edu.pe";
                        string area = teacherMeeting.AreaEncargada.ToUpper();

                        string retorno = SendEmail(teacherMeeting.Nivel, teacherMeeting.Semana, area, correo, teacherMeeting.Lugar, teacherMeeting.Fin.ToDateTime(), teacherMeeting.IdReunionProfesor);

                        if (retorno == "Enviado!")
                        {
                            continue;
                        }
                        else
                        {
                            return Json("Error", JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                else
                {
                    foreach (var mail in mailsForTest)
                    {
                        string fullMail = mail + "@upc.edu.pe";
                        string area = teacherMeeting.AreaEncargada.ToUpper();

                        string retorno = SendEmail(teacherMeeting.Nivel, teacherMeeting.Semana, area, fullMail, teacherMeeting.Lugar, teacherMeeting.Fin.ToDateTime(), teacherMeeting.IdReunionProfesor);

                        if (retorno == "Enviado!")
                        {
                            continue;
                        }
                        else
                        {
                            return Json("Error", JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                teacherMeeting.Notificada = true;
                teacherMeeting.FechaActualizacion = formatDateTimeNow.ToDateTime();

                #region Verificar los acuerdos que aún no fueron completados
                var rPAHList = new List<ReunionProfesorAcuerdoHistorico>();
                var rPTHList = new List<ReunionProfesorTareaHistorico>();
                //var notCompletedHistoricAgreements = Context.ReunionProfesorAcuerdoHistorico.Where(x => 
                //    x.IdEstado == onHoldAgreementStatusId &&
                //    x.ReunionProfesor.NumSemana < teacherMeeting.NumSemana &&
                //    x.ReunionProfesor.Nivel == teacherMeeting.Nivel &&
                //    x.ReunionProfesor.IdSubModalidadPeriodoAcademicoModulo == teacherMeeting.IdSubModalidadPeriodoAcademicoModulo &&
                //    x.ReunionProfesor.IdUnidadAcademica == teacherMeeting.IdUnidadAcademica
                //    ).ToList();
                var notCompletedHistoricAgreements = teacherMeetingViewModel.GetPendingTeacherMeetingAgreements(onHoldAgreementStatusId, teacherMeeting.NumSemana, teacherMeeting.Nivel, teacherMeeting.IdSubModalidadPeriodoAcademicoModulo.Value, teacherMeeting.IdUnidadAcademica).ToList();
                if (notCompletedHistoricAgreements.Count > 0)
                {
                    foreach (var historicAgreement in notCompletedHistoricAgreements)
                    {
                        ReunionProfesorAcuerdoHistorico newRPAH = new ReunionProfesorAcuerdoHistorico
                        {
                            IdUltimaReunionProfesor = teacherMeeting.IdReunionProfesor,
                            IdReunionProfesorAcuerdo = historicAgreement.IdReunionProfesorAcuerdo,
                            ComentarioCierreEspanol = historicAgreement.ComentarioCierreEspanol,
                            ComentarioCierreIngles = historicAgreement.ComentarioCierreIngles,
                            FechaCierre = historicAgreement.FechaCierre,
                            IdEstado = historicAgreement.IdEstado,
                        };

                        var rPAHExistance = Context.ReunionProfesorAcuerdoHistorico
                            .Any(x => x.IdReunionProfesorAcuerdo == newRPAH.IdReunionProfesorAcuerdo
                            && x.IdUltimaReunionProfesor == newRPAH.IdUltimaReunionProfesor
                            && x.ComentarioCierreEspanol == newRPAH.ComentarioCierreEspanol
                            && x.IdEstado == newRPAH.IdEstado);
                        if (!rPAHExistance)
                            rPAHList.Add(newRPAH);


                        var historicTasksInvolved = Context.ReunionProfesorTareaHistorico.Where(x => x.IdReunionProfesorAcuerdo == historicAgreement.IdReunionProfesorAcuerdo);
                        foreach (var historicTask in historicTasksInvolved)
                        {
                            ReunionProfesorTareaHistorico newRPTH = new ReunionProfesorTareaHistorico
                            {
                                IdReunionProfesorTarea = historicTask.IdReunionProfesorTarea,
                                IdUltimaReunionProfesor = teacherMeeting.IdReunionProfesor,
                                IdReunionProfesorAcuerdo = historicTask.IdReunionProfesorAcuerdo,
                                ComentarioCierreEspanol = historicTask.ComentarioCierreEspanol,
                                ComentarioCierreIngles = historicTask.ComentarioCierreIngles,
                                FechaCierre = historicTask.FechaCierre,
                                IdEstado = historicTask.IdEstado,
                            };
                            var rPTHExistance = Context.ReunionProfesorTareaHistorico
                                .Any(x => x.IdReunionProfesorTarea == newRPTH.IdReunionProfesorTarea
                                && x.IdReunionProfesorAcuerdo == newRPTH.IdReunionProfesorAcuerdo
                                && x.IdUltimaReunionProfesor == newRPTH.IdUltimaReunionProfesor
                                && x.ComentarioCierreEspanol == newRPTH.ComentarioCierreEspanol
                                && x.IdEstado == newRPTH.IdEstado);
                            if (!rPTHExistance)
                                rPTHList.Add(newRPTH);
                        }
                    }
                    Context.ReunionProfesorAcuerdoHistorico.AddRange(rPAHList);
                    Context.ReunionProfesorTareaHistorico.AddRange(rPTHList);
                }
                
                #endregion
                Context.SaveChanges();

                return Json("Success", JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return Json("Error", JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public string SendEmail(int nivel, string semana, string area, string correo, string lugar, DateTime fechafin, int idreunionprofesores)
        {
            var now = DateTime.Now;
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
            string Asunto = "Reunión de profesores de nivel " + nivel.ToString() + " del área " + area + " de la " + semana;
            string Mensaje = "<p style=\"text-align: justify; \">Estimado Profesor,</p><p style=\"text-align: justify; \"><br></p><p style=\"text-align: justify; \">La Escuela de Ingeniería de Sistemas y Computación (EISC) Lo invita a usted a participar en la reunión del área de  <i> <b>" + area.ToUpper() + "</b></i>  de la <i> <b>" + semana.ToUpper() + " </b></i> a realizarse el día <i> <b>" + fechafin.ToShortDateString() + "</b></i> con lugar de reunión en <i> <b>" + lugar.ToString() + "</b></i>. Usted podrá visualizar la reunión en el siguiente enlace: </p>";

            EmailGRALogic mailLogic = new EmailGRALogic();

            try
            {

                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                //   String Ruta = "Areas/Report/Resources/WebPages/ReportPlanMejoraCurso.aspx";
                String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/"); // + Ruta + queryString);
                String urlImage = String.Format("{0}://{1}{2}",
                                                Request.Url.Scheme,
                                                Request.Url.Authority, vdirectory);

                bool enviado = mailLogic.SendMailArc(Asunto, mailLogic.GetHtmlEmail(Mensaje, urlSurvey, urlImage + "/Areas/Meeting/Resources/Images/MailHeaderReunionProfesor.png", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), correo);
                if (enviado == false)
                {
                    return ("Error");
                }
                else
                {
                    return ("Enviado!");
                }
            }
            catch (Exception e)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = e.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return (e.InnerException.Message);
            }

        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult GetActaPdf(int teacherMeetingId, string culture)
        {
            var now = DateTime.Now;
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                ExportTeacherMeetingLogic pdfExport = new ExportTeacherMeetingLogic(Context, teacherMeetingId, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, culture);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.ACTAS_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                string filename = Path.GetFileName(path);
                //string filename = path.Split(new char[] { '/' }).Last();
                ViewBag.IdReunionActaCompletada = teacherMeetingId;
                return File(path, "application/pdf", filename);
            }
            catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                PostMessage(Helpers.MessageType.Error, MessageResource.OcurrioUnErrorExportarActaReunion + "   " + ex.ToString());
                return RedirectToAction("Consult");
            }
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult TeacherMeetingAttendance() //TODO
        {

            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();

            var idioma = Session.GetCulture().ToString();

            var ViewModel = new TeacherMeetingAttendanceViewModel();
            ViewModel.Fill(CargarDatosContext(), ModalidadId, EscuelaId, idioma);

            return View(ViewModel);

        }

        [HttpPost]
        public ActionResult TeacherMeetingAttendance(TeacherMeetingAttendanceViewModel viewModel)
        {
            if (!viewModel.FilterWeeks)
            {
                viewModel.NumSemanaDesde = -1;
                viewModel.NumSemanaHasta = -1;
                viewModel.FechaDesde = viewModel.FechaDesde.Add(new TimeSpan(0, 0, 1));
                viewModel.FechaHasta = viewModel.FechaHasta.Add(new TimeSpan(23, 59, 59));
            }

            if (ModelState.IsValid)
            {
                viewModel.HasValue = true;
                viewModel.ValidateIfDataExist(CargarDatosContext(), viewModel);
            }
            else
            {
                viewModel.HasValue = false;
            }
            return PartialView("_TeacherMeetingAttendance", viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult TeacherMeetingRealized() //TODO
        {

            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();

            var idioma = Session.GetCulture().ToString();

            var ViewModel = new TeacherMeetingRealizedViewModel();
            ViewModel.Fill(CargarDatosContext(), ModalidadId, EscuelaId, idioma);

            return View(ViewModel);

        }

        [HttpPost]
        public ActionResult TeacherMeetingRealized(TeacherMeetingRealizedViewModel viewModel)
        {
            if (!viewModel.FilterWeeks)
            {
                viewModel.NumSemanaDesde = -1;
                viewModel.NumSemanaHasta = -1;
                viewModel.FechaDesde = viewModel.FechaDesde.Add(new TimeSpan(0, 0, 1));
                viewModel.FechaHasta = viewModel.FechaHasta.Add(new TimeSpan(23, 59, 59));
            }

            if (ModelState.IsValid)
            {
                viewModel.HasValue = true;
                viewModel.ValidateIfDataExist(CargarDatosContext(), viewModel);
            }
            else
            {
                viewModel.HasValue = false;
            }
            return PartialView("_TeacherMeetingRealized", viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult TeacherMeetingCharts() //TODO
        {

            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();

            var idioma = Session.GetCulture().ToString();

            var ViewModel = new TeacherMeetingChartsViewModel();
            ViewModel.Fill(CargarDatosContext(), ModalidadId, EscuelaId, idioma);

            return View(ViewModel);

        }

        [HttpPost]
        public ActionResult TeacherMeetingCharts(TeacherMeetingChartsViewModel viewModel)
        {
            if (!viewModel.FilterWeeks)
            {
                viewModel.NumSemanaDesde = -1;
                viewModel.NumSemanaHasta = -1;
                viewModel.FechaDesde = viewModel.FechaDesde.Add(new TimeSpan(0, 0, 1));
                viewModel.FechaHasta = viewModel.FechaHasta.Add(new TimeSpan(23, 59, 59));
            }

            if (ModelState.IsValid)
            {
                viewModel.HasValue = true;
                viewModel.ValidateIfDataExist(CargarDatosContext(), viewModel);
            }
            else
            {
                viewModel.HasValue = false;
            }

            return PartialView("_TeacherMeetingCharts", viewModel);
        }

        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Docente, AppRol.Acreditador)]
        public ActionResult TeacherMeetingTasks()
        {

            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();
            int? idsubmodalidadperiodoacademico = Session.GetSubModalidadPeriodoAcademicoId();

            var idioma = Session.GetCulture().ToString();

            var ViewModel = new TeacherMeetingTasksViewModel();
            ViewModel.Fill(CargarDatosContext(), ModalidadId, EscuelaId, idioma, idsubmodalidadperiodoacademico);

            return View(ViewModel);

        }

        [HttpPost]
        public ActionResult TeacherMeetingTasks(TeacherMeetingTasksViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.HasValue = true;
                viewModel.ValidateIfDataExist(CargarDatosContext(), viewModel);
            }
            else
            {
                viewModel.HasValue = false;
            }

            return PartialView("_TeacherMeetingTasks", viewModel);
        }

        #region JavascriptReloadFields

        public JsonResult GetAllCursosForArea(int idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var cursos = (from ua in Context.UnidadAcademica
                          where ua.Nivel == 3
                                && (ua.UnidadAcademica2.UnidadAcademica2.IdUnidadAcademica == idAreaUnidadAcademica)
                                && ua.IdEscuela == EscuelaId
                          select ua).ToList();

            var items = cursos.Select(x => new SelectListItem {
                Value = x.IdUnidadAcademica.ToString(),
                Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles ?? x.NombreEspanol : x.NombreEspanol) });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllCursosForSubarea(int idSubareaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var cursos = (from ua in Context.UnidadAcademica
                          join cpa in Context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                          where ua.Nivel == 3
                          && (ua.UnidadAcademica2.IdUnidadAcademica == idSubareaUnidadAcademica)
                          && ua.IdEscuela == EscuelaId
                          select cpa).ToList();

            var items = cursos.Select(x => new SelectListItem {
                Value = x.IdCursoPeriodoAcademico.ToString(),
                Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.Curso.NombreIngles ?? x.Curso.NombreEspanol : x.Curso.NombreEspanol)
            });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
   
        [AuthorizeUser(AppRol.Usuario)]
        public JsonResult GetAllSedesForCurso(int idCursoPeriodoAcademico, int idSubModalidadPeriodoAcademicoModulo, string lang)
        {
            var SubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == idSubModalidadPeriodoAcademicoModulo);
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var sede = (from cpa in Context.CursoPeriodoAcademico
                        join s in Context.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                        join se in Context.Sede on s.IdSede equals se.IdSede
                        where cpa.IdCursoPeriodoAcademico == idCursoPeriodoAcademico && s.IdModulo == SubmodalidadPeriodoAcademicoModulo.IdModulo
                        select se).Distinct().ToList();

            sede.OrderBy(x => x.IdSede);

            var items = sede.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetAllSeccionesForCursoAndSede(int idSede, int idCursoPeriodoAcademico, int idSubModalidadPeriodoAcademicoModulo)
        {
            var SubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == idSubModalidadPeriodoAcademicoModulo);

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var seccion = (from cpa in Context.CursoPeriodoAcademico
                           join s in Context.Seccion on cpa.IdCursoPeriodoAcademico equals s.IdCursoPeriodoAcademico
                           join se in Context.Sede on s.IdSede equals se.IdSede
                           where cpa.IdCursoPeriodoAcademico == idCursoPeriodoAcademico && s.IdSede == idSede
                               && s.IdModulo == SubmodalidadPeriodoAcademicoModulo.IdModulo
                           select s).ToList();

            seccion.OrderBy(x => x.IdSede);

            var items = seccion.Select(x => new SelectListItem
            {
                Value = x.IdSeccion.ToString(),
                Text = x.Codigo
                + " : " +
                //(Context.DocenteSeccion.Where(y => y.IdSeccion == x.IdSeccion).Select(z => z.Docente.Apellidos + ", "+ z.Docente.Nombres).FirstOrDefault())
                (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.CursoPeriodoAcademico.Curso.NombreIngles ?? x.CursoPeriodoAcademico.Curso.NombreEspanol : x.CursoPeriodoAcademico.Curso.NombreEspanol)
            });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWeeksByModule(int IdSubmodalidadPeriodoAcademicoModulo)
        {
            // var weeks = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == IdSubmodalidadPeriodoAcademicoModulo)
            //     .Select(y => y.Modulo.Semanas).FirstOrDefault();

            var weeks = Context.ReunionProfesor.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == IdSubmodalidadPeriodoAcademicoModulo).Max(x => x.NumSemana);

            List<SelectListItem> items = new List<SelectListItem>();

            for (int i=0; i<= weeks; i++ )
            {
                var aux = new SelectListItem { Value = i.ToString(), Text = i.ToString() };

                items.Add(aux);
            }

            items.AsEnumerable();

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubareasForArea(int idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

        //    var NivelSubarea = Int32.Parse(TeacherMeetingResource.SubareaNumberValue);

            var subareas = (from ua in Context.UnidadAcademica
                             join ua2 in Context.UnidadAcademica on ua.IdUnidadAcademica equals ua2.IdUnidadAcademicaPadre
                            //              where ua.Nivel == NivelSubarea
                            where ua.IdUnidadAcademica == idAreaUnidadAcademica
                            select ua2).ToList();

            var items = subareas.Select(x => new SelectListItem { Value = x.IdUnidadAcademica.ToString(), Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles ?? x.NombreEspanol : x.NombreEspanol ) });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWeeksRange(string weekStart, int sMAPMId)
        {
            var module = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == sMAPMId)?.Modulo;
            var moduleStart = module.FechaInicio.Value;
            var startDateOfSelectedWeek = moduleStart.AddDays(7 * (int.Parse(weekStart) - 1));
            var endDateOfSelectedWeek = startDateOfSelectedWeek.AddDays(6).AddHours(23);
            var start = startDateOfSelectedWeek.ToShortDateString();
            var end = endDateOfSelectedWeek.ToShortDateString();
            return Json(new { start, end }, JsonRequestBehavior.AllowGet);
        }
     
        public JsonResult GetAreasBySubmodalidadPeriodoAcademicoModulo(int idSubmodalidadPeriodoAcademicoModulo)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var SubmodalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == idSubmodalidadPeriodoAcademicoModulo);

            var areas = (from ua in Context.UnidadAcademica
                         where ua.IdSubModalidadPeriodoAcademico == SubmodalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico
                                && ua.Nivel == (int)ConstantHelpers.ORGANIZATION_CHART_LEVELS.FIRST_LEVEL
                         select ua).Distinct().ToList();

            var items = areas.Select(x => new SelectListItem
            {
                Value = x.IdUnidadAcademica.ToString(),
                Text = (CurrentCulture == ConstantHelpers.CULTURE.INGLES ? x.NombreIngles ?? x.NombreEspanol : x.NombreEspanol)
            });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }


        public JsonResult TranslateToSpanish(string value, string from = "es", string to = "en")
        {
            var now = DateTime.Now;
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                YandexTranslatorAPIHelper yandexTranslatorAPIHelper = new YandexTranslatorAPIHelper();
                string fromTo = from + "-" + to;
                var result = yandexTranslatorAPIHelper.Translate(value, fromTo);
                return Json(new { result }, JsonRequestBehavior.AllowGet);
            } catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }
        #endregion
    }
}
