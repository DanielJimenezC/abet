﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.NeoMeetingDelegate;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using ClosedXML.Excel;
using System.Linq;
using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
//using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelReports;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using YandexTranslateCSharpSdk;
using System.Threading.Tasks;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.IfcManagement;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Resources.Areas.Meeting;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using System.Dynamic;


namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Acreditador)]
    public class NeoMeetingDelegateController : BaseController
    {
        public string CurrentCulture { get; set; }
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult _ConfirmacionDeleteARD(Int32 IdReunionDelegado)
        {
            var viewModel = new _ConfirmacionDeleteARDViewModel();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult _ConfirmacionDeleteARD(_ConfirmacionDeleteARDViewModel model)
        {
            try
            {
                var reunion = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == model.IdReunionDelegado);
                //int idReunionDelegado = hallazgo.IdReunionDelegado.Value;
                reunion.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
            }
            var ruta = Url.Action("LstReunionDelegados", "NeoMeetingDelegate");
            return Json(ruta);
        }


        public ActionResult LstReunionDelegados(Int32? NumeroPagina, Int32? IdPeriodoAcademico, Int32? IdSede, DateTime? Fecha)
        {
            var viewModel = new LstReunionDelegadosViewModel();
            var docenteId = Session.GetDocenteId();
            var IdSubmodalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            if (IdPeriodoAcademico.HasValue == false)
            {
                //IdPeriodoAcademico = context.PeriodoAcademico.FirstOrDefault(x => x.Estado == "ACT").IdPeriodoAcademico;
                var sessionSmpa = Session.GetSubModalidadPeriodoAcademicoId();
                IdPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == sessionSmpa).First().IdPeriodoAcademico;
            }
          
            viewModel.CargarDatos(CargarDatosContext(), NumeroPagina, IdPeriodoAcademico.Value, IdSede, Fecha, EscuelaId);
            return View(viewModel);
        }


        public ActionResult AddEditMeetingDelegate(int? IdReunionDelegado)
        {
            var viewmodel = new AddEditMeetingDelegateViewModel();
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetPeriodoAcademicoId(), Session.GetEscuelaId());
            viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());

            var sessionIdEscuela = Session.GetEscuelaId();
            var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();
            return View(viewmodel);
        }

        public ActionResult AddReuCommentDelegate(int? IdReunionDelegado)
        {
            var viewmodel = new AddReuCommentDelegateViewModel();
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetPeriodoAcademicoId(), Session.GetEscuelaId());
            viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());

            var sessionIdEscuela = Session.GetEscuelaId();
            var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();
            return View(viewmodel);
        }
        
        public ActionResult ObtLstAlmnAst(int? IdReunionDelegado)
        {
            var viewmodel = new ObtLstAlmnAstViewModel();
            //viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetPeriodoAcademicoId(), Session.GetEscuelaId());
            viewmodel.Fill(CargarDatosContext(), IdReunionDelegado, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());

            var sessionIdEscuela = Session.GetEscuelaId();
            var sessionIDPA = Session.GetSubModalidadPeriodoAcademicoId();
            return View(viewmodel);
        }



        [HttpPost]
        public ActionResult ObtLstAlmnAst(ObtLstAlmnAstViewModel model)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;
            try
            {
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_ASISTENCIA_ALUMNOS_ARD + ConstantHelpers.TEMPLATE_ASISTENCIA_ALUMNOS_ARD);
                String nombreReporte = string.Empty;
                objExcel = ExcelReportLogic.GenerateAsistenciaReport(CargarDatosContext(), model.IdSede, model.Fecha, path, ref nombreReporte);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
                
            }
            catch (Exception)
            {
                
                return RedirectToAction("ObtLstAlmnAst", "NeoMeetingDelegate");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }
        [HttpPost] 
        public ActionResult AddEditMeetingDelegate(AddEditMeetingDelegateViewModel model)
        {
            try
            {
                //int idciclo = model.IdCiclo;
                //int idsede = model.IdSede;
                //List<ReunionDelegado> reuniones = context.ReunionDelegado.Where(x => x.IdSubModalidadPeriodoAcademico == idciclo && x.IdSede == idsede).OrderByDescending(x => x.Identificador).ToList();

                //ReunionDelegado reunion = new ReunionDelegado();
                int?reunion = model.IdReunionDelegado;
                //if (model.IdReunionDelegado.HasValue)
                //{
                //    reunion = context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == model.IdReunionDelegado);
                //}
                //else
                //{
                    
                //    reunion.IdSubModalidadPeriodoAcademico = idciclo;
                //    reunion.IdEscuela = model.IdEscuela;
                //    reunion.IdUsuario = Session.GetUsuarioId();
                //    reunion.TipoReunion = "REU";

                //                    if(reuniones.Count ==0)
                //    {
                //        reunion.Identificador = 1;
                //    }
                //    else
                //    {
                //        int? auxidentificador = reuniones[0].Identificador;
                //            if (auxidentificador.HasValue == false) auxidentificador = 0;
                //        reunion.Identificador = auxidentificador + 1;

                //    }

                //}

                //reunion.IdSede = idsede;
                //reunion.Fecha = model.Fecha;
                //reunion.Estado = "ACT";
                //Sede sede = new Sede();
                //sede = context.Sede.FirstOrDefault(x => x.IdSede == reunion.IdSede);

                ////reunion.Comentario = "ARD-" + sede.Codigo + "-" +  (0) + "-" + model.Fecha.Value.ToShortDateString().Replace("/", "");

                //int? auxID = context.ReunionDelegado.Select(x => x.IdReunionDelegado).DefaultIfEmpty(0).Max();
                //if (auxID.HasValue == false) auxID = 0;
                

                //if(model.IdReunionDelegado.HasValue ==false)
                //reunion.Comentario = "ARD-" + sede.Codigo + "-" + (auxID + 1) + "-" +  model.Fecha.Value.ToShortDateString().Replace("/","");



                //if (model.IdReunionDelegado.HasValue == false || model.IdReunionDelegado.Value == 0)
                //    context.ReunionDelegado.Add(reunion);

                //context.SaveChanges();

                //PostMessage(MessageType.Success);

                return RedirectToAction("AddFinding", "NeoMeetingDelegate", new { IdReunionDelegado = reunion });
            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return RedirectToAction("AddEditMeetingDelegate", "NeoMeetingDelegate");

            }


        }
        [HttpPost]
        public ActionResult AddReuCommentDelegate(AddReuCommentDelegateViewModel model)
        {
            try
            {
                int idciclo = model.IdCiclo;
                int idsede = model.IdSede;
                List<ReunionDelegado> reuniones = Context.ReunionDelegado.Where(x => x.IdSubModalidadPeriodoAcademico == idciclo && x.IdSede == idsede).OrderByDescending(x => x.Identificador).ToList();

                ReunionDelegado reunion = new ReunionDelegado();
                if (model.IdReunionDelegado.HasValue)
                {
                    reunion = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == model.IdReunionDelegado);
                }
                else
                {

                    reunion.IdSubModalidadPeriodoAcademico = idciclo;
                    reunion.IdEscuela = model.IdEscuela;
                    reunion.IdUsuario = Session.GetUsuarioId();

                    if (reuniones.Count == 0)
                    {
                        reunion.Identificador = 1;
                    }
                    else
                    {
                        int? auxidentificador = reuniones[0].Identificador;
                        if (auxidentificador.HasValue == false) auxidentificador = 0;
                        reunion.Identificador = auxidentificador + 1;

                    }

                }

                reunion.IdSede = idsede;
                reunion.Fecha = model.Fecha;
                reunion.Estado = "ACT";
                reunion.TipoReunion = "COM"; 
                Sede sede = new Sede();
                sede = Context.Sede.FirstOrDefault(x => x.IdSede == reunion.IdSede);

                //reunion.Comentario = "ARD-" + sede.Codigo + "-" +  (0) + "-" + model.Fecha.Value.ToShortDateString().Replace("/", "");

                int? auxID = Context.ReunionDelegado.Select(x => x.IdReunionDelegado).DefaultIfEmpty(0).Max();
                if (auxID.HasValue == false) auxID = 0;


                if (model.IdReunionDelegado.HasValue == false)
                    reunion.Comentario = "ARD-" + sede.Codigo + "-" + (auxID + 1) + "-" + model.Fecha.Value.ToShortDateString().Replace("/", "");



                if (model.IdReunionDelegado.HasValue == false || model.IdReunionDelegado.Value == 0)
                    Context.ReunionDelegado.Add(reunion);

                Context.SaveChanges();

                PostMessage(MessageType.Success);

                return RedirectToAction("CallTheRoll", "NeoMeetingDelegate", new { IdReunionDelegado = reunion.IdReunionDelegado });
            }
            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return RedirectToAction("AddReuCommentDelegate", "NeoMeetingDelegate");

            }


        }

        public ActionResult CallTheRoll(int IdReunionDelegado)
        {
            var model = new CallTheRollViewModel();
            int? IdSubmoIdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            model.CargarDatos(CargarDatosContext(), IdReunionDelegado, IdSubmoIdSubModalidadPeriodoAcademico);    
            return View(model);
        }

        [HttpPost]
        public JsonResult CallTheRoll(string IdReunionDelegado, string LstAsistencia)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                dynamic jsonObject = serializer.Deserialize<dynamic>(LstAsistencia);
                List<string> arrcodalumnos = new List<string>();
                string codalumnosJoin = "";
                int index = 0;
                foreach (var codalumno in jsonObject)
                {
                    arrcodalumnos.Add(codalumno);
                    index++;
                };

                codalumnosJoin = string.Join(";", arrcodalumnos);

                int idReunionDelegado = Int32.Parse(IdReunionDelegado);
                var Asistencias = Context.Asistencia.Where(x => x.idReunionDelegado == idReunionDelegado);


                if (Asistencias.Any())
                {
                    foreach (var item in Asistencias)
                    {
                    
                        Context.Asistencia.Remove(item);
                    }

                    Context.SaveChanges();
                }
                Asistencia asistencia = new Asistencia();
                foreach (var item in jsonObject)
                {
                        asistencia = new Asistencia();
                        int IdAlumno = Int32.Parse(item);
                        var Result = Context.Alumno.First(x => x.IdAlumno == IdAlumno);
                        asistencia.idAlumno = Result.IdAlumno;
                        asistencia.estaPresente = true;
                        asistencia.fechaActual = DateTime.Now;
                        asistencia.idReunionDelegado = idReunionDelegado;
                        Context.Asistencia.Add(asistencia);

                }
                Context.SaveChanges();

                var ruta = Url.Action("AddComment", "NeoMeetingDelegate", new { @IdReunionDelegado = IdReunionDelegado,@codalumnosJoin = codalumnosJoin});
                return Json(ruta);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                throw;
            }
        }

        //Asistencia

        public ActionResult AddStudent(int IdReunionDelegado)
        {
            var viewmodel = new AddStudentViewModel();
            viewmodel.Fill(CargarDatosContext(), IdReunionDelegado);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddStudent(AddStudentViewModel viewmodel)
        {
            try
            {
                int idReunionDelegado = viewmodel.IdReunionDelegado;
                int idAlumno = viewmodel.IdAlumno;

                ReunionDelegadoAlumno rda = Context.ReunionDelegadoAlumno.FirstOrDefault(x => x.IdReunionDelegado == idReunionDelegado && x.IdAlumno == idAlumno);

                if(rda!=null)
                {
                    var codigo = Context.Alumno.FirstOrDefault(x => x.IdAlumno == idAlumno).Codigo;
                    PostMessage(MessageType.Error, MessageResource.ElAlumnoConCodigo + " "+ codigo + " "+MessageResource.YaFueRegistradoPreviamente);
                    return RedirectToAction("AddStudent", "NeoMeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado });
                }

                rda = new ReunionDelegadoAlumno();
                rda.IdReunionDelegado = idReunionDelegado;
                rda.IdAlumno = viewmodel.IdAlumno;

                Context.ReunionDelegadoAlumno.Add(rda);
                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddStudent", "NeoMeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado});

        }

        public ActionResult RemoveStudent(int IdReunionDelegadoAlumno, int IdReunionDelegado)
        {
            try
            { 
            ReunionDelegadoAlumno rda = Context.ReunionDelegadoAlumno.FirstOrDefault(x => x.IdReunionDelegadoAlumno == IdReunionDelegadoAlumno);

                Context.ReunionDelegadoAlumno.Remove(rda);

                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch(Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddStudent", "NeoMeetingDelegate", new { IdReunionDelegado = IdReunionDelegado });
        }
        //Commentarios
        public ActionResult AddComment(int IdReunionDelegado,string codalumnosJoin)////////////
        {
            var viewModel = new AddCommentViewModel();
            Int32? IdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado, IdSubModalidadPeriodoAcademico, codalumnosJoin);
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult AddComment(AddEditFindingViewModel model)////////////
        {
            try/////////////
            {

                if (model.IdAlumno == 0 && model.IdAlumnoInvitado == 0)
                    PostMessage(MessageType.Success);

                int IdPeriodoAcademico = model.IdPeriodoAcademico;


                int idSubModalidadPeriodoAcademico = (from s in Context.SubModalidadPeriodoAcademico
                                                      where s.IdPeriodoAcademico == IdPeriodoAcademico
                                                      select s.IdSubModalidadPeriodoAcademico).FirstOrDefault();

      
                int IdCurso = model.IdCurso;

                    ComentarioDelegado ComentarioDelegado = new ComentarioDelegado();
                    ComentarioDelegado.IdReunionDelegado = model.IdReunionDelegado;

                     ComentarioDelegado.DescripcionEspanol = model.DescripcionEspanol;
                     ComentarioDelegado.DescripcionIngles = model.DescripcionIngles;

                //***************************************************

                if (model.IdAlumno != 0)
                    ComentarioDelegado.IdAlumno = model.IdAlumno;
                    else
                    ComentarioDelegado.idAlumnoInvitado  = model.IdAlumnoInvitado;


                    var seccion = Context.Seccion.FirstOrDefault(x => x.IdSeccion == model.IdSeccion);
                    var curso = seccion.CursoPeriodoAcademico.Curso;
                    ComentarioDelegado.IdCurso = model.IdCurso;
                    ComentarioDelegado.IdSeccion = model.IdSeccion;
                    int idseccion = model.IdSeccion;

                    var docenteSeccion = Context.DocenteSeccion.FirstOrDefault(x => x.IdSeccion == idseccion);

                    if (docenteSeccion != null)
                    {
                    ComentarioDelegado.IdDocente = docenteSeccion.IdDocente;
                    }



                    var cicloacademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;
                    var cursocodigo = curso.Codigo;
                    var SedeCodigo = Context.Sede.FirstOrDefault(x => x.IdSede == model.IdSede).Codigo;

                    var numCom = (from a in Context.ComentarioDelegado select a.IdComentarioDelegado).Count();


                ComentarioDelegado.CodigoComentario = String.Format("{0}-{1}-{2}-{3}-F-{4}", ConstantHelpers.ENCUESTA.COM, cursocodigo, cicloacademico, SedeCodigo, numCom + 1);
    

                        Context.ComentarioDelegado.Add(ComentarioDelegado);
                        Context.SaveChanges();                                                    
                PostMessage(MessageType.Success);

                return Json("truecreate", JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);

                return Json("false", JsonRequestBehavior.AllowGet);

            }

            //return RedirectToAction("AddComment", "NeoMeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });

        }
       
        public ActionResult AddFinding(int IdReunionDelegado)
        {
            var viewModel = new AddEditFindingViewModel();
            viewModel.Fill(CargarDatosContext(), IdReunionDelegado, EscuelaId);
            return View(viewModel);
        }

        public ActionResult EditFinding(int IdReunionDelegado, int IdHallazgo)
        {
            var viewModel = new AddEditFindingViewModel();
            viewModel.FillEdit(CargarDatosContext(), IdReunionDelegado, IdHallazgo);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditFinding(AddEditFindingViewModel model)
        {
            try
            {
                Hallazgo hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo);
                hallazgo.IdCriticidad = model.IdCriticidad.Value;
                hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                hallazgo.DescripcionEspanol = model.DescripcionEspanol;
                hallazgo.DescripcionIngles = model.DescripcionEspanol;

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch(Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddFinding", "NeoMeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });
        }

        public ActionResult DeleteFinding(int IdHallazgo, int IdReunionDelegado)
        {
            var idReuDelegados = Context.ComentarioDelegado.Where(x => x.IdComentarioDelegado == IdReunionDelegado).Select(x => x.IdReunionDelegado).FirstOrDefault();
            try
            {
                Hallazgos ohallazgos = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                ohallazgos.Estado = ConstantHelpers.ESTADO.INACTIVO;

                var challazgo = Context.ComentarioHallazgo.Where(x => x.IdHallazgo == IdHallazgo).ToList();

                foreach (var ochallazgo in challazgo)
                {
                    Context.ComentarioHallazgo.Remove(ochallazgo);
                }


                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddFinding", "NeoMeetingDelegate", new { IdReunionDelegado = idReuDelegados });
        }
        public ActionResult DeleteComent(int idComentarioDe, int IdReunionDelegado)
        {
            try
            {
                ComentarioDelegado comnt = Context.ComentarioDelegado.FirstOrDefault(x => x.IdComentarioDelegado == idComentarioDe);
                Context.ComentarioDelegado.Remove(comnt);

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddComment", "NeoMeetingDelegate", new { IdReunionDelegado = IdReunionDelegado });
        }

        public ActionResult DeleteObservacion(int IdObservacionDelegado, int IdReunionDelegado)
        {
            try
            {
                ObservacionDelegado observacion = Context.ObservacionDelegado.FirstOrDefault(x => x.IdObservacionDelegado == IdObservacionDelegado);
                observacion.Estado = ConstantHelpers.ESTADO.INACTIVO;

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddFinding", "NeoMeetingDelegate", new { IdReunionDelegado = IdReunionDelegado });
        }

        //Implementacion de funcion traducir

        public async Task Traducir(YandexTranslatorHelper traductor)
        {
                YandexTranslateSdk yandex = new YandexTranslateSdk();
                yandex.ApiKey = traductor.key;
                traductor.textoSalida =  await yandex.TranslateText(traductor.textoEntrada, traductor.direccion);
        }
  
        [HttpPost]
        public JsonResult AddFinding(AddEditFindingViewModel model, string[] check)
        {
            try
            {
                if(check ==null)
                {
                    throw new System.ArgumentException("Debe Seleccionar un Comentario");
                }

                List<int> listcheck = check.Select(int.Parse).ToList();

                var ListComentariosElegidos = (from a in Context.ComentarioDelegado
                                               where listcheck.Contains(a.IdComentarioDelegado)
                                               select a
                                               ).ToList();

                int? IdReunionDelegado = ListComentariosElegidos.First().IdReunionDelegado;

                var ReunionDelegado = Context.ReunionDelegado.FirstOrDefault(x => x.IdReunionDelegado == IdReunionDelegado);
                        
                var SMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == ReunionDelegado.IdSubModalidadPeriodoAcademico);

                Hallazgos ohallazgos = new Hallazgos();

                ohallazgos.IdSubModalidadPeriodoAcademicoModulo = SMPAM.IdSubModalidadPeriodoAcademicoModulo;
                ohallazgos.IdConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.Instrumento.Acronimo == "ARD").IdConstituyenteInstrumento;
                ohallazgos.IdCurso = model.IdCurso;
                ohallazgos.Codigo = Convert.ToString(Context.Database.SqlQuery<string>("select[dbo].[FN_GetCodigoHallazgo](@p0, @p1, @p2,@p3)", ohallazgos.IdConstituyenteInstrumento, ohallazgos.IdCurso, 0,0).FirstOrDefault());
                ohallazgos.DescripcionEspanol = model.DescripcionEspanol;
                ohallazgos.DescripcionIngles = model.DescripcionIngles;
                if (model.DescripcionIngles == null)
                {
                    YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                    ohallazgos.DescripcionIngles = helper.Translate(model.DescripcionEspanol);
                }

                FindLogic ofl = new FindLogic(CargarDatosContext(), ohallazgos.IdConstituyenteInstrumento, SMPAM.IdSubModalidadPeriodoAcademico);
                ohallazgos.Identificador = ofl.ObtenerIndiceHallazgo();

                ohallazgos.IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == "Necesita mejora").IdNivelAceptacionHallazgo;
                ohallazgos.IdCriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == "Preocupante").IdCriticidad;
                ohallazgos.FechaRegistro = DateTime.Now;
                ohallazgos.Estado = "ACT";

                Context.Hallazgos.Add(ohallazgos);
                Context.SaveChanges();


                foreach (var ocomentario in ListComentariosElegidos)
                {
                    if (ocomentario.IdCurso == model.IdCurso)
                    {
                        ComentarioHallazgo ocomenthallazgo = new ComentarioHallazgo();
                        ocomenthallazgo.IdComentarioDelegado = ocomentario.IdComentarioDelegado;
                        ocomenthallazgo.IdHallazgo = ohallazgos.IdHallazgo;
                        Context.ComentarioHallazgo.Add(ocomenthallazgo);
                        Context.SaveChanges();
                    }
                }

                PostMessage(MessageType.Success);
                return Json("truecreate", JsonRequestBehavior.AllowGet);

            }
            catch(Exception e)
            {
                PostMessage(MessageType.Error);
                return Json("false", JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("AddFinding", "NeoMeetingDelegate", new { IdReunionDelegado = model.IdReunionDelegado });
        }

        [HttpPost]
        public ActionResult ListarDescripcionPorCurso(int? idSeccion, int idPeriodoAcademico)
        {
            AddEditFindingViewModel model = new AddEditFindingViewModel();

            int idCurso = Context.Seccion.FirstOrDefault(x => x.IdSeccion == idSeccion).CursoPeriodoAcademico.IdCurso;

            model.CargarDatosPartial(CargarDatosContext(), idCurso, idPeriodoAcademico);
            return PartialView(model);
        }

        public ActionResult AddEvidence(int IdReunionDelegado )
        {
            var viewModel = new AddEditEvidenceViewModel();
            //viewModel.IdReunionDelegado = IdReunionDelegado;
            viewModel.CargarDatos(CargarDatosContext(), IdReunionDelegado);
            return View(viewModel);
        }

        public ActionResult EditEvidence(int IdReunionDelegado, int IdEvidencia)
        {
            var viewModel = new AddEditEvidenceViewModel();
            //viewModel.IdReunionDelegado = IdReunionDelegado;
            viewModel.CargarDatosEdit(CargarDatosContext(), IdReunionDelegado, IdEvidencia);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditEvidence(AddEditEvidenceViewModel viewmodel)
        {
            try
            {
                Evidencia evidencia = new Evidencia();

                if (viewmodel.IdEvidencia != 0 && viewmodel.IdEvidencia.HasValue)
                {
                    evidencia = Context.Evidencia.FirstOrDefault(x => x.IdEvidencia == viewmodel.IdEvidencia);
                }

                String timestamp = (DateTime.Now).ToString("yyyyMMddHHmmssffff");
                var uploadDir = "~/uploads/ReunionDelegados";
                var imageUrl = "";

                if (viewmodel.file != null)
                {
                    if (Path.GetExtension(viewmodel.file.FileName).ToLower() != ".jpg" && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".png"
                        && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".gif" && Path.GetExtension(viewmodel.file.FileName).ToLower() != ".jpeg")
                    {
                        PostMessage(MessageType.Error, MessageResource.SoloSePermitenImagenesArchivosConExtensionJPGpngGIFjpeg);
                        return RedirectToAction("AddEvidence", "NeoMeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado });
                    }

                    var imagePath = Path.Combine(Server.MapPath(uploadDir), timestamp + Path.GetExtension(viewmodel.file.FileName));
                    imageUrl = "/uploads/ReunionDelegados/" + timestamp + Path.GetExtension(viewmodel.file.FileName);
                    viewmodel.file.SaveAs(imagePath);
                    evidencia.ubicacion = imageUrl;
                }
                // Order validate lines
                Int32 MaxValue;

                List<Evidencia> lstOrden = (from e in Context.Evidencia
                                        where e.IdReunionDelegado == viewmodel.IdReunionDelegado
                                        select e).ToList();
                if (lstOrden.Count == 0)
                {
                    MaxValue = 0;
                    evidencia.Orden = 1;
                }
                else
                    MaxValue = lstOrden.Select(x=>x.Orden).Max();

                evidencia.Orden = viewmodel.Orden;

                for (int i = 0; i < lstOrden.Count; i++)
                {
                    if (viewmodel.Orden.Equals(lstOrden[i].Orden) && viewmodel.IdEvidencia != lstOrden[i].IdEvidencia)
                    {
                        evidencia.Orden = MaxValue + 1;                // Si se repite el orden, se cambia el valor a la cantidad de evidencias + 1
                        break;
                    }

                }

                evidencia.IdReunionDelegado = viewmodel.IdReunionDelegado;

                if (viewmodel.IdEvidencia == 0 || viewmodel.IdEvidencia.HasValue == false)
                Context.Evidencia.Add(evidencia);

                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error, e.InnerException.ToString());
            }
            return RedirectToAction("AddEvidence", "NeoMeetingDelegate", new { IdReunionDelegado = viewmodel.IdReunionDelegado });

        }

        public ActionResult DeleteEvidence(int IdReunionDelegado, int IdEvidencia)
        {
            try
            {
                Evidencia evidencia = new Evidencia();
                evidencia = Context.Evidencia.FirstOrDefault(x => x.IdEvidencia == IdEvidencia);
                Context.Evidencia.Remove(evidencia);
                Context.SaveChanges();

                PostMessage(MessageType.Success);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }

            return RedirectToAction("AddEvidence", "NeoMeetingDelegate", new { IdReunionDelegado = IdReunionDelegado });
        }


        public ActionResult PrintReunionDelegado(int IdReunionDelegado)
        {
            var viewModel = new PrintReunionDelegadoViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdReunionDelegado);

            Document document = new Document();

            MemoryStream stream = new MemoryStream();



            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;

                document.Open();



                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/EncuestaSuperior2.PNG"));
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_RIGHT;

                document.Add(imagen);



                var fontNormal = FontFactory.GetFont("Arial", 16, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("Acta de Reunión de Delegados", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo);
                document.Add(Chunk.NEWLINE);
                var fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);

                var escuela = viewModel.reunion.Escuela.Nombre;
                Paragraph titulo2 = new Paragraph(escuela, fontsub);
                titulo2.Alignment = Element.ALIGN_CENTER;
                document.Add(titulo2);

                document.Add(Chunk.NEWLINE);
                document.Add(Chunk.NEWLINE);


                var fontNormal2 = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);
                document.Add(new Paragraph("Ciclo: " + viewModel.reunion.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, fontNormal2));
                document.Add(new Paragraph("Fecha: " + viewModel.reunion.Fecha.Value.ToString("dd/MM/yyyy"), fontNormal2));
                document.Add(new Paragraph("Sede: " + viewModel.reunion.Sede.Nombre, fontNormal2));

                document.Add(Chunk.NEWLINE);

                fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph tituloHallazgos = new Paragraph("Hallazgos:", fontsub);
                tituloHallazgos.Alignment = Element.ALIGN_LEFT;
                document.Add(tituloHallazgos);

                document.Add(Chunk.NEWLINE);

                PdfPTable table = new PdfPTable(4);
              
                table.WidthPercentage = 100;
               // table.SetWidthPercentage(new float[] { 70, 90, 70, 120, 60, 180 },document.PageSize);
                table.SetWidthPercentage(new float[] {120, 80,160 , 230 }, document.PageSize);
                var fontNormal3 = FontFactory.GetFont("Arial", 10,Font.BOLD, BaseColor.WHITE);

                PdfPCell cell = new PdfPCell(new Phrase("Criticidad", fontNormal3));
                cell.BackgroundColor = new BaseColor(237, 28, 36);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.PaddingBottom = 5;
               /* table.AddCell(cell);

                cell.Phrase = new Phrase("Nivel de Aceptación", fontNormal3);
                table.AddCell(cell);

                cell.Phrase = new Phrase("Outcome", fontNormal3);
                table.AddCell(cell);*/

                cell.Phrase =  new Phrase("Curso", fontNormal3);
                table.AddCell(cell);

                cell.Phrase = new Phrase("Sección", fontNormal3);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Comentario", fontNormal3);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Hallazgo", fontNormal3);
                table.AddCell(cell);

                fontNormal3 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
                cell.BackgroundColor = BaseColor.WHITE;
                for (int i = 0; i<viewModel.LstHallazgos.Count; i++)
                {
                   Hallazgos hallazgo = viewModel.LstHallazgos[i];

                    /* cell.Phrase = new Phrase(hallazgo.Criticidad.NombreEspanol, fontNormal3);
                     table.AddCell(cell);
                     cell.Phrase = new Phrase(hallazgo.NivelAceptacionHallazgo.NombreEspanol, fontNormal3);
                     table.AddCell(cell);
                     cell.Phrase = new Phrase(hallazgo.Comision.Codigo + " | " + hallazgo.Outcome.Nombre, fontNormal3);
                     table.AddCell(cell);*/


                    //var codigo = "";
                    //if (hallazgo.IdSeccion.HasValue) codigo = hallazgo.Seccion.Codigo;
                    //cell.Phrase = new Phrase(codigo, fontNormal3);
                    //table.AddCell(cell);

                    //var docente = "";
                    //if (hallazgo.IdDocente.HasValue) docente = hallazgo.Docente.Nombres + " " + hallazgo.Docente.Apellidos;

                    cell.Phrase = new Phrase(hallazgo.Curso.NombreEspanol, fontNormal3);
                    table.AddCell(cell);

                    var codigo = "";
                    var docente = "";
                    var Enter = "\n";
                    foreach (var item2 in hallazgo.ComentarioHallazgo)
                    {


                        var comentariodelegado = Context.ComentarioDelegado.FirstOrDefault(x => x.IdComentarioDelegado == item2.IdComentarioDelegado);

                        var text = "- " + comentariodelegado.Seccion.Codigo;
                        if (codigo.Count()==0)
                        {
                            codigo = codigo + text;
                        }
                        else
                        {
                            codigo = codigo + Enter + text;
                        }
                       
                       
                     }
                    cell.Phrase = new Phrase(codigo, fontNormal3);
                    table.AddCell(cell);


                    foreach (var item2 in hallazgo.ComentarioHallazgo)
                    {
                        var comentariodelegado = Context.ComentarioDelegado.FirstOrDefault(x => x.IdComentarioDelegado == item2.IdComentarioDelegado);

                        var text = "- " + comentariodelegado.DescripcionEspanol;
                        if (codigo.Count() == 0)
                        {
                            docente = docente + text;
                        }
                        else
                        {
                            docente = docente + Enter + text;
                        }


                    }
                    cell.Phrase = new Phrase(docente, fontNormal3);
                    table.AddCell(cell);

                    cell.Phrase = new Phrase(hallazgo.DescripcionEspanol, fontNormal3);
                    table.AddCell(cell);

                }

                Paragraph p = new Paragraph();
                p.IndentationLeft = 5;
                //p.IndentationRight = 5;
                table.HorizontalAlignment = Element.ALIGN_LEFT;
                p.Add(table);
                document.Add(p);


                //Observaciones

            //document.Add(Chunk.NEWLINE);

            //        fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
            //        tituloHallazgos = new Paragraph("Observaciones:", fontsub);
            //        tituloHallazgos.Alignment = Element.ALIGN_LEFT;
            //        document.Add(tituloHallazgos);

            //        document.Add(Chunk.NEWLINE);

            //        PdfPTable table2 = new PdfPTable(5);

            //        table2.WidthPercentage = 100;
            //        table2.SetWidthPercentage(new float[] { 70, 90, 190, 60, 180}, document.PageSize);

            //         fontNormal3 = FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.WHITE);

            //         cell = new PdfPCell(new Phrase("Criticidad", fontNormal3));
            //        cell.BackgroundColor = new BaseColor(237, 28, 36);
            //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            //        cell.PaddingBottom = 5;
            //        table2.AddCell(cell);

            //        cell.Phrase = new Phrase("Nivel de Aceptación", fontNormal3);
            //        table2.AddCell(cell);

            //        cell.Phrase = new Phrase("Curso", fontNormal3);
            //        table2.AddCell(cell);

            //        cell.Phrase = new Phrase("Sección", fontNormal3);
            //        table2.AddCell(cell);

            //        cell.Phrase = new Phrase("Descripción", fontNormal3);
            //        table2.AddCell(cell);

            //        fontNormal3 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
            //        cell.BackgroundColor = BaseColor.WHITE;
            //        for (int i = 0; i < viewModel.LstObservaciones.Count; i++)
            //        {
            //            ObservacionDelegado observacion = viewModel.LstObservaciones[i];

            //            cell.Phrase = new Phrase(observacion.Criticidad.NombreEspanol, fontNormal3);
            //            table2.AddCell(cell);
            //            cell.Phrase = new Phrase(observacion.NivelAceptacionHallazgo.NombreEspanol, fontNormal3);
            //            table2.AddCell(cell);
            //            cell.Phrase = new Phrase(observacion.Curso.NombreEspanol, fontNormal3);
            //            table2.AddCell(cell);
            //            cell.Phrase = new Phrase(observacion.Seccion.Codigo, fontNormal3);
            //            table2.AddCell(cell);
            //            cell.Phrase = new Phrase(observacion.DescripcionEspanol, fontNormal3);
            //            table2.AddCell(cell);
            //        }

            //     p = new Paragraph();
            //    p.IndentationLeft = 5;
            //    //p.IndentationRight = 5;
            //    table2.HorizontalAlignment = Element.ALIGN_LEFT;
            //    p.Add(table2);
            //    document.Add(p);



                document.NewPage();
                /*
                                //Asistencia
                                document.Add(Chunk.NEWLINE);
                                document.Add(Chunk.NEWLINE);
                                document.Add(Chunk.NEWLINE);

                                fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);
                                Paragraph tituloAsistencia = new Paragraph("Asistencia:", fontsub);
                                tituloAsistencia.Alignment = Element.ALIGN_LEFT;
                                document.Add(tituloAsistencia);

                                document.Add(Chunk.NEWLINE);

                                PdfPTable table3 = new PdfPTable(3);

                                table3.WidthPercentage = 100;
                                //table2.SetWidthPercentage(new float[] {100, 100, 100, 100 }, document.PageSize);

                                fontNormal3 = FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.WHITE);

                                cell = new PdfPCell(new Phrase("Código", fontNormal3));
                                cell.BackgroundColor = new BaseColor(237, 28, 36);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.PaddingBottom = 5;
                                table3.AddCell(cell);

                                cell.Phrase = new Phrase("Nombre completo", fontNormal3);
                                table3.AddCell(cell);

                                cell.Phrase = new Phrase("Delegado", fontNormal3);
                                table3.AddCell(cell);

                                fontNormal3 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
                                cell.BackgroundColor = BaseColor.WHITE;
                                for (int i = 0; i < viewModel.LstAlumnoAsistencia.Count; i++)
                                {
                                    AlumnoAsistencia alumnoseccion = viewModel.LstAlumnoAsistencia[i];

                                    cell.Phrase = new Phrase(alumnoseccion.AlumnoCodigo, fontNormal3);
                                    table3.AddCell(cell);
                                    cell.Phrase = new Phrase(alumnoseccion.NombreCompleto, fontNormal3);
                                    table3.AddCell(cell);
                                    cell.Phrase = new Phrase(alumnoseccion.Delegado, fontNormal3);
                                    table3.AddCell(cell);

                                }


                                Paragraph p2 = new Paragraph();
                                p2.IndentationLeft = 5;
                                //p.IndentationRight = 5;
                                table3.HorizontalAlignment = Element.ALIGN_LEFT;
                                p2.Add(table3);
                                document.Add(p2);

                                document.NewPage();*/


                List<Evidencia> LstEvidencias = Context.Evidencia.Where(x => x.IdReunionDelegado == IdReunionDelegado).OrderBy(x=>x.Orden).ToList();
               
                for(int i =0; i<LstEvidencias.Count; i++)
                {
                    try
                    {
                        var primeraparte = Request.Url.GetLeftPart(UriPartial.Authority);
                        var direccion = primeraparte + "/" + Url.Content("~/" + LstEvidencias[i].ubicacion);
                        imagen = iTextSharp.text.Image.GetInstance(direccion);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_CENTER;
                        //imagen.ScaleAbsoluteHeight(document.PageSize.Height * 0.9f);
                        if (imagen.Width > imagen.Height)
                            imagen.RotationDegrees = 90;

                        imagen.ScaleToFit(new Rectangle(document.PageSize.Width * 1.2f, document.PageSize.Width * 1.2f));




                        document.Add(imagen);

                        document.NewPage();
                    }
                    catch(Exception e)
                    {
                        continue;
                    }
                }

            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "Acta_Reunión_Delegados_" + viewModel.reunion.Comentario + ".pdf");
        }
        
        public ActionResult IndexReporte()
        {
            return View();
        }
        public ActionResult ExportExcelCarreraCurso(int? IdReunionDelegado)
        {
            
            var viewmodel = new ExportExcelCarreraCursoViewModel();
            viewmodel.Fill(CargarDatosContext(),Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId(),Session.GetModalidadId(), session.GetPeriodoAcademicoId(), Session.GetCulture().ToString());
            
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult ExportExcelCarreraCurso(ExportExcelCarreraCursoViewModel model)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;
            try
            {
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_ARD + ConstantHelpers.TEMPLATE_ARD_CARRERA_CURSO);
                String nombreReporte = string.Empty;

                int submodadlidaPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodo && x.SubModalidad.Modalidad.IdModalidad == ModalidadId ).Select(x => x.IdSubModalidadPeriodoAcademico).FirstOrDefault();
                int IdCarrera = model.IdCarrera;
                int IdArea = model.IdArea;
                int IdSubArea = model.IdSubArea;

                int IdCarreraPeriodoAcademico = 0;
                if (Context.CarreraPeriodoAcademico.Any(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == submodadlidaPeriodoAcademicoId))
                {
                    IdCarreraPeriodoAcademico = Context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == submodadlidaPeriodoAcademicoId).IdCarreraPeriodoAcademico;
                }
                int IdEscuela = Session.GetEscuelaId();

                objExcel = ExcelReportLogic.GenerateArdCarreraCurso(CargarDatosContext(), IdCarrera, IdCarreraPeriodoAcademico, IdArea, IdSubArea, submodadlidaPeriodoAcademicoId, model.IdPeriodo, IdEscuela, path, ref nombreReporte);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);

            }
            catch (Exception ex)
            {

                return RedirectToAction("IndexReporte", "NeoMeetingDelegate");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }
        public JsonResult GetAllSubareasForArea(string idAreaUnidadAcademica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
            var subareas = GedServices.GetAllSUbareasInAreaServices(Context, idAreaUnidadAcademica, EscuelaId).Select(x => CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles).Distinct();
            var items = subareas.Select(x => new SelectListItem { Value = x, Text = x });

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListarReuniones(int Sede)
        {
            if(Sede == 0)
            {
                var data = new[] { new SelectListItem { Value = "0", Text = "[- " + MessageResource.PrimeroSeleccionaUnaSede + " -]" } };
                return Json(new SelectList(data, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            int idmoda = Session.GetModalidadId();

            int? submodaactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == idmoda).
                FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var reunion = (from r in Context.ReunionDelegado
                           join cd in Context.ComentarioDelegado on r.IdReunionDelegado equals cd.IdReunionDelegado
                           where (r.IdSede == Sede
                           && r.TipoReunion =="COM" && r.Estado =="ACT" && r.IdSubModalidadPeriodoAcademico == submodaactivo)
                           select new SelectListItem { Value = r.IdReunionDelegado.ToString(), Text = r.Fecha.ToString() }).Distinct();

            return Json(new SelectList(reunion, "Value", "Text"),JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListarComentarioPorBusquedaJSON(int IdCurso, int? IdReunion)
        {                 
             var qe = (from cm in Context.ComentarioDelegado
                      join c in Context.Curso on cm.IdCurso equals c.IdCurso
                      join a in Context.Alumno on cm.IdAlumno equals a.IdAlumno
                      where (cm.IdReunionDelegado == IdReunion) && ( c.IdCurso == IdCurso)
                      select new
                      {
                          idcomentario = cm.IdComentarioDelegado,
                          alumno = a.Nombres + " " + a.Apellidos,
                          curso = c.NombreEspanol,
                          comentario = cm.DescripcionEspanol
                      }).ToList();
           
             var LstComentarios = qe;
                return Json(LstComentarios, JsonRequestBehavior.AllowGet); 
        }
        public JsonResult ListarCursosPorArea(int IdArea, int? IdReunion)
        {
            var LstCursos = (from rd in Context.ReunionDelegado
                                join cd in Context.ComentarioDelegado on rd.IdReunionDelegado equals cd.IdReunionDelegado
                                join smpa in Context.SubModalidadPeriodoAcademico on rd.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                join cu in Context.Curso on cd.IdCurso equals cu.IdCurso
                                join cpa in Context.CursoPeriodoAcademico on cu.IdCurso equals cpa.IdCurso
                                join uacurso in Context.UnidadAcademica on cpa.IdCursoPeriodoAcademico equals uacurso.IdCursoPeriodoAcademico
                                join uasubarea in Context.UnidadAcademica on uacurso.IdUnidadAcademicaPadre equals uasubarea.IdUnidadAcademica
                                join uaarea in Context.UnidadAcademica on uasubarea.IdUnidadAcademicaPadre equals uaarea.IdUnidadAcademica
                                join d in Context.Sede on rd.IdSede equals d.IdSede
                                where rd.IdReunionDelegado == IdReunion
                                && uaarea.IdUnidadAcademica == IdArea
                                && uacurso.Nivel == 3
                                && cpa.IdSubModalidadPeriodoAcademico == rd.IdSubModalidadPeriodoAcademico
                            select new SelectListItem { Text = cu.NombreEspanol, Value = cu.IdCurso.ToString() }).OrderBy(x => x.Text).Distinct().ToList();

            return Json(LstCursos, JsonRequestBehavior.AllowGet);
        }

    }
}