﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Logic.Areas.Professor.IfcManagement.Export;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.PreSchedule;
using UPC.CA.ABET.Presentation.Areas.Meeting.Resources.Views.Shared;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.PreSchedule;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using System.Data.Entity.Validation;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
//using ConstantHelpers.CULTURE = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;

namespace UPC.CA.ABET.Presentation.Areas.Meeting.Controllers
{
    public class PreScheduleController : BaseController
    {
        /*
       GedServices GedServices; 
        
        public PreScheduleController(GedServices xxxx)
        {
            GedServices = xxxx;
        }
        */
        public string CurrentCulture { get; set; }
        public ActionResult Index()
        {
            return RedirectToAction("Consult");
        }


        public ActionResult Consult(PreSchedulesViewModel model)
        {
            model.CargarDatos(CargarDatosContext());
            return View(model);
        }
        const string LogoUpc = "logo-upc-red.png";
        [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite, AppRol.Acreditador, AppRol.Docente)]
        public ActionResult GetReportPdf(int IdReunionProfesores)
        {
            var now = DateTime.Now;
            var formatDateTimeNow = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;
                ExportWorkerARP pdfExport = new ExportWorkerARP(Context, IdReunionProfesores, Server.MapPath("~/Areas/Professor/Content/assets/images/") + LogoUpc, CurrentCulture);
                string directory = Path.Combine(Server.MapPath(ConstantHelpers.PROFESSOR.SERVER_FILES_DIRECTORY), ConstantHelpers.PROFESSOR.IFC_DIRECTORY, "PDF");
                Directory.CreateDirectory(directory);
                string path = pdfExport.GetFile(ExportFileFormat.Pdf, directory);
                string filename = Path.GetFileName(path);
                return File(path, "application/pdf", filename);
            }
            catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = formatDateTimeNow.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                throw;
            }
        }

        // GET: Meeting/PreSchedule
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult ConsultOld(int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, int? idArea, bool? isSearchRequest = true, Int32 p = 1)
        {
            var viewModel = new ConsultPreScheduleViewModel();
            viewModel.CargarDatos(CargarDatosContext(), idNivel, idSemana, FechaInicio, FechaFin, idSede, isSearchRequest, idArea, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO,p);
            if (viewModel.ListaPreAgendaResultado.Count == 0 && isSearchRequest != null && (bool)isSearchRequest)
            {
                PostMessage(MessageType.Info, PreScheduleResource.SinResultados);
            }
            return View(viewModel);
        }

        public ActionResult addPassdata()
        {
            var viewModel = new ConsultPreScheduleViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult addPassdata(ConsultPreScheduleViewModel consultPreScheduleViewModel)
        {
            var viewModel = new ConsultPreScheduleViewModel();
            return View(viewModel);
        }


        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult ActualizarUnidadesAcademicas(int idNivel)
        {
            return Json(new SelectList(GedServices.GetAllUnidadesByUsuarioAndNivelServices(Context, Session, idNivel), "IdUnidadAcademica", currentCulture == ConstantHelpers.CULTURE.INGLES ? "NombreIngles" : "NombreEspanol"));
        }

       


        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult CreateReunionDocente(ConsultMeetingPreScheduleViewModel vm)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var reunion = Context.ReunionProfesor.Where(x => x.IdReunionProfesor == vm.IdReunionProfesor).FirstOrDefault();

                List<ReunionProfesorParticipante> newrpp = new List<ReunionProfesorParticipante>();
                List<ReunionProfesorParticipante> delrpp = new List<ReunionProfesorParticipante>();
                List<Int32> IdsDocentesTable = new List<Int32>();

                reunion.NombreReunion = vm.NombreReunion;
                reunion.Inicio = DateTime.Parse(vm.Inicio.ToString());
                reunion.Fin = DateTime.Parse(vm.Fin.ToString());
                reunion.Lugar = vm.Lugar;

                Context.SaveChanges();

                PostMessage(Helpers.MessageType.Success, MessageResource.LaAgendaFueExitosamenteConfigurada);
                return RedirectToAction("Index", "TeacherMeeting");
            }

            catch (DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(Helpers.MessageType.Error, MessageResource.LaAgendaNoPudoSerConfigurada);
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return RedirectToAction("Index", "TeacherMeeting");
            }    
        }


        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Create()
        {
            int idDocenteSesion = (int)SessionHelper.GetDocenteId(Session);
            //int idPeriodo = (int)SessionHelper.GetPeriodoAcademicoId(Session);
            int idSubModalidadPeriodo = (int)SessionHelper.GetSubModalidadPeriodoAcademicoId(Session);
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            //if (!GedServices.DocentePerteneceOrganigrama(context, idDocenteSesion , idPeriodo, escuelaId))
            if (!GedServices.DocentePerteneceOrganigrama(Context, idDocenteSesion , idSubModalidadPeriodo, idEscuela))
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoTienePermisosSuficientesParaCrearUnaPreagenda);
                return RedirectToAction("Consult");

            }

            var viewModel = new CreatePreScheduleViewModel();
            viewModel.Niveles = GedServices.GetNivelesUsuarioLogueadoServices(Context, Session).Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            viewModel.Sedes = GedServices.GetAllSedesServices(Context).Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            string nombreUnidadAcademica = String.Empty;
            int nivel = GedServices.GetNivelMaximoUsuarioLogueadoServices(Context, Session.GetDocenteId(), Session);
            switch (nivel)
            {
                case 0:
                    nombreUnidadAcademica = "Escuela";
                    break;
                case 1:
                    nombreUnidadAcademica = Filtros.Area;
                    break;
                case 2:
                    nombreUnidadAcademica = Filtros.Subarea;
                    break;
                case 3:
                    nombreUnidadAcademica = Filtros.Curso;
                    break;
            }
            viewModel.TituloUnidadAcademica = nombreUnidadAcademica;
            viewModel.UnidadesAcademicas = GedServices.GetAllUnidadesByUsuarioAndNivelServices(Context, Session, nivel)
                .Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            return View(viewModel);
        }

     

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Create(CreatePreScheduleViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (GedServices.CrearPreAgendaServices(Context, Session, model.PreAgenda, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO) != null)
                {
                    PostMessage(MessageType.Success, PreScheduleResource.PreAgendaCreada);
                    return RedirectToAction("Index");
                }
                else
                    PostMessage(MessageType.Error, PreScheduleResource.FechaNoPerteneceCiclo);
            }
            model.Niveles = GedServices.GetNivelesUsuarioLogueadoServices(Context, Session).Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            model.Sedes = GedServices.GetAllSedesServices(Context).Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            string nombreUnidadAcademica = String.Empty;
            int nivel = GedServices.GetNivelMaximoUsuarioLogueadoServices(Context, Session.GetDocenteId(), Session);
            switch (nivel)
            {
                case 0:
                    nombreUnidadAcademica = "Escuela";
                    break;
                case 1:
                    nombreUnidadAcademica = Filtros.Area;
                    break;
                case 2:
                    nombreUnidadAcademica = Filtros.Subarea;
                    break;
                case 3:
                    nombreUnidadAcademica = Filtros.Curso;
                    break;
            }
            model.TituloUnidadAcademica = nombreUnidadAcademica;
            model.UnidadesAcademicas = GedServices.GetAllUnidadesByUsuarioAndNivelServices(Context, Session, nivel)
                .Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            return View(model);
        }

        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Consult");
            }

            var idEscuela = Session.GetEscuelaId();
            var viewModel = new CreatePreScheduleViewModel();
            viewModel.PreAgenda = GedServices.GetPreAgendaServices(Context, id.Value);
            if (viewModel.PreAgenda == null)
                return RedirectToAction("Consult");
            viewModel.Niveles = GedServices.GetAllNivelesUnidadesAcademicasServices(Context, idEscuela)
                .Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            viewModel.Sedes = GedServices.GetAllSedesServices(Context)
                .Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            IEnumerable<UnidadAcademica> lstUnidadAcademica = null;
            string nombreUnidadAcademica = String.Empty;
            switch (viewModel.PreAgenda.Nivel)
            {
                case 0 :
                    nombreUnidadAcademica = "Escuela";
                    lstUnidadAcademica = Context.UnidadAcademica.Where(x => x.Nivel == 0).ToList();
                    break;

                case 1:
                    nombreUnidadAcademica = Filtros.Area;
                    lstUnidadAcademica = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 2:
                    nombreUnidadAcademica = Filtros.Subarea;
                    lstUnidadAcademica = GedServices.GetAllSubareasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 3:
                    nombreUnidadAcademica = Filtros.Curso;
                    lstUnidadAcademica = GedServices.GetAllCursosServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
            }
            viewModel.TituloUnidadAcademica = nombreUnidadAcademica;
            viewModel.UnidadesAcademicas = lstUnidadAcademica
                .Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            return View(viewModel);
        }
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Consult");
            }
            var idEscuela = Session.GetEscuelaId();
            Int32 IdPreAgendaCreador = Context.PreAgenda.FirstOrDefault(x => x.IdPreAgenda == id.Value).IdUsuarioCreador.Value;

            if (Session.GetDocenteId() != IdPreAgendaCreador)
            {
                PostMessage(MessageType.Warning, " " + MessageResource.NoTienePermisosParaEditarPreagendaSoloUsuarioCreadorPuedeEditar);
                return RedirectToAction("Consult");
            }

            var viewModel = new CreatePreScheduleViewModel();
            viewModel.PreAgenda = GedServices.GetPreAgendaServices(Context, id.Value);
            if (viewModel.PreAgenda == null)
                return RedirectToAction("Consult");
            viewModel.Niveles = GedServices.GetAllNivelesUnidadesAcademicasServices(Context, idEscuela)
                .Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            viewModel.Sedes = GedServices.GetAllSedesServices(Context)
                .Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            IEnumerable<UnidadAcademica> lstUnidadAcademica = null;
            string nombreUnidadAcademica = String.Empty;
            switch (viewModel.PreAgenda.Nivel)
            {
                case 0:
                    nombreUnidadAcademica = "Escuela";
                    lstUnidadAcademica = Context.UnidadAcademica.Where(x => x.Nivel == 0).ToList();
                    break;
                case 1:
                    nombreUnidadAcademica = Filtros.Area;
                    lstUnidadAcademica = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 2:
                    nombreUnidadAcademica = Filtros.Subarea;
                    lstUnidadAcademica = GedServices.GetAllSubareasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 3:
                    nombreUnidadAcademica = Filtros.Curso;
                    lstUnidadAcademica = GedServices.GetAllCursosServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
            }
            viewModel.TituloUnidadAcademica = nombreUnidadAcademica;
            viewModel.UnidadesAcademicas = lstUnidadAcademica
                .Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = CargarDatosContext().currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreatePreScheduleViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (GedServices.ActualizarPreAgendaServices(Context, Session, model.PreAgenda, ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO))
                {
                    PostMessage(MessageType.Success, PreScheduleResource.PreAgendaEditada);
                    return RedirectToAction("Consult");
                }
                else
                    ModelState.AddModelError("", PreScheduleResource.FechaNoPerteneceCiclo);
            }
            var idEscuela = Session.GetEscuelaId();

            model.Niveles = GedServices.GetAllNivelesUnidadesAcademicasServices(Context, idEscuela)
                .Select(c => new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            model.Sedes = GedServices.GetAllSedesServices(Context)
                .Select(c => new SelectListItem { Value = c.IdSede.ToString(), Text = c.Nombre });
            IEnumerable<UnidadAcademica> lstUnidadAcademica = null;
            string nombreUnidadAcademica = String.Empty;
            switch (model.PreAgenda.Nivel)
            {
                case 0:
                    nombreUnidadAcademica = "Escuela";
                    lstUnidadAcademica = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 1:
                    nombreUnidadAcademica = Filtros.Area;
                    lstUnidadAcademica = GedServices.GetAllAreasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 2:
                    nombreUnidadAcademica = Filtros.Subarea;
                    lstUnidadAcademica = GedServices.GetAllSubareasServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
                case 3:
                    nombreUnidadAcademica = Filtros.Curso;
                    lstUnidadAcademica = GedServices.GetAllCursosServices(Context, idEscuela, @Session.GetModalidadId());
                    break;
            }
            model.TituloUnidadAcademica = nombreUnidadAcademica;
            model.UnidadesAcademicas = lstUnidadAcademica.Select(c => new SelectListItem { Value = c.IdUnidadAcademica.ToString(), Text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles });
            return View(model);
        }
        [HttpGet]
        public ActionResult CreateReunionDocente(int IdReunionProfesor)
        {
            ConsultMeetingPreScheduleViewModel vm = new ConsultMeetingPreScheduleViewModel();
            var language = Session.GetCulture().ToString();
            vm.CargarDatos(Context, IdReunionProfesor, language);
           
            return PartialView("_AddReunionDocenteView", vm);
        }

        [HttpGet]
        public ActionResult ViewReunionProfesor(int IdReunionProfesores)
        {
            ConsultMeetingPreScheduleViewModel vm = new ConsultMeetingPreScheduleViewModel();
            var language = Session.GetCulture().ToString();
            vm.CargarDatos(Context, IdReunionProfesores, language);

            return PartialView("_ViewReunionDocenteView", vm);
        }


        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Delete(int? id)
        {
            if (GedServices.EliminarPreAgendaServices(Context, id.Value))
                PostMessage(MessageType.Info, PreScheduleResource.PreAgendaEliminada);
            else
                PostMessage(MessageType.Error, PreScheduleResource.PreAgendaEliminadaMensajeError);
            return RedirectToAction("Consult");
        }

        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult Agenda(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Consult");
            }
            var viewModel = new AgendaPreScheduleViewModel();
            var preAgenda = GedServices.GetPreAgendaServices(Context, id.Value);
            if (preAgenda == null)
            {
                return RedirectToAction("Consult");
            }
            viewModel.IdPreAgenda = preAgenda.IdPreAgenda;
            viewModel.Motivo = preAgenda.Motivo;
            viewModel.ListaTemasPreAgenda = GedServices.GetAllTemasPreAgendaServices(Context, id.Value);
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult AddTemaAgenda(string nombreTema, int idPreAgenda)
        {
            var temaPreAgenda = new TemaPreAgenda();
            temaPreAgenda.DescripcionTemaPreAgenda = nombreTema;
            temaPreAgenda.IdPreAgenda = idPreAgenda;
            int idTema = GedServices.CrearTemaPreAgendaServices(Context, temaPreAgenda);
            return Json(idTema);
        }

        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult DeleteTemaAgenda(int id)
        {
            if (GedServices.EliminarTemaPreAgendaServices(Context, id))
                return Json(true);
            else
                return Json(false);
        }

        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult DeletePreAgendasExtraordinariasNoUtilizadas()
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                GedServices.EliminarPreAgendasExtraordinariasNoUtilizadasServices(Context);
                return Json(true);
            }
            catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return Json(false);
            }
        }

        [HttpPost]
        [AuthorizeUserAttribute(AppRol.MiembroComite, AppRol.Docente)]
        public ActionResult FechasSemana(int? semana)
        {
            var aux = GedServices.GetDateStartWeekCurrentPeriodoAcademicoBySemanaServices(Context, Session, semana.HasValue == true ? semana.Value : 0);
            return Json(aux);
        }  

        [HttpPost]
        public string SendEmail(int nivel, string semana, string area, string correo, string lugar, DateTime fechafin, int idreunionprofesores)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            string Asunto = "Reunión de profesores de nivel " + nivel.ToString() + " del área " + area + " de la " + semana;
            string Mensaje = "<p style=\"text-align: justify; \">Estimado Profesor,</p><p style=\"text-align: justify; \"><br></p><p style=\"text-align: justify; \">La Escuela de Ingeniería de Sistemas y Computación (EISC) Lo invita a usted a participar en la reunión del área de  <i> <b>" + area.ToUpper() + "</b></i>  de la <i> <b>" + semana.ToUpper() + " </b></i> a realizarse el día <i> <b>" + fechafin.ToShortDateString() + "</b></i> con lugar de reunión en <i> <b>" + lugar.ToString() + "</b></i>. Usted podrá visualizar la reunión en el siguiente enlace: </p>";

            EmailGRALogic mailLogic = new EmailGRALogic();

            try
            {

                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                //   String Ruta = "Areas/Report/Resources/WebPages/ReportPlanMejoraCurso.aspx";
                String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/"); // + Ruta + queryString);
                String urlImage = String.Format("{0}://{1}{2}",
                                                Request.Url.Scheme,
                                                Request.Url.Authority, vdirectory);

                bool enviado = mailLogic.SendMailArc(Asunto, mailLogic.GetHtmlEmail(Mensaje, urlSurvey, urlImage + "/Areas/Meeting/Resources/Images/MailHeaderReunionProfesor.png", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), correo);
                if (enviado == false)
                {
                    return ("Error");
                }
                else
                {
                    return ("Enviado!");
                }
            }
            catch (Exception ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                Context.LogErrores.Add(newErrorLog);
                Context.SaveChanges();
                return (ex.InnerException.Message);
            }

        }
    }
}