﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration;
using UPC.CA.ABET.Presentation.Helper;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Usuario, AppRol.Invitado)]
    [OutputCache(Duration = 3600, VaryByParam = "none")]
    public class JsonController : BaseController
    {
        public readonly LoadCombosLogic loadCombosLogic;

        public JsonController()
        {
            this.loadCombosLogic = new LoadCombosLogic(Context);

        }
        // GET: Survey/Json
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEPEStudentsCode()
        {
            int ModalidadId = Session.GetModalidadId();
            var viewmodel = new GetEPEStudentsCodeViewModel();
            viewmodel.CargarDatos(CargarDatosContext(), ModalidadId);
            var data = viewmodel.LstCodigos;
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentInformation(Int32 IdAlumno)
        {
            var alumno = Context.Alumno.Where(x => x.IdAlumno == IdAlumno).FirstOrDefault();
            alumno.CorreoElectronico = GetCorreo(alumno.Codigo);
            Context.SaveChanges();

            var data = (from a in Context.Alumno
                        where a.IdAlumno == IdAlumno
                        select new { Nombres = a.Nombres, Apellidos = a.Apellidos, Carrera = a.Carrera.NombreEspanol, Correo = a.CorreoElectronico }).ToList();




            return Json(data, JsonRequestBehavior.AllowGet);
        }
        private string GetCorreo(string codigo)
        {
            string correo = "";

            int anio = Convert.ToInt32(codigo.Substring(0, 4));

            switch (codigo)
            {
                case "199710627":
                    correo = "a199710627@upc.edu.pe";
                    break;
                default:
                    {
                        if (anio < 2000)
                        {

                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "a" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2000 && anio < 2010)
                        {
                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "u" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2010)
                        {
                            correo = "u" + codigo + "@upc.edu.pe";
                        }
                    }
                    break;
            }
            return correo;
        }

        public JsonResult GetComentariosHallazgo(Int32 HallazgoId)
        {
            //var listComentarioId = (from ch in context.ComentarioHallazgo
            //                     where ch.IdHallazgo == HallazgoId
            //                     select ch.IdComentarioDelegado).ToList();


            var lstComentario = (from ch in Context.ComentarioHallazgo
                                 join cod in Context.ComentarioDelegado on ch.IdComentarioDelegado equals cod.IdComentarioDelegado
                                 where ch.IdHallazgo == HallazgoId
                                 select cod).ToList();

            List<ComentarioDelegado> lstcomentariodelegado = new List<ComentarioDelegado>();

            foreach (var comentario in lstComentario)
            {
                var tipopregunta = Context.TipoPregunta.Where(x => x.IdTipoPregunta == comentario.Pregunta.IdTipoPregunta).FirstOrDefault();

                ComentarioDelegado ocd = new ComentarioDelegado();

                if (tipopregunta.DescripcionEspanol == "Pregunta con Comentario")
                {
                    ocd.DescripcionEspanol = comentario.DescripcionEspanol;
                    ocd.CodigoComentario = comentario.CodigoComentario;
                    ocd.Alumno = comentario.Alumno;
                    lstcomentariodelegado.Add(ocd);
                }
                else
                if (tipopregunta.DescripcionEspanol == "Pregunta Escala")
                {
                    ocd.DescripcionEspanol = Context.NivelSatisfaccion.FirstOrDefault(x => x.IdNivelSatisfaccion == comentario.IdNivelSatisfaccion).DescripcionEspanol;
                    ocd.CodigoComentario = comentario.CodigoComentario;
                    ocd.Alumno = comentario.Alumno;
                    lstcomentariodelegado.Add(ocd);

                }

            }

            var data = (from a in lstcomentariodelegado
                        select new { CodComentario = a.CodigoComentario, Alumno = a.Alumno.Nombres + " " + a.Alumno.Apellidos, Comentario = a.DescripcionEspanol }).ToList();


            return Json(data, JsonRequestBehavior.AllowGet);





        }


        [HttpPost]
        public JsonResult GetCycleByModalidad(Int32 ModalityId)
        {
            var IdSM = Context.SubModalidad.Where(x => x.IdModalidad == ModalityId && x.NombreEspanol == "Regular").FirstOrDefault().IdSubModalidad;
            var data = Context.SubModalidadPeriodoAcademico
                        //.Where(x => x.IdSubModalidad == IdSM && x.PeriodoAcademico.Estado == "ACT")
                        .Where(x => x.IdSubModalidad == IdSM)
                        .OrderByDescending(x => x.IdSubModalidadPeriodoAcademico)
                        .Select(x => new { key = x.IdSubModalidadPeriodoAcademico, value = x.PeriodoAcademico.CicloAcademico })
                        .ToList();
            return Json(data);
        }


        public JsonResult GetOutcomeByComision(Int32? IdComision)
        {
            var data = Context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdComision == IdComision).Select(x => new { NombreEspanol = x.Outcome.Nombre }).ToList();
            return Json(data);
        }
        [HttpPost]
        public JsonResult GetOutcomesByTipoOutcome(Int32 IdTipoOutcome, Int32 IdCarrera)
        {
            var data = Context.uspGetOutcomesHistoricosEncuesta(IdTipoOutcome, IdCarrera, currentCulture).Select(x => new OutcomeEncuestaConfig { NombreEspanol = x.Key }).ToList();
            return Json(data);

        }
        [HttpPost]
        public JsonResult GetCursoBySedePeriodo(Int32? IdPeriodoAcademico)
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            {
                var data = Context.spGetCursosEncuestaLCFC(IdPeriodoAcademico, String.Empty).Select(x => new { id = x.IdCurso, text = x.NombreEspanol + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
            else
            {
                var data = Context.spGetCursosEncuestaLCFC(IdPeriodoAcademico, String.Empty).Select(x => new { id = x.IdCurso, text = x.NombreIngles + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
        }
        [HttpPost]
        public JsonResult ReordenarOutcomes(List<String> lstOrden, String TipoEncuesta, String TipoOutcome)
        {
            try
            {
                for (int i = 0; i < lstOrden.Count; i++)
                {
                    var Id = lstOrden[i].ToInteger();
                    var Outcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == Id);
                    Outcome.Orden = i + 1;
                }
                Context.SaveChanges();
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }
        public class ComisionClass
        {
            public int IdComision { get; set; }
            public string Codigo { get; set; }
        }

        [HttpPost]
        public JsonResult GetFiltroHasta(Int32 IdDesde)
        {
            int parModalidadId = Session.GetModalidadId();
            var desde = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdDesde).CicloAcademico.ToInteger();
            var lstPeriodos = (from pa in Context.PeriodoAcademico
                               join submodapa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                               join submoda in Context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                               where (submoda.IdModalidad == parModalidadId)
                               select pa).ToList();


            List<PeriodoAcademico> data = new List<PeriodoAcademico>();

            foreach (var item in lstPeriodos)
            {
                if (item.CicloAcademico.ToInteger() > desde)
                    data.Add(item);
            }
            data = data.Select(x => new PeriodoAcademico { IdPeriodoAcademico = x.IdPeriodoAcademico, CicloAcademico = x.CicloAcademico }).ToList();
            return Json(data);
        }


        private class ResponseComisionAndPA
        {
            public List<PeriodoAcademico> Data { get; set; }
            public List<ComisionClass> Comision { get; set; }

        }

        [HttpPost]
        public JsonResult GetFilterComissionAndAcademicPeriod(int IdCarrera, int IdDesde)
        {
            // Se obtine el nombre de la modalidad
            var ModalityName = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? Session.GetNombreModalidad().Replace("Pregrado ", "") : Session.GetNombreModalidad().Replace("Undergraduate ", "");
            //ModalityName = ModalityName.Replace("Pregrado ", "");
            // Se obtiene el id de la modalidad
            int parModalidadId = Session.GetModalidadId();
            // Se obtiene el Id de la  Submodalidad
            int subModalidadId = Context.SubModalidad.Where(x => x.IdModalidad == parModalidadId && (x.NombreEspanol == ModalityName || x.NombreIngles == ModalityName)).Select(x => x.IdSubModalidad).FirstOrDefault();
            // Se obtiene el ciclo académico inicial
            var desde = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdDesde).CicloAcademico.ToInteger();
            // Se obtiene si el ciclo pertenece o no a Arizona
            var isArizona = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdDesde && x.EsMallaArizona == true).FirstOrDefault() == null ? false : true;
            // Se obtiene la lista de periodos hasta donde se pueda seleccionar filtrado por la modalidad y si pertenece a Arizona o no
            var lstPeriodos = (from pa in Context.PeriodoAcademico
                               join submodapa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                               join submoda in Context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                               where (submoda.IdModalidad == parModalidadId && submodapa.EsMallaArizona == isArizona)
                               select pa).ToList();

            // Se Filtra la comision dependiendo la submodalidad , la acreditadora y la carrera 
            var Lstcomision = (from cc in Context.CarreraComision
                               join com in Context.Comision on cc.IdComision equals com.IdComision
                               join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                               join ac in Context.Acreditadora on apa.IdAcreditadora equals ac.IdAcreditadora
                               join spa in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals spa.IdSubModalidadPeriodoAcademico
                               join pa in Context.PeriodoAcademico on spa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                               join sm in Context.SubModalidad on spa.IdSubModalidad equals sm.IdSubModalidad
                               join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                               where cc.IdCarrera == IdCarrera
                                       && ac.Nombre == "ABET"
                                       && spa.IdPeriodoAcademico == IdDesde
                                       && m.IdModalidad == parModalidadId
                                       && sm.IdSubModalidad == subModalidadId
                               select cc)
                            .OrderBy(x => x.Comision.Codigo).ToList()
                            .Select(x => new ComisionClass { Codigo = currentCulture.Equals("es-PE") ? x.Comision.NombreEspanol : x.Comision.NombreIngles, IdComision = x.IdComision }).ToList(); ;


            List<PeriodoAcademico> data = new List<PeriodoAcademico>();
            //ResponseComisionAndPA response = new ResponseComisionAndPA();

            foreach (var item in lstPeriodos)
            {
                if (item.CicloAcademico.ToInteger() > desde)
                    data.Add(item);
            }
            data = data.Select(x => new PeriodoAcademico { IdPeriodoAcademico = x.IdPeriodoAcademico, CicloAcademico = x.CicloAcademico }).ToList();
            //response.Data = data;

            //var ltcomision = new List<ComisionClass>();
            //foreach (var item in Lstcomision)
            //{
            //    ltcomision.Add(new ComisionClass
            //    {
            //        Codigo = item.Codigo,
            //        IdComision = item.IdComision
            //    });
            //}
            //response.Comision = ltcomision;
            return Json(new { data, Lstcomision });
        }

        [HttpPost]
        public JsonResult GetFilterOutcomePerComission(int IdComision, int IdDesde) {

            // Se obtiene si el ciclo pertenece o no a Arizona
            var isArizona = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdDesde && x.EsMallaArizona == true).FirstOrDefault() == null ? false : true;

            var outcomes = (from ouc in Context.OutcomeComision
                            join ou in Context.Outcome on ouc.IdOutcome equals ou.IdOutcome
                            join com in Context.Comision on ouc.IdComision equals com.IdComision
                            join apa in Context.AcreditadoraPeriodoAcademico on com.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                            join spa in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals spa.IdSubModalidadPeriodoAcademico
                            where ouc.IdComision == IdComision && spa.EsMallaArizona == isArizona
                            select ou.Nombre).ToList();

            return Json(outcomes);
        }

        [HttpPost]
        public JsonResult GetEncuestasMantenimientoPPP(GetEncuestasMantenimientoPPPViewModel model)
        {
            model.Filter(CargarDatosContext());
            return Json(model.lstEncuesta);
        }
        [HttpPost]
        public JsonResult GetLogroByCurso(Int32 cursoId, String idioma)
        {
            var data = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == cursoId);
            return Json(idioma == ConstantHelpers.CULTURE.ESPANOL ? data.LogroFinCicloEspanol : data.LogroFinCicloIngles);
        }
        public JsonResult GetCicloByCarrera(Int32? carreraId)
        {
            var model = new GetCicloByCarreraViewModel();
            model.Filter(CargarDatosContext(), carreraId);
            return Json(model.lstPeriodoAcademico);
        }
        [HttpPost]
        public JsonResult GetAlumno(String q, Int32? IdCarrera)
        {
            var IdCiclo = Session.GetPeriodoAcademicoId();
            var data = Context.Alumno.Where(x => x.Codigo.Contains(q) && x.AlumnoMatriculado.FirstOrDefault(y => y.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && y.IdCarrera == IdCarrera) != null)
                                     .OrderBy(x => x.Codigo).ToList().Select(x => new { id = x.Codigo, text = x.Codigo });
            return Json(data);
        }
        [HttpPost]
        public JsonResult FindStudentName(String content, Int32? IdCarrera)
        {
            int parModalidadId = Session.GetModalidadId();
            var IdSubModalidadPeriodoAcademicoActivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var IdEscuela = Session.GetEscuelaId();
            if (IdCarrera.HasValue)
            {
                var data = Context.Alumno.Where(x => (x.Nombres + " " + x.Apellidos).Contains(content) && x.AlumnoMatriculado.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademicoActivo && y.Carrera.IdEscuela == IdEscuela && y.Carrera.IdCarrera == IdCarrera) != null)
                                     .OrderBy(x => x.Codigo).ToList().Select(x => new { id = x.IdAlumno, text = x.Nombres + " " + x.Apellidos });
                return Json(data);
            }
            else
            {

                var data = Context.Alumno.Where(x => (x.Nombres + " " + x.Apellidos).Contains(content) && x.AlumnoMatriculado.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademicoActivo && y.Carrera.IdEscuela == IdEscuela) != null)
                                     .OrderBy(x => x.Codigo).ToList().Select(x => new { id = x.IdAlumno, text = x.Nombres + " " + x.Apellidos });

                return Json(data);
            }
        }
        [HttpPost]
        public JsonResult FindStudentCode(String content, Int32? IdCarrera)
        {
            int parModalidadId = Session.GetModalidadId();
            var IdSubModalidadPeriodoAcademicoActivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var IdEscuela = Session.GetEscuelaId();

            if (IdCarrera.HasValue)
            {
                var data = Context.Alumno.Where(x => x.Codigo.Contains(content) && x.AlumnoMatriculado.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademicoActivo && y.Carrera.IdEscuela == IdEscuela && y.Carrera.IdCarrera == IdCarrera) != null)
                                     .OrderBy(x => x.Codigo).ToList().Select(x => new { id = x.IdAlumno, text = x.Codigo });
                return Json(data);
            }
            else
            {

                var data = Context.Alumno.Where(x => x.Codigo.Contains(content) && x.AlumnoMatriculado.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademicoActivo && y.Carrera.IdEscuela == IdEscuela) != null)
               .OrderBy(x => x.Codigo).ToList().Select(x => new { id = x.IdAlumno, text = x.Codigo });

                return Json(data);
            }
        }
        [HttpPost]
        public JsonResult GetAcreditadoraByCiclo(Int32 IdPeriodoAcademico)
        {
            var data = Context.ListarComboAcreditacion(IdPeriodoAcademico).Select(x => new Acreditadora { IdAcreditadora = x.Key, Nombre = x.Value }).ToList();
            return Json(data);
        }
        [HttpPost]
        public JsonResult GetCursoAjaxByCiclo(String q, Int32 IdPeriodoAcademico)
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            {
                var data = Context.spGetCursosEncuestaLCFC(IdPeriodoAcademico, q).Select(x => new { id = x.IdCurso, text = x.NombreEspanol + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
            else
            {
                var data = Context.spGetCursosEncuestaLCFC(IdPeriodoAcademico, q).Select(x => new { id = x.IdCurso, text = x.NombreIngles + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
        }
        [HttpPost]
        public JsonResult GetCursoAjax(String q)
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            {
                var data = Context.spGetCursosEncuestaLCFC(null, q).Select(x => new { id = x.IdCurso, text = x.NombreEspanol + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
            else
            {
                var data = Context.spGetCursosEncuestaLCFC(null, q).Select(x => new { id = x.IdCurso, text = x.NombreIngles + "-" + x.Codigo_Curso }).ToList();
                return Json(data);
            }
        }

        [HttpPost]
        public JsonResult GetCursos()
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            {
                var data = Context.Curso.Select(x => new { id = x.IdCurso, text = x.NombreEspanol + "-" + x.Codigo }).ToList();
                return Json(data);
            }
            else
            {
                var data = Context.Curso.Select(x => new { id = x.IdCurso, text = x.NombreEspanol + "-" + x.Codigo }).ToList();
                return Json(data);
            }
        }

        public JsonResult GetAlumnoByCodigo(String Codigo)
        {
            var matriculado = Context.AlumnoMatriculado.Include(x => x.Alumno).FirstOrDefault(x => x.Alumno.Codigo.Equals(Codigo));
            if (matriculado != null)
            {
                var data = new
                {
                    Nombre = matriculado.Alumno.Nombres + " " + matriculado.Alumno.Apellidos,
                    CarreraId = matriculado.IdCarrera,
                    NombreCarrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? matriculado.Carrera.NombreEspanol : matriculado.Carrera.NombreIngles,
                    CicloId = matriculado.SubModalidadPeriodoAcademico.IdPeriodoAcademico,
                    NombreCiclo = matriculado.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    SedeId = matriculado.IdSede,
                    NombreSede = matriculado.Sede == null ? "" : matriculado.Sede.Nombre
                };
                return Json(data);
            }
            return Json(null);
        }

        public JsonResult GetDocentesByCurso(string IdCurso, string IdAlumno)
        {
            var viewmodel = new GetDocentesByCursoViewModel();
            int EVDId = session.GetEncuestaVirtualDelegadoId();
            viewmodel.IdAlumno = IdAlumno.ToInteger();
            viewmodel.Filter(CargarDatosContext(), IdCurso.ToInteger(), EVDId);
            var data = viewmodel.LstDocente;
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetAlumnoById(Int32 IdAlumno)
        {

            int parModalidadId = Session.GetModalidadId();

            var idsubmodalidadperiodoacademicoactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;


            var matriculado = Context.AlumnoMatriculado.Include(x => x.Alumno).FirstOrDefault(x => x.IdAlumno == IdAlumno && x.IdSubModalidadPeriodoAcademico == idsubmodalidadperiodoacademicoactivo);

            if (matriculado != null)
            {
                var data = new
                {
                    IdAlumno = matriculado.IdAlumno,
                    Nombre = matriculado.Alumno.Nombres + " " + matriculado.Alumno.Apellidos,
                    Codigo = matriculado.Alumno.Codigo,
                    IdCarrera = matriculado.IdCarrera,
                    NombreCarrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? matriculado.Carrera.NombreEspanol : matriculado.Carrera.NombreIngles
                };
                return Json(data);
            }
            return Json(null);
        }

        public JsonResult SaveNotificationGRA(Int32 IdAlumno, Int32 IdCarrera)
        {
            try
            {
                int parModalidadId = Session.GetModalidadId();
                NotificacionEncuestaAlumno objNotificacion = new NotificacionEncuestaAlumno();
                objNotificacion.IdAlumno = IdAlumno;
                objNotificacion.IdCarrera = IdCarrera;
                int? idsubmodalidadperiodoacademicoactivo = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == parModalidadId && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;

                if (idsubmodalidadperiodoacademicoactivo != null)
                    //objNotificacion.IdPeriodoAcademico = Session.GetPeriodoAcademicoId().Value;
                    objNotificacion.IdSubModalidadPeriodoAcademico = idsubmodalidadperiodoacademicoactivo;
                objNotificacion.Estado = false;
                Context.NotificacionEncuestaAlumno.Add(objNotificacion);
                Context.SaveChanges();
                var IdNotificacion = objNotificacion.IdNotificacion;
                return Json(IdNotificacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult DeleteNotificationGRA(Int32 IdNotificacion)
        {
            try
            {
                var objNotificacion = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == IdNotificacion);
                Context.NotificacionEncuestaAlumno.Remove(objNotificacion);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult GetRazonSocial(String q)
        {
            try
            {
                var filtro = Context.Encuesta.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                && x.RazonSocial.Contains(q)).GroupBy(x => x.RazonSocial).Select(x => x.FirstOrDefault());
                var data = filtro.Select(x => new { id = x.RazonSocial, text = x.RazonSocial });
                return Json(data);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public JsonResult GetRucByRazonSocial(String RazonSocial)
        {
            var model = new GetRUCByRazonSocialViewModel();
            model.Filter(CargarDatosContext(), RazonSocial);
            return Json(model.RUC);
        }
        public JsonResult GetCarreraByCiclo(Int32? cicloId)
        {
            var model = new GetCarreraByCicloViewModel();
            model.Filter(CargarDatosContext(), cicloId);
            return Json(model.lstCarrera);
        }

        //NUEVO EVD


        public JsonResult TraduccionActualizar(string textoatraducir)
        {
            YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();

            string textotraducido = helper.Translate(textoatraducir);

            var data = new { Text = textotraducido };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetModuloByCiclo(int? IdPeriodoAcademico)
        {
            var model = new GetModulesByCicloViewModel();

            model.Filter(CargarDatosContext(), IdPeriodoAcademico);

            var data = model.LstModulo;

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuestionByModule(int? IdModulo)
        {
            var model = new GetQuestionByModuleViewModel();

            model.Filter(CargarDatosContext(), IdModulo);

            var data = model.LstPregunta;

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCursosByModulo(int? IdPregunta, int? IdModulo)
        {
            var model = new GetCursosByModuloViewModel();

            model.Fill(CargarDatosContext(), IdPregunta, IdModulo);

            var data = model.LstCurso;

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocenteByCursoModulo(int? IdModulo, int? IdCurso)
        {
            var model = new GetDocenteByCursoModuloViewModel();

            model.Fill(CargarDatosContext(), IdModulo, IdCurso);

            var data = model.LstDocente;

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //EVD
        public JsonResult GetSedeByCarrera(Int32? IdCarrera)
        {
            var model = new GetSedeByCarreraViewModel();
            model.Filter(CargarDatosContext(), IdCarrera);
            return Json(model.lstSede);
        }
        public JsonResult GetCicloBySede(Int32? sedeId)
        {
            var model = new GetCicloBySedeViewModel();
            model.Filter(CargarDatosContext(), sedeId);
            return Json(model.lstCiclo);
        }
        [HttpPost]
        public JsonResult GetCursoBySede(Int32? IdSede, Int32? IdPeriodoAcademico)
        {
            var language = currentCulture;
            List<GetSeccionCurso_Result> data = null;
            //var SeccionCurso = context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", SeccionC, CursoC, model.CicloId, null).FirstOrDefault();
            data = Context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", null, null, IdPeriodoAcademico, IdSede).GroupBy(x => new { x.IdCurso, x.NombreEnCurso, x.NombreEsCurso }).Select(x => x.FirstOrDefault()).ToList();
            return Json(data);
        }
        [HttpPost]
        public JsonResult GetSeccionByCursoFDC(Int32? cursoId, Int32? IdCarrera, Int32? IdSede, Int32? IdPeriodoAcademico)
        {
            var CodCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == cursoId).Codigo;
            var SubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;

            var data = Context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", null, CodCurso, SubModalidadPeriodoAcademico, IdSede).Select(x => new { IdSeccion = x.IdSeccion, Codigo = x.CodigoSeccion }).ToList();

            //var data = context.GetSeccionCurso(null,CodCurso,IdPeriodoAcademico,IdSede).Select( x => new Seccion{ IdSeccion = x.IdSeccion , Codigo = x.CodigoSeccion}).ToList();
            return Json(data);
        }
        [HttpPost]
        public JsonResult GetCursoByCiclo(Int32? cicloId)
        {
            var model = new GetCursoByCicloViewModel();
            model.Filter(CargarDatosContext(), cicloId);
            return Json(model.lstCurso);
        }
        public JsonResult GetSeccionByCurso(Int32? cursoId, Int32? IdCarrera, Int32? IdSede, Int32? IdPeriodoAcademico)
        {
            var model = new GetSeccionByCursoViewModel();
            model.Filter(CargarDatosContext(), cursoId, IdCarrera, IdSede, IdPeriodoAcademico);
            return Json(model.lstSeccion);
        }
        public JsonResult GetCarreraByAcreditadora(Int32? AcreditadoraId)
        {
            var model = new GetCarreraByAcreditadoraViewModel();
            model.Filter(CargarDatosContext(), AcreditadoraId);
            return Json(model.lstCarrera);
        }
        //public JsonResult GetComisionByCarrera(int IdCarrera = 0, int IdAcreditadora = 0, int IdPeriodoAcademico = 0)
        //{
        //    int idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
        //    var data = context.CarreraComision.Include(x => x.Comision)
        //                                        .Where(x => x.IdCarrera == IdCarrera &&
        //                                                    x.IdSubModalidadPeriodoAcademico == idsubmoda &&
        //                                                    x.Comision.Codigo != ConstantHelpers.COMISON_WASC)
        //                                        .OrderBy(x => x.Comision.Codigo).ToList()
        //                                        .Select(x => new { Codigo = currentCulture.Equals("es-PE") ? x.Comision.NombreEspanol : x.Comision.NombreIngles, IdComision = x.IdComision }).ToList();


        //    return Json(data);          

        //}

        public JsonResult GetComisionByCarrera(Int32? IdCarrera, Int32? IdAcreditadora, Int32? IdPeriodoAcademico)
        {
            int parModalidadId = Session.GetModalidadId();
            Int32 SubModalidadPeriodoAcademicoId;

            if (!IdPeriodoAcademico.HasValue)
            {
                IdPeriodoAcademico = Session.GetPeriodoAcademicoId();
                SubModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                SubModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            }
            var data = Context.CarreraComision.Include(x => x.Comision)
                                                .Where(x => x.IdCarrera == IdCarrera &&
                                                            x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId &&
                                                            x.Comision.Codigo != ConstantHelpers.COMISON_WASC)
                                                .OrderBy(x => x.Comision.Codigo).ToList()
                                                .Select(x => new { Codigo = currentCulture.Equals("es-PE") ? x.Comision.NombreEspanol : x.Comision.NombreIngles, IdComision = x.IdComision }).ToList();
            return Json(data);
        }




        public JsonResult GetCyclesForModality()
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCyclesForModality(ModalityId: ModalityId).ToOptions()
            };
        }
        public JsonResult GetCyclesFromForModality()
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCyclesFromForModality(ModalityId: ModalityId).ToOptions()
            };
        }
        public JsonResult GetCyclesToForModality(int CycleFromId = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCyclesToForModality(ModalityId: ModalityId, CycleFromId: CycleFromId).ToOptions()
            };
        }
        public JsonResult GetCampus(int CycleId = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCampus(CycleId).ToOptions()
            };
        }
        public ActionResult GetComisionByCarreraPPPOutcome(Int32? IdCarrera, Int32? IdTipoOutcome, Int32? IdOutcomeEncuestaPPPConfig, Int32? IdPeriodoAcademico, Int32? EscuelaId)
        {
            var model = new _AddPPPOutcomeViewModel();
            var query = (from cc in Context.CarreraComision
                         join c in Context.Carrera on cc.IdCarrera equals c.IdCarrera
                         join co in Context.Comision on cc.IdComision equals co.IdComision
                         join oc in Context.OutcomeComision on co.IdComision equals oc.IdComision
                         join o in Context.Outcome on oc.IdOutcome equals o.IdOutcome
                         //join oepo in Context.OutcomeEncuestaPPPOutcome on o.IdOutcome equals oepo.IdOutcome
                         //join oepc in Context.OutcomeEncuestaPPPConfig on oepo.IdOutcomeEncuestaPPPConfig equals oepc.IdOutcomeEncuestaPPPConfig
                         orderby co.Codigo
                         where (c.IdEscuela == EscuelaId && cc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && ((IdTipoOutcome == 1 && co.Codigo == "WASC") || (IdTipoOutcome == 2 && co.Codigo != "WASC")))
                         select new { cc.IdComision, c.IdCarrera });
            if (IdCarrera != null)
                query = query.Where(x => x.IdCarrera == IdCarrera);
            model.LstComisiones = query.Select(x => new ComisionOutcomes() { IdComision = x.IdComision }).Distinct().ToList();


            for (int i = 0; i < model.LstComisiones.Count; i++)
            {
                int IdComision = model.LstComisiones[i].IdComision;
                model.LstComisiones[i].comision = Context.Comision.FirstOrDefault(x => x.IdComision == IdComision);

                int idOutcome = (from cc in Context.CarreraComision
                                 join c in Context.Carrera on cc.IdCarrera equals c.IdCarrera
                                 join co in Context.Comision on cc.IdComision equals co.IdComision
                                 join oc in Context.OutcomeComision on co.IdComision equals oc.IdComision
                                 join o in Context.Outcome on oc.IdOutcome equals o.IdOutcome
                                 //join oepu in Context.OutcomeEncuestaPPPOutcome on o.IdOutcome equals oepu.IdOutcome
                                 //where c.IdEscuela == EscuelaId && cc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && oepu.IdOutcomeEncuestaPPPConfig == IdOutcomeEncuestaPPPConfig && oc.IdComision == IdComision
                                 //select oepu.IdOutcome).FirstOrDefault();                                 
                                 where c.IdEscuela == EscuelaId && cc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && oc.IdComision == IdComision
                                 select oc.IdOutcome).FirstOrDefault();

                model.LstComisiones[i].LstOutcomes = (from cc in Context.CarreraComision
                                                      join c in Context.Carrera on cc.IdCarrera equals c.IdCarrera
                                                      join co in Context.Comision on cc.IdComision equals co.IdComision
                                                      join oc in Context.OutcomeComision on co.IdComision equals oc.IdComision
                                                      join o in Context.Outcome on oc.IdOutcome equals o.IdOutcome
                                                      where (c.IdEscuela == EscuelaId && cc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && co.IdComision == IdComision)
                                                      orderby oc.Comision.Codigo
                                                      select new SelectListItem
                                                      {
                                                          Value = oc.IdOutcome.ToString(),
                                                          Text = oc.Comision.Codigo + " | " + oc.Outcome.Nombre,
                                                          Selected = oc.IdOutcome == idOutcome
                                                      }).Distinct().ToList();
            }
            return PartialView(model);
        }
        public JsonResult GetSedeByComision(Int32? ComisionId)
        {
            var model = new GetSedeByComisionViewModel();
            model.Filter(CargarDatosContext(), ComisionId);
            return Json(model.lstSede);
        }

        public JsonResult GetSurveyByFilter(Int32? carreraId, Int32? sedeId, Int32? cicloId, Int32? cursoId, Int32? seccionId)
        {
            var model = new GetSurveyByFilterViewModel();
            model.Filter(CargarDatosContext(), carreraId, sedeId, cicloId, cursoId, seccionId);
            return Json(model.lstTemplate);
        }
        [HttpPost]
        public JsonResult GetNivelesPPPByPeriodo(Int32? IdPeriodoAcademico)
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            {
                var data = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).Select(x => new { IdNivelAceptacionEncuesta = x.IdNivelAceptacionEncuesta, NombreEspanol = x.NombreEspanol }).ToList();
                return Json(data);
            }
            else
            {
                //var data = context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).Select(x => new { IdNivelAceptacionEncuesta = x.IdNivelAceptacionEncuesta, NombreEspanol = x.NombreIngles }).ToList();
                var data = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).Select(x => new { IdNivelAceptacionEncuesta = x.IdNivelAceptacionEncuesta, NombreEspanol = x.NombreIngles }).ToList();

                return Json(data);
            }
        }
        [HttpPost]
        public JsonResult GetNivelesGRAByPeriodo(Int32? IdPeriodoAcademico)
        {
            if (currentCulture == ConstantHelpers.CULTURE.ESPANOL)
            { 
                var data = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).Select(x => new { IdNivelAceptacionEncuesta = x.IdNivelAceptacionEncuesta, NombreEspanol = x.NombreEspanol }).ToList();
                return Json(data);
            }
            else
            {
                var data = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).Select(x => new { IdNivelAceptacionEncuesta = x.IdNivelAceptacionEncuesta, NombreEspanol = x.NombreIngles }).ToList();
                return Json(data);
            }
        }

        [HttpPost]
        public JsonResult GetOutcomeByComision2(Int32 IdComision)
        {
           
            var data = Context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdComision == IdComision).Select(x => new { IdOutcome = x.Outcome.IdOutcome, Nombre = x.Outcome.Nombre }).ToList();
            return Json(data);
        }

        [HttpPost]
        public JsonResult GetComisionByAcreditadora(Int32 IdAcreditadora, int IdSubModalidad)
        {

            //int IdPeriodoAcademicoActual = context.PeriodoAcademico.Where(y => y.Estado == "ACT").Select(u => u.IdPeriodoAcademico).FirstOrDefault();

            int IdPeriodoAcademicoActual = Context.SubModalidadPeriodoAcademico.Where(y => y.PeriodoAcademico.Estado == "ACT" && y.IdSubModalidad == IdSubModalidad).Select(u => u.IdPeriodoAcademico).FirstOrDefault();


            IQueryable<int> query1 = Context.AcreditadoraPeriodoAcademico.Where(y => y.IdAcreditadora == IdAcreditadora && y.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActual).Select(u => u.IdAcreditadoraPreiodoAcademico);

            var data = Context.Comision.Where(x => (query1).Contains(x.IdAcreditadoraPeriodoAcademico)).Select(x => new { IdComision = x.IdComision, Codigo = x.Codigo }).ToList();


            return Json(data);
        }


        [HttpPost]
        public JsonResult GetOutcomeByCurso(Int32 IdCurso)
        {
            // System.Diagnostics.Debug.WriteLine("Inicio "+ DateTime.Now);
            //int IdPeriodoAcademicoActual = context.PeriodoAcademico.Where(y => y.Estado == "ACT").Select(u => u.IdPeriodoAcademico).FirstOrDefault();

            //int IdPeriodoAcademicoActual = Context.SubModalidadPeriodoAcademico.Where(y => y.PeriodoAcademico.Estado == "ACT").Select(u => u.IdPeriodoAcademico).FirstOrDefault();
            var IdPeriodoAcademicoActual = Session.GetPeriodoAcademicoId();
            var queryMC = (from mc in Context.MallaCocos
                           join mcd in Context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                           where (mc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActual)
                           join oc in Context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                           join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                           where (cmc.IdCurso == IdCurso)
                           select oc.IdOutcomeComision);

            var data = Context.OutcomeComision.Where(x => (queryMC).Contains(x.IdOutcomeComision)).Select(x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre, NombreIngles = x.Comision.NombreIngles + " | " + x.Outcome.NombreIngles }).ToList();
            
            return Json(data);
        }


        [HttpPost]
        public JsonResult GetCursoByCarrera(Int32 IdCarrera, int IdSubModalidad)
        {

            //int IdPeriodoAcademicoActual = context.PeriodoAcademico.Where(y => y.Estado == "ACT").Select(u => u.IdPeriodoAcademico).FirstOrDefault();

            int IdPeriodoAcademicoActual = Context.SubModalidadPeriodoAcademico.Where(y => y.PeriodoAcademico.Estado == "ACT" && y.IdSubModalidad == IdSubModalidad).Select(u => u.IdPeriodoAcademico).FirstOrDefault();

            var queryCursosConOutcomes = (from mcd in Context.MallaCocosDetalle
                                          join oc in Context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                                          join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                                          join cpa in Context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                                          join ccpa in Context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                                          where (oc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActual && ccpa.IdCarrera == IdCarrera)
                                          select cmc.IdCurso);

            var data = Context.Curso.Where(x => (queryCursosConOutcomes).Contains(x.IdCurso)).Select(x => new { IdCurso = x.IdCurso, NombreEspanol = x.NombreEspanol, NombreIngles = x.NombreIngles }).ToList();
            return Json(data);
        }

        [HttpPost]
        public JsonResult NewAlumnReg(string nombres, string apellidos, string correo, string carrera, string codigo, string moduloId)
        {

            try
            {
                var findAlumnoCodigo = Context.AlumnoInvitado.Where(x => x.codigo == codigo);
                var data = new AlumnoInvitado();
                int ModalidadId = session.GetModalidadId();
                int CarreraId = Context.Carrera.Where(x => x.NombreEspanol == carrera).FirstOrDefault().IdCarrera;
                int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;


                if (findAlumnoCodigo.Count() == 0)
                {
                    var alumno = Context.Alumno.Where(x => x.Codigo == codigo);
                    data = Context.AlumnoInvitado.Add(new AlumnoInvitado
                    {
                        nombres = nombres,
                        apellido = apellidos,
                        correo = correo,
                        carrera = carrera,
                        codigo = codigo,
                        IdSubModalidadPeriodoAcademico = submodapaId,
                        Tipo = "EVD",
                        IdModulo = moduloId.ToInteger(),
                        IdCarrera = CarreraId

                    });
                    Context.SaveChanges();
                    return Json(new { result = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { result = "Warning" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "Error" }, JsonRequestBehavior.AllowGet);
            }

        }
    }
    public static class ReportBaseModelExtensions
    {
        public class OptionViewModel
        {
            public int Value { get; set; }
            public string TextContent { get; set; }
        }

        public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new SelectList(dictionary, "Key", "Value");
        }
        public static SelectList ToSelectList(this IEnumerable<ComboItem> data)
        {
            return new SelectList(data, "Key", "Value");
        }
        public static IEnumerable<OptionViewModel> ToOptions(this Dictionary<int, string> dictionary)
        {
            return dictionary.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
        public static IEnumerable<OptionViewModel> ToOptions(this IEnumerable<ComboItem> data)
        {
            return data.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
    }
}