﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using System.IO;
using Microsoft.Reporting.WebForms;
using ICSharpCode.SharpZipLib.Zip;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Areas.Survey.Models;
using UPC.CA.ABET.Presentation.Areas.Report.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.Acreditador)]

    public class ReportController : BaseController
    {

        private EVDModel model;

        public ReportController()
        {

            this.model = new EVDModel(CargarDatosContext(), reportBaseModel, this);
        }

        public ActionResult SurveyManagementEVD()
        {
            return View();
        }

        public ActionResult ReporteEncuestaVirtualEVD()
        {
            var viewModel = new ReporteEncuestaVirtualEVDViewModel();
            model.LoadDropDownListsReporteEncuestaVirtualEVD(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ReporteEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
        {
            model.ExistDataReporteEncuestaVirtualEVD(ViewModel);
            return PartialView("_ReporteEncuestaVirtualEVDReporte", ViewModel);
        }


        public ActionResult InformeEncuestaVirtualEVD()
        {
            var viewModel = new ReporteEncuestaVirtualEVDViewModel();
            model.LoadDropDownListsReporteEncuestaVirtualEVD(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult InformeEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
        {
            model.ExistDataInformeEncuestaVirtualEVD(ViewModel);
            return PartialView("_InformeEncuestaVirtualEVDReporte", ViewModel);
        }

        public void MergeFiles(string destinationFile, List<String> sourceFiles)
        {
            if (System.IO.File.Exists(destinationFile))
                System.IO.File.Delete(destinationFile);

            string[] sSrcFile;
            sSrcFile = new string[sourceFiles.Count];

            string[] arr = new string[sourceFiles.Count];
            for (int i = 0; i < sourceFiles.Count; i++)
            {
                if (sourceFiles[i] != null)
                {
                    if (sourceFiles[i].Trim() != "")
                        arr[i] = sourceFiles[i].ToString();
                }
            }

            if (arr != null)
            {
                sSrcFile = new string[sourceFiles.Count];

                for (int ic = 0; ic <= arr.Length - 1; ic++)
                {
                    sSrcFile[ic] = arr[ic].ToString();
                }
            }
            try
            {
                int f = 0;

                PdfReader reader = new PdfReader(sSrcFile[f]);
                int n = reader.NumberOfPages;
                //Response.Write("There are " + n + " pages in the original file.");
                //Document document = new Document(PageSize.A4);
                Document document = new Document();

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));

                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;

                int rotation;
                while (f < sSrcFile.Length)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;

                        //document.SetPageSize(PageSize.A4);
                        document.NewPage();
                        page = writer.GetImportedPage(reader, i);

                        rotation = reader.GetPageRotation(i);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 28, 160);
                        }
                        //Response.Write("\n Processed page " + i);
                    }

                    f++;
                    if (f < sSrcFile.Length)
                    {
                        reader = new PdfReader(sSrcFile[f]);
                        n = reader.NumberOfPages;
                        //Response.Write("There are " + n + " pages in the original file.");
                    }
                }
                //Response.Write("Success");
                document.Close();
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
            }


        }
        public FileResult DescargarTodo(String Key)
        {
            var baseOutputStream = new MemoryStream();
            ZipOutputStream zipOutput = new ZipOutputStream(baseOutputStream);
            zipOutput.IsStreamOwner = false;

            zipOutput.SetLevel(3);
            byte[] buffer = new byte[4096];

            var lstFile = Directory.GetFiles(Server.MapPath("~/Files/SurveyReport"));
            foreach (var file in lstFile)
            {
                if (Path.GetFileName(file).Contains(Key))
                {
                    ZipEntry entry = new ZipEntry(Path.GetFileName(file).Replace(Key, ""));
                    entry.DateTime = DateTime.Now;
                    zipOutput.PutNextEntry(entry);
                    using (FileStream fs = System.IO.File.OpenRead(file))
                    {
                        int sourceBytes = 0;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            zipOutput.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
            }

            zipOutput.Finish();
            zipOutput.Close();

            baseOutputStream.Position = 0;

            return new FileStreamResult(baseOutputStream, "application/x-zip-compressed")
            {
                FileDownloadName = "ReporteResultadosFDC.zip"
            };
        }
        #region FDC
        public ActionResult ReportResultadosFDCporCarrera(String Codigo, Int32 IdPeriodoAcademico)
        {
            try
            {
                var LstNivelesAceptacion = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC).ToList();

                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

                var idCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo == Codigo).IdCarrera;

                var IdMallaCurricular = Context.MallaCurricularPeriodoAcademico.Include(x => x.MallaCurricular).FirstOrDefault(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.MallaCurricular.Carrera.Codigo == Codigo).IdMallaCurricular;
                var lstCursos = Context.CursoMallaCurricular.Where(x => x.IdMallaCurricular == IdMallaCurricular).OrderBy(x => x.IdNivel).Select(x => new { CodigoCurso = x.Curso.Codigo, IdCurso = x.IdCurso }).ToList();

                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteResultadoSeccion.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var NombreCiclo = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).CicloAcademico;
                var extension = ".pdf";
                Int32 Cont = 0;
                var Lstpdf = new List<String>();

                foreach (var item in lstCursos)
                {
                    var lstSecciones = Context.GetSeccionCurso(null, item.CodigoCurso, IdPeriodoAcademico, null).ToList();

                    foreach (var seccion in lstSecciones)
                    {
                        var objSeccion = Context.Seccion.FirstOrDefault(x => x.IdSeccion == seccion.IdSeccion);

                        var CodCurso = item.CodigoCurso;
                        List<ReportParameter> parametros = new List<ReportParameter>();
                        parametros.Add(new ReportParameter("NombreCiclo", NombreCiclo));
                        fileName = "Resultados_Curso_" + CodCurso + "_Seccion_" + (objSeccion.Codigo).ToString() + Key + extension;
                        //DAR VALOR a IdSubModalidadPeriodoAvademico = 0
                        var query = Context.getFDC_Resultados_Seccion(0, ConstantHelpers.ENCUESTA.FDC, language, IdPeriodoAcademico, item.IdCurso, seccion.IdSeccion).ToList();
                        //var queryHallazgo = context.ExtraerHallazgosEncuesta(model.IdSeccion, ConstantHelpers.ENCUESTA.FDC).ToList();
                        var data = Context.ExtraerPromedioEncuestasData(ConstantHelpers.ENCUESTA.FDC, language, IdPeriodoAcademico, item.IdCurso, seccion.IdSeccion).FirstOrDefault();
                        if (query.Count > 0)
                        {
                            if (data != null)
                            {
                                parametros.Add(new ReportParameter("NombreCurso", data.Curso ?? "-"));
                                parametros.Add(new ReportParameter("CodigoCurso", data.Cod_Curso ?? "-"));

                                parametros.Add(new ReportParameter("CodigoSeccion", data.Seccion ?? "-"));
                                parametros.Add(new ReportParameter("Docente", data.Docente ?? "-"));
                                parametros.Add(new ReportParameter("TotalAlumnoSeccion", data.Total_Alumnos_Seccion.ToString() ?? "-"));
                                parametros.Add(new ReportParameter("TotalAlumnoEncuesta", data.Total_Alumnos_Encuesta.ToString() ?? "- "));
                                parametros.Add(new ReportParameter("Idioma", language));

                                parametros.Add(new ReportParameter("NM_MIN", LstNivelesAceptacion[0].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("NM_MAX", LstNivelesAceptacion[0].ValorMaximo.ToString()));
                                parametros.Add(new ReportParameter("ES_MIN", LstNivelesAceptacion[1].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("ES_MAX", LstNivelesAceptacion[1].ValorMaximo.ToString()));
                                parametros.Add(new ReportParameter("SO_MIN", LstNivelesAceptacion[2].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("SO_MAX", LstNivelesAceptacion[2].ValorMaximo.ToString()));

                                String Logro = "-";
                                var malla = Context.spGetCursosControlEncuesta(item.IdCurso, IdPeriodoAcademico, 3, data.Cod_Curso).Where(x => x.LogroFinCicloEspanol.Length > 0).FirstOrDefault();
                                if (malla != null)
                                {
                                    if (language == ConstantHelpers.CULTURE.ESPANOL)
                                        Logro = malla.LogroFinCicloEspanol;
                                    else
                                        Logro = malla.LogroFinCicloIngles;
                                }

                                parametros.Add(new ReportParameter("LogroCurso", Logro));

                                ReportLogic reporteLogic = new ReportLogic();
                                List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                                ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                                //ReportDataSource.Add(new ReportDataSource("DataSetExtraerHallazgo", queryHallazgo));

                                Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, "PDF", path, fileName, parametros).FileContents;
                                System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                                Lstpdf.Add(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName));
                                Cont++;
                            }
                            else
                            {
                                PostMessage(MessageType.Error);
                                return RedirectToAction("ReportResultadosFDC", "Report");
                            }

                        }
                        //PostMessage(MessageType.Warning, "No se generó reporte, debido a que no existe data.");
                        //return RedirectToAction("ReportResultadosFDC", "Report");
                    }
                }

                if (Cont == 0)
                {
                    PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);
                    return RedirectToAction("ReportResultadosFDC", "Report");
                }

                var rutaConsolidado = Path.Combine(Server.MapPath("~/Files/SurveyReport"), Key + "Consolidado.pdf");
                if (Cont > 0)
                    MergeFiles(rutaConsolidado, Lstpdf);


                return File(rutaConsolidado, System.Net.Mime.MediaTypeNames.Application.Octet, "Reporte_Resultados_LCFC_PorCarrea-" + Codigo + ".pdf");
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("ReportResultadosFDC", "Report");
            }
        }
        public ActionResult ReportResultadosFDC(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            var model = new ReportResultadosFDCViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess);
            model.fileName = fileName ?? "";
            return View(model);
        }
        [HttpPost]
        public ActionResult ReportResultadosFDC(ReportResultadosFDCViewModel model, String btnReport)
        {
            List<String> Lstpdf = new List<String>();
            Console.Write(model.IdCiclo);
            Console.Write(model.IdCurso);
            Console.Write(model.IdSeccion);


            try
            {
                //clearFolder(Server.MapPath("~/Files/SurveyReport"));   
                var LstNivelesAceptacion = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == model.IdCiclo && x.IdTipoEncuesta == model.IdTipoEncuesta).ToList();
                var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdCiclo).IdSubModalidadPeriodoAcademico;
                Int32? cantSuccess = 0;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteResultadoSeccion.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }
                if (System.IO.File.Exists(path))
                {
                    var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    var NombreCiclo = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdCiclo).CicloAcademico;
                    var NombreCarrera = ".";
                    var NombreSede = ".";
                    
                        if(language == ConstantHelpers.CULTURE.ESPANOL)
                            NombreCarrera = Context.Carrera.FirstOrDefault(yy => yy.IdCarrera == model.IdCarrera).NombreEspanol;
                        else
                            NombreCarrera = Context.Carrera.FirstOrDefault(yy => yy.IdCarrera == model.IdCarrera).NombreIngles;
                         var n2 = "";
                         if (model.IdSede == 0 && language == ConstantHelpers.CULTURE.ESPANOL)
                            n2 = "TODOS";
                         else if (model.IdSede == 0 && language == ConstantHelpers.CULTURE.INGLES)
                         {
                            n2 = "ALL";
                         }
                        else
                         {
                            n2 = Context.Sede.FirstOrDefault(yy => yy.IdSede == model.IdSede).Nombre;
                        }
                           
                        //var CodCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == model.IdCurso).Codigo;
                        List<ReportParameter> parametros = new List<ReportParameter>();
                        parametros.Add(new ReportParameter("NombreCiclo", NombreCiclo));
                        fileName = "Resultados_Consolidado_1" + Key + extension;

                        //0, corresponde a la submodalidadperiodoacademico=0model.IdCiclo
                        var SubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdCiclo).IdSubModalidadPeriodoAcademico;

                        //var query = Context.getFDC_Resultados_Seccion(SubModalidadPeriodoAcademico, ConstantHelpers.ENCUESTA.FDC, language, model.IdCiclo, model.IdCurso, model.IdSeccion).ToList();
                        var query = Context.ReporteLCFCConsolidadoCursoPivot(language, Convert.ToInt32(SubModalidadPeriodoAcademico), 0, 0, 0, model.IdCarrera, model.IdSede, 0, 0, 0, 0, 0).ToList();
                        
                        //var queryHallazgo = context.ExtraerHallazgosEncuesta(model.IdSeccion, ConstantHelpers.ENCUESTA.FDC).ToList();
                        //0, corresponde a la submodalidadperiodoacademico=0

                        //0, corresponde a la submodalidadperiodoacademico=0model.IdCiclo
                        //model.IdCurso = 4455;
                        //model.IdSeccion = 23303;
                        var data = Context.ExtraerPromedioEncuestasData(ConstantHelpers.ENCUESTA.FDC, language, SubModalidadPeriodoAcademico,
                            model.IdCurso, model.IdSeccion).FirstOrDefault();
                        if (query.Count > 0)
                        {
                            if (data != null)
                            {
                                parametros.Add(new ReportParameter("NombreCurso", data.Curso ?? "-"));
                                parametros.Add(new ReportParameter("CodigoCurso", data.Cod_Curso ?? "-"));

                                parametros.Add(new ReportParameter("CodigoSeccion", data.Seccion ?? "-"));
                                parametros.Add(new ReportParameter("Docente", data.Docente ?? "-"));
                                parametros.Add(new ReportParameter("TotalAlumnoSeccion", data.Total_Alumnos_Seccion.ToString() ?? "-"));
                                parametros.Add(new ReportParameter("TotalAlumnoEncuesta", data.Total_Alumnos_Encuesta.ToString() ?? "- "));
                                parametros.Add(new ReportParameter("Idioma", language));

                                parametros.Add(new ReportParameter("NM_MIN", LstNivelesAceptacion[0].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("NM_MAX", LstNivelesAceptacion[0].ValorMaximo.ToString()));
                                parametros.Add(new ReportParameter("ES_MIN", LstNivelesAceptacion[1].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("ES_MAX", LstNivelesAceptacion[1].ValorMaximo.ToString()));
                                parametros.Add(new ReportParameter("SO_MIN", LstNivelesAceptacion[2].ValorMinimo.ToString()));
                                parametros.Add(new ReportParameter("SO_MAX", LstNivelesAceptacion[2].ValorMaximo.ToString()));

                                parametros.Add(new ReportParameter("NMNombre", language == "es-PE" ? LstNivelesAceptacion[0].NombreEspanol : LstNivelesAceptacion[0].NombreIngles));
                                parametros.Add(new ReportParameter("ESNombre", language == "es-PE" ? LstNivelesAceptacion[1].NombreEspanol : LstNivelesAceptacion[1].NombreIngles));
                                parametros.Add(new ReportParameter("SONombre", language == "es-PE" ? LstNivelesAceptacion[2].NombreEspanol : LstNivelesAceptacion[2].NombreIngles));

                                parametros.Add(new ReportParameter("NombreCarrera", NombreCarrera));
                                parametros.Add(new ReportParameter("NombreSede", n2));

                            String Logro = "-";
                                var malla = Context.spGetCursosControlEncuesta(model.IdCurso, SubModalidadPeriodoAcademico, 3, data.Cod_Curso).Where(x => x.LogroFinCicloEspanol.Length > 0).FirstOrDefault();
                                if (malla != null)
                                {
                                    if (language == ConstantHelpers.CULTURE.ESPANOL)
                                        Logro = malla.LogroFinCicloEspanol;
                                    else
                                        Logro = malla.LogroFinCicloIngles;
                                }

                                parametros.Add(new ReportParameter("LogroCurso", Logro));

                                ReportLogic reporteLogic = new ReportLogic();
                                List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                                ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                                //ReportDataSource.Add(new ReportDataSource("DataSetExtraerHallazgo", queryHallazgo));

                                Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                                System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);

                                PostMessage(MessageType.Success, "Los reportes se generaron con éxito.");
                                cantSuccess++;
                                return RedirectToAction("ReportResultadosFDC", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });
                            }
                            else
                            {
                                PostMessage(MessageType.Error);
                                model.CargarDatos(CargarDatosContext(), model.IdCiclo, model.IdCurso, model.IdSeccion);
                                TryValidateModel(model);
                                return View(model);
                            }

                        }
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);
                        model.CargarDatos(CargarDatosContext(), model.IdCiclo, model.IdCurso, model.IdSeccion);
                        TryUpdateModel(model);
                        return View(model);

                    
                }
                else
                {
                    PostMessage(MessageType.Error);
                    model.CargarDatos(CargarDatosContext(), model.IdCiclo, model.IdCurso, model.IdSeccion);
                    TryUpdateModel(model);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message + "-" + (ex.InnerException == null ? String.Empty : ex.InnerException.Message));
                model.CargarDatos(CargarDatosContext(), model.IdCiclo, model.IdCurso, model.IdSeccion);
                TryUpdateModel(model);
                return View(model);
            }
        }
        public ActionResult ReportHistoricoResultadosFDC(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            var model = new ReportHistoricoResultadosFDCViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess);
            return View(model);
        }
        [HttpPost]
        public ActionResult ReportHistoricoResultadosFDC(ReportHistoricoResultadosFDCViewModel model, String btnReport)
        {
            try
            {
                //clearFolder(Server.MapPath("~/Files/SurveyReport"));
                Int32? cantSuccess = 0;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteHistorico.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }
                if (System.IO.File.Exists(path))
                {
                    var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    var objCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == model.IdCurso);

                    if (objCurso != null)
                    {
                        List<ReportParameter> parametros = new List<ReportParameter>();
                        parametros.Add(new ReportParameter("Idioma", language));

                        var queryHallazgo = Context.ExtraerHallazgosEncuesta(model.IdCurso, ConstantHelpers.ENCUESTA.FDC).ToList();

                        String malla = "-";
                        if (language == ConstantHelpers.CULTURE.ESPANOL)
                            malla = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == model.IdCurso && x.LogroFinCicloEspanol.Length > 0) != null ? Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == model.IdCurso && x.LogroFinCicloEspanol.Length > 0).LogroFinCicloEspanol : "-";
                        else
                            malla = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == model.IdCurso && x.LogroFinCicloIngles.Length > 0) != null ? Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == model.IdCurso && x.LogroFinCicloIngles.Length > 0).LogroFinCicloIngles : "-";


                        parametros.Add(new ReportParameter("LogroCurso", malla));
                        //VALIDAD 0 = IdSubModalidadPeriodoAcademico
                        var query = Context.getFDC_Resultados_HistoricosF(1, ConstantHelpers.ENCUESTA.FDC, language, model.IdCurso).ToList();
                        if (query.Count() > 0)
                        {
                            if (model.IdDesde.HasValue)
                            {
                                var inicio = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdDesde).CicloAcademico.ToInteger();
                                query = query.Where(x => x.CicloAcademico.ToInteger() >= inicio).ToList();
                            }
                            if (model.IdHasta.HasValue)
                            {
                                var hasta = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdHasta).CicloAcademico.ToInteger();
                                query = query.Where(x => x.CicloAcademico.ToInteger() <= hasta).ToList();
                            }
                            ReportLogic reporteLogic = new ReportLogic();

                            List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                            ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                            ReportDataSource.Add(new ReportDataSource("DataSetHallazgo", queryHallazgo));
                            parametros.Add(new ReportParameter("Curso", objCurso.Codigo ?? "-"));
                            parametros.Add(new ReportParameter("NombreCurso", language == ConstantHelpers.CULTURE.ESPANOL ? objCurso.NombreEspanol ?? "-" : objCurso.NombreIngles ?? "-"));

                            fileName = "Reporte_Histórico_Curso_" + (objCurso.Codigo).ToString() + Key + extension;
                            Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                            System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                            PostMessage(MessageType.Success, "Los reportes se generaron con éxito.");
                            ++cantSuccess;
                        }
                        else
                        {
                            PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);
                        }
                        return RedirectToAction("ReportHistoricoResultadosFDC", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });
                    }
                    else
                    {
                        //var SeccionCurso = context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", SeccionC, CursoC, model.CicloId, null).FirstOrDefault();

                        //var lstCursos = context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", null, null, null, null).ToList();
                        var lstCursos = Context.spGetCodigoCursosControlEncuesta(null, null, model.IdDesde, null).ToList();
                        //var lstCursos = context.GetSeccionCurso(null, null, null, null);
                        foreach (var item in lstCursos)
                        {   //SUBMODALIDAD=1
                            var query = Context.getFDC_Resultados_Historico(model.IdDesde, ConstantHelpers.ENCUESTA.FDC, language, null, item.IdCurso, null).ToList();
                            var queryHallazgo = Context.ExtraerHallazgosEncuesta(item.IdCurso, ConstantHelpers.ENCUESTA.FDC).ToList();
                            if (query.Count() > 0)
                            {
                                if (model.IdDesde.HasValue)
                                {
                                    var inicio = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdDesde).CicloAcademico.ToInteger();
                                    query = query.Where(x => x.CicloAcademico.ToInteger() >= inicio).ToList();
                                }
                                if (model.IdHasta.HasValue)
                                {
                                    var hasta = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdHasta).CicloAcademico.ToInteger();
                                    query = query.Where(x => x.CicloAcademico.ToInteger() >= hasta).ToList();
                                }

                                List<ReportParameter> parametros = new List<ReportParameter>();
                                parametros.Add(new ReportParameter("Idioma", language));
                                var objCursoTemp = Context.Curso.FirstOrDefault(x => x.IdCurso == item.IdCurso);
                                ReportLogic reporteLogic = new ReportLogic();
                                List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                                ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                                ReportDataSource.Add(new ReportDataSource("DataSetHallazgo", queryHallazgo));
                                parametros.Add(new ReportParameter("Curso", item.Codigo));

                                parametros.Add(new ReportParameter("NombreCurso", language == ConstantHelpers.CULTURE.ESPANOL ? objCursoTemp.NombreEspanol ?? "-" : objCursoTemp.NombreIngles ?? "-"));

                                String malla = "-";
                                if (language == ConstantHelpers.CULTURE.ESPANOL)
                                    malla = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == item.IdCurso && x.LogroFinCicloEspanol.Length > 0) != null ? Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == item.IdCurso && x.LogroFinCicloEspanol.Length > 0).LogroFinCicloEspanol : "-";
                                else
                                    malla = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == item.IdCurso && x.LogroFinCicloIngles.Length > 0) != null ? Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == item.IdCurso && x.LogroFinCicloIngles.Length > 0).LogroFinCicloIngles : "-";

                                parametros.Add(new ReportParameter("LogroCurso", malla));

                                fileName = Key + "Reporte_Histórico_Curso_" + item.Codigo + Key + extension;
                                Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                                System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                                ++cantSuccess;
                            }
                            else
                                continue;
                        }

                        if (cantSuccess == 0)
                            PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);
                        else
                            PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);

                        return RedirectToAction("ReportHistoricoResultadosFDC", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });
                    }
                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("ReportHistoricoResultadosFDC", "Report");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message + "-" + (ex.InnerException == null ? String.Empty : ex.InnerException.Message));
                return RedirectToAction("ReportHistoricoResultadosFDC", "Report");
            }
        }
        #endregion
        #region PPP
        public ActionResult ReportePercepcionPromedioPPP(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new ReportePercepcionPromedioPPPViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
            model.fileName = fileName ?? "";
            return View(model);
        }

        [HttpPost]
        public ActionResult ReportePercepcionPromedioPPP(ReportePercepcionPromedioPPPViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                Int32 SubModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var LstNivelesAceptacion = Context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.IdTipoEncuesta == model.IdTipoEncuesta).ToList();
                //String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReportePromedioOutcomePPP.rdlc");

                var language2 = Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL ? "es-PE" : "en-US";

                String path = "";
                if (language2.Equals(ConstantHelpers.CULTURE.ESPANOL))
                {
                    path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteFrecuenciaOutcomePPP.rdlc");
                }
                else
                {
                    path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteFrecuenciaOutcomePPPEnglish.rdlc");
                }


                //var Key = "_"+ DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':','_').Replace('/','-');
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }


                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";

                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

                    var NombreCarrera = "";
                    var NombreComision = "";
                    var NombreSede = "";
                    var NumeroPractica = "";
                    if (language2.Equals(ConstantHelpers.CULTURE.ESPANOL))
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreEspanol;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol;
                        NumeroPractica = Context.NumeroPractica.FirstOrDefault(x => x.IdNumeroPractica == model.IdNumeroPractica).NombreEspanol;
                        NombreSede = "Todas las sedes";
                    }
                    else
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreIngles;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreIngles;
                        NumeroPractica = Context.NumeroPractica.FirstOrDefault(x => x.IdNumeroPractica == model.IdNumeroPractica).NombreIngles;
                        NombreSede = "All Sites";
                    }

                    var CicloAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;



                    if (model.IdSede != 0)
                    {
                        NombreSede = Context.Sede.FirstOrDefault(x => x.IdSede == model.IdSede).Nombre;
                        if (language2.Equals(ConstantHelpers.CULTURE.ESPANOL))
                        {
                            NombreSede = "Sede: " + NombreSede;
                        }
                        else
                        {
                            NombreSede = "Site: " + NombreSede;
                        }

                    }




                    List<ReportParameter> parametros = new List<ReportParameter>();
                    parametros.Add(new ReportParameter("Idioma", language));

                    if (Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol != ConstantHelpers.ENCUESTA.COMISION_WASC)
                        parametros.Add(new ReportParameter("TipoOutcome", "STUDENT OUTCOME"));
                    else
                        parametros.Add(new ReportParameter("TipoOutcome", "COMPETENCIAS GENERALES"));

                    //parametros.Add(new ReportParameter("NM_MIN", LstNivelesAceptacion[0].ValorMinimo.ToString()));
                    //parametros.Add(new ReportParameter("NM_MAX", LstNivelesAceptacion[0].ValorMaximo.ToString()));
                    //parametros.Add(new ReportParameter("ES_MIN", LstNivelesAceptacion[1].ValorMinimo.ToString()));
                    //parametros.Add(new ReportParameter("ES_MAX", LstNivelesAceptacion[1].ValorMaximo.ToString()));
                    //parametros.Add(new ReportParameter("SO_MIN", LstNivelesAceptacion[2].ValorMinimo.ToString()));
                    //parametros.Add(new ReportParameter("SO_MAX", LstNivelesAceptacion[2].ValorMaximo.ToString()));
                    parametros.Add(new ReportParameter("Carrera", NombreCarrera + " - " + CicloAcademico));
                    parametros.Add(new ReportParameter("Periodo", CicloAcademico));
                    parametros.Add(new ReportParameter("Comision", NombreComision));
                    parametros.Add(new ReportParameter("Sede", NombreSede));
                    parametros.Add(new ReportParameter("NumPractica", NumeroPractica));

                    //var query = context.ReportePromedioPorOutcomePPP(model.IdPeriodoAcademico, model.IdCarrera, model.IdComision, model.IdSede, language, model.IdNumeroPractica).ToList();
                    var query = Context.ReporteFrecuenciaPorOutcomePPP(SubModalidadPeriodoAcademicoId, model.IdCarrera, model.IdComision, model.IdSede, language, model.IdNumeroPractica).ToList();
                    if (query.Count() > 0)
                    {
                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        fileName = "Reporte_Promedio_EncuestasPPP" + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);

                    }
                    return RedirectToAction("ReportePercepcionPromedioPPP", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });

                }
                else
                {
                    PostMessage(MessageType.Error, "1");
                    return RedirectToAction("ReportePercepcionPromedioPPP", "Report");
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                PostMessage(MessageType.Error, error);
                Console.Write(error);

                return RedirectToAction("ReportePercepcionPromedioPPP", "Report");
            }
        }

        public ActionResult ReporteHistoricoResultadosPPP(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new ReporteHistoricoResultadosPPPViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
            model.fileName = fileName ?? "";
            return View(model);
        }
        [HttpPost]
        public ActionResult ReporteHistoricoResultadosPPP(ReporteHistoricoResultadosPPPViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteHistoricoPPP.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }

                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";
                    var NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol;
                    var CodComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).Codigo;


                    List<ReportParameter> parametros = new List<ReportParameter>();
                    parametros.Add(new ReportParameter("Idioma", language));
                    if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
                    {
                        parametros.Add(new ReportParameter("Carrera", Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreEspanol));
                        parametros.Add(new ReportParameter("Practica", Context.NumeroPractica.FirstOrDefault(x => x.IdNumeroPractica == model.IdNumeroPractica).NombreEspanol));
                    }
                    else
                    {
                        parametros.Add(new ReportParameter("Carrera", Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreIngles));
                        parametros.Add(new ReportParameter("Practica", Context.NumeroPractica.FirstOrDefault(x => x.IdNumeroPractica == model.IdNumeroPractica).NombreIngles));
                    }

                    parametros.Add(new ReportParameter("Sede", Context.Sede.FirstOrDefault(x => x.IdSede == model.IdSede).Nombre));
                    parametros.Add(new ReportParameter("Comision", CodComision));


                    if (Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol != ConstantHelpers.ENCUESTA.COMISION_WASC)
                        parametros.Add(new ReportParameter("TipoOutcome", "STUDENT OUTCOME"));
                    else
                        parametros.Add(new ReportParameter("TipoOutcome", "COMPETENCIAS GENERALES"));
                    var query = Context.ReporteHistoricoPPPF(model.IdCarrera, model.IdSede, language, model.IdNumeroPractica, model.NombreOutcome, NombreComision).ToList();
                    if (query.Count() > 0)
                    {

                        if (model.IdDesde.HasValue)
                        {
                            var inicio = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdDesde).CicloAcademico.ToInteger();
                            query = query.Where(x => x.CicloAcademico.ToInteger() >= inicio).ToList();
                        }
                        if (model.IdHasta.HasValue)
                        {
                            var hasta = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdHasta).CicloAcademico.ToInteger();
                            query = query.Where(x => x.CicloAcademico.ToInteger() <= hasta).ToList();
                        }

                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        fileName = "Reporte_HistoricoPPP_Outcome" + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);

                    }
                    return RedirectToAction("ReporteHistoricoResultadosPPP", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });

                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("ReporteHistoricoResultadosPPP", "Report");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("ReporteHistoricoResultadosPPP", "Report");
            }
        }
        #endregion
        #region GRA
        public ActionResult PercepcionPromedioPorOutcomeGraduando(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();
            var model = new PercepcionPromedioPorOutcomeGraduandoViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
            model.fileName = fileName ?? "";
            return View(model);
        }
        [HttpPost]
        public ActionResult PercepcionPromedioPorOutcomeGraduando(PercepcionPromedioPorOutcomeGraduandoViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                var LstNivelesAceptacion = Context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdTipoEncuesta == model.IdTipoEncuesta).ToList();
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReportePromedioOutcomeGRA.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }
                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";


                    var NombreCarrera = "";
                    var NombreComision = "";
                    if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreEspanol;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol;
                    }
                    else
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreIngles;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreIngles;
                    }


                    var CicloAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;


                    List<ReportParameter> parametros = new List<ReportParameter>();
                    parametros.Add(new ReportParameter("Idioma", language));

                    if (Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol != ConstantHelpers.ENCUESTA.COMISION_WASC)
                        parametros.Add(new ReportParameter("TipoOutcome", "STUDENT OUTCOME"));
                    else
                        parametros.Add(new ReportParameter("TipoOutcome", "COMPETENCIAS GENERALES"));

                    parametros.Add(new ReportParameter("NM_MIN", LstNivelesAceptacion[0].ValorMinimo.ToString()));
                    parametros.Add(new ReportParameter("NM_MAX", LstNivelesAceptacion[0].ValorMaximo.ToString()));
                    parametros.Add(new ReportParameter("ES_MIN", LstNivelesAceptacion[1].ValorMinimo.ToString()));
                    parametros.Add(new ReportParameter("ES_MAX", LstNivelesAceptacion[1].ValorMaximo.ToString()));
                    parametros.Add(new ReportParameter("SO_MIN", LstNivelesAceptacion[2].ValorMinimo.ToString()));
                    parametros.Add(new ReportParameter("SO_MAX", LstNivelesAceptacion[2].ValorMaximo.ToString()));
                    parametros.Add(new ReportParameter("Carrera", NombreCarrera));
                    parametros.Add(new ReportParameter("Periodo", CicloAcademico));
                    parametros.Add(new ReportParameter("Comision", NombreComision));

                    var query = Context.ReportePercepcionPromPorOutcomeGraduando(model.IdComision, IdSubModalidadPeriodoAcademico, model.IdCarrera, language).ToList();
                    if (query.Count() > 0)
                    {
                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        fileName = "Reporte_Promedio_Outcome" + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);

                    }
                    return RedirectToAction("PercepcionPromedioPorOutcomeGraduando", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });
                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("PercepcionPromedioPorOutcomeGraduando", "Report");
                }
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("PercepcionPromedioPorOutcomeGraduando", "Report");
            }
        }
        public ActionResult PercepcionOutcomeGRA(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();
            var model = new PercepcionOutcomeGRAViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
            model.fileName = fileName ?? "";
            return View(model);
        }
        [HttpPost]
        public ActionResult PercepcionOutcomeGRA(PercepcionOutcomeGRAViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                /*Ticket 151 - 9/11/2019 */
                int escuelaId = Session.GetEscuelaId();
                Escuela escuela = Context.Escuela.Where(x => x.IdEscuela == escuelaId).FirstOrDefault();
                string escuelaCodigo = escuela.Codigo;
                var comisionCodigo = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).Codigo;
                var carreraCodigo = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).Codigo;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReportePercepcionOutcomeGRA.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }
                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";

                    var NombreCarrera = "";
                    var NombreComision = "";
                    if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreEspanol;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol;
                    }
                    else
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreIngles;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreIngles;

                    }


                    var CicloAcademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;


                    List<ReportParameter> parametros = new List<ReportParameter>();
                    parametros.Add(new ReportParameter("Idioma", language));

                    if (Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol != ConstantHelpers.ENCUESTA.COMISION_WASC)
                        parametros.Add(new ReportParameter("TipoOutcome", "STUDENT OUTCOME"));
                    else
                        parametros.Add(new ReportParameter("TipoOutcome", "COMPETENCIAS GENERALES"));

                    parametros.Add(new ReportParameter("Carrera", NombreCarrera));
                    parametros.Add(new ReportParameter("Periodo", CicloAcademico));
                    parametros.Add(new ReportParameter("Comision", NombreComision));

                    var EscuelaID = Session.GetEscuelaId();
                    var Escuela = Context.Escuela.FirstOrDefault(X => X.IdEscuela == EscuelaID).Codigo;

                    var query = Context.ReportePercecionPorOutcomeGraduando(model.IdPeriodoAcademico, model.IdCarrera, language, model.IdComision).ToList();
                    if (query.Count() > 0)
                    {
                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        fileName = "Reporte_Percepción_Outcome_" + escuelaCodigo + "" + CicloAcademico + "_ABET_" + comisionCodigo + "_" + carreraCodigo + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);
                    }
                    return RedirectToAction("PercepcionOutcomeGRA", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });

                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("PercepcionOutcomeGRA", "Report");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("PercepcionOutcomeGRA", "Report");
            }
        }
        public ActionResult _VerArchivo(String Archivo, String Nombre)
        {
            var model = new _VerArchivoViewModel();
            model.Fill(Archivo, Nombre);
            return View(model);
        }
        public ActionResult ReporteHistoricoOutcomesGRA(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();
            var model = new ReporteHistoricoOutcomesGRAViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
            model.fileName = fileName ?? "";
            return View(model);
        }
        [HttpPost]
        public ActionResult ReporteHistoricoOutcomesGRA(ReporteHistoricoOutcomesGRAViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteHistoricoGRA.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }

                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";

                    var NombreCarrera = "";
                    var NombreComision = "";
                    if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreEspanol;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreEspanol;
                    }
                    else
                    {
                        NombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.IdCarrera).NombreIngles;
                        NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).NombreIngles;
                    }


                    var Codigo = Context.Comision.FirstOrDefault(x => x.IdComision == model.IdComision).Codigo;
                    //var NombreOutcome = context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdPeriodoAcademico).NombreEspanol;

                    List<ReportParameter> parametros = new List<ReportParameter>
                    {
                        new ReportParameter("Idioma", language),
                        new ReportParameter("Carrera", NombreCarrera),
                        new ReportParameter("Outcome", model.NombreOutcome),
                        new ReportParameter("Comision", NombreComision)
                    };

                    var query = Context.ReporteHistoricoGraduandoF(model.IdCarrera, language, model.NombreOutcome, Codigo).ToList();
                    //var query = Context.ReporteHistoricoGraduandoF(model.IdCarrera, language, model.NombreOutcome).ToList();

                    if (query.Count() > 0)
                    {

                        if (model.IdDesde.HasValue)
                        {
                            var inicio = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico >= model.IdDesde).CicloAcademico.ToInteger();
                            query = query.Where(x => x.CicloAcademico.ToInteger() >= inicio).ToList();
                        }
                        if (model.IdHasta.HasValue)
                        {
                            var hasta = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico <= model.IdHasta).CicloAcademico.ToInteger();
                            query = query.Where(x => x.CicloAcademico.ToInteger() >= hasta).ToList();
                        }

                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        fileName = "Reporte_HistoricoGRA_Outcome" + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);

                    }
                    return RedirectToAction("ReporteHistoricoOutcomesGRA", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });

                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("ReporteHistoricoOutcomesGRA", "Report");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("ReporteHistoricoOutcomesGRA", "Report");
            }
        }
        #endregion

        public ActionResult ManagementDeEncuesta()
        {
            return View();
        }

        public ActionResult ReporteEncuestaVirtualGRA(String Key, String Extension, Int32? cantSuccess, string fileName)
        {
            int parModalidadId = Session.GetModalidadId();
            int parEscuelaId = Session.GetEscuelaId();
            var model = new ReporteEncuestaVirtualGRAViewModel();
            model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId, parEscuelaId);
            model.fileName = fileName ?? "";
            return View(model);
        }

        //public ActionResult ReporteEncuestaVirtualEVD(String Key, String Extension, Int32? cantSuccess , string fileName)
        //{
        //    int parModalidadId = Session.GetModalidadId();
        //    var model = new ReporteEncuestaVirtualEVDViewModel();
        //    model.Fill(CargarDatosContext(), Key, Extension, cantSuccess, parModalidadId);
        //    return View(model);
        //}




        [HttpPost]
        public ActionResult ReporteEncuestaVirtualGRA(ReporteEncuestaVirtualGRAViewModel model, String btnReport)
        {
            try
            {
                Int32? cantSuccess = 0;
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteEncuestaVirtualGRA.rdlc");
                var Key = "_" + DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':', '_').Replace('/', '-');
                string fileName = "";
                var extension = "";
                switch (btnReport)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }

                if (System.IO.File.Exists(path))
                {
                    //var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                    //var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
                    string language;
                    if (Convert.ToInt32(model.idIdioma) == ConstantHelpers.CULTURE.TYPE_ESPANOL)
                        language = "es-PE";
                    else
                        language = "en-US";
                    var Ciclo = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;

                    List<ReportParameter> parametros = new List<ReportParameter>();
                    parametros.Add(new ReportParameter("Idioma", language));
                    parametros.Add(new ReportParameter("Ciclo", Ciclo));
                    var CampusId = Session.GetEscuelaId();
                    var IdEscuela = Session.GetEscuelaId();
                    var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                    var query = Context.ReporteEncuestaVirtualGRA(CampusId, model.IdCarrera, IdSubModalidadPeriodoAcademico, language).ToList();

                    if (query.Count() > 0)
                    {
                        ReportLogic reporteLogic = new ReportLogic();

                        List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                        ReportDataSource.Add(new ReportDataSource("DataSetEncuestaVirtualGRA", query));
                        fileName = "Reporte_Encuesta_Virtual_GRA" + Key + extension;
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
                        System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
                        PostMessage(MessageType.Success, MessageResource.LosReportesSeGeneraronConExito);
                        ++cantSuccess;
                    }
                    else
                    {
                        PostMessage(MessageType.Warning, MessageResource.NoSeGeneroReporteDebioAqueNoExisteData);

                    }
                    return RedirectToAction("ReporteEncuestaVirtualGRA", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess, fileName = fileName });

                }
                else
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("ReporteEncuestaVirtualGRA", "Report");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("ReporteEncuestaVirtualGRA", "Report");
            }
        }


        //[HttpPost]
        //public ActionResult ReporteEncuestaVirtualEVD(ReporteEncuestaVirtualGRAViewModel model, String btnReport)
        //{
        //    try
        //    {
        //        Int32? cantSuccess = 0;
        //        String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "ReporteEncuestaVirtualEVD.rdlc");
        //        var Key = "_"+ DateTime.UtcNow.AddHours(-5).ToString().Replace(' ', '_').Replace(':','_').Replace('/','-');
        //        var extension = "";
        //        switch (btnReport)
        //        {
        //            case "PDF":
        //                extension = ".pdf";
        //                break;
        //            case "EXCEL":
        //                extension = ".xls";
        //                break;
        //            case "WORD":
        //                extension = ".doc";
        //                break;
        //        }

        //        if (System.IO.File.Exists(path))
        //        {
        //            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
        //            String language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
        //            var Ciclo = context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).CicloAcademico;
        //            int EncuestaVirtualDelegadoid = 0;
        //            int cursoid = 0;

        //            List<ReportParameter> parametros = new List<ReportParameter>();
        //            parametros.Add(new ReportParameter("Idioma", language));
        //            parametros.Add(new ReportParameter("EncuestaVirtualDelegadoId", EncuestaVirtualDelegadoid.ToString()));
        //            parametros.Add(new ReportParameter("CursoId", cursoid.ToString()));
        //            // var CampusId = Session.GetEscuelaId();
        //            //var IdEscuela = Session.GetEscuelaId();
        //            //var IdSubModalidadPeriodoAcademico = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
        //            var query = context.ReporteEVDComentariosPorCurso(language, EncuestaVirtualDelegadoid).ToList();

        //            if (query.Count() > 0)
        //            {
        //                ReportLogic reporteLogic = new ReportLogic();

        //                List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
        //                ReportDataSource.Add(new ReportDataSource("ReporteEVDComentariosPorCursoDataSet", query));
        //                var fileName = Key + "Reporte_Encuesta_Virtual_EVD" + extension;
        //                Byte[] archivoBytes = reporteLogic.GenerarReporte(ReportDataSource, btnReport, path, fileName, parametros).FileContents;
        //                System.IO.File.WriteAllBytes(Path.Combine(Server.MapPath("~/Files/SurveyReport"), fileName), archivoBytes);
        //                PostMessage(MessageType.Success, "Los reportes se generaron con éxito.");
        //                ++cantSuccess;
        //            }
        //            else
        //            {
        //                PostMessage(MessageType.Warning, "No se generó reporte, debido a que no existe data.");

        //            }
        //            return RedirectToAction("ReporteEncuestaVirtualEVD", "Report", new { Key = Key, Extension = extension, cantSuccess = cantSuccess , fileName = fileName });

        //        }
        //        else
        //        {
        //            PostMessage(MessageType.Error);
        //            return RedirectToAction("ReporteEncuestaVirtualEVD", "Report");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        PostMessage(MessageType.Error);
        //        return RedirectToAction("ReporteEncuestaVirtualEVD", "Report");
        //    }
        //}


    }
}