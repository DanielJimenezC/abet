﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using System.IO;
using System.Text;
using System.Web.Routing;
using UPC.CA.ABET.Presentation.Filters;
using System.Diagnostics;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Models;
using YandexTranslateCSharpSdk;
using UPC.CA.ABET.Presentation.Helper;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class MaintenanceController : BaseController
    {
        // GET: Survey/Maintenance
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MantenimientoEncuestaPPP(Int32? numeroPagina, Int32? IdCiclo, Int32? IdAcreditadora, Int32? IdCarrera, Int32? IdComision, Int32? IdSede, Int32? NroPractica)
        {
            var viewmodel = new MantenimientoEncuestaPPPViewModel();
            viewmodel.Fill(CargarDatosContext(), numeroPagina, IdCiclo, IdAcreditadora, IdCarrera, IdComision, IdSede, NroPractica, EscuelaId);
            return View(viewmodel);
        }

        public ActionResult MantenimientoEncuestaFDC(Int32? numeroPagina, Int32? IdCiclo, Int32? IdAcreditadora, Int32? IdCarrera, Int32? IdComision, Int32? IdSede, Int32? IdSeccion, Int32? IdEstado, Int32? IdCurso)
        {
            var viewmodel = new MantenimientoEncuestaFDCViewModel();
            viewmodel.Fill(CargarDatosContext(), numeroPagina, IdCiclo, IdAcreditadora, IdCarrera, IdComision, IdSede, IdSeccion, IdEstado, IdCurso);
            return View(viewmodel);
        }

        public ActionResult MantenimientoEncuestaGRA(Int32? numeroPagina, Int32? IdCiclo, Int32? IdCarrera, Int32? IdComision, Int32? IdSede)
        {
            int parModalidadId = Session.GetModalidadId();

            var viewmodel = new MantenimientoEncuestaGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), numeroPagina, IdCiclo, IdCarrera, IdComision, IdSede, parModalidadId);
            return View(viewmodel);

        }
        public ActionResult MantenimientoEncuestaVirtualGRA(Int32? numeroPagina, Int32? IdCiclo, Int32? IdCarrera)
        {
            var viewmodel = new MantenimientoEncuestaVirtualGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), numeroPagina, IdCiclo, IdCarrera);
            return View(viewmodel);
        }

        public ActionResult DetalleEncuestaGRA(Int32? Carrera, Int32? Ciclo)
        {
            var viewmodel = new DetalleEncuestaGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), Carrera, Ciclo);
            return View(viewmodel);
        }
        public ActionResult DetalleEncuestaVirtualGRA(Int32 Carrera, Int32 Ciclo)
        {
            var viewmodel = new DetalleEncuestaVirtualGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), Carrera, Ciclo);
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult MantenimientoEncuestaGRA(MantenimientoEncuestaGRAViewModel model, Int32? numeroPagina)
        {
            model.Filter(CargarDatosContext(), numeroPagina);
            return View(model);
        }






        public ActionResult _ConfirmacionDeleteHallazgoIRD(Int32 HallazgoID)
        {
            var model = new _ConfirmacionDeleteHallazgoViewModel();
            model.Fill_IRD(CargarDatosContext(), HallazgoID);
            return View(model);
        }

        [HttpPost]
        public ActionResult _ConfirmacionDeleteHallazgoIRD(_ConfirmacionDeleteHallazgoViewModel model)
        {
            //var tipo = context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta).TipoEncuesta.Acronimo;
            try
            {
                var hallazgo = Context.Hallazgo.Where(x => x.IdHallazgo == model.IdHallazgo).ToList();
                foreach (var item in hallazgo)
                {
                    item.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                }

                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("LstHallazgosIRD", "Register", new { Area = ConstantHelpers.AREAS.SURVEY });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("LstHallazgosIRD", "Register", new { Area = ConstantHelpers.AREAS.SURVEY });
            }
        }


        public ActionResult _ConfirmacionDeleteEncuesta(Int32? EncuestaId, Int32? Cantidad)
        {
            var model = new _ConfirmacionDeleteEncuestaViewModel();
            model.Fill(CargarDatosContext(), EncuestaId, Cantidad);
            return View(model);
        }

        [HttpPost]
        public JsonResult _ConfirmacionDeleteEncuesta(_ConfirmacionDeleteEncuestaViewModel model)
        {
            var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.EncuestaId);
            try
            {
                encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                var tipo = Context.TipoEncuesta.FirstOrDefault(x => x.IdTipoEncuesta == encuesta.IdTipoEncuesta);
                var ruta = "";
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                switch (tipo.Acronimo)
                {
                    case ConstantHelpers.ENCUESTA.GRA:
                        if (model.Cantidad == 1)
                            ruta = Url.Action("MantenimientoEncuestaGRA", "Maintenance");
                        else
                            ruta = Url.Action("DetalleEncuestaGRA", "Maintenance", new { Carrera = encuesta.IdCarrera, Ciclo = encuesta.IdSubModalidadPeriodoAcademico }); break;
                    default:
                        ruta = Url.Action("MantenimientoEncuesta" + encuesta.TipoEncuesta.Acronimo, "Maintenance");
                        break;
                }
                return Json(ruta);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
                throw;
            }
        }
        public ActionResult _AsignarEstado(Int32 EncuestaId)
        {
            var model = new _AsignarEstadoViewModel();
            model.Fill(CargarDatosContext(), EncuestaId);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult _AsignarEstado(_AsignarEstadoViewModel model)
        {
            try
            {
                var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);
                encuesta.IdEstado = model.IdEstado;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("MantenimientoEncuestaFDC", "Maintenance");
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public ActionResult _ConfirmacionDeleteHallazgo(Int32 IdEncuesta, String CodigoHallazgo)
        {
            var model = new _ConfirmacionDeleteHallazgoViewModel();
            model.Fill(CargarDatosContext(), IdEncuesta, CodigoHallazgo);
            return View(model);
        }

        [HttpPost]
        public ActionResult _ConfirmacionDeleteHallazgo(_ConfirmacionDeleteHallazgoViewModel model)
        {
            var tipo = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta).TipoEncuesta.Acronimo;
            try
            {
                var hallazgo = Context.Hallazgo.Where(x => x.Codigo == model.CodigoHallazgo).ToList();
                foreach (var item in hallazgo)
                {
                    item.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                }

                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("AddEditFindSurvey" + tipo, "Register", new { IdEncuesta = model.IdEncuesta });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("AddEditFindSurvey" + tipo, "Register", new { IdEncuesta = model.IdEncuesta });
            }
        }


        //NUEVO EVD

        #region EVD

        public ActionResult MantenimientoMaestroEVD(MantenimientoMaestroEVDViewModel model, Int32? numeroPagina, Int32? IdEncuestaVirtualDelegado)
        {
            int parModalidadId = Session.GetModalidadId();

            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == parModalidadId && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var viewmodel = new MantenimientoMaestroEVDViewModel();

            viewmodel.Fill(CargarDatosContext(), numeroPagina, model.IdPeriodoAcademico, model.IdModulo, parModalidadId, IdEncuestaVirtualDelegado, IdSubModalidadPeriodoAcademico);

            return View(viewmodel);
        }


        public ActionResult MantenimientoEncuestaEVD(MantenimientoEncuestaEVDViewModel model, Int32? numeroPagina)
        {
            int parModalidadId = Session.GetModalidadId();
            int parIdPeriodoAcademico = (from a in Context.PeriodoAcademico
                                         join b in Context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                         join c in Context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                                         where c.IdModalidad == parModalidadId
                                         select a).FirstOrDefault().IdPeriodoAcademico;

            var viewmodel = new MantenimientoEncuestaEVDViewModel();

            viewmodel.Fill(CargarDatosContext(), numeroPagina, model.IdPeriodoAcademico, model.IdModulo, parModalidadId);

            return View(viewmodel);
        }


        public ActionResult AgregarDelegadosEncuestaEVD(Int32? IdEncuestaVirtualDelegado)
        {
            var viewmodel = new AgregarDelegadosEncuestaEVDViewModel();
            viewmodel.Fill(CargarDatosContext(), IdEncuestaVirtualDelegado);

            bool satisfactorio = viewmodel.GetAgregadosSatisfactoriamente();
            int cantidadeinvitados = viewmodel.GetCantidadInvitadosAgregados();
            int cantidadelegados = viewmodel.GetCantidadDelegadosAgregados();

            if (satisfactorio)
            {
                if (cantidadelegados > 0)
                {
                    PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se agregaron satisfactoriamente " + cantidadelegados.ToString() + " delegados y " + cantidadeinvitados.ToString() + " invitados." : cantidadelegados.ToString() + " delegates were successfully added.");
                }
                else
                {
                    PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Todos los delegados e invitados se encuentran registrados para esta encuesta." : "All delegates and guests are registered for this survey.");
                }
            }
            else
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Se encontraron problemas, intente nuevamente." : "Problems were found, try again");
            }

            return RedirectToAction("ConfiguracionEVD", "Configuration");
        }

        public ActionResult DetalleEncuestaEVD(Int32? IdEncuestaVirtualDelegado, String CicloAcademico, Int32? Cantidad, Int32? IdModulo)
        {
            var viewmodel = new DetalleEncuestaEVDViewModel();

            viewmodel.Fill(CargarDatosContext(), IdEncuestaVirtualDelegado, CicloAcademico, Cantidad, IdModulo);

            return View(viewmodel);
        }

        public ActionResult FinalizarEVD(Int32 IdEncuestaVirtualDelegado)
        {
            try
            {
                var EVDFin = Context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado);


                //var comision = (from a in context.EncuestaVirtualDelegado
                //                    join b in context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademicoModulo equals b.IdSubModalidadPeriodoAcademicoModulo
                //                    join c in context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                //                    join d in context.CarreraComision on c.IdSubModalidadPeriodoAcademico equals d.IdSubModalidadPeriodoAcademico
                //                    join e in context.Comision on d.IdComision equals e.IdComision
                //                    where a.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado && e.NombreEspanol != "WASC"
                //                    select e).FirstOrDefault();


                //            int EscuelaId = session.GetEscuelaId();
                //List<HallazgoAutomatico> ListOrdenadaparaInsertarHallazgo = new List<HallazgoAutomatico>();

                //var ListComentariosGeneraHallazgo = (from a in context.ComentarioDelegado
                //									 join b in context.Pregunta on a.IdPregunta equals b.IdPregunta
                //									 join c in context.NivelSatisfaccion on a.IdNivelSatisfaccion equals c.IdNivelSatisfaccion
                //									 join d in context.EncuestaVirtualDelegado on a.IdEncuestaVirtualDelegado equals d.IdEncuestaVirtualDelegado
                //									 where c.GeneraHallazgo == true && d.IdEscuela == EscuelaId && a.IdEncuestaVirtualDelegado == EVDFin.IdEncuestaVirtualDelegado
                //									 select a
                //									  ).ToList();



                //foreach (var obj in ListComentariosGeneraHallazgo)
                //{
                //	string ns = context.NivelSatisfaccion.Where(X => X.IdNivelSatisfaccion == obj.IdNivelSatisfaccion).FirstOrDefault().DescripcionEspanol;

                //	HallazgoAutomatico ohallazgo = new HallazgoAutomatico();
                //	ohallazgo.IdComentarioDelegado = obj.IdComentarioDelegado;
                //	ohallazgo.CantidadAlumnos = 1;
                //	ohallazgo.IdCurso = Convert.ToInt32( obj.IdCurso);
                //	ohallazgo.CodigoCurso = obj.Curso.Codigo;
                //	ohallazgo.Comentario = obj.DescripcionEspanol;
                //	ohallazgo.borrar = false;
                //	ohallazgo.IdAlumno = Convert.ToInt32( obj.IdAlumno);
                //	ohallazgo.DescripcionPregunta = obj.Pregunta.DescripcionEspanol;
                //	ohallazgo.NombreNivelSatisfaccion = ns;
                //	ohallazgo.IdSubModalidadPeriodoAcademico = obj.EncuestaVirtualDelegado.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico;
                //	ListOrdenadaparaInsertarHallazgo.Add(ohallazgo);

                //}

                //for (int i = 0; i < ListOrdenadaparaInsertarHallazgo.Count(); i++)
                //{
                //	for (int j = i; j < ListOrdenadaparaInsertarHallazgo.Count(); j++)
                //	{
                //		if (j != i)
                //		{
                //			if (ListOrdenadaparaInsertarHallazgo[i].IdCurso == ListOrdenadaparaInsertarHallazgo[j].IdCurso && ListOrdenadaparaInsertarHallazgo[j].borrar == false)
                //			{
                //				ListOrdenadaparaInsertarHallazgo[i].CantidadAlumnos += 1;
                //				ListOrdenadaparaInsertarHallazgo[j].borrar = true;
                //				break;
                //			}
                //		}
                //	}
                //}

                //var ListaFilComentarios = ListOrdenadaparaInsertarHallazgo.Where(x => x.borrar == false).ToList();



                //foreach (var oevd in ListaFilComentarios)
                //{
                //	var alumnocomentario = context.AlumnoMatriculado.Where(x => x.IdAlumno == oevd.IdAlumno && x.IdSubModalidadPeriodoAcademico == oevd.IdSubModalidadPeriodoAcademico).FirstOrDefault();

                //	Hallazgo ohallazgo = new Hallazgo();
                //	ohallazgo.IdSubModalidadPeriodoAcademico = alumnocomentario.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
                //	ohallazgo.IdSede = alumnocomentario.IdSede;
                //	ohallazgo.IdInstrumento = context.Instrumento.Where(x => x.Acronimo == "EVD").FirstOrDefault().IdInstrumento;
                //	ohallazgo.IdConstituyente = 2;
                //	ohallazgo.IdAcreditadora = 1;
                //	ohallazgo.FechaRegistro = DateTime.Now;
                //	ohallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                //	ohallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                //	ohallazgo.IdNivelAceptacionHallazgo = 1;
                //	ohallazgo.IdCriticidad = 2;
                //                string comentarioespanol = oevd.CantidadAlumnos + " alumno de " + oevd.CantidadAlumnos + " eligieron: " + oevd.NombreNivelSatisfaccion.ToUpper() + " sobre la pregunta " + oevd.DescripcionPregunta.ToUpper() + " .";
                //                ohallazgo.DescripcionEspanol = comentarioespanol;

                //                YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                //                ohallazgo.DescripcionIngles = helper.Translate(comentarioespanol);


                //                ohallazgo.IdCurso = oevd.IdCurso;

                //	string cursocodigo = context.Curso.Where(x => x.IdCurso == oevd.IdCurso).FirstOrDefault().Codigo;
                //	string ciclo = context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == EVDFin.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico.CicloAcademico;
                //	string sede = context.Sede.Where(x => x.IdSede == alumnocomentario.IdSede).FirstOrDefault().Codigo;
                //	string NombreModulo = context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == EVDFin.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault().Modulo.IdentificadorSeccion;

                //	ohallazgo.Codigo = String.Format("{0}-{1}-{2}-{3}-F-{4}-{5}", "EVD", cursocodigo, ciclo, NombreModulo, sede, context.Hallazgo.Max(x => x.IdHallazgo) + 1);
                //	ohallazgo.IdComision = comision.IdComision;
                //	ohallazgo.IdCarrera = alumnocomentario.IdCarrera;

                //	context.Hallazgo.Add(ohallazgo);

                //	context.SaveChanges();

                //	ComentarioHallazgo comha = new ComentarioHallazgo();
                //	comha.IdComentarioDelegado = Convert.ToInt32(oevd.IdComentarioDelegado);
                //	comha.IdHallazgo = ohallazgo.IdHallazgo;

                //	context.ComentarioHallazgo.Add(comha);
                //	context.SaveChanges();

                //}
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se culminó la encuesta " + EVDFin.Codigo + " ." : "The " + EVDFin.Codigo + " survey was completed.");

                EVDFin.Estado = "FIN";
                Context.SaveChanges();
                return RedirectToAction("ConfiguracionEVD", "Configuration");

            }
            catch (Exception e)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Se encontraron problemas, intente nuevamente." : "Problems were found, try again");

                return RedirectToAction("ConfiguracionEVD", "Configuration");

                throw e;
            }

        }


        public ActionResult ActivarEVD(Int32 IdEncuestaVirtualDelegado)
        {
            try
            {

                var LstEVD = Context.EncuestaVirtualDelegado.ToList();

                foreach (var oevd in LstEVD)
                {
                    if (oevd.Estado != "FIN" && oevd.Estado != "PRC")
                    {
                        if (oevd.Estado == "ACT")
                        {
                            oevd.Estado = "PRC";

                        }
                        else
                        {
                            oevd.Estado = "INA";
                        }
                    }
                }

                var iLstEVD = LstEVD.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado);

                iLstEVD.Estado = "ACT";

                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se activó la encuesta " + iLstEVD.Codigo + " satisfactoriamente." : "The survey " + iLstEVD.Codigo + "was activated successfully.");


                Context.SaveChanges();

                return RedirectToAction("ConfiguracionEVD", "Configuration");
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? e.Message : e.Message);
                return RedirectToAction("ConfiguracionEVD", "Configuration");
            }


        }
        #endregion
    }
}