﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using System.IO;
using Microsoft.Reporting.WebForms;
using Zen.Barcode;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Areas.Survey;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Acreditador)]
    public class SurveyController : BaseController
    {
        // GET: Survey
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RegistrarEncuesta(String Tipo, Int32? EncuestaId)
        {
            var viewmodel = new RegistrarEncuestaViewModel();
            viewmodel.Fill(Tipo, EncuestaId);
            return View(viewmodel);
        }
        public ActionResult ConsultaSeccion()
        {
            var viewmodel = new SeccionCursoViewModel(CargarDatosContext());
            return View(viewmodel);
        }
        public ActionResult SurveyManagementPPP()
        {
            var viewmodel = new SurveyManagementPPPViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }

        public ActionResult NewSurveyManagementPPP()
        {
            var viewmodel = new SurveyManagementPPPViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }

        public ActionResult SurveyManagementFDC()
        {
            var viewmodel = new SurveyManagementFDCViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }
        public ActionResult SurveyManagementGRA()
        {
            var viewmodel = new SurveyManagementGRAViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }

        public JsonResult ConfirmAutomaticFindingsCreation()
        {
            var idSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            var subModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico);
            var modalidad = subModalidadPeriodoAcademico?.SubModalidad?.Modalidad;
            var periodoAcademico = subModalidadPeriodoAcademico?.PeriodoAcademico;

            var title = currentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                "¿Está seguro?" :
                "Are you sure?";
            var text = currentCulture == ConstantHelpers.CULTURE.ESPANOL ?
                "Se generarán los hallazgos automáticos de las encuestas graduando del Ciclo " + periodoAcademico?.CicloAcademico + " de la Modalidad " + modalidad?.NombreEspanol :
                "You are about to generate automatic findings of graduated surveys of the " + periodoAcademico?.CicloAcademico + " Cycle, " + modalidad?.NombreIngles + " Modality.";

            return Json(new {title, text}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateAutomaticFindingsGRA()
        {
            var subModalidadPeriodoAcademicoId = Session.GetSubModalidadPeriodoAcademicoId();
            var result = Context.Usp_CrearHallazgosGRAAutomaticos(subModalidadPeriodoAcademicoId);

            if (result > 0)
            {
                var message = currentCulture == ConstantHelpers.CULTURE.INGLES ?
                    "Automatic findings created." :
                    "Se crearon los hallazgos automáticos correctamente";

                PostMessage(MessageType.Success, message);
            }
            else
            {
                var message = currentCulture == ConstantHelpers.CULTURE.INGLES ?
                    "There was an error." :
                    "Ocurrió un error en la creación de hallazgos automáticos";

                PostMessage(MessageType.Error, message);
            }

            return RedirectToAction("SurveyManagementGRA");
        }

        public ActionResult SurveyManagementEVD()
        {
            var viewmodel = new SurveyManagementEVDViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }

        public ActionResult SurveyManagementIRD()
        {
            var viewmodel = new SurveyManagementIRDViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }

        public ActionResult CargaMasiva(String Tipo)
        {
            var viewmodel = new CargaMasivaViewModel();
            viewmodel.Fill(Tipo);
            return View(viewmodel);
        }

        #region MANTENIMIENTO
        public ActionResult MantenimientoEncuesta(String Tipo)
        {
            var viewmodel = new MantenimientoEncuestaViewModel();
            viewmodel.Fill(Tipo);
            return View(viewmodel);
        }
        public ActionResult HallazgoEncuesta(String Tipo, String Ciclo, Int32 IdAlumno, Int32 IdCarrera)
        {
            var viewmodel = new HallazgoEncuestaViewModel();
            var Alumno = Context.Alumno.FirstOrDefault(x => x.IdAlumno == IdAlumno);
            var Carrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera);
            viewmodel.Fill(Tipo, Ciclo, Alumno, Carrera);
            return View(viewmodel);
        }
        public ActionResult EliminarEncuesta(String Tipo)
        {
            //var viewmodel = new EliminarEncuestaViewModel();
            //viewmodel.Fill(Tipo);
            PostMessage(MessageType.Success, MessageResource.SeRealizoLaOperacionDeFormaCorrecta);
            return RedirectToAction("MantenimientoEncuesta", new { Tipo });
        }
        #endregion

        public ActionResult ReporteEncuesta(String Tipo, String Reporte)
        {
            var viewmodel = new ReporteEncuestaViewModel();
            viewmodel.Fill(Tipo, Reporte);
            return View(viewmodel);
        }
        public ActionResult ConfiguracionEncuesta(String Tipo)
        {
            var viewmodel = new ConfiguracionEncuestaViewModel();
            viewmodel.Fill(Tipo);
            return View(viewmodel);
        }
        public ActionResult NotificacionEncuesta()
        {
            var viewmodel = new NotificacionesEncuestaViewModel();
            viewmodel.Fill();
            return View(viewmodel);
        }
    }
}