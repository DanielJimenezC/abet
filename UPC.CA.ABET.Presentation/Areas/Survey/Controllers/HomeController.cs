﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Home;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Acreditador)]
    public class HomeController : BaseController
    {
        // GET: Survey/Home
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            model.Fill(CargarDatosContext());
            return View(model);
        }

        public ActionResult ChangeIdioma(String Idioma)
        {
            var culture = new CultureInfo(Idioma);

            Session.Set(SessionKey.Culture, culture);
            CookieHelpers.Set(CookieKey.Culture, Idioma);

            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }
    }
}