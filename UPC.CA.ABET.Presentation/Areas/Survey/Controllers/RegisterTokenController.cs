﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.RegisterToken;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Invitado, AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class RegisterTokenController : BaseController
    {
        // GET: Survey/RegisterToken
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ErrorRegisterSurvey()
        {
            return View();
        }
        public ActionResult SuccessRegisterSurvey()
        {
            return View();
        }

        public ActionResult AddSurveyGRAToken(String Token)
        {
            try
            {
                SecurityLogic secure = new SecurityLogic();

                var oToken = Context.EncuestaToken.FirstOrDefault(x => x.Token == Token);

                int? IdAlumno = oToken.IdAlumno;
                int? IdCarrera = oToken.IdCarrera;
                int? IdCiclo = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == oToken.IdSubModalidadPeriodoAcademico).IdPeriodoAcademico;

                if (IdAlumno == null || IdCarrera == null || IdCiclo == null)
                    return RedirectToAction("ErrorRegisterSurvey", "RegisterToken");

                EncuestaToken objToken = Context.EncuestaToken.FirstOrDefault(x => x.IdAlumno == IdAlumno &&
                                                                                   x.IdCarrera == IdCarrera &&
                                                                                   x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo &&
                                                                                   x.Estado == true);

                if (objToken == null)
                    return RedirectToAction("ErrorRegisterSurvey", "RegisterToken");
                int idescuela = Session.GetEscuelaId();
                var viewmodel = new AddSurveyGRATokenViewModel();
                viewmodel.Fill(CargarDatosContext(), objToken.IdAlumno, IdCiclo, objToken.IdCarrera, objToken.IdSubModalidadPeriodoAcademico, idescuela);

                return View(viewmodel);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult AddSGRAToken(AddSurveyGRATokenViewModel model, FormCollection frm)
        {
            try
            {
                EncuestaToken objToken = Context.EncuestaToken.FirstOrDefault(x => x.IdAlumno == model.IdAlumno &&
                                                                                   x.IdCarrera == model.IdCarrera &&
                                                                                   x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico &&
                                                                                   x.Estado == true);

                if (objToken == null)
                {
                    PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "Toke no válido." : "Invalid Token.");
                    return RedirectToAction("ErrorRegisterSurvey", "RegisterToken");
                }
                Encuesta encuesta = new Encuesta();
                var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA);
                encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

                encuesta.IdCarrera = model.IdCarrera;
                encuesta.Comentario = model.Comentario;
                encuesta.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico;

                Sede objSede = (from AM in Context.AlumnoMatriculado
                                join submodapa in Context.SubModalidadPeriodoAcademico on AM.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                                join S in Context.Sede on AM.IdSede equals S.IdSede
                                where submodapa.PeriodoAcademico.Estado == "ACT" && AM.IdAlumno == model.IdAlumno
                                select S).FirstOrDefault();
                if (objSede != null)
                    encuesta.IdSede = objSede.IdSede;

                Context.Encuesta.Add(encuesta);
                Context.SaveChanges();

                RegisterSurveyLogic addEditSurvey = new RegisterSurveyLogic();
                addEditSurvey.RegisterPerformanceGRA(Context, encuesta, frm, false);
                Context.SaveChanges();

                /* ACTUALIZAMOS EL OBJETO ENCUESTATOKEN */
                objToken.Estado = false;
                objToken.IdEncuesta = encuesta.IdEncuesta;
                Context.Entry(objToken).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();

                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "La encuesta ha sido guardada exitosamente. Gracias." : "The survey has been successfully saved. Thank you.");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                PostMessage(MessageType.Error);
            }

            return RedirectToAction("SuccessRegisterSurvey", "RegisterToken");
        }

        //EVD

        public ActionResult AddSurveyEVDToken(int IdEncuestaVirtualDelegado, String Token)
        {

            AddSurveyEVDTokenViewModel viewModel = new AddSurveyEVDTokenViewModel();
            viewModel.IdEncuestaVirtualDelegado = IdEncuestaVirtualDelegado;
            viewModel.CargarDatos(CargarDatosContext(), Token);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddSurveyEVDToken(AddSurveyEVDTokenViewModel viewModel)
        {
            try
            {
                EncuestaToken objToken = Context.EncuestaToken.FirstOrDefault(x => x.IdAlumno == viewModel.IdAlumno &&
                                                                                   x.IdCarrera == viewModel.IdCarrera &&
                                                                                   x.IdSubModalidadPeriodoAcademico == viewModel.IdSubModalidadPeriodoAcademico &&
                                                                                   x.Estado == true);

                if (objToken == null)
                {
                    PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "Token no válido." : "Invalid Token.");
                    return RedirectToAction("ErrorRegisterSurvey", "RegisterToken");
                }


                List<Pregunta> oLstPregunta = new List<Pregunta>();
                int EscuelaId = session.GetEscuelaId();
                int EVDId = session.GetEncuestaVirtualDelegadoId();
                var oEVD = Context.EncuestaVirtualDelegado.Where(x => x.IdEncuestaVirtualDelegado == EVDId).FirstOrDefault();
                var curso = Context.Curso.Where(x => x.IdCurso == viewModel.IdCurso).FirstOrDefault();


                foreach (var obj in viewModel.LstPregunta)
                {
                    Pregunta objpr = new Pregunta();
                    objpr = Context.Pregunta.Where(x => x.IdPregunta == obj.IdPregunta).FirstOrDefault();
                    oLstPregunta.Add(objpr);
                }

                for (int i = 0; i < viewModel.LstComentarios.Count; i++)
                {
                    if (viewModel.LstComentarios[i] == "")
                        return Json(new { result = "error" }, JsonRequestBehavior.AllowGet);
                }

                for (int i = 0; i < viewModel.LstComentarios.Count; i++)
                {
                    int numeroComentario = Context.ComentarioDelegado.Count();

                    string resp = viewModel.LstComentarios[i];
                    ComentarioDelegado cDelegado = new ComentarioDelegado();
                    cDelegado.IdAlumno = viewModel.IdAlumno;
                    cDelegado.IdCurso = viewModel.IdCurso;
                    if (viewModel.IdDocente == 0)
                    {
                        cDelegado.IdDocente = null;
                    }
                    else
                    {
                        cDelegado.IdDocente = viewModel.IdDocente;

                    }
                    if (oLstPregunta[i].IdTipoPregunta == 1)
                        cDelegado.DescripcionEspanol = resp;
                    else
                        cDelegado.IdNivelSatisfaccion = Int32.Parse(resp);
                    cDelegado.CodigoComentario = "COM-EVD-" + oEVD.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico + "-" + oEVD.SubModalidadPeriodoAcademicoModulo.Modulo.IdentificadorSeccion + "-" + curso.Codigo + "-" + (numeroComentario + i).ToString();

                    cDelegado.IdPregunta = oLstPregunta[i].IdPregunta;
                    cDelegado.IdEncuestaVirtualDelegado = viewModel.IdEncuestaVirtualDelegado;
                    Context.ComentarioDelegado.Add(cDelegado);
                }

                Context.SaveChanges();

                if (viewModel.CursosCount > 0)
                {
                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("AddSurveyEVDToken", "RegisterToken", new { IdEncuestaVirtualDelegado = viewModel.IdEncuestaVirtualDelegado, Token = viewModel.Token });
                    return Json(new { result = "reload", Url = redirectUrl }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { result = "continue" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { result = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult showSurvey(Int32? IdEncuestaVirtualDelegado)
        {

            ShowSurveyEVDViewModel viewModel = new ShowSurveyEVDViewModel();
            viewModel.Fill(CargarDatosContext(), IdEncuestaVirtualDelegado);
            return View(viewModel);
        }

        public ActionResult FinalizeSurvey(String Token)
        {
            var oToken = Context.EncuestaToken.FirstOrDefault(x => x.Token == Token);

            /* ACTUALIZAMOS EL OBJETO ENCUESTATOKEN */
            /*AGREGAMOS UNA ENCUESTA Y PERFORMANCE */
            Encuesta encuesta = new Encuesta();
            var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.EVD);
            encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
            encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

            encuesta.IdCarrera = oToken.IdCarrera;
            encuesta.IdSubModalidadPeriodoAcademico = oToken.IdSubModalidadPeriodoAcademico;

            Sede objSede = (from AM in Context.AlumnoMatriculado
                            join submodapa in Context.SubModalidadPeriodoAcademico on AM.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                            join S in Context.Sede on AM.IdSede equals S.IdSede
                            where submodapa.PeriodoAcademico.Estado == "ACT" && AM.IdAlumno == oToken.IdAlumno
                            select S).FirstOrDefault();
            if (objSede != null)
                encuesta.IdSede = objSede.IdSede;

            Context.Encuesta.Add(encuesta);
            Context.SaveChanges();


            /* ACTUALIZAMOS EL OBJETO ENCUESTATOKEN */
            oToken.Estado = false;
            oToken.IdEncuesta = encuesta.IdEncuesta;
            Context.Entry(oToken).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();

            PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "La encuesta ha sido guardada exitosamente. Gracias." : "The survey has been successfully saved. Thank you.");

            return View();
        }

    }
}