﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Survey.Logic;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView;
using System.Configuration;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera)]
    public class EmailController : BaseController
    {
        public ActionResult EmailSurveyGRA()
        {
            var model = new EmailSurveyGRAViewModel();
            model.Fill(CargarDatosContext(), @Session.GetModalidadId());
            return View(model);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EmailNSurveyGRA(EmailSurveyGRAViewModel model)
        {
            int parModalidadId = Session.GetModalidadId();
            int IdEscuela = Session.GetEscuelaId();

            int? IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            int? PeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;

            //var NotificacionEncuestaAlumnoReenviar = context.NotificacionEncuestaAlumno.Where(x => x.IdEncuestaVirtualDelegado == null && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdEncuestaToken.HasValue && x.Alumno.Carrera.IdEscuela==EscuelaId).ToList();


            var NotificacionEncuestaAlumnoReenviar = (from a in Context.NotificacionEncuestaAlumno
                                                      join b in Context.Carrera on a.IdCarrera equals b.IdCarrera
                                                      where (a.IdEncuestaVirtualDelegado == null && a.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && a.IdEncuestaToken.HasValue && b.IdEscuela == IdEscuela)
                                                      select a).ToList();



            var ListaparaReenviar = NotificacionEncuestaAlumnoReenviar.Where(x => x.EncuestaToken.Estado == true).ToList();



            ConfiguracionNotificacion objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo == "GRA");


            if (String.IsNullOrEmpty(model.lstDatos) && ListaparaReenviar.Count() == 0)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "No ha agregado alumnos para notificar." : "You have not added students to notify.");
                return RedirectToAction("EmailSurveyGRA");
            }
            else
            {
                EmailGRALogic mailLogic = new EmailGRALogic();

                foreach (NotificacionEncuestaAlumno item in ListaparaReenviar)
                {


                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                                                select e).FirstOrDefault();

                    bool enviado;

                    Random rnd = new Random();
                    int token = rnd.Next(1000000, 9999999);

                    while (Context.EncuestaToken.FirstOrDefault(x => x.Token == token.ToString()) != null)
                    {
                        token = rnd.Next(1000000, 9999999);
                    }

                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + objValidar.Token.ToString());

                    /*+ token.ToString());*/

                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);

                    string correo = GetCorreo(item.Alumno.Codigo);
                   
                    if (objValidar != null)
                    {
                        if (objValidar.Estado == true)
                        {

                            enviado = mailLogic.SendMail(model.objConfiguracionNotificacion.NombreAsunto, mailLogic.GetHtmlEmail(model.objConfiguracionNotificacion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), correo);


                            if (enviado)
                            {
                                objValidar.FechaEnvio = DateTime.Now;

                                Context.SaveChanges();
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");

                                continue; //validar que si ya se le ha enviado la notificacion no se le vuelva de enviar
                            }
                            else
                            {
                                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                                return RedirectToAction("EmailSurveyGRA");
                            }

                        }

                        continue; //validar que si ya se le ha enviado la notificacion no se le vuelva de enviar

                    }



                }

            }


            if (objConfiguracion == null)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Aún no se ha configurado el asunto, mensaje y fecha fin de las notificaciones." : "The subject, message, and deadline of notifications have not yet been configured.");
                return RedirectToAction("EmailSurveyGRA");
            }

            if (objConfiguracion.FechaFinLimite.AddDays(1) < DateTime.Now)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "La fecha máxima de la notificación no excede a la fecha actual." : "The notification deadline does not exceed the current date.");
                return RedirectToAction("EmailSurveyGRA");
            }

            try
            {
                List<StudenNotificationGRA> lstAuxAlumnos = new List<StudenNotificationGRA>();
                if (!String.IsNullOrEmpty(model.lstDatos))
                {
                    var firstEncode = model.lstDatos.Split(';');
                    for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                    {
                        var data = firstEncode[i].Split('|');
                        lstAuxAlumnos.Add(new StudenNotificationGRA
                        {
                            IdAlumno = ConvertHelpers.ToInteger(data[j]),
                            IdCarrera = ConvertHelpers.ToInteger(data[j + 1]),
                            Correo = data[j + 2]
                        });
                    }
                }

                EmailGRALogic mailLogic = new EmailGRALogic();

                foreach (StudenNotificationGRA item in lstAuxAlumnos)
                {


                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                                                select e).FirstOrDefault();

                    bool enviado;


                    Random rnd = new Random();
                    int token = rnd.Next(1000000, 9999999);

                    while (Context.EncuestaToken.FirstOrDefault(x => x.Token == token.ToString()) != null)
                    {
                        token = rnd.Next(1000000, 9999999);
                    }

                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + token.ToString());

                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);


                    if (objValidar != null)
                    {
                        if (objValidar.Estado == true)
                        {
                            String urlSurvey2 = String.Format("{0}://{1}{2}{3}",
                                                       Request.Url.Scheme,
                                                       Request.Url.Authority,
                                                       vdirectory,
                                                       "/?Token=" + objValidar.Token.ToString());

                            enviado = mailLogic.SendMail(model.objConfiguracionNotificacion.NombreAsunto, mailLogic.GetHtmlEmail(model.objConfiguracionNotificacion.NombreContenido, urlSurvey2, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), item.Correo);


                            if (enviado)
                            {
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");

                                continue; //validar que si ya se le ha enviado la notificacion no se le vuelva de enviar
                            }

                        }

                        continue; //validar que si ya se le ha enviado la notificacion no se le vuelva de enviar

                    }

                    enviado = mailLogic.SendMail(model.objConfiguracionNotificacion.NombreAsunto, mailLogic.GetHtmlEmail(model.objConfiguracionNotificacion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), item.Correo);

                    if (enviado)
                    {
                        EncuestaToken oEncuestaToken = new EncuestaToken();
                        oEncuestaToken.Token = token.ToString();
                        oEncuestaToken.Estado = true;
                        oEncuestaToken.FechaEnvio = DateTime.Today;
                        oEncuestaToken.FechaFin = model.objConfiguracionNotificacion.FechaFinLimite;
                        oEncuestaToken.IdAlumno = item.IdAlumno;
                        oEncuestaToken.IdCarrera = item.IdCarrera;
                        oEncuestaToken.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico ?? 0;
                        Context.EncuestaToken.Add(oEncuestaToken);
                        oEncuestaToken.Tipo = "GRA";
                        Context.SaveChanges();

                        NotificacionEncuestaAlumno objNotificacion = new NotificacionEncuestaAlumno();
                        objNotificacion.IdEncuestaToken = oEncuestaToken.IdEncuestaToken;
                        objNotificacion.IdAlumno = oEncuestaToken.IdAlumno;
                        objNotificacion.IdSubModalidadPeriodoAcademico = oEncuestaToken.IdSubModalidadPeriodoAcademico;
                        objNotificacion.IdCarrera = oEncuestaToken.IdCarrera;
                        objNotificacion.Estado = true;
                        Context.NotificacionEncuestaAlumno.Add(objNotificacion);
                        Context.SaveChanges();

                        var objNotificacionAnt = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdAlumno == objNotificacion.IdAlumno && x.IdCarrera == objNotificacion.IdCarrera && x.IdSubModalidadPeriodoAcademico == objNotificacion.IdSubModalidadPeriodoAcademico && x.Estado == false);
                        Context.NotificacionEncuestaAlumno.Remove(objNotificacionAnt);
                        Context.SaveChanges();
                    }
                    else
                    {
                        PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                        return RedirectToAction("EmailSurveyGRA");
                    }
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "No se pudo realizar el envío." : "Could not send.");
            }
            return RedirectToAction("EmailSurveyGRA");
        }

        public ActionResult EmailSurveyEVD()
        {
            try
            {
                var model = new EmailSurveyEVDViewModel();
                model.Fill(CargarDatosContext(), @Session.GetModalidadId());
                return View(model);
            }
            catch
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Debes activar una encuesta." : "You must activate a survey.");

                return RedirectToAction("SurveyManagementEVD", "Survey");
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EmailNSurveyEVD(EmailSurveyEVDViewModel model)
        {
            int IdEscuela = Session.GetEscuelaId();
            var oEVDValidar = Context.EncuestaVirtualDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela).FirstOrDefault();
            ConfiguracionNotificacion objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo == "EVDD");
            var ListDelegadosEVD = Context.NotificacionEncuestaAlumno.Where(x => x.IdEncuestaToken != null && x.IdEncuestaVirtualDelegado == oEVDValidar.IdEncuestaVirtualDelegado);
            EmailGRALogic mailLogic = new EmailGRALogic();

            if (objConfiguracion == null)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Aún no se ha configurado el asunto, mensaje y fecha fin de las notificaciones." : "The subject, message, and deadline of notifications have not yet been configured.");
                return RedirectToAction("EmailSurveyEVD");
            }

            if (objConfiguracion.FechaFinLimite.AddDays(1) < DateTime.Now)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "La fecha máxima de la notificación no excede a la fecha actual." : "The notification deadline does not exceed the current date.");
                return RedirectToAction("EmailSurveyEVD");
            }
            if (String.IsNullOrEmpty(model.lstDatosDelegados) && ListDelegadosEVD.Count() == 0)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "No ha agregado alumnos para notificar." : "You have not added students to notify.");
                return RedirectToAction("EmailSurveyEVD");
            }
            else
            if (ListDelegadosEVD.Count() > 0)
            {
                foreach (NotificacionEncuestaAlumno item in ListDelegadosEVD)
                {

                    int parModalidadId = Session.GetModalidadId();
                    var EVD = Context.EncuestaVirtualDelegado.FirstOrDefault(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela);
                    var SubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == EVD.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault();
                    var PeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico;

                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico &&
                                                      e.Tipo == "EVD" && e.IdEncuestaVirtualDelegado == EVD.IdEncuestaVirtualDelegado
                                                select e).FirstOrDefault();

                    String token = String.Format("{0}|{1}|{2}|{3}", item.IdAlumno, item.IdCarrera, PeriodoAcademico.IdPeriodoAcademico, item.IdNotificacion);
                    String md5Token = SecurityLogic.Encrypt(token);
                    md5Token = md5Token.Replace("+", "L");
                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + md5Token);
                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);
                    bool enviado;
                    string correo;
                    correo = GetCorreo(item.Alumno.Codigo);
                    String CodigoEncuesta = Context.Usp_GetCodigoNomenclatura(EVD.IdEncuestaVirtualDelegado).FirstOrDefault().ToString();

                    if (objValidar != null)
                    {
                        if (objValidar.Estado)
                        {
                            enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), correo);

                            if (enviado)
                            {
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
                            }
                            else
                            {
                                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                                return RedirectToAction("EmailSurveyEVD");
                            }
                            continue;
                        }
                        continue;
                    }
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
            }

            try
            {
                List<StudenNotificationEVD> lstAuxAlumnos = new List<StudenNotificationEVD>();
                if (!String.IsNullOrEmpty(model.lstDatosDelegados))
                {
                    var firstEncode = model.lstDatosDelegados.Split(';');
                    for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                    {
                        var data = firstEncode[i].Split('|');
                        lstAuxAlumnos.Add(new StudenNotificationEVD
                        {
                            IdAlumno = ConvertHelpers.ToInteger(data[j]),
                            IdCarrera = ConvertHelpers.ToInteger(data[j + 1]),
                            Correo = data[j + 2],
                            IdNotificacion = ConvertHelpers.ToInteger(data[j + 3])
                        });
                    }
                }

                foreach (StudenNotificationEVD item in lstAuxAlumnos)
                {

                    int parModalidadId = Session.GetModalidadId();
                    var EVD = Context.EncuestaVirtualDelegado.FirstOrDefault(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela);
                    var SubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == EVD.IdSubModalidadPeriodoAcademicoModulo);
                    var PeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico;

                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico &&
                                                      e.Tipo == "EVD" && e.IdEncuestaVirtualDelegado == EVD.IdEncuestaVirtualDelegado
                                                select e).FirstOrDefault();

                    String token = String.Format("{0}|{1}|{2}|{3}", item.IdAlumno, item.IdCarrera, PeriodoAcademico.IdPeriodoAcademico, item.IdNotificacion);

                    String md5Token = SecurityLogic.Encrypt(token);
                    md5Token = md5Token.Replace("+", "L");

                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + md5Token);

                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);

                    bool enviado;
                    String CodigoEncuesta = Context.Usp_GetCodigoNomenclatura(EVD.IdEncuestaVirtualDelegado).FirstOrDefault().ToString();

                    if (objValidar != null)
                    {
                        if (objValidar.Estado)
                        {
                            enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), item.Correo);
                            if (enviado)
                            {
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
                            }
                            else
                            {
                                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                                return RedirectToAction("EmailSurveyEVD");
                            }
                            continue;
                        }
                        continue;
                    }

                    enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), item.Correo);


                    if (enviado)
                    {
                        EncuestaToken oEncuestaToken = new EncuestaToken();
                        oEncuestaToken.Token = md5Token;
                        oEncuestaToken.Estado = true;
                        oEncuestaToken.FechaEnvio = DateTime.Today;
                        oEncuestaToken.FechaFin = objConfiguracion.FechaFinLimite;
                        oEncuestaToken.IdAlumno = item.IdAlumno;
                        oEncuestaToken.IdCarrera = item.IdCarrera;
                        oEncuestaToken.IdSubModalidadPeriodoAcademico = EVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico;
                        oEncuestaToken.Tipo = "EVD";
                        oEncuestaToken.IdEncuestaVirtualDelegado = EVD.IdEncuestaVirtualDelegado;
                        Context.EncuestaToken.Add(oEncuestaToken);
                        Context.SaveChanges();

                        NotificacionEncuestaAlumno objNotificacion = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == item.IdNotificacion);
                        objNotificacion.IdEncuestaToken = oEncuestaToken.IdEncuestaToken;

                        Context.SaveChanges();

                    }
                    else
                    {
                        PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                        return RedirectToAction("EmailSurveyEVD");
                    }
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "No se pudo realizar el envío." : "Could not send.");
            }
            return RedirectToAction("EmailSurveyEVD");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EmailNSurveyEVDInvitado(EmailSurveyEVDViewModel model)
        {
            int IdEscuela = Session.GetEscuelaId();
            var oEVDValidar = Context.EncuestaVirtualDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela).FirstOrDefault();
            var SubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVDValidar.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault();
            ConfiguracionNotificacion objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo == "EVDI");
            //var ListDelegadosEVD = context.NotificacionEncuestaAlumno.Where(x => x.IdEncuestaToken != null && x.IdEncuestaVirtualDelegado == oEVDValidar.IdEncuestaVirtualDelegado);
            var ListInvitados = Context.AlumnoInvitado.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico && x.IdModulo == SubModalidadPeriodoAcademicoModulo.IdModulo && x.Tipo == "EVD").Select(x => x.codigo);



            var ListInvitadosEVD = (from a in Context.NotificacionEncuestaAlumno
                                    join b in Context.Alumno on a.IdAlumno equals b.IdAlumno
                                    where ListInvitados.Contains(b.Codigo) && a.IdEncuestaToken != null && a.IdEncuestaVirtualDelegado == oEVDValidar.IdEncuestaVirtualDelegado
                                    select a).Distinct().ToList();

            EmailGRALogic mailLogic = new EmailGRALogic();

            if (objConfiguracion == null)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "Aún no se ha configurado el asunto, mensaje y fecha fin de las notificaciones." : "The subject, message, and deadline of notifications have not yet been configured.");
                return RedirectToAction("EmailSurveyEVD");
            }

            if (objConfiguracion.FechaFinLimite.AddDays(1) < DateTime.Now)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "La fecha máxima de la notificación no excede a la fecha actual." : "The notification deadline does not exceed the current date.");
                return RedirectToAction("EmailSurveyEVD");
            }
            if (String.IsNullOrEmpty(model.lstDatosInvitados) && ListInvitadosEVD.Count() == 0)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "No ha agregado alumnos para notificar." : "You have not added students to notify.");
                return RedirectToAction("EmailSurveyEVD");
            }
            else
            if (ListInvitadosEVD.Count() > 0)
            {
                foreach (NotificacionEncuestaAlumno item in ListInvitadosEVD)
                {

                    int parModalidadId = Session.GetModalidadId();
                    var EVD = Context.EncuestaVirtualDelegado.FirstOrDefault(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela);
                    var PeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico;

                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico &&
                                                      e.Tipo == "EVD" && e.IdEncuestaVirtualDelegado == EVD.IdEncuestaVirtualDelegado
                                                select e).FirstOrDefault();

                    String token = String.Format("{0}|{1}|{2}|{3}", item.IdAlumno, item.IdCarrera, PeriodoAcademico.IdPeriodoAcademico, item.IdNotificacion);
                    String md5Token = SecurityLogic.Encrypt(token);
                    md5Token = md5Token.Replace("+", "L");
                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + md5Token);
                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);
                    bool enviado;
                    string correo;
                    correo = GetCorreo(item.Alumno.Codigo);
                    String CodigoEncuesta = Context.Usp_GetCodigoNomenclatura(EVD.IdEncuestaVirtualDelegado).FirstOrDefault().ToString();

                    if (objValidar != null)
                    {
                        if (objValidar.Estado)
                        {
                            enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), correo);

                            if (enviado)
                            {
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
                            }
                            else
                            {
                                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                                return RedirectToAction("EmailSurveyEVD");
                            }
                            continue;
                        }
                        continue;
                    }
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
            }

            try
            {
                List<StudenNotificationEVD> lstAuxAlumnos = new List<StudenNotificationEVD>();
                if (!String.IsNullOrEmpty(model.lstDatosInvitados))
                {
                    var firstEncode = model.lstDatosInvitados.Split(';');
                    for (int i = 0, j = 0; i < firstEncode.Count(); i++)
                    {
                        var data = firstEncode[i].Split('|');
                        lstAuxAlumnos.Add(new StudenNotificationEVD
                        {
                            IdAlumno = ConvertHelpers.ToInteger(data[j]),
                            IdCarrera = ConvertHelpers.ToInteger(data[j + 1]),
                            Correo = data[j + 2],
                            IdNotificacion = ConvertHelpers.ToInteger(data[j + 3])
                        });
                    }
                }

                foreach (StudenNotificationEVD item in lstAuxAlumnos)
                {

                    int parModalidadId = Session.GetModalidadId();
                    var EVD = Context.EncuestaVirtualDelegado.FirstOrDefault(x => x.Estado == "ACT" && x.IdEscuela == IdEscuela);
                    var PeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico;

                    EncuestaToken objValidar = (from e in Context.EncuestaToken
                                                where e.IdAlumno == item.IdAlumno &&
                                                      e.IdCarrera == item.IdCarrera &&
                                                      e.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico &&
                                                      e.Tipo == "EVD" && e.IdEncuestaVirtualDelegado == EVD.IdEncuestaVirtualDelegado
                                                select e).FirstOrDefault();

                    String token = String.Format("{0}|{1}|{2}|{3}", item.IdAlumno, item.IdCarrera, PeriodoAcademico.IdPeriodoAcademico, item.IdNotificacion);

                    String md5Token = SecurityLogic.Encrypt(token);
                    md5Token = md5Token.Replace("+", "L");

                    String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                    String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                        Request.Url.Scheme,
                                                        Request.Url.Authority,
                                                        vdirectory,
                                                        "/?Token=" + md5Token);

                    String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);

                    bool enviado;
                    String CodigoEncuesta = Context.Usp_GetCodigoNomenclatura(EVD.IdEncuestaVirtualDelegado).FirstOrDefault().ToString();
                    if (objValidar != null)
                    {
                        if (objValidar.Estado)
                        {
                            enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), item.Correo);
                            if (enviado)
                            {
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
                            }
                            else
                            {
                                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                                return RedirectToAction("EmailSurveyEVD");
                            }
                            continue;
                        }
                        continue;
                    }

                    enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto + " " + CodigoEncuesta, mailLogic.GetHtmlEmailEVD(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPCEVD.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg", CodigoEncuesta), item.Correo);


                    if (enviado)
                    {
                        EncuestaToken oEncuestaToken = new EncuestaToken();
                        oEncuestaToken.Token = md5Token;
                        oEncuestaToken.Estado = true;
                        oEncuestaToken.FechaEnvio = DateTime.Today;
                        oEncuestaToken.FechaFin = objConfiguracion.FechaFinLimite;
                        oEncuestaToken.IdAlumno = item.IdAlumno;
                        oEncuestaToken.IdCarrera = item.IdCarrera;
                        oEncuestaToken.IdSubModalidadPeriodoAcademico = EVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico;
                        oEncuestaToken.Tipo = "EVD";
                        oEncuestaToken.IdEncuestaVirtualDelegado = EVD.IdEncuestaVirtualDelegado;
                        Context.EncuestaToken.Add(oEncuestaToken);
                        Context.SaveChanges();

                        NotificacionEncuestaAlumno objNotificacion = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == item.IdNotificacion);
                        objNotificacion.IdEncuestaToken = oEncuestaToken.IdEncuestaToken;

                        Context.SaveChanges();

                    }
                    else
                    {
                        PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                        return RedirectToAction("EmailSurveyEVD");
                    }
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "No se pudo realizar el envío." : "Could not send.");
            }
            return RedirectToAction("EmailSurveyEVD");
        }


        /*REENVIO DE MENSAJE DESDE MANTENIMIENTO*/
        [HttpPost]
        public JsonResult EmailNSurveyGRAReenviar(DetalleEncuestaVirtualGRAViewModel model)
        {
            String TituloCorreo = model.TituloCorreo;
            String CuerpoCorreo = model.CuerpoCorreo;
            DateTime fechaFin = model.NuevaFechaFin;
            String mensaje = "";
            EncuestaToken objEncuestaToken = Context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == model.objEncuestaToken.IdEncuestaToken);

            if (objEncuestaToken == null)
            {
                // PostMessage(MessageType.Error, "No se pudo realizar el envío.");
                String texto = currentCulture.Equals("es-PE") == true ? "No se pudo realizar el envío." : "Could not send.";
                mensaje = "danger+" + texto;
            }
            else
            {
                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                    Request.Url.Scheme,
                                                    Request.Url.Authority,
                                                    vdirectory,
                                                    "/?Token=" + objEncuestaToken.Token);

                EmailGRALogic mailLogic = new EmailGRALogic();
                Alumno objAlumno = Context.Alumno.Where(x => x.IdAlumno == objEncuestaToken.IdAlumno).FirstOrDefault();

                String urlImage = String.Format("{0}://{1}{2}",
                                                    Request.Url.Scheme,
                                                    Request.Url.Authority, vdirectory);
                bool enviado = mailLogic.SendMail(TituloCorreo, mailLogic.GetHtmlEmail(CuerpoCorreo, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), GetCorreo(objAlumno.Codigo));

                if (enviado)
                {
                    objEncuestaToken.FechaFin = fechaFin;
                    objEncuestaToken.FechaEnvio = DateTime.Today;

                    if (objEncuestaToken.IdEncuesta != null)
                    {
                        //tengo eliminar el performance de la encuesta y la encuesta
                        var puntacion = Context.PerformanceEncuesta.Where(x => x.IdEncuesta == objEncuestaToken.IdEncuesta);
                        if (puntacion != null)
                        {
                            Context.PerformanceEncuesta.RemoveRange(puntacion);
                            Context.SaveChanges();
                        }

                        var encuesta = Context.Encuesta.Where(x => x.IdEncuesta == objEncuestaToken.IdEncuesta).FirstOrDefault();
                        if (encuesta != null)
                        {

                            Context.Encuesta.Remove(encuesta);
                            Context.SaveChanges();
                        }
                        objEncuestaToken.IdEncuesta = null;
                        //tengo que eliminar los hallazgos sea el caso, preguntar 
                    }

                    objEncuestaToken.Estado = true;
                    Context.SaveChanges();
                    //PostMessage(MessageType.Success, "El envío se realizó con éxito");
                    String texto = (currentCulture.Equals("es-PE")) ? "La encuesta se envió con éxito." : "The survey was sent successfully.";
                    mensaje = "success+" + texto;
                }
                else
                {
                    //PostMessage(MessageType.Error, "Ocurrió un error al momento de reenviar el correo.");
                    String texto = (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.";
                    mensaje = "danger+" + texto;
                }
            }
            return Json(mensaje);

        }



        public ActionResult _ListStudentNotificationsGRA(Int32? IdCarrera)
        {
            var model = new ListStudentNotificationsGRAViewModel();
            model.Fill(CargarDatosContext(), IdCarrera, @Session.GetModalidadId());
            return PartialView("_ListStudentNotificationsGRA", model);
        }

        public ActionResult _ListStudentNotificationsEVD()
        {
            var model = new ListStudentNotificationsEVDViewModel();
            model.Fill(CargarDatosContext());
            return PartialView("_ListStudentNotificationsEVD", model);
        }

        public ActionResult _ListStudentNotificationsEVDInvitado()
        {
            var model = new ListStudentNotificacionsEVDInvitadosViewModel();
            model.Fill(CargarDatosContext());
            return PartialView("_ListStudentNotificationsEVDInvitado", model);
        }

        public ActionResult _SendEmailSurveyGRA(Int32 IdNotificacion)
        {
            var model = new SendEmailSurveyGRAViewModel();
            model.Fill(CargarDatosContext(), IdNotificacion);
            return View(model);
        }

        /*REENVIO DE MENSAJE DESDE NOTIFICACION*/
        [HttpPost]
        public JsonResult _SendEmailSurveyGRA(SendEmailSurveyGRAViewModel model)
        {
            NotificacionEncuestaAlumno objNotificacion = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == model.IdNotificacion);
            EncuestaToken objEncuestaToken = Context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == objNotificacion.IdEncuestaToken);

            int IdEscuela = Session.GetEscuelaId();
            ConfiguracionNotificacion objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo=="GRA");

            if (objConfiguracion == null)
            {
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "Aún no se ha configurado el asunto, mensaje y fecha fin de las notificaciones" : "The subject, message, and deadline of notifications have not yet been configured");
            }
            else if (objEncuestaToken.IdEncuesta != null)
            {
                PostMessage(MessageType.Info, (currentCulture.Equals("es-PE")) ? "El alumno ya registró su encuesta virtual." : "The student already registered a virtual survey.");
            }
            else if (objConfiguracion.FechaFinLimite.AddDays(1) < DateTime.Now)
            {
                PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "La fecha máxima de la notificación no excede a la fecha actual." : "The notification deadline does not exceed the current date.");
            }
            else
            {
                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                String urlSurvey = String.Format("{0}://{1}{2}{3}",
                                                    Request.Url.Scheme,
                                                    Request.Url.Authority,
                                                    vdirectory,
                                                    "/?Token=" + objEncuestaToken.Token);

                EmailGRALogic mailLogic = new EmailGRALogic();
                Alumno objAlumno = Context.Alumno.Where(x => x.IdAlumno == objEncuestaToken.IdAlumno).FirstOrDefault();

                String urlImage = String.Format("{0}://{1}{2}",
                                                    Request.Url.Scheme,
                                                    Request.Url.Authority, vdirectory);

                bool enviado = mailLogic.SendMail(objConfiguracion.NombreAsunto, mailLogic.GetHtmlEmail(objConfiguracion.NombreContenido, urlSurvey, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), GetCorreo(objAlumno.Codigo));
                if (enviado)
                {
                    objEncuestaToken.FechaFin = objConfiguracion.FechaFinLimite;
                    objEncuestaToken.FechaEnvio = DateTime.Today;

                    if (objEncuestaToken.IdEncuesta != null)
                    {
                        var performance = Context.PerformanceEncuesta.Where(x => x.IdEncuesta == objEncuestaToken.IdEncuesta);
                        if (performance != null)
                        {
                            Context.PerformanceEncuesta.RemoveRange(performance);
                            Context.SaveChanges();
                        }

                        var encuesta = Context.Encuesta.Where(x => x.IdEncuesta == objEncuestaToken.IdEncuesta).FirstOrDefault();
                        if (encuesta != null)
                        {
                            Context.Encuesta.Remove(encuesta);
                            Context.SaveChanges();
                        }
                        objEncuestaToken.IdEncuesta = null;
                    }

                    objEncuestaToken.Estado = true;
                    Context.SaveChanges();
                    PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "El correo se envió con éxito." : "The mail was sent successfully.");
                }
                else
                {
                    PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "El Servidor de correo está inactivo." : "The mail server is inactive.");
                }
            }

            return Json("");
        }

        public ActionResult _DeleteNotificationSurveyGRA(Int32 IdNotificacion)
        {
            var model = new DeleteNotificationSurveyGRAViewModel();
            model.Fill(CargarDatosContext(), IdNotificacion);
            return View(model);
        }

        public ActionResult _GetLinkGra(Int32 IdAlumno)
        {
            
            var model = new GetLinkGRAViewModel();
            model.escuelaId = Session.GetEscuelaId();
            model.Fill(CargarDatosContext(), IdAlumno);
            return View(model);
        }

        [HttpPost]
        public JsonResult _DeleteNotificationSurveyGRA(DeleteNotificationSurveyGRAViewModel model)
        {
            try
            {
                var objNotificacion = Context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == model.IdNotificacion);
                var objEncuestaToken = Context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == model.IdEncuestaToken);

                Context.EncuestaToken.Remove(objEncuestaToken);
                Context.SaveChanges();
                Context.NotificacionEncuestaAlumno.Remove(objNotificacion);
                Context.SaveChanges();

                if (objEncuestaToken.IdEncuesta != null)
                {
                    var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == objEncuestaToken.IdEncuesta);
                    try
                    {
                        encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                        var tipo = Context.TipoEncuesta.FirstOrDefault(x => x.IdTipoEncuesta == encuesta.IdTipoEncuesta);
                        Context.SaveChanges();
                        PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se eliminó correctamente." : "It was successfully deleted.");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "Ocurrió un error al eliminar." : "An error occurred while deleting.");
                throw ex;
            }

            return Json("");
        }

        [ValidateInput(false)]
        public ActionResult GuardarConfirmacionNotificacion(EmailSurveyGRAViewModel model)
        {
            try
            {
                ConfiguracionNotificacion objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdConfiguracionNotificacion == model.objConfiguracionNotificacion.IdConfiguracionNotificacion);

                if (objConfiguracion == null)
                {
                    model.objConfiguracionNotificacion.IdEscuela = Session.GetEscuelaId();
                    Context.ConfiguracionNotificacion.Add(model.objConfiguracionNotificacion);
                }
                else
                {
                    objConfiguracion.NombreAsunto = model.objConfiguracionNotificacion.NombreAsunto;
                    objConfiguracion.NombreContenido = model.objConfiguracionNotificacion.NombreContenido;
                    objConfiguracion.FechaFinLimite = model.objConfiguracionNotificacion.FechaFinLimite;
                    Context.Entry(objConfiguracion).State = System.Data.Entity.EntityState.Modified;
                }
                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se guardó la configuración de notificaciones correctamente" : "Notification settings saved successfully");

                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "No se logró guardar la configuración de las notificaciones" : "Could not save the notification setting");
            }

            return RedirectToAction("EmailSurveyGRA", "Email");
        }

        [ValidateInput(false)]
        public ActionResult GuardarConfirmacionNotificacionEVD(EmailSurveyEVDViewModel model)
        {
            try
            {
                ConfiguracionNotificacion objConfiguracion;

                if (model.objConfiguracionNotificacionDelegado != null)
                {
                    objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdConfiguracionNotificacion == model.objConfiguracionNotificacionDelegado.IdConfiguracionNotificacion);

                    objConfiguracion.NombreAsunto = model.objConfiguracionNotificacionDelegado.NombreAsunto;
                    objConfiguracion.NombreContenido = model.objConfiguracionNotificacionDelegado.NombreContenido;
                    objConfiguracion.FechaFinLimite = model.objConfiguracionNotificacionDelegado.FechaFinLimite;
                    Context.Entry(objConfiguracion).State = System.Data.Entity.EntityState.Modified;

                }
                if (model.objConfiguracionNotificacionInvitado != null)
                {
                    objConfiguracion = Context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdConfiguracionNotificacion == model.objConfiguracionNotificacionInvitado.IdConfiguracionNotificacion);

                    objConfiguracion.NombreAsunto = model.objConfiguracionNotificacionInvitado.NombreAsunto;
                    objConfiguracion.NombreContenido = model.objConfiguracionNotificacionInvitado.NombreContenido;
                    objConfiguracion.FechaFinLimite = model.objConfiguracionNotificacionInvitado.FechaFinLimite;
                    Context.Entry(objConfiguracion).State = System.Data.Entity.EntityState.Modified;
                }



                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se guardó la configuración de notificaciones correctamente" : "Notification settings saved successfully");

                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "No se logró guardar la configuración de las notificaciones" : "Could not save the notification setting");
            }

            return RedirectToAction("EmailSurveyEVD", "Email");
        }


        private string GetCorreo(string codigo)
        {
            string correo = "";

            int anio = Convert.ToInt32(codigo.Substring(0, 4));

            switch (codigo)
            {
                case "199710627":
                    correo = "a199710627@upc.edu.pe";
                    break;
                default:
                    {
                        if (anio < 2000)
                        {

                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "a" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2000 && anio < 2010)
                        {
                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "u" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2010)
                        {
                            correo = "u" + codigo + "@upc.edu.pe";
                        }
                    }
                    break;
            }
            return correo;
        }
    }
}