﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using System.Data.Entity;
using System.Data.Entity.Validation;
using UPC.CA.ABET.Presentation.Filters;
using System.Diagnostics;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Transactions;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Presentation.Helper;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.Areas.Professor.ViewModels.IfcManagement;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.Acreditador)]
    public class RegisterController : BaseController
    {
        // GET: Survey/Json
        public ActionResult Index()
        {
            return View();
        }

        //EVD


        #region Nuevo EVD

        public ActionResult EditHallazgoRD(int IdHallazgo)
        {
           
            EditHallazgoRDViewModel viewmodel = new EditHallazgoRDViewModel();

            try
            {
                viewmodel.CargarDato(CargarDatosContext(), IdHallazgo);
                return View(viewmodel);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.NoSePudoProcesarSuConsulta);
                return RedirectToAction("AddEditHallazgoEVD");
            }
        }

        [HttpPost]
        public ActionResult EditHallazgoRD(EditHallazgoRDViewModel ohallazgo)
        {
            try
            {
                var Hallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == ohallazgo.HallazgoId);

                Hallazgo.DescripcionEspanol = ohallazgo.DescripcionHallazgo;

                Context.SaveChanges();

                PostMessage(MessageType.Success, MessageResource.SeHaEditadoElHallazgo + " " + Hallazgo.Codigo+"-"+Hallazgo.Identificador+" "+MessageResource.Satisfactoriamente.ToLower()+".");
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, MessageResource.SucedioUnErrorAlActualizarElHallazgo);
            }            

            return RedirectToAction("AddEditHallazgoEVD");
        }



        public JsonResult ListarComentariosJSON(int? IdPeriodoAcademico, int? IdModulo, int? IdPregunta, int? IdDocente, int? IdCurso)
        {
            if (!IdPeriodoAcademico.HasValue) IdPeriodoAcademico = 0;
            if (!IdModulo.HasValue) IdModulo = 0;
            if (!IdPregunta.HasValue) IdPregunta = 0;
            if (!IdDocente.HasValue) IdDocente = 0;
            if (!IdCurso.HasValue) IdCurso = 0;


            var LstComentarioConHallazgo = (from a in Context.ComentarioHallazgo
                                            join b in Context.ComentarioDelegado on a.IdComentarioDelegado equals b.IdComentarioDelegado
                                            select b.IdComentarioDelegado).ToList();

            var LstComentario = (from a in Context.ComentarioDelegado
                                 join b in Context.EncuestaVirtualDelegado on a.IdEncuestaVirtualDelegado equals b.IdEncuestaVirtualDelegado
                                 join c in Context.SubModalidadPeriodoAcademicoModulo on b.IdSubModalidadPeriodoAcademicoModulo equals c.IdSubModalidadPeriodoAcademicoModulo
                                 join d in Context.Docente on a.IdDocente equals d.IdDocente
                                 join e in Context.Curso on a.IdCurso equals e.IdCurso
                                 join f in Context.SubModalidadPeriodoAcademico on c.IdSubModalidadPeriodoAcademico equals f.IdSubModalidadPeriodoAcademico
                                 join g in Context.Pregunta on a.IdPregunta equals g.IdPregunta
                                 join h in Context.TipoPregunta on g.IdTipoPregunta equals h.IdTipoPregunta
                                 where ((f.IdPeriodoAcademico == IdPeriodoAcademico || IdPeriodoAcademico == 0) &&
                                 (c.IdModulo == IdModulo || IdModulo == 0) &&
                                 (a.IdPregunta == IdPregunta || IdPregunta == 0) &&
                                 (a.IdDocente == IdDocente || IdDocente == 0) &&
                                 (a.IdCurso == IdCurso || IdCurso == 0)
                                 //&& (!LstComentarioConHallazgo.Contains(a.IdComentarioDelegado)) /*&& h.IdTipoPregunta == 1*/
                                 )
                                 select new
                                 {
                                     IdComentarioDelegado = a.IdComentarioDelegado,
                                     Docente = d.Codigo + " - " + d.Nombres + " " + d.Apellidos,
                                     Curso = e.Codigo + " - " + e.NombreEspanol,
                                     Comentario = a.DescripcionEspanol,
                                     TipoPregunta = g.IdTipoPregunta,
                                     NombreTipoPregunta = h.DescripcionEspanol,
                                     IdNivelSatisfaccion = a.IdNivelSatisfaccion
                                 }).ToList();

            List<ComentarioEVD> LstComentarioFinal = new List<ComentarioEVD>();


            foreach (var obj in LstComentario)
            {
                ComentarioEVD ocevd = new ComentarioEVD();

                if (obj.NombreTipoPregunta == "Pregunta con Comentario")
                {
                    ocevd.IdComentarioDelegado = obj.IdComentarioDelegado;
                    ocevd.Docente = obj.Docente;
                    ocevd.Curso = obj.Curso;
                    ocevd.Comentario = obj.Comentario;

                    LstComentarioFinal.Add(ocevd);
                }
                if (obj.NombreTipoPregunta == "Pregunta Escala")
                {
                    var nivel = Context.NivelSatisfaccion.Where(x => x.IdNivelSatisfaccion == obj.IdNivelSatisfaccion).FirstOrDefault();

                    if (nivel.GeneraHallazgo == true)
                    {
                        ocevd.IdComentarioDelegado = obj.IdComentarioDelegado;
                        ocevd.Docente = obj.Docente;
                        ocevd.Curso = obj.Curso;
                        var detallenivel = Context.NivelSatisfaccion.Where(x => x.IdNivelSatisfaccion == obj.IdNivelSatisfaccion).FirstOrDefault();
                        ocevd.Comentario = detallenivel.DescripcionEspanol;
                        LstComentarioFinal.Add(ocevd);
                    }

                }

            }

            return Json(LstComentarioFinal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditPlantillaEncuesta(Int32? IdPlantillaEncuesta)
        {
            var viewModel = new AddEditPlantillaEncuestaViewModel();
            viewModel.Fill(CargarDatosContext(), IdPlantillaEncuesta);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditPlantillaEncuesta(AddEditPlantillaEncuestaViewModel model)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    if (!ModelState.IsValid)
                    {
                        model.Fill(CargarDatosContext(), model.PlantillaEncuestaId);
                        TryUpdateModel(model);
                        PostMessage(MessageType.Error);
                        return View(model);
                    }

                    var plantillaencuesta = new PlantillaEncuesta();

                    if (model.PlantillaEncuestaId.HasValue)
                    {
                        plantillaencuesta = Context.PlantillaEncuesta.FirstOrDefault(x => x.IdPlantillaEncuesta == model.PlantillaEncuestaId);
                    }
                    else
                    {
                        Context.PlantillaEncuesta.Add(plantillaencuesta);
                    }

                    if (model.NombreEspanol == null)
                    {
                        model.NombreEspanol = " ";
                    }

                    plantillaencuesta.NombreEspanol = model.NombreEspanol;
                    plantillaencuesta.FechaCreacion = DateTime.Now;

                    Context.SaveChanges();
                    transaction.Complete();

                    PostMessage(MessageType.Success);
                    return RedirectToAction("ConfiguracionPlantillaEncuesta", "Configuration");
                }
            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error);
                model.Fill(CargarDatosContext(), model.PlantillaEncuestaId);
                TryUpdateModel(model);
                return View(model);
            }
        }

        public ActionResult AddEVR()
        {
            var pIdModalidad = Session.GetModalidadId();
            var IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == pIdModalidad && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var viewmodel = new AddEVRViewModel();
            viewmodel.CargarDatos(CargarDatosContext(), IdSubModalidadPeriodoAcademico, @Session.GetEscuelaId());
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddEVR(AddEVRViewModel model)
        {
            try
            {
                DateTime time = DateTime.Now;

                var modulo = Context.Modulo.Where(x => x.IdModulo == model.IdModulo).FirstOrDefault();

                EncuestaVirtualDelegado evd = new EncuestaVirtualDelegado();

                evd.IdSubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdModulo == modulo.IdModulo).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;
                evd.Codigo = "EVD_SISTEMAS_EPE_" + model.Ciclo + "_" + modulo.IdentificadorSeccion;
                evd.Estado = "INA";
                evd.FechaCreacion = time;
                evd.IdPlantillaEncuesta = model.IdPlantillaEncuesta;
                evd.IdCarrera = model.IdCarrera;
                evd.IdEscuela = model.IdEscuela;

                Context.EncuestaVirtualDelegado.Add(evd);

                Context.SaveChanges();
                PostMessage(MessageType.Success);

                return RedirectToAction("ConfiguracionEVD", "Configuration");
            }
            catch (Exception e)
            {
                return RedirectToAction("AddEVR", "Register");
            }
        }


        public ActionResult DelegatedVirtualSurveyConfiguration(int IdEncuestaVirtualDelegado)
        {
            var model = new DelegatedVirtualSurveyConfigurationViewModel();
            model.CargarDatos(CargarDatosContext(), 1, 1, IdEncuestaVirtualDelegado);
            return View(model);

        }

        [HttpGet]
        public ActionResult AddQuestion(int IdPlantillaEncuesta)
        {
            AddQuestionViewModel model = new AddQuestionViewModel();

            model.IdPlantillaEncuesta = IdPlantillaEncuesta;
            return PartialView("AddQuestion", model);
        }
        [HttpPost]
        public ActionResult AddQuestion(AddQuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    if (model.IdTipoPregunta == 1)
                    {
                        Pregunta preg = new Pregunta();
                        preg.IdPlantillaEncuesta = model.IdPlantillaEncuesta;
                        preg.IdTipoPregunta = model.IdTipoPregunta;
                        preg.DescripcionEspanol = model.DescripcionEspanol;
                        if (model.DescripcionIngles == null)
                        {
                            YandexTranslatorAPIHelper helper = new YandexTranslatorAPIHelper();
                            preg.DescripcionIngles = helper.Translate(model.DescripcionEspanol);
                        }
                        else
                        {
                            preg.DescripcionIngles = model.DescripcionIngles;
                        }
                        Context.Pregunta.Add(preg);
                        Context.SaveChanges();

                        int orden = Context.Pregunta.Where(x => x.IdPlantillaEncuesta == model.IdPlantillaEncuesta).Count();
                        preg.Orden = orden;

                        preg.IdTipoPregunta = model.IdTipoPregunta;
                    }
                    if (model.IdTipoPregunta == 2)
                    {
                        Pregunta preg = Context.Pregunta.Where(x => x.DescripcionEspanol == "PREGUNTA_CREADA_AUTOMATICAMENTE_AL_ELEGIR_ESCALA").FirstOrDefault();
                        preg.DescripcionEspanol = model.DescripcionEspanol;
                        preg.DescripcionIngles = model.DescripcionIngles;
                        preg.IdPlantillaEncuesta = model.IdPlantillaEncuesta;
                        preg.IdTipoPregunta = model.IdTipoPregunta;
                        preg.CantidadNiveles = Context.DetalleNiveles.Where(x => x.IdPregunta == preg.IdPregunta).OrderByDescending(x => x.Orden).First().Orden;
                        Context.SaveChanges();
                        int orden = Context.Pregunta.Where(x => x.IdPlantillaEncuesta == model.IdPlantillaEncuesta).Count();
                        preg.Orden = orden;


                    }
                    Context.SaveChanges();
                    return RedirectToAction("ConfiguracionPreguntasPlantillaEncuesta", "Configuration", new { IdPlantillaEncuesta = model.IdPlantillaEncuesta });
                }
                else
                {
                    TempData["ErrorQuestion"] = MessageResource.PorFavorLlenarTodosLosCampos;
                    return RedirectToAction("ConfiguracionPreguntasPlantillaEncuesta", "Configuration", new { IdPlantillaEncuesta = model.IdPlantillaEncuesta });
                }
            }
            catch
            {
                return RedirectToAction("AddQuestion", "Register");
            }
        }


        public ActionResult ConfigQuestionLevel(int IdPregunta)
        {
            var viewmodel = new ConfigQuestionLevelViewModel();

            Pregunta pregunta = new Pregunta();
            pregunta.IdPlantillaEncuesta = Context.PlantillaEncuesta.FirstOrDefault().IdPlantillaEncuesta;
            pregunta.IdTipoPregunta = Context.TipoPregunta.FirstOrDefault(x => x.DescripcionEspanol == "Pregunta Escala").IdTipoPregunta;
            pregunta.DescripcionEspanol = "PREGUNTA_CREADA_AUTOMATICAMENTE_AL_ELEGIR_ESCALA";
            pregunta.DescripcionIngles = " ";
            Context.Pregunta.Add(pregunta);
            Context.SaveChanges();

            viewmodel.IdPregunta = pregunta.IdPregunta;
            viewmodel.CargarDatos(CargarDatosContext());
            return PartialView(viewmodel);
        }

        [HttpPost]
        public ActionResult ConfigQuestionLevel(ConfigQuestionLevelViewModel model)
        {
            try
            {
                var pr = Context.Pregunta.Where(x => x.DescripcionEspanol == "PREGUNTA_CREADA_AUTOMATICAMENTE_AL_ELEGIR_ESCALA").FirstOrDefault();
                model.IdPregunta = pr.IdPregunta;
                int? maxNivel = model.maxNivel;

                if (Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta).FirstOrDefault() != null)
                {
                    DetalleNiveles detalle1 = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta && x.Orden == 1).FirstOrDefault();
                    DetalleNiveles detalle2 = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta && x.Orden == 2).FirstOrDefault();
                    DetalleNiveles detalle3 = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta && x.Orden == 3).FirstOrDefault();
                    DetalleNiveles detalle4 = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta && x.Orden == 4).FirstOrDefault();
                    DetalleNiveles detalle5 = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta && x.Orden == 5).FirstOrDefault();

                    if (maxNivel >= 3 && detalle3 != null)
                    {
                        detalle3.IdNivelSatisfaccion = model.Nivel3;
                    }
                    else if (maxNivel >= 3 && detalle3 == null)
                    {
                        DetalleNiveles newDetalle = new DetalleNiveles();
                        newDetalle.IdPregunta = model.IdPregunta;
                        newDetalle.IdNivelSatisfaccion = model.Nivel3;
                        newDetalle.Orden = 3;
                        Context.DetalleNiveles.Add(newDetalle);

                    }

                    if (maxNivel >= 4 && detalle4 != null)
                    {
                        detalle4.IdNivelSatisfaccion = model.Nivel4;
                    }
                    else if (maxNivel >= 4 && detalle4 == null)
                    {
                        DetalleNiveles newDetalle = new DetalleNiveles();
                        newDetalle.IdPregunta = model.IdPregunta;
                        newDetalle.IdNivelSatisfaccion = model.Nivel4;
                        newDetalle.Orden = 4;
                        Context.DetalleNiveles.Add(newDetalle);
                    }

                    if (maxNivel >= 5 && detalle5 != null)
                    {
                        detalle5.IdNivelSatisfaccion = model.Nivel5;
                    }
                    else if (maxNivel >= 5 && detalle5 == null)
                    {
                        DetalleNiveles newDetalle = new DetalleNiveles();
                        newDetalle.IdPregunta = model.IdPregunta;
                        newDetalle.IdNivelSatisfaccion = model.Nivel5;
                        newDetalle.Orden = 5;
                        Context.DetalleNiveles.Add(newDetalle);
                    }
                    int? cantidadNivelesGuardado = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta).OrderByDescending(x => x.Orden).First().Orden;

                    //Borrar Niveles Restantes
                    if (maxNivel < cantidadNivelesGuardado)
                    {
                        if (maxNivel != cantidadNivelesGuardado && detalle5 != null)
                        {
                            Context.DetalleNiveles.Remove(detalle5);
                            cantidadNivelesGuardado--;
                        }
                        if (maxNivel != cantidadNivelesGuardado && detalle4 != null)
                        {
                            Context.DetalleNiveles.Remove(detalle4);
                            cantidadNivelesGuardado--;
                        }
                        if (maxNivel != cantidadNivelesGuardado)
                        {
                            Context.DetalleNiveles.Remove(detalle3);
                        }
                    }

                    detalle1.IdNivelSatisfaccion = model.Nivel1;
                    detalle2.IdNivelSatisfaccion = model.Nivel2;

                    Context.SaveChanges();
                    return RedirectToAction("EditQuestion", "Register");
                }
                else
                {
                    DetalleNiveles detalle1 = new DetalleNiveles();
                    detalle1.IdPregunta = model.IdPregunta;
                    detalle1.IdNivelSatisfaccion = model.Nivel1;
                    detalle1.Orden = 1;
                    Context.DetalleNiveles.Add(detalle1);

                    DetalleNiveles detalle2 = new DetalleNiveles();
                    detalle2.IdPregunta = model.IdPregunta;
                    detalle2.IdNivelSatisfaccion = model.Nivel2;
                    detalle2.Orden = 2;
                    Context.DetalleNiveles.Add(detalle2);

                    if (maxNivel >= 3)
                    {
                        DetalleNiveles detalle3 = new DetalleNiveles();
                        detalle3.IdPregunta = model.IdPregunta;
                        detalle3.IdNivelSatisfaccion = model.Nivel3;
                        detalle3.Orden = 3;
                        Context.DetalleNiveles.Add(detalle3);

                        if (maxNivel >= 4)
                        {
                            DetalleNiveles detalle4 = new DetalleNiveles();
                            detalle4.IdPregunta = model.IdPregunta;
                            detalle4.IdNivelSatisfaccion = model.Nivel4;
                            detalle4.Orden = 4;
                            Context.DetalleNiveles.Add(detalle4);

                            if (maxNivel >= 5)
                            {
                                DetalleNiveles detalle5 = new DetalleNiveles();
                                detalle5.IdPregunta = model.IdPregunta;
                                detalle5.IdNivelSatisfaccion = model.Nivel5;
                                detalle5.Orden = 5;
                                Context.DetalleNiveles.Add(detalle5);

                            }
                        }
                    }
                    Context.SaveChanges();
                    return RedirectToAction("AddQuestion", "Register");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("ConfigQuestionLevel", "Register");
            }
        }

        public ActionResult EditQuestion(int IdPregunta)
        {
            EditQuestionViewModel viewmodel = new EditQuestionViewModel();
            viewmodel.IdPregunta = IdPregunta;
            viewmodel.CargarDatos(CargarDatosContext());
            return PartialView(viewmodel);
        }
        [HttpPost]
        public ActionResult EditQuestion(EditQuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Pregunta pregunta = Context.Pregunta.Where(x => x.IdPregunta == model.IdPregunta).FirstOrDefault();
                    pregunta.DescripcionEspanol = model.DescripcionEspanol;
                    pregunta.DescripcionIngles = model.DescripcionIngles;
                    if ((pregunta.IdTipoPregunta == 1 && model.IdTipoPregunta == 2) || model.IdTipoPregunta == 2)
                    {
                        pregunta.CantidadNiveles = Context.DetalleNiveles.Where(x => x.IdPregunta == model.IdPregunta).OrderByDescending(x => x.Orden).First().Orden;
                    }
                    else if (pregunta.IdTipoPregunta == 2 && model.IdTipoPregunta == 1)
                    {
                        pregunta.CantidadNiveles = null;
                    }
                    pregunta.IdTipoPregunta = model.IdTipoPregunta;
                    Context.SaveChanges();
                    return RedirectToAction("ConfiguracionPreguntasPlantillaEncuesta", "Configuration", new { IdPlantillaEncuesta = model.IdPlantillaEncuesta });
                }
                else
                {
                    TempData["ErrorQuestion"] = MessageResource.PorFavorLlenarTodosLosCampos;
                    return RedirectToAction("ConfiguracionPreguntasPlantillaEncuesta", "Configuration", new { IdPlantillaEncuesta = model.IdPlantillaEncuesta });
                }
            }
            catch
            {
                return RedirectToAction("DelegatedVirtualSurveyConfiguration", "Register");
            }

        }

        public ActionResult DeleteQuestion(int IdPregunta)
        {
            try
            {
                Pregunta preguntaBorrar = Context.Pregunta.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault();
                int IdPlantillaEncuesta = preguntaBorrar.IdPlantillaEncuesta;

                var _DetalleNiveles = Context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList();

                if (_DetalleNiveles.Count() > 0)
                {
                    Context.DetalleNiveles.RemoveRange(Context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta));

                }

                Context.Pregunta.Remove(preguntaBorrar);
                Context.SaveChanges();

                List<Pregunta> preguntas = Context.Pregunta.Where(x => x.IdPlantillaEncuesta == IdPlantillaEncuesta).ToList();
                for (int i = 0; i < preguntas.Count; i++)
                {
                    int IdPreg = preguntas[i].IdPregunta;
                    Pregunta preguntaEditar = Context.Pregunta.Where(x => x.IdPregunta == IdPreg).FirstOrDefault();
                    preguntaEditar.Orden = i + 1;
                }
                Context.SaveChanges();

                return RedirectToAction("ConfiguracionPreguntasPlantillaEncuesta", "Configuration", new { IdPlantillaEncuesta = IdPlantillaEncuesta });

            }
            catch
            {
                return View();
            }

        }

        public ActionResult NivelesSatisfaccion()
        {
            NivelesSatisfaccionViewModel viewModel = new NivelesSatisfaccionViewModel();
            viewModel.CargarDatosContext(CargarDatosContext());
            return View(viewModel);
        }

        public ActionResult AddEditNivelS(Int32? IdNivelSatisfaccion)
        {
            AddEditNivelSViewModel viewModel = new AddEditNivelSViewModel();
            viewModel.IdNivelSatisfaccion = IdNivelSatisfaccion;
            viewModel.CargarDatosContext(CargarDatosContext());
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditNivelS(AddEditNivelSViewModel viewModel)
        {
            if (viewModel.IdNivelSatisfaccion.HasValue)
            {
                NivelSatisfaccion nivel = Context.NivelSatisfaccion.Where(x => x.IdNivelSatisfaccion == viewModel.IdNivelSatisfaccion).FirstOrDefault();
                nivel.DescripcionEspanol = viewModel.DescripcionEspanol;
                nivel.DescripcionIngles = viewModel.DescripcionIngles;
                nivel.GeneraHallazgo = viewModel.GenerarHallazgo;
            }
            else
            {
                NivelSatisfaccion nivel = new NivelSatisfaccion();
                nivel.DescripcionEspanol = viewModel.DescripcionEspanol;
                nivel.DescripcionIngles = viewModel.DescripcionIngles;
                nivel.GeneraHallazgo = viewModel.GenerarHallazgo;
                Context.NivelSatisfaccion.Add(nivel);
            }
            Context.SaveChanges();
            return RedirectToAction("NivelesSatisfaccion", "Register");
        }

        public ActionResult DeleteNivelS(Int32? IdNivelSatisfaccion)
        {
            NivelSatisfaccion nivel = Context.NivelSatisfaccion.Where(x => x.IdNivelSatisfaccion == IdNivelSatisfaccion).FirstOrDefault();
            Context.NivelSatisfaccion.Remove(nivel);
            Context.SaveChanges();
            return RedirectToAction("NivelesSatisfaccion", "Register");
        }


        public ActionResult AddEditHallazgoEVD(Int32? numeroPaginaHallazgo)
        {
            var viewmodel = new AddEditHallazgoEVDViewModel();

            int parModalidadId = Session.GetModalidadId();

            viewmodel.Fill(CargarDatosContext(), 0, 0, 0, parModalidadId, numeroPaginaHallazgo);

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult AddEditHallazgoEVD(AddEditHallazgoEVDViewModel model, string[] check)
        {
            try
            {
                List<int> listcheck = check.Select(int.Parse).ToList();


                var ListComentariosElegidos = (from a in Context.ComentarioDelegado
                                               where listcheck.Contains(a.IdComentarioDelegado)
                                               select a
                                               ).ToList();

                var ListCursosElegidos = (from a in ListComentariosElegidos
                                          join b in Context.Curso on a.IdCurso equals b.IdCurso
                                          select b).ToList().Distinct();


                foreach (Curso ocurso in ListCursosElegidos)
                {
                    var SMPAM = ListComentariosElegidos.Where(x => x.IdCurso == ocurso.IdCurso).FirstOrDefault().EncuestaVirtualDelegado.SubModalidadPeriodoAcademicoModulo;
            
                    Hallazgos onhallazgo = new Hallazgos();
              
                    onhallazgo.IdSubModalidadPeriodoAcademicoModulo = SMPAM.IdSubModalidadPeriodoAcademicoModulo;
                    onhallazgo.IdConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.Instrumento.Acronimo == "ARD").IdConstituyenteInstrumento;
                    onhallazgo.IdCurso = ocurso.IdCurso;
                    onhallazgo.Codigo = Convert.ToString(Context.Database.SqlQuery<string>("select[dbo].[FN_GetCodigoHallazgo](@p0, @p1, @p2,@p3)", onhallazgo.IdConstituyenteInstrumento, onhallazgo.IdCurso, 0,0).FirstOrDefault());
                    onhallazgo.DescripcionEspanol = model.DescripcionEspanol;
                    onhallazgo.DescripcionIngles = model.DescripcionIngles;      
                                               
                    FindLogic ofl = new FindLogic(CargarDatosContext(), onhallazgo.IdConstituyenteInstrumento, SMPAM.IdSubModalidadPeriodoAcademico);
                    onhallazgo.Identificador = ofl.ObtenerIndiceHallazgo();

                    onhallazgo.IdNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == "Necesita mejora").IdNivelAceptacionHallazgo;
                    onhallazgo.IdCriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == "Preocupante").IdCriticidad;
                    onhallazgo.FechaRegistro = DateTime.Now;
                    onhallazgo.Estado = "ACT";

                    Context.Hallazgos.Add(onhallazgo);
                    Context.SaveChanges();

                    foreach (var ocomentario in ListComentariosElegidos)
                    {
                        if (ocomentario.IdCurso == ocurso.IdCurso)
                        {
                            ComentarioHallazgo ocomenthallazgo = new ComentarioHallazgo();
                            ocomenthallazgo.IdComentarioDelegado = ocomentario.IdComentarioDelegado;
                            ocomenthallazgo.IdHallazgo = onhallazgo.IdHallazgo;
                            Context.ComentarioHallazgo.Add(ocomenthallazgo);
                            Context.SaveChanges();
                        }
                    }                   
                }

                PostMessage(MessageType.Success);

            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }

            return RedirectToAction("AddEditHallazgoEVD", "Register");
        }

        public ActionResult DeleteFinding(int IdHallazgo)
        {
            try
            {
                Hallazgos hallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                hallazgo.Estado = ConstantHelpers.ESTADO.INACTIVO;

                var challazgo = Context.ComentarioHallazgo.Where(x => x.IdHallazgo == IdHallazgo).ToList();

                foreach (var ochallazgo in challazgo)
                {
                    Context.ComentarioHallazgo.Remove(ochallazgo);
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);

            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("AddEditHallazgoEVD", "Register");
        }

        #endregion

        public ActionResult AddEditSurveyPPP(Int32? EncuestaId, Int32? CicloId, Int32? CarreraId)
        {
            //ActualizarSuperHallazgosPPP(9);
            var viewmodel = new AddEditSurveyPPPViewModel();

            viewmodel.Fill(CargarDatosContext(), EncuestaId, CicloId, CarreraId);
            return View(viewmodel);
        }
        public ActionResult _SelectPeriodo(String TipoEncuestaNombre, Int32? EncuestaId)
        {
            var viewmodel = new _SelectPeriodoViewModel();
            viewmodel.Fill(CargarDatosContext(), TipoEncuestaNombre, EncuestaId);
            return View(viewmodel);
        }
        [HttpPost]
        public JsonResult _SelectPeriodo(_SelectPeriodoViewModel model)
        {
            try
            {
                var ruta = Url.Action("AddEditSurvey" + model.TipoEncuestaNombre, new { EncuestaId = model.EncuestaId, CicloId = model.CicloId, CarreraId = model.CarreraId });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                throw;
            }
        }
        //AddEditPPP
        [HttpPost]
        public ActionResult AddEditSurveyPPP(AddEditSurveyPPPViewModel model, FormCollection frm)
        {
            try
            {
                if ((model.FechaInicio == model.FechaFin) || (model.FechaFin < model.FechaInicio))
                {
                    InvalidarContext();
                    TryUpdateModel(model);
                    model.Fill(CargarDatosContext(), model.EncuestaId, model.IdCiclo, model.IdCarrera);
                    PostMessage(MessageType.Error, MessageResource.VerifiqueLasFechasIngresadas);
                    return View(model);
                }

                Encuesta encuesta = null;
                Boolean edit = false;
                Int32 submodapa = 0;

                submodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdCiclo).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                if (model.EncuestaId.HasValue)
                {

                    encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.EncuestaId);
                    edit = true;
                }
                else
                {
                    var alumnoLlenandoEncuesta = Context.Encuesta.Where(x => x.CodigoAlumno == model.CodigoAlumno && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO);
                    if (alumnoLlenandoEncuesta == null)
                    {
                        model.NroPractica = 1;
                    } else
                    {
                        var numeroEncuestaMaximo = alumnoLlenandoEncuesta.Max(x => x.IdNumeroPractica);
                        switch (numeroEncuestaMaximo)
                        {
                            case 1:
                                if (model.NroPractica == 1)
                                {
                                    PostMessage(MessageType.Error, MessageResource.ElAlumnoYaCuentaConElPrimerInformeRegistrado);
                                    return RedirectToAction("SurveyManagementPPP", "Survey");
                                }
                                break;
                            case 2:
                                PostMessage(MessageType.Error, MessageResource.ElAlumnoYaCuentaConTodosSusInformesRegistrados);
                                return RedirectToAction("SurveyManagementPPP", "Survey");
                        }
                    }
                    encuesta = new Encuesta();
                    var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP);
                    encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                    encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;
                    Context.Encuesta.Add(encuesta);
                }

                encuesta.CodigoAlumno = model.CodigoAlumno;
                var alumno = Context.Alumno.FirstOrDefault(x => x.Codigo == model.CodigoAlumno);
                encuesta.IdAlumno = alumno.IdAlumno;
                encuesta.IdCarrera = model.IdCarrera;
                encuesta.FechaInicio = model.FechaInicio;
                encuesta.FechaFin = model.FechaFin;
                encuesta.TotalHoras = model.TotalHoras.ToInteger();
                encuesta.NumeroInforme = model.NroPractica;
                encuesta.IdNumeroPractica = model.NroPractica;
                encuesta.RazonSocial = model.RazonSocial;
                encuesta.NombreJefe = model.JefeDirecto;
                encuesta.CargoJefe = model.CargoJefe;
                encuesta.CodigoEncuesta = model.CodigoEncuesta.ToInteger();
                encuesta.TelefonoJefe = model.TelefonoJefeDirecto;
                encuesta.CorreoJefe = model.EmailJefeDirecto;
                encuesta.Comentario = model.Comentario;
                encuesta.RUC = model.RUC;
                encuesta.IdSubModalidadPeriodoAcademico = submodapa;
                encuesta.IdSede = model.IdSede;
                Context.SaveChanges();

                RegisterSurveyLogic addEditSurvey = new RegisterSurveyLogic();
                addEditSurvey.RegisterPerformancePPP(Context, encuesta, frm, edit);



                Context.SaveChanges();

                PostMessage(MessageType.Success);
                if (model.EncuestaId.HasValue)
                    return RedirectToAction("MantenimientoEncuestaPPP", "Maintenance");
                else
                    return RedirectToAction("SurveyManagementPPP", "Survey");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("EX: " + ex);
                InvalidarContext();
                TryUpdateModel(model);
                model.Fill(CargarDatosContext(), model.EncuestaId, model.IdCiclo, model.IdCarrera);
                PostMessage(MessageType.Error);
                return View(model);
            }
        }
        public bool ActualizarSuperHallazgosPPP(int IdPeriodoAcademico)
        {
            /*List<Encuesta> encuestas = context.Encuesta.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && 
                                                                x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && 
                                                                x.Alumno.Carrera.IdEscuela == escuelaId  &&
                                                                x.IdPeriodoAcademico == IdPeriodoAcademico
                                                                ).ToList();
            System.Diagnostics.Debug.WriteLine("entre a actualizarsuperhallazgosppp");*/
            String CicloAcademico = Context.PeriodoAcademico.FirstOrDefault(X => X.IdPeriodoAcademico == IdPeriodoAcademico).CicloAcademico;


            try
            {

                List<OutcomeEncuestaPPPConfig> gruposPPP = Context.OutcomeEncuestaPPPConfig.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();

                List<PerformanceEncuestaPPP> resultados = new List<PerformanceEncuestaPPP>();

                foreach (OutcomeEncuestaPPPConfig grupoPPP in gruposPPP)
                {
                    System.Diagnostics.Debug.WriteLine("-grupo PPP: " + grupoPPP.NombreEspanol);

                    resultados = Context.PerformanceEncuestaPPP.Where(x => x.IdOutcomeEncuestaPPPConfig == grupoPPP.IdOutcomeEncuestaPPPConfig && x.PuntajeOutcome != -1).ToList();

                    if (resultados.Count == 0) continue;

                    int CantidadTotal = resultados.Count;

                    int CantidadNecesitaMejora = resultados.Where(x => x.PuntajeOutcome <= 1).ToList().Count;

                    // System.Diagnostics.Debug.WriteLine("necesita/Total = " + CantidadNecesitaMejora + "/" + CantidadTotal+"= "+((float)CantidadNecesitaMejora / (float)CantidadTotal));



                    if ((float)CantidadNecesitaMejora / (float)CantidadTotal >= 0.23)
                    {
                        float puntajePromedio = 0;
                        int nivelAceptacion = 1;
                        foreach (PerformanceEncuestaPPP resultado in resultados)
                        {
                            puntajePromedio = puntajePromedio + (float)resultado.PuntajeOutcome.Value;
                        }

                        puntajePromedio = puntajePromedio / (float)resultados.Count;

                        if (puntajePromedio < 1.50)
                        {
                            nivelAceptacion = 1;
                        }
                        else
                           if (puntajePromedio < 2.20)
                        {
                            nivelAceptacion = 2;
                        }
                        else
                        {
                            nivelAceptacion = 3;
                        }



                        String codigo = "PPP-" + CicloAcademico + "-" + grupoPPP.Escuela.Codigo + "-S-" + grupoPPP.IdOutcomeEncuestaPPPConfig;

                        List<Outcome> outcomesHallazgos = Context.OutcomeEncuestaPPPOutcome.Where(x => x.IdOutcomeEncuestaPPPConfig == grupoPPP.IdOutcomeEncuestaPPPConfig).Select(x => x.Outcome).ToList();
                        foreach (Outcome outcome in outcomesHallazgos)
                        {
                            try
                            {
                                System.Diagnostics.Debug.WriteLine("Crear superhallazgo con codigo: " + codigo + "-" + outcome.IdOutcome);
                                System.Diagnostics.Debug.WriteLine("y el puntaje promedio del outcome es: " + puntajePromedio);
                                Hallazgo hallazgo = Context.Hallazgo.FirstOrDefault(x => x.Codigo == codigo + " - " + outcome.IdOutcome);

                                if (hallazgo == null)
                                {
                                    hallazgo = new Hallazgo();
                                }
                                hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;

                                hallazgo.IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "PPP").IdInstrumento;
                                hallazgo.IdNivelAceptacionHallazgo = nivelAceptacion;
                                hallazgo.IdOutcome = outcome.IdOutcome;
                                hallazgo.IdComision = Context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == outcome.IdOutcome && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).IdComision;
                                hallazgo.Generado = "AUT";
                                hallazgo.IdOutcomeEncuestaPPPConfig = grupoPPP.IdOutcomeEncuestaPPPConfig;
                                hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == "PPP").IdTipoEncuesta;

                                hallazgo.Codigo = codigo + " - " + outcome.IdOutcome;
                                hallazgo.DescripcionEspanol = "hallazgo automatico";
                                hallazgo.DescripcionIngles = "Automatic finding";
                                hallazgo.SubModalidadPeriodoAcademico.IdPeriodoAcademico = IdPeriodoAcademico;
                                hallazgo.IdCriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == "Critico").IdCriticidad;
                                hallazgo.IdCarrera = Context.Carrera.FirstOrDefault(x => x.IdEscuela == EscuelaId).IdCarrera;
                                hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                hallazgo.FechaRegistro = DateTime.Now;
                                if (hallazgo == null)
                                {
                                    Context.Hallazgo.Add(hallazgo);
                                }



                                Context.SaveChanges();

                            }
                            catch (DbEntityValidationException e)
                            {
                                System.Diagnostics.Debug.WriteLine("ERROR: " + e.EntityValidationErrors);
                            }
                            catch (Exception e)
                            {
                                System.Diagnostics.Debug.WriteLine("ERROR: " + e);
                            }
                        }

                    }
                    System.Diagnostics.Debug.WriteLine("---");
                }



                /*   List<GrupoPPP> gruposPPP = (from e in context.Encuesta
                                         join cc in context.CarreraComision on e.IdCarrera equals cc.IdCarrera
                                         join co in context.Comision on cc.IdComision equals co.IdComision
                                         join oc in context.OutcomeComision on co.IdComision equals oc.IdComision
                                         join o in context.Outcome on oc.IdOutcome equals o.IdOutcome
                                         where (e.IdTipoEncuesta == IdTipoEncuesta && e.IdPeriodoAcademico == IdPeriodoAcademico && cc.IdPeriodoAcademico == IdPeriodoAcademico && co.Codigo!="WASC")
                                         select new GrupoPPP {IdCarrera = e.IdCarrera.Value, IdComision = co.IdComision, IdPeriodoAcademico = cc.IdPeriodoAcademico, IdOutcome = o.IdOutcome}).Distinct().ToList();

                //   System.Diagnostics.Debug.WriteLine("cantidad: " + gruposPPP.Count);

                  foreach ( GrupoPPP item in gruposPPP)
                   {
                       System.Diagnostics.Debug.WriteLine("-carrera: "+item.IdCarrera+", comision: "+item.IdComision+", outcome: "+item.IdOutcome);
                   }

       */
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }


        }
        public ActionResult AddEditSurveyFDC(Int32? EncuestaId)
        {
            var viewmodel = new AddEditSurveyFDCViewModel();
            viewmodel.Fill(CargarDatosContext(), EncuestaId);
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult AddEditSurveyFDC(AddEditSurveyFDCViewModel model, List<Int32> lstPuntaje, List<Int32> lstIdEncuesta, List<String> lstComentarios)
        {
            try
            {
                Encuesta encuesta = null;
                List<EncuestaComentario> lstEncuestaComentario = null;
                List<ResultadoFDC> lstResultado = null;
                Decimal? puntosAcumulados = 0;
                var cantidadAlumnos = 0;
                var auxId = 0;
                if (model.EncuestaId.HasValue)
                {
                    encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.EncuestaId);
                    lstEncuestaComentario = encuesta.EncuestaComentario.ToList();
                    foreach (var item in lstEncuestaComentario)
                        Context.EncuestaComentario.Remove(item);
                    lstResultado = encuesta.ResultadoFDC.ToList();
                    auxId = encuesta.IdEncuesta;
                }
                else
                {
                    encuesta = Context.Encuesta.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico
                        && x.IdCurso == model.IdCurso && x.IdSeccion == model.IdSeccion
                        && x.IdSede == model.IdSede);

                    if (encuesta != null)
                    {
                        cantidadAlumnos = Context.ResultadoFDC.Where(x => x.IdEncuesta == encuesta.IdEncuesta).Sum(x => x.CantidadRespuesta);
                        puntosAcumulados = encuesta.PuntajeTotal.Value * cantidadAlumnos;
                    }
                    else
                    {
                        encuesta = new Encuesta();
                        var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC);
                        encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                        encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;
                        encuesta.PuntajeTotal = 0;
                        Context.Encuesta.Add(encuesta);
                    }
                }
                encuesta.IdResultadoObtenido = model.IdResultadoObtenido;
                //encuesta.IdCarrera = model.IdCarrera;
                encuesta.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico;
                encuesta.IdCurso = model.IdCurso;
                encuesta.IdSeccion = model.IdSeccion;
                encuesta.IdEstado = Context.EstadoEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.ESTADO_DEVUELTO).IdEstado;
                encuesta.IdSede = model.IdSede;
                Context.SaveChanges();

                if (lstComentarios != null)
                {
                    EncuestaComentario comentario = null;
                    for (int i = 0; i < lstComentarios.Count; i++)
                    {
                        comentario = new EncuestaComentario();
                        comentario.Comentario = lstComentarios[i];
                        if (lstIdEncuesta != null)
                        {
                            comentario.IdEncuesta = encuesta.IdEncuesta;
                            comentario.CodigoEncuesta = lstIdEncuesta[i];
                        }
                        else
                        {
                            comentario.IdEncuesta = encuesta.IdEncuesta;
                            comentario.CodigoEncuesta = encuesta.IdEncuesta;
                        }
                        Context.EncuestaComentario.Add(comentario);
                    }
                    Context.SaveChanges();
                }
                ResultadoFDC resultado = null;
                var puntos = 0; var cantidad = 0;

                for (int i = 0; i < lstPuntaje.Count; i++)
                {
                    puntos++;
                    if (model.EncuestaId.HasValue)
                    {
                        resultado = lstResultado[i];
                    }
                    else
                    {
                        resultado = new ResultadoFDC();
                        Context.ResultadoFDC.Add(resultado);
                    }

                    resultado.Puntaje = puntos;
                    resultado.CantidadRespuesta = lstPuntaje[i];
                    cantidad = cantidad + lstPuntaje[i];
                    encuesta.PuntajeTotal = encuesta.PuntajeTotal + puntos * lstPuntaje[i];
                    resultado.IdEncuesta = encuesta.IdEncuesta;

                }
                encuesta.PuntajeTotal = (encuesta.PuntajeTotal + puntosAcumulados) / (cantidad + cantidadAlumnos);
                encuesta.Encuestados = cantidad + cantidadAlumnos;
                Context.SaveChanges();

                AutogeneratedFindingsLogic hallazgologic = new AutogeneratedFindingsLogic();
                hallazgologic.updateAutogeneratedFindingsFDC(Context, encuesta, auxId);

                PostMessage(MessageType.Success);

                //if (model.EncuestaId.HasValue)
                return RedirectToAction("MantenimientoEncuestaFDC", "Maintenance");
                //else
                //    return RedirectToAction("AddEditSurveyFDC", "Register");
            }
            catch (Exception ex)
            {
                InvalidarContext();
                TryUpdateModel(model);
                PostMessage(MessageType.Error);
                model.Fill(CargarDatosContext(), model.EncuestaId);
                return View(model);
            }
        }
        public ActionResult ReporteHistoricoVirtualGRA(Int32? EncuestaId, Int32? CicloId, Int32? CarreraId)
        {
            return View();
        }
        public ActionResult AddEditSurveyVirtualGRA(Int32? EncuestaId, Int32? CicloId, Int32? CarreraId)
        {
            var viewmodel = new AddEditSurveyVirtualGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), EncuestaId, CicloId, CarreraId);
            return View(viewmodel);
        }
        public ActionResult AddEditSurveyGRA(Int32? EncuestaId, Int32? SubmodalidadperiodoacademicoId, Int32? CarreraId)
        {
            int parModalidadId = Session.GetModalidadId();

            var viewmodel = new AddEditSurveyGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), EncuestaId, SubmodalidadperiodoacademicoId, CarreraId, parModalidadId);
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult AddEditGRA(AddEditSurveyGRAViewModel model, FormCollection frm)
        {
            try
            {
                Encuesta encuesta = null;
                Boolean edit = false;
                if (model.EncuestaId.HasValue)
                {
                    encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.EncuestaId);
                    edit = true;
                }
                else
                {
                    encuesta = new Encuesta();
                    var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA);
                    encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                    encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;
                    Context.Encuesta.Add(encuesta);
                }


                encuesta.IdCarrera = model.IdCarrera;
                encuesta.Comentario = model.Comentario;
                encuesta.IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdCiclo).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                Context.SaveChanges();

                RegisterSurveyLogic addEditSurvey = new RegisterSurveyLogic();
                addEditSurvey.RegisterPerformanceGRA(Context, encuesta, frm, edit);


                Context.SaveChanges();
                PostMessage(MessageType.Success);

                if (model.EncuestaId.HasValue)
                    return RedirectToAction("MantenimientoEncuestaGRA", "Maintenance");
                else
                    return RedirectToAction("SurveyManagementGRA", "Survey");
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);

                if (model.EncuestaId.HasValue)
                    return RedirectToAction("MantenimientoEncuestaGRA", "Maintenance");
                else
                    return RedirectToAction("SurveyManagementGRA", "Survey");
            }
        }
        public ActionResult AddEditFindSurveyPPP(Int32 IdEncuesta, Int32? IdHallazgo)
        {
            int parModalidadId = Session.GetModalidadId();

            var viewModel = new AddEditFindSurveyPPPViewModel();
            viewModel.Fill(CargarDatosContext(), IdEncuesta, IdHallazgo, parModalidadId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AddEditFSurveyPPP(AddEditFindSurveyPPPViewModel model)
        {
            try
            {
                var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);

                var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Encuesta.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                Int32 idHallazgo = 1;
                if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                {
                    var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2));
                    idHallazgo = aux + 1;
                }

                if (model.IdHallazgo.HasValue)
                {
                    var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo);

                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdCurso = model.IdCurso;
                    hallazgo.IdComision = Context.CarreraComision.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico && x.IdCarrera == encuesta.IdCarrera).IdComision;
                    //hallazgo.IdComision = context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).IdComision;
                    hallazgo.IdCriterioHallazgoEncuesta = model.IdCriterio;
                    hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;


                    if (model.IdNivelAceptacion == 1)
                    {
                        hallazgo.IdCriticidad = 2;
                    }

                    else
                    //   if(model.IdNivelAceptacion ==2 || model.IdNivelAceptacion ==3)
                    {
                        hallazgo.IdCriticidad = 3;

                    }

                    hallazgo.IdCarrera = model.IdCarrera;
                    hallazgo.IdOutcomeEncuestaConfig = model.IdOutcome;
                    var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).NombreEspanol;
                    hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                }
                else
                {
                    //var cicloacademico = context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == encuesta.IdPeriodoAcademico).CicloAcademico;
                    //var codigosede = context.Sede.FirstOrDefault(x => x.IdSede == encuesta.IdSede).Codigo;
                    var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdInstrumento;
                    //var curso = context.Curso.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso).Codigo;
                    var hallazgo = new Hallazgo();

                    System.Diagnostics.Debug.WriteLine(ConstantHelpers.ENCUESTA.PPP + " " + encuesta.SubModalidadPeriodoAcademico.IdPeriodoAcademico + " " + idHallazgo.ToString("D3"));

                    hallazgo.Codigo = String.Format("{0}-{1}-F-{2}", ConstantHelpers.ENCUESTA.PPP, encuesta.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idHallazgo.ToString("D3"));
                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                    hallazgo.IdCurso = model.IdCurso;
                    hallazgo.IdCarrera = encuesta.IdCarrera;
                    hallazgo.Encuesta = encuesta;
                    hallazgo.IdComision = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).IdComision;
                    hallazgo.FechaRegistro = DateTime.Now;
                    hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    //hallazgo.IdSede = encuesta.IdSede;
                    hallazgo.IdCriterioHallazgoEncuesta = model.IdCriterio;
                    hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                    hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;



                    if (model.IdNivelAceptacion == 1)
                    {
                        hallazgo.IdCriticidad = 2;
                    }

                    else
                    //   if(model.IdNivelAceptacion ==2 || model.IdNivelAceptacion ==3)
                    {
                        hallazgo.IdCriticidad = 3;

                    }



                    hallazgo.IdOutcomeEncuestaConfig = model.IdOutcome;
                    hallazgo.IdInstrumento = IdInstrumento;
                    hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_EMPLEADOR).IdConstituyente;


                    int idpregunta = Context.OutcomeEncuestaPPPConfig.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == model.IdOutcome).IdOutcomeEncuestaPPPConfig;

                    hallazgo.IdOutcome = Context.OutcomeEncuestaPPPOutcome.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == idpregunta).IdOutcome;
                    //var nombreOutcome                     
                    // hallazgo.IdOutcome = context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                    hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;

                    Context.Hallazgo.Add(hallazgo);
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);
                return RedirectToAction("AddEditFindSurveyPPP", "Register", new { IdEncuesta = encuesta.IdEncuesta });
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ex: " + ex);
                PostMessage(MessageType.Error, MessageResource.CompruebeQueLaPreguntaSeleccionadaTieneOutcomesRelacionados);
                if (!String.IsNullOrEmpty(model.CodigoHallazgo))
                    return RedirectToAction("AddEditFindSurveyPPP", "Register", new { IdEncuesta = model.IdEncuesta, IdHallazgo = model.IdHallazgo });
                else
                    return RedirectToAction("AddEditFindSurveyPPP", "Register", new { IdEncuesta = model.IdEncuesta });
            }
        }
        public ActionResult AddEditFindHallazgoIRD(Int32? IdHallazgo)
        {
            var viewModel = new AddEditFindHallazgoIRDViewModel();
            viewModel.Fill(CargarDatosContext(), IdHallazgo);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AddEditFindHallazgoIRD(AddEditFindHallazgoIRDViewModel model)
        {


            try
            {
                //  var encuesta = context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);
                if (model.HallazgoId.HasValue)
                {
                    System.Diagnostics.Debug.WriteLine("modelOutcome: " + model.IdOutcome);
                    System.Diagnostics.Debug.WriteLine("modelPeriodoAcademico: " + model.IdPeriodoAcademico);
                    model.GenerarComisionAcreditadora(CargarDatosContext());
                    model.GenerarCarreraComision(CargarDatosContext());
                    var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);

                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdCurso = model.IdCurso;
                    hallazgo.IdComision = model.IdComision;
                    hallazgo.IdCarrera = model.IdCarrera;

                    hallazgo.IdAcreditadora = model.IdAcreditadora;
                    //hallazgo.IdComision = context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == model.IdOutcome && x.IdPeriodoAcademico == model.IdPeriodoAcademico).IdComision;
                    //   hallazgo.IdComision = 2;
                    //      hallazgo.IdAcreditadora = context.AcreditadoraPeriodoAcademico.FirstOrDefault(x => x.IdAcreditadoraPreiodoAcademico == model.IdPeriodoAcademico).IdAcreditadora;

                    hallazgo.IdSede = model.IdSede;
                    hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                    hallazgo.IdCriticidad = model.IdCriticidad.Value;
                    hallazgo.IdOutcome = model.IdOutcome;
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);
                //PostMessage(MessageType.Success); 
                return RedirectToAction("LstHallazgosIRD", new { idSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico, IdCurso = model.IdCurso, IdOutcome = model.IdOutcome, IdSede = model.IdSede, Search = true });
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ERROR: " + ex);
                return RedirectToAction("LstHallazgosIRD");
            }
        }
        public ActionResult AddEditFindSurveyFDC(Int32 IdEncuesta, String CodigoHallazgo, Int32? IdSudModalidadPeriodoAcademico)
        {
            var viewModel = new AddEditFindSurveyFDCViewModel();
            viewModel.Fill(CargarDatosContext(), IdEncuesta, CodigoHallazgo, IdSudModalidadPeriodoAcademico);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AddEditFSurveyFDC(AddEditFindSurveyFDCViewModel model)
        {
            try
            {
                var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);

                var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Encuesta.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC && x.IdCurso == encuesta.IdCurso).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                Int32 iHallazgo = 1;
                if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                {
                    var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2));
                    iHallazgo = aux + 1;
                }

                //var hallazgo = new Hallazgo();

                if (!String.IsNullOrEmpty(model.CodigoHallazgo))
                {
                    var Lsthallazgo = Context.Hallazgo.Where(x => x.Codigo == model.CodigoHallazgo).Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();
                    foreach (var item in Lsthallazgo)
                    {
                        item.DescripcionEspanol = model.DescripcionEspanol;
                        item.DescripcionIngles = model.DescripcionIngles;
                        item.IdCriterioHallazgoEncuesta = model.IdCriterio;
                        item.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;
                        item.IdCarrera = model.IdCarrera;
                    }
                }
                else
                {
                    var lstoutcomes = Context.uspGetOutcomesCurso(encuesta.IdCurso, encuesta.IdSubModalidadPeriodoAcademico).ToList();
                    if (lstoutcomes.Count > 0)
                    {
                        var lcfcInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "LCFC");
                        var IdInstrumento = lcfcInstrumento == null ? 1 : lcfcInstrumento.IdInstrumento;
                        var AcronimoInstrumento = lcfcInstrumento == null ? "" : lcfcInstrumento.Acronimo;
                        var cicloacademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == encuesta.SubModalidadPeriodoAcademico.IdPeriodoAcademico).CicloAcademico;
                        var codigosede = Context.Sede.FirstOrDefault(x => x.IdSede == encuesta.IdSede).Codigo;
                        var curso = Context.Curso.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso).Codigo;
                        List<MallaCurricular> Mallas = Context.MallaCurricularPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico).Select(x => x.MallaCurricular).ToList();
                        foreach (var outcomeComision in lstoutcomes)
                        {
                            foreach (var malla in Mallas)
                            {
                                var CursoMallaCurricular = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso && x.IdMallaCurricular == malla.IdMallaCurricular);
                                var IdComisionHallazgo = Context.CarreraComision.FirstOrDefault(x => x.IdCarrera == malla.IdCarrera && x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico && x.Comision.Codigo != ConstantHelpers.COMISON_WASC).IdComision;
                                var NombreOutcome = outcomeComision[0].ToString();
                                var IdOutcomeHallazgo = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == IdComisionHallazgo && x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico && x.Outcome.Nombre == NombreOutcome).IdOutcome;
                                var CodigoCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso).Codigo;
                                var CodigoSeccion = Context.Seccion.FirstOrDefault(x => x.IdSeccion == encuesta.IdSeccion).Codigo;
                                var hallazgo = new Hallazgo();

                                hallazgo.Codigo = String.Format("{0}-{1}-{2}-{3}-F-{4}", AcronimoInstrumento, CodigoCurso, cicloacademico, codigosede, iHallazgo.ToString("D3"));
                                hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                                hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                                hallazgo.IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                                hallazgo.IdCurso = encuesta.IdCurso;
                                hallazgo.Encuesta = encuesta;
                                hallazgo.IdComision = IdComisionHallazgo;
                                hallazgo.FechaRegistro = DateTime.Now;
                                hallazgo.IdSede = encuesta.IdSede;
                                hallazgo.IdCarrera = malla.IdCarrera;
                                hallazgo.IdCriterioHallazgoEncuesta = model.IdCriterio;
                                hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                                hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;
                                hallazgo.IdOutcome = IdOutcomeHallazgo;
                                hallazgo.IdInstrumento = IdInstrumento;
                                hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
                                hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;
                                Context.Hallazgo.Add(hallazgo);
                            }
                        }
                        iHallazgo++;
                    }
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);
                return RedirectToAction("AddEditFindSurveyFDC", "Register", new { IdEncuesta = encuesta.IdEncuesta, IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                if (!String.IsNullOrEmpty(model.CodigoHallazgo))
                    return RedirectToAction("AddEditFindSurveyFDC", "Register", new { IdEncuesta = model.IdEncuesta, CodigoHallazgo = model.CodigoHallazgo });
                else
                    return RedirectToAction("AddEditFindSurveyFDC", "Register", new { IdEncuesta = model.IdEncuesta });
            }
        }
        public ActionResult AddEditFindSurveyGRA(Int32 IdEncuesta, Int32? IdHallazgo, Int32? IdCarrera, Int32? CicloId, Int32? opcion_rastro)
        {
            int parModalidadId = Session.GetModalidadId();

            var viewModel = new AddEditFindSurveyGRAViewModel();
            viewModel.Fill(CargarDatosContext(), IdEncuesta, IdHallazgo, IdCarrera, CicloId, opcion_rastro, parModalidadId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AddEditFSurveyGRA(AddEditFindSurveyGRAViewModel model)
        {
            try
            {
                var encuesta = Context.Encuesta.FirstOrDefault(x => x.IdEncuesta == model.IdEncuesta);

                var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Encuesta.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                Int32 iHallazgo = 1;
                if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                {
                    var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2));
                    iHallazgo = aux + 1;
                }

                if (model.IdHallazgo.HasValue)
                {
                    var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo);

                    var lstHallazgo = Context.Hallazgo.Where(x => x.Codigo == hallazgo.Codigo).ToList();
                    foreach (var item in lstHallazgo)
                    {
                        item.DescripcionEspanol = model.DescripcionEspanol;
                        item.DescripcionIngles = model.DescripcionIngles;
                        item.IdCriterioHallazgoEncuesta = model.IdCriterio;
                        item.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;
                        item.IdCurso = model.IdCurso;
                        item.IdCarrera = model.IdCarrera;
                        item.IdOutcomeEncuestaConfig = model.IdOutcome;
                        item.IdComision = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).IdComision;
                        var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).NombreEspanol;
                        hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                    }
                }
                else
                {
                    var lstsede = Context.SedeCarrera.Where(x => x.IdCarrera == encuesta.IdCarrera).ToList();
                    var cicloacademico = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == encuesta.SubModalidadPeriodoAcademico.IdPeriodoAcademico).CicloAcademico;
                    //var codigosede = context.Sede.FirstOrDefault(x => x.IdSede == encuesta.IdSede).Codigo;
                    var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdInstrumento;

                    foreach (var sede in lstsede)
                    {
                        var hallazgo = new Hallazgo();
                        hallazgo.Codigo = String.Format("{0}-{1}-{2}-F-{3}", ConstantHelpers.ENCUESTA.GRA, encuesta.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, encuesta.Carrera.Codigo, iHallazgo.ToString("D3"));
                        hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                        hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                        hallazgo.IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                        hallazgo.IdCurso = model.IdCurso;
                        hallazgo.IdCarrera = model.IdCarrera;
                        hallazgo.Encuesta = encuesta;
                        hallazgo.IdSede = sede.IdSede;
                        hallazgo.IdComision = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).IdComision;
                        hallazgo.FechaRegistro = DateTime.Now;
                        hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                        hallazgo.IdCriterioHallazgoEncuesta = model.IdCriterio;
                        hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                        hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacion;
                        hallazgo.IdOutcomeEncuestaConfig = model.IdOutcome;
                        hallazgo.IdCriticidad = model.IdCriticidad;
                        hallazgo.IdInstrumento = IdInstrumento;
                        var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcome).NombreEspanol[0].ToString();
                        hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                        hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_GRADUADOS).IdConstituyente;
                        hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
                        Context.Hallazgo.Add(hallazgo);
                    }
                    iHallazgo++;
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);
                return RedirectToAction("AddEditFindSurveyGRA", "Register", new { IdEncuesta = encuesta.IdEncuesta });
            }
            catch
            {
                PostMessage(MessageType.Error);
                if (!String.IsNullOrEmpty(model.CodigoHallazgo))
                    return RedirectToAction("AddEditFindSurveyGRA", "Register", new { IdEncuesta = model.IdEncuesta, CodigoHallazgo = model.CodigoHallazgo });
                else
                    return RedirectToAction("AddEditFindSurveyGRA", "Register", new { IdEncuesta = model.IdEncuesta });
            }
        }
        public ActionResult LstHallazgosPPP(Int32? NumeroPagina, Int32? IdPeriodoAcademico, Int32? IdCarrera, Int32? IdComision, Int32? IdNumeroPractica, Int32? IdNivelAceptacionEncuesta)
        {
            int parModalidadId = Session.GetModalidadId();
            var viewModel = new LstHallazgosPPPViewModel();
            viewModel.CargarDatos(CargarDatosContext(), NumeroPagina, IdPeriodoAcademico, IdCarrera, IdComision, IdNumeroPractica, IdNivelAceptacionEncuesta, false, EscuelaId, parModalidadId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LstHallazgosPPP(LstHallazgosPPPViewModel model)
        {
            int parModalidadId = Session.GetModalidadId();
            model.CargarDatos(CargarDatosContext(), model.NumeroPagina, model.IdPeriodoAcademico, model.IdCarrera, model.IdComision, model.IdNumeroPractica, model.IdNivelAceptacionEncuesta, true, EscuelaId, parModalidadId);
            return View(model);
        }
        public ActionResult PrintPlanAccion(int CursoID)
        {
            var viewModel = new PrintPlanAccionViewModel();
            viewModel.CargarDatos(CargarDatosContext(), CursoID);

            Document document = new Document();

            MemoryStream stream = new MemoryStream();

            try
            {
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                pdfWriter.CloseStream = false;

                document.Open();


                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_RIGHT;


                // Insertamos la imagen en el documento
                document.Add(imagen);



                var fontNormal = FontFactory.GetFont("Arial", 16, Font.UNDERLINE, BaseColor.BLACK);
                Paragraph titulo = new Paragraph("Informe de Outcomes por Curso a evaluar", fontNormal);
                titulo.Alignment = Element.ALIGN_CENTER;

                document.AddTitle("Informe Outcomes - " + viewModel.CursoNombre);



                document.Add(titulo);


                document.Add(Chunk.NEWLINE);
                document.Add(Chunk.NEWLINE);


                var fontNormal2 = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);
                document.Add(new Paragraph("Ciclo: " + viewModel.PeriodoAcademicoCodigo, fontNormal2));
                document.Add(new Paragraph("Curso: " + viewModel.CursoNombre, fontNormal2));

                document.Add(Chunk.NEWLINE);

                var comisionFont = FontFactory.GetFont("Arial", 12, Font.ITALIC, BaseColor.BLACK);
                var descripcionFont = FontFactory.GetFont("Arial", 10, Font.ITALIC, BaseColor.DARK_GRAY);

                if (viewModel.resultado.Count == 0)
                {
                    document.Add(new Paragraph("Curso de Formación", comisionFont));

                }
                else
                {

                    document.Add(new Paragraph("Outcomes:"));
                    document.Add(Chunk.NEWLINE);



                    foreach (var item in viewModel.resultado)
                    {
                        Chunk aux0 = new Chunk(item.ComisionNombre, comisionFont);
                        Chunk aux1 = new Chunk(" | " + item.OutcomeNombre);
                        Paragraph aux2 = new Paragraph(item.OutcomeDescripcion, descripcionFont);

                        document.Add(aux0);
                        document.Add(aux1);
                        document.Add(aux2);


                        document.Add(Chunk.NEWLINE);
                        //document.Add(Chunk.NEWLINE);

                        PdfPTable table;
                        PdfPCell cell;

                        table = new PdfPTable(1);
                        cell = new PdfPCell(new Phrase("\n\n\n\n\n\n"));
                        cell.BorderWidth = 2;

                        //cell.Padding = 5;
                        // cell.PaddingTop = 3;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table.AddCell(cell);
                        table.SetWidthPercentage(new float[1] { 598f }, PageSize.LETTER);
                        table.HorizontalAlignment = Element.ALIGN_CENTER;
                        document.Add(table);


                        document.Add(Chunk.NEWLINE);
                        document.Add(Chunk.NEWLINE);

                    }
                }



            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                Console.Error.WriteLine(ioe.Message);
            }

            document.Close();

            stream.Flush(); //Always catches me out
            stream.Position = 0; //Not sure if this is required

            return File(stream, "application/pdf", "InformeOutcomes" + viewModel.CursoNombre + ".pdf");

        }
        public ActionResult LstHallazgosIRD(Int32? NumeroPagina, Int32? IdPeriodoAcademico, Int32? IdCurso, Int32? IdOutcome, Int32? IdNivelAceptacionEncuesta, Int32? IdSede, Boolean? Search)
        {


            var viewModel = new LstHallazgosIRDViewModel();
            viewModel.CargarDatos(CargarDatosContext(), NumeroPagina, IdPeriodoAcademico, IdCurso, IdOutcome, IdSede, Search, EscuelaId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LstHallazgosIRD(LstHallazgosIRDViewModel model)
        {

            model.CargarDatos(CargarDatosContext(), model.NumeroPagina, model.IdPeriodoAcademico, model.IdCurso, model.IdOutcome, model.IdSede, true, EscuelaId);
            return View(model);
        }

        public ActionResult LstHallazgosGRA(Int32? NumeroPagina, Int32? idSubModalidadPeriodoAcademico, Int32? IdCarrera, Int32? IdComision, Int32? IdNumeroPractica, Int32? IdNivelAceptacionEncuesta, Int32? opcion_rastro)
        {
            int parModalidadId = Session.GetModalidadId();

            var viewModel = new LstHallazgosGRAViewModel();
            viewModel.CargarDatos(CargarDatosContext(), NumeroPagina, idSubModalidadPeriodoAcademico, IdCarrera, IdComision, IdNumeroPractica, IdNivelAceptacionEncuesta, false, opcion_rastro, parModalidadId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult LstHallazgosGRA(LstHallazgosGRAViewModel model)
        {
            int parModalidadId = Session.GetModalidadId();
            int idsubmodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            model.CargarDatos(CargarDatosContext(), model.NumeroPagina, idsubmodapa, model.IdCarrera, model.IdComision, model.IdNumeroPractica, model.IdNivelAceptacionEncuesta, true, model.opcion_rastro, parModalidadId);
            return View(model);
        }

        public ActionResult _AddEditHallazgoGRA(Int32? HallazgoId, Int32? IdPeriodoAcademico, Int32? IdCarrera, Int32? IdNivelAceptacionHallazgo, Int32? IdComision, Int32? IdCriticidad, Int32? IdNumeroPractica)
        {
            var viewModel = new _AddEditHallazgoGRAViewModel();
            int idsubmodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            viewModel.CargarDatos(CargarDatosContext(), HallazgoId, idsubmodapa, IdCarrera, IdNivelAceptacionHallazgo, IdComision, IdCriticidad, IdNumeroPractica);
            return View(viewModel);
        }
        public ActionResult _ConfirmacionDeleteHallazgoGRA(Int32 HallazgoId, Int32 IdNivelAceptacionHallazgo)
        {
            var viewModel = new _ConfirmacionDeleteHallazgoViewModel();
            viewModel.Fill(HallazgoId, null, IdNivelAceptacionHallazgo);
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult _ConfirmacionDeleteHallazgoGRA(_ConfirmacionDeleteHallazgoViewModel model)
        {
            try
            {
                var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);
                hallazgo.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                var lstHallazgo = Context.Hallazgo.Where(x => x.Codigo == hallazgo.Codigo).ToList();
                foreach (var item in lstHallazgo)
                {
                    item.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                }
                Context.SaveChanges();
                var ruta = Url.Action("LstHallazgosGRA", new { idSubModalidadPeriodoAcademico = hallazgo.IdSubModalidadPeriodoAcademico, IdCarrera = hallazgo.IdCarrera, IdComision = hallazgo.IdComision, IdNumeroPractica = model.IdNumeroPractica, IdNivelAceptacionEncuesta = model.IdNivelAceptacionHallazgo });
                return Json(ruta);
            }
            catch
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult _AddEditHallazgoGRA(_AddEditHallazgoGRAViewModel model)
        {
            try
            {
                /*

                int parModalidadId = Session.GetModalidadId();

                var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdCarrera == model.IdCarrera).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();

                var objUltimoHallazgo2 = (from a in Context.Hallazgo
                                          join b in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                                          join c in Context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                                          join d in Context.Instrumento on a.IdInstrumento equals d.IdInstrumento
                                          where (c.IdModalidad == parModalidadId && d.Acronimo == ConstantHelpers.ENCUESTA.GRA && a.IdCarrera == model.IdCarrera)
                                          select a).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();


                var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                Int32 iHallazgo = 1;
                if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                {
                    var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2));
                    iHallazgo = aux + 1;
                }
                
                if (model.HallazgoId.HasValue)
                {
                    var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);

                    var lstHallazgo = Context.Hallazgo.Where(x => x.Codigo == hallazgo.Codigo).ToList();
                    foreach (var item in lstHallazgo)
                    {
                        item.DescripcionEspanol = model.DescripcionEspanol;
                        item.DescripcionIngles = model.DescripcionIngles;

                        var nivelEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                        var nivelHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == nivelEncuesta).IdNivelAceptacionHallazgo;
                        hallazgo.IdNivelAceptacionHallazgo = nivelHallazgo;
                        hallazgo.IdCriticidad = model.IdCriticidad.Value;
                        item.IdCarrera = model.IdCarrera;
                        item.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;
                        item.IdComision = model.IdComision;
                        var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();
                        hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                    }
                }
                */

                if (model.HallazgoId.HasValue)
                {
                    var hallazgo = Context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);

                        hallazgo.DescripcionEspanol = model.DescripcionEspanol;
                        hallazgo.DescripcionIngles = model.DescripcionIngles;

                        var nivelEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                        var nivelHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == nivelEncuesta).IdNivelAceptacionHallazgo;

                        hallazgo.IdNivelAceptacionHallazgo = nivelHallazgo;
                        hallazgo.IdCriticidad = model.IdCriticidad.Value;
                    // item.IdCarrera = model.IdCarrera;
                    // hallazgo.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;
                    // item.IdComision = model.IdComision;
                        var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();
                        var idOutcomeComision = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == model.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                        hallazgo.IdOutcomeComision = idOutcomeComision;

                }
                else
                {

                    // var SubModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).PeriodoAcademico.CicloAcademico;
                    var idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).IdSubModalidadPeriodoAcademico;
                                                                                // GRA = 4
                    FindLogic ContIndicador = new FindLogic(CargarDatosContext(), 4, idSubModalidadPeriodoAcademico);

                    var identificador = ContIndicador.ObtenerIndiceHallazgo();

                    int SMPAM;

                    if (Session.GetModalidadId().Equals(1)) // REGULAR
                        SMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;
                    else                                  // EPE
                        SMPAM = Context.SubModalidadPeriodoAcademicoModulo.LastOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;





                    var idConstituyenteInstrumento = Context.ConstituyenteInstrumento.FirstOrDefault(x => x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdConstituyenteInstrumento;
                    // Validar para Idioma Ingles
                    var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();
                    var idOutcomeComision = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == model.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcomeComision;



                    var idNivelAceptacionEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                    var idNivelAceptacionHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == idNivelAceptacionEncuesta).IdNivelAceptacionHallazgo;

                    var hallazgo = new Hallazgos();

                    hallazgo.Codigo = "HGRA-" + nombreOutcome;
                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdConstituyenteInstrumento = idConstituyenteInstrumento;
                    hallazgo.IdOutcomeComision = idOutcomeComision;
                    // hallazgo.IdCurso = null;
                    hallazgo.IdNivelAceptacionHallazgo = idNivelAceptacionHallazgo;
                    hallazgo.FechaRegistro = DateTime.Now;
                    hallazgo.IdCriticidad = model.IdCriticidad.Value;
                    hallazgo.IdSubModalidadPeriodoAcademicoModulo = SMPAM;
                    hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    // hallazgo.IdNumeroPractica = null;
                    hallazgo.Identificador = identificador;

                    Context.Hallazgos.Add(hallazgo);
                }


                /*
                var lstsede = Context.SedeCarrera.Where(x => x.IdCarrera == model.IdCarrera).ToList();
                var cicloacademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).PeriodoAcademico.CicloAcademico;
                var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdInstrumento;
                var IdOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).
                var CodigoCarrera = Context.Carrera.Where(x => x.IdCarrera == model.IdCarrera).Any

                foreach (var sede in lstsede)
                {
                    var hallazgo = new Hallazgo();
                    hallazgo.Codigo = String.Format("{0}-{1}-{2}-F-{3}", ConstantHelpers.ENCUESTA.GRA, cicloacademico, CodigoCarrera, iHallazgo.ToString("D3"));
                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    int idsubmoda = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                    hallazgo.IdSubModalidadPeriodoAcademico = idsubmoda;
                    hallazgo.IdCarrera = model.IdCarrera;
                    hallazgo.IdSede = sede.IdSede;
                    hallazgo.IdComision = model.IdComision;
                    hallazgo.FechaRegistro = DateTime.Now;
                    hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                    hallazgo.IdCriticidad = model.IdCriticidad.Value;
                    var nivelEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                    var nivelHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == nivelEncuesta).IdNivelAceptacionHallazgo;
                    hallazgo.IdNivelAceptacionHallazgo = nivelHallazgo;

                    hallazgo.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;
                    hallazgo.IdInstrumento = IdInstrumento;
                    var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();
                    hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                    hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_GRADUADOS).IdConstituyente;
                    hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
                    Context.Hallazgo.Add(hallazgo);
                }
                iHallazgo++;

            }
            */
                Context.SaveChanges();


                int idsubmodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var ruta = Url.Action("LstHallazgosGRA", new { idSubModalidadPeriodoAcademico = idsubmodapa, IdCarrera = model.IdCarrera, IdComision = model.IdComision, IdNumeroPractica = model.IdNumeroPractica, IdNivelAceptacionEncuesta = model.IdNivelAceptacionHallazgo });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult _AddEditHallazgoPPP(Int32? HallazgoId, Int32? IdPeriodoAcademico, Int32? IdCarrera, Int32? IdNivelAceptacionHallazgo, Int32? IdComision, Int32? IdNumeroPractica)
        {
            var viewModel = new _AddEditHallazgoPPPViewModel();
            viewModel.CargarDatos(CargarDatosContext(), HallazgoId, IdPeriodoAcademico, IdCarrera, IdNivelAceptacionHallazgo, IdComision, IdNumeroPractica);
            return View(viewModel);
        }

        public ActionResult _AddEditHallazgoIRD(Int32? IdCurso, Int32? IdPeriodoAcademico, Int32? IdOutcome, Int32? IdSede)
        {
            var viewModel = new _AddEditHallazgoIRDViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdCurso, IdPeriodoAcademico, IdOutcome, IdSede);
            return View(viewModel);


        }

        [HttpPost]
        public JsonResult _AddEditHallazgoIRD(_AddEditHallazgoIRDViewModel model)
        {
            try
            {

                Hallazgo hallazgo = new Hallazgo();
                var cicloacademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico && x.IdSubModalidad == 1).PeriodoAcademico.CicloAcademico;
                var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.ARD).IdInstrumento;
                model.IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico && x.IdSubModalidad == 1).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                // System.Diagnostics.Debug.WriteLine("IdInstrumento: " + IdInstrumento);

                hallazgo.IdComision = model.IdComision;
                // System.Diagnostics.Debug.WriteLine("IdComision: " + model.IdComision);

                hallazgo.FechaRegistro = DateTime.Now;
                hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;

                hallazgo.IdOutcome = model.IdOutcome;

                hallazgo.IdConstituyente = 1;
                hallazgo.IdInstrumento = IdInstrumento;
                hallazgo.IdCurso = model.IdCurso;
                hallazgo.IdCarrera = model.IdCarrera;
                hallazgo.IdSede = model.IdSede;
                hallazgo.IdAcreditadora = model.IdAcreditadora;
                hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                hallazgo.IdCriticidad = model.IdCriticidad.Value;

                hallazgo.Codigo = String.Format("{0}-{1}-{2}-F-{3}", ConstantHelpers.ENCUESTA.ARD, cicloacademico, model.IdCurso, Context.Hallazgo.Max(x => x.IdHallazgo) + 1);
                hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;

                Context.Hallazgo.Add(hallazgo);

                Context.SaveChanges();


                PostMessage(MessageType.Success);

                var ruta = Url.Action("LstHallazgosIRD", new { IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico, IdCurso = model.IdCurso, IdOutcome = model.IdOutcome, IdSede = model.IdSede, Search = true });


                return Json(ruta);
            }
            catch (DbEntityValidationException ex)
            {

                foreach (var eve in ex.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {


                System.Diagnostics.Debug.WriteLine(ex);

                throw;
            }
        }

        public ActionResult _ConfirmacionDeleteHallazgoIRD(Int32 HallazgoId)
        {
            var viewModel = new _ConfirmacionDeleteHallazgoIRDViewModel();
            viewModel.Fill(CargarDatosContext(), HallazgoId);
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult _ConfirmacionDeleteHallazgoIRD(_ConfirmacionDeleteHallazgoIRDViewModel model)
        {
            try
            {
                var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);
                hallazgo.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                int idsubmodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var ruta = Url.Action("LstHallazgosIRD", new { IdSubModalidadPeriodoAcademico = idsubmodapa, IdCurso = model.IdCurso, IdOutcome = model.IdOutcome, IdSede = model.IdSede, Search = true });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult _ConfirmacionDeleteHallazgoPPP(Int32 HallazgoId, Int32 IdNumeroPractica, Int32 IdNivelAceptacionHallazgo)
        {
            var viewModel = new _ConfirmacionDeleteHallazgoViewModel();
            viewModel.Fill(HallazgoId, IdNumeroPractica, IdNivelAceptacionHallazgo);
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult _ConfirmacionDeleteHallazgoPPP(_ConfirmacionDeleteHallazgoViewModel model)
        {
            try
            {
                var hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);
                hallazgo.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                var ruta = Url.Action("LstHallazgosPPP", new { IdSubModalidadPeriodoAcademico = hallazgo.IdSubModalidadPeriodoAcademico, IdCarrera = hallazgo.IdCarrera, IdComision = hallazgo.IdComision, IdNumeroPractica = model.IdNumeroPractica, IdNivelAceptacionEncuesta = model.IdNivelAceptacionHallazgo });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult _AddEditHallazgoPPP(_AddEditHallazgoPPPViewModel model)
        {
            try
            {
                Hallazgo hallazgo = null;

                var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.PPP).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                Int32 iHallazgo = 1;

                if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                {
                    var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2));
                    iHallazgo = aux + 1;
                }


                if (model.HallazgoId.HasValue)
                {
                    hallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.HallazgoId);

                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdComision = model.IdComision;

                    var nivelEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                    var nivelHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == nivelEncuesta).IdNivelAceptacionHallazgo;
                    hallazgo.IdNivelAceptacionHallazgo = nivelHallazgo;

                    hallazgo.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;
                    var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();
                    hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                }
                else
                {
                    hallazgo = new Hallazgo();
                    var cicloacademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico).PeriodoAcademico.CicloAcademico;
                    var IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdInstrumento;

                    hallazgo.Codigo = String.Format("{0}-{1}-F-{2}", ConstantHelpers.ENCUESTA.PPP, cicloacademico, iHallazgo.ToString("D3"));
                    hallazgo.DescripcionEspanol = model.DescripcionEspanol ?? "-";
                    hallazgo.DescripcionIngles = model.DescripcionIngles ?? "-";
                    hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;
                    hallazgo.IdCarrera = model.IdCarrera;
                    hallazgo.IdComision = model.IdComision;
                    hallazgo.FechaRegistro = DateTime.Now;
                    hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                    var nivelEncuesta = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == model.IdNivelAceptacionHallazgo).NombreEspanol;
                    var nivelHallazgo = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == nivelEncuesta).IdNivelAceptacionHallazgo;
                    hallazgo.IdNivelAceptacionHallazgo = nivelHallazgo;
                    hallazgo.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;


                    var Encuestas = (from enc in Context.Encuesta
                                     join pe in Context.PerformanceEncuesta on enc.IdEncuesta equals pe.IdEncuesta
                                     where (enc.IdTipoEncuesta == 1 && enc.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico.Value && pe.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig)
                                     //select new { enc.IdEncuesta, pe.PuntajeOutcome });
                                     select pe.PuntajeOutcome);

                    System.Diagnostics.Debug.WriteLine("Cantidad: " + Encuestas.Count());

                    int total = 0;
                    int QnecesitaMejora = 0;

                    foreach (var item in Encuestas)
                    {
                        System.Diagnostics.Debug.WriteLine("-puntaje: " + item);
                        if (item != -1)
                        {
                            total += 1;
                            if (item >= 0 && item <= (decimal)1.5)
                            {
                                QnecesitaMejora += 1;
                            }
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("TOTAL: " + total);
                    System.Diagnostics.Debug.WriteLine("NECESITA MEJORA: " + QnecesitaMejora);
                    hallazgo.IdCriticidad = 2;
                    if (total != 0)
                    {

                        decimal porcentaje = QnecesitaMejora / total;

                        if (porcentaje >= (decimal)0.23)
                        {
                            hallazgo.IdCriticidad = 1;
                        }

                    }

                    hallazgo.IdInstrumento = IdInstrumento;
                    hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_EMPLEADOR).IdConstituyente;


                    var nombreOutcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).NombreEspanol[0].ToSafeString();

                    hallazgo.IdOutcome = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == hallazgo.IdComision && x.Outcome.Nombre == nombreOutcome).IdOutcome;
                    hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;

                    Context.Hallazgo.Add(hallazgo);
                }
                Context.SaveChanges();

                var ruta = Url.Action("LstHallazgosPPP", new { IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico, IdCarrera = model.IdCarrera, IdComision = model.IdComision, IdNumeroPractica = model.IdNumeroPractica, IdNivelAceptacionEncuesta = model.IdNivelAceptacionHallazgo });
                return Json(ruta);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult ListFindOutcomeSurveyFDC(Int32? numeroPagina, String TipoEncuesta, Int32? IdPeriodoAcademico)
        {
            var viewModel = new ListFindOutcomeSurveyFDCViewModel();
            viewModel.Fill(CargarDatosContext(), numeroPagina, TipoEncuesta, IdPeriodoAcademico);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ListFindOutcomeSurveyFDC(ListFindOutcomeSurveyFDCViewModel model)
        {
            try
            {
                var viewModel = new ListFindOutcomeSurveyFDCViewModel();
                viewModel.FindOutcomesHallazgos(CargarDatosContext(), model.IdSubModalidadPeriodoAcademico);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                return View(model);
            }
        }

        public ActionResult AddEditFindOutcomeSurveyFDC(Int32? IdOutcome, Int32? IdCarrera, String NivelAceptacion, Int32? IdPeriodoAcademico, Int32? IdHallazgo)
        {
            var viewModel = new AddEditFindOutcomeSurveyFDCViewModel();
            viewModel.Fill(CargarDatosContext(), IdOutcome, IdCarrera, NivelAceptacion, IdPeriodoAcademico, IdHallazgo);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditFindOutcomeSurveyFDC(AddEditFindOutcomeSurveyFDCViewModel model)
        {
            try
            {
                if (model.IdHallazgo.HasValue)
                {
                    var codigo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == model.IdHallazgo).Codigo;
                    var LstHallazgo = Context.Hallazgo.Where(x => x.Codigo == codigo).ToList();
                    foreach (var hallazgo in LstHallazgo)
                    {
                        hallazgo.DescripcionEspanol = model.DescripcionEspanol;
                        hallazgo.DescripcionIngles = model.DescripcionIngles;
                    }
                }
                else
                {
                    Hallazgo hallazgo = null;

                    var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Instrumento.Acronimo == "LCFC" && x.IdSede == null && x.IdTipoEncuesta == null && x.IdSubModalidadPeriodoAcademico == model.IdSubModalidadPeriodoAcademico).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                    var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";
                    Int32 iHallazgo = 1;

                    if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                    {
                        var aux = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 5));
                        iHallazgo = aux + 1;
                    }
                    var LstSede = Context.Sede.ToList();
                    var idsede = Context.Sede.FirstOrDefault(x => x.Codigo == "MO").IdSede;
                    var LstCurso = Context.FuncionCursosMallaCocos(model.IdSubModalidadPeriodoAcademico.Value, model.IdSubModalidadPeriodoAcademico.Value, idsede, model.IdCarrera, "", false, 0, model.IdOutcome, 0, "es-pe", true).ToList();

                    foreach (var sede in LstSede)
                    {
                        foreach (var curso in LstCurso)
                        {
                            hallazgo = new Hallazgo();
                            hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                            Context.Hallazgo.Add(hallazgo);
                            var lcfcInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "LCFC");
                            var IdInstrumento = lcfcInstrumento == null ? 1 : lcfcInstrumento.IdInstrumento;
                            var AcronimoInstrumento = lcfcInstrumento == null ? "" : lcfcInstrumento.Acronimo;
                            hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;
                            hallazgo.IdInstrumento = IdInstrumento;
                            hallazgo.Generado = ConstantHelpers.HALLAZGO.NORMAL;
                            hallazgo.IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico.Value;
                            hallazgo.Codigo = String.Format("{0}-{1}{2}-F-{3}-{4}", AcronimoInstrumento, model.NombreOutcome, Session.GetPeriodoAcademico(), iHallazgo.ToString("D3"), model.NombreCarrera);
                            hallazgo.FechaRegistro = DateTime.Now;
                            hallazgo.IdNivelAceptacionHallazgo = model.IdNivelAceptacionHallazgo;
                            hallazgo.IdCarrera = model.IdCarrera;
                            hallazgo.IdSede = sede.IdSede;
                            hallazgo.IdOutcome = model.IdOutcome;
                            hallazgo.DescripcionEspanol = model.DescripcionEspanol;
                            hallazgo.DescripcionIngles = model.DescripcionIngles;
                            hallazgo.IdCurso = curso.IdCurso;
                        }

                    }
                }

                Context.SaveChanges();

                PostMessage(MessageType.Success);
                return RedirectToAction("ListFindOutcomeSurveyFDC", "Register", new { TipoEncuesta = ConstantHelpers.ENCUESTA.FDC, IdSubModalidadPeriodoAcademico = model.IdSubModalidadPeriodoAcademico });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                return View(model);
            }
        }
        public ActionResult DeleteFindOutcomeSurveyFDC(Int32 IdHallazgo)
        {
            var CodigoHallazgo = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo).Codigo;
            var IdPeriodoAcademico = Context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo).SubModalidadPeriodoAcademico.IdPeriodoAcademico;
            var LstHallazgo = Context.Hallazgo.Where(x => x.Codigo == CodigoHallazgo).ToList();
            foreach (var item in LstHallazgo)
            {
                item.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
            }
            Context.SaveChanges();
            PostMessage(MessageType.Success);
            return RedirectToAction("ListFindOutcomeSurveyFDC", new { TipoEncuesta = "FDC", IdPeriodoAcademico = IdPeriodoAcademico });
        }



    }
}