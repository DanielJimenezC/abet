﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Generate;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using System.IO;
using Microsoft.Reporting.WebForms;
using Zen.Barcode;
//using Ionic.Zip;
using System.Text;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    public class RoundRectangle : IPdfPCellEvent
    {
        public void CellLayout(
          PdfPCell cell, Rectangle rect, PdfContentByte[] canvas
        )
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.RoundRectangle(
              rect.Left,
              rect.Bottom,
              rect.Width,
              rect.Height,
              2 // change to adjust how "round" corner is displayed
            );
            cb.SetRGBColorStroke(250, 128, 114);
            cb.SetLineWidth(1f);
            //cb.SetCMYKColorStrokeF(0f, 0f, 0f, 1f);
            cb.Stroke();
        }
    }

    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class GenerateController : BaseController
    {
        // GET: Survey/GenerateSurvey
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GenerateSurveyPPP()
        {
            var viewmodel = new GenerateSurveyPPPViewModel();
            viewmodel.Fill(CargarDatosContext());
            return View(viewmodel);
        }
        [HttpPost]  
        public ActionResult GenerateSurveyPPP(GenerateSurveyPPPViewModel model)
        {
            try
            {
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "EncuestaNewPPP.rdlc");
                if (System.IO.File.Exists(path))
                {
                    //Obtener Información
                    var objCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.carreraId);
                    var nombreCarrera = objCarrera.Codigo;



                    var semestre = Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == model.cicloId).CicloAcademico;
                    //var semestre = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.subModalidadPeriodoAcademicoId).PeriodoAcademico.CicloAcademico;

                    var outcomeEspecifico = Context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).IdTipoOutcomeEncuesta;
                    var outcomeGeneral = Context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).IdTipoOutcomeEncuesta;

                    //  var queryE = context.GetOutcomeGenerarEncuestaPPP(model.carreraId, model.cicloId, outcomeEspecifico).ToList();
                    // var queryG = context.GetOutcomeGenerarEncuestaPPP(model.carreraId, model.cicloId, outcomeGeneral).ToList();

                    int submodapa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.cicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                     var queryE = Context.GetOutcomeGenerarNewEncuestaPPP(EscuelaId, submodapa, outcomeEspecifico).ToList();
                     var queryG = Context.GetOutcomeGenerarNewEncuestaPPP(EscuelaId, submodapa, outcomeGeneral).ToList();

                    // Generar File usando RDLC
                    /*
                    ReportLogic reporteLogic = new ReportLogic();

                    List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                    ReportDataSource.Add(new ReportDataSource("DataSetE", queryE));
                    ReportDataSource.Add(new ReportDataSource("DataSetG", queryG));

                    List<ReportParameter> parametros = new List<ReportParameter>();

                    parametros.Add(new ReportParameter("Carrera", nombreCarrera));
                    parametros.Add(new ReportParameter("Semestre", semestre));

                     return reporteLogic.GenerarReporte(ReportDataSource, "PDF", path, "Encuesta PPP " + nombreCarrera + " " + semestre, parametros);
                    */

                    //Generar File usando iTextSharp                    

                    Document document = new Document();
                    document.SetMargins(38f, 38f, 3f, 1f);

                    MemoryStream stream = new MemoryStream();

                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                    pdfWriter.CloseStream = false;

                    document.Open();
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/EncuestaSuperior2.png"));

                    //iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/headerMailUPC.jpg"));

                    imagen.BorderWidth = 0;
                    imagen.ScaleToFit(document.PageSize);
                    imagen.Alignment = Element.ALIGN_CENTER;

                    document.Add(imagen);

                    //var fontNormal = FontFactory.GetFont("Arial", 16, Font.UNDERLINE, BaseColor.BLACK);
                    //Paragraph titulo = new Paragraph("Evaluación de Desempeño de Facultad de Ingeniería");
                    //titulo.Alignment = Element.ALIGN_CENTER;
                    //document.Add(titulo);
                    //var fontsub = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, BaseColor.BLACK);

                    //var subtitulo = "A ser realizado por el jefe directo";
                    //Paragraph titulo2 = new Paragraph(subtitulo, fontsub);
                    //titulo2.Alignment = Element.ALIGN_CENTER;
                    //document.Add(titulo2);


                    //Tabla Específicos

                    PdfPTable tableE = new PdfPTable(2);
                    Paragraph cellP = new Paragraph();
                    tableE.WidthPercentage = 100;
                    // La 1era columna es 4 veces más grande que la 2da
                    tableE.SetWidths(new float[] { 8f, 1f });
                    iTextSharp.text.BaseColor redBackground = new BaseColor(250, 128, 114);
                    iTextSharp.text.BaseColor whiteBackground = new BaseColor(255, 255, 255);
                    iTextSharp.text.BaseColor RedFont = new BaseColor(198, 53, 45);

                    var fontNormal3 = FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE);
                    var fontNormal4 = FontFactory.GetFont("Arial", 8, BaseColor.BLACK);
                    var fontNormal5 = FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.BLACK);
                    var fontNormal6 = FontFactory.GetFont("Arial", 8, Font.BOLD, RedFont);

                    RoundRectangle rr = new RoundRectangle();


                    PdfPCell cell = new PdfPCell(new Phrase("COMPETENCIAS ESPECÍFICAS DEL INGENIERO", fontNormal6));
                    PdfPCell imageCell = new PdfPCell();
                    imageCell.Border = PdfPCell.NO_BORDER;
                    imageCell.CellEvent = rr;
                    cell.BorderColorLeft = redBackground;
                    cell.BorderColorRight = redBackground;
                    cell.BorderColorTop = redBackground;
                    cell.BorderColorBottom = redBackground;
                    cell.BorderWidthLeft = 1f;
                    cell.BorderWidthRight = 1f;
                    cell.BorderWidthTop = 1f;
                    cell.BorderWidthBottom = 1f;

                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    imageCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    
                    cell.PaddingBottom = 2;
                    imageCell.PaddingBottom = 2;

                    cell.Border = PdfPCell.NO_BORDER;
                    
                    cell.CellEvent = rr;

                    tableE.AddCell(cell);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;

                    //cell.Phrase = new Phrase("- --------> +", fontNormal5);
                    imageCell.Image = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/Arrow.png"));
                    tableE.AddCell(imageCell);

                    //cell.Phrase = new Phrase(objCarrera.NombreEspanol, fontNormal4);
                    //tableE.AddCell(cell);

                    //cell.Phrase = new Phrase(objCarrera.NombreIngles, fontNormal4);
                    //tableE.AddCell(cell);

                    //Se creará un contador para que los colores no se alteren 
                    //si es que uno de los campos no se incluirá por ser de otra carrera
                    //Lo ideal sería filtrar en query y utilizar i del for

                    //int contador = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;

                    for (int i = 0; i<queryE.Count; i++)
                    {
                        //if (i > 12) break;

                        System.Diagnostics.Debug.WriteLine("i: "+i+ ", pregunta: " + queryE[i].NOMBRE);
                        cell.Phrase = new Phrase();
                            if (i % 2 == 0)
                            {
                                cell.BackgroundColor = whiteBackground;
                            imageCell.BackgroundColor = whiteBackground;

                        }
                            else
                            {
                                cell.BackgroundColor = redBackground;
                            imageCell.BackgroundColor = redBackground;
                        }

                            if(queryE[i].CARRERA == "")
                            {
                                cell.Phrase = new Phrase(queryE[i].ORDEN + ". " + queryE[i].DESCRIPCION, fontNormal4);
                                tableE.AddCell(cell);
                            }
                            else
                            {
                            if (queryE[i].CARRERA == "otra")
                            {
                                cell.AddElement(new Phrase("Solo para la carrera de otra escuela", fontNormal5));
                            }
                            else
                            {
                                cell.AddElement(new Phrase("Solo para la carrera de "+queryE[i].CARRERA, fontNormal5));
                            }
                            
                            cell.AddElement(new Phrase(queryE[i].ORDEN + ". " + queryE[i].DESCRIPCION, fontNormal4));
                            tableE.AddCell(cell);
                        }


                        //cell.Phrase = new Phrase("0 1 2 3 4 NA", fontNormal4);
                        if (i % 2 == 0)
                        {
                            imageCell.Image = imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/PuntosBlanco.png"));
                        }
                        else
                        {
                            imageCell.Image = imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/PuntosRojo.png"));
                        }

                        tableE.AddCell(imageCell);
                        

                    }
                    Paragraph p = new Paragraph();
                    p.IndentationLeft = 5;
                    tableE.HorizontalAlignment = Element.ALIGN_LEFT;
                    p.Add(tableE);
                    document.Add(p);
                    imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/EndPage.png"));
                    imagen.BorderWidth = 0;
                    imagen.ScaleToFit(document.PageSize);
                    imagen.Alignment = Element.ALIGN_CENTER;
                    document.Add(imagen);
                    document.NewPage();
                    document.Add(Chunk.NEWLINE);

                    cell.PaddingBottom = 4;
                    imageCell.PaddingBottom = 4;


                    PdfPTable tableG = new PdfPTable(3);
                    cell.BackgroundColor = whiteBackground;
                    imageCell.BackgroundColor = whiteBackground;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableG.WidthPercentage = 100;
                    tableG.SetWidths(new float[] { 2f, 6f, 1f });
                    //cell.Phrase = new Phrase(" ", fontNormal5);
                    //tableG.AddCell(cell);
                    cell.Phrase = new Phrase("COMPETENCIAS GENERALES", fontNormal6);
                    cell.Colspan = 2;
                    tableG.AddCell(cell);
                    cell.Colspan = 1;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //cell.Phrase = new Phrase("- --------> +", fontNormal5);
                    imageCell.Image = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/Arrow.png"));
                    tableG.AddCell(imageCell);
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;

                    for (int i = 0; i < queryG.Count; i++)
                    {
                        if (i % 2 == 0)
                        {
                            cell.BackgroundColor = whiteBackground;
                        }
                        else
                        {
                            cell.BackgroundColor = redBackground;
                        }
                        cell.Phrase = new Phrase(queryG[i].ORDEN + ". " + queryG[i].NOMBRE, fontNormal4);
                        tableG.AddCell(cell);

                        if (queryG[i].CARRERA == "")
                        {
                            cell.Phrase = new Phrase(queryG[i].DESCRIPCION, fontNormal4);
                            tableG.AddCell(cell);
                        }
                        else
                        {

                            if (queryG[i].CARRERA == "otra")
                            { cell.AddElement(new Phrase("OTRA ESCUELA", fontNormal5)); }
                            else
                            { cell.AddElement(new Phrase(queryG[i].CARRERA, fontNormal5)); }
                            
                            cell.AddElement(new Phrase(queryG[i].DESCRIPCION, fontNormal4));
                            tableG.AddCell(cell);
                        }


                        //cell.Phrase = new Phrase("0 1 2 3 4 NA", fontNormal4);
                        if (i % 2 == 0)
                        {
                            cell.Image = imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/PuntosBlanco.png"));
                        }
                        else
                        {
                            cell.Image = imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/PuntosRojo.png"));
                        }
                       
                        tableG.AddCell(cell);


                    }
                    p = new Paragraph();
                    p.IndentationLeft = 5;
                    tableG.HorizontalAlignment = Element.ALIGN_LEFT;
                    p.Add(tableG);
                    document.Add(p);

                    imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Areas/Survey/Resources/Images/EndPage.png"));
                    imagen.BorderWidth = 0;
                    imagen.ScaleToFit(document.PageSize);
                    imagen.Alignment = Element.ALIGN_CENTER;

                    document.Add(imagen);



                    document.Close();

                    stream.Flush();
                    stream.Position = 0;



                    return File(stream, "application/pdf", "Encuesta PPP " + nombreCarrera + " " + semestre + ".pdf");
                }
                else
                {
                    PostMessage(MessageType.Error, "Error al momento de extraer la data");
                    TryUpdateModel(model);
                    model.Fill(CargarDatosContext());
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                model.Fill(CargarDatosContext());
                return View(model);
            }
        }
        public ActionResult GenerateSurveyFDC()
        {
            var viewmodel = new GenerateSurveyFDCViewModel();
            viewmodel.Fill(CargarDatosContext());
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult GenerateSurveyFDC(GenerateSurveyFDCViewModel model)
        {
            var a = 0;
            try
            {
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "EncuestaFDC.rdlc");
                //var objZip = new ZipFile();
                
                var baseOutputStream = new MemoryStream();
                ZipOutputStream zipOutput = new ZipOutputStream(baseOutputStream);
                zipOutput.IsStreamOwner = false;

                zipOutput.SetLevel(3);
                byte[] buffer = new byte[4096];

                if (System.IO.File.Exists(path))
                {
                    //MemoryStream objStream = null;
                    var Seccion = Context.Seccion.FirstOrDefault(x => x.IdSeccion == model.seccionId);
                    var Curso = Context.Curso.FirstOrDefault(x => x.IdCurso == model.IdCurso);
                    var Semestre = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.subModalidadPeriodoAcademicoId).PeriodoAcademico.CicloAcademico;
                    var Sede = Context.Sede.FirstOrDefault( x => x.IdSede == model.sedeId);
                    var pSeccion = Seccion != null ? Seccion.Codigo : null;
                    var pCurso = Curso != null ? Curso.Codigo : null;

                    var lstEncuestas = Context.GetSeccionCurso(pSeccion, pCurso, model.subModalidadPeriodoAcademicoId, model.sedeId);

                    foreach (var item in lstEncuestas)
                    {
                        a++;
                        //var logro = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == item.IdCurso).LogroFinCicloEspanol;

                        String Logro = "-";
                        var malla = Context.spGetCursosControlEncuesta(item.IdCurso, model.subModalidadPeriodoAcademicoId, 3, item.CodigoCurso).Where(x => x.LogroFinCicloEspanol.Length > 0).FirstOrDefault();
                        if (malla != null)
                        {
                            Logro = malla.LogroFinCicloEspanol;
                        }


                        var nombreCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == item.IdCurso).NombreEspanol;
                        var docente = Context.DocenteSeccion.FirstOrDefault(x => x.IdSeccion == item.IdSeccion).Docente;

                        var query = Context.GetSeccionCurso(item.CodigoSeccion, item.CodigoCurso, model.subModalidadPeriodoAcademicoId, item.IdSede).ToList();

                        ReportLogic reporteLogic = new ReportLogic();
                        var reportDataSource = new List<ReportDataSource>();
                        reportDataSource.Add(new ReportDataSource("DataSetReporteFDC", query));
                        List<ReportParameter> parametros = new List<ReportParameter>();

                        parametros.Add(new ReportParameter("Semestre", Semestre));
                        parametros.Add(new ReportParameter("Curso", item.CodigoCurso));
                        parametros.Add(new ReportParameter("Seccion", item.CodigoSeccion));
                        parametros.Add(new ReportParameter("Docente", docente == null ? "-" : docente.Nombres + " " + docente.Apellidos));
                        parametros.Add(new ReportParameter("Logro", Logro));
                        parametros.Add(new ReportParameter("NombreCurso", nombreCurso));

                        DateTime FechaEnvioEmail = ConvertHelpers.ToDateTime(model.FechaEnvio);
                        // var PeriodoAcademico = context.PeriodoAcademico.FirstOrDefault(x => x.FechaInicioCiclo <= model.FechaEnvio && x.FechaFinCiclo >= model.FechaEnvio);
                        var PeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.FechaInicioPeriodo >= model.FechaEnvio && x.PeriodoAcademico.FechaFinPeriodo >= model.FechaEnvio && x.IdSubModalidadPeriodoAcademico == model.subModalidadPeriodoAcademicoId);

                        if (PeriodoAcademico != null)
                        {
                            var lstSecciones = Context.Seccion.Where(x => x.IdSeccion == item.IdSeccion).ToList();

                            if (lstSecciones.Count() > 0)
                            {
                                var lstDelegados = Context.AlumnoSeccion.Where(x => x.IdSeccion == item.IdSeccion && x.EsDelegado == true).ToList();
                                var Plantilla = Context.Plantilla.FirstOrDefault();

                                if (lstDelegados.Count() > 0)
                                {
                                    foreach (var delegado in lstDelegados)
                                    {
                                        var Email = new Email();
                                        Email.Asunto = Plantilla.Asunto;
                                        StringBuilder sb = new StringBuilder(Plantilla.Mensaje);
                                        sb.Replace("{PARAM_SECCION}", delegado.Seccion.Codigo);
                                        Email.Mensaje = sb.ToString();
                                        Email.FechaEnvio = ConvertHelpers.ToDateTime(model.FechaEnvio);
                                        Email.IdAlumnoSeccion = delegado.IdAlumnoSeccion;
                                        Email.IdPlantilla = Plantilla.IdPlantilla;
                                        Email.IdSubModalidadPeriodoAcademico = PeriodoAcademico.IdSubModalidadPeriodoAcademico;
                                        Email.Estado = ConstantHelpers.ENCUESTA.ESTADO_SIN_ENVIAR;
                                        Context.Email.Add(Email);
                                    }
                                }
                                else
                                {
                                    //PostMessage(MessageType.Error, "No existen delegados para la sección seleccionada.");
                                    //TryUpdateModel(model);
                                    //model.Fill(CargarDatosContext());
                                    //return View(model);
                                }

                            }
                            else
                            {
                                var lstDelegados = Context.AlumnoSeccion.Where(x => x.EsDelegado == true).ToList();
                                var Plantilla = Context.Plantilla.FirstOrDefault();

                                foreach (var delegado in lstDelegados)
                                {
                                    var Email = new Email();
                                    Email.Asunto = Plantilla.Asunto;
                                    StringBuilder sb = new StringBuilder(Plantilla.Mensaje);
                                    sb.Replace("{PARAM_SECCION}", delegado.Seccion.Codigo);
                                    Email.Mensaje = sb.ToString();
                                    Email.FechaEnvio = ConvertHelpers.ToDateTime(model.FechaEnvio);
                                    Email.IdAlumnoSeccion = delegado.IdAlumnoSeccion;
                                    Email.IdPlantilla = Plantilla.IdPlantilla;
                                    Email.IdSubModalidadPeriodoAcademico = PeriodoAcademico.IdSubModalidadPeriodoAcademico;
                                    Email.Estado = ConstantHelpers.ENCUESTA.ESTADO_SIN_ENVIAR;
                                    Context.Email.Add(Email);
                                }
                            }
                            //context.SaveChanges();
                        }
                        else
                        {
                            PostMessage(MessageType.Error, MessageResource.ErrorAlProgramarLaFechaDeEnvioDeCorreoNoExistePeriodoAcademicoParaFechaSeleccionada);
                            TryUpdateModel(model);
                            model.Fill(CargarDatosContext());
                            return View(model);
                        }
                        
                        var ruta = Path.Combine(Server.MapPath("~/Files/SurveyReport"), "Encuesta Fin de Ciclo Curso " + item.CodigoCurso + " Seccion " + item.CodigoSeccion + " Sede " + item.CodigoSede+".pdf");
                        Byte[] archivoBytes = reporteLogic.GenerarReporte(reportDataSource, "PDF", path, ruta, parametros).FileContents;
                        
                        System.IO.File.WriteAllBytes(ruta, archivoBytes);
                        //return reporteLogic.GenerarReporte("PDF", path, , parametros);
                        //objZip.AddFile(ruta);

                        ZipEntry entry = new ZipEntry(Path.GetFileName(ruta));
                        entry.DateTime = DateTime.Now;
                        zipOutput.PutNextEntry(entry);
                        using (FileStream fs = System.IO.File.OpenRead(ruta))
                        {
                            int sourceBytes = 0;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                zipOutput.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                                         
                    }
                    //objStream = new MemoryStream();
                    //objZip.Save(objStream);
                    //byte[] fileBytes = objStream.ToArray();
                    //
                    //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Zip, "Encuestas LCFC "+Sede.Codigo+" " + Semestre+".zip");
                    Context.SaveChanges();
                    zipOutput.Finish();
                    zipOutput.Close();

                    baseOutputStream.Position = 0;

                    return new FileStreamResult(baseOutputStream, "application/x-zip-compressed")
                    {
                        FileDownloadName = "Encuestas LCFC " + Sede.Codigo + " " + Semestre + ".zip"
                    };
                }
                else
                {
                    PostMessage(MessageType.Error, MessageResource.ErrorAlMomentoDeExtraerLaData);
                    TryUpdateModel(model);
                    model.Fill(CargarDatosContext());
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                model.Fill(CargarDatosContext());
                return View(model);
            }
    
        }
        public ActionResult GenerateSurveyGRA()
        {
            var viewmodel = new GenerateSurveyGRAViewModel();
            viewmodel.Fill(CargarDatosContext());
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult GenerateSurveyGRA(GenerateSurveyGRAViewModel model)
        {
            try
            {
                String path = Path.Combine(Server.MapPath("~/Areas/Survey/Resources/Reportes"), "EncuestaGRA.rdlc");
                if (System.IO.File.Exists(path))
                {
                    var nombreCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.carreraId).NombreEspanol;
                    var semestre = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.subModalidadPeriodoAcademicoId).PeriodoAcademico.CicloAcademico;
                    var outcomeEspecifico = Context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).IdTipoOutcomeEncuesta;
                    var outcomeGeneral = Context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).IdTipoOutcomeEncuesta;

                    var queryE = Context.GetOutcomeGenerarEncuestaGRA(model.carreraId, model.subModalidadPeriodoAcademicoId, outcomeEspecifico).ToList();
                    var queryG = Context.GetOutcomeGenerarEncuestaGRA(model.carreraId, model.subModalidadPeriodoAcademicoId, outcomeGeneral).ToList();

                    ReportLogic reporteLogic = new ReportLogic();

                    List<ReportDataSource> ReportDataSource = new List<ReportDataSource>();
                    ReportDataSource.Add(new ReportDataSource("DataSetE", queryE));
                    ReportDataSource.Add(new ReportDataSource("DataSetG", queryG));

                    List<ReportParameter> parametros = new List<ReportParameter>();

                    parametros.Add(new ReportParameter("Carrera", nombreCarrera));
                    parametros.Add(new ReportParameter("Semestre", semestre));

                    return reporteLogic.GenerarReporte(ReportDataSource, "PDF", path, "Encuesta Graduando " + nombreCarrera + " " + semestre, parametros);
                }
                else
                {
                    PostMessage(MessageType.Error, MessageResource.ErrorAlMomentoDeExtraerLaData);
                    TryUpdateModel(model);
                    model.Fill(CargarDatosContext());
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                TryUpdateModel(model);
                model.Fill(CargarDatosContext());
                return View(model);
            }
        }
        public ActionResult _ViewSurveyTemplate(Int32 TemplateEncuestaId)
        {
            var viewmodel = new _ViewSurveyTemplateViewModel();
            viewmodel.Fill(CargarDatosContext(), TemplateEncuestaId);
            return View(viewmodel);
        }
    }
}