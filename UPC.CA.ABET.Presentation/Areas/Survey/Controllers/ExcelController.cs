﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Models;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Data;
using Excel;
using System.Transactions;
using OfficeOpenXml;
using System.Drawing;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Filters;
using OfficeOpenXml.Style;
using System.Text.RegularExpressions;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email;
using System.Data.Entity.Core.Objects;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Globalization;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class ExcelController : BaseController
    {
        // GET: Survey/Excel
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DownloadTemplateSurveyPPP(Int32? acreditadoraId, Int32? carreraId, Int32? cicloId)
        {
            var viewmodel = new DownloadTemplateSurveyPPPViewModel();
            viewmodel.Fill(CargarDatosContext(), acreditadoraId, carreraId, cicloId);
            return View(viewmodel);
        }
        public ActionResult DownloadTemplateSurveyFDC(Int32? acreditadoraId, Int32? carreraId, Int32? cicloId)
        {
            var viewmodel = new DownloadTemplateSurveyFDCViewModel();
            viewmodel.Fill(CargarDatosContext(), acreditadoraId, carreraId, cicloId);
            return View(viewmodel);
        }
        public ActionResult DownloadTemplateSurveyGRA(Int32? acreditadoraId, Int32? carreraId, Int32? cicloId)
        {
            var viewmodel = new DownloadTemplateSurveyGRAViewModel();
            viewmodel.Fill(CargarDatosContext(), acreditadoraId, carreraId, cicloId);
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult DownloadTemplateExcel(Int32? acreditadoraId, Int32? carreraId, Int32? cicloId, String Tipo)
        {
            var fechaActual = DateTime.Now;
            var CodigoCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == carreraId).Codigo;
            String nombreArchivo = "Plantilla_CargaMasiva_" + CodigoCarrera + "_" + Tipo + "_" + fechaActual.ToString("ddMMyyyy") + ".xlsx";
            String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            try
            {
                var LstOutcomes = Context.OutcomeEncuestaPPPConfig.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == cicloId &&
                x.IdEscuela == EscuelaId
                // && ( x.IdCarrera == carreraId || x.IdCarrera.HasValue == false )
                && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderByDescending(x => x.IdTipoOutcomeEncuesta).ThenBy(x => x.Orden).ToList();

                Int32 i;
                var memoryStream = new MemoryStream();
                FileInfo newFile = new FileInfo(nombreArchivo);
                using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");
                    switch (Tipo)
                    {
                        case ConstantHelpers.ENCUESTA.PPP:
                            i = 13;
                            worksheet.Cells["A1"].Value = "CÓDIGO ENCUESTA";
                            worksheet.Cells["A1"].AutoFitColumns();
                            worksheet.Cells["B1"].Value = "CÓDIGO ALUMNO";
                            worksheet.Cells["B1"].AutoFitColumns();
                            worksheet.Cells["C1"].Value = "#PRÁCTICA";
                            worksheet.Cells["C1"].AutoFitColumns();
                            worksheet.Cells["D1"].Value = "TOTAL HORAS";
                            worksheet.Cells["D1"].AutoFitColumns();
                            worksheet.Cells["E1"].Value = "RAZÓN SOCIAL";
                            worksheet.Cells["E1"].AutoFitColumns();
                            worksheet.Cells["F1"].Value = "RUC";
                            worksheet.Cells["F1"].AutoFitColumns();
                            worksheet.Cells["G1"].Value = "NOMBRE JEFE";
                            worksheet.Cells["G1"].AutoFitColumns();
                            worksheet.Cells["H1"].Value = "CARGO JEFE";
                            worksheet.Cells["H1"].AutoFitColumns();
                            worksheet.Cells["I1"].Value = "TELÉFONO JEFE";
                            worksheet.Cells["I1"].AutoFitColumns();
                            worksheet.Cells["J1"].Value = "EMAIL JEFE";
                            worksheet.Cells["J1"].AutoFitColumns();
                            worksheet.Cells["K1"].Value = "FECHA INICIO (dd/mm/yyyy)";
                            worksheet.Cells["K1"].AutoFitColumns();
                            worksheet.Cells["L1"].Value = "FECHA FIN (dd/mm/yyyy)";
                            worksheet.Cells["L1"].AutoFitColumns();

                            foreach (var item in LstOutcomes)
                            {
                                worksheet.Cells[1, i].Value = "C" + (i - 12);
                                worksheet.Cells[1, i].AutoFitColumns();


                                var validationCell = worksheet.DataValidations.AddListValidation(worksheet.Cells[2, i].Address);
                                for (int k = 1; k <= 5; k++)
                                {
                                    if (k != 5)
                                        validationCell.Formula.Values.Add(k.ToString());
                                    else
                                        validationCell.Formula.Values.Add("NA");
                                }

                                i++;
                            }

                            foreach (var item in Context.PreguntaAdicional.ToList())
                            {
                                worksheet.Cells[1, i].Value = "PA " + (i - 12);
                                worksheet.Cells[1, i].AutoFitColumns();

                                var validationCell = worksheet.DataValidations.AddListValidation(worksheet.Cells[2, i].Address);
                                foreach (var respuesta in Context.PuntajePreguntaAdicional.Where(x => x.IdPreguntaAdicional == item.IdPreguntaAdicional))
                                {
                                    validationCell.Formula.Values.Add(respuesta.NombreEspanol);
                                }
                                i++;
                            }
                            worksheet.Cells[1, i].Value = "COMENTARIO";
                            worksheet.Cells[1, i].AutoFitColumns();

                            worksheet.Cells[1, 1, 1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[1, 1, 1, i].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            worksheet.Cells[1, 1, 1, i].Style.Font.Color.SetColor(Color.White);
                            break;
                        case ConstantHelpers.ENCUESTA.FDC:
                            i = 15;
                            worksheet.Cells["A1"].Value = "CÓDIGO SECCIÓN";
                            worksheet.Cells["A1"].AutoFitColumns();
                            worksheet.Cells["B1"].Value = "CÓDIGO CURSO";
                            worksheet.Cells["B1"].AutoFitColumns();
                            worksheet.Cells["C1"].Value = "1";
                            worksheet.Cells["C1"].AutoFitColumns();
                            worksheet.Cells["D1"].Value = "2";
                            worksheet.Cells["D1"].AutoFitColumns();
                            worksheet.Cells["E1"].Value = "3";
                            worksheet.Cells["E1"].AutoFitColumns();
                            worksheet.Cells["F1"].Value = "4";
                            worksheet.Cells["F1"].AutoFitColumns();
                            worksheet.Cells["G1"].Value = "5";
                            worksheet.Cells["G1"].AutoFitColumns();
                            worksheet.Cells["H1"].Value = "6";
                            worksheet.Cells["H1"].AutoFitColumns();
                            worksheet.Cells["I1"].Value = "7";
                            worksheet.Cells["I1"].AutoFitColumns();
                            worksheet.Cells["J1"].Value = "8";
                            worksheet.Cells["J1"].AutoFitColumns();
                            worksheet.Cells["K1"].Value = "9";
                            worksheet.Cells["K1"].AutoFitColumns();
                            worksheet.Cells["L1"].Value = "10";
                            worksheet.Cells["L1"].AutoFitColumns();
                            worksheet.Cells["O1"].Value = "CÓDIGO ENCUESTA";
                            worksheet.Cells["O1"].AutoFitColumns();
                            worksheet.Cells["P1"].Value = "CÓDIGO SECCIÓN";
                            worksheet.Cells["P1"].AutoFitColumns();
                            worksheet.Cells["Q1"].Value = "CÓDIGO CURSO";
                            worksheet.Cells["Q1"].AutoFitColumns();
                            worksheet.Cells["R1"].Value = "COMENTARIO";
                            worksheet.Cells["R1"].AutoFitColumns();

                            worksheet.Cells[1, 1, 1, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[1, 1, 1, 12].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            worksheet.Cells[1, 1, 1, 12].Style.Font.Color.SetColor(Color.White);

                            worksheet.Cells[1, 15, 1, 18].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[1, 15, 1, 18].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            worksheet.Cells[1, 15, 1, 18].Style.Font.Color.SetColor(Color.White);
                            break;
                        case ConstantHelpers.ENCUESTA.GRA:
                            i = 4;
                            worksheet.Cells["A1"].Value = "CÓDIGO ENCUESTA";
                            worksheet.Cells["A1"].AutoFitColumns();
                            worksheet.Cells["B1"].Value = "CÓDIGO CARRERA";
                            worksheet.Cells["B1"].AutoFitColumns();
                            worksheet.Cells["C1"].Value = "CICLO ACADÉMICO (yyyy01/yyyy02)";
                            worksheet.Cells["C1"].AutoFitColumns();

                            foreach (var item in LstOutcomes)
                            {
                                worksheet.Cells[1, i].Value = "C" + (i - 3);
                                worksheet.Cells[1, i].AutoFitColumns();

                                i++;
                            }

                            worksheet.Cells[1, i].Value = "COMENTARIO";
                            worksheet.Cells[1, i].AutoFitColumns();

                            worksheet.Cells[1, 1, 1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[1, 1, 1, i].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            worksheet.Cells[1, 1, 1, i].Style.Font.Color.SetColor(Color.White);
                            break;
                    }

                    var fileStream = new MemoryStream();
                    xlPackage.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = nombreArchivo;

                    return fsr;
                }
            }
            catch (IOException ioe)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("DownloadTemplateSurvey" + Tipo);




            //try
            //{
            //    var archivos = new DownloadTemplate().FilterTemplate(context, acreditadoraId, carreraId, cicloId, Tipo);
            //    if (archivos.Count > 0)
            //    {
            //        StringBuilder Path = new StringBuilder();
            //        MemoryStream output = new MemoryStream();
            //        if (archivos.Count > 1)
            //        {
            //            ZipFile zip = new ZipFile();
            //            for (int i = 0; i < archivos.Count; i++)
            //            {
            //                Path.Append(Server.MapPath("~/Areas/Survey/Resources/Templates/Excel/"));
            //                Path.Append(archivos[i].TipoEncuesta.Acronimo).Append("\\").Append(archivos[i].Nombre);
            //                zip.AddFile(Path.ToString(), "");
            //                Path.Clear();
            //            }
            //            zip.Save(output);
            //        }
            //        else
            //        {
            //            Path.Append(Server.MapPath("~/Areas/Survey/Resources/Templates/Excel/"));
            //            Path.Append(archivos[0].TipoEncuesta.Acronimo).Append("\\").Append(archivos[0].Nombre);
            //            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.ToString());
            //            string fileName = archivos[0].Nombre;
            //            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            //        }
            //        output.Seek(0, 0);
            //        Path.Append(Tipo).Append(" - Template Excel").Append(".zip");
            //        return File(output, "application/octet-stream", Path.ToString());
            //    }
            //    PostMessage(MessageType.Warning, "No se encontraron archivos.");
            //    return RedirectToAction("DownloadTemplateSurvey" + Tipo, "Excel", new { acreditadoraId = acreditadoraId, carreraId = carreraId, cicloId = cicloId });
            //}
            //catch (Exception ex)
            //{
            //    PostMessage(MessageType.Error);
            //    return RedirectToAction("DownloadTemplateSurvey" + Tipo, "Excel");
            //}
        }

        /*UPLOAD NOTIFICACION ENCUESTA ALUMNO*/
        [HttpPost]
        public JsonResult UploadNotificacionEncuestaGRA(EmailSurveyGRAViewModel model)
        {
            DataCarga data = new DataCarga();
            try
            {
                int colCodigoAlumno = 0;
                int cantFail = 0;
                int cantSuccess = 0;

                var idSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();

                var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                data.lstError = new List<string>();
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    try
                    {
                        var outputparameter = new ObjectParameter("Resultado", typeof(Int32));
                        var CodigoAlumno = dataTable.Rows[i][colCodigoAlumno].ToInteger().ToString().Trim();
                        int IdEscuela = Session.GetEscuelaId();
                        int IdCarrera = model.ValidarCarrera ? model.IdCarrera : 0;
                        Context.INSERTAR_NOTIFICACION_ENCUESTA_ALUMNO(CodigoAlumno, IdEscuela, IdCarrera, outputparameter);

                        Int32 resultado = (Int32)outputparameter.Value;
                        /*
                         -1:	Error de data existente
                         0:		Se registró con éxito
                         1:   	No se encuentra el alumno en los registros
                         2:		No está matriculado
						 3:		No pertenece a la carrera del coordinador
						 4:		Ya existe una notificación para el alumno
                        */
                        switch (resultado)
                        {
                            case -1:
                                cantFail++;
                                data.lstError.Add(CodigoAlumno + " " + ((currentCulture.Equals("es-PE")) ? "Ocurrió un error en el procedimiento." : "An error occurred in the procedure."));
                                break;
                            case 0:
                                cantSuccess++;
                                break;
                            case 1:
                                cantFail++;
                                data.lstError.Add(CodigoAlumno + " " + ((currentCulture.Equals("es-PE")) ? "No existe el alumno en nuestros registros." : "There is no student in our records."));
                                break;
                            case 2:
                                cantFail++;
                                data.lstError.Add(CodigoAlumno + " " + ((currentCulture.Equals("es-PE")) ? "No hay inscripción registrada." : "There is no registered enrollment."));
                                break;
                            case 3:
                                cantFail++;
                                data.lstError.Add(CodigoAlumno + " " + ((currentCulture.Equals("es-PE")) ? "No forma parte de la carrera del coordinador." : "It is not part of the coordinator's career."));
                                break;
                            case 4:
                                cantFail++;
                                data.lstError.Add(CodigoAlumno + " " + ((currentCulture.Equals("es-PE")) ? "Ya tiene una notificación." : "Already has a notification."));
                                break;
                        }

                        data.CantSuccess = cantSuccess;
                        data.Error = false;
                    }
                    catch (Exception ex)
                    {
                        data.Error = true;
                        return Json(data);
                    }
                }

                return Json(data);
            }
            catch (Exception ex)
            {
                data.Error = true;
                return Json(data);
            };
        }
        [HttpPost]
        public JsonResult UploadHorasProfesor(MaintenanceViewModel model)
        {
            DataCarga data = new DataCarga();
            try
            {
                Int32 colCodigo = 0;
                Int32 colHoras = 1;
                Int32 colSede = 2;

                Int32 cantFail = 0;
                Int32 cantSuccess = 0;

                var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                data.lstError = new List<string>();
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    try
                    {
                        var outputparameter = new ObjectParameter("Resultado", typeof(Int32));
                        var CodigoProfesor = dataTable.Rows[i][colCodigo].ToString().Trim();
                        var Horas = Convert.ToDouble(dataTable.Rows[i][colHoras].ToString().Trim());
                        var Sede = dataTable.Rows[i][colSede].ToString().Trim();
                        var idPeriodoAcademico = Session.GetPeriodoAcademico();


                        Context.INSERTAR_PROFESOR_HORAS(CodigoProfesor, Sede, idPeriodoAcademico, Horas, outputparameter);

                        Int32 resultado = (Int32)outputparameter.Value;
                        /*
                         -1:	Error en la inserción 
                         1:		Se registró con éxito
                         2:   	El código de docente no se encuentra
                         3:		La sede no existe 
                        */
                        switch (resultado)
                        {
                            case -1:
                                cantFail++;
                                data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "Ocurrió un error en el procedimiento." : "An error occurred in the procedure."));
                                break;
                            case 1:
                                cantSuccess++;
                                break;
                            case 2:
                                cantFail++;
                                data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de docente no existe ." : "The teacher code does not exist."));
                                break;
                            case 3:
                                cantFail++;
                                data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de sede no existe." : "The campus code doest not exist."));
                                break;
                        }

                        data.CantSuccess = cantSuccess;
                        data.Error = false;
                    }
                    catch (Exception ex)
                    {
                        data.Error = true;
                    }
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                data.Error = true;
                return Json(data);
            };
        }

        [HttpPost]
        public ActionResult DownloadProfesorHProgramadas()
        {
            var fechaActual = DateTime.Now;
            String nombreArchivo = "Plantilla_CargaMasiva_ProfesorHorasProgramadas" + fechaActual.ToString("ddMMyyyy") + ".xlsx";
            String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            try
            {
                Int32 i;
                var memoryStream = new MemoryStream();
                FileInfo newFile = new FileInfo(nombreArchivo);
                using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");

                    worksheet.Cells["A1"].Value = "CÓDIGO";
                    worksheet.Cells["A1"].AutoFitColumns();

                    worksheet.Cells["B1"].Value = "HORAS";
                    worksheet.Cells["B1"].AutoFitColumns();

                    worksheet.Cells["C1"].Value = "SEDE(MO,SI,VL,CS)";
                    worksheet.Cells["C1"].AutoFitColumns();


                    worksheet.Cells[2, 1].Value = "PCSIJABA";
                    worksheet.Cells[2, 1].AutoFitColumns();

                    worksheet.Cells[2, 2].Value = "18.5";
                    worksheet.Cells[2, 2].AutoFitColumns();

                    worksheet.Cells[2, 3].Value = "MO";
                    worksheet.Cells[2, 3].AutoFitColumns();



                    worksheet.Cells[1, 1, 1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, 1, 1, 3].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells[1, 1, 1, 3].Style.Font.Color.SetColor(Color.White);


                    var fileStream = new MemoryStream();
                    xlPackage.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = nombreArchivo;

                    return fsr;
                }
            }
            catch (IOException ioe)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("TimeProfesor", "Maintenance");
            }
        }
        [HttpPost]
        public ActionResult DownloadTemplateExcelNotificacionesGRA()
        {
            var fechaActual = DateTime.Now;
            String nombreArchivo = "Plantilla_CargaMasiva_Notificaicones" + fechaActual.ToString("ddMMyyyy") + ".xlsx";
            String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            try
            {
                Int32 i;
                var memoryStream = new MemoryStream();
                FileInfo newFile = new FileInfo(nombreArchivo);
                using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");

                    worksheet.Cells["A1"].Value = "CÓDIGO";
                    worksheet.Cells["A1"].AutoFitColumns();

                    worksheet.Cells[2, 1].Value = "201113344";
                    worksheet.Cells[2, 1].AutoFitColumns();

                    worksheet.Cells[1, 1, 1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, 1, 1, 1].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    worksheet.Cells[1, 1, 1, 1].Style.Font.Color.SetColor(Color.White);


                    var fileStream = new MemoryStream();
                    xlPackage.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = nombreArchivo;

                    return fsr;
                }
            }
            catch (IOException ioe)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("EmailSurveyGRA", "Email");
            }
        }
        public ActionResult UploadPPP()
        {
            var model = new UploadPPPViewModel();
            int ModalidadId = session.GetModalidadId();
            model.Fill(CargarDatosContext(), ModalidadId);
            return View(model);
        }
        
        public async Task<bool> ActualizarSuperHallazgosPPP(int IdPeriodoAcademico)
        {
            String CicloAcademico = Context.PeriodoAcademico.FirstOrDefault(X => X.IdPeriodoAcademico == IdPeriodoAcademico).CicloAcademico;

            try
            {
                int idtipooutcomeencuesta = Context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == "COMPETENCIAS ESPECIFICAS").IdTipoOutcomeEncuesta;
                List<OutcomeEncuestaPPPConfig> gruposPPP = Context.OutcomeEncuestaPPPConfig
                    .Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico 
                        && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO 
                        && x.IdTipoOutcomeEncuesta == idtipooutcomeencuesta
                        && x.IdEscuela == EscuelaId)
                    .ToList();
                List<PerformanceEncuestaPPP> resultados = new List<PerformanceEncuestaPPP>();
                List<NivelAceptacionEncuesta> Lstniveles = new List<NivelAceptacionEncuesta>();

                Lstniveles = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.IdTipoEncuesta == 1).ToList();

                foreach (OutcomeEncuestaPPPConfig grupoPPP in gruposPPP)
                {
                    resultados = Context.PerformanceEncuestaPPP.Where(x => x.Encuesta.IdTipoEncuesta == 1 && x.IdOutcomeEncuestaPPPConfig == grupoPPP.IdOutcomeEncuestaPPPConfig && x.PuntajeOutcome != -1).ToList();

                    if (resultados.Count == 0)
                        continue;

                    int CantidadTotal = resultados.Count;
                    int CantidadNecesitaMejora = resultados.Where(x => x.PuntajeOutcome <= 1).ToList().Count;
                    int idnivelAceptacion = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == "Esperado").IdNivelAceptacionHallazgo;
                    float puntajePromedio = 0;
                    int idCriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == "Preocupante").IdCriticidad;

                    if ((float)CantidadNecesitaMejora / (float)CantidadTotal >= 0.23)
                    {
                        idCriticidad = Context.Criticidad.FirstOrDefault(x => x.NombreEspanol == "Critico").IdCriticidad;
                    }

                    foreach (PerformanceEncuestaPPP resultado in resultados)
                    {
                        puntajePromedio = puntajePromedio + (float)resultado.PuntajeOutcome.Value;
                    }

                    puntajePromedio = puntajePromedio / (float)resultados.Count;

                    if (puntajePromedio < (float)Lstniveles[0].ValorMaximo)
                    {
                        idnivelAceptacion = 1;
                    }
                    else if (puntajePromedio < (float)Lstniveles[1].ValorMaximo)
                    {
                        idnivelAceptacion = 2;
                    }
                    else
                    {
                        idnivelAceptacion = 3;
                    }

                    String codigo = "PPP-" + CicloAcademico + "-" + grupoPPP.Escuela.Codigo + "-S-" + grupoPPP.IdOutcomeEncuestaPPPConfig;

                    List<Outcome> outcomesHallazgos = Context.OutcomeEncuestaPPPOutcome.Where(x => x.IdOutcomeEncuestaPPPConfig == grupoPPP.IdOutcomeEncuestaPPPConfig).Select(x => x.Outcome).ToList();
                    foreach (Outcome outcome in outcomesHallazgos)
                    {

                        System.Diagnostics.Debug.WriteLine("Crear superhallazgo con codigo: " + codigo + "-" + outcome.IdOutcome);
                        System.Diagnostics.Debug.WriteLine("y el puntaje promedio del outcome es: " + puntajePromedio);
                        Hallazgo hallazgo = Context.Hallazgo.FirstOrDefault(x => x.Codigo == codigo + " - " + outcome.IdOutcome);

                        if (hallazgo == null)
                            hallazgo = new Hallazgo();

                        hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;

                        hallazgo.IdInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "PPP").IdInstrumento;
                        hallazgo.IdNivelAceptacionHallazgo = idnivelAceptacion;
                        hallazgo.IdOutcome = outcome.IdOutcome;
                        int idcomision = Context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == outcome.IdOutcome && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).IdComision;
                        hallazgo.IdComision = idcomision;
                        hallazgo.Generado = "AUT";
                        hallazgo.IdOutcomeEncuestaPPPConfig = grupoPPP.IdOutcomeEncuestaPPPConfig;
                        hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == "PPP").IdTipoEncuesta;

                        hallazgo.Codigo = codigo + " - " + outcome.IdOutcome;


                        String codigocomision = Context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == outcome.IdOutcome && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).Comision.Codigo;
                        hallazgo.DescripcionEspanol = "Puntaje promedio PPP en '" + grupoPPP.DescripcionEspanol.ToUpper() + "': " + Math.Round(puntajePromedio, 2);
                        hallazgo.DescripcionIngles = "Average score (Outcome: " + outcome.NombreIngles + "', Commission: '" + codigocomision + "') is: " + Math.Round(puntajePromedio, 2);
                        var subModalidadPeriodoAcademico = await Context.SubModalidadPeriodoAcademico.FirstOrDefaultAsync(x => x.PeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico);
                        hallazgo.IdSubModalidadPeriodoAcademico = subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;

                        hallazgo.IdCriticidad = idCriticidad;
                        hallazgo.IdCarrera = Context.CarreraComision.FirstOrDefault(x => x.IdComision == idcomision && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).IdCarrera;
                        hallazgo.IdAcreditadora = 1;
                        hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                        hallazgo.FechaRegistro = DateTime.Now;
                        if (hallazgo.IdHallazgo == 0)
                            Context.Hallazgo.Add(hallazgo);
                        try
                        {
                            Context.SaveChanges();
                        }

                        catch (DbUpdateException e)
                        {
                            System.Diagnostics.Debug.WriteLine("ERROR: " + e);
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("---");
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadNewPPP(UploadPPPViewModel model)
        {
            bool esMallaArizona = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.CicloId).FirstOrDefault().EsMallaArizona;

            model.subModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                                  

            String fileExtension = System.IO.Path.GetExtension(model.Archivo.FileName);

            if (fileExtension == ".dat")
            {
                var data = new DataCarga();
                var ruta = Path.Combine(Server.MapPath("~/Files/SurveyReport"), model.Archivo.FileName);
                model.Archivo.SaveAs(ruta);
                try
                {
                    List<String> lstError = new List<String>();
                    model.Archivo.InputStream.Position = 0;

                    List<String> lstRegistros = new List<String>();
                    List<Int32> lstCEfile = new List<Int32>();
                    List<Int32> lstCGfile = new List<Int32>();
                    List<String> lstOutcomes = new List<String>();

                    StreamReader objInput = new StreamReader(ruta, System.Text.Encoding.Default);

                    while (objInput.Peek() >= 0)
                    {
                        String registroLeido = objInput.ReadLine();
                        var registroModificado = Regex.Replace(registroLeido, "NA", "- ");
                        lstRegistros.Add(registroModificado);
                    }

                    String codigoAlumno = "";
                    String codigoCarrera = "";
                    String numeroPractica = "";
                    String CEvalues = "";
                    String CGvalues = "";
                    String Outcomes = "";
                    Int32 cantSuccess = 0;
                    Int32 cantFail = 0;
                    Int32 contRegistros = 0;
                    Int32 contRegistrosCE = 0;
                    Int32 contRegistrosCG = 0;
                    Int32 codCarrera = 0;

                    Carrera carrera;

                    foreach (string s in lstRegistros)
                    {
                        lstOutcomes.Clear();
                        contRegistrosCE = 0;
                        contRegistros++;
                        codigoAlumno = s.Substring(46, 9);
                        codigoCarrera = s.Substring(56, 1);
                        numeroPractica = s.Substring(57, 1);
                        Outcomes = s.Substring(58).TrimEnd();



                        if (codigoCarrera == "1")
                        {
                            codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("CC")).IdCarrera;
                            carrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("CC"));
                        }
                        else
                        {
                            if (codigoCarrera == "2")
                            {
                                codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SW")).IdCarrera;
                                carrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SW"));
                            }
                            else
                            {
                                if (codigoCarrera == "8")
                                {
                                    codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SI")).IdCarrera;
                                    carrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SI"));
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }

                        AlumnoMatriculado objAlumno = Context.AlumnoMatriculado.FirstOrDefault(x => x.Alumno.Codigo == codigoAlumno/* && x.IdPeriodoAcademico == model.CicloId*/);
                        if (objAlumno == null)
                        {
                            if (codigoAlumno == "")
                                continue;
                            cantFail++;
                            lstError.Add("( " + (contRegistros + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + codigoAlumno + "]");
                            continue;
                        }

                        Int32 contOutcomes = 0;
                        String outcomeAux = "0";

                        foreach (char outcome in Outcomes)
                        {
                            contOutcomes++;

                            if (contOutcomes % 2 != 0)
                            {
                                if (outcome.Equals(' '))
                                {
                                    lstOutcomes.Add(outcomeAux);
                                }
                                else
                                {
                                    lstOutcomes.Add(outcome.ToString());
                                }
                            }
                            else
                            {
                                if (outcome.Equals(' '))
                                {
                                    continue;
                                }
                                else
                                {
                                    lstOutcomes.Add(outcome.ToString());
                                }
                            }
                        }

                        for (int o = 0; o < lstOutcomes.Count(); o++)
                        {
                            if (lstOutcomes[o].Equals("-"))
                            {
                                lstOutcomes[o] = "-1";
                            }
                        }

                        var LstCE = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                        var LstCENoVisible = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == false).OrderBy(x => x.Orden).ToList();

                        var LstCG = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == objAlumno.Carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();

                        var LstPA = Context.PreguntaAdicional.OrderBy(x => x.Orden).ToList();

                        Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                        Encuesta encuesta = new Encuesta();
                        var TipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP);
                        encuesta.IdTipoEncuesta = TipoEncuesta.IdTipoEncuesta;
                        encuesta.CodigoAlumno = codigoAlumno;
                        encuesta.IdCarrera = codCarrera;
                        encuesta.IdNumeroPractica = ConvertHelpers.ToInteger(numeroPractica);
                        encuesta.IdSubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId;
                        encuesta.IdAlumno = Context.Alumno.FirstOrDefault(x => x.Codigo == codigoAlumno).IdAlumno;
                        encuesta.IdCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == codCarrera).IdCarrera;

                        for (int j = 0; j < LstCE.Count; j++)
                        {
                            var performance = new PerformanceEncuestaPPP();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaPPPConfig = LstCE[j].IdOutcomeEncuestaPPPConfig;
                            //performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j]);
                            /**/
                            if ((j) >= lstOutcomes.Count())
                            {
                                performance.PuntajeOutcome = 0;
                            }
                            else
                            {
                                performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j]);
                            }
                            /**/
                            Context.PerformanceEncuestaPPP.Add(performance);
                            if (performance.PuntajeOutcome < 0)
                                DicPerformance[LstCE[j].NombreEspanol] = 0;
                            else
                            {
                                //DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = ConvertHelpers.ToInteger(lstOutcomes[j]);
                                /**/
                                if ((j) >= lstOutcomes.Count())
                                {
                                    DicPerformance[LstCE[j].NombreEspanol] = 0;
                                }
                                else
                                {
                                    DicPerformance[LstCE[j].NombreEspanol] = ConvertHelpers.ToInteger(lstOutcomes[j]);
                                }
                                /**/
                            }
                        }
                        var logicCAC = new RegisterSurveyLogic();

                        foreach (var cac in LstCENoVisible)
                        {
                            var performance = new PerformanceEncuestaPPP();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaPPPConfig = cac.IdOutcomeEncuestaPPPConfig;
                            performance.PuntajeOutcome = logicCAC.ConvertirOutcome_DesdeEAC_ParaCAC(cac.NombreEspanol, DicPerformance);
                            if (performance.PuntajeOutcome < 0)
                                performance.PuntajeOutcome = -1;
                            Context.PerformanceEncuestaPPP.Add(performance);
                        }
                        for (int j = 0; j < LstCG.Count; j++)
                        {
                            var performance = new PerformanceEncuestaPPP();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaPPPConfig = LstCG[j].IdOutcomeEncuestaPPPConfig;
                            //performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j + 12]);
                            /**/
                            if ((j + 12) >= lstOutcomes.Count())
                            {
                                performance.PuntajeOutcome = 0;
                            }
                            else
                            {
                                performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j + 12]);
                            }
                            /**/
                            Context.PerformanceEncuestaPPP.Add(performance);
                        }
                        for (int j = 0; j < LstPA.Count; j++)
                        {
                            var performance = new PerformanceEncuestaPPP();
                            performance.Encuesta = encuesta;
                            performance.IdPreguntaAdicional = LstPA[j].IdPreguntaAdicional;
                            //performance.PuntajePregunta = lstOutcomes[j + 24];
                            if ((j + 24) >= lstOutcomes.Count())
                            {
                                performance.PuntajePregunta = "";
                            }
                            else
                            {
                                performance.PuntajePregunta = lstOutcomes[j + 24];
                            }

                            var valorPregunta = Context.PuntajePreguntaAdicional.FirstOrDefault(x => x.NombreEspanol == performance.PuntajePregunta);
                            performance.PuntajeOutcome = valorPregunta == null ? 0 : valorPregunta.valor;
                            Context.PerformanceEncuestaPPP.Add(performance);
                        }

                        Context.SaveChanges();
                        cantSuccess++;
                    }

                    if (System.IO.File.Exists(ruta))
                    {
                        objInput.Close();
                        System.IO.File.Delete(ruta);
                    }

                    data.CantSuccess = cantSuccess;
                    data.lstError = lstError;
                    //REVISAR DEBE ENVIAR PERIODOACADEMICO
                    await ActualizarSuperHallazgosPPP(Convert.ToInt32(model.CicloId));
                    var subModalidadPeriodoAcademico = await Context.SubModalidadPeriodoAcademico.FirstOrDefaultAsync(x => x.IdPeriodoAcademico == model.CicloId);
                    Context.Usp_CrearHallazgosPPPAutomaticos(subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico);
                    return Json(data);
                }
                catch (Exception ex)
                {
                    data.Error = true;
                    return Json(data);
                }

            }
            else
            {
                var data = new DataCarga();
                List<String> lstError = new List<String>();

                Int32 colCodigoEncuesta = 0;
                Int32 colCodigoAlumno = 1;
                Int32 colNumeroPractica = 2;
                Int32 colTotalHoras = 3;
                Int32 colRazonSocial = 4;
                Int32 colRUC = 5;
                Int32 colNombreJefe = 6;
                Int32 colCargoJefe = 7;
                Int32 colTelefonoJefe = 8;
                Int32 colEmailJefe = 9;
                Int32 colFechaInicio = 10;
                Int32 colFechaFin = 11;

                Int32 cantSuccess = 0;
                Int32 cantFail = 0;


                var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);
                TipoEncuesta tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP);
                for (int i = 1; i < dataTable.Rows.Count; i++)
                    {
                        try
                        {
                            var CodigoEncuesta = dataTable.Rows[i][colCodigoEncuesta].ToInteger();
                            var CodigoAlumno = dataTable.Rows[i][colCodigoAlumno].ToString();
                            Int32 NumeroPractica = dataTable.Rows[i][colNumeroPractica].ToInteger();
                            var TotalHoras = dataTable.Rows[i][colTotalHoras].ToInteger();
                            var RazonSocial = dataTable.Rows[i][colRazonSocial].ToString();
                            var RUC = dataTable.Rows[i][colRUC].ToString();
                            var NombreJefe = dataTable.Rows[i][colNombreJefe].ToString();
                            var CargoJefe = dataTable.Rows[i][colCargoJefe].ToString();
                            var TelefonoJefe = dataTable.Rows[i][colTelefonoJefe].ToString();
                            var EmailJefe = dataTable.Rows[i][colEmailJefe].ToString();

                            if (CodigoAlumno == "" || CodigoAlumno.Trim() == "" || CodigoAlumno == null)
                                continue;

                            AlumnoMatriculado objAlumno = Context.AlumnoMatriculado.OrderByDescending(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico).FirstOrDefault(x => x.Alumno.Codigo == CodigoAlumno && x.IdSubModalidadPeriodoAcademico == model.subModalidadPeriodoAcademicoId);
                            if (objAlumno == null)
                            {
                                cantFail++;
                                lstError.Add("( " + (i + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + CodigoAlumno + "]");
                                continue;
                            }

                            Int32 indice = 12;
                            
                            Carrera carrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == objAlumno.IdCarrera);

                            //VALIDAR SI ES MALLA ARIZONA, OBTENER LOS OUTCOMES DE LA A-K, PESE QUE SEAN CODIFICACIÓN 1-7
                            var LstCE = new List<OutcomeEncuestaPPPConfig>() ;
                            if (esMallaArizona)
                            {
                                LstCE = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && x.IdSubModalidadPeriodoAcademico==10 && x.SubModalidadPeriodoAcademico.EsMallaArizona == false && x.IdEscuela == objAlumno.Carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                            }
                            else
                            {
                                LstCE = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == objAlumno.Carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                            }
                            

                            var LstCENoVisible = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == objAlumno.Carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == false).OrderBy(x => x.Orden).ToList();

                            List<Int32> colCE = new List<Int32>();

                            for (int j = 0; j < LstCE.Count; j++)
                            {
                                colCE.Add(indice + j);
                            }
                            indice += LstCE.Count;

                            var LstCG = Context.OutcomeEncuestaPPPConfig.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdEscuela == objAlumno.Carrera.IdEscuela && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                            List<Int32> colCG = new List<Int32>();
                            for (int j = 0; j < LstCG.Count; j++)
                            {
                                colCG.Add(indice + j);
                            }
                            indice += LstCG.Count;
                            var LstPA = Context.PreguntaAdicional.OrderBy(x => x.Orden).ToList();
                            List<Int32> colPA = new List<Int32>();
                            for (int j = 0; j < LstPA.Count; j++)
                                colPA.Add(indice + j);
                            indice += colPA.Count;

                            Int32 colComentario = indice;



                        string fechainiciostring = dataTable.Rows[i][colFechaInicio].ToString();
                        string fechafinstring = dataTable.Rows[i][colFechaFin].ToString();
                        var FechaInicio = String.IsNullOrEmpty(dataTable.Rows[i][colFechaInicio].ToString()) ? (DateTime?)null : dataTable.Rows[i][colFechaInicio].ToDateTime();
                        
                        //var FechaInicio = String.IsNullOrEmpty(dataTable.Rows[i][colFechaInicio].ToString()) ? (DateTime?)null : DateTime.ParseExact(dataTable.Rows[i][colFechaInicio].ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture) ;
                        //var FechaFin = String.IsNullOrEmpty(dataTable.Rows[i][colFechaFin].ToString()) ? (DateTime?)null : DateTime.ParseExact(dataTable.Rows[i][colFechaFin].ToString(), "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                        var FechaFin = String.IsNullOrEmpty(dataTable.Rows[i][colFechaFin].ToString()) ? (DateTime?)null : dataTable.Rows[i][colFechaFin].ToDateTime();
                        System.Diagnostics.Debug.WriteLine((i+1)+" : " +FechaInicio.ToString()+" a "+ FechaFin.ToString());
                        var CE = new List<Int32>();
                            for (int j = 0; j < LstCE.Count; j++)
                            {
                                var valor = dataTable.Rows[i][colCE[j]].ToString();
                                if (esMallaArizona)
                                {
                                    CE.Add((valor.Contains("NA") ? -1 : valor.ToInteger()));
                                }
                                else
                                {
                                    CE.Add((valor.Contains("NA") ? -1 : valor.ToInteger()));
                                }
                                
                            }
                            var CG = new List<Int32>();
                            for (int j = 0; j < LstCG.Count; j++)
                            {
                                var valor = dataTable.Rows[i][colCG[j]].ToString();
                                CG.Add((valor.Contains("NA") ? -1 : valor.ToInteger()));
                            }

                            var PA = new List<String>();
                            for (int j = 0; j < LstPA.Count; j++)
                            {
                                var pa = dataTable.Rows[i][colPA[j]].ToString();
                                if (!String.IsNullOrEmpty(pa))
                                    PA.Add(pa);
                            }



                            var Comentario = dataTable.Rows[i][colComentario].ToSafeString().ToUpper();

                        var alumnoLlenandoEncuesta = Context.Encuesta.Where(x => x.CodigoAlumno == CodigoAlumno && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO);
                        if (alumnoLlenandoEncuesta == null)
                        {
                            NumeroPractica = 1;
                        }
                        else
                        {
                            var numeroEncuestaMaximo = alumnoLlenandoEncuesta.Max(x => x.IdNumeroPractica);
                            switch (numeroEncuestaMaximo)
                            {
                                case 1:
                                    if (NumeroPractica == 1)
                                    {
                                        cantFail++;
                                        lstError.Add("( " + (i + 1) + " ) " + ": El alumno, cuyo código es [" + CodigoAlumno + "] ya cuenta con el primer informe registrado.");
                                        continue;
                                    }
                                    break;
                                case 2:
                                    cantFail++;
                                    lstError.Add("( " + (i + 1) + " ) " + ": El alumno, cuyo código es [" + CodigoAlumno + "] ya cuenta con todos sus informes registrados.");
                                    continue;
                            }
                        }
                        var practica = Context.NumeroPractica.FirstOrDefault(x => x.numero == NumeroPractica);

                            if (//TotalHoras > 0 
                                //&& 
                                objAlumno != null
                                && String.IsNullOrEmpty(CodigoAlumno) == false
                                && NumeroPractica > 0
                                //&& validarPA 
                                //&& FechaFin != null 
                                //&& FechaInicio != null
                                //&& String.IsNullOrEmpty(NombreJefe) == false
                                && practica != null)
                            {

                                Encuesta encuesta = new Encuesta();

                                if (CodigoEncuesta > 0)
                                    encuesta.CodigoEncuesta = CodigoEncuesta;
                                encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                                encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

                                encuesta.IdCarrera = objAlumno.IdCarrera;
                                encuesta.IdSede = objAlumno.IdSede;
                                encuesta.IdAlumno = objAlumno.Alumno.IdAlumno;

                                encuesta.IdSubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId;
                                encuesta.CodigoAlumno = CodigoAlumno;
                                encuesta.FechaFin = FechaFin;
                                encuesta.FechaInicio = FechaInicio;
                                encuesta.TotalHoras = TotalHoras;

                                encuesta.NumeroPractica = practica;
                                encuesta.RazonSocial = RazonSocial;
                                encuesta.NombreJefe = NombreJefe;
                                encuesta.CargoJefe = CargoJefe;
                                encuesta.TelefonoJefe = TelefonoJefe;
                                encuesta.CorreoJefe = EmailJefe;
                                encuesta.RUC = RUC;
                                if (String.IsNullOrEmpty(Comentario) == false)
                                    encuesta.Comentario = Comentario;

                                Context.Encuesta.Add(encuesta);
                            Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();

                            if (esMallaArizona)
                            {
                                List<decimal> listaPuntajeOutcomes = new List<decimal>();
                                var logicObtenerPuntajesArizona = new RegisterSurveyLogic();
                                for (int j = 0; j < CE.Count; j++)
                                {

                                    listaPuntajeOutcomes.Add((decimal)CE[j]);
                                }
                                List<Usp_Ins_RegistarPerformancePPP_Result> listaNuevosPuntajesArizona = logicObtenerPuntajesArizona.ObtenerNuevosPuntajesArizona(Context, listaPuntajeOutcomes, model.subModalidadPeriodoAcademicoId.Value, objAlumno.IdCarrera.Value);
                                for (int k = 0; k < listaNuevosPuntajesArizona.Count; k++)
                                {
                                    var idoutcomeencuestapppconfig = listaNuevosPuntajesArizona[k].id;
                                    var performance = new PerformanceEncuestaPPP();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaPPPConfig = idoutcomeencuestapppconfig;
                                    performance.PuntajeOutcome = listaNuevosPuntajesArizona[k].puntaje.Value;
                                    Context.PerformanceEncuestaPPP.Add(performance);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < LstCE.Count; j++)
                                {
                                    if (CE.Count > 0 && j < CE.Count)
                                    {

                                        var idoutcomeencuestapppconfig = LstCE[j].IdOutcomeEncuestaPPPConfig;
                                        var performance = new PerformanceEncuestaPPP();
                                        performance.Encuesta = encuesta;
                                        performance.IdOutcomeEncuestaPPPConfig = idoutcomeencuestapppconfig;

                                        performance.PuntajeOutcome = CE[j];

                                        Int32? IdCarreraPregunta = Context.OutcomeEncuestaPPPConfig.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == idoutcomeencuestapppconfig).IdCarrera;

                                        if (IdCarreraPregunta.HasValue)
                                        {
                                            if (IdCarreraPregunta != carrera.IdCarrera)
                                            {
                                                performance.PuntajeOutcome = -1;
                                            }

                                        }
                                        Context.PerformanceEncuestaPPP.Add(performance);



                                        DicPerformance[LstCE[j].NombreEspanol] = CE[j];


                                    }
                                }
                            }


                            var logicCAC = new RegisterSurveyLogic();

                            foreach (var cac in LstCENoVisible)
                            {
                                var performance = new PerformanceEncuestaPPP();
                                performance.Encuesta = encuesta;
                                performance.IdOutcomeEncuestaPPPConfig = cac.IdOutcomeEncuestaPPPConfig;
                                performance.PuntajeOutcome = logicCAC.ConvertirOutcome_DesdeEAC_ParaCAC(cac.NombreEspanol, DicPerformance);
                                if (performance.PuntajeOutcome < 0)
                                    performance.PuntajeOutcome = -1;
                                Context.PerformanceEncuestaPPP.Add(performance);
                            }
                            for (int j = 0; j < LstCG.Count; j++)
                            {
                                if (CG.Count > 0 && j < CG.Count)
                                {
                                    var performance = new PerformanceEncuestaPPP();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaPPPConfig = LstCG[j].IdOutcomeEncuestaPPPConfig;
                                    performance.PuntajeOutcome = CG[j];
                                    Context.PerformanceEncuestaPPP.Add(performance);
                                }

                            }
                            for (int j = 0; j < LstPA.Count; j++)
                            {
                                if (PA.Count > 0 && j < PA.Count)
                                {
                                    var performance = new PerformanceEncuestaPPP();
                                    performance.Encuesta = encuesta;
                                    performance.IdPreguntaAdicional = LstPA[j].IdPreguntaAdicional;
                                    performance.PuntajePregunta = PA[j];
                                    var valorPregunta = Context.PuntajePreguntaAdicional.FirstOrDefault(x => x.NombreEspanol == performance.PuntajePregunta);
                                    performance.PuntajeOutcome = valorPregunta == null ? 0 : valorPregunta.valor;
                                    Context.PerformanceEncuestaPPP.Add(performance);
                                }
                            }
                            cantSuccess++;
                            }

                            else
                            {
                                if (CodigoAlumno == "")
                                    continue;

                                cantFail++;
                                if (objAlumno == null)
                                    lstError.Add("( " + (i + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + CodigoAlumno + "].");
                                else if (String.IsNullOrEmpty(RazonSocial))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar razón social [" + CodigoAlumno + "].");
                                else if (String.IsNullOrEmpty(NombreJefe))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar nombre de jefe [" + CodigoAlumno + "].");
                                else if (String.IsNullOrEmpty(EmailJefe))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar email de jefe [" + CodigoAlumno + "].");
                                else if (practica == null)
                                    lstError.Add("( " + (i + 1) + " ) " + ": Número de práctica inválido [" + CodigoAlumno + "].");
                            }

                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine("error: " + ex);
                            data.Error = true;
                            return Json(data);
                        }
                    }

                try
                {
                    try
                    {
                        Context.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        Exception raise = dbEx;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                // raise a new exception nesting
                                // the current instance as InnerException
                                raise = new InvalidOperationException(message, raise);
                            }
                        }
                        throw raise;
                    }
                    data.CantSuccess = cantSuccess;
                    data.lstError = lstError;

                    string erroLista ="";
                    foreach (string a in data.lstError)
                    {
                        System.Diagnostics.Debug.WriteLine(a);
                    }
                    //DEBE ENVIAR EL PERIODO ACADEMICO
                    await ActualizarSuperHallazgosPPP(Convert.ToInt32(model.CicloId));
                    //var subModalidadPeriodoAcademico = await Context.SubModalidadPeriodoAcademico.FirstOrDefaultAsync(x => x.IdPeriodoAcademico == model.CicloId);
                    if(lstError.Count() == 0)
                        Context.Usp_CrearHallazgosPPPAutomaticos(model.subModalidadPeriodoAcademicoId);
                    return Json(data);
                }
                catch (DbUpdateException ex)
                {
                    System.Diagnostics.Debug.WriteLine("error: " + ex);
                    data.Error = true;
                    return Json(data);
                }
            }

        }


        [HttpPost]
        public async Task<JsonResult> UploadPPP(UploadPPPViewModel model)
        {
            String fileExtension = System.IO.Path.GetExtension(model.Archivo.FileName);

            if (fileExtension == ".dat")
            {
                var data = new DataCarga();
                var ruta = Path.Combine(Server.MapPath("~/Files/SurveyReport"), model.Archivo.FileName);
                model.Archivo.SaveAs(ruta);
                try
                {
                    List<String> lstError = new List<String>();
                    model.Archivo.InputStream.Position = 0;

                    List<String> lstRegistros = new List<String>();
                    List<Int32> lstCEfile = new List<Int32>();
                    List<Int32> lstCGfile = new List<Int32>();
                    List<String> lstOutcomes = new List<String>();

                    StreamReader objInput = new StreamReader(ruta, System.Text.Encoding.Default);

                    while (objInput.Peek() >= 0)
                    {
                        String registroLeido = objInput.ReadLine();
                        var registroModificado = Regex.Replace(registroLeido, "NA", "- ");
                        lstRegistros.Add(registroModificado);
                    }

                    String codigoAlumno = "";
                    String codigoCarrera = "";
                    String numeroPractica = "";
                    String CEvalues = "";
                    String CGvalues = "";
                    String Outcomes = "";
                    Int32 cantSuccess = 0;
                    Int32 cantFail = 0;
                    Int32 contRegistros = 0;
                    Int32 contRegistrosCE = 0;
                    Int32 contRegistrosCG = 0;
                    Int32 codCarrera = 0;

                    foreach (string s in lstRegistros)
                    {
                        lstOutcomes.Clear();
                        contRegistrosCE = 0;
                        contRegistros++;
                        codigoAlumno = s.Substring(46, 9);
                        codigoCarrera = s.Substring(56, 1);
                        numeroPractica = s.Substring(57, 1);
                        Outcomes = s.Substring(58).TrimEnd();

                        if (codigoCarrera == "1")
                        {
                            codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("CC")).IdCarrera;
                        }
                        else
                        {
                            if (codigoCarrera == "2")
                            {
                                codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SW")).IdCarrera;
                            }
                            else
                            {
                                if (codigoCarrera == "8")
                                {
                                    codCarrera = Context.Carrera.FirstOrDefault(x => x.Codigo.Equals("SI")).IdCarrera;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }

                        AlumnoMatriculado objAlumno = Context.AlumnoMatriculado.FirstOrDefault(x => x.Alumno.Codigo == codigoAlumno/* && x.IdPeriodoAcademico == model.CicloId*/);
                        if (objAlumno == null)
                        {
                            cantFail++;
                            lstError.Add("( " + (contRegistros + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + codigoAlumno + "]");
                            continue;
                        }

                        Int32 contOutcomes = 0;
                        String outcomeAux = "0";

                        foreach (char outcome in Outcomes)
                        {
                            contOutcomes++;

                            if (contOutcomes % 2 != 0)
                            {
                                if (outcome.Equals(' '))
                                {
                                    lstOutcomes.Add(outcomeAux);
                                }
                                else
                                {
                                    lstOutcomes.Add(outcome.ToString());
                                }
                            }
                            else
                            {
                                if (outcome.Equals(' '))
                                {
                                    continue;
                                }
                                else
                                {
                                    lstOutcomes.Add(outcome.ToString());
                                }
                            }
                        }

                        for (int o = 0; o < lstOutcomes.Count(); o++)
                        {
                            if (lstOutcomes[o].Equals("-"))
                            {
                                lstOutcomes[o] = "-1";
                            }
                        }

                        var LstCE = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                                    && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == codCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                        var LstCENoVisible = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                            && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == codCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == false).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();

                        var LstCG = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                                    && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == codCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();

                        var LstPA = Context.PreguntaAdicional.OrderBy(x => x.Orden).ToList();

                        Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                        Encuesta encuesta = new Encuesta();
                        var TipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP);
                        encuesta.IdTipoEncuesta = TipoEncuesta.IdTipoEncuesta;
                        encuesta.CodigoAlumno = codigoAlumno;
                        encuesta.IdCarrera = codCarrera;
                        encuesta.IdNumeroPractica = ConvertHelpers.ToInteger(numeroPractica);
                        encuesta.IdSubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId;
                        encuesta.IdAlumno = Context.Alumno.FirstOrDefault(x => x.Codigo == codigoAlumno).IdAlumno;
                        encuesta.IdCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == codCarrera).IdCarrera;

                        for (int j = 0; j < LstCE.Count; j++)
                        {
                            var performance = new PerformanceEncuesta();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaConfig = LstCE[j].IdOutcomeEncuestaConfig;
                            //performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j]);
                            /**/
                            if ((j) >= lstOutcomes.Count())
                            {
                                performance.PuntajeOutcome = 0;
                            }
                            else
                            {
                                performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j]);
                            }
                            /**/
                            Context.PerformanceEncuesta.Add(performance);
                            if (performance.PuntajeOutcome < 0)
                                DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = 0;
                            else
                            {
                                //DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = ConvertHelpers.ToInteger(lstOutcomes[j]);
                                /**/
                                if ((j) >= lstOutcomes.Count())
                                {
                                    DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = 0;
                                }
                                else
                                {
                                    DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = ConvertHelpers.ToInteger(lstOutcomes[j]);
                                }
                                /**/
                            }
                        }
                        var logicCAC = new RegisterSurveyLogic();

                        foreach (var cac in LstCENoVisible)
                        {
                            var performance = new PerformanceEncuesta();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaConfig = cac.IdOutcomeEncuestaConfig;
                            performance.PuntajeOutcome = logicCAC.ConvertirOutcome_DesdeEAC_ParaCAC(cac.NombreEspanol, DicPerformance);
                            if (performance.PuntajeOutcome < 0)
                                performance.PuntajeOutcome = -1;
                            Context.PerformanceEncuesta.Add(performance);
                        }
                        for (int j = 0; j < LstCG.Count; j++)
                        {
                            var performance = new PerformanceEncuesta();
                            performance.Encuesta = encuesta;
                            performance.IdOutcomeEncuestaConfig = LstCG[j].IdOutcomeEncuestaConfig;
                            //performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j + 12]);
                            /**/
                            if ((j + 12) >= lstOutcomes.Count())
                            {
                                performance.PuntajeOutcome = 0;
                            }
                            else
                            {
                                performance.PuntajeOutcome = ConvertHelpers.ToDecimal(lstOutcomes[j + 12]);
                            }
                            /**/
                            Context.PerformanceEncuesta.Add(performance);
                        }
                        for (int j = 0; j < LstPA.Count; j++)
                        {
                            var performance = new PerformanceEncuesta();
                            performance.Encuesta = encuesta;
                            performance.IdPreguntaAdicional = LstPA[j].IdPreguntaAdicional;
                            //performance.PuntajePregunta = lstOutcomes[j + 24];
                            if ((j + 24) >= lstOutcomes.Count())
                            {
                                performance.PuntajePregunta = "";
                            }
                            else
                            {
                                performance.PuntajePregunta = lstOutcomes[j + 24];
                            }

                            var valorPregunta = Context.PuntajePreguntaAdicional.FirstOrDefault(x => x.NombreEspanol == performance.PuntajePregunta);
                            performance.PuntajeOutcome = valorPregunta == null ? 0 : valorPregunta.valor;
                            Context.PerformanceEncuesta.Add(performance);
                        }

                        Context.SaveChanges();
                        cantSuccess++;
                    }

                    if (System.IO.File.Exists(ruta))
                    {
                        objInput.Close();
                        System.IO.File.Delete(ruta);
                    }

                    data.CantSuccess = cantSuccess;
                    data.lstError = lstError;
                    return Json(data);
                }
                catch (Exception ex)
                {
                    data.Error = true;
                    return Json(data);
                }
            }

            else
            {
                var data = new DataCarga();
                List<String> lstError = new List<String>();
                try
                {

                    Int32 colCodigoEncuesta = 0;
                    Int32 colCodigoAlumno = 1;
                    Int32 colNumeroPractica = 2;
                    Int32 colTotalHoras = 3;
                    Int32 colRazonSocial = 4;
                    Int32 colRUC = 5;
                    Int32 colNombreJefe = 6;
                    Int32 colCargoJefe = 7;
                    Int32 colTelefonoJefe = 8;
                    Int32 colEmailJefe = 9;
                    Int32 colFechaInicio = 10;
                    Int32 colFechaFin = 11;


                    Int32 cantSuccess = 0;
                    Int32 cantFail = 0;

                    var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);
                    TipoEncuesta tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP);

                    for (int i = 1; i < dataTable.Rows.Count; i++)
                    {
                        try
                        {
                            var CodigoEncuesta = dataTable.Rows[i][colCodigoEncuesta].ToInteger();
                            var CodigoAlumno = dataTable.Rows[i][colCodigoAlumno].ToString();
                            Int32 NumeroPractica = dataTable.Rows[i][colNumeroPractica].ToInteger();
                            var TotalHoras = dataTable.Rows[i][colTotalHoras].ToInteger();
                            var RazonSocial = dataTable.Rows[i][colRazonSocial].ToString();
                            var RUC = dataTable.Rows[i][colRUC].ToString();
                            var NombreJefe = dataTable.Rows[i][colNombreJefe].ToString();
                            var CargoJefe = dataTable.Rows[i][colCargoJefe].ToString();
                            var TelefonoJefe = dataTable.Rows[i][colTelefonoJefe].ToString();
                            var EmailJefe = dataTable.Rows[i][colEmailJefe].ToString();

                            // if (CodigoAlumno == "")
                            //      continue;

                            AlumnoMatriculado objAlumno = Context.AlumnoMatriculado.OrderByDescending(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico).FirstOrDefault(x => x.Alumno.Codigo == CodigoAlumno);
                            if (objAlumno == null)
                            {
                                cantFail++;
                                lstError.Add("( " + (i + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + CodigoAlumno + "]");
                                continue;
                            }

                            Int32 indice = 12;

                            var LstCE = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                                && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == objAlumno.IdCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                            var LstCENoVisible = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                                && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == objAlumno.IdCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == false).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                            List<Int32> colCE = new List<Int32>();
                            for (int j = 0; j < LstCE.Count; j++)
                            {
                                colCE.Add(indice + j);
                            }
                            indice += LstCE.Count;
                            var LstCG = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
                                && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == objAlumno.IdCarrera && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                            List<Int32> colCG = new List<Int32>();
                            for (int j = 0; j < LstCG.Count; j++)
                            {
                                colCG.Add(indice + j);
                            }
                            indice += LstCG.Count;
                            var LstPA = Context.PreguntaAdicional.OrderBy(x => x.Orden).ToList();
                            List<Int32> colPA = new List<Int32>();
                            for (int j = 0; j < LstPA.Count; j++)
                                colPA.Add(indice + j);
                            indice += colPA.Count;

                            Int32 colComentario = indice;

                            //

                            var FechaInicio = String.IsNullOrEmpty(dataTable.Rows[i][colFechaInicio].ToString()) ? (DateTime?)null : dataTable.Rows[i][colFechaInicio].ToDateTime();
                            var FechaFin = String.IsNullOrEmpty(dataTable.Rows[i][colFechaFin].ToString()) ? (DateTime?)null : dataTable.Rows[i][colFechaFin].ToDateTime();
                            var CE = new List<Int32>();
                            for (int j = 0; j < LstCE.Count; j++)
                            {
                                var valor = dataTable.Rows[i][colCE[j]].ToString();
                                CE.Add((valor.Contains("NA") ? -1 : valor.ToInteger()));
                            }
                            var CG = new List<Int32>();
                            for (int j = 0; j < LstCG.Count; j++)
                            {
                                var valor = dataTable.Rows[i][colCG[j]].ToString();
                                CG.Add((valor.Contains("NA") ? -1 : valor.ToInteger()));
                            }
                            //bool validarPA = true;
                            var PA = new List<String>();
                            for (int j = 0; j < LstPA.Count; j++)
                            {
                                var pa = dataTable.Rows[i][colPA[j]].ToString();
                                if (!String.IsNullOrEmpty(pa))
                                    PA.Add(pa);
                                //if (String.IsNullOrEmpty(pa)) validarPA = false;
                            }
                            var Comentario = dataTable.Rows[i][colComentario].ToSafeString().ToUpper();

                            var practica = Context.NumeroPractica.FirstOrDefault(x => x.numero == NumeroPractica);

                            if (//TotalHoras > 0 
                                //&& 
                                objAlumno != null
                                && String.IsNullOrEmpty(CodigoAlumno) == false
                                && NumeroPractica > 0
                                //&& validarPA 
                                //&& FechaFin != null 
                                //&& FechaInicio != null
                                && String.IsNullOrEmpty(NombreJefe) == false
                                && practica != null)
                            {
                                Encuesta encuesta = new Encuesta();

                                if (CodigoEncuesta > 0)
                                    encuesta.CodigoEncuesta = CodigoEncuesta;
                                encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                                encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

                                encuesta.IdCarrera = objAlumno.IdCarrera;
                                encuesta.IdSede = objAlumno.IdSede;
                                encuesta.IdAlumno = objAlumno.Alumno.IdAlumno;

                                encuesta.IdSubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId;
                                encuesta.CodigoAlumno = CodigoAlumno;
                                encuesta.FechaFin = FechaFin;
                                encuesta.FechaInicio = FechaInicio;
                                encuesta.TotalHoras = TotalHoras;

                                encuesta.NumeroPractica = practica;
                                encuesta.RazonSocial = RazonSocial;
                                encuesta.NombreJefe = NombreJefe;
                                encuesta.CargoJefe = CargoJefe;
                                encuesta.TelefonoJefe = TelefonoJefe;
                                encuesta.CorreoJefe = EmailJefe;
                                encuesta.RUC = RUC;
                                if (String.IsNullOrEmpty(Comentario) == false)
                                    encuesta.Comentario = Comentario;

                                Context.Encuesta.Add(encuesta);
                                Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();

                                for (int j = 0; j < LstCE.Count; j++)
                                {
                                    if (CE.Count > 0 && j < CE.Count)
                                    {
                                        var performance = new PerformanceEncuesta();
                                        performance.Encuesta = encuesta;
                                        performance.IdOutcomeEncuestaConfig = LstCE[j].IdOutcomeEncuestaConfig;

                                        performance.PuntajeOutcome = CE[j];
                                        Context.PerformanceEncuesta.Add(performance);
                                        //if (performance.PuntajeOutcome < 0)
                                        //    DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = 0;
                                        //else
                                        DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = CE[j];
                                    }
                                }
                                var logicCAC = new RegisterSurveyLogic();

                                foreach (var cac in LstCENoVisible)
                                {
                                    var performance = new PerformanceEncuesta();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaConfig = cac.IdOutcomeEncuestaConfig;
                                    performance.PuntajeOutcome = logicCAC.ConvertirOutcome_DesdeEAC_ParaCAC(cac.NombreEspanol, DicPerformance);
                                    if (performance.PuntajeOutcome < 0)
                                        performance.PuntajeOutcome = -1;
                                    Context.PerformanceEncuesta.Add(performance);
                                }
                                for (int j = 0; j < LstCG.Count; j++)
                                {
                                    if (CG.Count > 0 && j < CG.Count)
                                    {
                                        var performance = new PerformanceEncuesta();
                                        performance.Encuesta = encuesta;
                                        performance.IdOutcomeEncuestaConfig = LstCG[j].IdOutcomeEncuestaConfig;
                                        performance.PuntajeOutcome = CG[j];
                                        Context.PerformanceEncuesta.Add(performance);
                                    }

                                }
                                for (int j = 0; j < LstPA.Count; j++)
                                {
                                    if (PA.Count > 0 && j < PA.Count)
                                    {
                                        var performance = new PerformanceEncuesta();
                                        performance.Encuesta = encuesta;
                                        performance.IdPreguntaAdicional = LstPA[j].IdPreguntaAdicional;
                                        performance.PuntajePregunta = PA[j];
                                        var valorPregunta = Context.PuntajePreguntaAdicional.FirstOrDefault(x => x.NombreEspanol == performance.PuntajePregunta);
                                        performance.PuntajeOutcome = valorPregunta == null ? 0 : valorPregunta.valor;
                                        Context.PerformanceEncuesta.Add(performance);
                                    }
                                }
                                cantSuccess++;
                            }
                            else
                            {
                                cantFail++;
                                if (objAlumno == null)
                                    lstError.Add("( " + (i + 1) + " ) " + ": No existe registro de alumno con el código ingresado [" + CodigoAlumno + "]");
                                else if (String.IsNullOrEmpty(RazonSocial))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar razón social");
                                else if (String.IsNullOrEmpty(NombreJefe))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar nombre de jefe");
                                else if (String.IsNullOrEmpty(EmailJefe))
                                    lstError.Add("( " + (i + 1) + " ) " + ": Debe de ingresar email de jefe");
                                else if (practica == null)
                                    lstError.Add("( " + (i + 1) + " ) " + ": Número de práctica inválido");

                            }
                        }
                        catch (Exception ex)
                        {
                            data.Error = true;
                            return Json(data);
                        }
                    }
                    Context.SaveChanges();
                    data.CantSuccess = cantSuccess;
                    data.lstError = lstError;
                    return Json(data);
                }
                catch (Exception ex)
                {
                    data.Error = true;
                    return Json(data);
                }
            }
        }

        public ActionResult UploadFDC()
        {
            var model = new UploadFDCViewModel();
            model.Fill(CargarDatosContext());
            return View(model);
        }
        [HttpPost]
        public JsonResult UploadFDC(UploadFDCViewModel model)
        {
            var data = new DataCarga();

            List<String> lstError = new List<String>();
            try
            {

                Int32 colSeccion = 0;
                Int32 colCurso = 1;
                List<Int32> colPuntaje = new List<Int32>();
                for (int i = 0; i < ConstantHelpers.ENCUESTA.ESCALA_FDC; i++)
                    colPuntaje.Add(2 + i);

                Int32 colCodigoEncuesta = 14;
                Int32 colSeccionC = 15;
                Int32 colCursoC = 16;
                Int32 colComentario = 17;
                Int32 cantSuccess = 0;
                Int32 cantFail = 0;
                String CodCursoActual = "";
                var tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC);
                var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                Int32 iHallazgo = 1;

                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    try
                    {
                        var lstPuntaje = new List<Int32>();
                        var eSeccion = dataTable.Rows[i][colSeccion].ToString().ToUpper();
                        var eCurso = dataTable.Rows[i][colCurso].ToString().ToUpper();

                        for (int j = 0; j < ConstantHelpers.ENCUESTA.ESCALA_FDC; j++)
                            lstPuntaje.Add(dataTable.Rows[i][colPuntaje[j]].ToInteger());

                        Decimal cantidad = 0;
                        ResultadoFDC resultado = null;

                        var objCurso = Context.spGetCursosControlEncuesta(null, model.SubModalidadPeriodoAcademicoId, 3, eCurso).FirstOrDefault();

                        Seccion objSeccion = Context.Seccion.Include(x => x.Sede).FirstOrDefault(x => x.Codigo == eSeccion);


                        var SeccionCurso = Context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", eSeccion, eCurso, model.SubModalidadPeriodoAcademicoId, null).FirstOrDefault();
                        if (objSeccion != null && objCurso != null && SeccionCurso != null)
                        {
                            if (i == 1)
                                CodCursoActual = objCurso.Codigo;

                            var objUltimoHallazgo = Context.Hallazgo.Where(x => x.Encuesta.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC && x.IdCurso == objCurso.IdCurso).OrderByDescending(x => x.IdHallazgo).FirstOrDefault();
                            var codigoUltimoHallazgo = objUltimoHallazgo != null ? objUltimoHallazgo.Codigo : "";

                            if (!String.IsNullOrEmpty(codigoUltimoHallazgo))
                            {
                                iHallazgo = Convert.ToInt32(codigoUltimoHallazgo.Substring(codigoUltimoHallazgo.IndexOf("F-") + 2, codigoUltimoHallazgo.Length - codigoUltimoHallazgo.IndexOf("F-") - 2)) + 1;
                            }
                            else
                            {
                                if (CodCursoActual != objCurso.Codigo && i > 1)
                                {
                                    iHallazgo = 1;
                                    CodCursoActual = objCurso.Codigo;
                                }
                            }
                            Encuesta encuesta = Context.Encuesta.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.SubModalidadPeriodoAcademicoId
                               && x.IdCurso == SeccionCurso.IdCurso && x.IdSeccion == SeccionCurso.IdSeccion
                               && x.IdSede == SeccionCurso.IdSede);
                            //var numeroAlumnos = 0;
                            if (encuesta != null)
                            {
                                // numeroAlumnos = context.ResultadoFDC.Where(x => x.IdEncuesta == encuesta.IdEncuesta).Sum(x => x.CantidadRespuesta);
                                cantidad = encuesta.Encuestados.Value;
                                encuesta.PuntajeTotal = encuesta.PuntajeTotal * cantidad;
                            }
                            else
                            {
                                encuesta = new Encuesta();
                                encuesta.PuntajeTotal = 0;
                                encuesta.Encuestados = 0;
                                Context.Encuesta.Add(encuesta);
                            }
                            encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                            encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

                            encuesta.IdCurso = SeccionCurso.IdCurso;
                            encuesta.IdSeccion = SeccionCurso.IdSeccion;
                            encuesta.IdSubModalidadPeriodoAcademico = model.SubModalidadPeriodoAcademicoId;
                            encuesta.IdSede = SeccionCurso.IdSede;

                            encuesta.IdEstado = Context.EstadoEncuesta.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.ESTADO_DEVUELTO).IdEstado;


                            for (int j = 0; j < ConstantHelpers.ENCUESTA.ESCALA_FDC; j++)
                            {
                                resultado = new ResultadoFDC();
                                Context.ResultadoFDC.Add(resultado);
                                resultado.Puntaje = j + 1;
                                resultado.CantidadRespuesta = lstPuntaje[j];
                                cantidad = cantidad + lstPuntaje[j];

                                encuesta.PuntajeTotal = encuesta.PuntajeTotal + (resultado.Puntaje * resultado.CantidadRespuesta);

                                resultado.Encuesta = encuesta;
                            }
                            encuesta.Encuestados = (cantidad /*+ numeroAlumnos*/).ToInteger();
                            encuesta.PuntajeTotal = encuesta.PuntajeTotal / (cantidad/* + numeroAlumnos*/);

                            if ((Double)encuesta.PuntajeTotal < ConstantHelpers.ENCUESTA.FDC_ROJO)
                            {
                                var lstoutcomes = Context.uspGetOutcomesCurso(objCurso.IdCurso, model.SubModalidadPeriodoAcademicoId).ToList();
                                if (lstoutcomes.Count > 0)
                                {
                                    var idnivelaceptacion = Context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == "Necesita mejora").IdNivelAceptacionHallazgo;
                                    var cicloacademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == model.SubModalidadPeriodoAcademicoId).PeriodoAcademico.CicloAcademico;
                                    var codigosede = Context.Sede.FirstOrDefault(x => x.IdSede == SeccionCurso.IdSede).Codigo;

                                    var lcfcInstrumento = Context.Instrumento.FirstOrDefault(x => x.Acronimo == "LCFC");

                                    var IdInstrumento = lcfcInstrumento == null ? 1 : lcfcInstrumento.IdInstrumento;
                                    var AcronimoInstrumento = lcfcInstrumento == null ? "" : lcfcInstrumento.Acronimo;

                                    List<MallaCurricular> Mallas = Context.MallaCurricularPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == model.SubModalidadPeriodoAcademicoId).Select(x => x.MallaCurricular).ToList();
                                    foreach (var outcomeComision in lstoutcomes)
                                    {
                                        foreach (var malla in Mallas)
                                        {
                                            var CursoMallaCurricular = Context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso && x.IdMallaCurricular == malla.IdMallaCurricular);
                                            var IdComisionHallazgo = Context.CarreraComision.FirstOrDefault(x => x.IdCarrera == malla.IdCarrera && x.IdSubModalidadPeriodoAcademico == model.SubModalidadPeriodoAcademicoId && x.Comision.Codigo != ConstantHelpers.COMISON_WASC).IdComision;
                                            var NombreOutcome = outcomeComision[0].ToString();
                                            var IdOutcomeHallazgo = Context.OutcomeComision.FirstOrDefault(x => x.IdComision == IdComisionHallazgo && x.IdSubModalidadPeriodoAcademico == model.SubModalidadPeriodoAcademicoId && x.Outcome.Nombre == NombreOutcome).IdOutcome;
                                            var hallazgo = new Hallazgo();

                                            hallazgo.Codigo = String.Format("{0}-{1}-{2}-{3}-F-{4}", AcronimoInstrumento, objCurso.Codigo, cicloacademico, codigosede, iHallazgo.ToString("D3"));
                                            hallazgo.DescripcionEspanol = String.Format("La sección {0}, presenta un puntaje de: {1}", objSeccion.Codigo, Math.Round((Double)encuesta.PuntajeTotal * 1.0, 2));
                                            hallazgo.DescripcionIngles = String.Format("The classroom {0}, has a score : {1}", objSeccion.Codigo, Math.Round((Double)encuesta.PuntajeTotal * 1.0, 2));
                                            hallazgo.IdSubModalidadPeriodoAcademico = model.SubModalidadPeriodoAcademicoId;
                                            hallazgo.IdCurso = objCurso.IdCurso;
                                            hallazgo.Encuesta = encuesta;
                                            hallazgo.IdComision = IdComisionHallazgo;
                                            hallazgo.FechaRegistro = DateTime.Now;
                                            hallazgo.IdSede = encuesta.IdSede;
                                            hallazgo.IdCarrera = malla.IdCarrera;
                                            hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                            hallazgo.Generado = ConstantHelpers.HALLAZGO.AUTO;
                                            hallazgo.IdNivelAceptacionHallazgo = idnivelaceptacion;
                                            hallazgo.IdCriterioHallazgoEncuesta = Context.CriterioHallazgoEncuesta.FirstOrDefault(x => x.NombreEspanol == "Carrera").IdCriterioHallazgoEncuesta;
                                            hallazgo.IdOutcome = IdOutcomeHallazgo;
                                            hallazgo.IdInstrumento = IdInstrumento;
                                            hallazgo.IdTipoEncuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
                                            hallazgo.IdConstituyente = Context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;
                                            Context.Hallazgo.Add(hallazgo);
                                        }
                                    }
                                    //var CursoPeriodoAcademico = context.CursoPeriodoAcademico.FirstOrDefault(x => x.IdCurso == encuesta.IdCurso && x.IdPeriodoAcademico == encuesta.IdPeriodoAcademico);
                                    //foreach (var item in CursoPeriodoAcademico.CarreraCursoPeriodoAcademico)
                                    //{
                                    //    foreach (var outcomeComision in lstoutcomes)
                                    //    {
                                    //        var hallazgo = new Hallazgo();

                                    //        hallazgo.Codigo = String.Format("{0}-{1}-{2}-{3}-F-{4}", AcronimoInstrumento, objCurso.Codigo, cicloacademico, codigosede, iHallazgo.ToString("D3"));
                                    //        hallazgo.DescripcionEspanol = String.Format("La sección {0}, presenta un puntaje de: {1}", objSeccion.Codigo, Math.Round((Double)encuesta.PuntajeTotal * 1.0, 2));
                                    //        hallazgo.DescripcionIngles = String.Format("The classroom {0}, has a score : {1}", objSeccion.Codigo, Math.Round((Double)encuesta.PuntajeTotal * 1.0, 2));
                                    //        hallazgo.IdPeriodoAcademico = model.CicloId;
                                    //        hallazgo.IdCurso = objCurso.IdCurso;
                                    //        hallazgo.Encuesta = encuesta;
                                    //        hallazgo.IdComision = outcomeComision.IdComision;
                                    //        hallazgo.FechaRegistro = DateTime.Now;
                                    //        hallazgo.IdSede = encuesta.IdSede;
                                    //        hallazgo.IdCarrera = item.IdCarrera;
                                    //        hallazgo.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                    //        hallazgo.Generado = ConstantHelpers.HALLAZGO.AUTO;
                                    //        hallazgo.IdNivelAceptacionHallazgo = idnivelaceptacion;
                                    //        hallazgo.IdOutcome = outcomeComision.IdOutcome;
                                    //        hallazgo.IdInstrumento = IdInstrumento;

                                    //        hallazgo.IdConstituyente = context.Constituyente.FirstOrDefault(x => x.NombreEspanol == ConstantHelpers.ENCUESTA.CONSTITUYENTE_ESTUDIANTE).IdConstituyente;
                                    //        context.Hallazgo.Add(hallazgo);

                                    //    }
                                    //}
                                    iHallazgo++;
                                }
                            }
                            cantSuccess++;
                        }
                        else
                        {
                            if (objSeccion == null)
                                lstError.Add("( " + (i + 1).ToString() + " ) " + ": Error al cargar Sección");
                            else if (objCurso == null)
                                lstError.Add("( " + (i + 1).ToString() + " ) " + ": Error al cargar Curso");
                            else
                                lstError.Add("( " + (i + 1).ToString() + " ) " + ": Error Curso y Sección no están relacionados");
                        }
                    }
                    catch (Exception ex)
                    {
                        data.Error = true;
                        return Json(data);
                    }

                }
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    try
                    {
                        var Comentario = dataTable.Rows[i][colComentario].ToSafeString().ToUpper();
                        var CodigoEncuesta = dataTable.Rows[i][colCodigoEncuesta].ToInteger();
                        var SeccionC = dataTable.Rows[i][colSeccionC].ToSafeString().ToUpper();
                        var CursoC = dataTable.Rows[i][colCursoC].ToSafeString().ToUpper();

                        //var SeccionCurso = context.GetSeccionCurso(SeccionC, CursoC, model.CicloId,null).FirstOrDefault();
                        var SeccionCurso = Context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", SeccionC, CursoC, model.SubModalidadPeriodoAcademicoId, null).FirstOrDefault();

                        if (String.IsNullOrEmpty(SeccionC) == false && String.IsNullOrEmpty(CursoC) == false && CodigoEncuesta > 0)
                        {
                            Curso objCursoC = Context.Curso.FirstOrDefault(x => x.Codigo == CursoC);
                            Seccion objSeccionC = Context.Seccion.FirstOrDefault(x => x.Codigo == SeccionC);
                            if (objSeccionC != null && objCursoC != null)
                            {
                                var comentarioEncuesta = new EncuestaComentario();
                                if (CodigoEncuesta > 0)
                                    comentarioEncuesta.CodigoEncuesta = CodigoEncuesta;

                                comentarioEncuesta.Comentario = Comentario;
                                comentarioEncuesta.IdEncuesta = Context.Encuesta.FirstOrDefault(x => x.Seccion.IdSeccion == SeccionCurso.IdSeccion
                                   && x.Curso.IdCurso == SeccionCurso.IdCurso && x.IdSubModalidadPeriodoAcademico == SeccionCurso.IdSubModalidadPeriodoAcademico).IdEncuesta;
                                Context.EncuestaComentario.Add(comentarioEncuesta);
                                cantSuccess++;
                            }
                            else
                            {
                                cantFail++;
                                if (objSeccionC == null)
                                    lstError.Add("( " + (i + 1).ToString() + " ) " + ": Error al cargar Sección (Comentario)");
                                else
                                    lstError.Add("( " + (i + 1).ToString() + " ) " + ": Error al cargar Curso (Comentario)");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        data.Error = true;
                        return Json(data);
                    }
                }

                Context.SaveChanges();

                data.CantSuccess = cantSuccess;
                data.lstError = lstError;

                return Json(data);
            }
            catch (Exception ex)
            {
                data.Error = true;
                return Json(data);
            }

        }

        public ActionResult UploadGRA()
        {
            var model = new UploadGRAViewModel();
            model.Fill(CargarDatosContext());
            return View(model);
        }
        [HttpPost]
        public JsonResult UploadGRA(UploadGRAViewModel model)
        {
            var data = new DataCarga();
            List<String> lstError = new List<String>();
            try
            {
                Int32 colCodigoEncuesta = 0;
                Int32 colCodigoCarrera = 1;
                Int32 colCiclo = 2;
                //Int32 colSede = 3;

                Int32 cantSuccess = 0;
                Int32 cantFail = 0;
                TipoEncuesta tipoencuesta = Context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA);
                var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    try
                    {
                        var CodigoEncuesta = dataTable.Rows[i][colCodigoEncuesta].ToInteger();
                        var CodigoCarrera = dataTable.Rows[i][colCodigoCarrera].ToString();
                        var eCiclo = dataTable.Rows[i][colCiclo].ToString();
                        //var eSede = dataTable.Rows[i][colSede].ToString();

                        //var indice = 4;
                        var indice = 3;
                        var LstCE = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
                            && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == model.CarreraId && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                        var LstCENoVisible = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
                            && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == model.CarreraId && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible == false).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                        List<Int32> colCE = new List<Int32>();
                        for (int k = 0; k < LstCE.Count; k++)
                        {
                            colCE.Add(indice + k);
                        }

                        indice += LstCE.Count;
                        var LstCG = Context.OutcomeEncuestaConfig.Include(x => x.IdTipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
                            && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && model.subModalidadPeriodoAcademicoId == x.IdSubModalidadPeriodoAcademico && x.IdCarrera == model.CarreraId && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).OrderBy(x => x.Orden).ToList();
                        List<Int32> colCG = new List<Int32>();
                        for (int k = 0; k < LstCG.Count; k++)
                        {
                            colCG.Add(indice + k);
                        }
                        Int32 colComentario = colCG.LastOrDefault() + 1;

                        var CE = new List<Int32>();
                        for (int j = 0; j < LstCE.Count; j++)
                            CE.Add(dataTable.Rows[i][colCE[j]].ToInteger());
                        var CG = new List<Int32>();
                        for (int j = 0; j < LstCG.Count; j++)
                            CG.Add(dataTable.Rows[i][colCG[j]].ToInteger());

                        var Comentario = dataTable.Rows[i][colComentario].ToString();

                        if (String.IsNullOrEmpty(CodigoCarrera) == false && String.IsNullOrEmpty(eCiclo) == false)
                        //&& String.IsNullOrEmpty(eSede) == false)
                        {
                            var encuesta = new Encuesta();

                            //var objSede = context.Sede.FirstOrDefault(x => x.Codigo == eSede);
                            var objCarrera = Context.Carrera.FirstOrDefault(x => x.IdCarrera == model.CarreraId);
                            var objCiclo = Context.PeriodoAcademico.FirstOrDefault(x => x.CicloAcademico == eCiclo);

                            var objSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.CicloAcademico == eCiclo);

                            //if (objSede != null && objCarrera != null && objCiclo != null)
                            if (objCarrera != null && objCiclo != null)
                            {
                                if (CodigoEncuesta > 0)
                                    encuesta.CodigoEncuesta = CodigoEncuesta;
                                encuesta.IdTipoEncuesta = tipoencuesta.IdTipoEncuesta;
                                encuesta.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;

                                encuesta.Carrera = objCarrera;
                                //encuesta.Sede = objSede;
                                encuesta.SubModalidadPeriodoAcademico = objSubModalidadPeriodoAcademico;

                                if (String.IsNullOrEmpty(Comentario) == false)
                                    encuesta.Comentario = Comentario;

                                Context.Encuesta.Add(encuesta);
                                Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                                for (int j = 0; j < LstCE.Count; j++)
                                {
                                    var performance = new PerformanceEncuesta();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaConfig = LstCE[j].IdOutcomeEncuestaConfig;
                                    performance.PuntajeOutcome = CE[j];
                                    Context.PerformanceEncuesta.Add(performance);
                                    data.lstError = lstError;
                                    //if (performance.PuntajeOutcome < 0)
                                    //    DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = 0;
                                    //else
                                    DicPerformance[LstCE[j].NombreEspanol + LstCE[j].Comision.Codigo] = CE[j];
                                }
                                var logicCAC = new RegisterSurveyLogic();

                                foreach (var cac in LstCENoVisible)
                                {
                                    var performance = new PerformanceEncuesta();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaConfig = cac.IdOutcomeEncuestaConfig;
                                    performance.PuntajeOutcome = logicCAC.ConvertirOutcome_DesdeEAC_ParaCAC(cac.NombreEspanol, DicPerformance);
                                    Context.PerformanceEncuesta.Add(performance);
                                }
                                for (int j = 0; j < LstCG.Count; j++)
                                {
                                    var performance = new PerformanceEncuesta();
                                    performance.Encuesta = encuesta;
                                    performance.IdOutcomeEncuestaConfig = LstCG[j].IdOutcomeEncuestaConfig;
                                    performance.PuntajeOutcome = CG[j];
                                    Context.PerformanceEncuesta.Add(performance);
                                }
                                cantSuccess++;
                            }
                            else
                            {
                                //if (objSede == null)
                                //  lstError.Add("( " + i+1 + " ) " + ": La sede ingresada no existe.");
                                //else 
                                if (objCarrera == null)
                                    lstError.Add("( " + i + 1 + " ) " + ": La carrera ingresada no existe.");
                                else if (objCiclo == null)
                                    lstError.Add("( " + i + 1 + " ) " + ": El periodo académico ingresado no existe.");
                            }
                        }
                        else
                        {
                            cantFail++;
                            if (String.IsNullOrEmpty(CodigoCarrera))
                                lstError.Add("( " + i + 1 + " ) " + ": Debe ingresar código de carrera");
                            else if (String.IsNullOrEmpty(eCiclo))
                                lstError.Add("( " + i + 1 + " ) " + ": Debe ingresar periodo académico");
                            //else if (String.IsNullOrEmpty(eSede))
                            //  lstError.Add("( " + i+1 + " ) " + ": Debe ingresar sede");
                        }
                    }
                    catch (Exception ex)
                    {
                        data.Error = true;
                        return Json(data);
                    }

                }
                Context.SaveChanges();
                data.CantSuccess = cantSuccess;
                data.lstError = lstError;
                return Json(data);

            }
            catch (Exception ex)
            {
                data.Error = true;
                return Json(data);
            };
        }
    }
}
