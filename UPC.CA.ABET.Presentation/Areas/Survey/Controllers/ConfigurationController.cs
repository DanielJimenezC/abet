﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Data;
using Excel;
using System.Transactions;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Filters;
using System.Data.Entity.Validation;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.pdf;
//using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelReports;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using YandexTranslateCSharpSdk;
using System.Threading.Tasks;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Presentation.Areas.Professor.Resources.Views.IfcManagement;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Resources.Areas.Meeting;
using System.Globalization;
//Test de commit del recurso4
namespace UPC.CA.ABET.Presentation.Areas.Survey.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class ConfigurationController : BaseController
    {
        // GET: Survey/Configuration
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReplicarConfiguracionFDC(Int32? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                //var ultimoPeriodo = context.PeriodoAcademico.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO).OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdPeriodoAcademico;               
                var ultimosPeriodos = (from smpa in Context.SubModalidadPeriodoAcademico
                                       join PA in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals PA.IdPeriodoAcademico
                                       where PA.Estado == ConstantHelpers.ESTADO.INACTIVO
                                       select new
                                       {
                                           CicloAcademico = PA.CicloAcademico,
                                           IdSubModalidadPeriodoAcademico = smpa.IdSubModalidadPeriodoAcademico,
                                           idPeriodoAcademico = PA.IdPeriodoAcademico
                                       }).OrderByDescending(x => x.idPeriodoAcademico).ToList(); //Pueden haber mas de 2 periodos activos - 1 periodo por submodalidad



                //var lstNiveles = context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == ultimoPeriodo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC).ToList();
                var lstNiveles = (from nae in Context.NivelAceptacionEncuesta
                                  join ultpa in ultimosPeriodos on nae.IdSubModalidadPeriodoAcademico equals ultpa.IdSubModalidadPeriodoAcademico
                                  where nae.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC
                                  select new
                                  {
                                      NombreEspanol = nae.NombreEspanol,
                                      NombreIngles = nae.NombreIngles,
                                      ValorMaximo = nae.ValorMaximo,
                                      ValorMinimo = nae.ValorMinimo,
                                      EsFinal = nae.EsFinal,
                                      Color = nae.Color,
                                      IdTipoEncuesta = nae.IdTipoEncuesta,
                                      IdSubModalidadPeriodoAcademico = nae.IdSubModalidadPeriodoAcademico
                                  }).ToList();

                foreach (var item in lstNiveles)
                {
                    var nivel = new NivelAceptacionEncuesta();
                    nivel.NombreEspanol = item.NombreEspanol;
                    nivel.NombreIngles = item.NombreIngles;
                    nivel.ValorMaximo = item.ValorMaximo;
                    nivel.ValorMinimo = item.ValorMinimo;
                    nivel.EsFinal = item.EsFinal;
                    nivel.Color = item.Color;
                    nivel.IdTipoEncuesta = item.IdTipoEncuesta;
                    nivel.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico.HasValue ? IdSubModalidadPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    Context.NivelAceptacionEncuesta.Add(nivel);
                }
                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("ConfigurationFDC", new
            {
                CicloId = IdSubModalidadPeriodoAcademico
            });
        }
        public ActionResult ReplicarConfiguracionGRA(Int32? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                var lstOutcomes = new List<OutcomeEncuestaConfig>();
                var lstNiveles = new List<NivelAceptacionEncuesta>();
                var lstAcreditadora = new List<AcreditadoraPeriodoAcademico>();

                // var IdModalidad = Session.GetModalidadId();

                //var ultimoPeriodo = context.PeriodoAcademico.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO).OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdPeriodoAcademico;

                var IdSubmodalidadPeriodoAcademicoActual = Session.GetSubModalidadPeriodoAcademicoId();

                // Verifica si se está en Regular o EPE
                var IdSubmodalidad = (from m in Context.Modalidad
                                            join sm in Context.SubModalidad on m.IdModalidad equals sm.IdModalidad
                                            join smpa in Context.SubModalidadPeriodoAcademico on sm.IdSubModalidad equals smpa.IdSubModalidad
                                            join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                            where smpa.IdSubModalidadPeriodoAcademico == IdSubmodalidadPeriodoAcademicoActual
                                      select smpa.IdSubModalidad).FirstOrDefault();

                
                var IdSubmodalidadPeriodoAcademicoAnterior = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidad == IdSubmodalidad && x.PeriodoAcademico.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO).OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdSubModalidadPeriodoAcademico;


                /*
                var ultimosPeriodos = (from smpa in Context.SubModalidadPeriodoAcademico
                                       join PA in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals PA.IdPeriodoAcademico
                                       where smpa.IdSubModalidad == 1
                                       //where PA.Estado == ConstantHelpers.ESTADO.INACTIVO
                                       select new
                                       {
                                           CicloAcademico = PA.CicloAcademico,
                                           IdSubModalidadPeriodoAcademico = smpa.IdSubModalidadPeriodoAcademico,
                                           PeriodoAcademicoId = PA.IdPeriodoAcademico,
                                           IdsubModalidad = smpa.IdSubModalidad
                                       }).OrderByDescending(x => x.PeriodoAcademicoId).ToList(); //Pueden haber mas de 2 periodos activos - 1 periodo por submodalidad
                */
 


                //  lstOutcomes = context.OutcomeEncuestaConfig.Where(x => x.IdPeriodoAcademico == ultimoPeriodo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();

                lstOutcomes = Context.OutcomeEncuestaConfig.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubmodalidadPeriodoAcademicoAnterior && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();


                /*
                lstOutcomes = (from oec in Context.OutcomeEncuestaConfig
                               join upa in ultimosPeriodos on oec.IdSubModalidadPeriodoAcademico equals upa.IdSubModalidadPeriodoAcademico
                               where (oec.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && oec.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO)
                               select new OutcomeEncuestaConfig
                               {
                                   IdOutcomeEncuestaConfig = oec.IdOutcomeEncuestaConfig,
                                   Orden = oec.Orden,
                                   DescripcionEspanol = oec.DescripcionEspanol,
                                   DescripcionIngles = oec.DescripcionIngles,
                                   IdTipoEncuesta = oec.IdTipoEncuesta,
                                   IdTipoOutcomeEncuesta = oec.IdTipoOutcomeEncuesta,
                                   NombreEspanol = oec.NombreEspanol,
                                   NombreIngles = oec.NombreIngles,
                                   Estado = oec.Estado,
                                   IdCarrera = oec.IdCarrera,
                                   IdComision = oec.IdCarrera,
                                   EsVisible = oec.EsVisible,
                                   IdSubModalidadPeriodoAcademico = oec.IdSubModalidadPeriodoAcademico
                               }).ToList();
                */

                //   lstNiveles = context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == ultimoPeriodo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).ToList();

                lstNiveles = Context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubmodalidadPeriodoAcademicoAnterior && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).ToList();


                /* lstNiveles = (from nae in Context.NivelAceptacionEncuesta
                               join upa in ultimosPeriodos on nae.IdSubModalidadPeriodoAcademico equals upa.IdSubModalidadPeriodoAcademico
                               where (nae.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA)
                               select new NivelAceptacionEncuesta
                               {
                                   IdNivelAceptacionEncuesta = nae.IdNivelAceptacionEncuesta,
                                   NombreEspanol = nae.NombreEspanol,
                                   NombreIngles = nae.NombreIngles,
                                   ValorMinimo = nae.ValorMinimo,
                                   ValorMaximo = nae.ValorMaximo,
                                   Color = nae.Color,
                                   IdTipoEncuesta = nae.IdTipoEncuesta,
                                   EsFinal = nae.EsFinal,
                                   IdSubModalidadPeriodoAcademico = nae.IdSubModalidadPeriodoAcademico
                               }).ToList();
                               */
                //var lstAcreditadora = context.AcreditadoraPeriodoAcademico.Where(x => x.sub == IdPeriodoAcademico).ToList();

                lstAcreditadora = Context.AcreditadoraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubmodalidadPeriodoAcademicoAnterior).ToList();

                /*
                lstAcreditadora = (from apa in Context.AcreditadoraPeriodoAcademico
                                   join smp in Context.SubModalidadPeriodoAcademico on apa.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
                                   where smp.IdPeriodoAcademico == PeriodoAcademicoId
                                   select new AcreditadoraPeriodoAcademico
                                   {
                                       IdAcreditadoraPreiodoAcademico = apa.IdAcreditadoraPreiodoAcademico,
                                       IdAcreditadora = apa.IdAcreditadora,
                                       IdSubModalidadPeriodoAcademico = apa.IdSubModalidadPeriodoAcademico
                                   }).ToList();
                */

                //Esto queda tal cual
                List<Comision> LstComision = new List<Comision>();
                foreach (var item in lstAcreditadora)
                {
                    LstComision.AddRange(item.Comision.ToList());
                }
                //REVISAR GetPeriodoAcademicoId deberia ser GetSubModalidadPeriodoAcademico
                foreach (var item in lstOutcomes)
                {
                    var outcome = new OutcomeEncuestaConfig();
                    outcome.Orden = item.Orden;
                    outcome.DescripcionEspanol = item.DescripcionEspanol;
                    outcome.DescripcionIngles = item.DescripcionIngles;
                    outcome.IdTipoEncuesta = item.IdTipoEncuesta;
                    outcome.IdTipoOutcomeEncuesta = item.IdTipoOutcomeEncuesta;
                    outcome.NombreEspanol = item.NombreEspanol;
                    outcome.NombreIngles = item.NombreIngles;
                    outcome.Estado = item.Estado;
                    outcome.IdSubModalidadPeriodoAcademico = IdSubmodalidadPeriodoAcademicoActual;
                    outcome.IdCarrera = item.IdCarrera;
                    var NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == item.IdComision).Codigo;
                    
                    foreach (var comision in LstComision)
                    {
                        if (comision.Codigo == NombreComision)
                        {
                            outcome.IdComision = comision.IdComision;
                        }
                    }
                    //outcome.IdComision = item.IdComision;
                    outcome.EsVisible = item.EsVisible;
                    Context.OutcomeEncuestaConfig.Add(outcome);
                }

                foreach (var item in lstNiveles)
                {
                    var nivel = new NivelAceptacionEncuesta();
                    nivel.NombreEspanol = item.NombreEspanol;
                    nivel.NombreIngles = item.NombreIngles;
                    nivel.ValorMaximo = item.ValorMaximo;
                    nivel.ValorMinimo = item.ValorMinimo;
                    nivel.EsFinal = item.EsFinal;
                    nivel.Color = item.Color;
                    nivel.IdTipoEncuesta = item.IdTipoEncuesta;
                    nivel.IdSubModalidadPeriodoAcademico = IdSubmodalidadPeriodoAcademicoActual;
                    Context.NivelAceptacionEncuesta.Add(nivel);
                }
                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
            }
            //A travez de una submodalidadperiodoacademico devuelvo un idperiodoacademico
            var CicloId = (from smpa in Context.SubModalidadPeriodoAcademico
                           join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                           where smpa.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                           select new
                           {
                               IdCiclo = pa.IdPeriodoAcademico
                           }).FirstOrDefault();

            return RedirectToAction("ConfigurationGRA", new { CicloId = CicloId });
        }
        public ActionResult ReplicarConfiguracionPPP(Int32? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                var lstOutcomes = new List<OutcomeEncuestaConfig>();
                var lstNiveles = new List<NivelAceptacionEncuesta>();

                //CORREGIRLUEGO
                var ultimoPeriodo = Context.PeriodoAcademico.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO).OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdPeriodoAcademico;
                var IdPeriodo = Session.GetPeriodoAcademicoId();

                lstOutcomes = Context.OutcomeEncuestaConfig.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ultimoPeriodo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();
                lstNiveles = Context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ultimoPeriodo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();

                //var lstAcreditadora = context.AcreditadoraPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).ToList();
                var lstAcreditadora = Context.AcreditadoraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).ToList();
                List<Comision> LstComision = new List<Comision>();
                foreach (var item in lstAcreditadora)
                {
                    LstComision.AddRange(item.Comision.ToList());
                }

                foreach (var item in lstOutcomes)
                {
                    var outcome = new OutcomeEncuestaConfig();
                    outcome.Orden = item.Orden;
                    outcome.DescripcionEspanol = item.DescripcionEspanol;
                    outcome.DescripcionIngles = item.DescripcionIngles;
                    outcome.IdTipoEncuesta = item.IdTipoEncuesta;
                    outcome.IdTipoOutcomeEncuesta = item.IdTipoOutcomeEncuesta;
                    outcome.NombreEspanol = item.NombreEspanol;
                    outcome.NombreIngles = item.NombreIngles;
                    outcome.Estado = item.Estado;
                    //outcome.IdPeriodoAcademico = IdPeriodoAcademico.HasValue ? IdPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    //VERLUEGOVVI
                    outcome.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico.HasValue ? IdSubModalidadPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    outcome.IdCarrera = item.IdCarrera;
                    var NombreComision = Context.Comision.FirstOrDefault(x => x.IdComision == item.IdComision).Codigo;
                    foreach (var comision in LstComision)
                    {
                        if (comision.Codigo == NombreComision)
                        {
                            outcome.IdComision = comision.IdComision;
                        }
                    }
                    //outcome.IdComision = item.IdComision;
                    outcome.EsVisible = item.EsVisible;
                    Context.OutcomeEncuestaConfig.Add(outcome);
                }

                foreach (var item in lstNiveles)
                {
                    var nivel = new NivelAceptacionEncuesta();
                    nivel.NombreEspanol = item.NombreEspanol;
                    nivel.NombreIngles = item.NombreIngles;
                    nivel.ValorMaximo = item.ValorMaximo;
                    nivel.ValorMinimo = item.ValorMinimo;
                    nivel.Color = item.Color;
                    nivel.EsFinal = item.EsFinal;
                    nivel.IdTipoEncuesta = item.IdTipoEncuesta;
                    //nivel.IdPeriodoAcademico = IdPeriodoAcademico.HasValue ? IdPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    nivel.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico.HasValue ? IdSubModalidadPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    Context.NivelAceptacionEncuesta.Add(nivel);
                }
                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("ConfigurationPPP", new { CicloId = IdSubModalidadPeriodoAcademico });
        }

        public ActionResult ReplicarNewConfiguracionPPP(Int32? IdPeriodoAcademico)
        {
            try
            {
                if (IdPeriodoAcademico >= 6 && IdPeriodoAcademico <= 8)
                    IdPeriodoAcademico = 5;

                var lstOutcomes = new List<OutcomeEncuestaPPPConfig>();
                var lstOutcomesID = new List<int>();
                var lstOutcomesNuevos = new List<OutcomeEncuestaPPPConfig>();
                var lstNiveles = new List<NivelAceptacionEncuesta>();
                var lstOutcomePPPOutcomes = new List<OutcomeEncuestaPPPOutcome>();
                var lstAcreditadora = new List<AcreditadoraPeriodoAcademico>();
                //var ultimoPeriodo = context.PeriodoAcademico.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO).OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdPeriodoAcademico;

                var ultimoPeriodo = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidad == 1 && x.PeriodoAcademico.Estado == ConstantHelpers.ENCUESTA.ESTADO_INACTIVO    && x.IdPeriodoAcademico < IdPeriodoAcademico).
                    OrderByDescending(x => x.IdPeriodoAcademico).Take(1).ToList()[0].IdSubModalidadPeriodoAcademico;


                var IdPeriodo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;
                //var IdPeriodo = Session.GetPeriodoAcademicoId();



                //lstOutcomes = context.OutcomeEncuestaPPPConfig.Where(x => x.IdPeriodoAcademico == ultimoPeriodo &&  x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible.Value && x.IdEscuela==escuelaId).ToList();
                lstOutcomes = Context.OutcomeEncuestaPPPConfig.Where(x => x.IdSubModalidadPeriodoAcademico == ultimoPeriodo && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.EsVisible.Value && x.IdEscuela == EscuelaId).ToList();
                lstOutcomesID = lstOutcomes.Select(x => x.IdOutcomeEncuestaPPPConfig).ToList();
                //lstNiveles = context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == ultimoPeriodo && x.IdTipoEncuesta==1).ToList();
                lstNiveles = Context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == ultimoPeriodo && x.IdTipoEncuesta == 1).ToList();
                lstOutcomePPPOutcomes = Context.OutcomeEncuestaPPPOutcome.Where(x => lstOutcomesID.Contains(x.IdOutcomeEncuestaPPPConfig)).ToList();
                //lstAcreditadora = context.AcreditadoraPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).ToList();
                lstAcreditadora = Context.AcreditadoraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdPeriodoAcademico).ToList();
                /* List<Comision> LstComision = new List<Comision>();
                 foreach (var item in lstAcreditadora)
                 {
                     LstComision.AddRange(item.Comision.ToList());
                 }*/

                foreach (var item in lstOutcomes)
                {
                    var outcome = new OutcomeEncuestaPPPConfig();
                    outcome.Orden = item.Orden;
                    outcome.DescripcionEspanol = item.DescripcionEspanol;
                    outcome.DescripcionIngles = item.DescripcionIngles;
                    //outcome.IdTipoEncuesta = item.IdTipoEncuesta;
                    outcome.IdTipoOutcomeEncuesta = item.IdTipoOutcomeEncuesta;
                    outcome.NombreEspanol = item.NombreEspanol;
                    outcome.NombreIngles = item.NombreIngles;
                    outcome.Estado = item.Estado;
                    //outcome.IdPeriodoAcademico = IdPeriodoAcademico.HasValue ? IdPeriodoAcademico.Value : Session.GetPeriodoAcademicoId().Value;
                    outcome.IdSubModalidadPeriodoAcademico = IdPeriodoAcademico.HasValue ? Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).
                        FirstOrDefault().IdSubModalidadPeriodoAcademico
                        : IdPeriodo;

                    //IdPeriodoAcademico.Value : Session.GetPeriodoAcademicoId().Value;
                    outcome.IdCarrera = item.IdCarrera;
                    outcome.IdEscuela = item.IdEscuela;
                    /* var NombreComision = context.Comision.FirstOrDefault(x => x.IdComision == item.IdComision).Codigo;
                     foreach (var comision in LstComision)
                     {
                         if (comision.Codigo == NombreComision)
                         {
                             outcome.IdComision = comision.IdComision;
                         }
                     }*/
                    //outcome.IdComision = item.IdComision;
                    outcome.EsVisible = item.EsVisible;
                    Context.OutcomeEncuestaPPPConfig.Add(outcome);
                    lstOutcomesNuevos.Add(outcome);
                }

                foreach (var item in lstNiveles)
                {
                    var nivel = new NivelAceptacionEncuesta();
                    nivel.NombreEspanol = item.NombreEspanol;
                    nivel.NombreIngles = item.NombreIngles;
                    nivel.ValorMaximo = item.ValorMaximo;
                    nivel.ValorMinimo = item.ValorMinimo;
                    nivel.Color = item.Color;
                    nivel.EsFinal = item.EsFinal;
                    nivel.IdTipoEncuesta = item.IdTipoEncuesta;
                    //nivel.IdPeriodoAcademico = IdPeriodoAcademico.HasValue ? IdPeriodoAcademico : Session.GetPeriodoAcademicoId();
                    nivel.IdSubModalidadPeriodoAcademico = IdPeriodoAcademico.HasValue ? Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).
                       FirstOrDefault().IdSubModalidadPeriodoAcademico
                       : IdPeriodo;
                    Context.NivelAceptacionEncuesta.Add(nivel);
                }

                foreach (var item in lstOutcomePPPOutcomes)
                {

                    OutcomeEncuestaPPPConfig oepc = lstOutcomesNuevos.FirstOrDefault(x => x.Orden == item.OutcomeEncuestaPPPConfig.Orden && x.IdTipoOutcomeEncuesta == item.OutcomeEncuestaPPPConfig.IdTipoOutcomeEncuesta);

                    if (oepc != null)
                    {
                        OutcomeEncuestaPPPOutcome oepo = new OutcomeEncuestaPPPOutcome();
                        oepo.IdOutcome = item.IdOutcome;
                        oepo.OutcomeEncuestaPPPConfig = oepc;

                        Context.OutcomeEncuestaPPPOutcome.Add(oepo);
                    }
                }


                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("NewConfigurationPPP", new { CicloId = IdPeriodoAcademico });
        }

        public ActionResult ConfigurationPPP(Int32? CicloId)
        {
            var model = new ConfigurationPPPViewModel();
            model.Fill(CargarDatosContext(), CicloId);
            return View(model);
        }
        public ActionResult NewConfigurationPPP(Int32? CicloId)
        {
            var model = new NewConfigurationPPPViewModel();
            model.Fill(CargarDatosContext(), CicloId, EscuelaId);
            return View(model);
        }
        public ActionResult ConfigurationFDC(Int32? CicloId)
        {
            var model = new ConfigurationFDCViewModel();
            model.Fill(CargarDatosContext(), CicloId);
            return View(model);
        }
        public ActionResult ConfigurationGRA(Int32? subModalidadPeriodoAcademicoId)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new ConfigurationGRAViewModel();
            model.Fill(CargarDatosContext(), subModalidadPeriodoAcademicoId, parModalidadId);
            return View(model);
        }
        public ActionResult SurveyPPPConfiguration(Int32 CicloId)
        {
            var model = new SurveyPPPConfigurationViewModel();
            model.Fill(CargarDatosContext(), CicloId);
            return View(model);
        }
        public ActionResult NewSurveyPPPConfiguration(Int32 CicloId)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new NewSurveyPPPConfigurationViewModel();
            model.Fill(CargarDatosContext(), CicloId, EscuelaId, parModalidadId);
            return View(model);
        }

        public ActionResult SurveyGRAConfiguration(Int32 subModalidadPeriodoAcademicoId)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new SurveyGRAConfigurationViewModel();
            model.Fill(CargarDatosContext(), subModalidadPeriodoAcademicoId, parModalidadId);
            return View(model);
        }
        public ActionResult DeleteOutcomeConfig(Int32 IdOutcomeEncuestaConfig, Int32 subModalidadPeriodoAcademicoId, String TipoEncuestaNombre)
        {
            try
            {
                var outcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == IdOutcomeEncuestaConfig);
                outcome.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("Survey" + TipoEncuestaNombre + "Configuration", "Configuration", new { subModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("Survey" + TipoEncuestaNombre + "Configuration", "Configuration", new { subModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId });
            }
        }

        public ActionResult DeleteOutcomePPPConfig(Int32 IdOutcomeEncuestaPPPConfig, Int32 CicloId)
        {
            try
            {
                var outcome = Context.OutcomeEncuestaPPPConfig.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == IdOutcomeEncuestaPPPConfig);
                outcome.Estado = ConstantHelpers.ENCUESTA.ESTADO_INACTIVO;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("NewSurveyPPPConfiguration", "Configuration", new { CicloId = CicloId });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("NewSurveyPPPConfiguration", "Configuration", new { CicloId = CicloId });
            }
        }
        public ActionResult _AttachOutcome(Int32 IdOutcomeEncuestaConfig, Int32 CicloId, String TipoEncuestaNombre, Int32? IdComision)
        {
            var model = new _AttachOutcomeViewModel();
            model.Fill(CargarDatosContext(), IdOutcomeEncuestaConfig, CicloId, TipoEncuestaNombre, IdComision);
            return View(model);
        }
        [HttpPost]
        public ActionResult _AttachOutcome(_AttachOutcomeViewModel model, FormCollection frm)
        {
            try
            {
                var vinculos = Context.DetalleOutcomeEncuestaConfig.Where(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig).AsQueryable();
                Context.DetalleOutcomeEncuestaConfig.RemoveRange(vinculos);

                for (int i = 4; i <= frm.Count - 1; i++)
                {
                    var detalle = new DetalleOutcomeEncuestaConfig();
                    detalle.IdOutcomeEncuestaConfig = model.IdOutcomeEncuestaConfig;
                    detalle.IdOutcome = frm.GetKey(i).ToInteger();
                    Context.DetalleOutcomeEncuestaConfig.Add(detalle);
                }

                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { CicloId = model.SubModalidadPeriodoAcademicoId });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { CicloId = model.SubModalidadPeriodoAcademicoId });
            }

        }
        public ActionResult _AddPPPOutcome(Int32? IdOutcomeEncuestaPPPConfig, Int32 CicloId, String TipoOutcome)
        {
            int parModalidadId = Session.GetModalidadId();

            var model = new _AddPPPOutcomeViewModel();
            model.Fill(CargarDatosContext(), IdOutcomeEncuestaPPPConfig, CicloId, TipoOutcome, EscuelaId, parModalidadId);
            return View(model);
        }
        [HttpPost]
        public ActionResult _AddPPPOutcome(_AddPPPOutcomeViewModel model)
        {
            try
            {
                var outcome = new OutcomeEncuestaPPPConfig();
                if (model.IdOutcomeEncuestaPPPConfig.HasValue)
                {
                    outcome = Context.OutcomeEncuestaPPPConfig.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == model.IdOutcomeEncuestaPPPConfig);
                }
                else
                {
                    outcome.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;
                    outcome.IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                    outcome.IdTipoOutcomeEncuesta = model.IdTipoOutcome;
                    Context.OutcomeEncuestaPPPConfig.Add(outcome);
                }


                outcome.OtraCarrera = model.OtraCarrera;
                outcome.EsVisible = model.EsVisible;
                outcome.DescripcionIngles = model.DescripcionIn;
                outcome.DescripcionEspanol = model.DescripcionEs;
                outcome.NombreEspanol = model.NombreEs;
                outcome.NombreIngles = model.NombreIn;


                outcome.Orden = model.Orden;



                outcome.IdCarrera = model.IdCarrera;



                outcome.IdEscuela = EscuelaId;


                List<OutcomeEncuestaPPPOutcome> outcomesHallazgos = Context.OutcomeEncuestaPPPOutcome.Where(x => x.IdOutcomeEncuestaPPPConfig == outcome.IdOutcomeEncuestaPPPConfig).ToList();

                foreach (OutcomeEncuestaPPPOutcome item in outcomesHallazgos)
                {
                    Context.OutcomeEncuestaPPPOutcome.Remove(item);
                }


                /* List<int> outcomeHallazgos = new List<int>();

                 if(model.OutcomeHallazgo1.HasValue)
                 {
                     outcomeHallazgos.Add(model.OutcomeHallazgo1.Value);
                 }
                 if (model.OutcomeHallazgo2.HasValue )
                 {
                     if(!outcomeHallazgos.Contains(model.OutcomeHallazgo2.Value))
                         outcomeHallazgos.Add(model.OutcomeHallazgo2.Value);
                 }
                 if (model.OutcomeHallazgo3.HasValue)
                 {
                     if (!outcomeHallazgos.Contains(model.OutcomeHallazgo3.Value))
                         outcomeHallazgos.Add(model.OutcomeHallazgo3.Value);
                 }*/

                for (int i = 0; i < model.LstOutcomesSeleccionados.Count; i++)
                {
                    if (model.LstOutcomesSeleccionados[i] == 0) continue;
                    OutcomeEncuestaPPPOutcome item = new OutcomeEncuestaPPPOutcome();
                    item.IdOutcomeEncuestaPPPConfig = outcome.IdOutcomeEncuestaPPPConfig;
                    item.IdOutcome = model.LstOutcomesSeleccionados[i];
                    Context.OutcomeEncuestaPPPOutcome.Add(item);
                }

                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { CicloId = model.CicloId });
                //return RedirectToAction("NewSurveyPPPConfiguration", "Configuration", new { CicloId = model.CicloId });

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { CicloId = model.CicloId });
                //return RedirectToAction("NewSurveyPPPConfiguration","Configuration", new { CicloId = model.CicloId });

            }
        }


        public ActionResult _AddOutcome(Int32? IdOutcomeEncuestaConfig, Int32 subModalidadPeriodoAcademicoId, Int32 TipoEncuestaId, String TipoOutcome, Int32? CarreraId)
        {
            var model = new _AddOutcomeViewModel();
            model.Fill(CargarDatosContext(), IdOutcomeEncuestaConfig, subModalidadPeriodoAcademicoId, TipoEncuestaId, TipoOutcome, CarreraId);
            return View(model);
        }
        [HttpPost]
        public ActionResult _AddOutcome(_AddOutcomeViewModel model)
        {
            try
            {
                var outcome = new OutcomeEncuestaConfig();
                if (model.IdOutcomeEncuestaConfig.HasValue)
                {
                    outcome = Context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == model.IdOutcomeEncuestaConfig);
                }
                else
                {
                    outcome.Estado = ConstantHelpers.ENCUESTA.ESTADO_ACTIVO;
                    //outcome.IdPeriodoAcademico = model.CicloId;
                    outcome.IdSubModalidadPeriodoAcademico = model.SubModalidadPeriodoAcademicoId;
                    outcome.IdTipoEncuesta = model.TipoEncuestaId;
                    outcome.IdTipoOutcomeEncuesta = model.IdTipoOutcome;
                    Context.OutcomeEncuestaConfig.Add(outcome);
                }
                outcome.EsVisible = model.EsVisible;
                outcome.IdComision = model.IdComision;
                outcome.DescripcionIngles = model.DescripcionIn;
                outcome.DescripcionEspanol = model.DescripcionEs;
                outcome.NombreEspanol = model.NombreEs;
                outcome.NombreIngles = model.NombreIn;
                outcome.Orden = model.Orden;
                outcome.IdCarrera = model.IdCarrera;


                Context.SaveChanges();
                PostMessage(MessageType.Success);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { SubModalidadPeriodoAcademicoId = model.SubModalidadPeriodoAcademicoId });
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error);
                return RedirectToAction("Survey" + model.TipoEncuestaNombre + "Configuration", "Configuration", new { SubModalidadPeriodoAcademicoId = model.SubModalidadPeriodoAcademicoId });
            }
        }
        public ActionResult _EditarNivelesAceptacionPPP(Int32? IdPeriodoAcademico)
        {
            var viewModel = new _EditarNivelesAceptacionPPPViewModel();
            viewModel.Fill(CargarDatosContext(), IdPeriodoAcademico);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult _EditarNivelesAceptacionPPP(_EditarNivelesAceptacionPPPViewModel model)
        {
            try
            {
                //foreach (var item in model.LstNivelAceptacion)
                for (int i = 0; i < model.LstNivelAceptacion.Count; i++)
                {
                    int idNivelAceptacionEncuesta = model.LstNivelAceptacion[i].IdNivelAceptacionEncuesta;


                    var nivel = Context.NivelAceptacionEncuesta.Where(p => p.IdNivelAceptacionEncuesta == idNivelAceptacionEncuesta).FirstOrDefault();
                    if (nivel != null) // add this
                    {
                        if (i == 0)
                            nivel.ValorMinimo = 0;
                        else
                            nivel.ValorMinimo = model.LstNivelAceptacion[i - 1].ValorMaximo;

                        nivel.ValorMaximo = model.LstNivelAceptacion[i].ValorMaximo;
                    }
                }
                Context.SaveChanges();

                var ruta = Url.Action("NewConfigurationPPP");

                return Json(ruta);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                throw;
            }
        }

        public ActionResult _EditarNivelesAceptacion(Int32 IdPeriodoAcademico, Int32 IdTipoEncuesta)
        {
            var viewmodel = new _EditarNivelesAceptacionViewModel();
            viewmodel.Fill(CargarDatosContext(), IdPeriodoAcademico, IdTipoEncuesta);
            return View(viewmodel);
        }
        [HttpPost]
        public JsonResult _EditarNivelesAceptacion(_EditarNivelesAceptacionViewModel model)
        {
            try
            {
                foreach (var item in model.LstNiveles)
                {
                    var NivelAceptacion = Context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == item.IdNivelAceptacionEncuesta);
                    NivelAceptacion.ValorMaximo = item.ValorMaximo;
                    NivelAceptacion.ValorMinimo = item.ValorMinimo;
                }
                Context.SaveChanges();
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        #region EVD

        public ActionResult ConfiguracionDelegado()
        {
            var model = new ConfiguracionDelegadoViewModel();
            //int? IdSubmoIdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            int ModalidadId = session.GetModalidadId();
            int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;

            model.CargarDatos(CargarDatosContext(), submodapaId, null);
            return View(model);
        }

        [HttpPost]
        public ActionResult ConfiguracionDelegado(ConfiguracionDelegadoViewModel viewmodel)
        {
            var model = new ConfiguracionDelegadoViewModel();
            //int? IdSubmoIdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();
            int ModalidadId = session.GetModalidadId();
            int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;

            model.CargarDatos(CargarDatosContext(), submodapaId, viewmodel.IdModulo);
            return View(model);
        }

        public ActionResult EditarDelegado(String CodigoAlumno, int ModuloId)
        {
            EditarDelegadoViewModel viewModel = new EditarDelegadoViewModel();
            viewModel.CargarDatos(CodigoAlumno, CargarDatosContext(), ModuloId);
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult EditarDelegado(EditarDelegadoViewModel viewModel)
        {
            AlumnoSeccion delegado = Context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.Alumno.Codigo == viewModel.CodigoActualDelegado && x.Seccion.IdSeccion == viewModel.IdSeccion).FirstOrDefault();
            delegado.EsDelegado = false;

            AlumnoSeccion nuevoDelegado = Context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.Alumno.Codigo == viewModel.CodigoNuevoDelegado && x.Seccion.IdSeccion == viewModel.IdSeccion).FirstOrDefault();
            nuevoDelegado.EsDelegado = true;

            Context.SaveChanges();

            PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Se cambio al delegado con código " + delegado.AlumnoMatriculado.Alumno.Codigo + " por " + nuevoDelegado.AlumnoMatriculado.Alumno.Codigo + " en la sección " + delegado.Seccion.Codigo + "." : "I changed the delegate with code " + delegado.AlumnoMatriculado.Alumno.Codigo + " by " + nuevoDelegado.AlumnoMatriculado.Alumno.Codigo + " in the section  " + delegado.Seccion.Codigo + ".");

            return RedirectToAction("ConfiguracionDelegado", "Configuration");
        }

        public ActionResult ConfiguracionInvitado()
        {
            var model = new ConfiguracionInvitadoViewModel();
            int ModalidadId = session.GetModalidadId();

            int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;
            model.CargarDatos(CargarDatosContext(), submodapaId, null);
            return View(model);
        }


        [HttpPost]
        public ActionResult ConfiguracionInvitado(ConfiguracionInvitadoViewModel viewmodel)
        {
            var model = new ConfiguracionInvitadoViewModel();
            //int? IdSubmoIdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId();

            int ModalidadId = session.GetModalidadId();

            int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;

            model.CargarDatos(CargarDatosContext(), submodapaId, viewmodel.IdModulo);
            return View(model);
        }

        public ActionResult ConfiguracionEVD(ConfiguracionEVDViewModel model, Int32? numeroPagina, Int32? IdEncuestaVirtualDelegado)
        {
            int parModalidadId = Session.GetModalidadId();

            int IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == parModalidadId && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var viewmodel = new ConfiguracionEVDViewModel();

            viewmodel.Fill(CargarDatosContext(), numeroPagina, model.IdPeriodoAcademico, model.IdModulo, parModalidadId, IdEncuestaVirtualDelegado, IdSubModalidadPeriodoAcademico);

            return View(viewmodel);
        }



        public ActionResult ConfiguracionPlantillaEncuesta(Int32? numeroPagina)
        {
            var viewmodel = new ConfiguracionPlantillaEncuestaViewModel();
            viewmodel.Fill(CargarDatosContext(), numeroPagina);
            return View(viewmodel);
        }

        public ActionResult ConfiguracionPreguntasPlantillaEncuesta(int IdPlantillaEncuesta)
        {
            var viewmodel = new ConfiguracionPreguntasPlantillaEncuestaViewModel();
            viewmodel.Fill(CargarDatosContext(), IdPlantillaEncuesta);
            return View(viewmodel);
        }

        public ActionResult EliminarPlantillaEncuesta(int IdPlantillaEncuesta)
        {

            try
            {

                var ExisteEVDparaPlantilla = Context.EncuestaVirtualDelegado.Where(x => x.IdPlantillaEncuesta == IdPlantillaEncuesta);

                if (ExisteEVDparaPlantilla.Count() > 0)
                {
                    PostMessage(MessageType.Warning, (currentCulture.Equals("es-PE")) ? "La plantilla está siendo usada para una encuesta." : "The template is being used for a survey.");

                    return RedirectToAction("ConfiguracionPlantillaEncuesta");
                }

                var LstPreguntas = Context.Pregunta.Where(x => x.IdPlantillaEncuesta == IdPlantillaEncuesta).ToList();



                foreach (var pregunta in LstPreguntas)
                {
                    var LstDetalleNiveletes = Context.DetalleNiveles.Where(x => x.IdPregunta == pregunta.IdPregunta);

                    foreach (var detalleniveles in LstDetalleNiveletes)
                    {
                        Context.DetalleNiveles.Remove(detalleniveles);
                    }

                    Context.Pregunta.Remove(pregunta);
                }

                var PlantillaEncuesta = Context.PlantillaEncuesta.Where(x => x.IdPlantillaEncuesta == IdPlantillaEncuesta).FirstOrDefault();

                Context.PlantillaEncuesta.Remove(PlantillaEncuesta);

                Context.SaveChanges();

                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "La plantilla ha sido eliminada satisfactoriamente." : "The template has been eliminated successfully.");


                return RedirectToAction("ConfiguracionPlantillaEncuesta");

            }
            catch (Exception ex)
            {

                PostMessage(MessageType.Error, (currentCulture.Equals("es-PE")) ? "Sucedió un error." : "It happened an error.");

                return RedirectToAction("ConfiguracionPlantillaEncuesta");

            }


        }

        [HttpPost]
        public ActionResult GenerarAlumnoInvitado(string moduloId, int muestra)
        {
            int ModalidadId = session.GetModalidadId();
            int submodapaId = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.IdModalidad == ModalidadId && x.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademico;
            int carreraId = (from c in Context.Carrera
                             join spa in Context.SubModalidadPeriodoAcademico on c.IdSubmodalidad equals spa.IdSubModalidad
                             where spa.IdSubModalidadPeriodoAcademico == submodapaId
                             select c.IdCarrera).First();
            int modulo = moduloId.ToInteger();
            Context.sp_GetAlumnosMuestraEVD(submodapaId, modulo, carreraId, muestra);


            return RedirectToAction("ConfiguracionInvitado");
        }
        #endregion
    }
}