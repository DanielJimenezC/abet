﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Resources.DataSet
{
    public class FilaReporteIFC
    {
        public string NOMBRE_CURSO { get; set; }
        public string AREA { get; set; }
        public string CARRERA { get; set; }
        public string ESTADO { get; set; }
    }
}