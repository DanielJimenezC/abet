﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Logic
{
    public class EmailLogic
    {
        private AbetEntities context;
        private Controller Parent;

        public EmailLogic(Controller parent, CargarDatosContext datacontext)
        {
            this.Parent = parent;
            this.context = datacontext.context;
        }

        public void SendEmail(String Asunto, String Plantilla, String Remitente, String Receptor, String Seccion, String Mensaje, object model = null)
        {
            try
            {
                using (var SecurePassword = new SecureString())
                {

                    //Array.ForEach(ConfigurationManager.AppSettings["ClaveCorreoUPC"].ToArray(), SecurePassword.AppendChar);

                    TemplateRender templateRender = new TemplateRender();


                    //papo el helper para sacar tu data :v
                    var mail = AppSettingsHelper.GetAppSettings("AbetEmailCredentialUser");
                    var cred = AppSettingsHelper.GetAppSettings("AbetEmailCredentialPassword");

                    var host = AppSettingsHelper.GetAppSettings("HostAbet");
                    var port = AppSettingsHelper.GetAppSettings("HostAbetPort").ToInteger();
                    //aqui termina el helper si decides usarlo

                    using (var smtpClient = new SmtpClient
                    {
                        Host = "10.10.3.119",
                        Port = 25,
                        EnableSsl = false,
                        Timeout = 60000,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,

                        Credentials = new NetworkCredential("usreiscabet@upc.edu.pe", "escuelasistemas")
                    })
                    {
                        var from = new MailAddress(Remitente);
                        var to = new MailAddress(Receptor);
                        using (var mailMessage = new MailMessage(from, to))
                        {
                            var Message = templateRender.Render(Plantilla, new { MensajeEmail = Mensaje });
                            StringBuilder sb = new StringBuilder(Message);
                            sb.Replace("{PARAM_SECCION}", Seccion);

                            mailMessage.Body = sb.ToString();
                            mailMessage.IsBodyHtml = true;

                            mailMessage.Subject = Asunto;
                            mailMessage.Sender = from;
                            ServicePointManager.ServerCertificateValidationCallback =
                            delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                     X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
                            { return true; };

                            smtpClient.Send(mailMessage);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
