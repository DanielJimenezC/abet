﻿using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Models
{
    public class EVDModel : GenericModel
    {
        public EVDModel(CargarDatosContext ctx, ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "RC";
            this.model.OutcomeTypeName = "Control";
            this.model.DbContext = ctx.context;
        }

        public void LoadDropDownListsReporteEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
    {
            
        ViewModel.ListEvd = model.DropDownListEVD(FirstIdCycle);
      }

        public void LoadDropDownListsInformeEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
        {

            ViewModel.ListEvd = model.DropDownListEVD(FirstIdCycle);
        }


        public void ExistDataInformeEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
        {

            string valor = ViewModel.Languages.First().Key;
            SqlParameter param1 = new SqlParameter("@param1", valor);
            SqlParameter param2 = new SqlParameter("@param2", ViewModel.IdEncuestaVirtualDelegado);
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult_>("exec ReporteEVDComentarios @param1, @param2", param1, param2).Any();
        }

        public void ExistDataReporteEncuestaVirtualEVD(ReporteEncuestaVirtualEVDViewModel ViewModel)
        {
   
            string valor = ViewModel.Languages.First().Key;
            SqlParameter param1 = new SqlParameter("@param1", valor);
            SqlParameter param2 = new SqlParameter("@param2", ViewModel.IdEncuestaVirtualDelegado);
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult_>("exec ReporteEVDComentarios @param1, @param2", param1, param2).Any();
        }

        private class ProcedureResult_
        {
                public string CODIGOALUMNO { get; set;}  
                public string NOMBREALUMNO       { get; set;}
                public string SECCION      { get; set;}
                public string CURSO        { get; set;}
                public string ESTADO       { get; set; }


        }
    private class ProcedureResult
    {
        public int IdSubModalidadPeriodoAcademico { get; set; }
        public string Codigo { get; set; }
        public string Curso { get; set; }
        public string Outcome { get; set; }
        public int IdCurso { get; set; }
        public int IdModulo { get; set; }
        public string Modulo { get; set; }
        public int IdSeccion { get; set; }
        public int IdAlumnoSeccion { get; set; }
        public string Nota { get; set; }
        public string CicloAcademico { get; set; }
        public string Color { get; set; }
        public int Orden { get; set; }
        public int IdEncuestaVirtualDelegado { get; set; }
        }

    }
}