﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Models
{
    public class CustomOutcomeCOmision
    {
        public Int32? IdOutcome { get; set; }
        public Int32? IdComision { get; set; }
        public String OutcomeNombre { get; set; }
        public String OutcomeDescripcion { get; set; }
        public String ComisionNombre { get; set; }
        // public  Outcome outcome { get; set; }
        // public CargarDatosContext DataContext { get; set; }

        public CustomOutcomeCOmision( )
        {
           
        }

    }

}