﻿using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Models
{
    public class GenericModel
    {
        protected ControllerBase controller;
        protected ReportBaseModel model;

        private int firstIdCycle;
        protected int FirstIdCycle
        {
            get
            {
                if (model.cycles != null)
                {
                    firstIdCycle = model.cycles.FirstKey();
                }
                return firstIdCycle;
            }
        }
        private int firstIdCycleFrom;
        protected int FirstIdCycleFrom
        {
            get
            {
                if (model.cyclesFrom != null)
                {
                    firstIdCycleFrom = model.cyclesFrom.FirstKey();
                }
                return firstIdCycleFrom;
            }
        }
        private int firstIdCycleTo;
        protected int FirstIdCycleTo
        {
            get
            {
                if (model.cyclesTo != null)
                {
                    firstIdCycleTo = model.cyclesTo.FirstKey();
                }
                return firstIdCycleTo;
            }
        }

        private int modalityId;
        protected int ModalityId
        {
            get
            {
                if (model.modalities != null)
                {
                    modalityId = model.modalities.FirstKey();
                }
                return modalityId;
            }
        }
        private int firstIdCareer;
        protected int FirstIdCareer
        {
            get
            {
                if (model.careers != null)
                {
                    firstIdCareer = model.careers.FirstKey();
                }
                return firstIdCareer;
            }
        }
        private int firstIdAccreditationType;
        protected int FirstIdAccreditationType
        {
            get
            {
                if (model.accreditationTypes != null)
                {
                    firstIdAccreditationType = model.accreditationTypes.FirstKey();
                }
                return firstIdAccreditationType;
            }
        }
        public GenericModel(ReportBaseModel Model, ControllerBase Controller)
        {
            this.model = Model;
            this.controller = Controller;
        }
    }
}