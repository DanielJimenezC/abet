﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;

namespace UPC.CA.ABET.Presentation.Areas.Survey.Models
{
    public class ReportBaseModel
    {
        public readonly LoadCombosLogic loadCombosLogic;
        public string ReportCode { private get; set; }
        public string OutcomeTypeName { private get; set; }
        public string CurrentCulture { get; set; }
        public string CurrentModality { get; set; }
        public Dictionary<string, string> Languages { get; private set; }
        public Dictionary<string, string> Modalities { get; private set; }
        public AbetEntities DbContext { get; set; }

        public IEnumerable<ComboItem> accreditationTypes;
        public IEnumerable<ComboItem> campus;
        public IEnumerable<ComboItem> evd;
        public IEnumerable<ComboItem> cycles;
        public IEnumerable<ComboItem> cyclesFrom;
        public IEnumerable<ComboItem> cyclesTo;
        public IEnumerable<ComboItem> constituents;
        public IEnumerable<ComboItem> courses;
        public IEnumerable<ComboItem> levels;
        public IEnumerable<ComboItem> acceptanceLevels;
        public IEnumerable<ComboItem> commissions;
        public IEnumerable<ComboItem> careers;
        public IEnumerable<ComboItem> modalities;
        public IEnumerable<ComboItem> submodalities;
        public IEnumerable<ComboItem> modules;
        public IEnumerable<ComboItem> criticalityLevels;
        public IEnumerable<ComboItem> studentOutcomes;
        public IEnumerable<ComboItem> instruments;

        public ReportBaseModel(AbetEntities Context)
        {
            this.loadCombosLogic = new LoadCombosLogic(Context);

            this.DbContext = Context;

            var cultureCookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);

            this.CurrentCulture = cultureCookie == null ? Culture.ESPANOL : cultureCookie.Value ?? Culture.ESPANOL;
            this.CurrentModality = modalityCookie == null ? Modality.PREGRADO_REGULAR : modalityCookie.Value ?? Modality.PREGRADO_REGULAR;

            if (CurrentCulture == Culture.INGLES)
            {
                Languages = new Dictionary<string, string>
                {
                    { Culture.INGLES, "English" },
                    { Culture.ESPANOL, "Spanish" }
                };
                Modalities = new Dictionary<string, string>
                {
                    { Modality.PREGRADO_REGULAR, "Undergraduate Regular" },
                    { Modality.PREGRADO_EPE, "Undergraduate EPE" }
                };
            }
            else
            {
                Languages = new Dictionary<string, string>
                {
                    { Culture.ESPANOL, "Español" },
                    { Culture.INGLES, "Inglés" }
                };
                Modalities = new Dictionary<string, string>
                {
                    { Modality.PREGRADO_REGULAR, "Pregrado Regular" },
                    { Modality.PREGRADO_EPE, "Pregrado EPE" }
                };
            }


        }

        public Func<SqlParameter> TotalRowsParameter = () => new SqlParameter { ParameterName = "TotalRows", Value = 0, Direction = ParameterDirection.Output };

        public SelectList DropDownListCycle(int Skip = 0)
        {
            cycles = loadCombosLogic.ListCycles(Skip);
            return cycles.ToSelectList();
        }
        public SelectList DropDownListCycleforModality(string id, int Skip = 0)
        {
            cycles = loadCombosLogic.ListCyclesFromForModality(id, 0, Skip);
            return cycles.ToSelectList();
        }
        public SelectList DropDownListCyclesFromForModality(int Skip = 0)
        {
            cyclesFrom = loadCombosLogic.ListCyclesFromForModality(ModalityId: CurrentModality);
            return cyclesFrom.ToSelectList();
        }

        public SelectList DropDownListCyclesToForModality(int CycleFromId, int Skip = 0)
        {
            cyclesTo = loadCombosLogic.ListCyclesToForModality(ModalityId: CurrentModality, CycleFromId: CycleFromId);
            return cyclesTo.ToSelectList();
        }
        public SelectList DropDownListAccreditationType(int CycleID = 0)
        {
            accreditationTypes = loadCombosLogic.ListAccreditationTypes(CycleID);
            return accreditationTypes.ToSelectList();
        }
        public SelectList DropDownListAccreditationTypeBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            accreditationTypes = loadCombosLogic.ListAccreditationTypeBetweenCycles(CycleFromID, CycleToID);
            return accreditationTypes.ToSelectList();
        }
        public SelectList DropDownListCommission(int AccreditationID = 0, int CycleID = 0)
        {
            if (AccreditationID == 0)
            {
                AccreditationID = accreditationTypes.FirstKey();
            }
            //if (CycleID == 0)
            //{
            //    CycleID = cycles.FirstKey();
            //}
            commissions = loadCombosLogic.ListCommissions(AccreditationID, CycleID, CurrentCulture);
            return commissions.ToSelectList();
        }
        public SelectList DropDownListCommissionBetweenCycles(int AccreditationID = 0, int CycleFromID = 0, int CycleToID = 0, int CareerID = 0)
        {
            commissions = loadCombosLogic.ListCommissionBetweenCycles(AccreditationID, CycleFromID, CycleToID, CareerID, CurrentCulture);
            return commissions.ToSelectList();
        }
        public SelectList DropDownListModulesBetweenCyclesForModality(int CycleFromId = 0, int CycleToId = 0)
        {
            modules = loadCombosLogic.ListModulesBetweenCyclesForModality(CurrentModality, CycleFromId, CycleToId, CurrentCulture);
            return modules.ToSelectList();
        }
        public SelectList DropDownListModulesForCycleForModality(int CycleId = 0)
        {
            modules = loadCombosLogic.ListModulesForCycleForModality(CurrentModality, CycleId, CurrentCulture);
            return modules.ToSelectList();
        }
        public SelectList DropDownListCommissionByCareer(int AccreditationID = 0, int CareerID = 0, int CycleID = 0)
        {
            commissions = loadCombosLogic.ListCommissionsByCareer(AccreditationID, CareerID, CycleID, CurrentCulture);
            return commissions.ToSelectList();
        }
        public SelectList DropDownListCareerByComission(int CommissionID = 0)
        {
            careers = loadCombosLogic.ListCareersByCommission(CommissionID, CurrentCulture);
            return careers.ToSelectList();
        }
        public SelectList DropDownListCareers()
        {
            careers = loadCombosLogic.ListCareers(CurrentCulture, CurrentModality);
            return careers.ToSelectList();
        }
        public SelectList DropDownListCampus(int CycleID = 0)
        {
            campus = loadCombosLogic.ListCampus(CycleID);
            return campus.ToSelectList();
        }

        public SelectList DropDownListEVD(int CycleID = 0)
        {
            evd = loadCombosLogic.ListEvd(CycleID);
            return evd.ToSelectList();
        }

        public SelectList DropDownListCampusBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            campus = loadCombosLogic.ListCampusBetweenCycles(CycleFromID, CycleToID);
            return campus.ToSelectList();
        }
        public SelectList DropDownListConstituent()
        {
            constituents = loadCombosLogic.ListConstituents(CurrentCulture);
            return constituents.ToSelectList();
        }
        public SelectList DropDownListStudentOutcome(int CommissionID = 0, int? CycleID = 0, int? CareerID = 0, int CampusID = 0, string CourseOutcomeTypeName = "")
        {
            if (string.IsNullOrWhiteSpace(CourseOutcomeTypeName))
            {
                CourseOutcomeTypeName = this.OutcomeTypeName;
            }

            studentOutcomes = loadCombosLogic.ListStudentOutcomes(CommissionID, CycleID.HasValue ? CycleID.Value : 0, CareerID.HasValue ? CareerID.Value : 0, CampusID, CurrentCulture, CourseOutcomeTypeName);
            return studentOutcomes.ToSelectList();
        }
        public SelectList DropDownListStudentOutcomeBetweenCycles(int CommissionID = 0, int? CycleFromID = null, int? CycleToID = null, int? CareerID = null, int CampusID = 0, string CourseOutcomeTypeName = "")
        {
            if (string.IsNullOrWhiteSpace(CourseOutcomeTypeName))
            {
                CourseOutcomeTypeName = this.OutcomeTypeName;
            }

            studentOutcomes = loadCombosLogic.ListStudentOutcomesBetweenCycles(CommissionID, CycleFromID.HasValue ? CycleFromID.Value : 0, CycleToID.HasValue ? CycleToID.Value : 0, CareerID.HasValue ? CareerID.Value : 0, CampusID, CurrentCulture, CourseOutcomeTypeName);
            return studentOutcomes.ToSelectList();
        }
        public SelectList DropDownListStudentOutcomeByCommission(int CommissionID = 0, int CycleID = 0, int AccreditationID = 0)
        {
            studentOutcomes = loadCombosLogic.ListStudentOutcomesByCommission(CommissionID, CycleID, AccreditationID, CurrentCulture);
            return studentOutcomes.ToSelectList();
        }
        public SelectList DropDownListCourseByOutcomeType(int CareerID = 0, int StudentOutcomeID = 0, int CampusID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0, string CourseOutcomeTypeName = "")
        {
            if (string.IsNullOrWhiteSpace(CourseOutcomeTypeName))
            {
                CourseOutcomeTypeName = this.OutcomeTypeName;
            }

            courses = loadCombosLogic.ListCoursesByOutcomeType(CourseOutcomeTypeName, CareerID, StudentOutcomeID, CampusID, CommissionID, CycleFromID, CycleToID, CurrentCulture);
            return courses.ToSelectList();
        }
        public SelectList DropDownListCourseOfFormation(int CareerID = 0, int CampusID = 0, int CycleFromID = 0, int CycleToID = 0)
        {
            courses = loadCombosLogic.ListCoursesOfFormation(CareerID, CampusID, CycleFromID, CycleToID, CurrentCulture);
            return courses.ToSelectList();
        }
        public SelectList DropDownListCourseByConstituent(int ConstituentID, int CareerID = 0, int CycleID = 0, int CampusID = 0, int CommissionID = 0, int StudentOutcomeID = 0)
        {
            courses = loadCombosLogic.ListCoursesByConstituent(ConstituentID, CareerID, CycleID, CampusID, CommissionID, StudentOutcomeID, CurrentCulture);
            return courses.ToSelectList();
        }
        public SelectList DropDownListAcceptanceLevel(string ReportCode = "")
        {
            if (string.IsNullOrWhiteSpace(ReportCode))
            {
                ReportCode = this.ReportCode;
            }

            acceptanceLevels = loadCombosLogic.ListAcceptanceLevels(ReportCode, CurrentCulture);
            return acceptanceLevels.ToSelectList();
        }
        public SelectList DropDownListAcceptanceLevelFindings()
        {
            acceptanceLevels = loadCombosLogic.ListAcceptanceLevelsFindings(CurrentCulture);
            return acceptanceLevels.ToSelectList();
        }


        public SelectList DropDownListCriticality()
        {
            criticalityLevels = loadCombosLogic.ListCriticality(CurrentCulture);
            return criticalityLevels.ToSelectList();
        }


        public SelectList DropDownListInstrument(int ConstituentID = 0, string Acronyms = "")
        {
            instruments = loadCombosLogic.ListInstruments(ConstituentID, Acronyms, CurrentCulture);
            return instruments.ToSelectList();
        }
        public SelectList DropDownListLevel(int FirstIdCycleFrom = 0)
        //public SelectList DropDownListLevel(int FirstIdCycleFrom = 0)
        {
            levels = loadCombosLogic.SpListLevels(FirstIdCycleFrom);
            //levels = loadCombosLogic.ListLevels();
            return levels.ToSelectList();
        }
        public SelectList DropDownListLanguage()
        {
            return Languages.ToSelectList();
        }

        public SelectList DropDownListModalities()
        {
            //Cambios
            //modalities = loadCombosLogic.ListModalities(CurrentCulture);
            //return modalities.ToSelectList();
            return Modalities.ToSelectList();
        }
        /*
         * Sin uso
        public SelectList DropDownListPastCycles()
        {
            cycles = loadCombosLogic.ListCycles();
            return loadCombosLogic.ListPastCycles().ToSelectList();
        }
        */
        public JsonResult GetAccreditationTypes(int CycleID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListAccreditationTypes(CycleID).ToOptions()
            };
        }
        public JsonResult GetAccreditationTypesBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListAccreditationTypeBetweenCycles(CycleFromID, CycleToID).ToOptions()
            };
        }
        public JsonResult GetComissions(int AccreditationID = 0, int CycleID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCommissions(AccreditationID, CycleID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetComissionsByCareer(int AccreditationID = 0, int CareerID = 0, int CycleID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCommissionsByCareer(AccreditationID, CareerID, CycleID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetComissionsBetweenCycles(int AccreditationID = 0, int CycleFromID = 0, int CycleToID = 0, int CareerID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCommissionBetweenCycles(AccreditationID, CycleFromID, CycleToID, CareerID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetCareers()
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCareers(CurrentCulture, CurrentModality).ToOptions()
            };
        }
        public JsonResult GetCareersByCommission(int CommissionID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCareersByCommission(CommissionID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetControlCourses(int CareerID = 0, int StudentOutcomeID = 0, int CampusID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCoursesByOutcomeType("Control", CareerID, StudentOutcomeID, CampusID, CommissionID, CycleFromID, CycleToID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetFormationCourses(int CareerID = 0, int CampusID = 0, int CycleFromID = 0, int CycleToID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCoursesOfFormation(CareerID, CampusID, CycleFromID, CycleToID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetDiscoverCourses(int ConstituentID, int CareerID = 0, int CycleID = 0, int CampusID = 0, int CommissionID = 0, int StudentOutcomeID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCoursesByConstituent(ConstituentID, CareerID, CycleID, CampusID, CommissionID, StudentOutcomeID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetVerificationCourses(int CareerID = 0, int CampusID = 0, int StudentOutcomeID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCoursesByOutcomeType("Verification", CareerID, StudentOutcomeID, CampusID, CommissionID, CycleFromID, CycleToID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetCourseByOutcomeType(int CareerID = 0, int CampusID = 0, int StudentOutcomeID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0, string CourseOutcomeTypeName = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCoursesByOutcomeType(CourseOutcomeTypeName, CareerID, StudentOutcomeID, CampusID, CommissionID, CycleFromID, CycleToID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetInstruments(int ConstituentID = 0, string Acronyms = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListInstruments(ConstituentID, Acronyms, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetStudentOutcomes(int CommissionID = 0, int? CycleID = null, int? CareerID = null, int? CampusID = null, string OutcomeTypeName = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListStudentOutcomes(CommissionID,
                    CycleID.HasValue ? CycleID.Value : 0,
                    CareerID.HasValue ? CareerID.Value : 0,
                    CampusID.HasValue ? CampusID.Value : 0, CurrentCulture, OutcomeTypeName).ToOptions()
            };
        }
        public JsonResult GetStudentOutcomesBetweenCycles(int CommissionID = 0, int? CycleFromID = null, int? CycleToID = null, int? CareerID = null, int? CampusID = null, string OutcomeTypeName = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListStudentOutcomesBetweenCycles(CommissionID,
                    CycleFromID.HasValue ? CycleFromID.Value : 0,
                    CycleToID.HasValue ? CycleToID.Value : 0,
                    CareerID.HasValue ? CareerID.Value : 0,
                    CampusID.HasValue ? CampusID.Value : 0, CurrentCulture, OutcomeTypeName).ToOptions()
            };
        }
        public JsonResult GetStudentOutcomeByCommission(int CommissionID = 0, int CycleID = 0, int AccreditationID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListStudentOutcomesByCommission(CommissionID, CycleID, AccreditationID, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetCampus(int CycleID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCampus(CycleID).ToOptions()
            };
        }
        public JsonResult GetCampusBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCampusBetweenCycles(CycleFromID, CycleToID).ToOptions()
            };
        }
        public JsonResult GetCyclesFromForModality()
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCyclesFromForModality(CurrentModality).ToOptions()
            };
        }
        public JsonResult GetCyclesToForModality(int CycleFromId = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListCyclesToForModality(CurrentModality, CycleFromId: CycleFromId).ToOptions()
            };
        }

        public JsonResult GetModulesBetweenCyclesForModality(int CycleFromId = 0, int CycleToId = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListModulesBetweenCyclesForModality(CurrentModality, CycleFromId, CycleToId, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetModulesForCycleForModality(int CycleId = 0)
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListModulesForCycleForModality(CurrentModality, CycleId, CurrentCulture).ToOptions()
            };
        }
        public JsonResult GetExitStudents(int CycleID = 0, int CareerID = 0, int CampusID = 0, string Text = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListExitStudents(CycleID, CareerID, CampusID, Text)
                    .Select(s => new
                    {
                        id = s.IdAlumno,
                        Nombres = s.Nombres,
                        Apellidos = s.Apellidos,
                        Codigo = s.Codigo
                    })
            };
        }
        public JsonResult GetAcceptanceLevel(string ReportCode = "")
        {
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = loadCombosLogic.ListAcceptanceLevels(ReportCode, CurrentCulture).ToOptions()
            };
        }
    }

    public static class ReportBaseModelExtensions
    {
        public static int FirstKey(this Dictionary<int, string> dictionary)
        {
            return dictionary.Any() ? dictionary.First().Key : 0;
        }
        public static int FirstKey(this IEnumerable<ComboItem> data)
        {
            return data.Any() ? data.First().Key : 0;
        }
        public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new SelectList(dictionary, "Key", "Value");
        }
        public static SelectList ToSelectList(this IEnumerable<ComboItem> data)
        {
            return new SelectList(data, "Key", "Value");
        }
        public static MultiSelectList ToMultiSelectList(this IEnumerable<ComboItem> data, IEnumerable selectedValues)
        {
            return new MultiSelectList(data, "Key", "Value", selectedValues);
        }
        public static IEnumerable<OptionViewModel> ToOptions(this Dictionary<int, string> dictionary)
        {
            return dictionary.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
        public static IEnumerable<OptionViewModel> ToOptions(this IEnumerable<ComboItem> data)
        {
            return data.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
        public static DataTableOrderRequestViewModel CurrentOrdered(this IList<DataTableOrderRequestViewModel> list)
        {
            return list.FirstOrDefault() ?? new DataTableOrderRequestViewModel();
        }
        public static T Value<T>(this DbDataReader reader, string columnName) where T : IConvertible
        {
            var ordinal = reader.GetOrdinal(columnName);
            var value = reader.GetValue(ordinal) ?? default(T);
            return (T)Convert.ChangeType(value, typeof(T));
        }
        public static Nullable<T> ValueNullable<T>(this DbDataReader reader, string columnName) where T : struct
        {
            var ordinal = reader.GetOrdinal(columnName);
            var value = reader.GetValue(ordinal);

            if (value == DBNull.Value)
            {
                return null;
            }

            return (T)value;
        }
    }
}