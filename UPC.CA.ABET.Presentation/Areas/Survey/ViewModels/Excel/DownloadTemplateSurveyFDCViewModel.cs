﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel
{
    public class DownloadTemplateSurveyFDCViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        //public Int32? cicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }
        public Int32? carreraId { get; set; }
        public Int32? acreditadoraId { get; set; }
        public List<Acreditadora> lstAcreditadora { get; set; }
        //public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public List<Carrera> lstCarrera { get; set; }
        public String Tipo { get; set; }
        //public void Fill(CargarDatosContext dataContext, Int32? AcreditadoraId, Int32? CarreraId, Int32? CicloId)
        public void Fill(CargarDatosContext dataContext, Int32? AcreditadoraId, Int32? CarreraId, Int32? SubModalidadPeriodoAcademicoId)
        {
            //carreraId = CarreraId ?? null;
            //cicloId = CicloId ?? null;
            carreraId = CarreraId ?? null;
            subModalidadPeriodoAcademicoId = SubModalidadPeriodoAcademicoId ?? null;
            this.Tipo = ConstantHelpers.ENCUESTA.FDC;
            lstAcreditadora = dataContext.context.Acreditadora.ToList();
            //lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstSubMOdalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
        }
    }
}