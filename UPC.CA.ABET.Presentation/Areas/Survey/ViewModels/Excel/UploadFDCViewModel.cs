﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel
{
    public class UploadFDCViewModel
    {
        public Int32? AcreditadoraId { get; set; }
        public Int32? SedeId { get; set; }
        //public Int32? cicloId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }
        public Int32? CursoId { get; set; }
        public Int32? SeccionId { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public List<Acreditadora> lstAcreditadora { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        //public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }

        public UploadFDCViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext)
        {
            lstAcreditadora = dataContext.context.Acreditadora.ToList();
            lstSede = dataContext.context.Sede.ToList();
            //lstCiclo = dataContext.context.PeriodoAcademico.OrderBy(x => x.CicloAcademico).ToList();
            lstSubMOdalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.OrderBy(x => x.IdPeriodoAcademico).ToList();
            lstCurso = new List<Curso>();
            lstSeccion = new List<Seccion>();
            
                
        }
    }
}