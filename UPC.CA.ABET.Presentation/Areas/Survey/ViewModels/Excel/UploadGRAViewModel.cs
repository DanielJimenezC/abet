﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel
{
    public class UploadGRAViewModel
    {
        public Int32? CarreraId { get; set; }
        public Int32? SedeId { get; set; }
        //public Int32? cicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Sede> lstSede { get; set; }
        //public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        public UploadGRAViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            lstSede = new List<Sede>();
            //lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstSubMOdalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
        }
    }
}