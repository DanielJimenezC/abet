﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel
{
    public class UploadPPPViewModel
    {
        public Int32? CarreraId { get; set; }
        public Int32? SedeId { get; set; }
        public Int32? CicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        public UploadPPPViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext, int ModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();

            int SubModalidadId = dataContext.context.SubModalidad.FirstOrDefault(x => x.IdModalidad == ModalidadId).IdSubModalidad;

            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId && x.IdSubmodalidad== SubModalidadId).ToList();

       

            lstSede = new List<Sede>();


            //CAMBIARLO POR VARIABLE DE SWITCH MODALIDAD
            lstCiclo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submoda.IdSubModalidad == SubModalidadId)
                        select pa).ToList();

            lstSubMOdalidadPeriodoAcademico = (from a in dataContext.context.SubModalidadPeriodoAcademico
                                               join b in dataContext.context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
                                               where b.IdSubModalidad == SubModalidadId
                                               select a).OrderBy(x => x.IdPeriodoAcademico).ToList();
                                              

        }
    }
}