﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class _VerArchivoViewModel
    {
        public String Archivo { get; set; }
        public String Nombre { get; set; }
        public void Fill(String Archivo, String Nombre)
        {
            this.Archivo = Archivo;
            this.Nombre = Nombre;
        }
    }
}