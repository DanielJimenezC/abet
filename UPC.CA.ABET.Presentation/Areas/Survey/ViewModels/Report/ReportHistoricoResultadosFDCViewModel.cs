﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReportHistoricoResultadosFDCViewModel
    {
        public String Tipo { get; set; }
        public Int32? IdDesde { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public Int32? IdHasta { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCurso { get; set; }
        public List<SelectListItem> lstCurso { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public Int32? CantSuccess { get; set; }
        public string fileName { get; set; }
        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess)
        {
            CantSuccess = cantSuccess;
            Tipo = ConstantHelpers.ENCUESTA.FDC;
            Key = key;
            Extension = extension;
            lstCiclo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            lstCurso  = new List<SelectListItem>();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
        }
    }
}