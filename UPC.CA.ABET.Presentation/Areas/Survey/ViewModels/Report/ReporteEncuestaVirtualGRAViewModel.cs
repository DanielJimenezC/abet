﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.MaintenanceSurvey.MaintenanceSurveyGRA;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReporteEncuestaVirtualGRAViewModel
    {
        [Required]
        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdCarrera { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public Int32? CantSuccess { get; set; }
        public Int32? NumeroPagina { get; set; }
        public string fileName { get; set; }

        public Int32 idIdioma { get; set; }
        public List<SelectListItem> LstIdioma { get; set; }

        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess, Int32 parModalidadId,Int32 parEscuelaId)
        {
            try
            {
				
               
                LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                       join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                       join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                       where (submoda.IdModalidad == parModalidadId)
                                       select pa
                                                  ).ToList();
             
                LstCarrera = new List<Carrera>();

                Carrera objCarrera = new Carrera();
                LstCarrera = (from ca in dataContext.context.Carrera
                                           join capa in dataContext.context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                                           join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                                           join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                           where (submoda.IdModalidad == parModalidadId && ca.IdEscuela==parEscuelaId)
                                           select ca).Distinct().ToList();

                objCarrera.NombreEspanol = "[-Todas-]";
                objCarrera.NombreIngles = "[-All-]";
                LstCarrera.Add(objCarrera);

                CantSuccess = cantSuccess;
                Key = key;
                Extension = extension;
                var language = dataContext.currentCulture;
                LstIdioma = new List<SelectListItem>();
                if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
                {                    
                    LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Español" });
                    LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "Inglés" });
                }
                else
                {                 
                    LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Spanish" });
                    LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "English" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
