﻿
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Survey;
namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReporteEncuestaVirtualEVDViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(SurveyResources), ErrorMessageResourceName = "RequiereEVD")]
        public int IdEncuestaVirtualDelegado { get; set; }
    }
}
