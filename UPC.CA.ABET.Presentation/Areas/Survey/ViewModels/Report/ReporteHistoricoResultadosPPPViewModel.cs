﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReporteHistoricoResultadosPPPViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdTipoOutcome { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdNumeroPractica { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<TipoOutcomeEncuesta> LstTipoOutcome { get; set; }
        public List<Sede> LstSede { get; set; }
        public List<NumeroPractica> LstNumeroPractica { get; set; }
        public String Tipo { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public String NombreOutcome { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? CantSuccess { get; set; }
        public Int32? IdDesde { get; set; }
        public Int32? IdHasta { get; set; }
        public List<SelectListItem> LstOutcomes { get; set; }
        public string fileName { get; set; }
        public Int32 idIdioma { get; set; }
        public List<SelectListItem> LstIdioma { get; set; }
        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess, Int32 parModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();


            LstCarrera = (from ca in dataContext.context.Carrera
                          join capa in dataContext.context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join es in dataContext.context.Escuela on ca.IdEscuela equals es.IdEscuela
                          where (submoda.IdModalidad == parModalidadId && es.IdEscuela == escuelaId) 
                          select ca).Distinct().ToList();

            LstSede = dataContext.context.Sede.ToList();

            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                   join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                   where (submoda.IdModalidad == parModalidadId)
                                   select pa
                                    ).ToList();

            LstComision = new List<Comision>();
            LstNumeroPractica = dataContext.context.NumeroPractica.ToList();
            CantSuccess = cantSuccess;
            Tipo = ConstantHelpers.ENCUESTA.PPP;
            Key = key;
            Extension = extension;
            LstOutcomes = new List<SelectListItem>();
            LstIdioma = new List<SelectListItem>();
            var language = dataContext.currentCulture;
            if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
            {                
                LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Español" });
                LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "Inglés" });
            }
            else
            {             
                LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Spanish" });
                LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "English" });
            }
        }
    }
}

