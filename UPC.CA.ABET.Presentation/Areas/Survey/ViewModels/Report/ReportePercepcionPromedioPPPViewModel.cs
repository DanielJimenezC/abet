﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase;


namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReportePercepcionPromedioPPPViewModel: ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdComision { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdNumeroPractica { get; set; }
        public Int32 idIdioma { get; set; }
        public List<SelectListItem> LstIdioma { get; set; }

        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<SelectListItem> LstSede { get; set; }
        public List<NumeroPractica> LstNumeroPractica { get; set; }

        public String Tipo { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public Int32? CantSuccess { get; set; }
        public String fileName { get; set; }
       


        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess, Int32 parModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();

            LstCarrera = (from ca in dataContext.context.Carrera
                          join capa in dataContext.context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();



            LstSede = dataContext.context.Sede.Select(x => new SelectListItem { Value = x.IdSede.ToString(), Text = x.Nombre }).ToList();

            var language = dataContext.currentCulture;
            LstIdioma = new List<SelectListItem>();
            if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
            {
                LstSede.Insert(0, new SelectListItem { Value = "0", Text = "TODOS" });                
                LstIdioma.Insert(0, new SelectListItem { Value="1", Text = "Español"});
                LstIdioma.Insert(1, new SelectListItem { Value="2", Text = "Inglés"});
            }
            else
            {
                LstSede.Insert(0, new SelectListItem { Value = "0", Text = "ALL" });                
                LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Spanish" });
                LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "English" });
            }
            

            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                   join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                   where (submoda.IdModalidad == parModalidadId)
                                   select pa
                                    ).ToList();

            LstComision = new List<Comision>();

            //System.Diagnostics.Debug.WriteLine("ww " + IdCarrera);

            LstNumeroPractica = dataContext.context.NumeroPractica.ToList();
            CantSuccess = cantSuccess;
            Tipo = ConstantHelpers.ENCUESTA.GRA;
            Key = key;
            Extension = extension; 
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;
        }
    }
}
