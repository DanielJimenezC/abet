﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class ReportResultadosFDCViewModel
    {
        public String Tipo { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public Int32? EncuestaId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCiclo { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdSeccion { get; set; }
        public Int32? IdNivelAcademico { get; set; }
        public Decimal PuntajeTotal { get; set; }
        public Int32 IdResultadoObtenido { get; set; }
        public List<NivelAcademico> lstNivelAcademico { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public List<Sede> lstSede { get; set; }
        //public List<SelectListItem> ListaSede2 { get; set; }
        public List<EncuestaComentario> lstComentario { get; set; }
        public List<ResultadoFDC> lstResultado { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public Int32? CantSuccess { get; set; }
        public string fileName { get; set; }

        public void CargarDatos(CargarDatosContext dataContext,Int32 idCiclo,Int32? idCurso, Int32? idSeccion)
        {

            IdCiclo = idCiclo;
            IdCurso = idCurso;
            IdSeccion = idSeccion;

            Tipo = ConstantHelpers.ENCUESTA.FDC;
            //lstCiclo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            lstCiclo = (from ciclo in dataContext.context.PeriodoAcademico join sub in dataContext.context.SubModalidadPeriodoAcademico on ciclo.IdPeriodoAcademico equals sub.IdPeriodoAcademico where sub.IdSubModalidad == 1 orderby ciclo.IdPeriodoAcademico descending select ciclo).ToList();
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdSubmodalidad == escuelaId && x.IdEscuela == 1).ToList();
            lstCurso = new List<Curso>();
            if (IdCurso.HasValue)
            {
                lstSeccion = dataContext.context.SeccionPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo).Select(x => x.Seccion).ToList();
                
            }
            else
                lstSeccion = new List<Seccion>();
            lstSede = dataContext.context.Sede.ToList();
            lstNivelAcademico = dataContext.context.NivelAcademico.ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault( x => x.Acronimo == Tipo).IdTipoEncuesta;
            
        }
        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess)
        {
            CantSuccess = cantSuccess;
            Tipo = ConstantHelpers.ENCUESTA.FDC;
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == Tipo).IdTipoEncuesta;
            Key = key;
            Extension = extension;
            //lstCiclo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            lstCiclo = (from ciclo in dataContext.context.PeriodoAcademico join sub in dataContext.context.SubModalidadPeriodoAcademico on ciclo.IdPeriodoAcademico equals sub.IdPeriodoAcademico where sub.IdSubModalidad == 1 orderby ciclo.IdPeriodoAcademico descending select ciclo).ToList();
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdSubmodalidad == escuelaId && x.IdEscuela == 1).ToList();
            lstCurso = dataContext.context.Curso.ToList();
            lstSeccion = new List<Seccion>();
            lstSede = dataContext.context.Sede.ToList();
            string language = dataContext.currentCulture;
            if (language == "es-PE")
            {
                lstSede.Insert(0, new Sede { IdSede = 0, Nombre = "TODOS" });
            }
            else
            {
                lstSede.Insert(0, new Sede { IdSede = 0, Nombre = "ALL" });
            }
            
            lstNivelAcademico = dataContext.context.NivelAcademico.ToList();
            if (EncuestaId.HasValue)
            {
                var encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == EncuestaId);
                IdCiclo = encuesta.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                IdCarrera = encuesta.IdCarrera.Value;
                IdCurso = encuesta.IdCurso.Value;
                IdSeccion = encuesta.IdSeccion.Value;
                IdSede = encuesta.IdSede.Value;
                lstComentario = encuesta.EncuestaComentario.ToList();
                lstResultado = encuesta.ResultadoFDC.ToList();
            }
        }
    }
}