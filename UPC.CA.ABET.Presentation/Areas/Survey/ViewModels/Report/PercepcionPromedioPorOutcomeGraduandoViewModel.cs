﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Report
{
    public class PercepcionPromedioPorOutcomeGraduandoViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdTipoOutcome { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Comision> LstComision { get; set; }
        public Int32? CantSuccess { get; set; }
        public Int32 IdComision { get; set; }
        public String Tipo { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public String Key { get; set; }
        public String Extension { get; set; }
        public string fileName { get; set; }
        public Int32 idIdioma { get; set; }
        public List<SelectListItem> LstIdioma { get; set; }


        public void CargarDatos(CargarDatosContext dataContext, Int32 idCarrera, Int32 idPeriodoAcademico, Int32 idTipoOutcome, Int32 parModalidadId)
        {
            IdCarrera = idCarrera;
            IdPeriodoAcademico = idPeriodoAcademico;
            IdTipoOutcome = idTipoOutcome;

            var escuelaId = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            LstComision = new List<Comision>();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
        }
        public void Fill(CargarDatosContext dataContext, String key, String extension, Int32? cantSuccess, Int32 parModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();

            LstCarrera = (from ca in dataContext.context.Carrera
                          join capa in dataContext.context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          where (submoda.IdModalidad == parModalidadId && ca.IdEscuela==escuelaId)
                          select ca).Distinct().ToList();

            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                   join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                   where (submoda.IdModalidad == parModalidadId)
                                   select pa
                                    ).ToList();


            LstComision = new List<Comision>();

            CantSuccess = cantSuccess;
            Tipo = ConstantHelpers.ENCUESTA.GRA;
            Key = key;
            Extension = extension;
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
            LstIdioma = new List<SelectListItem>();
            var language = dataContext.currentCulture;
            if (language.Equals(ConstantHelpers.CULTURE.ESPANOL))
            {
                LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Español" });
                LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "Inglés" });
            }
            else
            {
                LstIdioma.Insert(0, new SelectListItem { Value = "1", Text = "Spanish" });
                LstIdioma.Insert(1, new SelectListItem { Value = "2", Text = "English" });
            }
        }
    }
}
