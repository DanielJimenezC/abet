﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditFindHallazgoIRDViewModel
    {
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdAcreditadora { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? HallazgoId { get; set; }
        public Int32? IdCriticidad { get; set; }


        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Acreditadora> LstAcreditadora { get; set; }
        public List<Sede> LstSede { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }


        public AddEditFindHallazgoIRDViewModel()
        {
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstCarrera = new List<Carrera>();

            LstComision = new List<Comision>();
            LstCurso = new List<Curso>();
            LstNivelAceptacion = new List<NivelAceptacionHallazgo>();

            LstOutcome = new List<Outcome>();
            LstAcreditadora = new List<Acreditadora>();
            LstSede = new List<Sede>();
            LstCriticidad = new List<Criticidad>();

        }

        public void Fill(CargarDatosContext dataContext, Int32? idHallazgo)
        {
            HallazgoId = idHallazgo;

            var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == idHallazgo);

            IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
            IdCriticidad = hallazgo.IdCriticidad;
            IdSede = hallazgo.IdSede;
            IdComision = hallazgo.IdComision;
            IdCurso = hallazgo.IdCurso;
            IdAcreditadora = hallazgo.IdAcreditadora;
            IdSubModalidadPeriodoAcademico = hallazgo.IdSubModalidadPeriodoAcademico;
            IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == hallazgo.IdSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;
            DescripcionEspanol = hallazgo.DescripcionEspanol;
            DescripcionIngles = hallazgo.DescripcionIngles;
            IdOutcome = hallazgo.IdOutcome;
            IdCarrera = hallazgo.IdCarrera;

            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            LstSede = dataContext.context.Sede.ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();


            var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          where oc.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                          select cmc.IdCurso);


            LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).ToList();

            var queryMC = (from mc in dataContext.context.MallaCocos
                           join mcd in dataContext.context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                           where (mc.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico)
                           join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                           join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                           where (cmc.IdCurso == IdCurso)
                           select oc.IdOutcomeComision);


            var data = dataContext.context.OutcomeComision.Where(x => (queryMC).Contains(x.IdOutcomeComision)).Select(x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre, NombreIngles = x.Comision.NombreIngles + " | " + x.Outcome.NombreIngles }).ToList();

            foreach (var item in data)
            {
                LstOutcome.Add(new Outcome() { IdOutcome = item.IdOutcome, Nombre = item.Nombre, NombreIngles = item.NombreIngles });
            }
        }

        public void GenerarComisionAcreditadora(CargarDatosContext dataContext)
        {
            
            IdComision = dataContext.context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == IdOutcome && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).IdComision;

            int IdAcreditadoraPeriodoAcademico = dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).IdAcreditadoraPeriodoAcademico;

            IdAcreditadora = dataContext.context.AcreditadoraPeriodoAcademico.FirstOrDefault(x => x.IdAcreditadoraPreiodoAcademico == IdAcreditadoraPeriodoAcademico).IdAcreditadora;

        }

        public void GenerarCarreraComision(CargarDatosContext dataContext)
        {
            IdCarrera = dataContext.context.CarreraComision.FirstOrDefault(x => x.IdComision == IdComision && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).IdCarrera;
        }
    }
}