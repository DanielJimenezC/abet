﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.AddEditSurvey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditSurveyPPPViewModel
    {
        public String Tipo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]

        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
     
        public Int32? IdCiclo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]

        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public bool MallaArizona { get; set; } 
        public Int32 IdSeccion { get; set; }
        public Int32 IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }

        public String CodigoAlumno { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]

        public String NombreAlumno { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]

        public Int32 NroPractica { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        [Display(Name = " ")]
        public DateTime? FechaInicio { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        [Display(Name = " ")]
        public DateTime? FechaFin { get; set; }

        //[StringLength(11, MinimumLength = 11, ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "RUCValidation")]
        public String RUC { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String RazonSocial { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String JefeDirecto { get; set; }

        public String TelefonoJefeDirecto { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String EmailJefeDirecto { get; set; }
        public Int32 CantidadMaxima { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public List<OutcomeEncuestaPPPConfig> lstCE { get; set; }
        public List<PreguntaAdicional> lstPreguntaAdicional { get; set; }
        public List<OutcomeEncuestaPPPConfig> lstCG { get; set; }
        public List<PerformanceEncuestaPPP> lstPerformanceCE { get; set; }
        public List<PerformanceEncuestaPPP> lstPerformanceCG { get; set; }
        public List<PerformanceEncuestaPPP> lstPerformancePA { get; set; }
        public List<Sede> lstSede { get; set; }
        public Int32? EncuestaId { get; set; }
        public Int32? EscuelaId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String CodigoEncuesta { get; set; }

        //[StringLength(3, MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        [Display(Name = "TotalHoras", ResourceType = typeof(Resources.Views.AddEditSurvey.AddEditSurveyPPP.AddEditPPPResource))]
        public Int32 TotalHoras { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String CargoJefe { get; set; }

        public String Comentario { get; set; }

        public List<NumeroPractica> lstNroPractica { get; set; }

        public List<SelectListItem> lstCodigoAlumno { get; set; }
        public String NombreCarrera { get; set; }
        public String NombreSede { get; set; }
        public String NombreCiclo { get; set; }
        public String CodigoCarrera { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? _EncuestaId, Int32? CicloId, Int32? CarreraId)
        {

            IdCiclo = CicloId;

            if (CicloId != null)
            {
                IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdCiclo).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                MallaArizona = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdCiclo).FirstOrDefault().EsMallaArizona;
            }
            

            if(CarreraId.HasValue)
                EscuelaId = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == CarreraId.Value).IdEscuela;


            var currentCulture = dataContext.currentCulture;

            Tipo = ConstantHelpers.ENCUESTA.PPP;
            EncuestaId = _EncuestaId;
            lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstPerformanceCE = new List<PerformanceEncuestaPPP>();
            lstPerformanceCG = new List<PerformanceEncuestaPPP>();
            lstPerformancePA = new List<PerformanceEncuestaPPP>();

            lstCE = new List<OutcomeEncuestaPPPConfig>();
            lstCG = new List<OutcomeEncuestaPPPConfig>();
            lstPreguntaAdicional = new List<PreguntaAdicional>();

            lstCodigoAlumno = new List<SelectListItem>();
            lstNroPractica = dataContext.context.NumeroPractica.ToList();

            if (EncuestaId.HasValue)
            {

                var encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == EncuestaId);
                         
                EscuelaId = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == encuesta.IdCarrera).IdEscuela;
                CodigoCarrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == encuesta.IdCarrera).Codigo;
                var alumno = dataContext.context.Alumno.FirstOrDefault(x => x.Codigo == encuesta.CodigoAlumno);
                if (encuesta.IdSede != null)
                    IdSede = encuesta.IdSede.Value;
                if (encuesta.IdSede != null)
                    NombreSede = dataContext.context.Sede.FirstOrDefault(x => x.IdSede == IdSede).Nombre;
                IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                NombreCiclo = alumno.AlumnoMatriculado.FirstOrDefault().SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
                IdCarrera = alumno.AlumnoMatriculado.FirstOrDefault().IdCarrera.Value;
                NombreCarrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? alumno.AlumnoMatriculado.FirstOrDefault().Carrera.NombreEspanol : alumno.AlumnoMatriculado.FirstOrDefault().Carrera.NombreIngles;
                CodigoAlumno = encuesta.CodigoAlumno;
                NombreAlumno = encuesta.Alumno.Nombres + " " + encuesta.Alumno.Apellidos;
                NroPractica = encuesta.NumeroPractica.numero.Value;
                if (encuesta.FechaInicio != null)
                    FechaInicio = encuesta.FechaInicio;
                if (encuesta.FechaFin != null)
                    FechaFin = encuesta.FechaFin;
                if (encuesta.RUC != null)
                    RUC = encuesta.RUC;
                if (encuesta.RazonSocial != null)
                    RazonSocial = encuesta.RazonSocial;
                if (encuesta.NombreJefe != null)
                    JefeDirecto = encuesta.NombreJefe;
                if (encuesta.CargoJefe != null)
                    CargoJefe = encuesta.CargoJefe;
                if (encuesta.TelefonoJefe != null)
                    TelefonoJefeDirecto = encuesta.TelefonoJefe;
                if (encuesta.CorreoJefe != null)
                    EmailJefeDirecto = encuesta.CorreoJefe;
                if (encuesta.Comentario != null)
                    Comentario = encuesta.Comentario;
                if (encuesta.TotalHoras != null)
                    TotalHoras = encuesta.TotalHoras.Value;
                CodigoAlumno = encuesta.CodigoAlumno;
                CodigoEncuesta = encuesta.CodigoEncuesta == null ? "-" : encuesta.CodigoEncuesta.Value.ToSafeString();
                if (encuesta.RazonSocial != null)
                    RazonSocial = encuesta.RazonSocial;

                var performance = dataContext.context.PerformanceEncuestaPPP.Include(x => x.OutcomeEncuestaPPPConfig).Include(x => x.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == EncuestaId).OrderBy(x => x.OutcomeEncuestaPPPConfig.Orden).ToList();
                foreach (var item in performance)
                {
                    if (item.OutcomeEncuestaPPPConfig != null)
                    {
                        if (item.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                        {
                            lstPerformanceCE.Add(item);
                        }
                        else
                        {
                            lstPerformanceCG.Add(item);
                        }
                    }
                    else
                    {
                        lstPerformancePA.Add(item);
                    }
                }
                var lstOutcome = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdEscuela == EscuelaId && x.EsVisible == true).OrderBy(x => x.Orden).AsQueryable();

                if (IdSubModalidadPeriodoAcademico.HasValue)
                    lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();

                

                lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
                lstPreguntaAdicional = dataContext.context.PreguntaAdicional.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            }
            else
            {
                if (CarreraId.HasValue)
                {
                    IdCarrera = CarreraId;
                    CodigoCarrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == CarreraId).Codigo;
                }
                if (MallaArizona)
                {
                    //
                    var lstOutcome = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdEscuela == EscuelaId && x.EsVisible == true && CarreraId == IdCarrera).OrderBy(x => x.Orden).AsQueryable();
                    if (IdSubModalidadPeriodoAcademico.HasValue)
                        lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();
                    if (EscuelaId.HasValue)
                        lstOutcome = lstOutcome.Where(x => x.IdEscuela == EscuelaId).AsQueryable();
                    lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
                    if (IdCarrera.HasValue)
                        lstOutcome = lstOutcome.Where(x => x.IdCarrera == IdCarrera).AsQueryable();

                    lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                    
                    lstPreguntaAdicional = dataContext.context.PreguntaAdicional.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
                }
                else {
                    var lstOutcome = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdEscuela == EscuelaId && x.EsVisible == true).OrderBy(x => x.Orden).AsQueryable();
                    if (IdSubModalidadPeriodoAcademico.HasValue)
                        lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();
                    if (EscuelaId.HasValue)
                        lstOutcome = lstOutcome.Where(x => x.IdEscuela == EscuelaId).AsQueryable();

                    lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                    lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
                    lstPreguntaAdicional = dataContext.context.PreguntaAdicional.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
                }

               
            }
        }
    }
}