﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditFindSurveyFDCViewModel
    {
        public Int32 IdEncuesta { get; set; }
        public Encuesta Encuesta { get; set; }
        public CursoMallaCurricular Logro { get; set; }
        public List<ResultadoFDC> lstResultadoFDC { get; set; }
        public List<EncuestaComentario> lstComentario { get; set; }
        public String CodigoHallazgo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCriterio { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacion { get; set; }
        public Int32? IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public List<CriterioHallazgoEncuesta> lstCriterios { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public List<GetHallazgosFDCPorCodigoEncuesta_Result> lstHallazgos { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 _IdEncuesta, String _CodigoHallazgo, Int32? _IdSubModalidadPeriodoAcademico)
        {
            IdEncuesta = _IdEncuesta;
            CodigoHallazgo = _CodigoHallazgo;
            this.IdSubModalidadPeriodoAcademico = _IdSubModalidadPeriodoAcademico;
            Encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == IdEncuesta);
            lstComentario = dataContext.context.EncuestaComentario.Where(x => x.IdEncuesta == IdEncuesta).ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault( x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
            lstCriterios = dataContext.context.CriterioHallazgoEncuesta.ToList();
            lstSede = dataContext.context.Sede.ToList();
            lstCurso = dataContext.context.Curso.ToList();
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            lstHallazgos = dataContext.context.GetHallazgosFDCPorCodigoEncuesta(dataContext.context.Instrumento.FirstOrDefault( x => x.Acronimo == "LCFC").IdInstrumento, _IdSubModalidadPeriodoAcademico, IdTipoEncuesta,IdEncuesta).ToList();
            Logro = dataContext.context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == Encuesta.IdCurso);
            lstResultadoFDC = Encuesta.ResultadoFDC.ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            if (!String.IsNullOrEmpty(CodigoHallazgo))
            {
                var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.Codigo == CodigoHallazgo);
                IdCriterio = hallazgo.IdCriterioHallazgoEncuesta;
                IdNivelAceptacion = hallazgo.IdNivelAceptacionHallazgo;
                IdSede = hallazgo.Encuesta.IdSede;
                IdCurso = hallazgo.IdCurso;
                IdCarrera = hallazgo.IdCarrera;
                DescripcionEspanol = hallazgo.DescripcionEspanol;
                DescripcionIngles = hallazgo.DescripcionIngles;
            }
            else
            {
                IdSede = ConvertHelpers.ToInteger(Encuesta.IdSede);
                IdCurso = Encuesta.IdCurso;
                IdCarrera = Encuesta.IdCarrera;
            }
        }
    }
}