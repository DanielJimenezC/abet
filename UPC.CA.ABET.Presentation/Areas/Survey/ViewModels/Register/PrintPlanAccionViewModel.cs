﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Survey.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class PrintPlanAccionViewModel
    {
        public int CursoID { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public String PeriodoAcademicoCodigo { get; set; }
        public String CursoNombre { get; set; }
        public List<int?> LstIdOutcome { get; set; }
        public List<OutcomeComision> LstOutcomeComision { get; set; }
      
       // public List<Outcome, Comision> LstObjectOutcomeComision { get; set; }

        public List<CustomOutcomeCOmision> resultado { get; set; }
        public PrintPlanAccionViewModel()
        {
            LstIdOutcome = new List<int?>();
             LstOutcomeComision = new List<OutcomeComision>();
        }


        public void CargarDatos(CargarDatosContext dataContext, int IDcurso)
        {


            IdSubModalidadPeriodoAcademico = dataContext.session.GetSubModalidadPeriodoAcademicoId();

            this.CursoID = IDcurso;
            PeriodoAcademicoCodigo = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).PeriodoAcademico.CicloAcademico;
            var curso = dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == CursoID);
            CursoNombre = curso.NombreEspanol;


            IQueryable<int?> query0 = dataContext.context.MallaCocos.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).Select(u => (int?)u.IdMallaCocos);

            // IQueryable<int?> LstIdCursoMallaCurricular = dataContext.context.CursoMallaCurricular.Where(x => x.IdCurso == CursoID).Select(u => (int?)u.IdCursoMallaCurricular);

            //1
            //resultado = dataContext.context.CursoMallaCurricular.Where(x => x.IdCurso == CursoID).Select(u =>u.IdCursoMallaCurricular).ToList();

            IQueryable<int?> query1 = dataContext.context.CursoMallaCurricular.Where(x => x.IdCurso == CursoID).Select(u => (int?) u.IdCursoMallaCurricular);


            //2
            // resultado = dataContext.context.MallaCocosDetalle.Where(x=>(query1).Contains(x.IdCursoMallaCurricular)).Select(u =>u.IdOutcomeComision).ToList();


            IQueryable<int?> query2 = dataContext.context.MallaCocosDetalle.Where(x => (query1).Contains(x.IdCursoMallaCurricular) && (query0).Contains(x.IdMallaCocos)).Select(u => (int?) u.IdOutcomeComision);

            //3

            resultado = dataContext.context.OutcomeComision.Where(x => (query2).Contains(x.IdOutcomeComision)).Select(u => new CustomOutcomeCOmision { OutcomeDescripcion= u.Outcome.DescripcionEspanol, ComisionNombre = u.Comision.NombreEspanol, IdComision = u.IdComision, OutcomeNombre = u.Outcome.Nombre, IdOutcome = u.IdOutcome }).ToList();



        }


    }
}