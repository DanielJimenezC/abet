﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class ListFindOutcomeSurveyFDCViewModel
    {
        public List<uspGetOutcomesCursoHallazgos_Result> lstOutcomesHallazgos { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public Int32? numeroPagina { get; set; }
        public List<Hallazgo> lstHallazgosEncontrados { get; set; }

        public ListFindOutcomeSurveyFDCViewModel()
        {
        }

        public void Fill(CargarDatosContext dataContext, Int32? _numeroPagina, String TipoEncuesta, Int32? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                this.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico;
                lstCiclo = dataContext.context.PeriodoAcademico.ToList();
                var escuelaId = dataContext.session.GetEscuelaId();
                LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
                numeroPagina = _numeroPagina ?? 1;
                Int32 IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == TipoEncuesta).IdTipoEncuesta;
                IQueryable<uspGetOutcomesCursoHallazgos_Result> query = dataContext.context.uspGetOutcomesCursoHallazgos(IdTipoEncuesta, IdSubModalidadPeriodoAcademico).AsQueryable();
                lstOutcomesHallazgos = query.ToList();
                lstHallazgosEncontrados = dataContext.context.Hallazgo.Where(x => x.IdEncuesta == null && x.IdTipoEncuesta == null && x.IdInstrumento == (dataContext.context.Instrumento.FirstOrDefault(y => y.Acronimo == "LCFC")).IdInstrumento && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();
            } catch(Exception ex)
            {
                throw;
            }
        }
        public void FindOutcomesHallazgos(CargarDatosContext dataContext, Int32? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                this.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico;
                lstCiclo = dataContext.context.PeriodoAcademico.ToList();
                var escuelaId = dataContext.session.GetEscuelaId();
                LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
                Int32 IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
                IQueryable<uspGetOutcomesCursoHallazgos_Result> query = dataContext.context.uspGetOutcomesCursoHallazgos(IdTipoEncuesta, IdSubModalidadPeriodoAcademico).AsQueryable();
                lstOutcomesHallazgos = query.ToList();
                lstHallazgosEncontrados = dataContext.context.Hallazgo.Where(x => x.IdEncuesta == null && x.IdTipoEncuesta == null && x.IdInstrumento == (dataContext.context.Instrumento.FirstOrDefault(y => y.Acronimo == "LCFC")).IdInstrumento && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}