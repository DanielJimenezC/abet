﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.AddEditSurvey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class LstHallazgosGRAViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdComision { get; set; }
        public Int32? IdModalidad { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdPeriodoAcademico { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNumeroPractica { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Modalidad> LstModalidad { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<NumeroPractica> LstNroPractica { get; set; }
        public Int32? NumeroPagina { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacionEncuesta { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public Boolean Search { get; set; }

        public List<String> LstOutcome { get; set; }
        public List<String> LstSede { get; set; }
        public List<Double> LstPuntaje { get; set; }
        public Dictionary<String, List<Double>> DicSerie { get; set; }

        public Dictionary<String, Decimal> DicPuntajePromedioOutome { get; set; }
        public String NombreNivel { get; set; }
        public String NivelMinimo { get; set; }
        public String NivelMaximo { get; set; }
        public String NivelColor { get; set; }
        // public List<Hallazgo> LstHallazgo { get; set; }
        public List<Hallazgos> LstHallazgos { get; set; }

        /*VARIABLE PARA REALIZAR CONDICIONAL PARA EL BREADCRUMB PORQUE SE UTILIZA LA VISTA ASOCIADA A ESTE VIEW MODEL
         DESDE EL MANTENIMIENTO Y MANTENIMIENTO VIRTUAL(SABER A QUE PANTALLA REGRESAR)*/
        public Int32? opcion_rastro;
        public LstHallazgosGRAViewModel()
        {
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstModalidad = new List<Modalidad>();
            LstCarrera = new List<Carrera>();
            LstNroPractica = new List<NumeroPractica>();
            LstComision = new List<Comision>();
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();

            LstOutcome = new List<String>();
            LstSede = new List<String>();
            LstPuntaje = new List<Double>();
            DicSerie = new Dictionary<String, List<Double>>();
            DicPuntajePromedioOutome = new Dictionary<String, Decimal>();
            //    LstHallazgo = new List<Hallazgo>();
            LstHallazgos = new List<Hallazgos>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32? numeroPagina, Int32? idSubModalidadPeriodoAcademico, Int32? idCarrera, Int32? idComision, Int32? idNumeroPractica, Int32? idNivelAceptacionEncuesta, Boolean search, Int32? opcion_rastro, Int32 parModalidadId)
        {        
            this.Search = search;
            this.IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            this.IdCarrera = idCarrera;
            this.IdComision = idComision;
            this.IdNumeroPractica = idNumeroPractica;
            this.IdNivelAceptacionEncuesta = idNivelAceptacionEncuesta;

            if (!this.IdSubModalidadPeriodoAcademico.HasValue)
                IdSubModalidadPeriodoAcademico = dataContext.session.GetSubModalidadPeriodoAcademicoId();

            int SMPAM;

            if (parModalidadId.Equals(1)) // REGULAR
                SMPAM = dataContext.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;
            else                          // EPE
                SMPAM = dataContext.context.SubModalidadPeriodoAcademicoModulo.LastOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;


            IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;

            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                   join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                   where (submoda.IdModalidad == parModalidadId)
                                   select pa
                                    ).ToList();


            var escuelaId = dataContext.session.GetEscuelaId();

            LstCarrera = (from ca in dataContext.context.Carrera
                          join capa in dataContext.context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();

            LstNroPractica = dataContext.context.NumeroPractica.ToList();

            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Include(x => x.TipoEncuesta).Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).ToList();

            LstModalidad = dataContext.context.Modalidad.ToList();

            var IdNivelEncuestaHallazgo = 0;

     //       LstHallazgo = dataContext.context.Hallazgo.Where(x => /*x.IdEncuesta == null && */x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == IdCarrera && x.Sede.Codigo == "MO" && x.IdComision == IdComision).OrderBy(x => x.IdCriticidad).ToList();

            LstHallazgos = dataContext.context.Hallazgos.Where(x => x.ConstituyenteInstrumento.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademicoModulo == SMPAM && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.OutcomeComision.IdComision == IdComision).OrderBy(x => x.IdCriticidad).ToList();

            foreach (var item in LstHallazgos)
            {
                item.Codigo = item.Codigo + '-' + item.Identificador.ToString();
            }

            if (Search || (IdSubModalidadPeriodoAcademico.HasValue && IdCarrera.HasValue && IdComision.HasValue && IdNivelAceptacionEncuesta.HasValue))
            {
                Search = true;
                CrearGrafico(dataContext);
                LstComision = dataContext.context.CarreraComision.Include(x => x.Comision).Where(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Comision.Codigo != ConstantHelpers.COMISON_WASC).OrderBy(x => x.Comision.Codigo).Select(x => x.Comision).ToList();
                foreach (var item in LstComision)
                {
                    if (item.Codigo == ConstantHelpers.ENCUESTA.COMISION_EAC)
                        item.Codigo = "INGENIERÍA";
                    else if (item.Codigo == ConstantHelpers.ENCUESTA.COMISION_WASC)
                        item.Codigo = "WASC";
                    else
                        item.Codigo = "COMPUTACIÓN";
                }
                var NombreNivelEncuesta = dataContext.context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == idNivelAceptacionEncuesta).NombreEspanol;
                IdNivelEncuestaHallazgo = dataContext.context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == NombreNivelEncuesta).IdNivelAceptacionHallazgo;
            }

            if (IdNivelEncuestaHallazgo != 0)
                //    LstHallazgo = dataContext.context.Hallazgo.Where(x => /*x.IdEncuesta == null && */x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == IdCarrera && x.Sede.Codigo == "MO" && x.IdComision == IdComision && x.IdNivelAceptacionHallazgo == IdNivelEncuestaHallazgo).OrderBy(x => x.IdCriticidad).ToList();
                LstHallazgos = dataContext.context.Hallazgos.Where(x => x.ConstituyenteInstrumento.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademicoModulo == SMPAM && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.OutcomeComision.IdComision == IdComision && x.IdNivelAceptacionHallazgo == IdNivelEncuestaHallazgo).OrderBy(x => x.IdCriticidad).ToList();

            //ADD
            this.opcion_rastro = opcion_rastro; 
        }
        public void CrearGrafico(CargarDatosContext dataContext)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;



            var LstSedes = dataContext.context.Sede.ToList();
            foreach (var item in LstSedes)
            {
                DicSerie.Add(item.Nombre, new List<Double>());
            }

            var Nivel = dataContext.context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == IdNivelAceptacionEncuesta);
            NombreNivel = language == ConstantHelpers.CULTURE.ESPANOL ? Nivel.NombreEspanol : Nivel.NombreIngles;
            NivelMaximo = Nivel.ValorMaximo.ToSafeString();
            NivelMinimo = Nivel.ValorMinimo.ToSafeString();
            NivelColor = Nivel.Color;
            var Data = dataContext.context.ReportePercepcionPromPorOutcomeGraduando(IdComision, IdSubModalidadPeriodoAcademico, IdCarrera, language).Where(x => x.Puntaje >= Nivel.ValorMinimo && x.Puntaje <= Nivel.ValorMaximo).ToList();

            foreach (var item in Data)
            {
                if (!LstOutcome.Contains(item.Nombre))
                    LstOutcome.Add(item.Nombre);
                DicPuntajePromedioOutome[item.Nombre] = item.Puntaje.Value;
            }
        }
    }
}