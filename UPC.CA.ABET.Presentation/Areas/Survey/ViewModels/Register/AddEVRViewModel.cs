﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEVRViewModel
    {
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdSubModalidadPeriodoAcademicoModulo { get; set; }
        public Int32 IdModulo { get; set; }
		public Int32 IdPlantillaEncuesta { get; set; }
        public Int32 IdCarrera { get; set; }
        public String Ciclo { get; set; }
        public Int32 IdEscuela { get; set; }

        public List<Modulo> LstModulo { get; set; }
        public List<Carrera> LstCarrera { get; set; }
		public List<PlantillaEncuesta> LstPlantillaEncuesta { get; set; }

        public AddEVRViewModel()
        {
            LstModulo = new List<Modulo>();
        }
        public void CargarDatos(CargarDatosContext ctx, Int32 pIdSubModalidadPeriodoAcademico, Int32 pIdEscuela)
        {
            IdSubModalidadPeriodoAcademico = pIdSubModalidadPeriodoAcademico;

            int IdSubModalidad = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == pIdSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidad;
         
            Ciclo = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().PeriodoAcademico.CicloAcademico;

            var LstSubModalidad = (from smpam in ctx.context.SubModalidadPeriodoAcademicoModulo
                                   join m in ctx.context.Modulo on smpam.IdModulo equals m.IdModulo
                                   where (smpam.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico)
                                   select new { IdSubModalidadPeriodoAcademicoModulo = smpam.IdSubModalidadPeriodoAcademicoModulo , IdModulo = m.IdModulo}
                                    ).ToList();

			LstPlantillaEncuesta = ctx.context.PlantillaEncuesta.ToList();

            IdEscuela = pIdEscuela;


            for (int i=0; i < LstSubModalidad.Count; i++)
            {
                int idSub = LstSubModalidad[i].IdSubModalidadPeriodoAcademicoModulo;
                if(ctx.context.EncuestaVirtualDelegado.Where(x=>x.IdSubModalidadPeriodoAcademicoModulo == idSub).FirstOrDefault() == null)
                {
                    int idModulo = LstSubModalidad[i].IdModulo;
                    Modulo modulo = ctx.context.Modulo.Where(x => x.IdModulo == idModulo).FirstOrDefault();
                    LstModulo.Add(modulo);
                }
            }

            LstCarrera = ctx.context.Carrera.Where(x => x.IdEscuela == pIdEscuela && x.IdSubmodalidad==IdSubModalidad).ToList();
        }

    }
}