﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class _SelectPeriodoViewModel
    {
        public Int32? EncuestaId { get; set; }
        public String TipoEncuestaNombre { get; set; }
        public List<PeriodoAcademico> LstPeriodo { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public Int32? CicloId { get; set; }
        public Int32? CarreraId { get; set; }
        public _SelectPeriodoViewModel()
        {
            LstPeriodo = new List<PeriodoAcademico>();
        }
        public void Fill(CargarDatosContext dataContext, String tipoEncuestaNombre, Int32? encuestaId)
        {
            //SubModalidadPeriodoAcademicoId = dataContext.session.GetSubModalidadPeriodoAcademicoId();
            EncuestaId = encuestaId;
            TipoEncuestaNombre = tipoEncuestaNombre;
            LstPeriodo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                          join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                          where (submoda.IdSubModalidad == 1)
                          select pa).ToList();
                
            var escuelaId = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId && x.Codigo!="IS").ToList();

           
        }
    }
}