﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class _AddEditHallazgoIRDViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionEspanol { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionIngles { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdAcreditadora { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCriticidad { get; set; }

        public Int32? IdComision { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdNumeroPractica { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? HallazgoId { get; set; }

        //public List<OutcomeEncuestaConfig> LstOutcome { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Acreditadora> LstAcreditadora { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<Outcome> LstOutcome { get; set; }
        public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public List<Criticidad> LstCriticidad { get; set; }

        // public Int32 IdOutcomeEncuestaConfig { get; set; }
        public String NombreCarrera { get; set; }
        public String NombreComision { get; set; }
        public String NombreCurso { get; set; }
        public String NombreAcreditadora { get; set; }

        public String NombreOutcome { get; set; }
        public String NombreNivelAceptacion { get; set; }
        public String NombreSede { get; set; }

        //  public Boolean Search { get; set; }

        public _AddEditHallazgoIRDViewModel()
        {
            LstNivelAceptacion = new List<NivelAceptacionHallazgo>();
            LstCriticidad = new List<Criticidad>();
            // LstOutcome = new List<OutcomeEncuestaConfig>();
            //LstOutcome = new List<Outcome>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32? idCurso, Int32? idPeriodoAcademico, Int32? idOutcome, Int32? idSede)
        {

            int idsubmoda = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            IdPeriodoAcademico = idPeriodoAcademico;


            IdSubModalidadPeriodoAcademico = idsubmoda;
            IdSede = idSede;
            NombreSede = dataContext.context.Sede.FirstOrDefault(x => x.IdSede == idSede).Nombre;
            IdCurso = idCurso;
            NombreCurso = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == idCurso).NombreEspanol : dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == idCurso).NombreIngles;

            IdOutcome = idOutcome;
            NombreOutcome = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Outcome.FirstOrDefault(x => x.IdOutcome == idOutcome).Nombre : dataContext.context.Outcome.FirstOrDefault(x => x.IdOutcome == idOutcome).NombreIngles;


            IdComision = dataContext.context.OutcomeComision.FirstOrDefault(x => x.IdOutcome == IdOutcome && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdComision;
            NombreComision = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).NombreEspanol : dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).NombreIngles;


            IdCarrera = dataContext.context.CarreraComision.FirstOrDefault(x => x.IdComision == IdComision && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdCarrera;
            NombreCarrera = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).NombreEspanol : dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).NombreIngles;


            int IdAcreditadoraPeriodoAcademico = dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).IdAcreditadoraPeriodoAcademico;

            IdAcreditadora = dataContext.context.AcreditadoraPeriodoAcademico.FirstOrDefault(x => x.IdAcreditadoraPreiodoAcademico == IdAcreditadoraPeriodoAcademico).IdAcreditadora;

            NombreAcreditadora = dataContext.context.Acreditadora.FirstOrDefault(x => x.IdAcreditadora == IdAcreditadora).Nombre;

            LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();
        }
    }
}