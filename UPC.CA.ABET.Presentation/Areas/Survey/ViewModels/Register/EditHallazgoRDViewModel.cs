﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class EditHallazgoRDViewModel
    {
        public string DescripcionHallazgo { get; set; }
        public string Codigo { get; set; }
        public int HallazgoId { get; set; }


        public void CargarDato(CargarDatosContext context, int HallazgoId)
        {
            var hallazgo = context.context.Hallazgos.FirstOrDefault(x => x.IdHallazgo == HallazgoId);

            DescripcionHallazgo = hallazgo.DescripcionEspanol;
            this.HallazgoId = hallazgo.IdHallazgo;
            Codigo = hallazgo.Codigo + "-" + hallazgo.Identificador;
        }
    }
}