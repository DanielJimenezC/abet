﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class ConfigQuestionLevelViewModel
    {
        public Int32 IdPregunta { get; set; }

        public int? Nivel1 { get; set; }
        public int? Nivel2 { get; set; }
        public int? Nivel3 { get; set; }
        public int? Nivel4 { get; set; }
        public int? Nivel5 { get; set; }

        public int? maxNivel { get; set; }
        public List<NivelSatisfaccion> LstNiveles { get; set;}

        public ConfigQuestionLevelViewModel(){
            LstNiveles = new List<NivelSatisfaccion>();
            maxNivel = 2;

        }
        public void CargarDatos(CargarDatosContext dataContext)
        {
            LstNiveles = dataContext.context.NivelSatisfaccion.ToList();

            if (dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
            {
                maxNivel = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).Count();
                Nivel1 = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 1).FirstOrDefault().IdNivelSatisfaccion;
                Nivel2 = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 2).FirstOrDefault().IdNivelSatisfaccion;
                if (maxNivel>=3 && dataContext.context.DetalleNiveles.Where(x=>x.IdPregunta==IdPregunta && x.Orden == 3).FirstOrDefault() != null)
                {
                    Nivel3 = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 3).FirstOrDefault().IdNivelSatisfaccion;
                    if (maxNivel >= 4 && dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 4).FirstOrDefault() != null)
                    {
                        Nivel4 = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 4).FirstOrDefault().IdNivelSatisfaccion;
                        if (maxNivel >= 5 && dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 5).FirstOrDefault() != null)
                        {
                            Nivel5 = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta && x.Orden == 5).FirstOrDefault().IdNivelSatisfaccion;
                        }
                    }
                }
            }                            
        }
    }
}