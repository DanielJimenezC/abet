﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditFindSurveyPPPViewModel
    {
        public Int32 IdEncuesta { get; set; }
        public Encuesta Encuesta { get; set; }
        public String NombreAlumno { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCE { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCG { get; set; }
        public List<PerformanceEncuestaPPP> lstPerformancePPP_CE { get; set; }
        public List<PerformanceEncuestaPPP> lstPerformancePPP_CG { get; set; }
        
        public List<OutcomeEncuestaConfig> lstCE { get; set; }
        public List<OutcomeEncuestaConfig> lstCG { get; set; }
        public List<OutcomeEncuestaPPPConfig> lstPPP_CE { get; set; }
        public List<OutcomeEncuestaPPPConfig> lstPPP_CEHallazgo { get; set; }
        public List<OutcomeEncuestaPPPConfig> lstPPP_CG { get; set; }
        public String CodigoHallazgo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCriterio { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacion { get; set; }
        public Int32? IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdOutcome { get; set; }
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public List<CriterioHallazgoEncuesta> lstCriterios { get; set; }
        public List<NivelAceptacionHallazgo> lstNivelAceptacion { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Hallazgo> lstHallazgos { get; set; }
        public List<OutcomeEncuestaConfig> Lstoutcomes { get; set; }
        public Int32? IdHallazgo { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 _IdEncuesta, Int32? idHallazgo, Int32 parModalidadId)
        {
            IdEncuesta = _IdEncuesta;

            IdHallazgo = idHallazgo;

            Encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == IdEncuesta);
            Lstoutcomes = dataContext.context.OutcomeEncuestaConfig.Include(x => x.TipoEncuesta).
                Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO 
                && x.IdCarrera == Encuesta.IdCarrera && x.EsVisible == true && x.IdSubModalidadPeriodoAcademico == Encuesta.IdSubModalidadPeriodoAcademico
                 && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();

            NombreAlumno = String.Format("{0} {1}", Encuesta.Alumno.Nombres, Encuesta.Alumno.Apellidos);

            lstCriterios = dataContext.context.CriterioHallazgoEncuesta.ToList();
            lstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            lstSede = dataContext.context.Sede.ToList();
            lstCurso = dataContext.context.Curso.ToList();
            var escuelaId = dataContext.session.GetEscuelaId();

            lstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();

            var instrumentoId = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdInstrumento;
            lstHallazgos = dataContext.context.Hallazgo.Include(x => x.CriterioHallazgoEncuesta).Include(x => x.NivelAceptacionHallazgo).Where(x => x.IdInstrumento == instrumentoId && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdEncuesta == IdEncuesta).OrderBy(x => x.IdCriticidad).ToList();

            lstPerformanceCE = new List<PerformanceEncuesta>();
            lstPerformanceCG = new List<PerformanceEncuesta>();
            lstPerformancePPP_CE = new List<PerformanceEncuestaPPP>();
            lstPerformancePPP_CG = new List<PerformanceEncuestaPPP>();
            lstCE = new List<OutcomeEncuestaConfig>();
            lstCG = new List<OutcomeEncuestaConfig>();
            lstPPP_CE = new List<OutcomeEncuestaPPPConfig>();
            lstPPP_CEHallazgo = new List<OutcomeEncuestaPPPConfig>();
            lstPPP_CG = new List<OutcomeEncuestaPPPConfig>();
            try
            {
                var performance = dataContext.context.PerformanceEncuesta.Include(x => x.OutcomeEncuestaConfig).Include(x => x.OutcomeEncuestaConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == IdEncuesta).OrderBy(x => x.OutcomeEncuestaConfig.IdComision).ThenBy(x => x.OutcomeEncuestaConfig.Orden).ToList();
                foreach (var item in performance)
                {
                    if (item.OutcomeEncuestaConfig != null)
                    {
                        if (item.OutcomeEncuestaConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                        {
                            lstPerformanceCE.Add(item);
                        }
                        else
                        {
                            lstPerformanceCG.Add(item);
                        }
                    }
                }
                var lstOutcome = dataContext.context.OutcomeEncuestaConfig.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == Encuesta.IdCarrera && x.IdSubModalidadPeriodoAcademico == Encuesta.IdSubModalidadPeriodoAcademico && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();


                var performancePPP = dataContext.context.PerformanceEncuestaPPP.Include(x => x.OutcomeEncuestaPPPConfig).Include(x => x.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == IdEncuesta).OrderBy(x => x.OutcomeEncuestaPPPConfig.Orden).ToList();
                foreach (var item in performancePPP)
                {
                    if (item.OutcomeEncuestaPPPConfig != null)
                    {
                        if (item.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                        {
                            lstPerformancePPP_CE.Add(item);
                        }
                        else
                        {
                            lstPerformancePPP_CG.Add(item);
                        }
                    }
                }


                var lstOutcomePPP = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdEscuela == Encuesta.Carrera.IdEscuela && x.IdSubModalidadPeriodoAcademico == Encuesta.IdSubModalidadPeriodoAcademico && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                lstPPP_CE = lstOutcomePPP.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstPPP_CG = lstOutcomePPP.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();

                lstPPP_CEHallazgo = lstOutcomePPP.Where(x => (x.IdCarrera.HasValue == false || x.IdCarrera.Value == Encuesta.IdCarrera) && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();


            }
            catch (Exception ex)
            {

            }

            if (IdHallazgo.HasValue)
            {
                var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                IdCriterio = hallazgo.IdCriterioHallazgoEncuesta;
                IdNivelAceptacion = hallazgo.IdNivelAceptacionHallazgo;
                IdSede = hallazgo.Encuesta.IdSede;
                IdCurso = hallazgo.IdCurso;
                IdCarrera = hallazgo.IdCarrera;
                DescripcionEspanol = hallazgo.DescripcionEspanol;
                DescripcionIngles = hallazgo.DescripcionIngles;
                IdOutcome = hallazgo.IdOutcomeEncuestaConfig;
            }
            else
            {
                IdSede = ConvertHelpers.ToInteger(Encuesta.IdSede);
                IdCurso = Encuesta.IdCurso;
                IdCarrera = Encuesta.IdCarrera;
            }
        }
    }
}