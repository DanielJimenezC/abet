﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditHallazgoEVDViewModel
    {

        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdModulo { get; set; }
        public Int32? IdPregunta { get; set; }
		public Int32? IdComentario { get; set; }
		public Int32? IdDocente { get; set; }
		public Int32? IdCurso { get; set; }
        public List<SelectListItem> LstPeriodoAcademico { get; set; }
        public List<SelectListItem> LstModulo { get; set; }
        public List<SelectListItem> LstPregunta { get; set; }
		public List<SelectListItem> LstCursos { get; set; }
		public List<SelectListItem> LstDocentes { get; set; }
        public List<Comentario> LstComentario { get; set; }
		public Int32? IdHallazgo { get; set; }


        [Required(ErrorMessage = "Debe ingresar la descripción del hallazgo.")]
        public String DescripcionEspanol { get; set; }

        [Required(ErrorMessage = "You must enter the description of the finding.")]
        public String DescripcionIngles { get; set; }



		public List<bool> LstMarcados { get; set; }
		public int cantidadAsaltear { get; set; }
		public IPagedList<Hallazgos> LstHallazgo { get; set; }
        public Int32? NumeroPaginaHallazgo { get; set; }


        public AddEditHallazgoEVDViewModel()
        {
            LstPeriodoAcademico = new List<SelectListItem>();
            LstModulo = new List<SelectListItem>();
            LstPregunta = new List<SelectListItem>();
            LstComentario = new List<Comentario>();
			LstCursos = new List<SelectListItem>();
			LstDocentes = new List<SelectListItem>();
		}

        public void Fill(CargarDatosContext ctx, Int32? parIdPeriodoAcademico, Int32? parIdModulo, Int32? parIdPregunta,Int32? parModalidadId, Int32? numeroPaginaHallazgo)
        {
	
			LstPeriodoAcademico = (from a in ctx.context.SubModalidadPeriodoAcademico
								   join b in ctx.context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
								   join c in ctx.context.SubModalidad on a.IdSubModalidad equals c.IdSubModalidad
								   where (c.IdModalidad == parModalidadId)
								   select new SelectListItem { Text = b.CicloAcademico, Value = b.IdPeriodoAcademico.ToString() }
								  ).ToList();

            NumeroPaginaHallazgo = numeroPaginaHallazgo ?? 1;

            LstHallazgo = (from a in ctx.context.ComentarioHallazgo
						   join b in ctx.context.Hallazgos on a.IdHallazgo equals b.IdHallazgo
                           join c in ctx.context.ComentarioDelegado on a.IdComentarioDelegado equals c.IdComentarioDelegado
						   where c.IdEncuestaVirtualDelegado.HasValue == true
						   select b).Distinct().OrderByDescending(x => x.IdHallazgo).ToList().ToPagedList(NumeroPaginaHallazgo.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

			int pa = Convert.ToInt32(LstPeriodoAcademico[0].Value);


            LstModulo = (from a in ctx.context.SubModalidadPeriodoAcademicoModulo
							 join b in ctx.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
							 join c in ctx.context.Modulo on a.IdModulo equals c.IdModulo
							 where b.IdPeriodoAcademico == pa
							 select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();


			int mo = Convert.ToInt32(LstModulo[0].Value);


			LstPregunta = (from a in ctx.context.EncuestaVirtualDelegado
						   join b in ctx.context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademicoModulo equals b.IdSubModalidadPeriodoAcademicoModulo
						   join c in ctx.context.Pregunta on a.IdPlantillaEncuesta equals c.IdPlantillaEncuesta
						   where b.IdModulo == mo && c.TipoPregunta.IdTipoPregunta==1
						   select new SelectListItem { Text = c.DescripcionEspanol, Value = c.IdPregunta.ToString() }).OrderByDescending(x => x.Value).ToList();


			if (parIdPregunta.HasValue)
            {
				

			}


        }
    }
}