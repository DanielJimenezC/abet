﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditSurveyFDCViewModel
    {
        public String Tipo { get; set; }
        public Int32? EncuestaId { get; set; }
        public Int32? IdPeriodoAcademico { get; set; } 
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSeccion { get; set; }
        public Decimal PuntajeTotal { get; set; }
        public Int32 IdResultadoObtenido { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<EncuestaComentario> lstComentario { get; set; }
        public List<ResultadoFDC> lstResultado { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? _EncuestaId)
        {
            Tipo = ConstantHelpers.ENCUESTA.FDC;
            EncuestaId = _EncuestaId;

            if(EncuestaId.HasValue)
            {
                var encuesta = dataContext.context.Encuesta.FirstOrDefault( x => x.IdEncuesta == EncuestaId);
                IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                //IdCarrera = encuesta.IdCarrera.Value;
                IdCurso = encuesta.IdCurso.Value;
                IdSeccion = encuesta.IdSeccion.Value;
                IdSede = encuesta.IdSede.Value;

                lstCiclo = dataContext.context.PeriodoAcademico.ToList();
                //var escuelaId = dataContext.session.GetEscuelaId();
                //lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
                lstCurso = dataContext.context.Curso.Where( x => x.IdCurso == IdCurso).ToList();
                lstSeccion = dataContext.context.Seccion.Where( x => x.IdSeccion == IdSeccion).ToList();
                lstSede = dataContext.context.Sede.ToList();

                lstResultado = dataContext.context.ResultadoFDC.Where(x => x.IdEncuesta == EncuestaId).ToList();
                lstComentario = dataContext.context.EncuestaComentario.Where(x => x.IdEncuesta == EncuestaId).ToList();
            }
            else
            {
                lstCiclo = dataContext.context.PeriodoAcademico.ToList();
                //var escuelaId = dataContext.session.GetEscuelaId();
                //lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
                lstCurso = new List<Curso>();
                lstSeccion = new List<Seccion>();
                lstSede = dataContext.context.Sede.ToList();

                lstComentario = new List<EncuestaComentario>();
            }
        }
    }
}