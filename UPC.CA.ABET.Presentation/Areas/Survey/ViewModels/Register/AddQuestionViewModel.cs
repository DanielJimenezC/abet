﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddQuestionViewModel
    {
        public Int32 IdPregunta { get; set; }
        public Int32 IdTipoPregunta { get; set; }
        [Required]
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public Int32 IdPlantillaEncuesta { get; set; }  
    }
}