﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class _ConfirmacionDeleteHallazgoViewModel
    {
        public Int32 HallazgoId { get; set; }
        public Int32? IdNumeroPractica { get; set; }
        public Int32 IdNivelAceptacionHallazgo { get; set; }
        public _ConfirmacionDeleteHallazgoViewModel()
        {

        }
        public void Fill(Int32 hallazgoId, Int32? idNumeroPractica,Int32 idNivelAceptacionHallazgo)
        {
            HallazgoId = hallazgoId;
            IdNivelAceptacionHallazgo = idNivelAceptacionHallazgo;
            IdNumeroPractica = idNumeroPractica;

        }
    }
}