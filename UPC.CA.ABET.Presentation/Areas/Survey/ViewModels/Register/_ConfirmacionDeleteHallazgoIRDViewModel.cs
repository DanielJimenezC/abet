﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class _ConfirmacionDeleteHallazgoIRDViewModel
    {
        public Int32 HallazgoId { get; set; }
        public Int32 IdCurso { get; set; }
        public Int32 IdOutcome { get; set; }
        public Int32 IdSede { get; set; }

        public String NombreCurso { get; set; }
        public String NombreNivelAceptacion { get; set; }
        public String Codigo { get; set; }
        public Hallazgo hallazgo { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }

        public _ConfirmacionDeleteHallazgoIRDViewModel()
        {

        }
        public void Fill(CargarDatosContext dataContext, Int32 hallazgoId)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;


            HallazgoId = hallazgoId;
            hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == hallazgoId);
            IdCurso = hallazgo.Curso.IdCurso;
            IdOutcome = hallazgo.Outcome.IdOutcome;
            IdSede = hallazgo.Sede.IdSede;
            IdSubModalidadPeriodoAcademico = hallazgo.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico;
            NombreCurso = language == ConstantHelpers.CULTURE.ESPANOL ? hallazgo.Curso.NombreEspanol : hallazgo.Curso.NombreIngles;
            Codigo = hallazgo.Codigo;
            NombreNivelAceptacion = language == ConstantHelpers.CULTURE.ESPANOL ? hallazgo.NivelAceptacionHallazgo.NombreEspanol : hallazgo.NivelAceptacionHallazgo.NombreIngles;
            IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;
        }
    }
}