﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class NivelesSatisfaccionViewModel
    {
        public List<List<String>> lstNiveles { get; set; }

        public NivelesSatisfaccionViewModel()
        {
            lstNiveles = new List<List<string>>();
        }

        public void CargarDatosContext(CargarDatosContext dataContext)
        {
            foreach (var nivel in dataContext.context.NivelSatisfaccion)
            {
                lstNiveles.Add(new List<string>
                {
                nivel.IdNivelSatisfaccion.ToString(),
                nivel.DescripcionEspanol,
                nivel.DescripcionIngles,
				nivel.GeneraHallazgo.ToString(),
                (dataContext.context.DetalleNiveles.Where(x => x.IdNivelSatisfaccion == nivel.IdNivelSatisfaccion).FirstOrDefault() == null ? "Y": "N")
                });
            }

        }

    }
}