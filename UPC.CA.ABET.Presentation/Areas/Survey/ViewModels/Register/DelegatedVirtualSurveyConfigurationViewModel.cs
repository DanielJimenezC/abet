﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class DelegatedVirtualSurveyConfigurationViewModel
    {
        public Int32 IdEncuestaVirtualDelegado { get; set; }
        public List<Pregunta> LstPregunta { get; set; }

        public DelegatedVirtualSurveyConfigurationViewModel()
        {
            LstPregunta = LstPregunta;
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 pIdSubModalidadPeriodoAcademico, Int32 pIdEscuela, Int32 pIdEncuestaVirtualDelegado)
        {
            IdEncuestaVirtualDelegado = pIdEncuestaVirtualDelegado;

            //LstPregunta = dataContext.context.Pregunta.Where(x => x.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado).ToList();

      
        }
    }
}