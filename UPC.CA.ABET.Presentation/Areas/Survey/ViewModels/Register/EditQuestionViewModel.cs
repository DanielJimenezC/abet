﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class EditQuestionViewModel
    {
        public Int32 IdPregunta { get; set; }
        public Int32 IdTipoPregunta { get; set; }
        [Required]
        public String DescripcionEspanol { get; set; }
        [Required]
        public String DescripcionIngles { get; set; }
        public Int32 IdPlantillaEncuesta { get; set; }

        public void CargarDatos(CargarDatosContext dataContext)
        {
            Pregunta pregunta = dataContext.context.Pregunta.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault();
            IdTipoPregunta = pregunta.IdTipoPregunta;
            DescripcionEspanol = pregunta.DescripcionEspanol;
            DescripcionIngles = pregunta.DescripcionIngles;
            IdPlantillaEncuesta = pregunta.IdPlantillaEncuesta;

        }
    }
}