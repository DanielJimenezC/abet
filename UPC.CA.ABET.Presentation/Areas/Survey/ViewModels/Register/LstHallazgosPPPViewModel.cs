﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.AddEditSurvey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class LstHallazgosPPPViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdComision { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNumeroPractica { get; set; }
        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<NumeroPractica> LstNroPractica { get; set; }
        public Int32? NumeroPagina { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacionEncuesta { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public Boolean Search { get; set; }

        public List<String> LstOutcome { get; set; }
        public List<String> LstSede { get; set; }
        public List<Double> LstPuntaje { get; set; }
        public Dictionary<String, List<Double>> DicSerie { get; set; }

        public Dictionary<Tuple<String, String>, Decimal> DicPuntajePromedioOutome { get; set; }
        public String NombreNivel { get; set; }
        public String NivelMinimo { get; set; }
        public String NivelMaximo { get; set; }
        public String NivelColor { get; set; }
        public List<Hallazgo> LstHallazgo { get; set; }
        public LstHallazgosPPPViewModel()
        {
            LstPeriodoAcademico = new List<PeriodoAcademico>();
            LstCarrera = new List<Carrera>();
            LstNroPractica = new List<NumeroPractica>();
            LstComision = new List<Comision>();
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();

            LstOutcome = new List<String>();
            LstSede = new List<String>();
            LstPuntaje = new List<Double>();
            DicSerie = new Dictionary<String, List<Double>>();
            DicPuntajePromedioOutome = new Dictionary<Tuple<String, String>, Decimal>();
            LstHallazgo = new List<Hallazgo>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32? numeroPagina, Int32? idPeriodoAcademico, Int32? idCarrera, Int32? idComision, Int32? idNumeroPractica, Int32? idNivelAceptacionEncuesta, Boolean search, int? idEscuela,Int32 parModalidadId)
        {
            this.Search = search;
            this.IdPeriodoAcademico = idPeriodoAcademico;
            this.IdCarrera = idCarrera;
            this.IdComision = idComision;
            this.IdNumeroPractica = idNumeroPractica;
            this.IdNivelAceptacionEncuesta = idNivelAceptacionEncuesta;
            Int32 IdNivelEncuestaHallazgo = 0;
            if (!this.IdPeriodoAcademico.HasValue)
                IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdPeriodoAcademico;

        
            LstPeriodoAcademico = (from pa in dataContext.context.PeriodoAcademico
                                   join submodapa in dataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                   join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                                   where (submoda.IdModalidad == parModalidadId)
                                   select pa
                ).ToList();




            //LstCarrera = dataContext.context.Carrera.Where(x=>x.IdEscuela == escuelaId).ToList();

            //Listar las carreras del PeriodoAcademico elegido en la modalidad que se encuentra la session
            LstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();
            


            LstNroPractica = dataContext.context.NumeroPractica.ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Include(x => x.TipoEncuesta).Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico==IdPeriodoAcademico && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();

            if (Search || (IdPeriodoAcademico.HasValue && IdCarrera.HasValue && IdComision.HasValue && IdNumeroPractica.HasValue && IdNivelAceptacionEncuesta.HasValue))
            {
                Search = true;
                CrearGrafico(dataContext);
                LstComision = dataContext.context.CarreraComision.Include(x => x.Comision).Where(x => x.IdCarrera == IdCarrera && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.Comision.Codigo != ConstantHelpers.COMISON_WASC).OrderBy(x => x.Comision.Codigo).Select(x => x.Comision).ToList();
                foreach (var item in LstComision)
                {
                    if (item.Codigo == ConstantHelpers.ENCUESTA.COMISION_EAC)
                        item.Codigo = "INGENIERÍA";
                    else if (item.Codigo == ConstantHelpers.ENCUESTA.COMISION_WASC)
                        item.Codigo = "WASC";
                    else
                        item.Codigo = "COMPUTACIÓN";
                }
                var NombreNivelEncuesta = dataContext.context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == idNivelAceptacionEncuesta).NombreEspanol;
                IdNivelEncuestaHallazgo = dataContext.context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol == NombreNivelEncuesta).IdNivelAceptacionHallazgo;
            }
            if (IdNivelEncuestaHallazgo != 0)
                LstHallazgo = dataContext.context.Hallazgo.Where(x => /*x.IdEncuesta == null &&*/ x.Instrumento.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == IdCarrera && x.IdComision == IdComision && x.IdNivelAceptacionHallazgo == IdNivelEncuestaHallazgo).OrderBy(x => x.IdNivelAceptacionHallazgo).OrderBy(x => x.IdCriticidad).ToList();
        }
        public void CrearGrafico(CargarDatosContext dataContext)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            int idsubmodapa = 0;


            var LstSedes = dataContext.context.Sede.ToList();
            foreach (var item in LstSedes)
            {
                DicSerie.Add(item.Nombre, new List<Double>());
            }

            var Nivel = dataContext.context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == IdNivelAceptacionEncuesta);
            NombreNivel = language == ConstantHelpers.CULTURE.ESPANOL ? Nivel.NombreEspanol : Nivel.NombreIngles;
            NivelMaximo = Nivel.ValorMaximo.ToSafeString();
            NivelMinimo = Nivel.ValorMinimo.ToSafeString();
            NivelColor = Nivel.Color;

            if (IdPeriodoAcademico != null)
                idsubmodapa = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var Data = dataContext.context.ReportePromedioPorOutcomePPP(idsubmodapa, IdCarrera, IdComision, null, language, IdNumeroPractica).Where(x => x.Puntaje >= Nivel.ValorMinimo && x.Puntaje <= Nivel.ValorMaximo).ToList();


            var Data2 = dataContext.context.ReportePromedioPorOutcomePPP(idsubmodapa, IdCarrera, IdComision, null, language, IdNumeroPractica).ToList();


            foreach (var item in Data)
            {
                if (!LstOutcome.Contains(item.Nombre))
                    LstOutcome.Add(item.Nombre);

                if (!LstSede.Contains(item.Sede))
                    LstSede.Add(item.Sede);
            }
            DicPuntajePromedioOutome = Data.ToDictionary(x => new Tuple<String, String>(x.Sede, x.Nombre), x => x.Puntaje.GetValueOrDefault(0));
        }
    }
}