﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditSurveyVirtualGRAViewModel
    {
        public Int32? EncuestaId { get; set; }
        public String Tipo { get; set; }
        public String Comentario { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        public List<OutcomeEncuestaConfig> lstCE { get; set; }
        public List<OutcomeEncuestaConfig> lstCG { get; set; }

        public List<PerformanceEncuesta> lstPerformanceCE { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCG { get; set; }
        public List<EncuestaComentario> lstComentarios { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? _EncuestaId, Int32? idSubModalidadPeriodoAcademico, Int32? idCarrera)
        {
            var currentCulture = dataContext.currentCulture;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            IdCarrera = idCarrera;
            Tipo = ConstantHelpers.ENCUESTA.GRA;
            EncuestaId = _EncuestaId;
            lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstCarrera = dataContext.context.Carrera.ToList();

            lstSede = dataContext.context.Sede.ToList();
            lstPerformanceCE = new List<PerformanceEncuesta>();
            lstPerformanceCG = new List<PerformanceEncuesta>();

            if (EncuestaId.HasValue)
            {
                lstCE = new List<OutcomeEncuestaConfig>();
                lstCG = new List<OutcomeEncuestaConfig>();

                var encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == EncuestaId);
                //IdSede = encuesta.IdSede.Value;
                IdSubModalidadPeriodoAcademico = encuesta.IdSubModalidadPeriodoAcademico.Value;
                IdCarrera = encuesta.IdCarrera.Value;

                Comentario = encuesta.Comentario;

                var performance = dataContext.context.PerformanceEncuesta.Include(x => x.OutcomeEncuestaConfig).Include(x => x.OutcomeEncuestaConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == EncuestaId).ToList();

                foreach (var item in performance)
                {
                    if (item.OutcomeEncuestaConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                    {
                        lstPerformanceCE.Add(item);
                    }
                    else
                    {
                        lstPerformanceCG.Add(item);
                    }
                }

                var lstOutcome = dataContext.context.OutcomeEncuestaConfig.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == encuesta.IdCarrera && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).AsQueryable();

                if (IdSubModalidadPeriodoAcademico.HasValue)
                    lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();

                lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();

            }
            else
            {
                var lstOutcome = dataContext.context.OutcomeEncuestaConfig.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == IdCarrera && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).AsQueryable();

                if (IdSubModalidadPeriodoAcademico.HasValue)
                    lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();
                if (IdCarrera.HasValue)
                    lstOutcome = lstOutcome.Where(x => x.IdCarrera == IdCarrera).AsQueryable();

                lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
            }
        }
    }
}