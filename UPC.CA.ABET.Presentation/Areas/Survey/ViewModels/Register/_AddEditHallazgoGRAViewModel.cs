﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class _AddEditHallazgoGRAViewModel
    {
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdCriticidad { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdNumeroPractica { get; set; }
        public Int32? HallazgoId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public List<OutcomeEncuestaConfig> LstOutcome { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public List<Criticidad> LstCriticidad { get; set;  }
        public Int32 IdOutcomeEncuestaConfig { get; set; }
        public String NombreCarrera { get; set; }
        public String NombreComision { get; set; }
        public _AddEditHallazgoGRAViewModel()
        {
            LstOutcome = new List<OutcomeEncuestaConfig>();
            LstCriticidad = new List<Criticidad>();
        }
        public void CargarDatos(CargarDatosContext dataContext, Int32? hallazgoId,Int32? idSubModalidadPeriodoAcademico, Int32? idCarrera, Int32? idNivelAceptacionHallazgo, Int32? idComision, Int32? idCriticidad, Int32? idNumeroPractica)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            HallazgoId = hallazgoId;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            IdPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;
            IdCarrera = idCarrera;
            IdNivelAceptacionHallazgo = idNivelAceptacionHallazgo;
            IdComision = idComision;
            IdNumeroPractica = idNumeroPractica;
            IdCriticidad = idCriticidad;

            NombreCarrera = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).NombreEspanol : dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).NombreIngles;
            NombreComision = language == ConstantHelpers.CULTURE.ESPANOL ? dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).NombreEspanol : dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision).NombreIngles;

            if (HallazgoId.HasValue)
            {
                var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == HallazgoId);
                DescripcionEspanol = hallazgo.DescripcionEspanol;
                DescripcionIngles = hallazgo.DescripcionIngles;
                IdOutcomeEncuestaConfig = hallazgo.IdOutcomeEncuestaConfig.Value;
            }

            var Nivel = dataContext.context.NivelAceptacionEncuesta.FirstOrDefault(x => x.IdNivelAceptacionEncuesta == idNivelAceptacionHallazgo);
            var Data = dataContext.context.ReportePercepcionPromPorOutcomeGraduando(IdComision, IdSubModalidadPeriodoAcademico, IdCarrera, language).Where(x => x.Puntaje >= Nivel.ValorMinimo && x.Puntaje <= Nivel.ValorMaximo).ToList();
            List<String> LstOutcomeS = new List<String>();
            foreach (var item in Data)
            {
                if (!LstOutcomeS.Contains(item.Nombre))
                    LstOutcomeS.Add(item.Nombre);
            }
            LstOutcome = dataContext.context.OutcomeEncuestaConfig.Where(x => LstOutcomeS.Contains(x.NombreEspanol) && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdComision == IdComision && x.IdCarrera == IdCarrera).ToList();
            LstCriticidad = dataContext.context.Criticidad.ToList();
        }
    }
}