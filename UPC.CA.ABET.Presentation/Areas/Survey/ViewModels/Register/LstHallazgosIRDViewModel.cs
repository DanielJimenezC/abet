﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.AddEditSurvey;
using PagedList;
using UPC.CA.ABET.Presentation.Areas.Survey.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class LstHallazgosIRDViewModel
    {
        //[Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdComision { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdOutcome { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdAcreditadora { get; set; }


        public List<PeriodoAcademico> LstPeriodoAcademico { get; set; }
        // public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<Curso> LstCurso { get; set; }
        public List<Acreditadora> LstAcreditadora { get; set; }
        public List<Sede> LstSede { get; set; }

        // public List<Criticidad>



        // public List<int?> LstAcreditadoraPeriodoAcademico { get; set; }

        //  public List<NumeroPractica> LstNroPractica { get; set; }
        public Int32? NumeroPagina { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        //public Int32? IdNivelAceptacionHallazgo { get; set; }

        //  public List<NivelAceptacionHallazgo> LstNivelAceptacion { get; set; }
        public Boolean? Search { get; set; }

        public List<Outcome> LstOutcome { get; set; }
        //public List<String> LstSede { get; set; }
        public List<Double> LstPuntaje { get; set; }
        //public Dictionary<String, List<Double>> DicSerie { get; set; }

        // public Dictionary<Tuple<String, String>, Decimal> DicPuntajePromedioOutome { get; set; }
        public String NombreNivel { get; set; }
        public String NivelMinimo { get; set; }
        public String NivelMaximo { get; set; }
        public String NivelColor { get; set; }
        public IPagedList<Hallazgo> LstHallazgo { get; set; }

        public LstHallazgosIRDViewModel()
        {



            LstPeriodoAcademico = new List<PeriodoAcademico>();
            //LstAcreditadoraPeriodoAcademico = new List<int?>();

            // LstCarrera = new List<Carrera>();
            //LstNroPractica = new List<NumeroPractica>();
            LstComision = new List<Comision>();
            LstCurso = new List<Curso>();
            //LstNivelAceptacion = new List<NivelAceptacionHallazgo>();

            LstOutcome = new List<Outcome>();
            LstAcreditadora = new List<Acreditadora>();
            LstSede = new List<Sede>();
            LstPuntaje = new List<Double>();

        }

        public void CargarDatos(CargarDatosContext dataContext, Int32? numeroPagina, Int32? idPeriodoAcademico, Int32? idCurso, Int32? IdOutcome, Int32? idSede, Boolean? search, Int32? idEscuela)
        {
            int idperiodoacademicoactualpregradoregular = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;
            // dataContext.context.Configuration.ProxyCreationEnabled = false;
            AbetEntities db = dataContext.context;
            this.IdSede = idSede;
            this.Search = search;
            this.IdPeriodoAcademico = IdPeriodoAcademico;
            //this.IdCarrera = idCarrera;

            this.IdCurso = idCurso;
            this.IdOutcome = IdOutcome;
            //this.IdNivelAceptacionHallazgo = idNivelAceptacionHallazgo;
            this.IdAcreditadora = IdAcreditadora;

            if (!this.IdPeriodoAcademico.HasValue)
                IdPeriodoAcademico = idperiodoacademicoactualpregradoregular;
                //IdPeriodoAcademico = dataContext.session.GetSubModalidadPeriodoAcademicoId();

            LstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();


            var query0 = (from mcd in dataContext.context.MallaCocosDetalle
                          join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                          join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                          join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                          join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                          join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                          join submoda in dataContext.context.SubModalidadPeriodoAcademico on oc.IdSubModalidadPeriodoAcademico equals submoda.IdSubModalidadPeriodoAcademico
                          where submoda.IdPeriodoAcademico == IdPeriodoAcademico && c.IdEscuela == idEscuela
                          select cmc.IdCurso);


            LstCurso = dataContext.context.Curso.Where(x => (query0).Contains(x.IdCurso)).ToList();







            // IQueryable <int?> query1 = dataContext.context.AcreditadoraPeriodoAcademico.Where(y => y.IdPeriodoAcademico == IdPeriodoAcademico).Select(u => u.IdAcreditadora);



            // LstAcreditadora = dataContext.context.Acreditadora.Where(x => (query1).Contains(x.IdAcreditadora)).ToList();




            // LstOutcome = dataContext.context.Outcome.ToList();


            // LstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();


            LstSede = dataContext.context.Sede.ToList();


            NumeroPagina = numeroPagina ?? 1;
            // var query = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == 8 && x.Estado == "ACT").ToList();

            var query = dataContext.context.Hallazgo.Where(x => false).ToList();




            if ((Search.HasValue && Search.Value == true)
            //    || (IdPeriodoAcademico.HasValue && IdComision.HasValue  && idCurso.HasValue && idSede.HasValue)
               )
            {

                var queryMC = (from mc in dataContext.context.MallaCocos
                               join mcd in dataContext.context.MallaCocosDetalle on mc.IdMallaCocos equals mcd.IdMallaCocos
                               join oc in dataContext.context.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                               join cmc in dataContext.context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                               join cpa in dataContext.context.CursoPeriodoAcademico on cmc.IdCurso equals cpa.IdCurso
                               join ccpa in dataContext.context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                               join c in dataContext.context.Carrera on ccpa.IdCarrera equals c.IdCarrera
                               join submoda in dataContext.context.SubModalidadPeriodoAcademico on mc.IdSubModalidadPeriodoAcademico equals submoda.IdSubModalidadPeriodoAcademico
                               where (submoda.IdPeriodoAcademico == IdPeriodoAcademico && cmc.IdCurso == IdCurso && c.IdEscuela == idEscuela)
                               select oc.IdOutcomeComision);

                /* var data =   dataContext.context.OutcomeComision.Join(dataContext.context.Outcome, oc => oc.IdOutcome, o =>o.IdOutcome,(oc,c) => new { })


                        .Where(x => (queryMC).Contains(x.IdOutcome)).Select(x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre, NombreIngles = x.Comision.NombreIngles + " | " + x.Outcome.NombreIngles }).ToList();
                        */


                //LstOutcome = dataContext.context.OutcomeComision.Where(x => (queryMC).Contains(x.IdOutcome)).Select (x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre, NombreIngles = x.Comision.NombreIngles + " | " + x.Outcome.NombreIngles }).ToList();

                var data = dataContext.context.OutcomeComision.Where(x => (queryMC).Contains(x.IdOutcomeComision)).Select(x => new { IdOutcome = x.IdOutcome, Nombre = x.Comision.NombreEspanol + " | " + x.Outcome.Nombre, NombreIngles = x.Comision.NombreIngles + " | " + x.Outcome.NombreIngles }).ToList();

                foreach (var item in data)
                {

                    LstOutcome.Add(new Outcome() { IdOutcome = item.IdOutcome, Nombre = item.Nombre, NombreIngles = item.NombreIngles });

                }


                Search = true;
                query = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == 8 && x.IdCurso == idCurso && x.IdOutcome == IdOutcome && x.IdSede == idSede && x.Estado == "ACT").ToList();

            }


            LstHallazgo = query.OrderBy(x => x.IdCriticidad).ThenByDescending(x => x.IdEncuesta).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

        }








    }

}