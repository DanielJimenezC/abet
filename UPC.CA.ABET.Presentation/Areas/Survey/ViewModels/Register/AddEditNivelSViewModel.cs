﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditNivelSViewModel
    {
        public Int32? IdNivelSatisfaccion { get; set; }
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public Boolean GenerarHallazgo { get; set; }

        public AddEditNivelSViewModel()
        {
            GenerarHallazgo = false;
        }

        public void CargarDatosContext(CargarDatosContext dataContext)
        {
            if (IdNivelSatisfaccion.HasValue)
            {
                NivelSatisfaccion nivel = dataContext.context.NivelSatisfaccion.Where(x => x.IdNivelSatisfaccion == IdNivelSatisfaccion).FirstOrDefault();
                DescripcionEspanol = nivel.DescripcionEspanol;
                DescripcionIngles = nivel.DescripcionIngles;
                GenerarHallazgo = nivel.GeneraHallazgo ?? false;
            }

        }

    }
}