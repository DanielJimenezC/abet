﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditFindSurveyGRAViewModel
    {
        public List<Criticidad> lstCritidad { get; set; }
        public int IdCriticidad { get; set; }
        public Int32 IdEncuesta { get; set; }
        public Int32? IdHallazgo { get; set; }
        public Encuesta Encuesta { get; set; }
        public List<OutcomeEncuestaConfig> lstCE { get; set; }
        public List<OutcomeEncuestaConfig> lstCG { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCE { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCG { get; set; }
        public String CodigoHallazgo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCriterio { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdNivelAceptacion { get; set; }
        public Int32? IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCurso { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdOutcome { get; set; }
        public String DescripcionEspanol { get; set; }
        public String DescripcionIngles { get; set; }
        public List<CriterioHallazgoEncuesta> lstCriterios { get; set; }
        public List<NivelAceptacionHallazgo> lstNivelAceptacion { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<OutcomeEncuestaConfig> Lstoutcomes { get; set; }
        public List<Hallazgo> lstHallazgos { get; set; }
        //public List<GetHallazgosPorEncuesta_Result> lstHallazgosAGenerado { get; set; }

        //CAMPO AGREGADO PARA SABER CUAL ES LA PAGINA ANTERIOR A LA QUE DEBE VOLVER (YA SEA MANTENIMIENTO ENCUESTA, O MANTENIMIENTO ENCUESTA VIRTUAL) 
        public Int32? opcion_rastro;
        public Int32? CarreraId_rastro;
        public Int32? CicloId_rastro; 
        public void Fill(CargarDatosContext dataContext, Int32 _IdEncuesta, Int32? idHallazgo, Int32? CarreraId, Int32? CicloId, Int32? opcion_rastro, Int32 parModalidadId)
        {
            IdEncuesta = _IdEncuesta;
            IdHallazgo = idHallazgo;
            Encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == IdEncuesta);
            Lstoutcomes = dataContext.context.OutcomeEncuestaConfig.Include(x => x.TipoEncuesta).
                Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO
                && x.IdCarrera == Encuesta.IdCarrera && x.EsVisible == true && x.IdSubModalidadPeriodoAcademico == Encuesta.IdSubModalidadPeriodoAcademico && x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
            lstCriterios = dataContext.context.CriterioHallazgoEncuesta.ToList();
            lstNivelAceptacion = dataContext.context.NivelAceptacionHallazgo.ToList();
            lstSede = dataContext.context.Sede.ToList();
            lstCurso = dataContext.context.Curso.ToList();
            var escuelaId = dataContext.session.GetEscuelaId();

            lstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId && ca.IdEscuela==escuelaId)
                          select ca).Distinct().ToList();



            var instrumento = dataContext.context.Instrumento.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdInstrumento;
            lstCritidad = dataContext.context.Criticidad.ToList();
            lstHallazgos = dataContext.context.Hallazgo.Where(x => x.IdInstrumento == instrumento && x.IdEncuesta == Encuesta.IdEncuesta 
            && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.Sede.Codigo == "MO").ToList();

            lstPerformanceCE = new List<PerformanceEncuesta>();
            lstPerformanceCG = new List<PerformanceEncuesta>();
            lstCE = new List<OutcomeEncuestaConfig>();
            lstCG = new List<OutcomeEncuestaConfig>();
            try
            {
                var performance = dataContext.context.PerformanceEncuesta.Include(x => x.OutcomeEncuestaConfig).Include(x => x.OutcomeEncuestaConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == IdEncuesta).OrderBy(x => x.OutcomeEncuestaConfig.IdComision).ThenBy(x => x.OutcomeEncuestaConfig.Orden).ToList();
                foreach (var item in performance)
                {
                    if (item.OutcomeEncuestaConfig != null)
                    {
                        if (item.OutcomeEncuestaConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                        {
                            lstPerformanceCE.Add(item);
                        }
                        else
                        {
                            lstPerformanceCG.Add(item);
                        }
                    }
                }
                var lstOutcome = dataContext.context.OutcomeEncuestaConfig.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == Encuesta.IdCarrera && x.IdSubModalidadPeriodoAcademico == Encuesta.IdSubModalidadPeriodoAcademico && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
                lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
            }
            catch(Exception ex)
            {

            }

            if (IdHallazgo.HasValue)
            {
                var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
                IdCriterio = hallazgo.IdCriterioHallazgoEncuesta;
                IdNivelAceptacion = hallazgo.IdNivelAceptacionHallazgo;
                IdCurso = hallazgo.IdCurso;
                IdCarrera = hallazgo.IdCarrera;
                DescripcionEspanol = hallazgo.DescripcionEspanol;
                DescripcionIngles = hallazgo.DescripcionIngles;
                IdOutcome = hallazgo.IdOutcomeEncuestaConfig;
                IdCriticidad = hallazgo.IdCriticidad;
            }
            else
            {
                IdCurso = Encuesta.IdCurso;
                IdCarrera = Encuesta.IdCarrera;
            }



            //CAMPOS PARA ACTUALIZAR EL BREADCRUMB 
            this.opcion_rastro = opcion_rastro;
            this.CarreraId_rastro = CarreraId;
            this.CicloId_rastro = CicloId; 
        }
    }
}