﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
    public class AddEditFindOutcomeSurveyFDCViewModel
    {
        public Int32? IdHallazgo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionEspanol { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionIngles { get; set; }
        public String NombreOutcome { get; set; }
        public String NombreCarrera { get; set; }
        public Int32? IdOutcome { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdNivelAceptacionHallazgo { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public String Codigo { get; set; }
        public AddEditFindOutcomeSurveyFDCViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext, Int32? IdOutcome, Int32? IdCarrera, String NivelAceptacion, Int32? IdSubModalidadPeriodoAcademico, Int32? IdHallazgo)
        {
            this.IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico;
            this.IdOutcome = IdOutcome;
            this.IdCarrera = IdCarrera;
            this.NombreOutcome = dataContext.context.Outcome.FirstOrDefault(x => x.IdOutcome == IdOutcome).Nombre;
            this.NombreCarrera = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera).Codigo;
            this.IdNivelAceptacionHallazgo = dataContext.context.NivelAceptacionHallazgo.FirstOrDefault(x => x.NombreEspanol.ToUpper().Contains(NivelAceptacion.ToUpper())).IdNivelAceptacionHallazgo;

            if (IdOutcome.HasValue && IdCarrera.HasValue && IdSubModalidadPeriodoAcademico.HasValue && IdHallazgo.HasValue)
            {
                var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);

                if (hallazgo != null)
                {
                    this.Codigo = hallazgo.Codigo;
                    this.DescripcionEspanol = hallazgo.DescripcionEspanol;
                    this.DescripcionIngles = hallazgo.DescripcionIngles;
                }
            }
        }
    }
}
