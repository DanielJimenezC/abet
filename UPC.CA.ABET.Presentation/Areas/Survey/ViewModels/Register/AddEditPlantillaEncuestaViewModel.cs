﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Register
{
	public class AddEditPlantillaEncuestaViewModel
	{
		public Int32? PlantillaEncuestaId { get; set; }
		public String NombreEspanol { get; set; }

		public AddEditPlantillaEncuestaViewModel()
		{

		}

		public void Fill(CargarDatosContext ctx,Int32? _PlantillaEncuestaId)
		{
			if (_PlantillaEncuestaId.HasValue)
			{
			
				var oplantillaencuesta = ctx.context.PlantillaEncuesta.FirstOrDefault(x => x.IdPlantillaEncuesta == _PlantillaEncuestaId);

				PlantillaEncuestaId = _PlantillaEncuestaId;
				NombreEspanol = oplantillaencuesta.NombreEspanol;
	
			}
		
		}
	}
}