﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase
{
    public class ReportBaseViewModel
    {


        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public int? ModalityId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 CareerId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public int? CampusId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 CycleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? CycleFromId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? CycleToId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 CommissionId { get; set; }


        public bool? ExistData { get; set; }

        public List<Comision> LstComision { get; set; }
        public AbetEntities Context { get; set; }
        public readonly LoadCombosLogic loadCombosLogic;
        public string CurrentCulture { get; set; }
        public string CurrentModality { get; set; }
        public Dictionary<string, string> Languages { get; private set; }
        public Dictionary<string, string> Modalities { get; private set; }

        public IEnumerable<ComboItem> modalities;
        public IEnumerable<ComboItem> careers;
        public IEnumerable<ComboItem> cycles;
        public IEnumerable<ComboItem> cyclesFrom;
        public IEnumerable<ComboItem> cyclesTo;
        public IEnumerable<ComboItem> campus;
        public IEnumerable<ComboItem> evd;

        public SelectList ListCyclesForModality { get; set; }
        public SelectList ListCyclesFromForModality { get; set; }
        public SelectList ListCyclesToForModality { get; set; }
        public SelectList ListCampus { get; set; }
        public SelectList ListEvd { get; set; }


        public ReportBaseViewModel()
        {
            if (Context == null)
            {
                Context = new AbetEntities();
                Context.Database.CommandTimeout = 60;
            }
            this.loadCombosLogic = new LoadCombosLogic(Context);
            var cultureCookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);

            this.CurrentCulture = cultureCookie == null ? Culture.ESPANOL : cultureCookie.Value ?? Culture.ESPANOL;
            this.CurrentModality = modalityCookie == null ? Modality.PREGRADO_REGULAR : modalityCookie.Value ?? Modality.PREGRADO_REGULAR;
            if (CurrentCulture == Culture.INGLES)
            {
                Languages = new Dictionary<string, string>
                {
                    { Culture.INGLES, "English" },
                    { Culture.ESPANOL, "Spanish" }
                };
                Modalities = new Dictionary<string, string>
                {
                    { Modality.PREGRADO_REGULAR, "Undergraduate Regular" },
                    { Modality.PREGRADO_EPE, "Undergraduate EPE" }
                };
            }
            else
            {
                Languages = new Dictionary<string, string>
                {
                    { Culture.ESPANOL, "Español" },
                    { Culture.INGLES, "Inglés" }
                };
                Modalities = new Dictionary<string, string>
                {
                    { Modality.PREGRADO_REGULAR, "Pregrado Regular" },
                    { Modality.PREGRADO_EPE, "Pregrado EPE" }
                };
            }
            LstComision = new List<Comision>();
        }

        public SelectList DropDownListCyclesForModality(string ModalityId, int Skip = 0)
        {
            cycles = loadCombosLogic.ListCyclesForModality(ModalityId: ModalityId);
            return cycles.ToSelectList();
        }

        public SelectList DropDownListCyclesFromForModality(string ModalityId, int CycleFromId = 0, int Skip = 0)
        {
            cyclesFrom = loadCombosLogic.ListCyclesFromForModality(ModalityId: ModalityId);
            return cyclesFrom.ToSelectList();
        }

        public SelectList DropDownListCyclesToForModality(string ModalityId, int CycleFromId, int Skip = 0)
        {
            cyclesTo = loadCombosLogic.ListCyclesToForModality(ModalityId: ModalityId, CycleFromId: CycleFromId);
            return cyclesTo.ToSelectList();
        }

        public SelectList DropDownListModalities()
        {
            modalities = loadCombosLogic.ListModalities(CurrentCulture);
            return modalities.ToSelectList();
        }

        public SelectList DropDownListCareers()
        {
            careers = loadCombosLogic.ListCareers(CurrentCulture, CurrentModality);
            return careers.ToSelectList();
        }

        public SelectList DropDownListCampus(int CycleId = 0)
        {
            campus = loadCombosLogic.ListCampus(CycleId);
            return campus.ToSelectList();
        }

        public SelectList DropDownListEVD(int CycleFromId = 0)
        {
            evd = loadCombosLogic.ListEvd(CycleId);
            return evd.ToSelectList();
        }
    }

    public static class ReportBaseModelExtensions
    {
        public class OptionViewModel
        {
            public int Value { get; set; }
            public string TextContent { get; set; }
        }

        public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new SelectList(dictionary, "Key", "Value");
        }
        public static SelectList ToSelectList(this IEnumerable<ComboItem> data)
        {
            return new SelectList(data, "Key", "Value");
        }
        public static IEnumerable<OptionViewModel> ToOptions(this Dictionary<int, string> dictionary)
        {
            return dictionary.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
        public static IEnumerable<OptionViewModel> ToOptions(this IEnumerable<ComboItem> data)
        {
            return data.Select(d => new OptionViewModel
            {
                Value = d.Key,
                TextContent = d.Value
            });
        }
    }
}