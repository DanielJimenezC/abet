﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase
{
    public class OptionViewModel
    {
        public int Value { get; set; }
        public string TextContent { get; set; }
    }
}