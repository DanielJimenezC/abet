﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.ReportBase
{
    public class DataTableRequestViewModel
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public DataTableSearchRequestViewModel search { get; set; }
        public IList<DataTableColumnRequestViewModel> columns { get; set; }
        public IList<DataTableOrderRequestViewModel> order { get; set; }

        public DataTableRequestViewModel()
        {
            this.search = new DataTableSearchRequestViewModel();
            this.columns = new List<DataTableColumnRequestViewModel>();
            this.order = new List<DataTableOrderRequestViewModel>();
            this.start = 0;
            this.length = 10;
        }
    }

    public class DataTableSearchRequestViewModel
    {
        public string value { get; set; }
        public bool regex { get; set; }

        public DataTableSearchRequestViewModel()
        {
            this.value = string.Empty;
        }
    }

    public class DataTableOrderRequestViewModel
    {
        public int column { get; set; }
        public string dir { get; set; }

        public DataTableOrderRequestViewModel()
        {
            this.column = 1;
            this.dir = "asc";
        }
    }

    public class DataTableColumnRequestViewModel
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public DataTableSearchRequestViewModel search { get; set; }

        public DataTableColumnRequestViewModel()
        {
            this.search = new DataTableSearchRequestViewModel();
        }
    }


    public class DataTableResponseViewModel<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IList<T> data { get; set; }
        public string error { get; set; }

        public string DT_RowId { get; set; }
        public string DT_RowClass { get; set; }
        public object DT_RowData { get; set; }
        public object DT_RowAttr { get; set; }
    }
}