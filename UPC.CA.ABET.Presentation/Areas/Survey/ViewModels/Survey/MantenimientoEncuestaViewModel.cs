﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class MantenimientoEncuestaViewModel
    {
        public String Tipo { get; set; }
        public String Ciclo { get; set; }
        public String Curso { get; set; }
        public String Seccion { get; set; }
        public String Estado { get; set; }
        public String Carrera { get; set; }
        public String NroPractica { get; set; }
        public List<String> lstCiclo { get; set; }
        public List<String> lstCarrera { get; set; }
        public List<String> lstNroPractica { get; set; }
        public List<String> lstCurso { get; set; }
        public List<String> lstSeccion { get; set; }
        public List<String> lstEstado { get; set; }
        public void Fill(String _Tipo)
        {
            Tipo = _Tipo;
            lstCiclo = new List<String>();
            lstCarrera = new List<String>();
            lstNroPractica = new List<String>();
            lstEstado = new List<String>();
            lstSeccion = new List<String>();
            lstCurso = new List<String>();
        }
    }
}