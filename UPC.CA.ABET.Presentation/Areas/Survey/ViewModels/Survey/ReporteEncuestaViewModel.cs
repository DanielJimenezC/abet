﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class ReporteEncuestaViewModel
    {
        public String Tipo { get; set; }
        public String Reporte { get; set; }
        public String Ciclo { get; set; }
        public String Idioma { get; set; }
        public List<String> lstCiclo { get; set; }
        public List<String> lstIdioma { get; set; }
        public void Fill(String _Tipo, String _Reporte)
        {
            Tipo = _Tipo;
            Reporte = _Reporte;
            lstCiclo = new List<String>();
            lstIdioma = new List<String>();
        }
    }
}