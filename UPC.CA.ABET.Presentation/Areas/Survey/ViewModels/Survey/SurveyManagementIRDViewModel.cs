﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class SurveyManagementIRDViewModel
    {
        public String Tipo { get; set; }

        public void Fill()
        {
            Tipo = ConstantHelpers.ENCUESTA.ARD;
        }
    }
}




