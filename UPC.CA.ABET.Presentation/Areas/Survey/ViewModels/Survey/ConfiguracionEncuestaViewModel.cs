﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class ConfiguracionEncuestaViewModel
    {
        public String Tipo { get; set; }
        public String Ciclo { get; set; }
        public List<String> lstCiclo { get; set; }
        public void Fill(String _Tipo)
        {
            Tipo = _Tipo;
            lstCiclo = new List<String>();
        }
    }
}