﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class SeccionCursoViewModel
    {
        public List<GetSeccionCurso_Result> lstData { get; set; }
        public SeccionCursoViewModel(CargarDatosContext dataContext)
        {
            lstData = dataContext.context.GetSeccionCurso(null, null, null, null).ToList();
        }
    }
}