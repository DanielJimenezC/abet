﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class HallazgoEncuestaViewModel
    {
        public String Tipo { get; set; }
        public String Ciclo { get; set; }
        public Alumno Alumno { get; set; }
        public Carrera Carrera { get; set; }
        public String Criterio { get; set; }
        public String Curso { get; set; }
        public List<String> lstCriterio { get; set; }
        public List<String> lstCurso { get; set; }
        public void Fill(String _Tipo,String _Ciclo, Alumno _Alumno, Carrera _Carrera)
        {
            Tipo = _Tipo;
            Ciclo = _Ciclo;
            Alumno = _Alumno;
            Carrera = _Carrera;
            lstCriterio = new List<String>();
            lstCurso = new List<String>();
        }
    }
}