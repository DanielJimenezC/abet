﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Survey
{
    public class RegistrarEncuestaViewModel
    {
        /* Plantilla segun tipo deberia sacarse de BD*/
        public String Tipo { get; set; }
        public Int32 Sede { get; set; }
        public Int32 Ciclo { get; set; }
        public Int32 Seccion { get; set; }
        public Int32 Curso { get; set; }
        public Int32 Carrera { get; set; }
        public String CodigoAlumno { get; set; }
        public String NombreAlumno { get; set; }
        public Boolean NroPractica { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public String RUC { get; set; }
        public String RazonSocial { get; set; }
        public String JefeDirecto { get; set; }
        public String TelefonoJefeDirecto { get; set; }
        public String EmailJefeDirecto { get; set; }
        public Int32 CantidadMaxima { get; set; }
        public List<String> lstCiclo { get; set; }
        public List<String> lstCarrera { get; set; }
        public List<String> lstCurso { get; set; }
        public List<String> lstSeccion { get; set; }
        public List<String> lstSede { get; set; }
        public Int32? EncuestaId { get; set; }
        public void Fill(String _Tipo, Int32? _EncuestaId)
        {
            Tipo = _Tipo;
            lstCiclo = new List<String>();
            lstCarrera = new List<String>();
            lstCurso = new List<String>();
            lstSeccion = new List<String>();
            lstSede = new List<String>();
            EncuestaId = _EncuestaId;
        }
    }
}