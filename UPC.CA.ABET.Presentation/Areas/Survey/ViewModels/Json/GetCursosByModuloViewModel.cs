﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
	public class GetCursosByModuloViewModel
	{
		public Int32? IdPregunta { get; set; }
		public Int32? IdModulo { get; set; }
		public List<SelectListItem> LstCurso { get; set; }

		public GetCursosByModuloViewModel()
		{
			LstCurso = new List<SelectListItem>();
		}

		public void Fill(CargarDatosContext ctx, int? IdPregunta,int? IdModulo)
		{
			this.IdPregunta = IdPregunta;
			this.IdModulo = IdModulo;
			if (IdPregunta.HasValue && IdModulo.HasValue)
			{
				LstCurso = (from a in ctx.context.ComentarioDelegado
							join b in ctx.context.Curso on a.IdCurso equals b.IdCurso
							join c in ctx.context.EncuestaVirtualDelegado on a.IdEncuestaVirtualDelegado equals c.IdEncuestaVirtualDelegado
							join d in ctx.context.SubModalidadPeriodoAcademicoModulo on c.IdSubModalidadPeriodoAcademicoModulo equals d.IdSubModalidadPeriodoAcademicoModulo
							where (a.IdPregunta == this.IdPregunta || IdPregunta==0) && (d.IdModulo==this.IdModulo || IdModulo==0)
							select new SelectListItem { Text = b.Codigo + " - " + b.NombreEspanol,
								Value = b.IdCurso.ToString() }).Distinct().ToList();
							
			}
		}
	}
}