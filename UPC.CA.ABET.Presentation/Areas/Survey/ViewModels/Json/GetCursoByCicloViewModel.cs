﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetCursoByCicloViewModel
    {
        public Int32? cicloId { get; set; }
        public List<Curso> lstCurso { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? cicloId)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            this.cicloId = cicloId;
            lstCurso = dataContext.context.CursoPeriodoAcademico.Include(x => x.Curso).Where(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == cicloId).ToList().
                Select(x => new Curso { NombreEspanol = (language == ConstantHelpers.CULTURE.ESPANOL ? x.Curso.NombreEspanol + "-" + x.Curso.Codigo : x.Curso.NombreIngles + "-" + x.Curso.Codigo), IdCurso = x.IdCurso }).ToList();
            //lstCurso = dataContext.context.spGetCursosEncuestaLCFC(cicloId, "").Select(x => new Curso { IdCurso = x.IdCurso, NombreEspanol = x.NombreEspanol + "-" + x.Codigo_Curso, NombreIngles = x.NombreIngles + "-" + x.Codigo_Curso }).ToList();
        }
    }
}