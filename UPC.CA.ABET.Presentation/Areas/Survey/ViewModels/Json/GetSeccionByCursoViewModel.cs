﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetSeccionByCursoViewModel
    {
        public Int32? cursoId { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? cursoId, Int32? IdCarrera, Int32? IdSede, Int32? IdPeriodoAcademico)
        {
            this.cursoId = cursoId;

            var CodCurso = dataContext.context.Curso.FirstOrDefault(x => x.IdCurso == this.cursoId).Codigo;
            //var data = dataContext.context.GetSeccionCurso(null, CodCurso, IdPeriodoAcademico, IdSede).ToList();
            var data = dataContext.context.Database.SqlQuery<GetSeccionCurso_Result>("GetSeccionCurso {0}, {1}, {2}, {3}", null, CodCurso, IdPeriodoAcademico, IdSede).ToList();
            lstSeccion = data.Select(x => new Seccion { Codigo = x.CodigoSeccion + " - " + x.CodigoSede, IdSeccion = x.IdSeccion }).ToList();

        }
    }
}