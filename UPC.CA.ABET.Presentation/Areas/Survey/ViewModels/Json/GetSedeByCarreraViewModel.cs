﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetSedeByCarreraViewModel
    {
        public Int32? carreraId { get; set; }
        public List<Sede> lstSede { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? carreraId)
        {
            this.carreraId = carreraId;
            lstSede = dataContext.context.SedeCarrera.Include( x => x.Sede ).Where( x => x.IdCarrera == carreraId ).ToList().
                Select(x => new Sede { Codigo = x.Sede.Codigo, IdSede = x.IdSede }).ToList();
        }
    }
}