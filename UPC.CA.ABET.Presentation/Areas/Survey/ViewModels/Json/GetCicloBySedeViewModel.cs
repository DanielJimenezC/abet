﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetCicloBySedeViewModel
    {
        public Int32? sedeId { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<PeriodoAcademico> lstSubModalidadPeriodoAcademico { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? sedeId)
        {
            this.sedeId = sedeId;
            //lstCiclo = dataContext.context.PeriodoAcademicoSede.Include(x => x.PeriodoAcademico).Where(x => x.IdSede == sedeId).ToList().
                //Select(x => new PeriodoAcademico { CicloAcademico = x.PeriodoAcademico.CicloAcademico, IdPeriodoAcademico = x.IdPeriodoAcademico }).ToList();
            lstCiclo = dataContext.context.PeriodoAcademicoSede.Include( x => x.SubModalidadPeriodoAcademico.PeriodoAcademico ).Where( x => x.IdSede == sedeId).ToList().
                Select(x => new PeriodoAcademico { CicloAcademico = x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, IdPeriodoAcademico = x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico }).ToList();
        }
    }
}