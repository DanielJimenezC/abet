﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetAlumnoViewModel
    {
        public class AlumnoObject
        {
            public String Nombre;
            public Int32 CarreraId;
            public String NombreCarrera;
            //public Int32 CicloId;
            public Int32 SuModalidadPeriodoAcademicoId;
            public String NombreCiclo;
            public Int32 SedeId;
            public String NombreSede;
        }
        public AlumnoObject data { get; set; }
        public void Filter(CargarDatosContext dataContext, String Codigo)
        {
            var currentCulture = dataContext.currentCulture;
            data = new AlumnoObject();
            var alumno = dataContext.context.AlumnoMatriculado.Include(x => x.Alumno).FirstOrDefault(x => x.Alumno.Codigo == Codigo);
            if (alumno != null)
            {
                data.Nombre = alumno.Alumno.Nombres + " " + alumno.Alumno.Apellidos;
                data.CarreraId = alumno.IdCarrera.Value;
                data.NombreCarrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? alumno.Carrera.NombreEspanol : alumno.Carrera.NombreIngles;
                //data.CicloId = alumno.IdPeriodoAcademico;
                //data.NombreCiclo = alumno.PeriodoAcademico.CicloAcademico;
                data.SuModalidadPeriodoAcademicoId = alumno.SubModalidadPeriodoAcademico.ToInteger();
                data.NombreCiclo = alumno.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
                data.SedeId = alumno.IdSede.Value;
                data.NombreSede = alumno.Sede.Nombre;
            }
            else
            {
                data.Nombre = " ";
                data.CarreraId = 0;
                //data.CicloId = 0;
                data.SuModalidadPeriodoAcademicoId = 0;
                data.SedeId = 0;
            }

        }
    }
}