﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetComisionByCarreraViewModel
    {
        public Int32? CarreraId { get; set; }
        public List<CarreraComision> lstComision { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? CarreraId,Int32? IdAcreditadora, Int32? IdPeriodoAcademico)
        {
            if (IdAcreditadora == null)
                IdAcreditadora = 0;
            if (IdPeriodoAcademico == null)
                IdPeriodoAcademico = 0;

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            this.CarreraId = CarreraId;
            
        }
    }
}