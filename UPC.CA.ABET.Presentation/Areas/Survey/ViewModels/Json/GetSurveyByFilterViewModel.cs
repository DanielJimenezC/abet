﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetSurveyByFilterViewModel
    {
        public Int32? carreraId;
        public Int32? sedeId;
        public Int32? cicloId;
        public Int32? cursoId;
        public Int32? seccionId;
        public List<TemplateEncuesta> lstTemplate { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? carreraId,Int32? sedeId, Int32? cicloId, Int32? cursoId, Int32? seccionId)
        {
            //this.carreraId = carreraId;
            //this.sedeId = sedeId;
            //this.cicloId = cicloId;
            //this.cursoId = cursoId;
            //this.seccionId = seccionId;
            //lstTemplate = dataContext.context.TemplateEncuesta.Where(x => x.IdCarrera == this.carreraId
              //  && x.IdSede == this.sedeId && x.IdPeriodoAcademico == this.cicloId && x.IdCurso == this.cursoId
                //&& x.IdSeccion == this.seccionId && x.Estado == ConstantHelpers.ESTADO.ACTIVO).ToList().Select(x => new TemplateEncuesta { Nombre = x.Nombre, IdTemplateEncuesta = x.IdTemplateEncuesta }).ToList();
        }
    }
}