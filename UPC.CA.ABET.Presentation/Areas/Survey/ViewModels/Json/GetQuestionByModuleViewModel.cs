﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
	public class GetQuestionByModuleViewModel
	{
		public Int32? IdModulo { get; set; }
		public List<SelectListItem> LstPregunta { get; set; }

		public GetQuestionByModuleViewModel()
		{
			LstPregunta = new List<SelectListItem>();
		}

		public void Filter(CargarDatosContext ctx, Int32? parModuloId)
		{
			var currentCulture = ctx.currentCulture;
			this.IdModulo = parModuloId;

			if (IdModulo.HasValue)
			{

				LstPregunta = (from a in ctx.context.EncuestaVirtualDelegado
							   join b in ctx.context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademicoModulo equals b.IdSubModalidadPeriodoAcademicoModulo
							   join c in ctx.context.Pregunta on a.IdPlantillaEncuesta equals c.IdPlantillaEncuesta
							   where b.IdModulo == IdModulo /*&& c.TipoPregunta.IdTipoPregunta==1*/
							   select new SelectListItem { Text = c.DescripcionEspanol, Value = c.IdPregunta.ToString() }).ToList();
			}
		}
	}
}