﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetModulesByCicloViewModel
    {
        public Int32? IdPeriodoAcademico { get; set; }
        public List<SelectListItem> LstModulo { get; set; }

        public GetModulesByCicloViewModel()
        {
            LstModulo = new List<SelectListItem>();
        }

        public void Filter(CargarDatosContext dataContext, Int32? parPeriodoAcademicoId)
        {
            var currentCulture = dataContext.currentCulture;
            this.IdPeriodoAcademico = parPeriodoAcademicoId;

            if (IdPeriodoAcademico.HasValue)
            {
                LstModulo = (from a in dataContext.context.SubModalidadPeriodoAcademicoModulo
                             join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                             join c in dataContext.context.Modulo on a.IdModulo equals c.IdModulo
                             where b.IdPeriodoAcademico == IdPeriodoAcademico
                             select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();

				
            }                       
        }

    }
}