﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
	public class GetDocentesByCursoViewModel
	{
		public Int32? IdCurso { get; set; }
        public Int32 IdAlumno { get; set; }
		public List<SelectListItem> LstDocente { get; set; }

		public GetDocentesByCursoViewModel()
		{
			LstDocente = new List<SelectListItem>();
		}

		public void Filter(CargarDatosContext ctx, Int32? IdCurso, Int32? EVDId)
		{
			var currentCulture = ctx.currentCulture;
			this.IdCurso = IdCurso;

            var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado== EVDId);

			var oidsmpam = ctx.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVD.IdSubModalidadPeriodoAcademicoModulo);


			if (IdCurso.HasValue)
			{
                var idsDocente = (from c in ctx.context.ComentarioDelegado
                                  where c.IdAlumno == IdAlumno
                                  select c.IdDocente).ToList();

                LstDocente = (from a in ctx.context.DocenteSeccion
							  join b in ctx.context.Seccion on a.IdSeccion equals b.IdSeccion
							  join c in ctx.context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
							  join d in ctx.context.Curso on c.IdCurso equals d.IdCurso
							  join e in ctx.context.Docente on a.IdDocente equals e.IdDocente
							  where (c.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && b.IdModulo == oidsmpam.IdModulo && d.IdCurso==IdCurso) && !idsDocente.Contains(e.IdDocente)
                              select new SelectListItem { Text = e.Codigo + " - " + e.Nombres + " " + e.Apellidos, Value = e.IdDocente.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();

			}
		}
	}
}