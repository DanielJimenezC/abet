﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetCarreraByCicloViewModel
    {
        public Int32? cicloId { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? cicloId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            var currentCulture = dataContext.currentCulture;
            this.cicloId = cicloId;

            if (cicloId.HasValue)
            {
                int idsubmoda = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                //lstCarrera = dataContext.context.CarreraPeriodoAcademico.Include(x => x.Carrera).Where(x => x.IdPeriodoAcademico == cicloId && x.Carrera.IdEscuela == escuelaId).ToList()
                lstCarrera = dataContext.context.CarreraPeriodoAcademico.Include(x => x.Carrera).Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.Carrera.IdEscuela == escuelaId).ToList()
                                                                                                .Select(x => new Carrera { NombreEspanol = (currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.Carrera.NombreEspanol : x.Carrera.NombreIngles), IdCarrera = x.IdCarrera }).ToList();
            }
            else
                lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList().Select(x => new Carrera { NombreEspanol = (currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles), IdCarrera = x.IdCarrera }).ToList();
        }

    }
}