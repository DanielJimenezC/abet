﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetCicloByCarreraViewModel
    {
        public Int32? carreraId { get; set; }
        public List<PeriodoAcademico> lstPeriodoAcademico { get; set; }
        //public List<SubModalidadPeriodoAcademico> lstSubModalidadPeriodoAcademico { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? carreraId)
        {
            var currentCulture = dataContext.currentCulture;
            this.carreraId = carreraId;
            if (carreraId.HasValue)
                //lstPeriodoAcademico = dataContext.context.CarreraPeriodoAcademico.Include(x => x.PeriodoAcademico).Where(x => x.IdCarrera == carreraId).ToList().
                //    Select(x => new PeriodoAcademico { CicloAcademico = x.PeriodoAcademico.CicloAcademico, IdPeriodoAcademico = x.IdPeriodoAcademico }).ToList();
                lstPeriodoAcademico = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico).Where(x => x.IdCarrera == carreraId).ToList().
                //lstSubModalidadPeriodoAcademico = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico).Where(x => x.IdCarrera == carreraId).ToList()
                   //Select(x => new PeriodoAcademico { CicloAcademico = x.PeriodoAcademico.CicloAcademico, IdPeriodoAcademico = x.IdSubModalidadPeriodoAcademico }).ToList();
                   Select(x => new PeriodoAcademico { CicloAcademico = x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, IdPeriodoAcademico = x.IdSubModalidadPeriodoAcademico.ToInteger() }).ToList();
            else
                lstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList().Select(x => new PeriodoAcademico { CicloAcademico = x.CicloAcademico, IdPeriodoAcademico = x.IdPeriodoAcademico }).ToList();
                //lstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList().Select(x => new SubModalidadPeriodoAcademico { CicloAcademico = x.IdSubModalidadPeriodoAcademico, IdPeriodoAcademico = x.IdPeriodoAcademico }).ToList();
        }
    }
}