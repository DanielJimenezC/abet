﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class FiltrosViewModel
    {
        public Int32? IdCarrera { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdSubModalidadPeriodoAcademicoModulo { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdSeccion { get; set; }
        //public void Fill(Int32? idCarrera, Int32? idSede, Int32? PeriodoAcademicoId, Int32? idCurso, Int32? idSeccion)
        public void Fill(Int32? idCarrera, Int32? idSede, Int32? idPeriodoAcademico, Int32? idCurso, Int32? idSeccion, Int32? idSubModalidadPeriodoAcademico)
        {
            IdCarrera = idCarrera; IdSede = idSede; IdPeriodoAcademico = idPeriodoAcademico; IdCurso = idCurso;
            IdSeccion = idSeccion;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
        }
    }
}