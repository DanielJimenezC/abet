﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetEPEStudentsCodeViewModel
    {
        public List<SelectListItem> LstCodigos { get; set; }

        public GetEPEStudentsCodeViewModel()
        {
            LstCodigos = new List<SelectListItem>();
        }

        public void CargarDatos(CargarDatosContext dataContext,int ModalidadId)
        {
            var codigosAlumnosInvitados = (from ai in dataContext.context.AlumnoInvitado
                                       select ai.codigo).ToList();

            LstCodigos = (from a in dataContext.context.Alumno
                         join b in dataContext.context.AlumnoMatriculado on a.IdAlumno equals b.IdAlumno
                         join c in dataContext.context.SubModalidadPeriodoAcademico on b.IdSubModalidadPeriodoAcademico equals c.IdSubModalidadPeriodoAcademico
                         join d in dataContext.context.SubModalidad on c.IdSubModalidad equals d.IdSubModalidad
                         join e in dataContext.context.Modalidad on d.IdModalidad equals e.IdModalidad
                         join f in dataContext.context.Carrera on d.IdSubModalidad equals f.IdSubmodalidad
                         where e.IdModalidad == ModalidadId && !codigosAlumnosInvitados.Contains(a.Codigo)
                         select new SelectListItem { Text = a.Codigo, Value = a.IdAlumno.ToString() }).Distinct().ToList();
        }

    }
}