﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetGraficoReporteOutcomePPPViewModel
    {
        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdComision { get; set; }
        public Int32 IdNumeroPractica { get; set; }
        public List<String> LstOutcome { get; set; }
        public List<String> LstSede { get; set; }
        public List<Double> LstPuntaje { get; set; }
        public Dictionary<String,List<Double>> DicSerie { get; set; }
    }
}