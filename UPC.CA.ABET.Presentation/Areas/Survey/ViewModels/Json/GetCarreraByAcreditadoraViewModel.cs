﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;


namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetCarreraByAcreditadoraViewModel
    {
        public Int32? AcreditadoraId { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public void Filter(CargarDatosContext dataContext, Int32? AcreditadoraId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();

            var currentCulture = dataContext.currentCulture;
            this.AcreditadoraId = AcreditadoraId;
            if (AcreditadoraId.HasValue)
                lstCarrera = dataContext.context.CarreraAcreditadora.Include(x => x.Carrera).Where(x => x.IdAcreditadora == AcreditadoraId && x.Carrera.IdEscuela == escuelaId).ToList().
                 Select(x => new Carrera { NombreEspanol = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.Carrera.NombreEspanol : x.Carrera.NombreIngles, IdCarrera = x.IdCarrera }).ToList();
            else
                lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).Select(x => new Carrera { NombreEspanol = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.NombreEspanol : x.NombreIngles, IdCarrera = x.IdCarrera }).ToList(); ;
        }
    }
}