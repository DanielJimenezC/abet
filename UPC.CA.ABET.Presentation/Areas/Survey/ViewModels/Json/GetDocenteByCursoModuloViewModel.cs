﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
	public class GetDocenteByCursoModuloViewModel
	{
		public Int32? IdModulo { get; set; }
		public Int32? IdCurso { get; set; }
		public List<SelectListItem> LstDocente { get; set; }

		public GetDocenteByCursoModuloViewModel()
		{
			LstDocente = new List<SelectListItem>();
		}
		public void Fill(CargarDatosContext ctx, int? IdModulo, int? IdCurso)
		{
			this.IdCurso = IdCurso;
			this.IdModulo = IdModulo;

			if (IdCurso.HasValue && IdModulo.HasValue)
			{
				LstDocente = (from a in ctx.context.ComentarioDelegado
							  join b in ctx.context.Docente on a.IdDocente equals b.IdDocente
							  join c in ctx.context.EncuestaVirtualDelegado on a.IdEncuestaVirtualDelegado equals c.IdEncuestaVirtualDelegado
							  join d in ctx.context.SubModalidadPeriodoAcademicoModulo on c.IdSubModalidadPeriodoAcademicoModulo equals d.IdSubModalidadPeriodoAcademicoModulo
							  where (a.IdCurso == this.IdCurso || IdCurso==0) && d.IdModulo == this.IdModulo
							  select new SelectListItem { Text = b.Codigo + " - " + b.Nombres + " " + b.Apellidos, Value = b.IdDocente.ToString() }).Distinct().ToList();
			}
		}
	}
}