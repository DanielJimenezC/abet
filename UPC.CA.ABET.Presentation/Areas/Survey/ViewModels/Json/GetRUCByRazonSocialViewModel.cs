﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetRUCByRazonSocialViewModel
    {
        public String RUC { get; set; }
        public void Filter(CargarDatosContext dataContext, String RazonSocial)
        {
            var encuesta = dataContext.context.Encuesta.FirstOrDefault(x => x.RazonSocial == RazonSocial);
            if( encuesta != null)
            {
                RUC = encuesta.RUC;
            }
            else
            {
                RUC = " ";
            }
        }
    }
}