﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Json
{
    public class GetEncuestasMantenimientoPPPViewModel
    {
        public List<Encuesta> lstEncuesta { get; set; }
        public Int32 IdAcreditadora { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdComision { get; set; }
        public Int32 IdSede { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 NroPractica { get; set; }
        public void Filter(CargarDatosContext dataContext)
        {

        }
    }
}