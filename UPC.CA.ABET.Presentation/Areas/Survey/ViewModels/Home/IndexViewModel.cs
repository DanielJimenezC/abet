﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Home
{
    public class IndexViewModel
    {
        public Int32 cantidadPPP { get; set; }
        public Int32 cantidadFDC { get; set; }
        public Int32 cantidadGRA { get; set; }
        public void Fill(CargarDatosContext dataContext)
        {
            var lstEncuesta = dataContext.context.Encuesta.ToList();
            cantidadPPP = lstEncuesta.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).Count();
            cantidadFDC = lstEncuesta.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).Count();
            cantidadGRA = lstEncuesta.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).Count();
        }
    }
}