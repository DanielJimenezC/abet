﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using UPC.CA.ABET.Logic.Areas.Admin;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.RegisterToken
{
    public class AddSurveyGRATokenViewModel
    {
        public String Tipo { get; set; }
        public String Comentario { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 IdSede { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        public int? IdCiclo { get; set; }
        public List<OutcomeEncuestaConfig> lstCE { get; set; }
        public List<OutcomeEncuestaConfig> lstCG { get; set; }
        public string Nombres { get; set; }
        public string codigo { get; set; }
        public string Escuela { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCE { get; set; }
        public List<PerformanceEncuesta> lstPerformanceCG { get; set; }
        public List<EncuestaComentario> lstComentarios { get; set; }

        public int? IdAlumno { get; set; }

        public void Fill(CargarDatosContext dataContext, int? IdAlumno, int? IdCiclo, Int32 idCarrera, Int32? idSubModalidadPeriodoAcademico, int EscuelaId)
        {
            this.IdCiclo = IdCiclo;
            var alumno = dataContext.context.Alumno.FirstOrDefault(x => x.IdAlumno == IdAlumno);
            Nombres = alumno.Nombres + " " + alumno.Apellidos;
            codigo = "u" + alumno.Codigo;
            var escuela = dataContext.context.Escuela.FirstOrDefault(x => x.IdEscuela == EscuelaId);
            Escuela = escuela.Nombre;

            this.IdAlumno = IdAlumno;
            IdCarrera = idCarrera;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            var currentCulture = dataContext.currentCulture;
            Tipo = ConstantHelpers.ENCUESTA.GRA;
            lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstCarrera = dataContext.context.Carrera.ToList();

            lstSede = dataContext.context.Sede.ToList();
            lstPerformanceCE = new List<PerformanceEncuesta>();
            lstPerformanceCG = new List<PerformanceEncuesta>();

            var lstOutcome = dataContext.context.OutcomeEncuestaConfig.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdCarrera == IdCarrera && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).AsQueryable();

            if (IdSubModalidadPeriodoAcademico.HasValue)
                lstOutcome = lstOutcome.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).AsQueryable();
            if (IdCarrera.HasValue)
                lstOutcome = lstOutcome.Where(x => x.IdCarrera == IdCarrera).AsQueryable();

            lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
            lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL).ToList();
        }
    }
}