﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.RegisterToken
{
	public class AddSurveyEVDTokenViewModel
	{
		public List<SelectListItem> LstCurso { get; set; }
		public Int32 IdCurso { get; set; }
		public List<SelectListItem> LstDocente { get; set; }
		public Int32 IdDocente { get; set; }
		public List<Pregunta> LstPregunta { get; set; }
		public List<string> LstComentarios { get; set; }
		public List<DetalleNiveles> LstDetalleNiveles { get; set; }
		public Int32? IdEncuestaVirtualDelegado { get; set; }
		public Int32? IdTipoEncuesta { get; set; }
		public Int32? IdAlumno { get; set; }
		public Int32? IdEncuestaToken { get; set; }
		public EncuestaToken encuestaToken { get; set; }
		public Int32? IdSubModalidadPeriodoAcademico { get; set; }
		public Int32? IdCarrera { get; set; }
		public Int32? IdSeccion { get; set; }
        public Int32 CursosCount { get; set; }
		public String Ciclo { get; set; }
		public String Modulo { get; set; }
		public String Carrera { get; set; }
		public String Token { get; set; }
		public String Codigo { get; set; }

		public AddSurveyEVDTokenViewModel()
        {
            LstPregunta = new List<Pregunta>();
            LstDetalleNiveles = new List<DetalleNiveles>();
            LstCurso = new List<SelectListItem>();
            LstDocente = new List<SelectListItem>();
            LstComentarios = new List<string>();
        }

        public void CargarDatos(CargarDatosContext ctx,String parToken)
        {
			IdTipoEncuesta = ctx.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == "EVD").IdTipoEncuesta;

			IdAlumno = (from a in ctx.context.EncuestaToken
					   join b in ctx.context.Alumno on a.IdAlumno equals b.IdAlumno
					   where a.Token == parToken
					   select b.IdAlumno).FirstOrDefault();

			Token = parToken;
		

			encuestaToken = ctx.context.EncuestaToken.FirstOrDefault(x => x.Token == parToken);

			var _EVD = ctx.context.EncuestaVirtualDelegado.Where(x => x.IdEncuestaVirtualDelegado == encuestaToken.IdEncuestaVirtualDelegado).FirstOrDefault();

		
			int ModalidadId = ctx.session.GetModalidadId();
			var Modalidad = ctx.context.Modalidad.Where(x => x.IdModalidad == ModalidadId).FirstOrDefault();
			var Carrera = ctx.context.Carrera.Where(x => x.IdCarrera == _EVD.IdCarrera).FirstOrDefault();
			var Periodo = ctx.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == _EVD.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault().SubModalidadPeriodoAcademico.PeriodoAcademico;
			var Modulo = ctx.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == _EVD.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault().Modulo;

			Codigo = ctx.context.Usp_GetCodigoNomenclatura(ctx.session.GetEncuestaVirtualDelegadoId()).FirstOrDefault().ToString();



			var Submodapamodu = ctx.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == _EVD.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault();
			
		

			var LstCur = (from c in ctx.context.ComentarioDelegado
							where c.IdAlumno == IdAlumno && c.IdEncuestaVirtualDelegado==_EVD.IdEncuestaVirtualDelegado
							select c.IdCurso).ToList();

			var ListCursosSeccionEsDelegado = (from a in ctx.context.AlumnoSeccion
											   join b in ctx.context.AlumnoMatriculado on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
											   join c in ctx.context.Alumno on b.IdAlumno equals c.IdAlumno
											   join d in ctx.context.Seccion on a.IdSeccion equals d.IdSeccion
											   join e in ctx.context.SeccionCurso on a.IdSeccion equals e.IdSeccion
											   join f in ctx.context.Curso on e.IdCurso equals f.IdCurso
											   join g in ctx.context.DocenteSeccion on a.IdSeccion equals g.IdSeccion
											   join h in ctx.context.Docente on g.IdDocente equals h.IdDocente
											   where a.EsDelegado == true && b.IdSubModalidadPeriodoAcademico == _EVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico && d.IdModulo == _EVD.SubModalidadPeriodoAcademicoModulo.IdModulo && b.IdAlumno== IdAlumno && !LstCur.Contains(f.IdCurso)
											   select new
											   {
												   IdAlumno = c.IdAlumno,
												   IdSeccion = d.IdSeccion,
												   IdCurso = f.IdCurso,
												   NombreEspanolCurso = f.NombreEspanol,
												   CodigoCurso = f.Codigo,
												   CodigoDocente = h.Codigo,
												   NombreDocente = h.Nombres,
												   ApellidoDocente = h.Apellidos,
												   IdDocente = g.IdDocente,
												   IdModulo = d.IdModulo,
												   IdSubModalidadPeriodoAcademico = b.IdSubModalidadPeriodoAcademico
											   }
											  ).ToList();


            this.CursosCount = ListCursosSeccionEsDelegado.Count;

			if (ListCursosSeccionEsDelegado.Count > 0)
			{

				IdEncuestaToken = encuestaToken.IdEncuestaToken;

				IdSubModalidadPeriodoAcademico = encuestaToken.IdSubModalidadPeriodoAcademico;

				IdCarrera = encuestaToken.IdCarrera;

				if (IdEncuestaVirtualDelegado.HasValue)
				{
					var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado);

					LstPregunta = ctx.context.Pregunta.Where(x => x.IdPlantillaEncuesta == oEVD.IdPlantillaEncuesta).ToList();
					for (int i = 0; i < LstPregunta.Count; i++)
					{
						LstComentarios.Add("");
						int IdPregunta = LstPregunta[i].IdPregunta;
						if (ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
						{
							LstDetalleNiveles.AddRange(ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList());
						}
					}

					var oidsmpam = ctx.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVD.IdSubModalidadPeriodoAcademicoModulo);

					var idsCurso = (from c in ctx.context.ComentarioDelegado
									where c.IdAlumno == IdAlumno
									select c.IdCurso).ToList();

					LstCurso = (from a in ListCursosSeccionEsDelegado
								select new SelectListItem { Text = a.CodigoCurso + " - " + a.NombreEspanolCurso, Value = a.IdCurso.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();
						 
						      
					if (LstCurso.Count > 0)
					{
						int pcurso = Convert.ToInt32(LstCurso[0].Value);

						var idsDocente = (from c in ctx.context.ComentarioDelegado
										  where c.IdAlumno == IdAlumno
										  select c.IdDocente).ToList();

						LstDocente = (from a in ListCursosSeccionEsDelegado
									select new SelectListItem { Text = a.CodigoDocente + " - " + a.NombreDocente + " " + a.ApellidoDocente, Value = a.IdDocente.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();
					}

				}
				else
				{
					var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x=>x.IdEncuestaVirtualDelegado==ctx.session.GetEncuestaVirtualDelegadoId());

					LstPregunta = ctx.context.Pregunta.Where(x => x.IdPlantillaEncuesta == oEVD.IdPlantillaEncuesta).ToList();
					for (int i = 0; i < LstPregunta.Count; i++)
					{
						LstComentarios.Add("");
						int IdPregunta = LstPregunta[i].IdPregunta;
						if (ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
						{
							LstDetalleNiveles.AddRange(ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList());
						}
					}

					var oidsmpam = ctx.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVD.IdSubModalidadPeriodoAcademicoModulo);

					var idsCurso = (from c in ctx.context.ComentarioDelegado
									where c.IdAlumno == IdAlumno
									select c.IdCurso).ToList();

					LstCurso = (from a in ctx.context.CursoPeriodoAcademico
								join b in ctx.context.SeccionCurso on a.IdCurso equals b.IdCurso
								join c in ctx.context.Seccion on b.IdSeccion equals c.IdSeccion
								join d in ctx.context.Curso on b.IdCurso equals d.IdCurso
								join e in ctx.context.CursoMallaCurricular on d.IdCurso equals e.IdCurso
								where a.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && c.IdModulo == oidsmpam.IdModulo && e.EsFormacion == false && !idsCurso.Contains(d.IdCurso)
								select new SelectListItem { Text = d.Codigo + " - " + d.NombreEspanol, Value = d.IdCurso.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();


					if (LstCurso.Count > 0)
					{
						int pcurso = Convert.ToInt32(LstCurso[0].Value);

						var idsDocente = (from c in ctx.context.ComentarioDelegado
										  where c.IdAlumno == IdAlumno
										  select c.IdDocente).ToList();

						LstDocente = (from a in ctx.context.DocenteSeccion
									  join b in ctx.context.Seccion on a.IdSeccion equals b.IdSeccion
									  join c in ctx.context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
									  join d in ctx.context.Curso on c.IdCurso equals d.IdCurso
									  join e in ctx.context.Docente on a.IdDocente equals e.IdDocente
									  where (c.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && b.IdModulo == oidsmpam.IdModulo && d.IdCurso == pcurso && !idsDocente.Contains(e.IdDocente))
									  select new SelectListItem { Text = e.Codigo + " - " + e.Nombres + " " + e.Apellidos, Value = e.IdDocente.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();
					}

				}
			}
			else
			{
				IdEncuestaToken = encuestaToken.IdEncuestaToken;

				IdSubModalidadPeriodoAcademico = encuestaToken.IdSubModalidadPeriodoAcademico;

				IdCarrera = encuestaToken.IdCarrera;

				if (IdEncuestaVirtualDelegado.HasValue)
				{
					var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == IdEncuestaVirtualDelegado);

					LstPregunta = ctx.context.Pregunta.Where(x => x.IdPlantillaEncuesta == oEVD.IdPlantillaEncuesta).ToList();
					for (int i = 0; i < LstPregunta.Count; i++)
					{
						LstComentarios.Add("");
						int IdPregunta = LstPregunta[i].IdPregunta;
						if (ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
						{
							LstDetalleNiveles.AddRange(ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList());
						}
					}

					var oidsmpam = ctx.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVD.IdSubModalidadPeriodoAcademicoModulo);

					var idsCurso = (from c in ctx.context.ComentarioDelegado
									where c.IdAlumno == IdAlumno
									select c.IdCurso).ToList();

					LstCurso = (from a in ctx.context.CursoPeriodoAcademico
								join b in ctx.context.SeccionCurso on a.IdCurso equals b.IdCurso
								join c in ctx.context.Seccion on b.IdSeccion equals c.IdSeccion
								join d in ctx.context.Curso on b.IdCurso equals d.IdCurso
								join e in ctx.context.CursoMallaCurricular on d.IdCurso equals e.IdCurso
								where a.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && c.IdModulo == oidsmpam.IdModulo && e.EsFormacion == false && !idsCurso.Contains(d.IdCurso)
								select new SelectListItem { Text = d.Codigo + " - " + d.NombreEspanol, Value = d.IdCurso.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();

					if (LstCurso.Count > 0)
					{
						int pcurso = Convert.ToInt32(LstCurso[0].Value);

						var idsDocente = (from c in ctx.context.ComentarioDelegado
										  where c.IdAlumno == IdAlumno
										  select c.IdDocente).ToList();

						LstDocente = (from a in ctx.context.DocenteSeccion
									  join b in ctx.context.Seccion on a.IdSeccion equals b.IdSeccion
									  join c in ctx.context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
									  join d in ctx.context.Curso on c.IdCurso equals d.IdCurso
									  join e in ctx.context.Docente on a.IdDocente equals e.IdDocente
									  where (c.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && b.IdModulo == oidsmpam.IdModulo && d.IdCurso == pcurso) && !idsDocente.Contains(e.IdDocente)
									  select new SelectListItem { Text = e.Codigo + " - " + e.Nombres + " " + e.Apellidos, Value = e.IdDocente.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();
					}

				}
				else
				{
					var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == ctx.session.GetEncuestaVirtualDelegadoId());

					LstPregunta = ctx.context.Pregunta.Where(x => x.IdPlantillaEncuesta == oEVD.IdPlantillaEncuesta).ToList();
					for (int i = 0; i < LstPregunta.Count; i++)
					{
						LstComentarios.Add("");
						int IdPregunta = LstPregunta[i].IdPregunta;
						if (ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
						{
							LstDetalleNiveles.AddRange(ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList());
						}
					}

					var oidsmpam = ctx.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == oEVD.IdSubModalidadPeriodoAcademicoModulo);

					var idsCurso = (from c in ctx.context.ComentarioDelegado
									where c.IdAlumno == IdAlumno
									select c.IdCurso).ToList();

					LstCurso = (from a in ctx.context.CursoPeriodoAcademico
								join b in ctx.context.SeccionCurso on a.IdCurso equals b.IdCurso
								join c in ctx.context.Seccion on b.IdSeccion equals c.IdSeccion
								join d in ctx.context.Curso on b.IdCurso equals d.IdCurso
								join e in ctx.context.CursoMallaCurricular on d.IdCurso equals e.IdCurso
								where a.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && c.IdModulo == oidsmpam.IdModulo && e.EsFormacion == false && !idsCurso.Contains(d.IdCurso)
								select new SelectListItem { Text = d.Codigo + " - " + d.NombreEspanol, Value = d.IdCurso.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();


					if (LstCurso.Count > 0)
					{
						int pcurso = Convert.ToInt32(LstCurso[0].Value);

						var idsDocente = (from c in ctx.context.ComentarioDelegado
										  where c.IdAlumno == IdAlumno
										  select c.IdDocente).ToList();

						LstDocente = (from a in ctx.context.DocenteSeccion
									  join b in ctx.context.Seccion on a.IdSeccion equals b.IdSeccion
									  join c in ctx.context.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
									  join d in ctx.context.Curso on c.IdCurso equals d.IdCurso
									  join e in ctx.context.Docente on a.IdDocente equals e.IdDocente
									  where (c.IdSubModalidadPeriodoAcademico == oidsmpam.IdSubModalidadPeriodoAcademico && b.IdModulo == oidsmpam.IdModulo && d.IdCurso == pcurso && !idsDocente.Contains(e.IdDocente))
									  select new SelectListItem { Text = e.Codigo + " - " + e.Nombres + " " + e.Apellidos, Value = e.IdDocente.ToString() }).Distinct().OrderByDescending(x => x.Value).ToList();
					}

				}
			}

			    

		}
       
    }
}