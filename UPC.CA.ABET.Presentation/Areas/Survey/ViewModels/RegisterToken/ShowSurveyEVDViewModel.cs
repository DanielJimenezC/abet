﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.RegisterToken
{
	public class ShowSurveyEVDViewModel
	{
		public List<Pregunta> LstPregunta { get; set; }
		public List<string> LstComentarios { get; set; }
		public List<DetalleNiveles> LstDetalleNiveles { get; set; }
		public List<SelectListItem> LstCurso { get; set; }
		public Int32? IdCurso { get; set; }
		public List<SelectListItem> LstDocente { get; set; }
		public Int32? IdDocente { get; set; }
		public String Ciclo { get; set; }
		public String Modulo { get; set; }
		public String Carrera { get; set; }
		public String Codigo { get; set; }

		public ShowSurveyEVDViewModel()
		{
			LstPregunta = new List<Pregunta>();
			LstComentarios = new List<string>();
			LstDetalleNiveles = new List<DetalleNiveles>();
			LstDocente = new List<SelectListItem>();
			LstCurso = new List<SelectListItem>();
	}

		public void Fill(CargarDatosContext ctx, Int32? _IdEncuestaVirtualDelegado)
		{
			var oEVD = ctx.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == _IdEncuestaVirtualDelegado);

            Codigo = ctx.context.Usp_GetCodigoNomenclatura(_IdEncuestaVirtualDelegado).FirstOrDefault().ToString();

            LstPregunta = ctx.context.Pregunta.Where(x => x.IdPlantillaEncuesta == oEVD.IdPlantillaEncuesta).ToList();

            for (int i = 0; i < LstPregunta.Count; i++)
			{
				LstComentarios.Add("");

				int IdPregunta = LstPregunta[i].IdPregunta;

				if (ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).FirstOrDefault() != null)
				{
					LstDetalleNiveles.AddRange(ctx.context.DetalleNiveles.Where(x => x.IdPregunta == IdPregunta).ToList());
				}
			}
		}
	}
}