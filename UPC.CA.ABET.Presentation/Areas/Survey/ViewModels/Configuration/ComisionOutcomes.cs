﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class ComisionOutcomes
    {
        public Int32 IdComision { get; set; }
        public Comision comision { get; set; }
        public List<SelectListItem> LstOutcomes { get; set; }


        public ComisionOutcomes()
        {
            LstOutcomes = new List<SelectListItem>();
        }


    }
}