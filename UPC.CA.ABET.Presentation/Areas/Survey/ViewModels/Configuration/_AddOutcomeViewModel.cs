﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class _AddOutcomeViewModel
    {

        //public Int32 CicloId { get; set; }
        //public Int32 SubModalidadPeriodoAcademicoModuloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public Int32 TipoEncuestaId { get; set; }
        public Int32 IdTipoOutcome { get; set; }
        public String TipoOutcomeNombre { get; set; }
        public String Periodo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")] 
        public Int32? IdComision { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 Orden { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionEs { get; set; }
        public String DescripcionIn { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String NombreEs { get; set; }        
        public String NombreIn { get; set; }
        public Int32? IdOutcomeEncuestaConfig { get; set; }
        public String TipoEncuestaNombre { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public List<Comision> LstComision { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Boolean EsVisible { get; set; }
        public _AddOutcomeViewModel()
        {
            LstCarrera = new List<Carrera>();
        }
        //public void Fill(CargarDatosContext dataContext, Int32? idOutcomeEncuestaConfig, Int32 cicloId, Int32 tipoEncuestaId, String tipoOutcome, Int32? carreraId)
        public void Fill(CargarDatosContext dataContext, Int32? idOutcomeEncuestaConfig, Int32 subModalidadPeriodoAcademicoId, Int32 tipoEncuestaId, String tipoOutcome, Int32? carreraId)
        {
            IdCarrera = carreraId;
            var escuelaId = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            IdOutcomeEncuestaConfig = idOutcomeEncuestaConfig;
            //CicloId = cicloId;
            SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            //SubModalidadPeriodoAcademicoModuloId = subModalidadPeriodoAcademicoModuloId;
            //Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == CicloId).CicloAcademico;
            Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(y=>y.IdSubModalidadPeriodoAcademico== subModalidadPeriodoAcademicoId).IdPeriodoAcademico).CicloAcademico;
            TipoEncuestaId = tipoEncuestaId;
            TipoEncuestaNombre = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.IdTipoEncuesta == tipoEncuestaId).Acronimo;
            IdTipoOutcome = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).IdTipoOutcomeEncuesta;
            //Orden = dataContext.context.OutcomeEncuestaConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.TipoOutcomeEncuesta.NombreEspanol == tipoOutcome && x.IdTipoEncuesta == tipoEncuestaId && x.IdPeriodoAcademico == CicloId && x.IdCarrera == IdCarrera).Count() + 1;
            Orden = dataContext.context.OutcomeEncuestaConfig.Where(x =>x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.TipoOutcomeEncuesta.NombreEspanol == tipoOutcome && x.IdTipoEncuesta == tipoEncuestaId && x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId && x.IdCarrera == IdCarrera).Count() + 1;
            //LstComision = dataContext.context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdPeriodoAcademico == CicloId).ToList();
            LstComision = dataContext.context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId).ToList();


            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            if (language == ConstantHelpers.CULTURE.ESPANOL)
                TipoOutcomeNombre = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).NombreEspanol;
            else
                TipoOutcomeNombre = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).NombreIngles;

            if (IdOutcomeEncuestaConfig.HasValue)
            {
                var outcome = dataContext.context.OutcomeEncuestaConfig.FirstOrDefault(x => x.IdOutcomeEncuestaConfig == IdOutcomeEncuestaConfig);

                NombreEs = outcome.NombreEspanol;
                NombreIn = outcome.NombreIngles;
                DescripcionEs = outcome.DescripcionEspanol;
                DescripcionIn = outcome.DescripcionIngles;
                Orden = outcome.Orden;
                EsVisible = outcome.EsVisible ?? false;
                IdCarrera = outcome.IdCarrera;
                IdComision = outcome.IdComision;
            }
        }
    }
}