﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class _EditarNivelesAceptacionPPPViewModel
    {
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public _EditarNivelesAceptacionPPPViewModel()
        {
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();
        }

        //public void Fill(CargarDatosContext dataContext, Int32? cicloId)
        public void Fill(CargarDatosContext dataContext, Int32? cicloId)
        {
            //LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == cicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == cicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();



        }

    }
}