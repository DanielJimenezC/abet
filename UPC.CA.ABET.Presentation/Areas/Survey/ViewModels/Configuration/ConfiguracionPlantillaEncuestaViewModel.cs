﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
	public class ConfiguracionPlantillaEncuestaViewModel
	{
		public IPagedList<PlantillaEncuesta> LstPlantillaEncuesta { get; set; }
		public Int32? PlantillaEncuestaId { get; set; }
		public Int32? NumeroPagina { get; set; }

		public ConfiguracionPlantillaEncuestaViewModel()
		{
			
		}

		public void Fill(CargarDatosContext ctx,Int32? numeroPagina)
		{
			var Lst = ctx.context.PlantillaEncuesta.ToList();
			NumeroPagina = numeroPagina ?? 1;
			LstPlantillaEncuesta = Lst.OrderByDescending(x => x.IdPlantillaEncuesta).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

		}
	}
}