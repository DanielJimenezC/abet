﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
	public class ConfiguracionPreguntasPlantillaEncuestaViewModel
	{
		public Int32 PlantillaEncuestaId { get; set; }
		public List<Pregunta> LstPregunta { get; set; }

		public ConfiguracionPreguntasPlantillaEncuestaViewModel()
		{
			LstPregunta = LstPregunta;
		}

		public void Fill(CargarDatosContext dataContext, Int32 _PlantillaEncuestaId)
		{
			PlantillaEncuestaId = _PlantillaEncuestaId;

            var LstPreguntasaBorrar = dataContext.context.Pregunta.Where(x => x.DescripcionEspanol == "PREGUNTA_CREADA_AUTOMATICAMENTE_AL_ELEGIR_ESCALA").ToList();

            foreach (var pab in LstPreguntasaBorrar)
            {
                var detalleniveles = dataContext.context.DetalleNiveles.Where(x => x.IdPregunta == pab.IdPregunta).ToList();

                foreach (var dn in detalleniveles)
                {
                    dataContext.context.DetalleNiveles.Remove(dn);

                }
                dataContext.context.Pregunta.Remove(pab);
            }

            dataContext.context.SaveChanges();

            LstPregunta = dataContext.context.Pregunta.Where(x => x.IdPlantillaEncuesta == PlantillaEncuestaId).ToList();
            
		}

	}
}