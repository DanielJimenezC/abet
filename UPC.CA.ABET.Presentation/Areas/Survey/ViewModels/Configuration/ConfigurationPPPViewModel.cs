﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class ConfigurationPPPViewModel
    {
        public List<OutcomeEncuestaConfig> LstConfig { get; set; }
        //public List<PeriodoAcademico> LstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        //public Int32? CicloId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public ConfigurationPPPViewModel()
        {
            //LstCiclo = new List<PeriodoAcademico>();
            LstSubModalidadPeriodoAcademico = new List<SubModalidadPeriodoAcademico>();
            LstCarrera = new List<Carrera>();
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();
            LstConfig = new List<OutcomeEncuestaConfig>();
        }
        //public void Fill(CargarDatosContext dataContext, Int32? cicloId)
        public void Fill(CargarDatosContext dataContext, Int32? subModalidadPeriodoAcademicoId)
        {
            //if (!cicloId.HasValue)
            if (!subModalidadPeriodoAcademicoId.HasValue)
            {
                //CicloId = dataContext.session.GetPeriodoAcademicoId();
                SubModalidadPeriodoAcademicoId = dataContext.session.GetSubModalidadPeriodoAcademicoId();
            }
            else
            {
                //CicloId = cicloId;
                SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            }

            var escuelaId = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            LstConfig = dataContext.context.OutcomeEncuestaConfig.Where(x =>
                x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO &&
                x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
               // && x.IdPeriodoAcademico == CicloId && x.EsVisible == true).OrderBy(x => x.Orden).ToList();
                && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.EsVisible == true).OrderBy(x => x.Orden).ToList();

            //LstCiclo = dataContext.context.PeriodoAcademico.ToList();
            LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            //LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == CicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;
        }
    }
}