﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Helpers;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;



namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
	public class ConfiguracionInvitadoViewModel
	{
   
        public int IdModulo { get; set; }
        public List<AlumnoInvitado> ListaAlumnosInvitados { get; set; }
        private AbetEntities context { get; set; }
        public IEnumerable<ComboItem> modulo;
        public List<SelectListItem> LstModulo { get; set; }

        public ConfiguracionInvitadoViewModel()
        {
           ListaAlumnosInvitados = new List<AlumnoInvitado>();
        }


        public void CargarDatos(CargarDatosContext DataContext, int? IdSubmoIdSubModalidadPeriodoAcademico, int? IdModulo)
        {

            int modalidadid = DataContext.session.GetModalidadId();
            int parIdSubModa = 0;
            var modalidad = DataContext.context.Modalidad.Where(x => x.IdModalidad == modalidadid).FirstOrDefault();

            if (!IdSubmoIdSubModalidadPeriodoAcademico.HasValue)
            {
                parIdSubModa = DataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == modalidadid && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                parIdSubModa = IdSubmoIdSubModalidadPeriodoAcademico.ToInteger();
            }

            if (IdModulo.HasValue)
            {
                this.IdModulo = DataContext.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == parIdSubModa).IdModulo;
            }
            else
            {
                this.IdModulo = IdModulo.ToInteger();
            }



            LstModulo = (from a in DataContext.context.SubModalidadPeriodoAcademicoModulo
                         join b in DataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                         join c in DataContext.context.Modulo on a.IdModulo equals c.IdModulo
                         where b.IdSubModalidadPeriodoAcademico == parIdSubModa
                         select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();

            ListaAlumnosInvitados = (from cu in DataContext.context.AlumnoInvitado
                                                          where cu.Tipo == "EVD" && cu.IdSubModalidadPeriodoAcademico == IdSubmoIdSubModalidadPeriodoAcademico && cu.IdModulo == this.IdModulo
                                     select cu).ToList();



        }
    }
}