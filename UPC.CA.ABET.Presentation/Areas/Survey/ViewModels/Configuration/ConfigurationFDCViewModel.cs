﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class ConfigurationFDCViewModel
    {
        //public List<PeriodoAcademico> LstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> LstSubModalidadPeriodoAcademico { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        //public Int32? CicloId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public ConfigurationFDCViewModel()
        {
            //LstCiclo = new List<PeriodoAcademico>();
            LstSubModalidadPeriodoAcademico = new List<SubModalidadPeriodoAcademico>();
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();
        }

        //public void Fill(CargarDatosContext dataContext, Int32? cicloId)
        public void Fill(CargarDatosContext dataContext, Int32? subModalidadPeriodoAcademicoId)
        {
            if (!subModalidadPeriodoAcademicoId.HasValue)
            {
                SubModalidadPeriodoAcademicoId = dataContext.session.GetSubModalidadPeriodoAcademicoId();
            }
            else
            {
                SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            }
            //var escuelaId = dataContext.session.GetEscuelaId();
            //LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();            
            //LstConfig = dataContext.context.OutcomeEncuestaConfig.Where(x =>
            //    x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO &&
            //    x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
            //    && x.IdPeriodoAcademico == CicloId).OrderBy(x => x.Orden).ToList();

            //LstCiclo = dataContext.context.PeriodoAcademico.ToList();
            LstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            //LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == CicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC).ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC).ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.FDC).IdTipoEncuesta;
        }
    }
}
