﻿using System;
using System.Linq;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Helpers;
using System.Web.Mvc;
using System.Collections.Generic;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class EditarDelegadoViewModel
    {
        public EVDDelegate delegadoActual { get; set; }
        public List<SelectListItem> LstAlumnos { get; set; }
        public String CodigoNuevoDelegado { get; set; }
        public String CodigoActualDelegado { get; set; }
        public int IdSeccion { get; set; }

        public EditarDelegadoViewModel()
        {
            delegadoActual = new EVDDelegate();
        }

        public void CargarDatos(String CodigoAlumno, CargarDatosContext ctx,int ModuloId)
        {

            int modalidadid = ctx.session.GetModalidadId();

            int parIdSubModa = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == modalidadid && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var delegado = (from a in ctx.context.AlumnoSeccion
                        join b in ctx.context.AlumnoMatriculado on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                        join c in ctx.context.Seccion on a.IdSeccion equals c.IdSeccion
                        join d in ctx.context.SeccionCurso on c.IdSeccion equals d.IdSeccion
                        join e in ctx.context.Alumno on b.IdAlumno equals e.IdAlumno
                        join f in ctx.context.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals f.IdCursoPeriodoAcademico
                        join g in ctx.context.Curso on f.IdCurso equals g.IdCurso
                        join h in ctx.context.DocenteSeccion on c.IdSeccion equals h.IdSeccion
                        join i in ctx.context.Docente on h.IdDocente equals i.IdDocente
                        join j in ctx.context.Modulo on c.IdModulo equals j.IdModulo
                        where a.EsDelegado == true && b.IdSubModalidadPeriodoAcademico == parIdSubModa && c.IdModulo == ModuloId && e.Codigo == CodigoAlumno
                        select new
                        {
                            Codigo_Alumno = e.Codigo,
                            Nombre_Alumno = e.Nombres + " " + e.Apellidos,
                            IdSeccion = c.IdSeccion,
                            Codigo_Seccion = c.Codigo,
                            Codigo_Curso = g.Codigo,
                            Nombre_Curso = g.NombreEspanol,
                            Codigo_Docente = i.Codigo,
                            Nombre_Docente = i.Nombres + " " + i.Apellidos,
                            ModuloId = j.IdModulo,
                            Modulo = j.NombreEspanol
                        }).FirstOrDefault();

            delegadoActual.Codigo_Alumno = delegado.Codigo_Alumno;
            delegadoActual.Nombre_Alumno = delegado.Nombre_Alumno;
            delegadoActual.Codigo_Seccion = delegado.Codigo_Seccion;
            delegadoActual.Codigo_Curso = delegado.Codigo_Curso;
            delegadoActual.Nombre_Curso = delegado.Nombre_Curso;
            delegadoActual.Codigo_Docente = delegado.Codigo_Docente;
            delegadoActual.Nombre_Docente = delegado.Nombre_Docente;

            this.CodigoActualDelegado = delegadoActual.Codigo_Alumno;
            this.IdSeccion = delegado.IdSeccion;

            LstAlumnos = (from asc in ctx.context.AlumnoSeccion
                          join am in ctx.context.AlumnoMatriculado on asc.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                          join s in ctx.context.Seccion on asc.IdSeccion equals s.IdSeccion
                          join sc in ctx.context.SeccionCurso on s.IdSeccion equals sc.IdSeccion
                          join al in ctx.context.Alumno on am.IdAlumno equals al.IdAlumno
                          join cpa in ctx.context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                          join cs in ctx.context.Curso on cpa.IdCurso equals cs.IdCurso
                          join ds in ctx.context.DocenteSeccion on s.IdSeccion equals ds.IdSeccion
                          join dc in ctx.context.Docente on ds.IdDocente equals dc.IdDocente
                          where asc.EsDelegado == false && am.IdSubModalidadPeriodoAcademico == parIdSubModa && s.Codigo == delegado.Codigo_Seccion && cs.Codigo == delegado.Codigo_Curso
                          select new SelectListItem { Text = al.Nombres + " " + al.Apellidos, Value = al.Codigo }).Distinct().ToList();

        }
    }
}