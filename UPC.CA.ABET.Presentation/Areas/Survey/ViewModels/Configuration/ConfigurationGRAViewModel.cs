﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class ConfigurationGRAViewModel
    {
        public List<OutcomeEncuestaConfig> LstConfig { get; set; }
        //public List<PeriodoAcademico> LstCiclo { get; set; }
        public IEnumerable<SelectListItem> LstSubModalidadPeriodoAcademico { get; set; }
        public List<Modalidad> LstModalidad { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        //public Int32? CicloId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public Int32 IdModalidad { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public ConfigurationGRAViewModel()
        {
            LstCarrera = new List<Carrera>();
            //LstCiclo = new List<PeriodoAcademico>();
            
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();
            LstConfig = new List<OutcomeEncuestaConfig>();
        }
        //public void Fill(CargarDatosContext dataContext, Int32? cicloId)
        public void Fill(CargarDatosContext dataContext, Int32? subModalidadPeriodoAcademicoId, Int32 parModalidadId)
        {
            if (!subModalidadPeriodoAcademicoId.HasValue)
            {
                //CicloId = dataContext.session.GetPeriodoAcademicoId();
                SubModalidadPeriodoAcademicoId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                //CicloId = cicloId;
                SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            }

            var escuelaId = dataContext.session.GetEscuelaId();
            // LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            LstCarrera = (from cpa in dataContext.context.CarreraPeriodoAcademico
                          join ca in dataContext.context.Carrera on cpa.IdCarrera equals ca.IdCarrera
                          where (ca.IdEscuela == escuelaId && cpa.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId)
                          select ca).ToList();


    
            LstConfig = dataContext.context.OutcomeEncuestaConfig.Where(x =>
                x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO &&
                x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
                && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.EsVisible == true).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
            //LstCiclo = dataContext.context.PeriodoAcademico.ToList();
            LstSubModalidadPeriodoAcademico = (from smpa in dataContext.context.SubModalidadPeriodoAcademico
                                               join pa in dataContext.context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                               join mo in dataContext.context.SubModalidad on smpa.IdSubModalidad equals mo.IdSubModalidad
                                               where  mo.IdModalidad == parModalidadId
                                               select new SelectListItem
                                               {
                                                   Value = smpa.IdSubModalidadPeriodoAcademico.ToString(),
                                                   Text = pa.CicloAcademico
                                               }).ToList();
            //LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == CicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).ToList();
            LstModalidad = dataContext.context.Modalidad.ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA).ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
            IdModalidad = dataContext.context.Modalidad.FirstOrDefault().IdModalidad;
        }
    }
}