﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class _AddPPPOutcomeViewModel
    {
        public Int32 EscuelaId { get; set; }
        public Int32 CicloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public Int32 IdTipoOutcome { get; set; }
        public String TipoOutcomeNombre { get; set; }
        public String Periodo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? IdCarrera { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 Orden { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String DescripcionEs { get; set; }
        public String DescripcionIn { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public String NombreEs { get; set; }
        public String NombreIn { get; set; }
        public Int32? IdOutcomeEncuestaPPPConfig { get; set; }
        public String TipoEncuestaNombre { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]

        public List<Carrera> LstCarrera { get; set; }

        public List<Outcome> LstOutcomes { get; set; }

        public List<SelectListItem> LstCustomOutcomes { get; set; }

        public List<Comision> LstComision { get; set; }

        public List<ComisionOutcomes> LstComisiones { get; set; }
        public List<int> LstOutcomesSeleccionados { get; set; }

        public int? OutcomeHallazgo1 { get; set; }
        public int? OutcomeHallazgo2 { get; set; }
        public int? OutcomeHallazgo3 { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Boolean EsVisible { get; set; }
        public Boolean OtraCarrera { get; set; }
        public _AddPPPOutcomeViewModel()
        {
            LstCarrera = new List<Carrera>();
            LstCustomOutcomes = new List<SelectListItem>();
            
            LstComisiones = new List<ComisionOutcomes>();
            LstOutcomesSeleccionados = new List<int>();

        }

        //public void Fill(CargarDatosContext dataContext, Int32? idOutcomeEncuestaPPPConfig, Int32 cicloId, String tipoOutcome, Int32 IdEscuela)
        public void Fill(CargarDatosContext dataContext, Int32? idOutcomeEncuestaPPPConfig, Int32 cicloId, String tipoOutcome, Int32 IdEscuela, Int32 parModalidadId)
        {

            CicloId = cicloId;
            int submodapa = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            EscuelaId = IdEscuela;

            LstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapaa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapaa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapaa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();

            if (idOutcomeEncuestaPPPConfig!=null)
            { 
                IdCarrera = dataContext.context.OutcomeEncuestaPPPConfig.First(x => x.IdOutcomeEncuestaPPPConfig == idOutcomeEncuestaPPPConfig).IdCarrera;
            }
            else
            {
                EsVisible = true;
            }

            IdOutcomeEncuestaPPPConfig = idOutcomeEncuestaPPPConfig;
            SubModalidadPeriodoAcademicoId = submodapa;
            //Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == CicloId).CicloAcademico;
            Periodo = dataContext.context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloId).FirstOrDefault().CicloAcademico;
            IdTipoOutcome = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).IdTipoOutcomeEncuesta;
            //Orden = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.TipoOutcomeEncuesta.NombreEspanol == tipoOutcome && x.IdPeriodoAcademico == CicloId && x.IdEscuela == EscuelaId).Count() + 1;
            Orden = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.TipoOutcomeEncuesta.NombreEspanol == tipoOutcome && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.IdEscuela == EscuelaId).Count() + 1;

          

            //NEW

            var query = (from cc in dataContext.context.CarreraComision
                         join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
                         join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
                         join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
                         join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
                         //join oepo in dataContext.context.OutcomeEncuestaPPPOutcome on o.IdOutcome equals oepo.IdOutcome
                         //join oepc in dataContext.context.OutcomeEncuestaPPPConfig on oepo.IdOutcomeEncuestaPPPConfig equals oepc.IdOutcomeEncuestaPPPConfig
                         orderby co.Codigo
                         //where (c.IdEscuela == EscuelaId && cc.IdPeriodoAcademico == CicloId && ((IdTipoOutcome == 1 && co.Codigo == "WASC") || (IdTipoOutcome == 2 && co.Codigo != "WASC")))
                         where (c.IdEscuela == EscuelaId && cc.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && ((IdTipoOutcome == 1 && co.Codigo == "WASC") || (IdTipoOutcome == 2 && co.Codigo != "WASC"))) 
                         select new { cc.IdComision, c.IdCarrera/*,*/ /*oepc.IdOutcomeEncuestaPPPConfig*/ });

            if (IdCarrera != null)
                query = query.Where(x => x.IdCarrera == IdCarrera);
            LstComisiones = query.Select(x => new ComisionOutcomes() { IdComision = x.IdComision }).Distinct().ToList();

            for (int i = 0; i < LstComisiones.Count; i++)
            {
                int IdComision = LstComisiones[i].IdComision;
                LstComisiones[i].comision = dataContext.context.Comision.FirstOrDefault(x => x.IdComision == IdComision);

                int idOutcome = (from cc in dataContext.context.CarreraComision
                                 join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
                                 join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
                                 join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
                                 join oepo in dataContext.context.OutcomeEncuestaPPPOutcome on oc.IdOutcome equals oepo.IdOutcome
                                 //where c.IdEscuela == EscuelaId && cc.IdPeriodoAcademico == CicloId && oepo.IdOutcomeEncuestaPPPConfig == IdOutcomeEncuestaPPPConfig && oc.IdComision == IdComision
                                 where c.IdEscuela == EscuelaId && cc.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && oepo.IdOutcomeEncuestaPPPConfig == IdOutcomeEncuestaPPPConfig && oc.IdComision == IdComision
                                 select oepo.IdOutcome).FirstOrDefault();

                LstComisiones[i].LstOutcomes = (from cc in dataContext.context.CarreraComision
                                                join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
                                                join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
                                                join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
                                                //where (c.IdEscuela == EscuelaId && cc.IdPeriodoAcademico == CicloId && co.IdComision == IdComision)
                                                where (c.IdEscuela == EscuelaId && cc.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && co.IdComision == IdComision)
                                                orderby oc.Comision.Codigo
                                                select new SelectListItem
                                                {
                                                    Value = oc.IdOutcome.ToString(),
                                                    Text = oc.Comision.Codigo + " | " + oc.Outcome.Nombre,
                                                    Selected = oc.IdOutcome == idOutcome
                                                }).Distinct().ToList();
            }
            

            //LstCustomOutcomes = (from cc in dataContext.context.CarreraComision
            //                     join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
            //                     join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
            //                     join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
            //                     join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
            //                     where (c.IdEscuela == EscuelaId && cc.IdPeriodoAcademico == CicloId)
            //                     orderby oc.Comision.Codigo
            //                     select new SelectListItem
            //                     {
            //                         Value = oc.IdOutcome.ToString(),
            //                         Text = oc.Comision.Codigo + " | " + oc.Outcome.Nombre
            //                     }).Distinct().ToList();


            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            if (language == ConstantHelpers.CULTURE.ESPANOL)
                TipoOutcomeNombre = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).NombreEspanol;
            else
                TipoOutcomeNombre = dataContext.context.TipoOutcomeEncuesta.FirstOrDefault(x => x.NombreEspanol == tipoOutcome).NombreIngles;

            if (IdOutcomeEncuestaPPPConfig.HasValue)
            {
                var outcome = dataContext.context.OutcomeEncuestaPPPConfig.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == IdOutcomeEncuestaPPPConfig);

                NombreEs = outcome.NombreEspanol;
                NombreIn = outcome.NombreIngles;
                DescripcionEs = outcome.DescripcionEspanol;
                DescripcionIn = outcome.DescripcionIngles;
                Orden = outcome.Orden;
                if (outcome.IdCarrera.HasValue)
                    IdCarrera = outcome.IdCarrera;

                EsVisible = outcome.EsVisible ?? false;
                OtraCarrera = outcome.OtraCarrera;

                /* OutcomeEncuestaPPPOutcome HallazgoOutcome = dataContext.context.OutcomeEncuestaPPPOutcome.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == outcome.IdOutcomeEncuestaPPPConfig);

                 if (HallazgoOutcome != null)

                     OutcomeHallazgo1 = HallazgoOutcome.IdOutcome;


                 if (OutcomeHallazgo1.HasValue)
                 {

                     HallazgoOutcome = dataContext.context.OutcomeEncuestaPPPOutcome.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == outcome.IdOutcomeEncuestaPPPConfig && x.IdOutcome != OutcomeHallazgo1.Value);
                     if (HallazgoOutcome != null)
                         HallazgoOutcome = dataContext.context.OutcomeEncuestaPPPOutcome.FirstOrDefault(x => x.IdOutcomeEncuestaPPPConfig == outcome.IdOutcomeEncuestaPPPConfig && x.IdOutcome != OutcomeHallazgo1.Value && x.IdOutcome != OutcomeHallazgo2.Value);             
                     if(OutcomeHallazgo2.HasValue)
                         OutcomeHallazgo2 = HallazgoOutcome.IdOutcome;
                     if (HallazgoOutcome != null)
                         OutcomeHallazgo3 = HallazgoOutcome.IdOutcome;


                 }

     */

            }

        }
    }
}