﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;


namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class SurveyGRAConfigurationViewModel
    {
        public String Periodo { get; set; }
        //public Int32 CicloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public List<OutcomeEncuestaConfig> LstConfig { get; set; }
        public Int32 TipoEncuesta { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public SurveyGRAConfigurationViewModel()
        {
            LstConfig = new List<OutcomeEncuestaConfig>();
            LstCarrera = new List<Carrera>();
        }
        //public void Fill(CargarDatosContext dataContext,Int32 cicloId)
        public void Fill(CargarDatosContext dataContext, Int32 subModalidadPeriodoAcademicoId, Int32 parModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            //CicloId = cicloId;
            SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            //Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault( x => x.IdPeriodoAcademico == CicloId).CicloAcademico;
            Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId).IdPeriodoAcademico).CicloAcademico;
            LstConfig = dataContext.context.OutcomeEncuestaConfig.Where(x =>
             x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO &&
             x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA
             //&& x.IdPeriodoAcademico == CicloId).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
             && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).OrderBy(x => x.IdComision).ThenBy( x => x.Orden).ToList();

            LstCarrera = (from cpa in dataContext.context.CarreraPeriodoAcademico
                          join ca in dataContext.context.Carrera on cpa.IdCarrera equals ca.IdCarrera
                          where (ca.IdEscuela == escuelaId && cpa.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId)
                          select ca).ToList();


            TipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault( x => x.Acronimo == ConstantHelpers.ENCUESTA.GRA).IdTipoEncuesta;
        }
    }
}