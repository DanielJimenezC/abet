﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class _AttachOutcomeViewModel
    {
        public List<OutcomeComision> LstOutcome { get; set; }
        public Int32 IdOutcomeEncuestaConfig { get; set; }
        //public Int32 CicloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public String TipoEncuestaNombre { get; set; }
        public Int32? IdComision { get; set; }
        public List<Comision> LstComision { get; set; }
        public List<Int32> LstVinculos { get; set; }
        public _AttachOutcomeViewModel()
        {
            LstOutcome = new List<OutcomeComision>();
            LstComision = new List<Comision>();
            LstVinculos = new List<Int32>();
        }
        //public void Fill(CargarDatosContext dataContext, Int32 idOutcomeEncuestaConfig, Int32 cicloId, String tipoEncuestaNombre, Int32? idComision)
        public void Fill(CargarDatosContext dataContext, Int32 idOutcomeEncuestaConfig, Int32 subModalidadPeriodoAcademicoId, String tipoEncuestaNombre, Int32? idComision)
        {
            IdComision = idComision;
            IdOutcomeEncuestaConfig = idOutcomeEncuestaConfig;
            //CicloId = cicloId;
            SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            TipoEncuestaNombre = tipoEncuestaNombre;
            
            if(!IdComision.HasValue)
                LstOutcome = dataContext.context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).ToList();
            //LstOutcome = dataContext.context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdPeriodoAcademico == CicloId).ToList();
            else
               // LstOutcome = dataContext.context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdPeriodoAcademico == CicloId && x.IdComision == IdComision).ToList();
                LstOutcome = dataContext.context.OutcomeComision.Include(x => x.Outcome).Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.IdComision == IdComision).ToList();
            //LstComision = dataContext.context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdPeriodoAcademico == CicloId).ToList();
            LstComision = dataContext.context.Comision.Where(x => x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).ToList();
            LstVinculos = dataContext.context.DetalleOutcomeEncuestaConfig.Where(x => x.IdOutcomeEncuestaConfig == IdOutcomeEncuestaConfig).Select(x => x.IdOutcome).ToList();
        }
    }
}