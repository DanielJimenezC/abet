﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class NewSurveyPPPConfigurationViewModel
    {
        public String Periodo { get; set; }
        public Int32 CicloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public List<OutcomeEncuestaPPPConfig> LstConfig { get; set; }
        public Int32 TipoEncuesta { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public NewSurveyPPPConfigurationViewModel()
        {
            LstConfig = new List<OutcomeEncuestaPPPConfig>();
            LstCarrera = new List<Carrera>();
        }
        //public void Fill(CargarDatosContext dataContext, Int32 cicloId, Int32 escuelaId)
        public void Fill(CargarDatosContext dataContext, Int32 cicloId, Int32 idEscuela, Int32 parModalidadId)
        {

            CicloId = cicloId;

            SubModalidadPeriodoAcademicoId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico; 
   
            Int32 escuelaId = idEscuela;

            LstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId)
                          select ca).Distinct().ToList();



            Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == CicloId).CicloAcademico;

            LstConfig = dataContext.context.OutcomeEncuestaPPPConfig.Where(x => x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO         
            && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.IdEscuela == idEscuela).OrderBy(x => x.Orden).ToList();

            TipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;

        }
    }
}