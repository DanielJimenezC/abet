﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class SurveyPPPConfigurationViewModel
    {
        public String Periodo { get; set; }
        //public Int32 CicloId { get; set; }
        public Int32 SubModalidadPeriodoAcademicoId { get; set; }
        public List<OutcomeEncuestaConfig> LstConfig { get; set; }
        public Int32 TipoEncuesta { get; set; }
        public List<Carrera> LstCarrera { get; set; }
        public SurveyPPPConfigurationViewModel()
        {
            LstConfig = new List<OutcomeEncuestaConfig>();
            LstCarrera = new List<Carrera>();
        }
        //public void Fill(CargarDatosContext dataContext,Int32 cicloId)
        public void Fill(CargarDatosContext dataContext, Int32 subModalidadPeriodoAcademicoId)
        {
            //CicloId = cicloId;
            SubModalidadPeriodoAcademicoId = subModalidadPeriodoAcademicoId;
            var escuelaId = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            //Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == CicloId).CicloAcademico;
            Periodo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(y => y.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId).IdPeriodoAcademico).CicloAcademico;
            LstConfig = dataContext.context.OutcomeEncuestaConfig.Where(x =>
             x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO &&
             x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP
             //&& x.IdPeriodoAcademico == CicloId).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
             && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).ToList();
            TipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;
        }
    }
}
