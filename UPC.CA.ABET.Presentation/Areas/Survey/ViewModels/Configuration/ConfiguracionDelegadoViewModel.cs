﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Meeting.Models;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Helpers;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
	public class ConfiguracionDelegadoViewModel
	{

        //public List<SelectListItem> ListaAsistencia {get;set;}
        public List<EVDDelegate> ListaAlumnos { get; set; }
        public int IdModulo { get; set; }
        //public List<int> LstIdAlumno;
        //public List<CheckboxModel> Checkboxes { get; set; }
        private AbetEntities context { get; set; }
        public IEnumerable<ComboItem> modulo;
        public List<SelectListItem> LstModulo { get; set; }

        public ConfiguracionDelegadoViewModel()
        {
            //Checkboxes = new List<CheckboxModel>();
            //LstIdAlumno = new List<int>();
            ListaAlumnos = new List<EVDDelegate>();
        }


        public void CargarDatos(CargarDatosContext DataContext, int? IdSubmoIdSubModalidadPeriodoAcademico,int? IdModulo)
        {

            //List<AlumnoInvitado> auxListAlumnoInvitado = (from cu in DataContext.context.AlumnoInvitado
            //                                              select cu).ToList();

            //List<bool> auxlistPresent = new List<bool>();
            //List<int> auxListaIdAlumnos=new List<int>();
            int modalidadid = DataContext.session.GetModalidadId();
            int parIdSubModa = 0;
            var modalidad = DataContext.context.Modalidad.Where(x => x.IdModalidad == modalidadid).FirstOrDefault();

            if (!IdSubmoIdSubModalidadPeriodoAcademico.HasValue)
            {
                parIdSubModa = DataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == modalidadid && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                parIdSubModa = IdSubmoIdSubModalidadPeriodoAcademico.ToInteger();
            }

            if (IdModulo.HasValue)
			{
				this.IdModulo = DataContext.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == parIdSubModa).IdModulo;
			}
            else
            {
                this.IdModulo = IdModulo.ToInteger();
            }

            LstModulo = (from a in DataContext.context.SubModalidadPeriodoAcademicoModulo
                         join b in DataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                         join c in DataContext.context.Modulo on a.IdModulo equals c.IdModulo
                         where b.IdSubModalidadPeriodoAcademico == parIdSubModa
                         select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();


            var List = (from a in DataContext.context.AlumnoSeccion
                                         join b in DataContext.context.AlumnoMatriculado on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                                         join c in DataContext.context.Seccion on a.IdSeccion equals c.IdSeccion
                                         join d in DataContext.context.SeccionCurso on c.IdSeccion equals d.IdSeccion
                                         join e in DataContext.context.Alumno on b.IdAlumno equals e.IdAlumno
                                         join f in DataContext.context.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals f.IdCursoPeriodoAcademico
                                         join g in DataContext.context.Curso on f.IdCurso equals g.IdCurso
                                         join h in DataContext.context.DocenteSeccion on c.IdSeccion equals h.IdSeccion
                                         join i in DataContext.context.Docente on h.IdDocente equals i.IdDocente
                                         join j in DataContext.context.Modulo on c.IdModulo equals j.IdModulo
                                         where a.EsDelegado == true && b.IdSubModalidadPeriodoAcademico == parIdSubModa && c.IdModulo == this.IdModulo
						select new {
                                             Codigo_Alumno = e.Codigo,
                                             Nombre_Alumno = e.Nombres + " " + e.Apellidos,
                                             Codigo_Seccion = c.Codigo,
                                             Codigo_Curso = g.Codigo,
                                             Nombre_Curso = g.NombreEspanol,
                                             Codigo_Docente=i.Codigo,
                                             Nombre_Docente=i.Nombres + " " +i.Apellidos,
                                             ModuloId=j.IdModulo,
                                             Modulo =j.NombreEspanol
                                         } ).OrderBy(x=>x.Codigo_Alumno).ToList();

            foreach (var item in List)
            {
                EVDDelegate obj = new EVDDelegate();
                obj.Codigo_Alumno = item.Codigo_Alumno;
                obj.Nombre_Alumno = item.Nombre_Alumno;
                obj.Codigo_Seccion = item.Codigo_Seccion;
                obj.Codigo_Curso = item.Codigo_Curso;
                obj.Nombre_Curso = item.Nombre_Curso;
                obj.Codigo_Docente = item.Codigo_Docente;
                obj.Nombre_Docente = item.Nombre_Docente;
                obj.Modulo_Id = item.ModuloId;
                obj.Modulo = item.Modulo;

                ListaAlumnos.Add(obj);
            }

       
        }

    }
}