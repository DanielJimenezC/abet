﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class NewConfigurationPPPViewModel
    {
        public List<OutcomeEncuestaPPPConfig> LstConfig { get; set; }
        public List<PeriodoAcademico> LstCiclo { get; set; }
         public List<Carrera> LstCarrera { get; set; }
        public Int32? CicloId { get; set; }
        public Int32? SubModalidadPeriodoAcademicoId { get; set; }
        public List<NivelAceptacionEncuesta> LstNivelAceptacion { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public Int32 IdEscuela { get; set; }
        public NewConfigurationPPPViewModel()
        {
            LstCiclo = new List<PeriodoAcademico>();
            LstCarrera = new List<Carrera>();
            LstNivelAceptacion = new List<NivelAceptacionEncuesta>();
            LstConfig = new List<OutcomeEncuestaPPPConfig>();
        }
        public void Fill(CargarDatosContext dataContext, Int32? cicloId, Int32? idEscuela)
        {
       
            if (!cicloId.HasValue)
            {

                //CicloId = dataContext.session.GetPeriodoAcademicoId();
                CicloId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;
                SubModalidadPeriodoAcademicoId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                CicloId = cicloId;
                SubModalidadPeriodoAcademicoId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }

            IdEscuela = dataContext.session.GetEscuelaId();
            LstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == IdEscuela).ToList();
            LstConfig = dataContext.context.OutcomeEncuestaPPPConfig.Where(x =>
                x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO
                //&& x.IdPeriodoAcademico == CicloId && x.EsVisible == true && x.IdEscuela == escuelaId).OrderBy(x => x.Orden).ToList();
                && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.EsVisible == true && x.IdEscuela == idEscuela).OrderBy(x => x.Orden).ToList();
            LstCiclo = (from submodapa in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submodapa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submodapa.IdSubModalidad == 1)
                        select pa).ToList();

            //System.Diagnostics.Debug.WriteLine("cantidad ciclos: " + LstCiclo.Count);
            //LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == CicloId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            LstNivelAceptacion = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
            IdTipoEncuesta = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ENCUESTA.PPP).IdTipoEncuesta;
        }
    }
}