﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Configuration
{
    public class _EditarNivelesAceptacionViewModel
    {
        //public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdTipoEncuesta { get; set; }
        public List<NivelAceptacionEncuesta> LstNiveles { get; set; }
        public String TipoEncuestaNombre { get; set; }
        //public void Fill(CargarDatosContext dataContext, Int32 PeriodoAcademicoId, Int32 idTipoEncuesta)
        public void Fill(CargarDatosContext dataContext, Int32 idSubModalidadPeriodoAcademico, Int32 idTipoEncuesta)
        {
            //IdPeriodoAcademico = PeriodoAcademicoId;
            IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            IdTipoEncuesta = idTipoEncuesta;
            TipoEncuestaNombre = dataContext.context.TipoEncuesta.FirstOrDefault(x => x.IdTipoEncuesta == IdTipoEncuesta).Acronimo;
            // LstNiveles = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico && x.IdTipoEncuesta == IdTipoEncuesta).ToList();
            LstNiveles = dataContext.context.NivelAceptacionEncuesta.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && x.IdTipoEncuesta == IdTipoEncuesta).ToList();
        }
    }
}
