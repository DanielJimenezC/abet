﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Generate
{
    public class GenerateSurveyPPPViewModel
    {
        public String Tipo { get; set; }
        public Int32 sedeId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32? cicloId { get; set; }
        public Int32? SubModalidadId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 carreraId { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public List<Sede> lstSede { get; set; }
        public void Fill(CargarDatosContext dataContext)
        {
            this.Tipo = ConstantHelpers.ENCUESTA.PPP;


            lstCiclo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submoda.IdSubModalidad == 1)
                        select pa).ToList();

            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId && x.Codigo!="IS").ToList();
         
            lstCurso = dataContext.context.Curso.ToList();
            lstSeccion = dataContext.context.Seccion.ToList();
            lstSede = dataContext.context.Sede.ToList();

            if (cicloId != null)
            {
                subModalidadPeriodoAcademicoId = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            //carreraId = sedeId = cicloId = 0;
        }
    }
}