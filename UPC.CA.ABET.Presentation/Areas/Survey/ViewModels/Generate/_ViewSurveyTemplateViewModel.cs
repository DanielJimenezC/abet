﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Generate
{
    public class _ViewSurveyTemplateViewModel
    {
        public Int32 TemplateEncuestaId { get; set; }
        public String Desing { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32 TemplateEncuestaId)
        {
            this.TemplateEncuestaId = TemplateEncuestaId;
            var template = dataContext.context.TemplateEncuesta.FirstOrDefault(x => x.IdTemplateEncuesta == this.TemplateEncuestaId);
        }
    }
}