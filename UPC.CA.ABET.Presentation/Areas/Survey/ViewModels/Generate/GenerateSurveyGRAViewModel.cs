﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using System.ComponentModel.DataAnnotations;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Generate
{
    public class GenerateSurveyGRAViewModel
    {
        public String Tipo { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 sedeId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        //public Int32? cicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Views.Validation.ValidationResource), ErrorMessageResourceName = "CamposRequeridosValidation")]
        public Int32 carreraId { get; set; }
        //public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public List<Sede> lstSede { get; set; }
        public void Fill(CargarDatosContext dataContext)
        {
            this.Tipo = ConstantHelpers.ENCUESTA.GRA;
            //lstCiclo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            lstSubMOdalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.OrderByDescending(x => x.IdPeriodoAcademico).ToList();
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            lstCurso = new List<Curso>();
            lstSeccion = new List<Seccion>();
            lstSede = dataContext.context.Sede.ToList();
            //carreraId = sedeId = cicloId = 0;
            subModalidadPeriodoAcademicoId=carreraId = sedeId = 0;
        }
    }
}

