﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email
{
    public class EmailSurveyEVDViewModel
    {
        public Int32 IdCarrera { get; set; }
        public Carrera CarreraCoordinador { get; set; }
        public Boolean ValidarCarrera { get; set; }
        public Int32 IdAlumno { get; set; }
        public Int32 IdCarreraAlumno { get; set; }
        public String NombreAlumno { get; set; }
        public String CodigoAlumno { get; set; }
        public String NombreCarrera { get; set; }
        public Int32? IdEncuestaToken { get; set; }
        public List<SelectListItem> lstCodigoAlumno { get; set; }
        public List<SelectListItem> lstNombresAlumno { get; set; }
        public ConfiguracionNotificacion objConfiguracionNotificacionDelegado { get; set; }
        public ConfiguracionNotificacion objConfiguracionNotificacionInvitado { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public String lstDatos { get; set; }
        public String lstDatosDelegados { get; set; }
        public String lstDatosInvitados { get; set; }
        public String CodigoEncuesta { get; set; }

        public void Fill(CargarDatosContext dataContext, int parModalidadId)
        {
            try
            {
                lstCodigoAlumno = new List<SelectListItem>();
                lstNombresAlumno = new List<SelectListItem>();
                int EscuelaId = dataContext.session.GetEscuelaId();
                int EncuestaVirtualDelegado = dataContext.context.EncuestaVirtualDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == EscuelaId).FirstOrDefault().IdEncuestaVirtualDelegado;


                CodigoEncuesta = dataContext.context.Usp_GetCodigoNomenclatura(EncuestaVirtualDelegado).FirstOrDefault().ToString();

                var IdCiclo = dataContext.session.GetPeriodoAcademicoId();
                var IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var IdEscuela = dataContext.session.GetEscuelaId();

                objConfiguracionNotificacionDelegado = dataContext.context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo == "EVDD");

                if (objConfiguracionNotificacionDelegado == null)
                    objConfiguracionNotificacionDelegado = new ConfiguracionNotificacion();

                objConfiguracionNotificacionInvitado = dataContext.context.ConfiguracionNotificacion.FirstOrDefault(x => x.IdEscuela == IdEscuela && x.Tipo == "EVDI");

                if (objConfiguracionNotificacionInvitado == null)
                    objConfiguracionNotificacionInvitado = new ConfiguracionNotificacion();

                var Roles = dataContext.session.GetRoles();

                if (Roles.HasRole(AppRol.CoordinadorCarrera) && !Roles.HasRole(AppRol.Administrador) && !Roles.HasRole(AppRol.DirectorCarrera))
                {
                    var ctx = dataContext.context;
                    IdCarrera = (from cpa in ctx.CarreraPeriodoAcademico
                                 join ua in ctx.UnidadAcademica on cpa.IdCarreraPeriodoAcademico equals ua.IdCarreraPeriodoAcademico
                                 join sua in ctx.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                                 join uar in ctx.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                                 //where ua.Nivel == 1 && uar.IdDocente == IdDocente && ua.IdPeriodoAcademico == IdCiclo
                                 where ua.Nivel == 1 && /*uar.IdDocente == IdDocente &&*/ ua.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                                 select cpa).FirstOrDefault().IdCarrera;
                    CarreraCoordinador = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == IdCarrera);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}