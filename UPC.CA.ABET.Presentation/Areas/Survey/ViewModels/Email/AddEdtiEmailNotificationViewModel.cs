﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email
{
    public class AddEdtiEmailNotificationViewModel
    {
        public String Asunto { get; set; }
        public String Mensaje { get; set; }
        
        public AddEdtiEmailNotificationViewModel()
        {
        }

        public void Fill(CargarDatosContext datacontext)
        {
            try
            {
                var PlantillaEmail = datacontext.context.Plantilla.FirstOrDefault();

                if(PlantillaEmail != null)
                {
                    this.Mensaje = PlantillaEmail.Mensaje;
                    this.Asunto = PlantillaEmail.Asunto;
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            
        }
    }
}