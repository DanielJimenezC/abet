﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email
{
    public class ViewEmailNotificationViewModel
    {
        public Int32? IdEmail { get; set; }
        public String Asunto { get; set; }
        public String Mensaje { get; set; }
        public String Estado { get; set; }
        public DateTime FechaEnvio { get; set; }

        public ViewEmailNotificationViewModel()
        {
        }

        public void Fill(CargarDatosContext datacontext, Int32? IdEmail)
        {
            if(IdEmail.HasValue)
            {
                var Email = datacontext.context.Email.FirstOrDefault(x => x.IdEmail == IdEmail);
                this.Asunto = Email.Asunto;
                this.Mensaje = Email.Mensaje;
                this.FechaEnvio = Email.FechaEnvio;
                this.Estado = ConstantHelpers.GetEstadoEmail(Email.Estado);
            }
        }
    }
}