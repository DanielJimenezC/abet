﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using PagedList;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email
{
    public class EmailNotificationViewModel
    {
        public List<UPC.CA.ABET.Models.Email> lstEmail { get; set; }
        public Int32? NumeroPagina { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdCiclo { get; set; }
        public Int32? IdSubModalidadPeriodoAcademico { get; set; }
        public Int32? IdSede { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubModalidadPeriodoAcademico { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Sede> lstSede { get; set; }

        public EmailNotificationViewModel()
        {
        }

        public void Fill(CargarDatosContext dataContext, int parModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();

            lstCiclo = (from submodapa in dataContext.context.SubModalidadPeriodoAcademico
                        join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                        join pa in dataContext.context.PeriodoAcademico on submodapa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        select pa

                ).ToList();
      
            lstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            lstEmail = dataContext.context.Email.OrderBy(x => x.IdEmail).ToList();
            lstSede = dataContext.context.Sede.ToList();
        }

        //public void FindMail(CargarDatosContext dataContext, Int32? IdCarrera, Int32? IdSede, Int32? IdCiclo)
        public void FindMail(CargarDatosContext dataContext, Int32? IdCarrera, Int32? IdSede, Int32? IdSubModalidadPeriodoAcademico)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            //lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.ToList();
            lstSede = dataContext.context.Sede.ToList();

            var query = dataContext.context.Email.ToList();

            if (IdCarrera.HasValue)
            {
                query = query.Where(x => x.AlumnoSeccion.AlumnoMatriculado.IdCarrera == IdCarrera).ToList();
            }

            if (IdSede.HasValue)
            {
                query = query.Where(x => x.AlumnoSeccion.AlumnoMatriculado.IdSede == IdSede).ToList();
            }
            //if (IdCiclo.HasValue)
            if (IdSubModalidadPeriodoAcademico.HasValue)
            {
                //query = query.Where(x => x.IdPeriodoAcademico == IdCiclo).ToList();
                query = query.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).ToList();
            }

            lstEmail = query.OrderBy(x => x.IdEmail).ToList();
        }
    }
}