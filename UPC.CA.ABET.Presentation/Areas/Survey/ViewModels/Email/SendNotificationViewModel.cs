﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email
{
    public class SendNotificationViewModel
    {
        public Int32? IdSeccion { get; set; }
        public List<Seccion> lstSeccion { get; set; }

        public SendNotificationViewModel()
        {
        }

        public void Fill(CargarDatosContext datacontext)
        {
            try
            {
                lstSeccion = datacontext.context.Seccion.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}