﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class ListStudentNotificacionsEVDInvitadosViewModel
    {
        public List<StudenNotificationEVD> LstNotificacionAlumnosInvitados { get; set; }
        public String lstDatosInvitados { get; set; }

        public ListStudentNotificacionsEVDInvitadosViewModel()
        {

            LstNotificacionAlumnosInvitados = new List<StudenNotificationEVD>();
        }

        public void Fill(CargarDatosContext ctx)
        {
            try
            {
                int EscuelaId = ctx.session.GetEscuelaId();
                var oEVD = ctx.context.EncuestaVirtualDelegado.Where(x => x.Estado == "ACT" && x.IdEscuela == EscuelaId).FirstOrDefault();
                var IdEscuela = ctx.session.GetEscuelaId();
                var Roles = ctx.session.GetRoles();
                var ListaAlumnosInvitados = (from cu in ctx.context.AlumnoInvitado
                                             where cu.Tipo == "EVD" && cu.IdSubModalidadPeriodoAcademico == oEVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico && cu.IdModulo == oEVD.SubModalidadPeriodoAcademicoModulo.IdModulo
                                             select cu.codigo).ToList();

                var ListAlumnoReal = (from a in ctx.context.Alumno
                                      where ListaAlumnosInvitados.Contains(a.Codigo)
                                      select a.IdAlumno).ToList().Distinct();

                var ListNotificacionInvitados = (from a in ctx.context.NotificacionEncuestaAlumno
                                                 where ListAlumnoReal.Contains(a.IdAlumno) && a.IdEncuestaVirtualDelegado == oEVD.IdEncuestaVirtualDelegado
                                                 select a).ToList();



                if (Roles.HasRole(AppRol.Administrador) || Roles.HasRole(AppRol.DirectorCarrera) || Roles.HasRole(AppRol.CoordinadorCarrera))

                    foreach (var oNoti in ListNotificacionInvitados)
                    {
                        StudenNotificationEVD obj = new StudenNotificationEVD();
                        obj.IdAlumno = oNoti.IdAlumno.ToInteger();
                        obj.IdCarrera = oNoti.IdCarrera.ToInteger();
                        obj.Codigo = oNoti.Alumno.Codigo;


                        obj.Correo = GetCorreo(oNoti.Alumno.Codigo);
                        obj.NombreCompleto = oNoti.Alumno.Nombres + " , " + oNoti.Alumno.Apellidos;

                        var carrera = ctx.context.Carrera.Where(x => x.IdCarrera == oNoti.IdCarrera).FirstOrDefault();
                        obj.NombreCarreraEspanol = carrera.NombreEspanol;
                        obj.NombreCarreraIngles = carrera.NombreIngles;
                        obj.Estado = oNoti.IdEncuestaToken != null ? true : false;
                        if (oNoti.IdEncuestaToken.HasValue)
                        {
                            DateTime FechaEnvio = ctx.context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == oNoti.IdEncuestaToken).FechaEnvio;
                            obj.FechaEnvio = FechaEnvio.ToShortDateString();
                        }
                        else
                        {
                            obj.FechaEnvio = "-";
                        }
                        obj.IdNotificacion = oNoti.IdNotificacion;

                        LstNotificacionAlumnosInvitados.Add(obj);

                        if (!obj.Estado)
                        {
                            lstDatosInvitados += ";" + obj.IdAlumno + "|" + obj.IdCarrera + "|" + obj.Correo + "|" + obj.IdNotificacion;
                            if (lstDatosInvitados[0] == ';')
                                lstDatosInvitados = lstDatosInvitados.Replace(";", "");



                        }
                    }
                LstNotificacionAlumnosInvitados = LstNotificacionAlumnosInvitados.OrderBy(x => x.Estado).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetCorreo(string codigo)
        {
            string correo = "";

            int anio = Convert.ToInt32(codigo.Substring(0, 4));

            switch (codigo)
            {
                case "199710627":
                    correo = "a199710627@upc.edu.pe";
                    break;
                default:
                    {
                        if (anio < 2000)
                        {

                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "a" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2000 && anio < 2010)
                        {
                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "u" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2010)
                        {
                            correo = "u" + codigo + "@upc.edu.pe";
                        }
                    }
                    break;
            }
            return correo;
        }
    }
}