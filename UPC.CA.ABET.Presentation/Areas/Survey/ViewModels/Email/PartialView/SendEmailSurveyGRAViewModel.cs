﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class SendEmailSurveyGRAViewModel
    {
        public Int32 IdNotificacion { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 IdNotificacion)
        {
            this.IdNotificacion = IdNotificacion;
        }
    }
}