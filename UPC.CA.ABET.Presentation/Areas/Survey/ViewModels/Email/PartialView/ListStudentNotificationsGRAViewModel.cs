﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class ListStudentNotificationsGRAViewModel
    {
        public List<StudenNotificationGRA> lstAlumnosDatos { get; set; }
        public String lstDatos { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32? IdCarrera, int parModalidadId)
        {
            try
            {
                lstAlumnosDatos = new List<StudenNotificationGRA>();
                //var IdCiclo = dataContext.session.GetPeriodoAcademicoId();

                var IdSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                var IdEscuela = dataContext.session.GetEscuelaId();
                var Roles = dataContext.session.GetRoles();

                List<NotificacionEncuestaAlumno> lstAlumnos;

                if (Roles.HasRole(AppRol.Administrador) || Roles.HasRole(AppRol.DirectorCarrera) || !IdCarrera.HasValue)
                    lstAlumnos = dataContext.context.NotificacionEncuestaAlumno.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdEncuestaVirtualDelegado == null && dataContext.context.Carrera.FirstOrDefault(y => y.IdCarrera == x.IdCarrera).IdEscuela == IdEscuela).OrderBy(x => x.IdCarrera).ToList();

                else
                    lstAlumnos = dataContext.context.NotificacionEncuestaAlumno.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdCarrera == IdCarrera).ToList();

                foreach (NotificacionEncuestaAlumno item in lstAlumnos)
                {
                    StudenNotificationGRA oDatos = new StudenNotificationGRA();
                    oDatos.IdAlumno = item.IdAlumno;
                    oDatos.IdCarrera = item.IdCarrera;
                    oDatos.Codigo = item.Alumno.Codigo;
                    oDatos.Correo = GetCorreo(item.Alumno.Codigo);
                    oDatos.NombreCompleto = item.Alumno.Nombres + " " + item.Alumno.Apellidos;
                    oDatos.NombreCarreraEspanol = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == item.IdCarrera).NombreEspanol;
                    oDatos.NombreCarreraIngles = dataContext.context.Carrera.FirstOrDefault(x => x.IdCarrera == item.IdCarrera).NombreIngles;
                    oDatos.Estado = item.IdEncuestaToken != null ? true : false;
                    oDatos.FechaEnvio = (oDatos.Estado == true) ? item.EncuestaToken.FechaEnvio.ToShortDateString() : "-";
                    oDatos.IdNotificacion = item.IdNotificacion;

                    lstAlumnosDatos.Add(oDatos);

                    if (!oDatos.Estado)
                    {
                        lstDatos += ";" + oDatos.IdAlumno + "|" + oDatos.IdCarrera + "|" + oDatos.Correo;
                        if (lstDatos[0] == ';')
                            lstDatos = lstDatos.Replace(";", "");
                    }
                }
                lstAlumnosDatos = lstAlumnosDatos.OrderBy(x => x.Estado).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetCorreo(string codigo)
        {
            string correo = "";

            int anio = Convert.ToInt32(codigo.Substring(0, 4));

            switch (codigo)
            {
                case "199710627":
                    correo = "a199710627@upc.edu.pe";
                    break;
                default:
                    {
                        if (anio < 2000)
                        {

                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "a" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2000 && anio < 2010)
                        {
                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "u" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2010)
                        {
                            correo = "u" + codigo + "@upc.edu.pe";
                        }
                    }
                    break;
            }
            return correo;
        }
    }
}