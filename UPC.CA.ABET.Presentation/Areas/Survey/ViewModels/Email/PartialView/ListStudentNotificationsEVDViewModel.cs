﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class ListStudentNotificationsEVDViewModel
    {
        public List<StudenNotificationEVD> LstNotificacionAlumnosDelegados { get; set; }
        public String lstDatosDelegados { get; set; }


        public ListStudentNotificationsEVDViewModel()
        {
            LstNotificacionAlumnosDelegados = new List<StudenNotificationEVD>();

        }

        public void Fill(CargarDatosContext dataContext)
        {
            try
            {
                var IdEscuela = dataContext.session.GetEscuelaId();

                var Roles = dataContext.session.GetRoles();

                var oEVD = dataContext.context.EncuestaVirtualDelegado.Where(x => x.IdEscuela == IdEscuela && x.Estado == "ACT").FirstOrDefault();

                var LstNotificacionDelegadop = dataContext.context.Usp_ObtenerNotificacionDelegados(oEVD.IdCarrera, oEVD.IdEscuela).ToList();

                if (Roles.HasRole(AppRol.Administrador) || Roles.HasRole(AppRol.DirectorCarrera) || Roles.HasRole(AppRol.CoordinadorCarrera))

                    foreach (var oNoti in LstNotificacionDelegadop)
                    {
                        StudenNotificationEVD obj = new StudenNotificationEVD();
                        obj.IdAlumno = oNoti.IdAlumno;
                        obj.IdCarrera = oNoti.IdCarrera;
                        obj.Codigo = oNoti.CodigoAlumno;
                        obj.Correo = GetCorreo(oNoti.CodigoAlumno);
                        obj.NombreCompleto = oNoti.Nombres + " , " + oNoti.Apellidos;
                        obj.NombreCarreraEspanol = oNoti.NombreEspanolCarrera;
                        obj.NombreCarreraIngles = oNoti.NombreInglesCarrera;
                        obj.Estado = oNoti.IdEncuestaToken != null ? true : false;
                        if (oNoti.IdEncuestaToken.HasValue)
                        {
                            DateTime FechaEnvio = dataContext.context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == oNoti.IdEncuestaToken).FechaEnvio;
                            obj.FechaEnvio = FechaEnvio.ToShortDateString();
                        }
                        else
                        {
                            obj.FechaEnvio = "-";
                        }
                        obj.IdNotificacion = oNoti.IdNotificacion;


                        LstNotificacionAlumnosDelegados.Add(obj);

                        if (!obj.Estado)
                        {
                            lstDatosDelegados += ";" + obj.IdAlumno + "|" + obj.IdCarrera + "|" + obj.Correo + "|" + obj.IdNotificacion;
                            if (lstDatosDelegados[0] == ';')
                                lstDatosDelegados = lstDatosDelegados.Replace(";", "");
                        }
                    }
                LstNotificacionAlumnosDelegados = LstNotificacionAlumnosDelegados.OrderBy(x => x.Estado).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetCorreo(string codigo)
        {
            string correo = "";

            int anio = Convert.ToInt32(codigo.Substring(0, 4));

            switch (codigo)
            {
                case "199710627":
                    correo = "a199710627@upc.edu.pe";
                    break;
                default:
                    {
                        if (anio < 2000)
                        {

                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "a" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2000 && anio < 2010)
                        {
                            int ultimo = codigo.Count();
                            string codigofinal = codigo.Substring(3, ultimo - 3);
                            correo = "u" + codigofinal + "@upc.edu.pe";
                        }
                        if (anio >= 2010)
                        {
                            correo = "u" + codigo + "@upc.edu.pe";
                        }
                    }
                    break;
            }
            return correo;
        }
    }
}