﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class DeleteNotificationSurveyGRAViewModel
    {
        public Int32 IdNotificacion { get; set; }
        public Int32 IdEncuestaToken { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 IdNotificacion)
        {
            this.IdNotificacion = IdNotificacion;
            var objNotificacion = dataContext.context.NotificacionEncuestaAlumno.FirstOrDefault(x => x.IdNotificacion == IdNotificacion);
            this.IdEncuestaToken = dataContext.context.EncuestaToken.FirstOrDefault(x => x.IdAlumno == objNotificacion.IdAlumno &&
                                                                                        x.IdCarrera == objNotificacion.IdCarrera &&
                                                                                            //x.IdPeriodoAcademico == objNotificacion.IdPeriodoAcademico).IdEncuestaToken;
                                                                                            x.IdSubModalidadPeriodoAcademico == objNotificacion.IdSubModalidadPeriodoAcademico).IdEncuestaToken;
        }

    }
}