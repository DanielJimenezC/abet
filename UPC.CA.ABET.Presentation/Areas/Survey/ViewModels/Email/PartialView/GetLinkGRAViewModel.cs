﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email.PartialView
{
    public class GetLinkGRAViewModel
    {
        public Int32 IdAlumno { get; set; }
        public Int32 IdEncuestaToken { get; set; }
        public int escuelaId { get; set; }
        public string link { get; set; }

        public void Fill(CargarDatosContext dataContext, int parIdAlumno)
        {
            bool postlinkencontrado = false;
            string prelink;
            if (escuelaId == 1) { 
                prelink = "http://abetdesarrollo.upc.edu.pe/ABETSistemas/Survey/RegisterToken/AddSurveyGRAToken?Token=";
            }
            else
            {
                prelink = "http://abetdesarrollo.upc.edu.pe/ABETIngenierias/Survey/RegisterToken/AddSurveyGRAToken?Token=";

            }
            string postlink;
            IdAlumno = parIdAlumno;

            try
            {
                postlink = dataContext.context.EncuestaToken.Where(x => x.IdAlumno == parIdAlumno).FirstOrDefault().Token;
                postlinkencontrado = true;
            }
            catch (Exception e)
            {
                postlinkencontrado = false;
                throw;
            }

            if (postlinkencontrado == true)
            {
                link = prelink + postlink;
            }
            else
            {
                link = "El alumno no cuenta con un token.";
            }
            
        }
    }
}