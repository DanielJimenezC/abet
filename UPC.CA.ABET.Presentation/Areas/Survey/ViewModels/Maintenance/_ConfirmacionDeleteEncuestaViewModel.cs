﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class _ConfirmacionDeleteEncuestaViewModel
    {
        public Int32? EncuestaId { get; set; }
        public String Tipo { get; set; }
        public Int32? Cantidad { get; set; }
        public void Fill(CargarDatosContext dataContext,Int32? EncuestaId,Int32? cantidad)
        {
            Cantidad = cantidad;
            this.EncuestaId = EncuestaId;
            var encuesta = dataContext.context.Encuesta.FirstOrDefault( x => x.IdEncuesta == this.EncuestaId);
            Tipo = encuesta.TipoEncuesta.Acronimo;
        }
    }
}