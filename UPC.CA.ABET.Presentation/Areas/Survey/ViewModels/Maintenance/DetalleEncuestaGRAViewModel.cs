﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class DetalleEncuestaGRAViewModel
    {
        public List<Encuesta> lstEncuesta { get; set; }
        
        public String Carrera { get; set; }
        public String Ciclo { get; set; }
        public String Sede { get; set; }
        public Int32 Cantidad { get; set; }



        public void Fill(CargarDatosContext dataContext, Int32? IdCarrera, Int32? IdCiclo)
        {
           //idCiclo tiene el valor de submodalidad

            var currentCulture = dataContext.currentCulture;

            lstEncuesta = dataContext.context.Encuesta.Where(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdCiclo && x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();
           
            Cantidad = lstEncuesta.Count;



            if (lstEncuesta.Count > 0)
            {
                Carrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? lstEncuesta[0].Carrera.NombreEspanol : lstEncuesta[0].Carrera.NombreIngles;
                Ciclo = lstEncuesta[0].SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            }
        }

    }
}