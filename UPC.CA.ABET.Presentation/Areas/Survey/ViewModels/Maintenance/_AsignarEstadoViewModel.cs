﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class _AsignarEstadoViewModel
    {
        public List<EstadoEncuesta> lstEstado { get; set; }
        public Int32 IdEncuesta { get; set; }
        public Int32 IdEstado { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32 _IdEncuesta)
        {
            lstEstado = dataContext.context.EstadoEncuesta.ToList();
            IdEncuesta = _IdEncuesta;
            IdEstado = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == _IdEncuesta).IdEstado ?? 0;
        }
    }
}