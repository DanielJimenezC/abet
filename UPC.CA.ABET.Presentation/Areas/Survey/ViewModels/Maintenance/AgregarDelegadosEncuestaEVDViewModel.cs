﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
	public class AgregarDelegadosEncuestaEVDViewModel
	{
		Int32? EncuestaVirtualDelegadoId { get; set; }
		bool AgregarDelegado { get; set; }
        bool AgregarInvitado { get; set; }
		Int32? SubModalidadPeriodoAcademicoId { get; set; }
		Int32? ModuloId { get; set; }
		int CantidadDelegadosAgregados { get; set; }
        int CantidadInvitadoAgregados { get; set; }


		public AgregarDelegadosEncuestaEVDViewModel()
		{
			AgregarDelegado = false;
            AgregarInvitado = false;
            CantidadInvitadoAgregados = 0;
			CantidadDelegadosAgregados = 0;
		}

		public void Fill(CargarDatosContext _context, Int32? parIdEncuestaVirtualDelegado)
		{
			var EVD = _context.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == parIdEncuestaVirtualDelegado);


			var LstDelegados = _context.context.Usp_ObtenerDelegados(EVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico, EVD.SubModalidadPeriodoAcademicoModulo.IdModulo,EVD.IdCarrera,EVD.IdEscuela).ToList();
            var LstInvitados = _context.context.AlumnoInvitado.Where(x => x.Tipo == "EVD" && x.IdSubModalidadPeriodoAcademico == EVD.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico && x.IdModulo == EVD.SubModalidadPeriodoAcademicoModulo.IdModulo && x.IdCarrera == EVD.IdCarrera).ToList();

			try
			{
				foreach (var obj in LstDelegados)
				{
					bool existe = BuscarDelegado(_context, obj.IdAlumno, EVD.IdEncuestaVirtualDelegado);

					if (!existe)
					{
						NotificacionEncuestaAlumno objNEA = new NotificacionEncuestaAlumno();

						objNEA.IdAlumno = obj.IdAlumno;
						objNEA.IdCarrera = obj.IdCarrera;
						objNEA.IdSubModalidadPeriodoAcademico = obj.IdSubModalidadPeriodoAcademico;
						objNEA.Estado = false;
						objNEA.IdEncuestaVirtualDelegado = EVD.IdEncuestaVirtualDelegado;
						_context.context.NotificacionEncuestaAlumno.Add(objNEA);
						_context.context.SaveChanges();

						CantidadDelegadosAgregados += 1;
					}

				}
				AgregarDelegado = true;

                foreach (var obj in LstInvitados)
                {
                    var alumno = BuscarAlumnoPorCodigo(_context, obj.codigo);

                    bool existeinvitado = BuscarInvitado(_context, alumno.IdAlumno, EVD.IdEncuestaVirtualDelegado);

                    if (!existeinvitado)
                    {
                        NotificacionEncuestaAlumno objNEA = new NotificacionEncuestaAlumno();

                        objNEA.IdAlumno = alumno.IdAlumno;
                        objNEA.IdCarrera = alumno.Carrera.IdCarrera;
                        objNEA.IdSubModalidadPeriodoAcademico = obj.IdSubModalidadPeriodoAcademico;
                        objNEA.Estado = false;
                        objNEA.IdEncuestaVirtualDelegado = EVD.IdEncuestaVirtualDelegado;
                        _context.context.NotificacionEncuestaAlumno.Add(objNEA);
                        _context.context.SaveChanges();

                        CantidadInvitadoAgregados += 1;
                    }
                }
                AgregarInvitado = true;

            }
			catch (Exception e)
			{
				AgregarDelegado = false;
				throw;
			}

		}
		public bool GetAgregadosSatisfactoriamente()
		{
			return AgregarDelegado;
		}
		public int GetCantidadDelegadosAgregados()
		{
			return CantidadDelegadosAgregados;
		}
        public bool GetAgregadosInvitadosSatisfactoriamente()
        {
            return AgregarInvitado;
        }
        public int GetCantidadInvitadosAgregados()
        {
            return CantidadInvitadoAgregados;
        }

		public bool BuscarDelegado(CargarDatosContext _context, int parIdAlumno, int parIdEVD)
		{
			var Delegado = _context.context.NotificacionEncuestaAlumno.Where(x => x.IdAlumno == parIdAlumno && x.IdEncuestaVirtualDelegado == parIdEVD);

			if (Delegado.Count() > 0)
				return true;
			else
				return false;
		}

        public bool BuscarInvitado(CargarDatosContext _context, int parIdAlumno, int parIdEVD)
        {
            var Invitado = _context.context.NotificacionEncuestaAlumno.Where(x => x.IdAlumno == parIdAlumno && x.IdEncuestaVirtualDelegado == parIdEVD);

           if (Invitado.Count() > 0)
                return true;

            else
                return false;
        }


        public Alumno BuscarAlumnoPorCodigo(CargarDatosContext _context,String Codigo)
        {
            Alumno alumno = _context.context.Alumno.Where(x => x.Codigo == Codigo).FirstOrDefault();

            return alumno;
        }
	}
}