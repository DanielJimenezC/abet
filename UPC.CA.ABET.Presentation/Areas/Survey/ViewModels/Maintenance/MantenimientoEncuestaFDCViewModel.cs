﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using PagedList;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class MantenimientoEncuestaFDCViewModel
    {
        public Int32? IdAcreditadora { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdEstado { get; set; }
        public Int32? IdCurso { get; set; }
        public Int32? IdSeccion { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdCiclo { get; set; }
        public Int32? NroPractica { get; set; }
        public List<Acreditadora> lstAcreditadora { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<ListarComboComisionPorCarrera_Result> lstComision { get; set; }
        public List<EstadoEncuesta> lstEstado { get; set; }
        public List<Sede> lstSede { get; set; }
        public IPagedList<Encuesta> lstEncuestas { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Seccion> lstSeccion { get; set; }
        public Int32? NumeroPagina { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? numeroPagina, Int32? IdCiclo, Int32? IdAcreditadora, Int32? IdCarrera, Int32? IdComision, Int32? IdSede, Int32? IdSeccion, Int32? IdEstado, Int32? IdCurso)
        {
            lstCurso = new List<Curso>();
            lstSeccion = new List<Seccion>();
            NumeroPagina = numeroPagina ?? 1;
            var query = dataContext.context.Encuesta.Include(x => x.EstadoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.FDC && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).ToList();

            if (IdCarrera != null)
            {
                query = query.Where(x => x.IdCarrera == IdCarrera).ToList();
                this.IdCarrera = IdCarrera;
            }
            if (IdSede != null)
            {
                query = query.Where(x => x.IdSede == IdSede).ToList();
                this.IdSede = IdSede;
            }
            if (IdCiclo != null)
            {
                query = query.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo).ToList();
                this.IdCiclo = IdCiclo;
            }
            if (IdSeccion != null)
            {
                //query = query.Where(x => x.IdPeriodoAcademico == IdSeccion).ToList();
                query = query.Where(x => x.IdSeccion == IdSeccion).ToList();
                this.IdSeccion = IdSeccion;
            }
            if (IdEstado != null)
            {
                query = query.Where(x => x.IdEstado == IdEstado).ToList();
                this.IdEstado = IdEstado;
            }
            if (IdCurso != null)
            {
                query = query.Where(x => x.IdCurso == IdCurso).ToList();
                this.IdCurso = IdCurso;
            }
            lstEncuestas = query.OrderBy(x => x.Curso.Codigo).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            
            lstSede = dataContext.context.Sede.ToList();
            lstCiclo = dataContext.context.PeriodoAcademico.ToList();
            lstEstado = dataContext.context.EstadoEncuesta.ToList();
            Int32? IdPeriodo = dataContext.session.GetPeriodoAcademicoId();

        }
    }
}