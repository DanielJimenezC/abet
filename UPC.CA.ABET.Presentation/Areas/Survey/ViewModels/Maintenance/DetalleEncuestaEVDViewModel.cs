﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
	public class DetalleEncuestaEVDViewModel
	{
		public List<Usp_ObtenerNotificacionEVD_Result> LstALumnoNotificacionEVD { get; set; }
		public String PeriodoAcademico { get; set; }
		public Int32? Cantidad { get; set; }
		public String CodigoEncuestaEVD { get; set; }
		public String Ciclo { get; set; }
		public String Modulo { get; set; }
		public String CodigoAlumno { get; set; }
		public String NombreAlumno { get; set; }
		public String TituloCorreo { get; set; }
		[Required]
		public String CuerpoCorreo { get; set; }
		public DateTime NuevaFechaFin { get; set; }

		public EncuestaToken objEncuestaToken { get; set; }



		public DetalleEncuestaEVDViewModel()
		{
			LstALumnoNotificacionEVD = new List<Usp_ObtenerNotificacionEVD_Result>();
		}

		public void Fill(CargarDatosContext dataContext,Int32? parIdEncuestaVirtualDelegado,String parCicloAcademico,Int32? parCantidad,Int32? parIdModulo)
        {
            var EVD = dataContext.context.EncuestaVirtualDelegado.FirstOrDefault(x => x.IdEncuestaVirtualDelegado == parIdEncuestaVirtualDelegado);
            var vmodulo = dataContext.context.Modulo.FirstOrDefault(x => x.IdModulo == parIdModulo);

            CodigoEncuestaEVD = EVD.Codigo;

            Ciclo = parCicloAcademico;

            Cantidad = parCantidad;

            Modulo = vmodulo.IdentificadorSeccion;

			var currentCulture = dataContext.currentCulture;

			LstALumnoNotificacionEVD = dataContext.context.Usp_ObtenerNotificacionEVD(EVD.IdEncuestaVirtualDelegado).ToList();

			Cantidad = LstALumnoNotificacionEVD.Count();
		}

    }
}