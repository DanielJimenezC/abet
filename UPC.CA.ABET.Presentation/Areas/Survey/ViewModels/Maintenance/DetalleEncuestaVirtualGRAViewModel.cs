﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Survey;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Survey.Resources.Views.MaintenanceSurvey.MaintenanceSurveyGRA;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class DetalleEncuestaVirtualGRAViewModel
    {
        public List<EncuestaVirtual> lstEncuesta { get; set; }
        
        public String Carrera { get; set; }
        public String Ciclo { get; set; }
        public String Sede { get; set; }
        public Int32 Cantidad { get; set; }
        /*PARAMETROS PARA EL REENVIO DE CORREO*/
        public String TituloCorreo { get; set; }
        [Required]
        public String CuerpoCorreo { get; set; }
        public DateTime NuevaFechaFin { get; set; }

        public EncuestaToken objEncuestaToken { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 IdCarrera, Int32 IdCiclo)
        {
            this.objEncuestaToken = new EncuestaToken();
            var currentCulture = dataContext.currentCulture;
            NuevaFechaFin = DateTime.Today;

         

            lstEncuesta = (from et in dataContext.context.EncuestaToken
                           join c in dataContext.context.Carrera on et.IdCarrera equals c.IdCarrera
                           join pa in dataContext.context.SubModalidadPeriodoAcademico on et.IdSubModalidadPeriodoAcademico equals pa.IdSubModalidadPeriodoAcademico
                           join e in dataContext.context.Encuesta on et.IdEncuesta equals e.IdEncuesta into gj
                           from x in gj.DefaultIfEmpty()
                           where et.IdCarrera == IdCarrera && et.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == IdCiclo
                           select new EncuestaVirtual
                           {
                               IdEncuestaToken = et.IdEncuestaToken,
                               IdSubModalidadPeriodoAcademico = et.IdSubModalidadPeriodoAcademico,
                               IdCarrera = c.IdCarrera,
                               IdAlumno = et.IdAlumno,
                               FechaFin = et.FechaFin,
                               Id = et.IdEncuesta,
                               Carrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? c.NombreEspanol : c.NombreIngles,
                               Ciclo = pa.PeriodoAcademico.CicloAcademico,
                               Comentario = x.Comentario,
                               Status = et.IdEncuesta == null ? false : true
                                  }
                                ).ToList();
            
           Cantidad = lstEncuesta.Count;
            if (lstEncuesta.Count > 0)
            {
                //Carrera = currentCulture == ConstantHelpers.CULTURE.ESPANOL ? lstEncuesta[0].Carrera.NombreEspanol : lstEncuesta[0].Carrera.NombreIngles;
                //Ciclo = lstEncuesta[0].PeriodoAcademico.CicloAcademico;
                Carrera = lstEncuesta[0].Carrera;
                Ciclo = lstEncuesta[0].Ciclo;
            }
        }

    }
}