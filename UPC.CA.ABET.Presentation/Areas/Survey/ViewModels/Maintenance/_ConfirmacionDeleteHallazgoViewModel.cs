﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class _ConfirmacionDeleteHallazgoViewModel
    {
        public String CodigoHallazgo { get; set; }
        public Int32 IdEncuesta { get; set; }
        public String Tipo { get; set; }
        public String Codigo { get; set; }
        public Int32 IdHallazgo { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32 _IdEncuesta, String codigoHallazgo)
        {
            CodigoHallazgo = codigoHallazgo;
            IdEncuesta = _IdEncuesta;
            //var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
            Tipo = dataContext.context.Encuesta.FirstOrDefault(x => x.IdEncuesta == _IdEncuesta).TipoEncuesta.Acronimo;
        }

        //Se llama a esta función para cargar el helper para eliminar un hallazgo en IRDs
        public void Fill_IRD(CargarDatosContext dataContext, Int32 _IdHallazgo)
        {
            IdHallazgo = _IdHallazgo;

            //var hallazgo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == IdHallazgo);
            Codigo = dataContext.context.Hallazgo.FirstOrDefault(x => x.IdHallazgo == _IdHallazgo).Codigo;
        }

    }
}