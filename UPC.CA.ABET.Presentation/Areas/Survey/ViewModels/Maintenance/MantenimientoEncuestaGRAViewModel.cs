﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using PagedList;
namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class MantenimientoEncuestaGRAViewModel
    {
        public Int32? IdAcreditadora { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdCiclo { get; set; }
        public Int32? NroPractica { get; set; }
        public List<Acreditadora> lstAcreditadora { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<ListarComboComisionPorCarrera_Result> lstComision { get; set; }
        public List<Sede> lstSede { get; set; }
        public IPagedList<uspListarResultadoEncuestaGraduando_Result> lstEncuestas { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public Int32? NumeroPagina { get; set; }
        public void Fill(CargarDatosContext dataContext, Int32? numeroPagina, Int32? IdCiclo, Int32? IdCarrera, Int32? IdComision, Int32? IdSede, Int32 parModalidadId)
        {
            //RECIBE IDPERIODOACADEMICO

            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            int parModalidadIdthis = parModalidadId;
            NumeroPagina = numeroPagina ?? 1;
            var lstSp = dataContext.context.uspListarResultadoEncuestaGraduando(0, 0, 0).ToList();

			var EscuelaId = dataContext.session.GetEscuelaId();


          

				var lstSP2 = (from a in lstSp
							  join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
							  join c in dataContext.context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
							  join d in dataContext.context.Carrera on a.IdCarrera equals d.IdCarrera
							  where c.IdModalidad == parModalidadIdthis && d.IdEscuela==EscuelaId
							  select a
							).ToList();

				lstSp = lstSP2;
		
            

            if (IdCarrera != null)
                lstSp = lstSp.Where(x => x.IdCarrera == IdCarrera).ToList();

       
            if (IdCiclo != null)
            {
                var idsmpa = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdCiclo).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                lstSp = lstSp.Where(x => x.IdSubModalidadPeriodoAcademico == idsmpa).ToList();
            }
    
            lstEncuestas = lstSp.OrderByDescending(x => x.IdSubModalidadPeriodoAcademico).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);

            var escuelaId = dataContext.session.GetEscuelaId();

            lstCarrera = (from capa in dataContext.context.CarreraPeriodoAcademico
                          join submodapa in dataContext.context.SubModalidadPeriodoAcademico on capa.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                          join submoda in dataContext.context.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                          join ca in dataContext.context.Carrera on capa.IdCarrera equals ca.IdCarrera
                          where (submoda.IdModalidad == parModalidadId && ca.IdEscuela == escuelaId)
                          select ca).Distinct().ToList();

            lstSede = dataContext.context.Sede.ToList();

            //Lista los ciclos de acuerdo a la Modalidad en la que se encuentre la Session
            lstCiclo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        join sub in dataContext.context.SubModalidad on submoda.IdSubModalidad equals sub.IdSubModalidad
                        where (sub.IdModalidad==parModalidadId)
                        select pa).ToList();
           

        }
        public void Filter(CargarDatosContext dataContext,Int32? numeroPagina)
        {
            NumeroPagina = numeroPagina ?? 1;
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).ToList();
            lstAcreditadora = dataContext.context.Acreditadora.ToList();
            lstSede = dataContext.context.Sede.ToList();
            lstCiclo = dataContext.context.PeriodoAcademico.ToList();
        }
    }
}