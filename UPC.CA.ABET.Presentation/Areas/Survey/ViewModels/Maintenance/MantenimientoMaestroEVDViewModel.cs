﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class MantenimientoMaestroEVDViewModel
    {
        public Int32? IdPeriodoAcademico { get; set; }
        public Int32? IdModulo { get; set; }
        public Int32? IdSubModalidadPeriodoAcademicoModulo { get; set; }
        public List<SelectListItem> LstPeriodoAcademico { get; set; }
        public List<SelectListItem> LstModulo { get; set; }
        public Int32? NumeroPagina { get; set; }
        public Int32? ModalidadId { get; set; }
        public IPagedList<Usp_ListarEVD_Result> LstEncuestasEVD { get; set; }

        public List<Modulo> LstModuloValido { get; set; }

        public MantenimientoMaestroEVDViewModel()
        {
            LstPeriodoAcademico = new List<SelectListItem>();
            LstModulo = new List<SelectListItem>();
            LstModuloValido = new List<Modulo>();
        }

        public void Fill(CargarDatosContext dataContext, Int32? numeroPagina, Int32? parIdPeriodoAcademico, Int32? parIdModulo, Int32? parModalidadId,Int32? parIdEncuestaVirtualDelegado, int IdSubModalidadPeriodoAcademico)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;

            ModalidadId = parModalidadId;

            NumeroPagina = numeroPagina ?? 1;

            var Lst = dataContext.context.Usp_ListarEVD(0).ToList();

            if (parIdPeriodoAcademico.HasValue)
                Lst = Lst.Where(x => x.IdPeriodoAcademico == parIdPeriodoAcademico).ToList();

            if (parIdModulo.HasValue && parIdModulo != 0)
                Lst = Lst.Where(x => x.IdModulo == parIdModulo).ToList();


            var LstSubModalidad = (from smpam in dataContext.context.SubModalidadPeriodoAcademicoModulo
                                   join m in dataContext.context.Modulo on smpam.IdModulo equals m.IdModulo
                                   where (smpam.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico)
                                   select new { IdSubModalidadPeriodoAcademicoModulo = smpam.IdSubModalidadPeriodoAcademicoModulo, IdModulo = m.IdModulo }
                                    ).ToList();

            for (int i = 0; i < LstSubModalidad.Count; i++)
            {
                int idSub = LstSubModalidad[i].IdSubModalidadPeriodoAcademicoModulo;
                if (dataContext.context.EncuestaVirtualDelegado.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idSub).FirstOrDefault() == null)
                {
                    int idModulo = LstSubModalidad[i].IdModulo;
                    Modulo modulo = dataContext.context.Modulo.Where(x => x.IdModulo == idModulo).FirstOrDefault();
                    LstModuloValido.Add(modulo);
                }
            }

            LstEncuestasEVD = Lst.OrderByDescending(x => x.IdEncuestaVirtualDelegado).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);



            LstPeriodoAcademico = (from a in dataContext.context.SubModalidadPeriodoAcademico
                                   join b in dataContext.context.PeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                                   join c in dataContext.context.SubModalidad on a.IdSubModalidad equals c.IdSubModalidad
                                   where (c.IdModalidad == parModalidadId)
                                   select new SelectListItem { Text = b.CicloAcademico, Value = b.IdPeriodoAcademico.ToString()}
                                   ).ToList();

            if (parIdModulo.HasValue)
            {
                LstModulo = (from a in dataContext.context.SubModalidadPeriodoAcademicoModulo
                             join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                             join c in dataContext.context.Modulo on a.IdModulo equals c.IdModulo
                             where b.IdPeriodoAcademico == parIdPeriodoAcademico
                             select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();
            }
            else
            {
                int variable = LstPeriodoAcademico[0].Value.ToInteger();

                LstModulo = (from a in dataContext.context.SubModalidadPeriodoAcademicoModulo
                             join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                             join c in dataContext.context.Modulo on a.IdModulo equals c.IdModulo
                             where b.IdPeriodoAcademico == variable
                             select new SelectListItem { Text = c.NombreEspanol, Value = c.IdModulo.ToString() }).ToList();
            }
            



        }
    }
}