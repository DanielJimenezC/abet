﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class _ConfirmacionDeleteEncuestaGRAViewModel
    {
        public Int32 IdCarrera { get; set; }
        public Int32 IdEncuesta { get; set; }
        public Int32 IdPeriodoAcademico { get; set; }
        public void Fill(Int32 idCarrera, Int32 idPeriodoAcademico, Int32 idEncuesta)
        {
            this.IdPeriodoAcademico = idPeriodoAcademico;
            this.IdCarrera = idCarrera;
            this.IdEncuesta = idEncuesta;
        }
    }
}