﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using PagedList;
namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class MantenimientoEncuestaVirtualGRAViewModel
    {
        public Int32? IdCiclo { get; set; }
        public Int32? IdCarrera { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public IPagedList<uspListarResultadoEncuestaVirtualGraduando_Result> lstEncuestas { get; set; }
        public Int32? NumeroPagina { get; set; }

        public void Fill(CargarDatosContext dataContext, Int32? numeroPagina, Int32? IdCiclo, Int32? IdCarrera)
        {
            var IdEscuela = dataContext.session.GetEscuelaId();
            int ModalidadId = dataContext.session.GetModalidadId();
            int SMPAId;
    

            NumeroPagina = numeroPagina ?? 1;
            var lstSp = dataContext.context.uspListarResultadoEncuestaVirtualGraduando(IdEscuela, 0, 0).ToList(); //TODO

            lstSp = (from a in lstSp
                     join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                     where b.SubModalidad.IdModalidad == ModalidadId
                     select a).ToList();

            if (IdCarrera != null)
                lstSp = lstSp.Where(x => x.IdCarrera == IdCarrera).ToList();

            if (IdCiclo != null)
            {
                SMPAId = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdCiclo).IdSubModalidadPeriodoAcademico;
                lstSp = lstSp.Where(x => x.IdSubModalidadPeriodoAcademico == SMPAId).ToList();
            }

            lstEncuestas = lstSp.OrderByDescending(x => x.IdSubModalidadPeriodoAcademico).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);
             
            lstCarrera = (from a in dataContext.context.Carrera
                          join b in dataContext.context.SubModalidad on a.IdSubmodalidad equals b.IdSubModalidad
                          where b.IdModalidad == ModalidadId && a.IdEscuela==IdEscuela
                          select a).ToList();

            lstCiclo = (from a in dataContext.context.PeriodoAcademico
                        join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                        join c in dataContext.context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                        where c.IdModalidad== ModalidadId
                        select a).ToList();
        }
        public void Filter(CargarDatosContext dataContext,Int32? numeroPagina)
        {
            int IdEscuela = dataContext.session.GetEscuelaId();
            int ModalidadId = dataContext.session.GetModalidadId();

            NumeroPagina = numeroPagina ?? 1;

            lstCarrera = (from a in dataContext.context.Carrera
                          join b in dataContext.context.SubModalidad on a.IdSubmodalidad equals b.IdSubModalidad
                          where b.IdModalidad == ModalidadId && a.IdEscuela == IdEscuela
                          select a).ToList();

            lstCiclo = (from a in dataContext.context.PeriodoAcademico
                        join b in dataContext.context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                        join c in dataContext.context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
                        where c.IdModalidad == ModalidadId
                        select a).ToList();
        }
    }
}