﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using System.Data.Entity;
using System.Web.Mvc;
using PagedList;

namespace UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Maintenance
{
    public class MantenimientoEncuestaPPPViewModel
    {
        public Int32? IdAcreditadora { get; set; }
        public Int32? IdCarrera { get; set; }
        public Int32? IdComision { get; set; }
        public Int32? IdSede { get; set; }
        public Int32? IdCiclo { get; set; }
        public Int32? NroPractica { get; set; }
        public List<Acreditadora> lstAcreditadora { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<ListarComboComisionPorCarrera_Result> lstComision { get; set; }
        public List<Sede> lstSede { get; set; }
        public IPagedList<Encuesta> LstEncuestas { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<SelectListItem> lstNroPractica { get; set; }
        public Int32? NumeroPagina { get; set; }


        public void Fill(CargarDatosContext dataContext, Int32? numeroPagina, Int32? IdPeriodoAcademico, Int32? IdAcreditadora, Int32? IdCarrera, Int32? IdComision, Int32? IdSede, Int32? NroPractica, Int32? IdEscuela)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var language = cookie != null ? cookie.Value.ToString() : ConstantHelpers.CULTURE.ESPANOL;
            
            NumeroPagina = numeroPagina ?? 1;
            var query = dataContext.context.Encuesta.Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.Alumno.Carrera.IdEscuela == IdEscuela).ToList();

            if (IdCarrera.HasValue)
            {
                query = query.Where(x => x.IdCarrera == IdCarrera).ToList();
                this.IdCarrera = IdCarrera;
            }
            if (IdSede.HasValue)
            { 
                query = query.Where(x => x.IdSede == IdSede).ToList();
                this.IdSede = IdSede;
            }
            if (IdPeriodoAcademico.HasValue)
            { 
                query = query.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).ToList();
				IdCiclo = IdPeriodoAcademico;
            }
            if (NroPractica.HasValue)
            { 
                query = query.Where(x => x.NumeroPractica.numero == NroPractica.Value).ToList();
                this.NroPractica = NroPractica;
            }

            LstEncuestas = query.OrderByDescending(x => x.IdEncuesta).ToPagedList(NumeroPagina.Value, ConstantHelpers.DEFAULT_PAGE_SIZE);
            
            var escuelaId = dataContext.session.GetEscuelaId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId && x.Codigo!="IS").ToList();

            lstSede = dataContext.context.Sede.ToList();
            lstCiclo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submoda.IdSubModalidad == 1)
                        select pa).ToList();
			lstNroPractica = new List<SelectListItem>
			{
				new SelectListItem { Text = "Primera", Value = "1" },
				new SelectListItem { Text = "Segunda", Value = "2" }
			};
		}

    }
}