﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric
{
    public class ClaseProyectoAcademico
    {
        public string Acronimo { get; set; }
        public string Nombre { get; set; }
        public string CodigoAlumno { get; set; }
        public string Descripcion { get; set; }
        public string TipoEvaluador { get; set; }
        public string EmpresaVirtual { get; set; }
        public string Curso { get; set; }
        public int IdProyectoAcademico { get; set; }
        public int IdTipoEvaluacion { get; set; }
        public string Tipo { get; set; }
        public int IdEvaluador { get; set; }
        public bool Guardada { get; set; }
    }
}