﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Models
{
    public class RubricControlExcelObjCapstone
    {
        public RubricControlExcelObjCapstone(string a,
            string b,decimal  c,string d, string e, decimal f,decimal g)
        {
            this.CodigoCurso = a;
            this.Pregunta = b;
            this.PuntajeMaximo = c;
            this.NombreCriterio = d;
            this.Criterio = e;
            this.PuntajeMinimoCriterio = f;
            this.PuntajeMaximoCriterio = g;
        }
        public string CodigoCurso { get; set; }

        public string Pregunta { get; set; }

        public decimal PuntajeMaximo { get; set; } 

        public string NombreCriterio { get; set; }

        public string Criterio { get; set; }

        public decimal PuntajeMinimoCriterio { get; set; }

        public decimal PuntajeMaximoCriterio { get; set; }
    }
}