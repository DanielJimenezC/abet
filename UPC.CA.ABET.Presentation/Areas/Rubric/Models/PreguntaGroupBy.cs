﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Models
{
    public class PreguntaGroupBy
    {
        public string Pregunta { get; set; }
        public decimal PuntajeMaximo { get; set; }

        public List<RubricControlExcelObj> Children { get; set; }

        public List<RubricControlExcelObjCapstone> ChildrenCapstone { get; set; }
    }
}