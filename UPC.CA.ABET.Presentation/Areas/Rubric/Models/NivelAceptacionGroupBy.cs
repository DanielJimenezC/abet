﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Models
{
    public class NivelAceptacionGroupBy
    {
        public string Criterio { get; set; }

        public decimal Peso { get; set; }
        public List<RubricControlExcelObj> Children { get; set; }
    }
}