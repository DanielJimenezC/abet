﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Models
{
    public class OptionSelect
    {
        public int Value { get; set; }

        public string Label { get; set; }
    }
}