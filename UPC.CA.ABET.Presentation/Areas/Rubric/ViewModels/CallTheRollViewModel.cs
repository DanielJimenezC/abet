﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class CallTheRollViewModel
    {
        #region Propiedades
        public Int32 IdReunionDelegado { get; set; }
        public List<Alumno> ListaAlumnos { get; set; }
        public List<int> ListaIdAlumnos { get; set; }
        public List<bool> ListaPresent { get; set; }
        public List<AlumnoInvitado> ListaAlumnosInvitados { get; set; }
        private AbetEntities context { get; set; }
        #endregion

        #region Metodos
        public CallTheRollViewModel()
        {

        }
        public void CargarDatos(CargarDatosContext DataContext, int IdReunionDelegado)
        {
            this.IdReunionDelegado = IdReunionDelegado;
            List<bool> auxlistPresent = new List<bool>();
            List<int> auxListaIdAlumnos=new List<int>();
            List<Alumno> auxListaAlumnos = ((from cu in DataContext.context.Alumno
                             select cu
                             ).Take(40)).ToList();
            List<AlumnoInvitado> auxListAlumnoInvitado = (from cu in DataContext.context.AlumnoInvitado
                                                           select cu).OrderBy(c => c.idAlumnoInvitado).ToList();
            ListaAlumnos = auxListaAlumnos;
            int auxid;
            for (int i =0; i < ListaAlumnos.Count(); i++)
            {
                auxid = auxListaAlumnos[i].IdAlumno;
                auxListaIdAlumnos.Add(auxid);
                auxlistPresent.Add(false);
            }
            ListaPresent = auxlistPresent;
            ListaIdAlumnos = auxListaIdAlumnos;
            ListaAlumnosInvitados = auxListAlumnoInvitado;
        }
        #endregion
    }
}