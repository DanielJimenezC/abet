﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.OutcomeGoal
{
    public class ListOutcomeGoalViewModel
    {
        #region Propiedades

        public List<Outcome> LstOutcome { get; set; }

        public Int32 OutcomeId { get; set; }

        public Int32 WascId { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga todos los Outcomes para la comisión WASC para
        /// el periodo académico activo.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext)
        {
            Int32 periodoAcademicoActualId = DataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
                    .FirstOrDefault(x=>x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            WascId = BulkInsertLogic .GetWascId(DataContext.context, periodoAcademicoActualId);

            if(WascId == 0)
            {
                throw new Exception("No existe la comisión WASC para el ciclo activo");
            }

            var lstOutcomesId = DataContext.context.OutcomeComision.Where(
                x => x.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId && x.IdComision == WascId)
                .Select(x => x.IdOutcome);

            LstOutcome = DataContext.context.Outcome.Where(x => lstOutcomesId.Any(y => y == x.IdOutcome)).ToList();
        }

        #endregion
    }
}
