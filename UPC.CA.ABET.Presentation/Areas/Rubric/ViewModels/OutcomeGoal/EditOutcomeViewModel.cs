﻿
#region Imports

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.OutcomeGoal;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.OutcomeGoal
{
    public class EditOutcomeViewModel
    {
        #region Propiedades

        public Int32 OutcomeComisionId { get; set; }

        public String Outcome { get; set; }

        [Required]
        [StringLength(500)]
        [Display(Name = "LogroEsperado", ResourceType = typeof(ListOutcomeGoalResource))]
        public String LogroEsperado { get; set; }

        [Required]
        [StringLength(500)]
        [Display(Name = "LogroSobresaliente", ResourceType = typeof(ListOutcomeGoalResource))]
        public String LogroSobresaliente { get; set; }

        [Required]
        [StringLength(500)]
        [Display(Name = "LogroEjemplar", ResourceType = typeof(ListOutcomeGoalResource))]
        public String LogroEjemplar { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de los logros de un Outcome.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="OutcomeComisionId">ID del Outcome Comisión Seleccionado</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32 OutcomeComisionId)
        {
            this.OutcomeComisionId = OutcomeComisionId;

            Outcome =
                DataContext.context.OutcomeComision.FirstOrDefault(x => x.IdOutcomeComision == OutcomeComisionId)
                    .Outcome.Nombre;

            var lstLogros = DataContext.context.LogroOutcomeComision.Where(x => x.IdOutcomeComision == OutcomeComisionId);

            foreach (var item in lstLogros)
            {
                switch (item.Orden)
                {
                    case 0:
                        LogroEsperado = item.Descripcion;
                        break;
                    case 1:
                        LogroSobresaliente = item.Descripcion;
                        break;
                    case 2:
                        LogroEjemplar = item.Descripcion;
                        break;
                }
            }
        }

        #endregion
    }
}
