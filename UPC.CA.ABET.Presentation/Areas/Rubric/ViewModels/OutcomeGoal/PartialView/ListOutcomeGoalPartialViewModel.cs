﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.OutcomeGoal.PartialView
{
    public class ListOutcomeGoalPartialViewModel
    {
        #region Propiedades

        public List<OutcomeComision> LstOutcomeWasc { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de logros de Outcomes de la comisión WASC.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="OutcomeId">ID del Outcome Seleccionado</param>
        /// <param name="WascId">ID de la comisión WASC</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32? OutcomeId, Int32 WascId)
        {
            Int32 periodoAcademicoActualId =
                DataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
                    .FirstOrDefault(X=>X.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            LstOutcomeWasc = DataContext.context.OutcomeComision.Where(
                x =>
                    x.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId && x.IdComision == WascId &&
                    x.LogroOutcomeComision.Any() && (x.IdOutcome == OutcomeId || OutcomeId == 0)).ToList();
        }

        #endregion
    }
}
