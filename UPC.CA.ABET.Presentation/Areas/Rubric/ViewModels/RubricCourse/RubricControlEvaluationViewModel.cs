﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class RubricControlEvaluationViewModel
    {
        public int idAlumno { get; set; }
        public string nombreAlumno { get; set; }
        public string codigoAlumno { get; set; }
        public int idDocenteCalificador { get; set; }
        public string nombreDocente { get; set; }
        public string nombreCurso { get; set; }
        public string nombreSeccion { get; set; }
        public int idRubricaControl { get; set; }
        public DateTime fecha { get; set; }
        public int idSeccion { get; set; }
        public int idCurso { get; set; }
        public int idRubricType { get; set; }        
        public string rubricTypeName { get; set; }
        public List<RubricaControlCriterio> lstRCCriterio { get; set; }
        public List<RubricaControlPregunta> lstRCPregunta { get; set; }
        public List<RubricaControlNivelAceptacion> lstRCNivelAceptacion { get; set; }

        public void Fill(CargarDatosContext ctx, int idStudent, int idCourse, int idSection, int rubricType)
        {
            var context = ctx.context;
            if (ctx.session.GetCulture().Equals(ConstantHelpers.CULTURE.ESPANOL))
            {
                this.rubricTypeName = rubricType == 1 ? "Evaluación Parcial" : "Evaluación Final";
            }
            else
            {
                this.rubricTypeName = rubricType == 1 ? "Partial Evaluation" : "Final Evaluation";
            }
            idRubricType = rubricType;
            var isFinal = rubricType == 1 ? true : false;
            var submodalPA = ctx.session.GetSubModalidadPeriodoAcademicoId().Value;
            var seccion = context.Seccion.Where(x => x.IdSeccion == idSection).FirstOrDefault();
            nombreSeccion = seccion.Codigo;
            idSeccion = idSection;
            var curso = context.Curso.Where(x => x.IdCurso == idCourse).FirstOrDefault();
            idCurso = idCourse;
            nombreCurso = curso.NombreEspanol;
            idAlumno = idStudent;
            var student = context.Alumno.Where(x => x.IdAlumno == idStudent).FirstOrDefault();
            nombreAlumno = student.Nombres + " " + student.Apellidos;
            codigoAlumno = student.Codigo;
            idDocenteCalificador = ctx.session.GetDocenteId().ToInteger();
            var doc = context.Docente.Where(x => x.IdDocente == idDocenteCalificador).FirstOrDefault();
            nombreDocente = doc.Nombres + " " + doc.Apellidos;
            idRubricaControl = context.RubricaControl.Where(x => x.IdCurso == idCourse && x.IsDeleted == false && x.IdSubModalidadPeriodoAcademico == submodalPA && x.IsFinal == isFinal).Select(x => x.idRubricaControl).FirstOrDefault();

            lstRCPregunta = context.RubricaControlPregunta.Where(x => x.idRubricaControl == idRubricaControl).ToList();
            lstRCNivelAceptacion = context.RubricaControlNivelAceptacion.Where(x => x.idRubricaControl == idRubricaControl).ToList();
            lstRCCriterio = context.RubricaControlCriterio.Where(x => x.RubricaControlPregunta.idRubricaControl == idRubricaControl).ToList();

        }

        public bool CreateRubricControlEvaluation(string[][] arrayData, int idRubricaControl, int idAlumno, int idDocenteCalificador, int idSeccion) {
            return RubricCourseLogic.CreateRubricControlEvaluation(arrayData, idRubricaControl, idAlumno, idDocenteCalificador, idSeccion);
        }
    }
}