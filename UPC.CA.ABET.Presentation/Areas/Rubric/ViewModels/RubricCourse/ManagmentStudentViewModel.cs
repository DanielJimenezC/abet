﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class ManagmentStudentViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredCode", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public string Codigo { get; set; }
        [Required(ErrorMessageResourceName = "RequiredStudentName", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public string NombreAlumno { get; set; }
        [Required(ErrorMessageResourceName = "RequiredStudentLastName", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public string ApellidoAlumno { get; set; }
        [Required(ErrorMessageResourceName = "RequiredCourse", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public int IdCurso { get; set; }
        [Required(ErrorMessageResourceName = "RequiredSection", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public int IdSeccion { get; set; }
        [Required(ErrorMessageResourceName = "RequiredCareer", ErrorMessageResourceType = typeof(RubricCourseResource))]
        public int IdCarrera { get; set; }
        public bool Saved { get; set; }

        public IEnumerable<SelectListItem> sltCourses { get; set; }
        public IEnumerable<SelectListItem> sltSection { get; set; }
        public IEnumerable <SelectListItem> sltCareer { get; set; }

        public void Fill(CargarDatosContext ctx) {
           
            var context = ctx.context;
            var IdEscuela = ctx.session.GetEscuelaId();
            int? idDocente = ctx.session.GetDocenteId();
            int? idModalidad = ctx.session.GetModalidadId();
            int? submodapa = ctx.session.GetSubModalidadPeriodoAcademicoId();
            var tp1 = "TALLER DE PROYECTO I";

            var lstCurso = (from mcd in context.MallaCocosDetalle
                        join cmc in context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                        join cur in context.Curso on cmc.IdCurso equals cur.IdCurso
                        join sc in context.SeccionCurso on cur.IdCurso equals sc.IdCurso
                        join ds in context.DocenteSeccion on sc.IdSeccion equals ds.IdSeccion
                        join lc in context.LogCarga on sc.IdLogCarga equals lc.IdLogCarga
                        where mcd.IdTipoOutcomeCurso == 1 &&
                               lc.IdSubModalidadPeriodoAcademico == submodapa &&
                               ds.IdDocente == idDocente &&
                               cur.NombreEspanol != tp1
                        select cur).Distinct().ToList();

            if (ctx.session.GetCulture().Equals(ConstantHelpers.CULTURE.ESPANOL))
            {
                sltCareer = (from car in context.Carrera
                            join sum in context.SubModalidad on car.IdSubmodalidad equals sum.IdSubModalidad
                            where sum.IdModalidad == idModalidad
                            select new SelectListItem {
                                Value = car.IdCarrera.ToString(), Text = car.NombreEspanol
                            }).ToList();
                sltCourses = lstCurso.Select(x => new SelectListItem { Value = x.IdCurso.ToString(), Text = x.NombreEspanol });
                sltSection = context.Seccion.Where(x => x.Codigo == "0").Select(x => new SelectListItem { Value = "", Text = "Seleccione" });
            }
            else {
                sltCareer = (from car in context.Carrera
                             join sum in context.SubModalidad on car.IdSubmodalidad equals sum.IdSubModalidad
                             where sum.IdModalidad == idModalidad
                             select new SelectListItem
                             {
                                 Value = car.IdCarrera.ToString(),
                                 Text = car.NombreIngles
                             }).ToList();
                sltCourses = lstCurso.Select(x => new SelectListItem { Value = x.IdCurso.ToString(), Text = x.NombreIngles });
                sltSection = context.Seccion.Where(x => x.Codigo == "0").Select(x => new SelectListItem { Value = "", Text = "Select" });

            }
        }

        public bool SaveStudent(string codigo, string nombre, string apellido,int idCurso, int idCarrera, int idSection, int idDocente)
        {            
            return RubricCourseLogic.SaveStudent(codigo,nombre, apellido, idCurso, idCarrera, idSection, idDocente);
            
        }

    }
}