﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class RubricCourseViewModel
    {
        public int idSeccion { get; set; }
        public int idCurso { get; set; }
        public int idSede { get; set; }
        public List<Seccion> lstSection { get; set; }
        public List<Curso> lstCurso { get; set; }        
        public bool? Saved { get; set; }
        public Int32 tipoRubrica { get; set; }

        public List<OptionSelect> lstTipoRubrica { get; set; }

        public void Fill(CargarDatosContext ctx)
        {
            lstTipoRubrica = new List<OptionSelect>();
            
            string a = ctx.session.GetCulture().ToString();
            var context = ctx.context;
            var IdEscuela = ctx.session.GetEscuelaId();
            int? idDocente = ctx.session.GetDocenteId();
            int? submodapa = ctx.session.GetSubModalidadPeriodoAcademicoId();
            var tp1 = "TALLER DE PROYECTO I";
            //lstSection = (from submoda in ctx.context.SubModalidadPeriodoAcademico
            //            join pa in ctx.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
            //            where (submoda.IdSubModalidad == 1)
            //            select pa).ToList();
            lstSection = (from sec in context.Seccion
                          join sd in context.DocenteSeccion on sec.IdSeccion equals sd.IdSeccion
                          join doc in context.Docente on sd.IdDocente equals doc.IdDocente
                          join lc in context.LogCarga on sd.IdLogCarga equals lc.IdLogCarga
                          where sd.IdDocente == idDocente && lc.IdSubModalidadPeriodoAcademico == submodapa
                          select sec).ToList();


            var cursosDocente = (from cur in context.Curso
                        join sc in context.SeccionCurso on cur.IdCurso equals sc.IdCurso
                        join sd in context.DocenteSeccion on sc.IdSeccion equals sd.IdSeccion
                        join lc in context.LogCarga on sd.IdLogCarga equals lc.IdLogCarga
                        where sd.IdDocente == idDocente && lc.IdSubModalidadPeriodoAcademico == submodapa
                        select cur.IdCurso).ToList();

            if (ctx.session.GetCulture().Equals(ConstantHelpers.CULTURE.ESPANOL))
            {
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Parcial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }
            else
            {
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Partial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }

            lstCurso = (from ua in context.UnidadAcademica
                        join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                        join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                        join doc in context.Docente on uar.IdDocente equals doc.IdDocente
                        join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                        join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
                        join cmc in context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
                        join mcd in context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                        where ua.IdSubModalidadPeriodoAcademico == submodapa && ua.IdEscuela == IdEscuela 
                        //&& uar.IdDocente == idDocente
                        && mcd.IdTipoOutcomeCurso == 1 && cur.NombreEspanol != "TALLER DE PROYECTO I"
                        && cursosDocente.Contains(cur.IdCurso)
                        select cur).Distinct().ToList();

            

        }       
    }
}
    