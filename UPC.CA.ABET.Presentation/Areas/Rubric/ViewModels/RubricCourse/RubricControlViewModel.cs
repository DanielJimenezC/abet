﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class RubricControlViewModel
    {
        public int idRubricaControl { get; set; }
        public int idCurso { get; set; }
        public int idDocente { get; set; }
        public DateTime fecha { get; set; }
        public bool isDeleted { get; set; }
        public List<RubricaControlCriterio> lstRCCriterio { get; set; }
        public List<RubricaControlPregunta> lstRCPregunta { get; set; }
        public List<RubricaControlNivelAceptacion> lstRCNivelAceptacion { get; set; }

        public void Fill(CargarDatosContext ctx, int idRubric)
        {
            var context = ctx.context;
            if (SessionHelper.GetDocenteId(ctx.session) != null)
            {
                int aux = SessionHelper.GetDocenteId(ctx.session).Value;
                var rubricaControl = context.RubricaControl.Where(x => x.idRubricaControl == idRubric).FirstOrDefault();
                idRubricaControl = idRubric;
                fecha = rubricaControl.fecha.Value;
                idCurso = rubricaControl.IdCurso;
                isDeleted = rubricaControl.IsDeleted;
                idDocente = rubricaControl.IdDocente;
                lstRCPregunta = rubricaControl.RubricaControlPregunta.ToList();
                lstRCNivelAceptacion = rubricaControl.RubricaControlNivelAceptacion.ToList();
                lstRCPregunta = rubricaControl.RubricaControlPregunta.ToList();               
                lstRCCriterio = context.RubricaControlCriterio.Where(x => x.RubricaControlPregunta.idRubricaControl == idRubric).ToList();
            }

        }
    }
}