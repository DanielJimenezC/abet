﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.ActionPlan;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class MaintenanceListRubricViewModel
    {

        public List<RubricaControl> lstRubric { get; set; }

        public List<Curso> lstCourses { get; set; }

        public List<bool> lstBooleanos { get; set; }
        

        public void Fill(CargarDatosContext ctx )
        {
            lstBooleanos = new List<bool>();
            var context = ctx.context;
            if (SessionHelper.GetDocenteId(ctx.session) != null)
            {
                int aux = SessionHelper.GetDocenteId(ctx.session).Value;


                lstRubric = (from rc in context.RubricaControl
                             join c in context.Curso on rc.IdCurso equals c.IdCurso
                             where rc.IdDocente == aux && rc.IsDeleted == false
                             select rc).ToList();

                lstCourses = (from c in context.Curso
                              join rc in context.RubricaControl on c.IdCurso equals rc.IdCurso
                              where rc.IdDocente == aux && rc.IsDeleted == false
                              select c).ToList();

                var lstRubricControlEvaluation = (from rce in context.RubricaControlEvaluacion
                                                  where rce.idDocenteCalificador == aux
                                                  select rce).ToList();

                for (int i = 0; i < lstRubric.Count(); i++)
                {
                    lstBooleanos.Add(false);
                    for (int j = 0; j < lstRubricControlEvaluation.Count(); j++)
                    {
                        if (lstRubric[i].idRubricaControl == lstRubricControlEvaluation[j].idRubricaControl)
                        {
                            lstBooleanos[i] = true;
                        }
                    }
                }

            }


           
            
           





        }

        
    }
}