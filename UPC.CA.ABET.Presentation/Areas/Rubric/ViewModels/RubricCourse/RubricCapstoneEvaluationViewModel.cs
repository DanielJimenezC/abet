﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class RubricCapstoneEvaluationViewModel
    {
        public string nombreProyecto { get; set; }
        public string observaciones { get; set; }
        public int idCurso { get; set; }
        public int idRubricaControl { get; set; }
        public int idProyectoAcademico { get; set; }
        public List<Alumno> lstAlumno { get; set; }
        public List<RubricaControlCriterioCapstone> lstRCCapstone { get; set; }
        public List<RubricaControlCriterio> lstRCCriterio { get; set; }
        public List<RubricaControlPregunta> lstRCPregunta { get; set; }
        public List<RubricaControlEvaluacionResultadoCapstone> lstNotasPregunta { get; set; }
        public List<string> lstRCNivelAceptacion { get; set; }

        public bool Edit { get; set; }
        public void Fill(CargarDatosContext ctx, int IdProyectoAcademico, int IdCurso)
        {
            var context = ctx.context;
            var docenteID = ctx.session.GetDocenteId().Value;

            var submodalPA = ctx.session.GetSubModalidadPeriodoAcademicoId().Value;

            var curso = context.Curso.Where(x => x.IdCurso == IdCurso).FirstOrDefault();
            idCurso = IdCurso;
            nombreProyecto = context.ProyectoAcademico.First(x => x.IdProyectoAcademico == IdProyectoAcademico).Nombre;

            //var alumnoSeccion = (from als in context.AlumnoSeccion
            //                    join pa in context.ProyectoAcademico on als.IdProyectoAcademico equals pa.IdProyectoAcademico
            //                    where pa.IdProyectoAcademico == IdProyectoAcademico
            //                    select als).ToList();    
            lstAlumno = (from al in context.Alumno
                         join alm in context.AlumnoMatriculado on al.IdAlumno equals alm.IdAlumno
                         join als in context.AlumnoSeccion on alm.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                         where als.IdProyectoAcademico == IdProyectoAcademico
                         orderby als.IdAlumnoSeccion
                         select al).ToList();

            this.idProyectoAcademico = IdProyectoAcademico;

            idRubricaControl = context.RubricaControl.Where(x => x.IdCurso == IdCurso && x.IsDeleted == false && x.IdSubModalidadPeriodoAcademico == submodalPA && x.IsFinal == false).Select(x => x.idRubricaControl).FirstOrDefault();
            lstRCPregunta = context.RubricaControlPregunta.Where(x => x.idRubricaControl == idRubricaControl).ToList();
            var idRubPreg = lstRCPregunta.Count() >0 ? lstRCPregunta[0].idRubricaControlPregunta : 0;
            lstRCNivelAceptacion = context.RubricaControlCriterioCapstone.Where(x => x.RubricaControlPregunta.idRubricaControl == idRubricaControl && x.idRubricaControlPregunta == idRubPreg).Select(x => x.nombreEspanol).ToList();
            lstRCCapstone = context.RubricaControlCriterioCapstone.Where(x => x.RubricaControlPregunta.idRubricaControl == idRubricaControl).ToList();
            //lstRCCriterio = context.RubricaControlCriterio.Where(x => x.RubricaControlPregunta.idRubricaControl == idRubricaControl).ToList();

            //REVISANDO PERMISO
            var rubricaControlCalifica = context.RubricaControlCalifica.Where(x => x.IdSubModalidadPeriodoAcademico== submodalPA
             && x.IdProyectoAcademico==IdProyectoAcademico && x.IdDocente== docenteID)                
                .FirstOrDefault() ;

            Edit = rubricaControlCalifica.Edit.Value;

            //Cargando Notas
            lstNotasPregunta = (from ASE in context.AlumnoSeccion
                                join PAC in context.ProyectoAcademico on ASE.IdProyectoAcademico equals PAC.IdProyectoAcademico
                                join RCE in context.RubricaControlEvaluacion on ASE.IdAlumnoSeccion equals RCE.idAlumnoSeccion
                                join RCO in context.RubricaControl on RCE.idRubricaControl equals RCO.idRubricaControl
                                join RCERC in context.RubricaControlEvaluacionResultadoCapstone on RCE.IdRubricaControlEvaluacion equals RCERC.IdRubricaControlEvaluacion
                                where RCO.IdCurso == IdCurso && PAC.IdProyectoAcademico == IdProyectoAcademico
                                orderby RCERC.IdRubricaControlPregunta, ASE.IdAlumnoSeccion
                                select RCERC
                                ).ToList();

            int IdRubricaControlEvaluacion = 0;
            if (lstNotasPregunta.Count() > 0)
            {
                IdRubricaControlEvaluacion = lstNotasPregunta.FirstOrDefault().RubricaControlEvaluacion.IdRubricaControlEvaluacion;
                observaciones = context.RubricaControlEvaluacion.Where(x => x.IdRubricaControlEvaluacion == IdRubricaControlEvaluacion).FirstOrDefault().observacion;
            }
        }
        
        public void CreateRubricCapstoneEvaluation(CargarDatosContext ctx, string[][] arrayData, string[][] arrayData2,int idRubricaControl, int idProyectoAcademico, int idCurso, string observation)
        {
            var docenteID = ctx.session.GetDocenteId().Value;
            RubricCourseLogic.CreateRubricCapstoneEvaluation(arrayData, arrayData2, idRubricaControl, idProyectoAcademico, idCurso, docenteID, observation);
        }

        public void EditRubricCapstoneEvaluation(CargarDatosContext ctx, string[][] arrayData, string[][] arrayData2, int idRubricaControl, int idProyectoAcademico, int idCurso, string observation)
        {
            var docenteID = ctx.session.GetDocenteId().Value;
            RubricCourseLogic.EditRubricCapstoneEvaluation(lstNotasPregunta, arrayData, arrayData2, idRubricaControl, idProyectoAcademico, idCurso, docenteID, observation);
        }
    }
}