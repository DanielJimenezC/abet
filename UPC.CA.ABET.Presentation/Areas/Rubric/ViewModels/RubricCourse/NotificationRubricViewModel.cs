﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class NotificationRubricViewModel
    {
        public String asunto { get; set; }
        public String mensaje { get; set; }

        public List<Curso> LstCurso { get; set; }
        public int? IdCurso { get; set; }
        
        //public string CodCurso { get; set; }
        //public int? IdTipoEvaluadorMaestro { get; set; }
        public string Estado { get; set; }

        public void fill(CargarDatosContext dataContext, int? IdPeriodoAcademico, int? idEscuela)
        {
            //LstEstados = new List<SelectListItem>();
            //LstEstados.Add(new SelectListItem { Text = "CALIFICADO", Value = "CALIFICADO" });
            //LstEstados.Add(new SelectListItem { Text = "NOCALIFICADO", Value = "NOCALIFICADO" });

            var context = dataContext.context;

            var objMensaje = context.Mensaje.Where(x => x.idEscuela == idEscuela).FirstOrDefault();
            var submodapa = dataContext.session.GetSubModalidadPeriodoAcademicoId();
            var IdEscuela = dataContext.session.GetEscuelaId();
            var idDocente = dataContext.session.GetDocenteId();

            this.mensaje = (objMensaje == null) ? "" : objMensaje.Mensaje1;
            this.asunto = (objMensaje == null) ? "" : objMensaje.Asunto;

            var cursoCreado = context.RubricaControl.Where(x => x.IsDeleted == false).Select(x => x.IdCurso).ToList();

            this.LstCurso = new List<Curso>();
            LstCurso = (from ua in context.UnidadAcademica
                        join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                        join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                        join doc in context.Docente on uar.IdDocente equals doc.IdDocente
                        join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                        join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
                        join cmc in context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
                        join mcd in context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                        where ua.IdSubModalidadPeriodoAcademico == submodapa && ua.IdEscuela == IdEscuela && uar.IdDocente == idDocente
                        && mcd.IdTipoOutcomeCurso == 1 && cur.NombreEspanol != "TALLER DE PROYECTO I" && cursoCreado.Contains(cur.IdCurso)
                        select cur).Distinct().ToList();

        }
    }
    
}