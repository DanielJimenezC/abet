﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class UploadRubricViewModel
    {
        public Int32? CarreraId { get; set; }
        public Int32? SedeId { get; set; }
        public Int32? CicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }

        public Int32? tipoRubrica { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }

        public List<OptionSelect> lstTipoRubrica { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        public bool isCoordinador { get; set; }

        public string currentCulture { get; set; }
        public UploadRubricViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext, int ModalidadId)
        {
            var escuelaId = dataContext.session.GetEscuelaId();

            int SubModalidadId = dataContext.context.SubModalidad.FirstOrDefault(x => x.IdModalidad == ModalidadId).IdSubModalidad;
            var subModalidadPaId = dataContext.session.GetSubModalidadPeriodoAcademicoId();
            lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId && x.IdSubmodalidad == SubModalidadId).ToList();



            lstSede = new List<Sede>();

            lstTipoRubrica = new List<OptionSelect>();
            if(currentCulture== ConstantHelpers.CULTURE.ESPANOL)
            {
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value=1,
                    Label="Parcial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }
            else
            {
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Partial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }
            

            //CAMBIARLO POR VARIABLE DE SWITCH MODALIDAD
            lstCiclo = (from submoda in dataContext.context.SubModalidadPeriodoAcademico
                        join pa in dataContext.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submoda.IdSubModalidad == SubModalidadId)
                        select pa).ToList();

            lstSubMOdalidadPeriodoAcademico = (from a in dataContext.context.SubModalidadPeriodoAcademico
                                               join b in dataContext.context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
                                               where b.IdSubModalidad == SubModalidadId
                                               select a).OrderBy(x => x.IdPeriodoAcademico).ToList();

            var lstCurso = from ua in dataContext.context.UnidadAcademica
                                     join sua in dataContext.context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                                     join uar in dataContext.context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                                     join doc in dataContext.context.Docente on uar.IdDocente equals doc.IdDocente
                                     join cpa in dataContext.context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                                     join cur in dataContext.context.Curso on cpa.IdCurso equals cur.IdCurso
                                     //join cmc in dataContext.context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
                                     // join mcd in dataContext.context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                                     where ua.IdSubModalidadPeriodoAcademico == subModalidadPaId && ua.IdEscuela == escuelaId
                                     //&& uar.IdDocente == idDocente
                                     //&& mcd.IdTipoOutcomeCurso == 1 && cur.NombreEspanol != "TALLER DE PROYECTO I"
                                     select doc.IdDocente;
            isCoordinador = lstCurso.Contains(dataContext.session.GetDocenteId().Value);

        }
    }
}