﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class DowloadRubricViewModel
    {
        public int idSeccion { get; set; }
        public int idCurso { get; set; }
        public List<Seccion> lstSection { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Curso> lstCursoCapstone { get; set; }
        public Int32 tipoRubrica { get; set; }
        public List<OptionSelect> lstTipoRubrica { get; set; }

        public List<GetListPartialRubricCapstone_Result> lstNotas { get; set; }
        public void Fill(CargarDatosContext ctx, int? cursoId)
        {
            idCurso = cursoId.HasValue ? cursoId.Value : idCurso;
            var context = ctx.context;
            var IdEscuela = ctx.session.GetEscuelaId();
            int? idDocente = ctx.session.GetDocenteId();
            int? submodapa = ctx.session.GetSubModalidadPeriodoAcademicoId();
            


            var cursoCreado = (from rce in context.RubricaControlEvaluacion
                               join rc in context.RubricaControl on rce.idRubricaControl equals rc.idRubricaControl
                               where rc.IsDeleted == false
                               select rc.IdCurso).ToList();

            var cursosDocente = (from cur in context.Curso
                                 join sc in context.SeccionCurso on cur.IdCurso equals sc.IdCurso
                                 join sd in context.DocenteSeccion on sc.IdSeccion equals sd.IdSeccion
                                 join lc in context.LogCarga on sd.IdLogCarga equals lc.IdLogCarga
                                 where sd.IdDocente == idDocente && lc.IdSubModalidadPeriodoAcademico == submodapa
                                 select cur.IdCurso).ToList();



            lstNotas = context.GetListPartialRubricCapstone(0, submodapa, idCurso, 0).ToList();



            this.lstCurso = new List<Curso>();
            lstCurso = (from ua in context.UnidadAcademica
                        join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                        join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                        join doc in context.Docente on uar.IdDocente equals doc.IdDocente
                        join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                        join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
                        join cmc in context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
                        join mcd in context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                        where ua.IdSubModalidadPeriodoAcademico == submodapa && ua.IdEscuela == IdEscuela && uar.IdDocente == idDocente
                        && cursoCreado.Contains(cur.IdCurso)
                        //&& cursosDocente.Contains(cur.IdCurso)
                        select cur).Distinct().ToList();

            this.lstCursoCapstone = new List<Curso>();
            lstCursoCapstone = (from ua in context.UnidadAcademica
                        join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                        join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                        join doc in context.Docente on uar.IdDocente equals doc.IdDocente
                        join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                        join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
                        where ua.IdSubModalidadPeriodoAcademico == submodapa && ua.IdEscuela == IdEscuela
                        && cursoCreado.Contains(cur.IdCurso)
                        //&& cursosDocente.Contains(cur.IdCurso)
                        select cur).Distinct().ToList();



            lstSection = (from sec in context.Seccion
                          join sd in context.DocenteSeccion on sec.IdSeccion equals sd.IdSeccion
                          join doc in context.Docente on sd.IdDocente equals doc.IdDocente
                          join lc in context.LogCarga on sd.IdLogCarga equals lc.IdLogCarga
                          where sd.IdDocente == idDocente && lc.IdSubModalidadPeriodoAcademico == submodapa
                          select sec).ToList();

            lstTipoRubrica = new List<OptionSelect>();
            if (ctx.session.GetCulture().Equals(ConstantHelpers.CULTURE.ESPANOL))
            {               
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Parcial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }
            else
            {
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Partial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }




        }
    }
}