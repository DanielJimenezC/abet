﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Dto;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class EvaluateRubricCapstoneViewModel
    {
        public Int32? CarreraId { get; set; }
        public Int32? SedeId { get; set; }
        public Int32? CicloId { get; set; }
        public Int32? subModalidadPeriodoAcademicoId { get; set; }

        public Int32? docenteId { get; set; }
        public Int32? tipoRubrica { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Sede> lstSede { get; set; }
        public List<ProyectoAcademicoDto> proyectos { get; set; }
        public List<OptionSelect> lstTipoRubrica { get; set; }
        public List<SubModalidadPeriodoAcademico> lstSubMOdalidadPeriodoAcademico { get; set; }
        public bool isCoordinador { get; set; }

        public string currentCulture { get; set; }

        public List<bool> isQualified { get; set; }
        public EvaluateRubricCapstoneViewModel()
        {
        }
        public void Fill(CargarDatosContext dataContext, int ModalidadId)
        {
            var IdSubModalidadPeriodoAcademicoModulo = dataContext.context.SubModalidadPeriodoAcademicoModulo.Where(
                z => z.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId.Value).Select(x => x.IdSubModalidadPeriodoAcademicoModulo)
                .FirstOrDefault();

            //proyectos = dataContext.context.ProyectoAcademico.Include("Curso").Where(
            //    x => x.IdSubModalidadPeriodoAcademicoModulo.Value == IdSubModalidadPeriodoAcademicoModulo)
            //    .OrderBy(x => x.Nombre)
            //    .ToList()
            //    ;


            var context = dataContext.context;
            proyectos = (from ua in context.ProyectoAcademico
                         join sua in context.Curso on ua.IdCurso equals sua.IdCurso
                         join rcc in context.RubricaControlCalifica on ua.IdProyectoAcademico equals rcc.IdProyectoAcademico
                         where ua.IdSubModalidadPeriodoAcademicoModulo == IdSubModalidadPeriodoAcademicoModulo
                          && rcc.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId
                          && rcc.IdDocente == docenteId
                         select new ProyectoAcademicoDto()
                         {
                             IdProyectoAcademico=ua.IdProyectoAcademico,
                             Nombre=ua.Nombre,
                             CursoNombreEspanol = sua.NombreEspanol,
                             CursoNombreIngles = sua.NombreIngles,
                             IdCurso =sua.IdCurso,
                             Register=rcc.Register.Value,
                             Edit=rcc.Edit.Value

                         }).OrderBy(x => x.Nombre).ToList();


            foreach (ProyectoAcademicoDto pa in proyectos)
            {
                var Alumnos = (
                            from ua in context.AlumnoSeccion
                            join sua in context.ProyectoAcademico on ua.IdProyectoAcademico equals sua.IdProyectoAcademico
                            join am in context.AlumnoMatriculado on ua.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
                            join a in context.Alumno on am.IdAlumno equals a.IdAlumno
                            where ua.IdProyectoAcademico == pa.IdProyectoAcademico
                            select a).ToList()
                    ;

                foreach(Alumno a in Alumnos)
                {
                    pa.Alumnos += $"{a.Nombres} {a.Apellidos},";

                }

            }


                var selectCurso = (from ua in context.RubricaControlEvaluacion
                               join sua in context.AlumnoSeccion on ua.idAlumnoSeccion equals sua.IdAlumnoSeccion
                               join rc in context.RubricaControl on ua.idRubricaControl equals rc.idRubricaControl
                               where rc.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId                                                            
                               select sua.IdProyectoAcademico).ToList().Distinct();

            isQualified = new List<bool>();

            foreach(ProyectoAcademicoDto pa in proyectos)
            {
                if (selectCurso.Contains(pa.IdProyectoAcademico))
                {
                    isQualified.Add(true);
                }
                else
                {
                    isQualified.Add(false);
                }
            }

        }
    }
}