﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class LstNotificationTeachViewModel
    {
        public List<DocenteRow> LstDocente { get; set; }

        public void fill(CargarDatosContext dataContext, int idCurso)
        {
            var context = dataContext.context;
            this.LstDocente = new List<DocenteRow>();

            var sesion = dataContext.session;
            var docenteId = sesion.GetDocenteId();
            var subModalidadId = sesion.GetSubModalidadPeriodoAcademicoId();           
            var lenguage = dataContext.session.GetCulture().ToString();
            var LstDocenteProcd = context.GetListNotificationTeachRubric(docenteId,subModalidadId, idCurso, lenguage);
            foreach (var lst in LstDocenteProcd)
            {
                LstDocente.Add(new DocenteRow
                {
                    DocenteNombres = lst.Nombre,
                    DocenteCodigo=lst.Codigo,
                    DocenteCorreo=lst.Correo,
                    Curso= lst.NombreCurso
                });
            }
        }
    }
    public class DocenteRow
    {
        public string DocenteNombres { get; set; }
        public string DocenteCodigo { get; set; }
        public string DocenteCorreo { get; set; }
        public string Curso { get; set; }
        //public bool Notificado { get; set; }
    }
}