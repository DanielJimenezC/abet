﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class ConfigurationRubricViewModel
    {
        public IEnumerable<SelectListItem> LstCurso{ get; set; }

        public int idCurso { get; set; }

        public int idRubricControl { get; set; }

        public Int32? tipoRubrica { get; set; }

        public List<OptionSelect> lstTipoRubrica { get; set; }
        public string currentCulture { get; set; }
        public string[][] dataSettingRubric { get; set; }

        public void Fill(CargarDatosContext ctx) {

            lstTipoRubrica = new List<OptionSelect>();           

            var idEscuela = ctx.session.GetEscuelaId();
            var idDocente = ctx.session.GetDocenteId();
            idDocente = idDocente == null ? 0 : idDocente;
            var idSubmoda = ctx.session.GetSubModalidadPeriodoAcademicoId();

            var context = ctx.context;
            var cursoCreado = context.RubricaControl.Where(x => x.IsDeleted == false).Select(x => x.IdCurso).ToList();

            //var selectCurso = (from ua in context.UnidadAcademica
            //                join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
            //                join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
            //                join doc in context.Docente on uar.IdDocente equals doc.IdDocente
            //                join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
            //                join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
            //                join cmc in context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
            //                join mcd in context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
            //                where ua.IdSubModalidadPeriodoAcademico == idSubmoda && ua.IdEscuela == idEscuela && uar.IdDocente == idDocente
            //                && mcd.IdTipoOutcomeCurso == 1 && !cursoCreado.Contains(cur.IdCurso)
            //                select cur).ToList().Distinct().OrderBy(x => x.NombreEspanol);

            var selectCurso = (from ua in context.UnidadAcademica
                               join sua in context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                               join uar in context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                               join doc in context.Docente on uar.IdDocente equals doc.IdDocente
                               join cpa in context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                               join cur in context.Curso on cpa.IdCurso equals cur.IdCurso
                               where ua.IdSubModalidadPeriodoAcademico == idSubmoda && ua.IdEscuela == idEscuela && uar.IdDocente == idDocente
                               && !cursoCreado.Contains(cur.IdCurso)
                               select cur
                               ).ToList().Distinct().OrderBy(x => x.NombreEspanol);


            if (ctx.session.GetCulture().Equals(ConstantHelpers.CULTURE.ESPANOL))
            {               
                LstCurso = selectCurso.Select(x => new SelectListItem {
                    Value = x.IdCurso.ToString(),
                    Text = x.NombreEspanol
                });
                
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Parcial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }
            else {
                LstCurso = selectCurso.Select(x => new SelectListItem {
                    Value = x.IdCurso.ToString(),
                    Text = x.NombreIngles
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 1,
                    Label = "Partial"
                });
                lstTipoRubrica.Add(new OptionSelect()
                {
                    Value = 2,
                    Label = "Final"
                });
            }

        }

        public int CreateRubricControl(int idCurso, int idDocente, int submodalidaPA, int rubricType) {
            return RubricCourseLogic.CreateRubricControl( idCurso, idDocente , submodalidaPA, rubricType);
        }

        public int CreateRubriControlAcceptanceLevel(int idRubricControl, string[] dataAcceptanceLevel)
        {
            return RubricCourseLogic.CreateRubriControlAcceptanceLevel(idRubricControl, dataAcceptanceLevel);
        }

        public bool CreateRubricControlPregunta(string[][] dataSettingRubric, int idRubricControl, List<int> lstIdRubricControlAcceptanceLevel) {
            return RubricCourseLogic.CreateRubricControlPregunta(dataSettingRubric,idRubricControl, lstIdRubricControlAcceptanceLevel);
        }
    }
}