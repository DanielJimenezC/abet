﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse
{
    public class ShowTableStruct
    {
        public int idAlumno { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string carrera { get; set; }
        public int idSeccion { get; set; }
        public string seccion { get; set; }
        public int idCurso { get; set; }
        public string curso { get; set; }
        public bool isEvaluated { get; set; }
        public int rubricType { get; set; }
        
    }
    public class StudentListRubricViewModel
    {

        public List<Alumno> lstStudent{ get; set; }
        public IEnumerable<ShowTableStruct> listDatatable { get; set; }
       
        public void Fill(CargarDatosContext ctx, Int32? IdSection, Int32? IdCourse, Int32 rubricType) {           

            var context = ctx.context;
            var IsEvaluated = new List<int>();
            if (rubricType == 1)
            {
                IsEvaluated = (from rce in context.RubricaControlEvaluacion
                               join rc in context.RubricaControl on rce.idRubricaControl equals rc.idRubricaControl
                               where rc.IsFinal == false && rc.IdCurso == IdCourse && rce.idSeccion == IdSection
                               select rce.idAlumno).ToList();
            }
            else {
                IsEvaluated = (from rce in context.RubricaControlEvaluacion
                               join rc in context.RubricaControl on rce.idRubricaControl equals rc.idRubricaControl
                               where rc.IsFinal == true && rc.IdCurso == IdCourse && rce.idSeccion == IdSection
                               select rce.idAlumno).ToList();

            }

            lstStudent = (from al in context.Alumno
                          join alm in context.AlumnoMatriculado on al.IdAlumno equals alm.IdAlumno
                          join als in context.AlumnoSeccion on alm.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                          join sc in context.SeccionCurso on als.IdSeccion equals sc.IdSeccion                          
                          where als.IdSeccion == IdSection && sc.IdCurso == IdCourse
                          select al
                          ).ToList();


            var lstStudentEvaluated = lstStudent.Where(x => IsEvaluated.Contains(x.IdAlumno)).Select(x => x.IdAlumno).ToList();
            var codigoSeccion = (from sec in context.Seccion where sec.IdSeccion == IdSection select sec.Codigo).FirstOrDefault();
                        
            var nombreCurso = context.Curso.Where(x => x.IdCurso == IdCourse).Select(x => x.NombreEspanol).FirstOrDefault();

            listDatatable = lstStudent.Select(x => new ShowTableStruct {
                idAlumno = x.IdAlumno,
                codigo = x.Codigo,
                nombre = x.Nombres + " " + x.Apellidos,
                carrera = x.Carrera == null ?
                (from car in context.Carrera
                 join am in context.AlumnoMatriculado on car.IdCarrera equals am.IdCarrera
                 where am.IdAlumno == x.IdAlumno
                 select car.NombreEspanol).FirstOrDefault()
                 : x.Carrera.NombreEspanol,
                idSeccion = IdSection.ToInteger(),
                seccion = codigoSeccion,
                idCurso = IdCourse.ToInteger(),
                curso = nombreCurso,
                isEvaluated = lstStudentEvaluated.Contains(x.IdAlumno) ? true : false,
                rubricType = rubricType.ToInteger()
            });
        }
    }
}