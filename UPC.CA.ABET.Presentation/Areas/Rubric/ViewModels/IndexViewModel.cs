﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Data.Entity;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Shared;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class IndexViewModel
    {
        /*LISTAS PARA FILTROS*/
        public List<EmpresaVirtual> lstEmpresaVirtual { get; set; }
        public Int32? IdEmpresa { get; set; }
        public List<CustomCurseModel> lstCurso { get; set; }
        public Int32? IdCurso { get; set; }
        public List<PeriodoAcademico> lstPeriodoAcademico { get; set; }
        public Int32? IdPeriodoAcademico { get; set; }
        public List<SelectListItem> lstEstado { get; set; }
        public string codEstado { get; set; }


        /*
        public int[] SelectedComsiones { get; set; }
        public string PieSeries { get; set; }
        public string ColumnSeries { get; set; }
        public string ColumnSeries2 { get; set; }*/


        public IndexViewModel()
        {
            this.lstEmpresaVirtual = new List<EmpresaVirtual>();
            this.lstCurso = new List<CustomCurseModel>();
            this.lstPeriodoAcademico = new List<PeriodoAcademico>();
            this.lstEstado = new List<SelectListItem>();

        }

        public List<ListaProyectos_Result> CargarDatosRubrica(CargarDatosContext dataContext, IndexViewModel model)
        {
            var result = dataContext.context.ListaProyectos(model.IdCurso, model.IdPeriodoAcademico, model.IdEmpresa, model.codEstado).ToList();

            return result;
        }

        /*CARGAR DATOS HOMECONTROLLER /INDEX */
        public void CargarDatos(CargarDatosContext dataContext, IndexViewModel model)
        {

            var periodoAcademicoActivo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO));

            this.IdPeriodoAcademico = periodoAcademicoActivo.IdPeriodoAcademico;

            //DROPDOWN LSTCICLOS
            this.lstPeriodoAcademico = dataContext.context.PeriodoAcademico.ToList();

            //var RolesEvaludor = dataContext.context.TipoEvaluador.Where(x => x.IdPeriodoAcademico == periodoAcademicoActivo.IdPeriodoAcademico).ToList();
            //var docenteId = dataContext.session.GetDocenteId();
            //var ProyectosAcademics = dataContext.context.Evaluador.Where(x => /*x.IdDocente == docenteId && */ x.ProyectoAcademico.IdPeriodoAcademico == periodoAcademicoActivo.IdPeriodoAcademico).ToList();
            //var RubricasCalificadas = dataContext.context.RubricaCalificada.Where(x => /*x.Evaluador.IdDocente == docenteId  && */ x.Evaluador.ProyectoAcademico.IdPeriodoAcademico == periodoAcademicoActivo.IdPeriodoAcademico).ToList();
            //var TiposEvaluacion = ConstantHelpers.RUBRICFILETYPE.ListEvaluationProject();

            //TotalProyectos(RolesEvaludor, ProyectosAcademics);
            //ProyetosCalificados(RolesEvaludor, TiposEvaluacion, RubricasCalificadas);

            //this.lstEmpresaVirtual = dataContext.context.EmpresaVirtual.Where(x => x.IdPeriodoAcademico == this.IdPeriodoAcademico).OrderByDescending(x => x.Nombre).ToList();

            /*DROPDOWNLIST CURSOS*/
            this.lstCurso = dataContext.context.CursosEvaluarRubrica.Where(x => x.IdEscuela == 1 && x.IdSubModalidadPeriodoAcademicoModulo == this.IdPeriodoAcademico)
                .Select(x => new CustomCurseModel
                {
                    IdCurso = x.IdCurso,
                    NombreCompleto = x.Curso.NombreEspanol,
                    Abreviatura = x.Curso.NombreEspanol
                }).ToList();

            /*DROPDOWNLIST ESTADOS*/
            this.lstEstado.Add(new SelectListItem { Value = "LPC", Text = "LISTOS PARA COMITÉ" });
            this.lstEstado.Add(new SelectListItem { Value = "PPC1", Text = "EN PROCESO DE CALIFICACIÓN CUALITATIVA" });
            this.lstEstado.Add(new SelectListItem { Value = "FIN", Text = "FINALIZADOS" });
            this.lstEstado.Add(new SelectListItem { Value = "PPC2", Text = "EN PROCESO DE CALIFICACIÓN CUANTITATIVA" });
        }



        /*
        public void TotalProyectos(List<TipoEvaluador> RolesEvaluador, List<Evaluador> EvaludoresProyecto)
        {
            List<HighChartsSeries> allSeries = new List<HighChartsSeries>();
            List<HighChartsPoint> allPoint = new List<HighChartsPoint>();

            var totalProyectos = EvaludoresProyecto.Count;
            foreach (var rol in RolesEvaluador)
            {
                var sumaProyectosRol = EvaludoresProyecto.Where(x => x.TipoEvaluador.TipoEvaluadorMaestra.Codigo == rol.TipoEvaluadorMaestra.Codigo 
                && x.ProyectoAcademico.AlumnoSeccion.Any(y=>y.AlumnoMatriculado.EstadoMatricula.ToUpper().Equals("MRE")))
                .Select(x => x.ProyectoAcademico).Sum(x => x.AlumnoSeccion.Count);

                //var sumaProyectosRol = EvaludoresProyecto.Count(x => x.TipoEvaluador.Codigo == rol.Codigo);
                allPoint.Add(new HighChartsPoint { y = sumaProyectosRol, name = rol.TipoEvaluadorMaestra.Nombre });
            }

            allSeries.Add(new HighChartsSeries
            {
                data = new List<HighChartsPoint>(allPoint),
                name = OptionResource.NameTotalProyectos,
                type = "pie"
            });

            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            PieSeries = oSerializer.Serialize(allSeries);
        }

        public void ProyetosCalificados(List<TipoEvaluador> RolesEvaluador, List<String> TiposEvaluacion, List<RubricaCalificada> RubricasCalificadas)
        {
            List<HighChartsSeriesColumn> allSeries = new List<HighChartsSeriesColumn>();

            List<String> lstStatus = new List<String>();
            lstStatus.Add("CALIFICADOS");
            lstStatus.Add("NO CALIFICADOS");

            foreach (var status in lstStatus)
            {
                var cont = 0;
                int indice = 0;
                int[] arrayCantidadCalificadas = new int[RolesEvaluador.Count];
                foreach (var evaluacion in RolesEvaluador)
                {
                    //cont = RubricasCalificadas.Count(x => x.Evaluador.TipoEvaluador.Codigo == rol.Codigo && x.Rubrica.Evaluacion.TipoEvaluacion.Tipo == evaluacion);
                    arrayCantidadCalificadas[indice] = 18;
                    indice++;
                }
                allSeries.Add(new HighChartsSeriesColumn
                {

                    data = arrayCantidadCalificadas,
                    name = status,
                    type = "column"
                });
                
            }


            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            ColumnSeries = oSerializer.Serialize(allSeries);
            

            List<HighChartsSeriesColumn> allSeries2 = new List<HighChartsSeriesColumn>();
            foreach (var status in lstStatus)
            {
                var cont = 0;
                int indice = 0;
                int[] arrayCantidadCalificadas = new int[3];
                foreach (var evaluacion in arrayCantidadCalificadas)
                {
                    //cont = RubricasCalificadas.Count(x => x.Evaluador.TipoEvaluador.Codigo == rol.Codigo && x.Rubrica.Evaluacion.TipoEvaluacion.Tipo == evaluacion);
                    arrayCantidadCalificadas[indice] = 18;
                    indice++;
                }
                allSeries2.Add(new HighChartsSeriesColumn
                {

                    data = arrayCantidadCalificadas,
                    name = status,
                    type = "column"
                });

            }
            
            ColumnSeries2 = oSerializer.Serialize(allSeries2);
        }

    }
   

    public class HighChartsPoint
    {
        public double x { set; get; }
        public double y { set; get; }
        public string color { set; get; }
        public string id { set; get; }
        public string name { set; get; }
    }

    public class HighChartsPointColumn
    {
        public double x { set; get; }
        public int y { set; get; }
        public string color { set; get; }
        public string id { set; get; }
        public string name { set; get; }
    }

    public class HighChartsSeries
    {
        public List<HighChartsPoint> data { set; get; }
        public string name { set; get; }
        public string type { set; get; }
    }

    public class HighChartsSeriesColumn
    {
        public int[] data { set; get; }
        public string name { set; get; }
        public string type { set; get; }
    }
     */
    }
}