﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Data.Entity;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class ConfigCommentViewModel
    {
        public int IdCicloActual { get; set; }
        public List<String> Evaluadores { get; set; }
        public List<List<bool>> Selected { get; set; }

        public ConfigCommentViewModel()
        {
            this.Evaluadores = new List<String>();
        }

        public void CargarDatos(AbetEntities context, ConfigCommentViewModel model)
        {
            var periodoAcademicoActivo = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO));
            this.IdCicloActual = periodoAcademicoActivo.IdPeriodoAcademico;

            this.Evaluadores = (from e in context.Evaluador
                                join te in context.TipoEvaluador on e.IdTipoEvaluador equals te.IdTipoEvaluador
                                where (te.IdTipoEvaluador == IdCicloActual)
                                select e.Docente.Nombres + " " + e.Docente.Apellidos).Distinct().ToList();

            this.Selected = new List<List<bool>>();

            foreach(var eval in this.Selected){
                List<bool> capitulos = new List<bool>();
                for (int i = 0; i < 7; i++)
                {
                    capitulos.Add(false);
                }
                this.Selected.Add(capitulos);
            }
        }

    }
}