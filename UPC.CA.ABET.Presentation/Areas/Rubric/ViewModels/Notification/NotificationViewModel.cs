﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Data.Entity;
//using Newtonsoft.Json;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Shared;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using System.Data.SqlClient;
using System.Transactions;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class NotificationViewModel
    {
        /*CONFIGURACION DEL MENSAJE*/

        public String asunto { get; set; }
        public String mensaje { get; set; }
        

        public List<EmpresaVirtual>  ListEmpresaVirtual { get; set; }
        public List<CustomCurseModel> LstCurso { get; set; }
        public List<TipoEvaluadorCombobox> ListRoles { get; set; }

        public List<SelectListItem> LstEstados { get; set; }

        public int?  IdEmpresa { get; set; }
        public int? IdCurso { get; set; }
        public string CodigoRol { get; set; }
        //public string CodCurso { get; set; }
        //public int? IdTipoEvaluadorMaestro { get; set; }
        public string Estado { get; set; }

        public void CargarDatos(CargarDatosContext dataContext, int? IdPeriodoAcademico, int? idEscuela)
        {
            LstEstados = new List<SelectListItem>();
            LstEstados.Add(new SelectListItem { Text = "CALIFICADO", Value = "CALIFICADO" });
            LstEstados.Add(new SelectListItem { Text = "NOCALIFICADO", Value = "NOCALIFICADO" });
            


            var objMensaje = dataContext.context.Mensaje.Where(x => x.idEscuela == idEscuela).FirstOrDefault();
            this.mensaje = (objMensaje == null) ? "" : objMensaje.Mensaje1;
            this.asunto = (objMensaje == null) ? "" : objMensaje.Asunto;

            this.ListEmpresaVirtual = dataContext.context.EmpresaVirtual.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico && x.IdEscuela == idEscuela)
                   .OrderByDescending(x => x.Nombre).ToList();


            //VARIABLESTEMPORALES
            int idSubModalidad = 1;
            int idmodulo = 8;

            var dataCursosEvaluar = dataContext.context.GETCURSOSRUBRICAS(idSubModalidad, idmodulo, IdPeriodoAcademico, idEscuela).ToList();
            this.LstCurso = new List<CustomCurseModel>();
            LstCurso.AddRange(dataCursosEvaluar.Select(x => new CustomCurseModel
            {
                IdCurso = x.IdCurso,
                NombreCompleto = x.NombreEspanol,
                Abreviatura = x.NombreEspanol,
                Codigo = x.Codigo
            }));

            this.ListRoles = dataContext.context.TipoEvaluadorMaestra.Where(x=> x.IdEscuela == idEscuela).
                Select(x => new TipoEvaluadorCombobox
                    {
                        idTipoEvaluador = x.idTipoEvaluador,
                        nombre = x.Nombre,
                        codigoRol = x.Codigo
                    }).ToList();

        }
       



    }

    public class EvaluadorRow
    {
        public string DocenteCodigo { get; set; }
        public string DocenteNombres { get; set; }
        public string TipoEvaluadorNombre { get; set; }
        public string EmpresaNombre { get; set; }
        public string TipoCurso { get; set; }
    }
    
}