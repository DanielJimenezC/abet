﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Data.Entity;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Shared;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using System.Data.SqlClient;
using System.Transactions;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class NotificationPartialEvaluadorViewModel
    {
        //public List<EvaluadorRow> lstEvaluadores { get; set; }
        public List<SP_LIST_ESTADO_CALIFICACION_EVALUACION_POR_ALUMNO_Result> lstEvaluadores { get; set; }


        public void CargarDatos(CargarDatosContext dataContext, int? idPeriodoAcademico, int? idEscuela,
            int? IdEmpresa, int? idCurso, string codigoRol, string Estado)
        {
            int idSubModalidadPeriodoAcademico = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idSubModalidadPeriodoAcademicoModulo = dataContext.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

           lstEvaluadores= dataContext.context.SP_LIST_ESTADO_CALIFICACION_EVALUACION_POR_ALUMNO(
                idSubModalidadPeriodoAcademicoModulo,
                idCurso,
                codigoRol.Equals("") == true ? null : codigoRol,
                IdEmpresa,
                Estado.Equals("")==true?null:Estado
                ).ToList() ;

            /*
            lstEvaluadores = dataContext.context.Evaluador.Where(
                x => x.ProyectoAcademico.IdPeriodoAcademico == PeriodoAcademicoId
                && x.TipoEvaluador.TipoEvaluadorMaestra.IdEscuela == escuelaId
                && x.ProyectoAcademico.EmpresaVirtual.IdEmpresa == (IdEmpresa == 0? x.ProyectoAcademico.EmpresaVirtual.IdEmpresa: IdEmpresa)
                && x.TipoEvaluador.Curso.Codigo == (CodCurso.Equals("")==true? x.TipoEvaluador.Curso.Codigo : CodCurso)
                && x.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == (IdTipoEvaluadorMaestro==0 ? x.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador : IdTipoEvaluadorMaestro)

                ).Select(x => new EvaluadorRow
            {
                DocenteCodigo = x.Docente.Codigo,
                DocenteNombres = x.Docente.Nombres,
                TipoEvaluadorNombre = x.TipoEvaluador.TipoEvaluadorMaestra.Nombre,
                EmpresaNombre = x.ProyectoAcademico.EmpresaVirtual.Nombre,
                TipoCurso = x.ProyectoAcademico.Curso.NombreEspanol

            }).Distinct().ToList();*/

        }
        


    }
}