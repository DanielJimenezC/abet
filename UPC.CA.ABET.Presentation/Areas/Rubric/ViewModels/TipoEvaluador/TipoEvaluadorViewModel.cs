﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Data.Entity;
//using Newtonsoft.Json;
using System.Web.Script.Serialization;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Shared;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class TipoEvaluadorViewModel
    {
        public List<TipoEvaluadorMaestra> lstCodigoRoles { get; set; }
        public List<GETCURSOSRUBRICAS_Result> lstCursosEvaluar { get; set; }

        public TipoEvaluador tipoEvaluador { get; set; }

        public string codCurso { get; set; }

        public TipoEvaluadorViewModel()
        {
            this.lstCodigoRoles = new List<TipoEvaluadorMaestra>();
            this.lstCursosEvaluar = new List<GETCURSOSRUBRICAS_Result>();
            this.tipoEvaluador = new TipoEvaluador();

        }

        public void CargarDatos(CargarDatosContext dataContext,int? idPeriodoAcademico, int? idEscuela)
        {
          

            int idsubmoda = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidad;
            
             this.lstCursosEvaluar= dataContext.context.GETCURSOSRUBRICAS(idsubmoda, 0,idPeriodoAcademico,idEscuela).ToList();
             this.lstCodigoRoles = dataContext.context.TipoEvaluadorMaestra.Where(x=>x.IdEscuela==idEscuela).ToList();
            

        }
    }
        
}