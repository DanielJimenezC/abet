﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class RubricWASCViewModel
    {
        #region ATRIBUTOS
        public Int32 IdRubrica { get; set; }
        public String CarreraNombre { get; set; }
        public String CursoNombre { get; set; }
        public String EvaluacionNombre { get; set; }
        public List<OutcomeRubrica> Outcomes { get; set; }
        public List<NivelDesempenio> NivelesDesempenio { get; set; }
        #endregion

        public void Fill(CargarDatosContext ctx, RubricViewModel _Rubrica)
        {
            try
            {
                var context = ctx.context;
                Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == _Rubrica.IdEvaluacion);
                IdRubrica = Rubrica.IdRubrica;
                CarreraNombre = context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == _Rubrica.IdCarrera).Carrera.NombreEspanol;
                CursoNombre = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == _Rubrica.IdCurso).CursoPeriodoAcademico.Curso.NombreEspanol;
                EvaluacionNombre = context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;

                Outcomes = Rubrica.OutcomeRubrica.ToList();

                var RubricaNivelDesempenio = Rubrica.RubricaNivelDesempenio;
                if (RubricaNivelDesempenio.Any())
                    NivelesDesempenio = RubricaNivelDesempenio.Select(x => x.NivelDesempenio).ToList();
                else
                {
                    var IdCiclo = Rubrica.Evaluacion.TipoEvaluacion.IdSubModalidadPeriodoAcademico;
                    NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("Competences") && x.IdPeriodoAcademico == IdCiclo).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}