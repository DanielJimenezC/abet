﻿using System;
using System.Collections.Generic;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class OutcomeViewModel
    {
        public int IdOutcome { get; set; }
        public String NombreOutcome { get; set; }
        public string DescripcionOutcome { get; set; }
        public Double NotaOutcome { get; set; }
        public List<CriterioViewModel> Criterios { get; set; }
        public float MaxValueLastCriterio { get; set; }
        public List<bool> Matchs { get; set; }
        public String NombreComision{ get; set; }
    }
}