﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.PartialView
{
    public class ListOutcomeConfigureViewModel
    {
        public List<ConfiguredOutcomeViewModel> ConfiguredOutcomes { get; set; }
        
        public void Fill(CargarDatosContext ctx, Int32? IdCarrera, Int32? IdCurso, Int32? IdComision)
        {
           
            try
            {
                var context = ctx.context;
                var currentCulture = ctx.session.GetCulture().ToString();
                var IdSubModalidad = ctx.session.GetSubModalidadPeriodoAcademicoId();
                ConfiguredOutcomes = new List<ConfiguredOutcomeViewModel>();

                var Comisiones = context.CarreraComision.Where(x => x.IdCarrera == IdCarrera && x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == IdSubModalidad).Select(x => x.Comision);
                if (!Comisiones.Any())
                    return;

                var MallaCurricular = context.MallaCurricularPeriodoAcademico.FirstOrDefault(x => x.MallaCurricular.IdCarrera == IdCarrera && x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == IdSubModalidad);
                if (MallaCurricular == null)
                    return;

                var CursoMallaCurricular = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == IdCurso && x.IdMallaCurricular == MallaCurricular.IdMallaCurricular);
                if (CursoMallaCurricular == null)
                    return;

                var MallaCocos = context.MallaCocos.FirstOrDefault(x => x.CarreraComision.IdCarrera == IdCarrera && x.CarreraComision.IdComision == IdComision);
                var MallaCocosDetalle = context.MallaCocosDetalle.Where(x => x.IdMallaCocos == MallaCocos.IdMallaCocos && x.IdCursoMallaCurricular == CursoMallaCurricular.IdCursoMallaCurricular);

                ConfiguredOutcomes = MallaCocosDetalle.Select(x => new ConfiguredOutcomeViewModel
                {
                    IdMallaCocosDetalle = x.IdMallaCocosDetalle,
                    CodigoComision = x.OutcomeComision.Comision.Codigo,
                    NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                    Descripcion = (currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.OutcomeComision.Outcome.DescripcionEspanol : x.OutcomeComision.Outcome.DescripcionIngles),
                    EsCalificado = (x.EsCalificado == null ? false : x.EsCalificado.Value)
                }).ToList();
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }

    public class ConfiguredOutcomeViewModel
    {
        public Int32 IdMallaCocosDetalle { get; set; }
        public String CodigoComision { get; set; }
        public String NombreOutcome { get; set; }
        public String Descripcion { get; set; }
        public Boolean EsCalificado { get; set; }
    }
}