﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.PartialView
{
    public class ListRubricViewModel
    {
        public List<Rubrica> lstRubricas { get; set; }
        public void Fill(CargarDatosContext ctx, Int32? IdCiclo, Int32? IdCarrera)
        {
            var context = ctx.context;

            if(IdCarrera.HasValue)
            {
                if(IdCarrera.Value > 0)
                {
                    lstRubricas = context.Rubrica.Where(x => x.Evaluacion.TipoEvaluacion.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && x.Evaluacion.CarreraCursoPeriodoAcademico.IdCarrera == IdCarrera).ToList();
                    return;
                }
            }

            var idSubModalidadPeriodoAcademico = context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdCiclo).IdSubModalidadPeriodoAcademico;
            //var idSubModalidadPeriodoAcademicoModulo = context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).IdSubModalidadPeriodoAcademicoModulo;


            //var idSubModalidadPeriodoAcademicoModulo = context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            var IdEscuela = ctx.session.GetEscuelaId();
            lstRubricas = context.Rubrica.Where(x => x.Evaluacion.TipoEvaluacion.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && x.Evaluacion.CarreraCursoPeriodoAcademico.Carrera.IdEscuela == IdEscuela)
                            .OrderBy(x => x.Evaluacion.CarreraCursoPeriodoAcademico.Carrera.NombreEspanol)
                            .ThenByDescending(x => x.Evaluacion.TipoEvaluacion.Tipo).ToList();
        }
    }
}