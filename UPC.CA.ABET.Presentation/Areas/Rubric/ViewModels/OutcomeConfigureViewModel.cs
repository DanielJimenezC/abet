﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class OutcomeConfigureViewModel
    {
        #region Atributos
        public Int32 IdCarrera { get; set; }
        public Int32 IdCurso { get; set; }
        public Int32 IdComision { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }
        public List<Comision> lstComision { get; set; }
        #endregion

        #region Propiedades
        public void Fill(CargarDatosContext ctx, int parIdSubModalidadPeriodoAcademicoActivo)
        {
            try
            {
                var context = ctx.context;
                var IdEscuela = ctx.session.GetEscuelaId();

                //lstCarrera = context.Carrera.Where(x => x.IdEscuela == IdEscuela ).ToList();

                lstCarrera = (from ca in context.Carrera
                              join capa in context.CarreraPeriodoAcademico on ca.IdCarrera equals capa.IdCarrera
                              where (capa.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademicoActivo && ca.IdEscuela == IdEscuela)
                              select ca
                    ).ToList();

                lstCurso = new List<Curso>();
                lstComision = new List<Comision>();
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}