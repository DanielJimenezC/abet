﻿
#region Imports

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.BulkInsert;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.BulkInsert
{
    public class UploadExcelFilesViewModel
    {
        #region Propiedades

        [Required]
        public HttpPostedFileBase Archivo { get; set; }
        public BulkInsertType TipoCargaMasiva { get; set; }
        public String TituloPagina { get; set; }
        public String Icono { get; set; }
        public Dictionary<String, String> DicMensajes { get; set; }



        /*ROLES DISPONIBLES */
        public IEnumerable<TipoEvaluadorMaestra> lstTipoEvaluadorMaestra { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de la página correspondientes al tipo de carga masiva los cuales 
        /// son determinados según el tipo de carga masiva elegida en el sidebar principal,
        /// como también los mensajes de validaciones correspondientes almacenados en un 
        /// tipo de dato diccionario.
        /// </summary>
        /// <param name="TipoCargaMasiva">Enumerador que determina el tipo de carga masiva</param>
        /// <returns></returns>
        public void CargarDatos(BulkInsertType TipoCargaMasiva)
        {
            this.TipoCargaMasiva = TipoCargaMasiva;
            DicMensajes = new Dictionary<String, String>();
            DicMensajes.Add("HeadersInvalidos", UploadFilesResource.HeadersInvalidos);

            switch (TipoCargaMasiva)
            {
                case BulkInsertType.VirtualCompany:
                    TituloPagina = "5."+UploadFilesResource.TituloEmpresa;
                    Icono = "plus-building";

                    DicMensajes.Add("CodigosRepetidosEmpresas", VirtualCompanyBulkInsertResource.CodigosRepetidosEmpresas);
                    DicMensajes.Add("CodigosBaseRepetidosEmpresas", VirtualCompanyBulkInsertResource.CodigosBaseRepetidosEmpresas);
                    DicMensajes.Add("NombresBaseRepetidosEmpresas", VirtualCompanyBulkInsertResource.NombresBaseRepetidosEmpresas);
                    DicMensajes.Add("NombresRepetidosEmpresas", VirtualCompanyBulkInsertResource.NombresRepetidosEmpresas);
                    break;
                case BulkInsertType.AcademicProject:
                    TituloPagina = "7."+UploadFilesResource.TituloProyectoAcademico;
                    Icono = "plus-book";

                    DicMensajes.Add("CodigoRepetidosProyecto", AcademicProjectBulkInsertResource.CodigoRepetidosProyecto);
                    DicMensajes.Add("CodigosBaseRepetidosProyecto", AcademicProjectBulkInsertResource.CodigosBaseRepetidosProyecto);
                    DicMensajes.Add("NombresBaseRepetidosProyecto", AcademicProjectBulkInsertResource.NombresBaseRepetidosProyecto);
                    DicMensajes.Add("NombresRepetidosProyecto", AcademicProjectBulkInsertResource.NombresRepetidosProyecto);
                    DicMensajes.Add("CodigosEmpresaNoExistentes", AcademicProjectBulkInsertResource.CodigosEmpresaNoExistentes);
                    DicMensajes.Add("CantidadAlumnos", AcademicProjectBulkInsertResource.CantidadAlumnos);
                    DicMensajes.Add("CodigosAlumnosRepetidos", AcademicProjectBulkInsertResource.CodigosAlumnosRepetidos);
                    DicMensajes.Add("AlumnosSinMatricula", AcademicProjectBulkInsertResource.AlumnosSinMatricula);
                    DicMensajes.Add("AlumnosSinSeccion", AcademicProjectBulkInsertResource.AlumnosSinSeccion);
                    DicMensajes.Add("AlumnosConProyecto", AcademicProjectBulkInsertResource.AlumnosConProyecto);
                    DicMensajes.Add("CursosNoExistentes", AcademicProjectBulkInsertResource.CursosNoExistentes);
                    DicMensajes.Add("TipoEvaluacionNoCargadas", AcademicProjectBulkInsertResource.TipoEvaluacionNoCargadas);
                    break;
                case BulkInsertType.EvaluationType:
                    TituloPagina = "3."+UploadFilesResource.TituloTipoEvaluacion;
                    Icono = "plus-book";

                    DicMensajes.Add("CodigosTipoEvaluacionRepetidos", EvaluationTypeResource.CodigosTipoEvaluacionRepetidos);
                    DicMensajes.Add("CodigosBaseRepetidosTipoEvaluacion", EvaluationTypeResource.CodigosBaseRepetidosTipoEvaluacion);
                    break;
                case BulkInsertType.Evaluation:
                    TituloPagina = "4."+UploadFilesResource.TituloEvaluacion;
                    Icono = "plus-book";

                    DicMensajes.Add("CursoEvaluacionRepetida", EvaluationResource.CursoEvaluacionRepetida);
                    DicMensajes.Add("CursoPesoDiferenteUno", EvaluationResource.CursoPesoDiferenteUno);
                    DicMensajes.Add("CarrerasNoExistentes", EvaluationResource.CarrerasNoExistentes);
                    DicMensajes.Add("TiposEvaluacionNoExistentes", EvaluationResource.TiposEvaluacionNoExistentes);
                    DicMensajes.Add("CursosNoExistentes", EvaluationResource.CursosNoExistentes);
                    DicMensajes.Add("CursosConEvaluaciones", EvaluationResource.CursosConEvaluaciones);
                    break;
                case BulkInsertType.EvaluatorType:
                    TituloPagina = "2.Carga Masiva de Roles de Evaluador por Curso";
                    Icono = "plus-book";

                    DicMensajes.Add("CodigosRepetidosTP1", EvaluatorTypeResource.CodigosRepetidosTP1);
                    DicMensajes.Add("CodigosRepetidosTP2", EvaluatorTypeResource.CodigosRepetidosTP2);
                    DicMensajes.Add("NoExisteRolCreado", EvaluatorTypeResource.NoExisteRolCreado);
                    DicMensajes.Add("DebeAsignarEsRolComiteTP1", EvaluatorTypeResource.DebeAsignarEsRolComiteTP1);
                    DicMensajes.Add("DebeAsignarEsRolComiteTP2", EvaluatorTypeResource.DebeAsignarEsRolComiteTP2);
                    DicMensajes.Add("SoloUnoAsignarEsRolComiteTP1", EvaluatorTypeResource.SoloUnoAsignarEsRolComiteTP1);
                    DicMensajes.Add("SoloUnoAsignarEsRolComiteTP2", EvaluatorTypeResource.SoloUnoAsignarEsRolComiteTP2);
                    DicMensajes.Add("RolesExistentes", EvaluatorTypeResource.RolesExistentes);

                    break;
                case BulkInsertType.GrupoComite:
                    TituloPagina = "6.GRUPO COMITÉ PARA EVALUAR RÚBRICA TRABAJO FINAL";
                    Icono = "plus-book";

                    break;
                case BulkInsertType.EvaluatorTypeMaster:
                    TituloPagina = "Carga Masiva Roles de Evaluadores";
                    Icono = "plus-book";

                    DicMensajes.Add("CodigosRepetidosTiposEvaluador", EvaluatorTypeResource.CodigosRepetidosTiposEvaluador);                   
                    DicMensajes.Add("TiposEvaluadorDeBase", EvaluatorTypeResource.TiposEvaluadorDeBase);
                    DicMensajes.Add("NombresRepetidosTiposEvaluador", EvaluatorTypeResource.NombresRepetidosTiposEvaluador);


                    /*DicMensajes.Add("CodigosRepetidosTiposEvaluador", EvaluatorTypeResource.CodigosRepetidosTiposEvaluador);
                    DicMensajes.Add("SumaPesoDiferenteDeCero", EvaluatorTypeResource.SumaPesoDiferenteDeCero);
                    DicMensajes.Add("TiposEvaluadorDeBase", EvaluatorTypeResource.TiposEvaluadorDeBase);
                    DicMensajes.Add("NombresRepetidosTiposEvaluador", EvaluatorTypeResource.NombresRepetidosTiposEvaluador);*/
                    break;
                case BulkInsertType.CoursesToEvaluation:
                    TituloPagina = "1.Carga Masiva de Cursos a Evaluar";
                    Icono = "plus-book";
                    break;
                case BulkInsertType.Evaluator:
                    TituloPagina = UploadFilesResource.TituloEvaluador;
                    Icono = "plus-book";

                    DicMensajes.Add("DocentesNoExistentes", EvaluatorResource.DocentesNoExistentes);
                    DicMensajes.Add("ProyectosNoExistentes", EvaluatorResource.ProyectosNoExistentes);
                    DicMensajes.Add("TipoEvaluadorNoExistente", EvaluatorResource.TipoEvaluadorNoExistente);
                    DicMensajes.Add("ProyectosFaltaTipoEvaluador", EvaluatorResource.ProyectosFaltaTipoEvaluador);
                    DicMensajes.Add("ProyectosDocentesRepetidos", EvaluatorResource.ProyectosDocentesRepetidos);
                    DicMensajes.Add("ProyectosConDocentes", EvaluatorResource.ProyectosConDocentes);
                    break;
                case BulkInsertType.OutcomeGoalCommission:
                    TituloPagina = UploadFilesResource.TituloOutcomeComision;
                    Icono = "plus-book";

                    DicMensajes.Add("OutcomesNoExistentes", OutcomeGoalCommissionResource.OutcomesNoExistentes);
                    DicMensajes.Add("OutcomesNoExistentesEnComision", OutcomeGoalCommissionResource.OutcomesNoExistentesEnComision);
                    DicMensajes.Add("OutcomesRepetidos", OutcomeGoalCommissionResource.OutcomesRepetidos);
                    DicMensajes.Add("OutcomeYaRegistrado", OutcomeGoalCommissionResource.OutcomeYaRegistrado);
                    break;
                case BulkInsertType.RubricToEvaluation:
                    TituloPagina = "8. Carga Masiva de Rubrícas Evaluadas";
                    Icono = "plus-book";
                    break;
            }
        }

        #endregion
    }
}
