﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.BulkInsert
{
    public class StatusReportViewModel
    {
        public String Codigo { get; set; }
        public String Nombre { get; set; }
        public String Docente { get; set; }
        public String Evaluador { get; set; }
        public String TipoCurso { get; set; }
    }
}