﻿#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.BulkInsert
{
    public class OutcomeReportViewModel
    {
        #region Propiedades

        public Int32 IdPeriodoAcademico { get; set; }
        public Int32 IdCarrera { get; set; }
        public Int32 IdCurso { get; set; }
        public string Prueba { get; set; }

        public List<PeriodoAcademico> lstPeriodoAcademico { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        public List<Curso> lstCurso { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga todos los periodos académicos existentes y los cursos de proyectos
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, int? IdEscuela)
        {
            var context = DataContext.context;
            var IdCiclo = DataContext.session.GetPeriodoAcademicoId();

            lstPeriodoAcademico = (from submoda in context.SubModalidadPeriodoAcademico
                                   join pa in context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                   where (submoda.IdSubModalidad == 1)
                                   select pa).ToList();
            lstCarrera = context.CarreraPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && x.Carrera.IdEscuela == IdEscuela).Select(x => x.Carrera).ToList();
            lstCurso = context.CursosEvaluarRubrica.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && x.IdEscuela == IdEscuela).Select(x => x.Curso).ToList();
        }

        #endregion
    }
}
