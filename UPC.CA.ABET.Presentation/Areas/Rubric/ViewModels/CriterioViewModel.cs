﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class CriterioViewModel
    {
        public int IdOutcome { get; set; }
        public int IdCriterio { get; set; }
        public string NombreCriterio { get; set; }
        public string DescripcionCriterio { get; set; }
        public Decimal NotaMaxima { get; set; }
        public Decimal? NotaMinima { get; set; }
        public bool EsEvaluadoPorDefecto { get; set; }
        public List<NivelViewModel> Niveles { get; set; }
        public bool SePuedeAgregar { get; set; }

        public CriterioViewModel() { Niveles = new List<NivelViewModel>(); }
        public CriterioViewModel(int IdOutcome, int IdCriterio)
        {
            this.IdOutcome = IdOutcome;
            this.IdCriterio = IdCriterio;
            this.SePuedeAgregar = true;
        }
        public CriterioViewModel(int IdOutcome)
        {
            this.IdOutcome = IdOutcome;
            this.SePuedeAgregar = true;
        }
        public CriterioViewModel(int IdOutcome, string NombreCriterio, string DescripcionCriterio, float PuntajeCriterio)
        {
            this.IdOutcome = IdOutcome;
            this.NombreCriterio = NombreCriterio;
            this.DescripcionCriterio = DescripcionCriterio;
            this.NotaMaxima = (Decimal)PuntajeCriterio;
        }

        public CriterioViewModel ConvertFromHelperEntity(CriterioEntity helperEntity)
        {
            CriterioViewModel criterio = new CriterioViewModel();

            criterio.IdCriterio = helperEntity.IdCriterio;
            criterio.IdOutcome = helperEntity.IdOutcome;
            criterio.NombreCriterio = helperEntity.NombreCriterio;
            criterio.NotaMaxima = helperEntity.NotaMaxima;
            criterio.NotaMinima = helperEntity.NotaMinima;
            criterio.DescripcionCriterio = helperEntity.DescripcionCriterio;
            criterio.EsEvaluadoPorDefecto = helperEntity.EsEvaluadoPorDefecto;
            return criterio;
        }

        public List<CriterioViewModel> AddListCriterios(CargarDatosContext dataContext, CriterioViewModel Criterio)
        {
            CriterioOutcome nuevoCriterio = new CriterioOutcome();
            nuevoCriterio.IdOutcomeRubrica = Criterio.IdOutcome;
            nuevoCriterio.Nombre = Criterio.NombreCriterio;
            nuevoCriterio.Descripcion = Criterio.DescripcionCriterio;
            nuevoCriterio.ValorMaximo = Criterio.NotaMaxima;
            nuevoCriterio.EsEvaluadoPorDefecto = true;

            var criterios = RubricLogic.AddListCriterio(dataContext.context, nuevoCriterio);

            NivelViewModel viewModel = new NivelViewModel();
            var ListNiveles = viewModel.AddDefaulltNiveles(dataContext, nuevoCriterio.IdCriterioOutcome);

            var criteriosViewModel = criterios.Where(x => x.IdOutcomeRubrica == Criterio.IdOutcome).Select(x => new CriterioViewModel
            {
                IdCriterio = x.IdCriterioOutcome,
                NombreCriterio = x.Nombre,
                DescripcionCriterio = x.Descripcion,
                IdOutcome = x.IdOutcomeRubrica,
                NotaMaxima = x.ValorMaximo,
                EsEvaluadoPorDefecto = x.EsEvaluadoPorDefecto,
                Niveles = x.NivelCriterio.Select(y => new NivelViewModel
                {
                    IdCriterio = x.IdCriterioOutcome,
                    IdNivel = y.IdNivelCriterio,
                    NombreNivel = y.Nombre,
                    NivelNotaMinima = y.NotaMenor,
                    NivelNotaMaxima = y.NotaMayor,
                    DescripcionNivel = y.DescripcionNivel
                }).ToList()
            }).ToList();

            foreach (var item in criteriosViewModel)
            {
                var max = item.Niveles.Count > 0 ? item.Niveles.OrderByDescending(x => x.NivelNotaMaxima).FirstOrDefault().NivelNotaMaxima : -1;
                if (max != -1)
                {
                    if (max == item.NotaMaxima)
                    {
                        item.SePuedeAgregar = false;
                    }
                    else
                    {
                        item.SePuedeAgregar = true;
                    }
                }
            }
            return criteriosViewModel;
        }
        public List<CriterioViewModel> DeleteListCriterios(CargarDatosContext dataContext, CriterioViewModel Criterio)
        {
            var Criterios = RubricLogic.DeleteCriterioById(dataContext.context, Criterio.IdCriterio);

            return Criterios.Where(x => x.IdOutcomeRubrica == Criterio.IdOutcome).Select(x => new CriterioViewModel
            {
                IdCriterio = x.IdCriterioOutcome,
                NombreCriterio = x.Nombre,
                DescripcionCriterio = x.Descripcion,
                IdOutcome = x.IdOutcomeRubrica,
                NotaMaxima = x.ValorMaximo,
                Niveles = x.NivelCriterio.Select(y => new NivelViewModel
                {

                    IdCriterio = x.IdCriterioOutcome,
                    IdNivel = y.IdNivelCriterio,
                    NombreNivel = y.Nombre,
                    NivelNotaMinima = y.NotaMenor,
                    NivelNotaMaxima = y.NotaMayor,
                    DescripcionNivel = y.DescripcionNivel
                }).ToList()
            }).ToList();
        }

        public List<CriterioViewModel> DeleteListCriteriosParticipacion(CargarDatosContext dataContext, CriterioViewModel Criterio)
        {
            List<CriterioOutcome> listadoCriteriosOutcome = dataContext.context.CriterioOutcome.Where(x => x.IdOutcomeRubrica == Criterio.IdOutcome).ToList();

            foreach (var item in listadoCriteriosOutcome)
            {
                RubricLogic.DeleteCriterioById(dataContext.context, item.IdCriterioOutcome);
            }

            return new List<CriterioViewModel>();
        }

    }
}