﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class LstScoreRubricViewModel
    {
        public IPagedList<RubricQualifyDetail> LstRubricasCalificadas { get; set; }
        public Int32 p { get; set; }
        public String CampoBuscar { get; set; }

        public List<JsonClass> LstBusqueda { get; set; }

        public LstScoreRubricViewModel()
        {
            LstBusqueda = new List<JsonClass>();
        }

        public void CargarDatos(CargarDatosContext DataContext, Int32? Page, String CampoBuscar, int? idSubModalidad)
        {
            idSubModalidad = 1;

            this.p = Page ?? 1;
            //var cicloActual = DataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado == ConstantHelpers.ESTADO.ACTIVO).IdPeriodoAcademico;

            var cicloActual = DataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && x.IdSubModalidad == idSubModalidad).IdPeriodoAcademico;

            var query = DataContext.context.RubricaCalificada
                .Where(x => x.AlumnoSeccion.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == cicloActual)
                .GroupBy(x => new { x.Rubrica.Evaluacion.TipoEvaluacion, x.AlumnoSeccion.ProyectoAcademico })
                .Select(x => new RubricQualifyDetail
                {
                    CodigoProyecto = x.Key.ProyectoAcademico.Codigo,
                    LstAlumnos = x.Key.ProyectoAcademico.AlumnoSeccion.Select(y => new AlumnoDetail
                    {
                        Codigo = y.AlumnoMatriculado.Alumno.Codigo,
                        Nombre = y.AlumnoMatriculado.Alumno.Nombres + " " + y.AlumnoMatriculado.Alumno.Apellidos
                    }).ToList(),
                    TipoEvaluacion = x.Key.TipoEvaluacion.Tipo,
                    NombreProyecto = x.Key.ProyectoAcademico.Nombre,
                    ProyectoID = x.Key.ProyectoAcademico.IdProyectoAcademico,
                    IdTipoEvaluacion = x.Key.TipoEvaluacion.IdTipoEvaluacion,
                    SePermiteSustentacion = x.Count(y => y.SePermiteSustentacion == false && y.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo != "COM") > 0 ? false : true
                }).AsQueryable();

            if (!string.IsNullOrEmpty(CampoBuscar))
                foreach (var token in CampoBuscar.Split(' '))
                {
                    query = query.Where(x => x.CodigoProyecto.Contains(token) || x.NombreProyecto.Contains(token));
                }

            LstRubricasCalificadas = query.OrderByDescending(x => x.CodigoProyecto).
                ToPagedList(this.p, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }
    }
}