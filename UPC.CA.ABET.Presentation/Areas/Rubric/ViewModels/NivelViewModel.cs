﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class NivelViewModel
    {
        public int IdCriterio { get; set; }
        public int IdNivel { get; set; }
        public string NombreNivel { get; set; }
        public Decimal? NivelNotaMaxima { get; set; }
        public Decimal? NivelNotaMinima { get; set; }
        public string DescripcionNivel { get; set; }
        public string Color { get; set; }

        public NivelViewModel() { }

        public NivelViewModel(int idCriterio, string NombreNivel, float NotaMaxima, float NotaMinima, string DescripcionNivel)
        {
            this.IdCriterio = idCriterio;
            this.NombreNivel = NombreNivel;
            this.NivelNotaMaxima = (Decimal)NotaMaxima;
            this.NivelNotaMinima = (Decimal)NotaMinima;
            this.DescripcionNivel = DescripcionNivel;
        }

        public List<NivelViewModel> AddListNiveles(CargarDatosContext dataContext, NivelViewModel Nivel)
        {
            NivelCriterio nuevoNidel = new NivelCriterio();
            nuevoNidel.IdCriterioOutcome = Nivel.IdCriterio;
            nuevoNidel.Nombre = Nivel.NombreNivel;
            nuevoNidel.DescripcionNivel = Nivel.DescripcionNivel;
            nuevoNidel.NotaMayor = Nivel.NivelNotaMaxima;
            nuevoNidel.NotaMenor = Nivel.NivelNotaMinima;
            nuevoNidel.Color = Nivel.Color;

            var niveles = RubricLogic.AddListNiveles(dataContext.context, nuevoNidel, Nivel.IdCriterio);

            return niveles.Where(x => x.IdCriterioOutcome == Nivel.IdCriterio).Select(x => new NivelViewModel
            {
                IdCriterio = x.IdCriterioOutcome,
                DescripcionNivel = x.DescripcionNivel,
                NombreNivel = x.Nombre,
                NivelNotaMaxima = x.NotaMayor,
                NivelNotaMinima = x.NotaMenor,
                Color = x.Color,
                IdNivel = x.IdNivelCriterio
            }).ToList(); ;
        }

        public List<NivelViewModel> DeleteListNiveles(CargarDatosContext dataContext, int IdCriterio, bool porDefecto = false)
        {
            try
            {
                RubricLogic.DeleteNivelesByCriterio(dataContext.context, IdCriterio, porDefecto);
                return new List<NivelViewModel>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NivelViewModel> AddDefaulltNiveles(CargarDatosContext dataContext, int IdCriterio)
        {
            var defaultNiveles = RubricLogic.AddDefaultNiveles(dataContext.context, IdCriterio);

            return defaultNiveles.Where(x => x.IdCriterioOutcome == IdCriterio).Select(x => new NivelViewModel
            {
                IdCriterio = x.IdCriterioOutcome,
                DescripcionNivel = x.DescripcionNivel,
                NombreNivel = x.Nombre,
                NivelNotaMaxima = x.NotaMayor,
                NivelNotaMinima = x.NotaMenor,
                Color = x.Color,
                IdNivel = x.IdNivelCriterio
            }).ToList();
        }
    }
}