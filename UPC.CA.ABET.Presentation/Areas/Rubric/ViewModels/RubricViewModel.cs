﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class RubricViewModel
    {
        

        public int IdCarrera { get; set; }  //IdCarreraPeriodoAcademico
        public int IdPeriodoAcademico { get; set; }
        public int IdEvaluacion { get; set; }
        public int IdCurso { get; set; }    //IdCarreraCursoPeriodoAcademico
        public int IdCicloActual { get; set; }
        public bool EsNotaEvaluada { get; set; }
        public string NombreCicloActual { get; set; }
        public List<PeriodoAcademico> Ciclos { get; set; }
        public List<CarreraViewModel> Carreras { get; set; }
        public List<CarreraViewModel> CarrerasCrear { get; set; }
        public List<EvaluacionViewModel> Evaluaciones { get; set; }
        public List<CursoViewModel> Cursos { get; set; }
        public List<ComisionViewModel> Comisiones { get; set; }
        public List<CarreraComision> LstComisionesCarrera { get; set; }
        public int[] SelectedComsiones { get; set; }

        public const string TALLERES_PROYECTO = "Taller de Proyecto";

        public RubricViewModel()
        {
            this.Ciclos = new List<PeriodoAcademico>();
            this.Carreras = new List<CarreraViewModel>();
            this.Evaluaciones = new List<EvaluacionViewModel>();
            this.CarrerasCrear = new List<CarreraViewModel>();
            this.Cursos = new List<CursoViewModel>();
            this.LstComisionesCarrera = new List<CarreraComision>();
            this.Comisiones = new List<ComisionViewModel>();
            
        }

        public void CargarDatos(CargarDatosContext dataContext)
        {
            int getmodalidad = dataContext.session.GetModalidadId();
            var submodaact = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.Modalidad.IdModalidad == getmodalidad && x.PeriodoAcademico.Estado.Equals("ACT"));
            //var PeriodoAcademicoActivo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x=>x.Estado.Equals("ACT"));
            //this.IdCicloActual = PeriodoAcademicoActivo.IdPeriodoAcademico;
            //this.NombreCicloActual = PeriodoAcademicoActivo.CicloAcademico;

            this.IdCicloActual = submodaact.FirstOrDefault().IdPeriodoAcademico;
            this.NombreCicloActual = submodaact.FirstOrDefault().PeriodoAcademico.CicloAcademico;
            Ciclos = dataContext.context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico != IdCicloActual).ToList();

            var escuelaId = dataContext.session.GetEscuelaId();
            
            CarrerasCrear = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico).Where(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == IdCicloActual && x.Carrera.IdEscuela == escuelaId).Select(x => new CarreraViewModel
            {
                Id = x.IdCarreraPeriodoAcademico,
                Nombre = x.Carrera.NombreEspanol
            }).ToList();
        }
        /*ESTO ES PARA EL FILTRO, FALTA TRABJAR*/

        public void CargarDatos(CargarDatosContext dataContext, RubricViewModel model)
        {
            //SOLO TRABAJO CON REGULAR
            var lstsubmodapa = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado Regular").ToList();
            //var PeriodoAcademicoActivo = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x => x.Estado.Equals("ACT"));
            var PeriodoAcademicoActivo = lstsubmodapa.OrderByDescending(x => x.PeriodoAcademico.CicloAcademico).FirstOrDefault(x => x.PeriodoAcademico.Estado.Equals("ACT"));


            this.IdCicloActual = PeriodoAcademicoActivo.IdPeriodoAcademico;
            this.NombreCicloActual = PeriodoAcademicoActivo.PeriodoAcademico.CicloAcademico;
            Ciclos = dataContext.context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico != IdCicloActual).ToList();

            var escuelaId = dataContext.session.GetEscuelaId();

            //CarrerasCrear = dataContext.context.Carrera.Where(x => x.IdEscuela == escuelaId).Select(x => new CarreraViewModel
            CarrerasCrear = dataContext.context.CarreraPeriodoAcademico.Include(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico).Where(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == IdCicloActual && x.Carrera.IdEscuela == escuelaId).Select(x => new CarreraViewModel
            {
                Id = x.IdCarrera,
                Nombre = x.Carrera.NombreEspanol
            }).ToList();


        }

        public void CargarCarreras(CargarDatosContext dataContext, int IdPeriodoaAcademico)
        {
            var escuelaId = dataContext.session.GetEscuelaId();
            Carreras = dataContext.context.CarreraPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == IdPeriodoaAcademico && x.Carrera.IdEscuela == escuelaId).Select(x => new CarreraViewModel
            {
                Id = x.IdCarreraPeriodoAcademico,
                Nombre = x.Carrera.NombreEspanol
            }).ToList();
        }

        /*IdCarrera-> IdCarreraPeriodoAcademico */
        public void CargarCursoCarrera(CargarDatosContext dataContext, int IdCarrera)
        {
            var CarreraPeriodo = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == IdCarrera);

            var CursosEvaluarRubrica = dataContext.context.CursosEvaluarRubrica.Where(x => x.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico == CarreraPeriodo.IdSubModalidadPeriodoAcademico);

            Cursos = dataContext.context.CarreraCursoPeriodoAcademico.Where(x => x.IdCarrera == CarreraPeriodo.IdCarrera
                && x.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == CarreraPeriodo.IdSubModalidadPeriodoAcademico
                && CursosEvaluarRubrica.Any(cer => cer.IdCurso == x.CursoPeriodoAcademico.IdCurso)).Select(x => new CursoViewModel
            {
                Id = x.IdCarreraCursoPeriodoAcademico,
                Nombre = x.CursoPeriodoAcademico.Curso.NombreEspanol
            }).ToList();
        }
        /*x.IdCarreraPeriodoAcademico->IdCarreraCursoPeriodoAcademico   
          idCurso -> IdCarreraCursoPeriodoAcademico */
        public void CargarEvaluacionesCurso(CargarDatosContext dataContext, int idCurso)
        {
            Evaluaciones = dataContext.context.Evaluacion.Where(x => x.IdCarreraCursoPeriodoAcademico == idCurso).Select(x => new EvaluacionViewModel
            {
                Id = x.IdEvaluacion,
                Nombre = x.TipoEvaluacion.Tipo
            }).ToList();
        }
        /*DMUNOZ - Only TF */
        /*x.IdCarreraPeriodoAcademico->IdCarreraCursoPeriodoAcademico   
          idCurso -> IdCarreraCursoPeriodoAcademico> */
        public void CargarEvaluacionesCursoOnlyTF(CargarDatosContext dataContext, int idCurso)
        {
            Evaluaciones = dataContext.context.Evaluacion.Where(x => x.IdCarreraCursoPeriodoAcademico == idCurso && x.TipoEvaluacion.Tipo!="TP").Select(x => new EvaluacionViewModel
            {
                Id = x.IdEvaluacion,
                Nombre = x.TipoEvaluacion.Tipo
            }).ToList();
        }
        /*CarreraId -> IdCarreraPeriodoAcademico */
        public void CargarComisionesCarrera(CargarDatosContext dataContext, Int32 EvaluacionId, Int32 CarreraId)
        {
            //TEMPORAL SOLO PREGRADO REGULAR
            var tipoEvaluacion = dataContext.context.Evaluacion.Find(EvaluacionId).TipoEvaluacion.Tipo;

            var lstSubmodapa = dataContext.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.SubModalidad.Modalidad.NombreEspanol== "Pregrado Regular");

            // var cicloId = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO));

            var cicloId = lstSubmodapa.FirstOrDefault().PeriodoAcademico;

            var acreditadora = String.Empty;
            var carreraId = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == CarreraId).Carrera.IdCarrera;
            
            /*Si el tipo de Evaluación(PA,TF,TP(DEPRECADO)) es igual  a PA*/
            if (tipoEvaluacion.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
                acreditadora = ConstantHelpers.ACREDITADORA_WASC;
            else
                acreditadora = ConstantHelpers.ACREDITADORA_ABET;
            
            Comisiones = dataContext.context.CarreraComision.Where(x => x.IdCarrera == carreraId && x.Comision.AcreditadoraPeriodoAcademico.Acreditadora.Nombre.Equals(acreditadora) && x.IdSubModalidadPeriodoAcademico == cicloId.IdPeriodoAcademico).Select(x => new ComisionViewModel
            {
                Id = x.IdComision,
                Nombre = x.Comision.Codigo
            }).ToList();
            SelectedComsiones = new int[Comisiones.Count];
            
            /*Agregamos las comisione seleccionadas por defecto (El usuario ya no seleccionará las comisiones)*/
            for(int i=0;i<Comisiones.Count; i++)
            {
                SelectedComsiones[i] = Comisiones.ElementAt(i).Id;
            }
        }
       

        public void BindDataFromCreate(RubricDetailViewModel rubrica)
        {
            this.IdCarrera = rubrica.IdCarrera;
            this.IdCicloActual = rubrica.IdPeriodoAcademico;
            this.IdEvaluacion = rubrica.IdEvaluacion;
            this.IdCurso = rubrica.IdCurso;
            this.EsNotaEvaluada = rubrica.EsNotaEvaluada;
            this.NombreCicloActual = rubrica.NombreCicloActual;
            this.SelectedComsiones = rubrica.SelectedComsiones;
        }
    }

    public class CarreraViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }

    public class CursoViewModel : CarreraViewModel { }
    public class EvaluacionViewModel : CarreraViewModel {
        
        public Evaluacion evaluacion { get; set; }
       // public List<Evaluacion> lstEvaluacion { get; set; }
        public List<TipoEvaluacion> lstTipoEvaluacion { get; set; }
        public List<SelectListItem> lstProjectCourse { get; set; }
        public List<Carrera> lstCarrera { get; set; }

        public int IdCarrera { get; set; }
        public string CodCurso { get; set; }

        public EvaluacionViewModel()
        {
            this.evaluacion = new Evaluacion();
            //this.lstEvaluacion = new List<Evaluacion>();
            this.lstTipoEvaluacion = new List<TipoEvaluacion>();
            this.lstProjectCourse = new List<SelectListItem>();
            this.lstCarrera = new List<Carrera>();

           
        }
        public void CargarDatos(CargarDatosContext dataContext, int? IdPeriodoAcademico, int? IdEscuela, int idModalidad)
        {
            var evaluacion = dataContext.context.Evaluacion.Include(e => e.CarreraCursoPeriodoAcademico).Where(x => x.TipoEvaluacion.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico);
            //this.lstEvaluacion= evaluacion.Distinct().ToList();

            this.lstCarrera = dataContext.context.Carrera.Where(x => x.IdEscuela == IdEscuela).ToList();
            this.lstTipoEvaluacion = dataContext.context.TipoEvaluacion.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademico).ToList();

            this.evaluacion = new Evaluacion();

            this.lstCarrera = (from carrera in dataContext.context.Carrera
                               join submodalidad in dataContext.context.SubModalidad on carrera.IdSubmodalidad equals submodalidad.IdSubModalidad
                               join modalidad in dataContext.context.Modalidad on submodalidad.IdModalidad equals modalidad.IdModalidad
                               where carrera.IdEscuela == IdEscuela && modalidad.IdModalidad == idModalidad
                               select carrera).ToList();

            //TEMPORAL

            int idSubmodalidad = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodoAcademico).IdSubModalidad;
            int idmodulo = 1;



            var cursosEvaluar = dataContext.context.GETCURSOSRUBRICAS(idSubmodalidad,idmodulo,IdPeriodoAcademico, IdEscuela).ToList();
            this.lstProjectCourse.AddRange(cursosEvaluar.Select(x => new SelectListItem
            {
                Text = x.NombreEspanol,
                Value = x.Codigo
            }));
            
        }

    }
    public class ComisionViewModel : CarreraViewModel { }
}