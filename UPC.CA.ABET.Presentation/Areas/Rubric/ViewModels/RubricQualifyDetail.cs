﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class RubricQualifyDetail
    {
        public Int32 RubricaCalificadaID { get; set; }
        public String TipoEvaluacion { get; set; }
        public Int32 ProyectoID { get; set; }
        public String NombreProyecto { get; set; }
        public String CodigoProyecto { get; set; }
        public List<AlumnoDetail> LstAlumnos { get; set; }
        public String NombreEvaluacion { get; set; }
        public String NombreEvaluador { get; set; }
        public Boolean SePermiteSustentacion { get; set; }
        public Int32 IdTipoEvaluacion { get; set; }

    }

    public class AlumnoDetail
    {
        public String Nombre { get; set; }
        public String Codigo { get; set; }
    }
}