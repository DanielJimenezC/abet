﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class RubricDetailViewModel
    {
        public String DescripcionRubrica { get; set; }
        public List<OutcomeViewModel> Outcomes { get; set; }
        public int IdEvaluacion { get; set; }
        public int IdRubrica { get; set; }
        public string CarreraNombre { get; set; }
        public string CursoNombre { get; set; }
        public string EvaluacionNombre { get; set; }
        public bool EsNotaEvaluada { get; set; }
        public int IdCarrera { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdCurso { get; set; }
        public String NombreCicloActual { get; set; }
        public int[] SelectedComsiones { get; set; }
        public List<Comision> Comisiones { get; set; }
        public RubricDetailViewModel()
        {
            Outcomes = new List<OutcomeViewModel>();
        }
        public void CargarRubrica(CargarDatosContext dataContext, int idEvaluacion, RubricViewModel Rubrica)
        {
            Rubrica rubrica = RubricLogic.LoadRubric(dataContext.context, idEvaluacion);
            this.IdCarrera = Rubrica.IdCarrera;
            this.IdCurso = Rubrica.IdCurso;
            this.IdEvaluacion = idEvaluacion;
            this.IdRubrica = rubrica.IdRubrica;
            this.CarreraNombre = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == Rubrica.IdCarrera).Carrera.NombreEspanol;
            this.CursoNombre = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == Rubrica.IdCurso).CursoPeriodoAcademico.Curso.NombreEspanol;
            this.EvaluacionNombre = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
            this.SelectedComsiones = rubrica.OutcomeRubrica.Select(x => x.OutcomeComision.Comision.IdComision).Distinct().ToArray();
            this.Comisiones = rubrica.OutcomeRubrica.Select(x => x.OutcomeComision.Comision).Distinct().ToList();
            this.EsNotaEvaluada = rubrica.EsNotaEvaluada;
            this.DescripcionRubrica = rubrica.Descripcion;

            this.Outcomes = rubrica.OutcomeRubrica.Select(x => new OutcomeViewModel
            {
                DescripcionOutcome = x.OutcomeComision.Outcome.DescripcionEspanol,
                IdOutcome = x.IdOutcomeRubrica,
                NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                NombreComision = x.OutcomeComision.Comision.Codigo,
                Criterios = x.CriterioOutcome.Select(y => new CriterioViewModel
                {
                    IdCriterio = y.IdCriterioOutcome,
                    NombreCriterio = y.Nombre,
                    DescripcionCriterio = y.Descripcion,
                    NotaMaxima = y.ValorMaximo,
                    NotaMinima = y.ValorMinimo,
                    EsEvaluadoPorDefecto = y.EsEvaluadoPorDefecto,
                    IdOutcome = y.IdOutcomeRubrica,
                    Niveles = y.NivelCriterio.Select(z => new NivelViewModel
                    {
                        IdNivel = z.IdNivelCriterio,
                        NombreNivel = z.Nombre,
                        NivelNotaMaxima = z.NotaMayor,
                        NivelNotaMinima = z.NotaMenor,
                        DescripcionNivel = z.DescripcionNivel,
                        Color = z.Color,
                        IdCriterio = z.IdCriterioOutcome
                    }).ToList()
                }).ToList()
            }).ToList();

            foreach (var item in Outcomes)
            {
                Decimal max = 0;
                foreach (var subItem in item.Criterios)
                {
                    if(Decimal.Compare(subItem.NotaMaxima,max) > 0)
                    {
                        max = subItem.NotaMaxima;
                    }
                }
                item.MaxValueLastCriterio = (float)max;
            }
        }

        public void CargarRubrica(CargarDatosContext dataContext, int idEvaluacion, RubricDetailViewModel Rubrica)
        {
            Rubrica rubrica = RubricLogic.LoadRubric(dataContext.context, idEvaluacion);
            this.IdEvaluacion = idEvaluacion;
            this.IdRubrica = rubrica.IdRubrica;
            this.CarreraNombre = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == Rubrica.IdCarrera).Carrera.NombreEspanol;
            this.CursoNombre = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == Rubrica.IdCurso).CursoPeriodoAcademico.Curso.NombreEspanol;
            this.EvaluacionNombre = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
            this.SelectedComsiones = rubrica.OutcomeRubrica.Select(x => x.OutcomeComision.Comision.IdComision).Distinct().ToArray();
            this.Comisiones = rubrica.OutcomeRubrica.Select(x=>x.OutcomeComision.Comision).Distinct().ToList();
            this.DescripcionRubrica = rubrica.Descripcion;
            this.EsNotaEvaluada = rubrica.EsNotaEvaluada;

            this.Outcomes = rubrica.OutcomeRubrica.Select(x => new OutcomeViewModel
            {
                DescripcionOutcome = x.OutcomeComision.Outcome.DescripcionEspanol,
                IdOutcome = x.IdOutcomeRubrica,
                NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                NombreComision = x.OutcomeComision.Comision.Codigo,
                Criterios = x.CriterioOutcome.Select(y => new CriterioViewModel
                {
                    IdCriterio = y.IdCriterioOutcome,
                    NombreCriterio = y.Nombre,
                    DescripcionCriterio = y.Descripcion,
                    NotaMaxima = y.ValorMaximo,
                    NotaMinima = y.ValorMinimo,
                    EsEvaluadoPorDefecto = y.EsEvaluadoPorDefecto,
                    IdOutcome = y.IdOutcomeRubrica,
                    Niveles = y.NivelCriterio.Select(z => new NivelViewModel
                    {
                        IdNivel = z.IdNivelCriterio,
                        NombreNivel = z.Nombre,
                        NivelNotaMaxima = z.NotaMayor,
                        NivelNotaMinima = z.NotaMenor,
                        DescripcionNivel = z.DescripcionNivel,
                        IdCriterio = z.IdCriterioOutcome
                    }).ToList()
                }).ToList()
            }).ToList();

            foreach (var item in Outcomes)
            {
                Decimal max = 0;
                foreach (var subItem in item.Criterios)
                {
                    if (Decimal.Compare(subItem.NotaMaxima, max) > 0)
                    {
                        max = subItem.NotaMaxima;
                    }
                }
                item.MaxValueLastCriterio = (float)max;
            }
        }

        public void CargarNombresRubrica(CargarDatosContext dataContext, RubricViewModel Rubrica)
        {
            this.CarreraNombre = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == Rubrica.IdCarrera).Carrera.NombreEspanol;
            this.CursoNombre = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == Rubrica.IdCurso).CursoPeriodoAcademico.Curso.NombreEspanol;
            this.EvaluacionNombre = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
            this.IdEvaluacion = Rubrica.IdEvaluacion;
            this.EsNotaEvaluada = Rubrica.EsNotaEvaluada;

            RubricaEntity rubrica = new RubricaEntity(Rubrica.IdCarrera, Rubrica.IdEvaluacion, Rubrica.IdCurso, Rubrica.IdCicloActual, Rubrica.EsNotaEvaluada,Rubrica.IdCicloActual,Rubrica.SelectedComsiones);
            Int32 idRubrica = RubricLogic.CreateOutcomePerRubrica(dataContext.context, rubrica, this.EvaluacionNombre);
            var ResultOutcomes = RubricLogic.GetOutcomesByRubricaId(dataContext.context, idRubrica);
            Outcomes = ResultOutcomes.Select(x=> new OutcomeViewModel
            {
                IdOutcome = x.IdOutcome,
                NombreOutcome = x.NombreOutcome,
                DescripcionOutcome = x.DescripcionOutcome,
            }).ToList();

            Outcomes.ForEach(x => x.Criterios = new List<CriterioViewModel>());
        }

        public bool EsEvaluacionParticipacion(CargarDatosContext dataContext, int IdEvaluacion)
        {
            return dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == IdEvaluacion).TipoEvaluacion.Equals("PA") ? true : false;
        }

        public bool ExisteRubrica(CargarDatosContext dataContext, RubricViewModel Rubrica)
        {
            var RubricaEncontrada = dataContext.context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion);
            return RubricaEncontrada != null ? true : false;
        }

        public bool SePuedeEditar(CargarDatosContext dataContext, RubricViewModel Rubrica)
        {
            var RubricaEncontrada = dataContext.context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion);
            return RubricaEncontrada.SePuedeEditar;
        }

        public void BindDataFromIndex(RubricViewModel rubrica)
        {
            this.IdCarrera = rubrica.IdCarrera;
            this.IdPeriodoAcademico = rubrica.IdCicloActual;
            this.IdEvaluacion = rubrica.IdEvaluacion;
            this.IdCurso = rubrica.IdCurso;
            this.EsNotaEvaluada = rubrica.EsNotaEvaluada;
            this.NombreCicloActual = rubrica.NombreCicloActual;
            this.SelectedComsiones = rubrica.SelectedComsiones;
        }

        public void CargarRubrica(CargarDatosContext dataContext,int idRubrica,int idEvaluacion, int idCarreraCicloCurso)
        {
            Rubrica rubrica = dataContext.context.Rubrica.Include(
                    x => x.OutcomeRubrica.Select(y => y.CriterioOutcome.Select(z => z.NivelCriterio)))
                    .FirstOrDefault(x => x.IdRubrica == idRubrica);

            this.IdEvaluacion = idEvaluacion;
            this.IdRubrica = rubrica.IdRubrica;

            var escuelaId = dataContext.session.GetEscuelaId();
            var CarreraCicloCurso = dataContext.context.CarreraCursoPeriodoAcademico.Where(x => x.Carrera.IdEscuela == escuelaId).FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == idCarreraCicloCurso);

            this.CarreraNombre = CarreraCicloCurso.Carrera.NombreEspanol;
            this.CursoNombre = CarreraCicloCurso.CursoPeriodoAcademico.Curso.NombreEspanol;
            this.EvaluacionNombre = CarreraCicloCurso.Evaluacion.First().TipoEvaluacion.Tipo;
            this.SelectedComsiones = rubrica.OutcomeRubrica
                .Select(x => x.OutcomeComision.Comision.IdComision).Distinct().ToArray();
            this.Comisiones = rubrica.OutcomeRubrica.Select(x => x.OutcomeComision.Comision).Distinct().ToList();

            this.Outcomes = rubrica.OutcomeRubrica.Select(x => new OutcomeViewModel
            {
                DescripcionOutcome = x.OutcomeComision.Outcome.DescripcionEspanol,
                IdOutcome = x.IdOutcomeRubrica,
                NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                NombreComision = x.OutcomeComision.Comision.Codigo,
                Criterios = x.CriterioOutcome.Select(y => new CriterioViewModel
                {
                    IdCriterio = y.IdCriterioOutcome,
                    NombreCriterio = y.Nombre,
                    DescripcionCriterio = y.Descripcion,
                    NotaMaxima = y.ValorMaximo,
                    NotaMinima = y.ValorMinimo,
                    EsEvaluadoPorDefecto = y.EsEvaluadoPorDefecto,
                    IdOutcome = y.IdOutcomeRubrica,
                    Niveles = y.NivelCriterio.Select(z => new NivelViewModel
                    {
                        IdNivel = z.IdNivelCriterio,
                        NombreNivel = z.Nombre,
                        NivelNotaMaxima = z.NotaMayor,
                        NivelNotaMinima = z.NotaMenor,
                        DescripcionNivel = z.DescripcionNivel,
                        IdCriterio = z.IdCriterioOutcome
                    }).ToList()
                }).ToList()
            }).ToList();

        }

    }
}