﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class MaintenanceRubricViewModel
    {
        public Int32 IdCiclo { get; set; }
        public Int32 IdCarrera { get; set; }
        public List<PeriodoAcademico> lstCiclo { get; set; }
        public List<Carrera> lstCarrera { get; set; }
        
        public void Fill(CargarDatosContext ctx)
        {
            var context = ctx.context;
            var IdEscuela = ctx.session.GetEscuelaId();
            IdCiclo = ctx.session.GetPeriodoAcademicoId().Value;
            


            lstCiclo = (from submoda in ctx.context.SubModalidadPeriodoAcademico
                        join pa in ctx.context.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                        where (submoda.IdSubModalidad == 1)
                        select pa).ToList();

     
            lstCarrera = context.CarreraPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && x.Carrera.IdEscuela == IdEscuela).Select(x => x.Carrera).ToList();
        }
    }
}