﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class RubricABETViewModel
    {
        #region Atributos
        public Int32 IdRubrica { get; set; }
        public String CarreraNombre { get; set; }
        public String CursoNombre { get; set; }
        public String EvaluacionNombre { get; set; }
        public List<Tuple<Comision, List<OutcomeRubrica>>> OutcomesComision { get; set; }
        public List<NivelDesempenio> NivelesDesempenio { get; set; }
        #endregion

        public void Fill(CargarDatosContext ctx, RubricViewModel _Rubrica)
        {
            try
            {
                var context = ctx.context;
                Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == _Rubrica.IdEvaluacion);
                var IdCiclo = Rubrica.Evaluacion.TipoEvaluacion.IdSubModalidadPeriodoAcademico;
                var CarreraCicloCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == _Rubrica.IdCurso);
                OutcomesComision = new List<Tuple<Comision, List<OutcomeRubrica>>>();
                
                IdRubrica = Rubrica.IdRubrica;
                CarreraNombre = context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == _Rubrica.IdCarrera).Carrera.NombreEspanol;
                CursoNombre = CarreraCicloCurso.CursoPeriodoAcademico.Curso.NombreEspanol;
                EvaluacionNombre = context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
                
                var Comisiones = context.CarreraComision.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera && x.IdSubModalidadPeriodoAcademico == IdCiclo && !x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC)).Select(x => x.Comision).OrderByDescending(x => x.Codigo);

                foreach(var Comision in Comisiones)
                    OutcomesComision.Add(new Tuple<Comision, List<OutcomeRubrica>>(Comision, Rubrica.OutcomeRubrica.Where(x => x.OutcomeComision.IdComision == Comision.IdComision).ToList()));

                var RubricaNivelDesempenio = Rubrica.RubricaNivelDesempenio;
                if (RubricaNivelDesempenio.Any())
                    NivelesDesempenio = RubricaNivelDesempenio.Select(x => x.NivelDesempenio).ToList();
                else
                {
                    NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("StudentOutcome") && x.IdPeriodoAcademico == IdCiclo).ToList();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}