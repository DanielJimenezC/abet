﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class EvaluatorsViewModel
    {
        public List<TipoEvaluador> Evaluadores { get; set; }
        public List<OutcomeViewModel> Outcomes { get; set; }
        public int IdCicloActual { get; set; }
        public string CarreraNombre { get; set; }
        public string CursoNombre { get; set; }
        public string EvaluacionNombre { get; set; }
        public int IdRubrica { get; set; }
        public List<Tuple<int,int>> Matches { get; set; }

        public EvaluatorsViewModel()
        {
            Evaluadores = new List<TipoEvaluador>();
            Outcomes = new List<OutcomeViewModel>();
        }

        public void CargarDatos(CargarDatosContext dataContext, RubricDetailViewModel rubrica)
        {
            IdCicloActual = dataContext.context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).First(x=>x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
            Evaluadores = dataContext.context.TipoEvaluador.Where(x => x.IdSubModalidadPeriodoAcademico == IdCicloActual).ToList();
            Outcomes = dataContext.context.OutcomeRubrica.Include(y => y.OutcomeComision).Include(y => y.OutcomeComision.Outcome).Where(x => x.IdRubrica == rubrica.IdRubrica).Select(x => new OutcomeViewModel
            {
                IdOutcome = x.IdOutcomeRubrica,
                NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                NombreComision = x.OutcomeComision.Comision.NombreEspanol
            }).ToList();

            Matches = new List<Tuple<int, int>>();

            foreach (var item in Outcomes)
            {
                var lstOutcomeEvaluador = dataContext.context.OutcomeEvaluador.Where(x => x.IdOutcomeRubrica == item.IdOutcome && x.Estado);
                foreach (var subItem in lstOutcomeEvaluador)
                {
                    Matches.Add(new Tuple<int, int>(subItem.IdOutcomeRubrica, subItem.IdTipoEvaluador));
                }
            }
            CarreraNombre = rubrica.CarreraNombre;
            CursoNombre = rubrica.CursoNombre;
            EvaluacionNombre = rubrica.EvaluacionNombre;
            IdRubrica = rubrica.IdRubrica;
        }
    }
}