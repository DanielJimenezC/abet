﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels
{
    public class ReportViewModel
    {
        public String empresa { get; set; }
        public String curso { get; set; }
        public String ciclo { get; set; }
        public String estado { get; set; }
    }
}