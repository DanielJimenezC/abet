﻿
#region Imports

using System;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.VirtualCompany;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.VirtualCompany
{
    public class AddEditVirtualCompanyViewModel
    {
        #region Propiedades

        public Int32? EmpresaVirtualId { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(50)]
        [Display(Name = "Codigo", ResourceType = typeof(AddEditVirtualCompanyResource))]
        public String Codigo { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(50)]
        [Display(Name = "Nombre", ResourceType = typeof(AddEditVirtualCompanyResource))]
        public String Nombre { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(1000)]
        [Display(Name = "Descripcion", ResourceType = typeof(AddEditVirtualCompanyResource))]
        public String Descripcion { get; set; }

        public String TituloPagina { get; set; }
        public String Icono { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de una Empresa Virtual, donde se diferencia si la página
        /// es Nuevo o Editar, devolviendo el Titúlo de Página e Icono respectivo.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="EmpresaVirtualId">ID de la Empresa Virtual</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32? EmpresaVirtualId, int? IdEscuela)
        {
            if (EmpresaVirtualId.HasValue)
            {
                var empresaVirtual = DataContext.context.EmpresaVirtual.Find(EmpresaVirtualId);

                this.EmpresaVirtualId = empresaVirtual.IdEmpresa;
                Codigo = empresaVirtual.Codigo;
                Nombre = empresaVirtual.Nombre;
                Descripcion = empresaVirtual.Descripcion;

                TituloPagina = AddEditVirtualCompanyResource.TituloEditar;
                Icono = "edit";
            }
            else
            {
                TituloPagina = AddEditVirtualCompanyResource.TituloCrear;
                Icono = "plus-circle";
            }
        }

        #endregion
    }
}
