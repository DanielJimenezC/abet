﻿
#region Imports

using System;
using System.Linq;
using PagedList;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.VirtualCompany.PartialView
{
    public class ListVirtualCompanyPartialViewModel
    {
        #region Propiedades

        public IPagedList<EmpresaVirtual> ListEmpresaVirtual { get; set; }
        public Int32 Pagina { get; set; }
        public Int32 PeriodoAcademicoActualId { get; set; }
        public Int32 PeriodoAcademicoId { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de empresas virtuales del periodo académico actual.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="PeriodoAcademicoId">ID del periodo académico actual</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32 PeriodoAcademicoId, int? IdEscuela, Int32? Pagina)
        {
            this.Pagina = Pagina ?? 1;
            this.PeriodoAcademicoId = PeriodoAcademicoId;

            var query = DataContext.context.EmpresaVirtual.AsQueryable().OrderBy(x => x.Nombre);
            ListEmpresaVirtual =
                query.Where(
                    x =>
                        x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId ||
                        (PeriodoAcademicoId == 0 && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico== PeriodoAcademicoActualId) 
                        && x.IdEscuela == IdEscuela)
                        .ToPagedList(this.Pagina, ConstantHelpers.DEFAULT_PAGE_SIZE);
        }

        #endregion
    }
}
