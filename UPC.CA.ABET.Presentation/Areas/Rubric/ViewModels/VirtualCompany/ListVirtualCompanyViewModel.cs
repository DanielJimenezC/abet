﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.VirtualCompany
{
    public class ListVirtualCompanyViewModel
    {
        #region Propiedades

        public List<PeriodoAcademico> ListPeriodoAcademico { get; set; }
        public Int32 PeriodoAcademicoId { get; set; }
        public Int32 PeriodoAcademicoActualId { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga todos los periodos académicos existentes, obteniendo
        /// el periodo académico actual.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext)
        {
            //TEMPORAL


            ListPeriodoAcademico = (from pa in DataContext.context.PeriodoAcademico
                                    join submodapa in DataContext.context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
                                    where (submodapa.IdSubModalidad == 1)
                                    select pa
                                    ).OrderByDescending(x=>x.IdPeriodoAcademico).ToList();
                
            PeriodoAcademicoActualId = ListPeriodoAcademico.FirstOrDefault(z=>z.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            PeriodoAcademicoId = PeriodoAcademicoActualId;
        }

        #endregion
    }
}
