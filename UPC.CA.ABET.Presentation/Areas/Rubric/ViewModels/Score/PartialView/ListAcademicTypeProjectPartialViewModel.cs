﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView
{
    public class ListAcademicTypeProjectPartialViewModel
    {
        #region Propiedades
        public List<RubricaCalificada> ListRubricaCalificada { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de proyectos académicos de una determinada empresa virtual,
        /// o también todas las empresas existentes.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="EmpresaVirtualId">ID de la Empresa Virtual</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32? EmpresaVirtualId, Int32? DocenteId, String CodigoAlumno, Int32? FiltroAll)
        {
            //Int32 periodoID = DataContext.session.GetSubModalidadPeriodoAcademicoId().Value;

            Int32 periodoID = DataContext.session.GetPeriodoAcademicoId().Value;

            var query = DataContext.context.RubricaCalificada.OrderBy(x => x.IdRubricaCalificada).AsQueryable();

            if(FiltroAll.Value==0)
                query = query.Where(x => (x.AlumnoSeccion.ProyectoAcademico.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0));
            else
                query = query.Where(x => (x.AlumnoSeccion.ProyectoAcademico.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0) && x.Evaluador.IdDocente == DocenteId);

            query = query.Where(x => x.Evaluador.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoID && ( 
            (x.Rubrica.Evaluacion.TipoEvaluacion.Tipo==ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA && 
             x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo == "GER")
            || (x.Rubrica.Evaluacion.TipoEvaluacion.Tipo == ConstantHelpers.RUBRICFILETYPE.EvaluationType.TF &&
            x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo == "COM")));

            if (!CodigoAlumno.Equals("0"))
            {
                query = query.Where(x => x.AlumnoSeccion.AlumnoMatriculado.Alumno.Codigo.Equals(CodigoAlumno));
            }
            ListRubricaCalificada = query.OrderBy(x => x.AlumnoSeccion.ProyectoAcademico.Codigo).ToList();
        }

        #endregion
    }
}