﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView
{
    public class ListAcademicProjectPartialViewModel
    {
        #region Propiedades

        public List<Evaluador> ListEvaluador { get; set; }
        public List<ClaseProyectoAcademico> ListProyAcad { get; set; }
        private AbetEntities context { get; set; }
        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de proyectos académicos de una determinada empresa virtual,
        /// o también todas las empresas existentes.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="EmpresaVirtualId">ID de la Empresa Virtual</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32 EmpresaVirtualId, Int32 DocenteId, String CodigoAlumno)
        {
            ////context = DataContext.context;
            ////Int32 periodoId = DataContext.session.GetPeriodoAcademicoId().Value;
            ////var query = DataContext.context.Evaluador.AsQueryable();
            ////query = query.Where(x => (x.ProyectoAcademico.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0) && x.IdDocente == DocenteId && x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoId);

            ////if (!CodigoAlumno.Equals("0"))
            ////    query = query.Where(x => x.ProyectoAcademico.AlumnoSeccion.Any(y => y.AlumnoMatriculado.Alumno.Codigo.Equals(CodigoAlumno)));

            ////ListEvaluador = query.OrderBy(x => x.ProyectoAcademico.Codigo).ToList();
            ////Int32 periodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().Value;
            ////Int32 periodoAcademicoActualId2 = DataContext.session.GetPeriodoAcademicoId().Value;
            //Int32 SubModalidadperiodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().Value;
            //ListProyAcad = ((from cu in DataContext.context.Curso
            //                 join proa in DataContext.context.ProyectoAcademico on cu.IdCurso equals proa.IdCurso
            //                 join ase in DataContext.context.AlumnoSeccion on proa.IdProyectoAcademico equals ase.IdProyectoAcademico
            //                 join am in DataContext.context.AlumnoMatriculado on ase.IdAlumnoMatriculado equals am.IdAlumnoMatriculado
            //                 join a in DataContext.context.Alumno on am.IdAlumno equals a.IdAlumno
            //                 join eva in DataContext.context.Evaluador on proa.IdProyectoAcademico equals eva.IdProyectoAcademico
            //                 join teva in DataContext.context.TipoEvaluador on eva.IdTipoEvaluador equals teva.IdTipoEvaluador
            //                 join smp in DataContext.context.SubModalidadPeriodoAcademico on teva.IdSubModalidadPeriodoAcademico equals smp.IdSubModalidadPeriodoAcademico
            //                 join tem in DataContext.context.TipoEvaluadorMaestra on teva.idTipoEvaluadorMaestra equals tem.idTipoEvaluador
            //                 join doc in DataContext.context.Docente on eva.IdDocente equals doc.IdDocente
            //                 join ev in DataContext.context.EmpresaVirtual on proa.IdEmpresaVirtual equals ev.IdEmpresa
            //                 join te in DataContext.context.TipoEvaluacion on smp.IdSubModalidadPeriodoAcademico equals te.IdSubModalidadPeriodoAcademico
            //                 where (doc.IdDocente == DocenteId
            //                 && smp.IdSubModalidadPeriodoAcademico == SubModalidadperiodoAcademicoActualId
            //                 && te.Tipo == "TF"
            //                 && (proa.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0))
            //                 select new ClaseProyectoAcademico
            //                 {
            //                     Acronimo = proa.Codigo,
            //                     Nombre = proa.Nombre,
            //                     CodigoAlumno = a.Codigo,
            //                     Descripcion = proa.Descripcion,
            //                     TipoEvaluador = tem.Codigo,
            //                     EmpresaVirtual = ev.Nombre,
            //                     Curso = cu.NombreEspanol,
            //                     IdProyectoAcademico = proa.IdProyectoAcademico,
            //                     IdTipoEvaluacion = te.IdTipoEvaluacion,
            //                     Tipo = te.Tipo,
            //                     IdEvaluador = eva.IdEvaluador,
            //                     Guardada = true
            //                }).Distinct()).ToList();
            //if (CodigoAlumno != "0")
            //    ListProyAcad = ListProyAcad.Where(x => x.CodigoAlumno == CodigoAlumno).ToList();
            //    ListProyAcad = ListProyAcad.GroupBy(x => x.IdProyectoAcademico).Select(g => g.First()).ToList();
            context = DataContext.context;
            Int32 periodoId = DataContext.session.GetPeriodoAcademicoId().Value;
            var query = DataContext.context.Evaluador.AsQueryable();
            query = query.Where(x => (x.ProyectoAcademico.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0) && x.IdDocente == DocenteId && x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoId);

            if (!CodigoAlumno.Equals("0"))
                query = query.Where(x => x.ProyectoAcademico.AlumnoSeccion.Any(y => y.AlumnoMatriculado.Alumno.Codigo.Equals(CodigoAlumno)));

            ListEvaluador = query.OrderBy(x => x.ProyectoAcademico.Codigo).ToList();
        }

        public Boolean ProyectoCalificado(ProyectoAcademico proyecto, Evaluador evaluador, Int32 IdTipoEvaluacion)
        {
            
                var Alumnos = proyecto.AlumnoSeccion.Where(x => !x.AlumnoMatriculado.EstadoMatricula.Equals("RTE")).ToList();
                Int32 CantCalificados = 0;
                foreach (var alumno in Alumnos)
                {
                    Int32 IdCarrera = alumno.AlumnoMatriculado.IdCarrera.Value;
                    Int32 IdCursoPeriodoAcademico = alumno.Seccion.IdCursoPeriodoAcademico;
                    CarreraCursoPeriodoAcademico CarreraCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico);
                    Evaluacion Evaluacion = CarreraCurso.Evaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion);
                    if (Evaluacion == null)
                        return false;
                    Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Evaluacion.IdEvaluacion);
                    if (Rubrica == null)
                        return false;

                    /*
                    if (!evaluador.RubricaCalificada.Any())
                        return false;*/

                    //                var RubricaCalificadaGuardada = evaluador.RubricaCalificada.FirstOrDefault(x => x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && x.IdRubrica == Rubrica.IdRubrica);

                    var RubricaCalificadaGuardada = context.RubricaCalificada.Where(x => x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador
                    && x.IdRubrica == Rubrica.IdRubrica
                    && x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && x.Guardada == true).ToList();

                    if (RubricaCalificadaGuardada == null)
                        return false;

                    if (RubricaCalificadaGuardada.Count == 0)
                        return false;

                    //if (RubricaCalificadaGuardada.Guardada.Value)
                    CantCalificados++;
                }
                return CantCalificados == Alumnos.Count;
          
        }
        #endregion
    }
}