﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView
{
    public class ListScoreViewModel
    {
        public AbetEntities Context { get; set; }
        public List<NivelDesempenio> LstNivelDesempenio { get; set; }
        public SubModalidadPeriodoAcademico SubModalidadPeriodoAcademico { get; set; }

        public ListScoreViewModel(AbetEntities Context)
        {
            this.Context = Context;
        }

        public void Fill(int modalityId, int? CycleId)
        {
            if (CycleId == null)
                SubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.IdModalidad == modalityId && x.PeriodoAcademico.Estado == "ACT");
            else
                SubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.IdModalidad == modalityId && x.PeriodoAcademico.IdPeriodoAcademico == CycleId);

            LstNivelDesempenio = Context.NivelDesempenio.Where(x => x.IdPeriodoAcademico == SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico).ToList();
            LstNivelDesempenio = LstNivelDesempenio.OrderBy(x => x.TipoEvaluacion).ThenBy(x => x.PuntajeMayor).ToList();
        }
    }
}