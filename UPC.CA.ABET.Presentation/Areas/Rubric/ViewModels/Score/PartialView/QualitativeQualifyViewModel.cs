﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Score;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView
{
    public class QualitativeQualifyViewModel
    {
        public Int32 IdEvaluador { get; set; }
        public String TipoEvaluador { get; set; }
        public List<QualitativeAlumnoViewModel> alumnosProyecto { get; set; }
        public String MessageError { get; set; }

        public void Fill(CargarDatosContext ctx, Int32 IdProyectoAcademico, Int32 _IdEvaluador, String _TipoEvaluador, Int32 IdTipoEvaluacion)
        {
            var context = ctx.context;
          //var IdCiclo = ctx.session.GetPeriodoAcademicoId();
            var IdCiclo = ctx.session.GetSubModalidadPeriodoAcademicoId();

            var parIdSubModalidadPeriodoAcademico = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == 1).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            IdEvaluador = _IdEvaluador;
            TipoEvaluador = _TipoEvaluador;
            alumnosProyecto = new List<QualitativeAlumnoViewModel>();
            try
            {
                var alumnos = context.AlumnoSeccion.Where(x => x.IdProyectoAcademico == IdProyectoAcademico && x.Seccion.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico
                                                            && !x.AlumnoMatriculado.EstadoMatricula.Equals("RTE")).ToList();
                if (alumnos.Count <= 0) { MessageError = ScoreIndexResource.AlertaSinAlumnos; return; }

                foreach (var alumno in alumnos)
                {
                    var IdCarrera = alumno.AlumnoMatriculado.IdCarrera;
                    var IdCursoPeriodoAcademico = alumno.Seccion.IdCursoPeriodoAcademico;
                    CarreraCursoPeriodoAcademico carreraCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico);
                    Evaluacion Evaluacion = carreraCurso.Evaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion);
                    
                    Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Evaluacion.IdEvaluacion);

                    if (Rubrica == null) { MessageError = ScoreIndexResource.AlertaRubricaExiste; return; }

                    if (!Rubrica.OutcomeRubrica.Any() || !Rubrica.OutcomeRubrica.Any(x => x.CriterioOutcome.Any() )) { MessageError = ScoreIndexResource.AlertaOutcomes; return; }

                    RubricaCalificada rubricaExiste = context.RubricaCalificada.FirstOrDefault(x => x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && x.IdEvaluador == IdEvaluador);

                    var qualitativeAlumno = new QualitativeAlumnoViewModel();
                    qualitativeAlumno.IdAlumnoSeccion = alumno.IdAlumnoSeccion;
                    qualitativeAlumno.Nombres = alumno.AlumnoMatriculado.Alumno.Nombres + " " + alumno.AlumnoMatriculado.Alumno.Apellidos;
                    qualitativeAlumno.IdRubrica = Rubrica.IdRubrica;
                    if (rubricaExiste != null)
                        qualitativeAlumno.Observacion = rubricaExiste.Observacion;
                    else
                        qualitativeAlumno.Observacion = "";

                    alumnosProyecto.Add(qualitativeAlumno);
                }
            }
            catch(Exception e)
            {
                MessageError = e.Message;
            }
        }
    }

    public class QualitativeAlumnoViewModel
    {
        public Int32 IdRubrica { get; set; }
        public Int32 IdAlumnoSeccion { get; set; }
        public String Nombres { get; set; }
        public String Observacion { get; set; }
    }
}