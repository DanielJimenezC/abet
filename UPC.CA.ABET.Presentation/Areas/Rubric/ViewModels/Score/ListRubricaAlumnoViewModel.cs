﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class ListRubricaAlumnoViewModel
    {
        #region Propiedades

        public List<EmpresaVirtual> ListEmpresaVirtual { get; set; }
        public List<RubricaCalificada> ListRubricasCalificadas { get; set; }
        public Int32 EmpresaId { get; set; }
        public Int32 IdTipoEvaluacion { get; set; }

        public List<Alumno> LstAlumnos { get; set; }
        public String CodigoAlumno { get; set; }

        #endregion


        /// <summary>
        /// Carga los datos de las Empresas Virtuales del presente ciclo académico.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext)
        {
            Int32 periodoAcademicoActualId = DataContext.context.PeriodoAcademico.FirstOrDefault(x=>x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
            var docenteID = DataContext.session.GetDocenteId();
            ListEmpresaVirtual = DataContext.context.EmpresaVirtual.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == periodoAcademicoActualId).ToList();
            LstAlumnos = new List<Alumno>();

        }
    }
}