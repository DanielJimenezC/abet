﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class RubricRateViewModel
    {
        #region ATRIBUTOS
        public Int32 IdProyectoAcademico { get; set; }
        public Int32 IdEvaluador { get; set; }
        public Int32 IdTipoEvaluacion { get; set; }
        public String NombreEvaluacion { get; set; }
        public Int32 IdEvaluador_Actual { get; set; }

        public List<Tuple<AlumnoSeccion, Rubrica, List<NivelDesempenio>,List<CriterioCalificado>, String, List<Tuple<String, String>>>> EvaluacionesPorAlumno { get; set; }
        public ProyectoAcademico Proyecto { get; set; }
        #endregion

        public Int32 Fill(CargarDatosContext ctx, Int32 _IdProyectoAcademico, Int32 _IdTipoEvaluacion, Int32 _IdEvaluador)
        {
            var context = ctx.context;
            IdProyectoAcademico = _IdProyectoAcademico;
            IdEvaluador = _IdEvaluador;
            IdTipoEvaluacion = _IdTipoEvaluacion;
            
            var Evaluador = context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
            try
            {
                Proyecto = context.ProyectoAcademico.Find(IdProyectoAcademico);
                var Alumnos = context.AlumnoSeccion.Where(x => x.IdProyectoAcademico == IdProyectoAcademico && !x.AlumnoMatriculado.EstadoMatricula.Equals("RTE")).ToList();

                if (!Alumnos.Any())
                    return 1;

                EvaluacionesPorAlumno = new List<Tuple<AlumnoSeccion, Rubrica, List<NivelDesempenio>, List<CriterioCalificado>, string, List<Tuple<string, string>>>>();
                
                foreach (var alumno in Alumnos)
                {
                    Int32 IdCarrera = alumno.AlumnoMatriculado.IdCarrera.Value;
                    Int32 IdCursoPeriodoAcademico = alumno.Seccion.IdCursoPeriodoAcademico;
                    CarreraCursoPeriodoAcademico CarreraCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico);
                    Evaluacion Evaluacion = CarreraCurso.Evaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion);
                    Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Evaluacion.IdEvaluacion);
                    
                    if (Rubrica == null)
                        return 2; 

                    if (Rubrica.OutcomeRubrica.Count == 0 || Rubrica.OutcomeRubrica.Any(x => x.CriterioOutcome.Count == 0))
                        return 3;

                    NombreEvaluacion = Evaluacion.TipoEvaluacion.Tipo;

                    RubricaCalificada RubricaCalificada = context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == Rubrica.IdRubrica && x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && 
                                                                                                   x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador);
                    var NivelesDesempenio = Rubrica.RubricaNivelDesempenio.Select(x => x.NivelDesempenio).ToList();
                    var EvaluadoresObservaciones = context.RubricaCalificada.Where(x => x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && x.IdRubrica == Rubrica.IdRubrica && x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador != Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador)
                                                    .Select(x => new {
                                                        IdTipoEvaluador = x.Evaluador.IdTipoEvaluador,
                                                        EvaluadorCodigo = x.Evaluador.Docente.Nombres + " " + x.Evaluador.Docente.Apellidos + "(" + x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Nombre + ")",
                                                        Observacion = x.Observacion }).AsEnumerable()
                                                        .Select(x => new Tuple<String, String>(x.EvaluadorCodigo, x.Observacion)).ToList();
                    //IdEvaluador_Actual = context.Evaluador.FirstOrDefault(x => x.IdDocente == IdDocenteActual && x.IdProyectoAcademico == IdProyectoAcademico).IdEvaluador;
                    if (RubricaCalificada != null)
                    {
                        var CriteriosCalificados = RubricaCalificada.OutcomeCalificado.SelectMany(x => x.CriterioCalificado).ToList();
                        EvaluacionesPorAlumno.Add(new Tuple<AlumnoSeccion, Rubrica, List<NivelDesempenio>, List<CriterioCalificado>, string, List<Tuple<string, string>>>(alumno, Rubrica, NivelesDesempenio, CriteriosCalificados, RubricaCalificada.Observacion, EvaluadoresObservaciones));
                    }
                    else
                        EvaluacionesPorAlumno.Add(new Tuple<AlumnoSeccion, Rubrica, List<NivelDesempenio>, List<CriterioCalificado>, string, List<Tuple<string, string>>>(alumno, Rubrica, NivelesDesempenio, null, String.Empty, EvaluadoresObservaciones));
                }
                return 0;
            }
            catch(Exception)
            {
                return -1;
            }
        }

        public Decimal DameNotaCriterio(List<CriterioCalificado> CriteriosCalificados, Int32 IdCriterioOutcome)
        {
            if (CriteriosCalificados == null)
                return -1;

            var CriterioCalificado = CriteriosCalificados.FirstOrDefault(x => x.IdCriterio == IdCriterioOutcome);

            if (CriterioCalificado == null)
                return -1;

            return CriterioCalificado.NotaCriterio.Value;
        }

        public Decimal DameNotaEvaluacion(List<CriterioCalificado> CriteriosCalificados, Decimal NotaMaxima)
        {
            if (CriteriosCalificados == null)
                return 0;

            Decimal notaEvaluacion = 0;
            foreach(var criterio in CriteriosCalificados)
                notaEvaluacion += criterio.NotaCriterio.Value;

            return Decimal.Round(notaEvaluacion*20/NotaMaxima, 2);
        }
    }
}