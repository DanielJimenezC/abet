﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class RubricQualifyViewModel
    {
        public Int32 IdTipoEvaluacion { get; set; }
        public Int32 IdTipoEvaluador { get; set; }
        public Int32 IdEvaluador { get; set; }
        public Int32 IdProyectoAcademico { get; set; }
        public ProyectoAcademico proyectoAcademico { get; set; }
        public List<AlumnoSeccion> alumnosProyecto { get; set; }
        public Dictionary<AlumnoSeccion, Rubrica> rubricasPorAlumno { get; set; }
        public List<OutcomeRubrica> outcomesRubrica { get; set; }
        public String nombreEvaluacion { get; set; }
        public String nombreEvaluador { get; set; }
        public List<Comision> ListComision { get; set; }
        public Int32 DocenteId { get; set; }
        public String RolEvaluador { get; set; }

        public Int32 CargarDatos(CargarDatosContext ctx, Int32 _IdTipoEvaluacion, Int32 _IdTipoEvaluador, Int32 _IdProyectoAcademico, Int32 _IdEvaluador)
        {
            var context = ctx.context;
            var IdCiclo = ctx.session.GetSubModalidadPeriodoAcademicoId();

            IdTipoEvaluacion = _IdTipoEvaluacion;
            IdTipoEvaluador = _IdTipoEvaluador;
            IdEvaluador = _IdEvaluador;
            IdProyectoAcademico = _IdProyectoAcademico;

            proyectoAcademico = context.ProyectoAcademico.Find(IdProyectoAcademico);
            alumnosProyecto = context.AlumnoSeccion.Where(x => x.IdProyectoAcademico == IdProyectoAcademico && x.Seccion.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico== IdCiclo && x.AlumnoMatriculado.EstadoMatricula != "RTE").ToList();

            if(alumnosProyecto.Count <= 0)
            {
                return 6;
            }

            rubricasPorAlumno = new Dictionary<AlumnoSeccion, Rubrica>();
            nombreEvaluador = context.Evaluador.Include(x => x.TipoEvaluador).FirstOrDefault(x => x.IdEvaluador == IdEvaluador).TipoEvaluador.TipoEvaluadorMaestra.Codigo;

            DocenteId = ctx.session.GetDocenteId().Value;
            
            var queryOutcomeEvaluador = context.OutcomeEvaluador.Include(x => x.OutcomeRubrica).Where(x => x.IdTipoEvaluador == IdTipoEvaluador && x.Estado).AsQueryable();

            foreach (var alumno in alumnosProyecto)
            {
                Int32 IdCarrera = alumno.AlumnoMatriculado.IdCarrera.Value;
                Int32 IdCursoPeriodoAcademico = alumno.Seccion.IdCursoPeriodoAcademico;
                CarreraCursoPeriodoAcademico carreraCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico);
                Evaluacion evaluacion = carreraCurso.Evaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion);
                Rubrica rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == evaluacion.IdEvaluacion);

                if (rubrica == null) { return 1; }
                
                var outcomeEvaluador = queryOutcomeEvaluador.Where(x => x.OutcomeRubrica.IdRubrica == rubrica.IdRubrica).Select(x => x.IdOutcomeRubrica).ToList();

                RubricaCalificada rubricaCalificada = context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == rubrica.IdRubrica && x.IdEvaluador == IdEvaluador);
                if (rubricaCalificada != null) { return 2; }

                var outomceTemp = rubrica.OutcomeRubrica.Where(x => outcomeEvaluador.Contains(x.IdOutcomeRubrica)).AsQueryable();
                rubrica.OutcomeRubrica = outomceTemp.ToList();

                rubricasPorAlumno.Add(alumno, rubrica);

                if (rubrica.OutcomeRubrica.Count == 0)
                {
                    return 5;
                }

                if (rubrica.OutcomeRubrica.Any(x => x.CriterioOutcome.Count == 0))
                {
                    return 5;
                }
                String rolEvaluador = context.Evaluador.Find(IdEvaluador).TipoEvaluador.TipoEvaluadorMaestra.Codigo;

                #region validacionABET

                if (rolEvaluador == "COM" && evaluacion.TipoEvaluacion.Tipo != "PA")
                {
                    List<RubricaCalificada> rubricasCalificadas = context.RubricaCalificada.Where(x => x.IdRubrica == rubrica.IdRubrica && x.IdAlumnoSeccion == alumno.IdAlumnoSeccion).ToList();
                    List<Evaluador> evaluadoresProyecto = context.Evaluador.Where(x => x.IdProyectoAcademico == alumno.IdProyectoAcademico).ToList();
                    var countEvaluadoresProyecto = context.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == alumno.IdAlumnoSeccion).ProyectoAcademico.Evaluador.Count;
                    Int32 countEvaluadores = context.Evaluador.Where(x => x.IdProyectoAcademico == alumno.IdProyectoAcademico).Count();

                    if ((rubricasCalificadas.Count) != (countEvaluadoresProyecto - 1))
                    {
                        return 3;
                    }
                }
                else if (rolEvaluador != "GER" && evaluacion.TipoEvaluacion.Tipo == "PA")
                {
                    return 4;
                }

                this.RolEvaluador = rolEvaluador;
                #endregion
            }

            nombreEvaluacion = context.TipoEvaluacion.Find(IdTipoEvaluacion).Tipo;

            return 0;
        }
    }
}
 