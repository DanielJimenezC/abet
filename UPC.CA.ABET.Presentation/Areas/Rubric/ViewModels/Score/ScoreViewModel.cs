using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Web.Mvc;
using UPC.CA.ABET.Logic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class ScoreViewModel
    {
        public AbetEntities Context { get; set; }
        public LoadCombosLogic LoadCombosLogic { get; set; }
        public int? IdNivelDesempenio { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public decimal PuntajeMenor { get; set; }
        public decimal PuntajeMayor { get; set; }
        public string Color { get; set; }
        public string TipoEvaluacion { get; set; }
        public int CycleId { get; set; }
        public string Title { get; set; }
        public SelectList DropDownListCyclesForModality { get; set; }
        public SubModalidadPeriodoAcademico SubModalidadPeriodoAcademico { get; set; }

        public ScoreViewModel()
        {
            Context = new AbetEntities();
            LoadCombosLogic = new LoadCombosLogic(Context);
        }

        public async Task Fill(int modalityId, int? cycleId)
        {
            DropDownListCyclesForModality = LoadCombosLogic.ListCyclesForModality(ModalityId: modalityId.ToString()).ToSelectList();

            if (cycleId == null)
                SubModalidadPeriodoAcademico = await Context.SubModalidadPeriodoAcademico.FirstOrDefaultAsync(x => x.SubModalidad.Modalidad.IdModalidad == modalityId && x.PeriodoAcademico.Estado == "ACT");
            else
                SubModalidadPeriodoAcademico = await Context.SubModalidadPeriodoAcademico.FirstOrDefaultAsync(x => x.SubModalidad.Modalidad.IdModalidad == modalityId && x.PeriodoAcademico.IdPeriodoAcademico == cycleId);

            CycleId = SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico;
            Title = " del per�odo " + SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;
            Color = "#3CAE08";
            TipoEvaluacion = "StudentOutcome";

        }
    }
}