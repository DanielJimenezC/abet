﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class ScoreIndexViewModel
    {
        #region Propiedades

        public List<EmpresaVirtual> ListEmpresaVirtual { get; set; }
        public List<Evaluador> ListEvaluador { get; set; }
        public Int32 EmpresaId { get; set; }
        public Int32 IdTipoEvaluacion { get; set; }
        public String CodigoAlumno { get; set; }
        public List<Alumno> LstAlumnos { get; set; }
        public int? docenteId { get; set; }

        #endregion
        //var query = (from cc in dataContext.context.CarreraComision
        //             join c in dataContext.context.Carrera on cc.IdCarrera equals c.IdCarrera
        //             join co in dataContext.context.Comision on cc.IdComision equals co.IdComision
        //             join oc in dataContext.context.OutcomeComision on co.IdComision equals oc.IdComision
        //             join o in dataContext.context.Outcome on oc.IdOutcome equals o.IdOutcome
        //             join oepo in dataContext.context.OutcomeEncuestaPPPOutcome on o.IdOutcome equals oepo.IdOutcome
        //             join oepc in dataContext.context.OutcomeEncuestaPPPConfig on oepo.IdOutcomeEncuestaPPPConfig equals oepc.IdOutcomeEncuestaPPPConfig
        //             orderby co.Codigo
        //             //where (c.IdEscuela == EscuelaId && cc.IdPeriodoAcademico == CicloId && ((IdTipoOutcome == 1 && co.Codigo == "WASC") || (IdTipoOutcome == 2 && co.Codigo != "WASC")))
        //             where (c.IdEscuela == EscuelaId && cc.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoId && ((IdTipoOutcome == 1 && co.Codigo == "WASC") || (IdTipoOutcome == 2 && co.Codigo != "WASC")))
        //             select new { cc.IdComision, c.IdCarrera, oepc.IdOutcomeEncuestaPPPConfig });

        /// <summary>
        /// Carga los datos de las Empresas Virtuales del presente ciclo académico.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32? DocenteId)
        {
            //Int32 SubModalidadperiodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().ToInteger();

            //Int32 periodoAcademicoActualId = DataContext.session.GetPeriodoAcademicoId().Value;

            //int idsubmodapamodu = DataContext.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            //ListEvaluador = DataContext.context.Evaluador.Where(x => x.IdDocente == DocenteId && x.ProyectoAcademico.IdSubModalidadPeriodoAcademicoModulo == idsubmodapamodu).ToList();
            //ListEmpresaVirtual = new List<EmpresaVirtual>();
            //LstAlumnos = new List<Alumno>();


            ////Int32 periodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().Value;
            ////Int32 periodoAcademicoActualId = DataContext.session.GetPeriodoAcademicoId().Value;

            ////ListEvaluador = DataContext.context.Evaluador.Where(x => x.IdDocente == DocenteId && x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).ToList();
            ////ListEvaluador = DataContext.context.Evaluador.Where(x => x.IdDocente == DocenteId 
            ////&& x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == SubModalidadperiodoAcademicoActualId).ToList();

            //   foreach (var item in ListEvaluador)
            //{
            //    if(ListEmpresaVirtual.Count == 0)
            //   {
            //      ListEmpresaVirtual.Add(item.ProyectoAcademico.EmpresaVirtual);
            //   }
            //  else
            //   {
            //        if (ListEmpresaVirtual.FirstOrDefault(x => x.IdEmpresa == item.ProyectoAcademico.IdEmpresaVirtual) == null)
            //       {
            //           ListEmpresaVirtual.Add(item.ProyectoAcademico.EmpresaVirtual);
            //        }
            //   }
            //}
            //Int32 periodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().Value;
            //Int32 periodoAcademicoActualId = DataContext.session.GetPeriodoAcademicoId().Value;
            Int32 SubModalidadperiodoAcademicoActualId = DataContext.session.GetSubModalidadPeriodoAcademicoId().ToInteger();
            //ListEvaluador = DataContext.context.Evaluador.Where(x => x.IdDocente == DocenteId && x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).ToList();
            ListEvaluador = DataContext.context.Evaluador.Where(x => x.IdDocente == DocenteId && x.ProyectoAcademico.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == SubModalidadperiodoAcademicoActualId).ToList();
            ListEmpresaVirtual = new List<EmpresaVirtual>();
            docenteId = DocenteId;
            LstAlumnos = new List<Alumno>();
            foreach (var item in ListEvaluador)
            {
                if (ListEmpresaVirtual.Count == 0)
                {
                    ListEmpresaVirtual.Add(item.ProyectoAcademico.EmpresaVirtual);
                }
                else
                {
                    if (ListEmpresaVirtual.FirstOrDefault(x => x.IdEmpresa == item.ProyectoAcademico.IdEmpresaVirtual) == null)
                    {
                        ListEmpresaVirtual.Add(item.ProyectoAcademico.EmpresaVirtual);
                    }
                }
            }

        }

    }
}