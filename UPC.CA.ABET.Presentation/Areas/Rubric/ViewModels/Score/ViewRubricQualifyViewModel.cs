﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score
{
    public class ViewRubricQualifyViewModel
    {
        public Int32 AlumnoSeccionId { get; set; }
        public String nombreEvaluacion { get; set; }
        public List<NotaVerificacionAlumno> ListNotasAlumno { get; set; }
        public AlumnoSeccion alumnoActual { get; set; }
        public List<Comision> ListComision { get; set; }
        public String ObservacionAlumno { get; set; }
        public void CargarDatos(CargarDatosContext DataContext, Int32? IdRubricaCalificada,
            Int32 AlumnoSeccionId, String nombreEvaluacion)
        {
            this.AlumnoSeccionId = AlumnoSeccionId;
            this.nombreEvaluacion = nombreEvaluacion;

            ObservacionAlumno = DataContext.context.RubricaCalificada.FirstOrDefault(x => x.IdRubricaCalificada== IdRubricaCalificada).Observacion;

            ListNotasAlumno = DataContext.context.NotaVerificacionAlumno.Where(x => x.IdAlumnoSeccion == AlumnoSeccionId && x.NombreEvaluacion.Equals(nombreEvaluacion)).ToList();
            alumnoActual = DataContext.context.AlumnoSeccion.Find(AlumnoSeccionId);
            ListComision = ListNotasAlumno.Select(x => x.OutcomeRubrica.OutcomeComision.Comision).Distinct().ToList();
        }
    }
}