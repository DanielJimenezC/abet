﻿
#region Imports

using System;
using System.Linq;
using PagedList;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Collections.Generic;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject.PartialView
{
    public class ListAcademicProjectPartialViewModel
    {
        #region Propiedades

        //public IPagedList<ProyectoAcademico> ListProyectoAcademico { get; set; }
        public List<ProyectoAcademico> ListProyectoAcademico { get; set; }
        public Int32 Pagina { get; set; }
        public Int32 EmpresaVirtualId { get; set; }
        public Int32 IdSubModalidadPeriodoAcademico { get; set; }
        public Int32 IdSubModalidadPeriodoAcademicoModulo { get; set; }
        public string Codcurso { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de proyectos académicos de una determinada empresa virtual,
        /// o también todas las empresas existentes.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="EmpresaVirtualId">ID de la Empresa Virtual</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32 EmpresaVirtualId, String TipoCurso, Int32? Pagina, int? periodoAcademicoActualId, int? idEscuela, String CodigoAlumno)
        {
            this.Codcurso = TipoCurso;
            this.IdSubModalidadPeriodoAcademico = DataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == periodoAcademicoActualId).IdSubModalidadPeriodoAcademico;
            this.IdSubModalidadPeriodoAcademicoModulo = DataContext.context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;
            this.EmpresaVirtualId = EmpresaVirtualId;
            this.Pagina = Pagina ?? 1;
            
            var query = DataContext.context.ProyectoAcademico.AsQueryable().OrderBy(x => x.Nombre).AsQueryable();
            query = query.Where(x => x.EmpresaVirtual.IdSubModalidadPeriodoAcademicoModulo == IdSubModalidadPeriodoAcademicoModulo);
            query = query.Where(x => (x.IdEmpresaVirtual == EmpresaVirtualId || EmpresaVirtualId == 0) && x.EmpresaVirtual.IdEscuela == idEscuela);

            var curso = DataContext.context.Curso.Where(x => x.Codigo == TipoCurso).FirstOrDefault();
            if (!String.IsNullOrEmpty(TipoCurso))
            {
                query = query.Where(x => x.IdCurso == curso.IdCurso);
            }
            if (!CodigoAlumno.Equals("0"))
                query = query.Where(x => x.AlumnoSeccion.Any(y=>y.AlumnoMatriculado.Alumno.Codigo.Equals(CodigoAlumno)));


            var data = query.ToList();

            //ListProyectoAcademico = query.OrderBy(x => x.Codigo).ToPagedList(this.Pagina, 10 );
            ListProyectoAcademico = query.OrderBy(x => x.Codigo).ToList();
        }

        #endregion
    }
}
