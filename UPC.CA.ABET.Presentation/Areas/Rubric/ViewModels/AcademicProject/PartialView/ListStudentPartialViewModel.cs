﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.AdministrationModel;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject.PartialView
{
    public class ListStudentPartialViewModel
    {
        #region Propiedades

        public List<AlumnoAdminModel> ListAlumno { get; set; }
        public String MensajeRespuesta { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de alumnos que fueron seleccionados para un determinado
        /// proyecto académico.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="AlumnosSeleccionados">Lista de IDs de alumnos seleccionados para un proyecto académico</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, List<Int32> AlumnosSeleccionados, int IdSubModalidad)
        {
            //Int32 periodoAcademicoActualId = DataContext.context.PeriodoAcademico
            //            .FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            Int32 submodalidadPeriodoAcademicoActualId = DataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad == IdSubModalidad).IdSubModalidadPeriodoAcademico;

            ListAlumno = DataContext.context.AlumnoSeccion
                .Where(x => x.AlumnoMatriculado.IdSubModalidadPeriodoAcademico == submodalidadPeriodoAcademicoActualId && AlumnosSeleccionados.Contains(x.IdAlumnoSeccion))
                .Select(x => new AlumnoAdminModel
                {
                    AlumnoId = x.AlumnoMatriculado.Alumno.IdAlumno,
                    Codigo = x.AlumnoMatriculado.Alumno.Codigo,
                    Apellidos = x.AlumnoMatriculado.Alumno.Apellidos,
                    CursoMatriculado = x.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol,
                    Nombres = x.AlumnoMatriculado.Alumno.Nombres,
                    estaRetirado = x.AlumnoMatriculado.EstadoMatricula == ConstantHelpers.ESTADO_RETIRADO,
                    AlumnoSeccionId = x.IdAlumnoSeccion
                }).ToList();
                                                                                
            //ListAlumno = (from a in DataContext.context.Alumno
            //              join b in DataContext.context.AlumnoMatriculado
            //              on a.IdAlumno equals b.IdAlumno
            //              where
            //                  b.IdPeriodoAcademico == periodoAcademicoActualId &&
            //                  AlumnosSeleccionados.Contains(a.IdAlumno)
            //              select new AlumnoAdminModel()
            //              {
            //                  AlumnoId = a.IdAlumno,
            //                  Codigo = a.Codigo,
            //                  Nombres = a.Nombres,
            //                  Apellidos = a.Apellidos,
            //                  estaRetirado = b.EstadoMatricula == ConstantHelpers.ESTADO_RETIRADO
            //              }
            //        ).ToList();
        }

        #endregion
    }
}
