﻿
#region Imports

using System;
using System.Collections.Generic;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject.PartialView
{
    public class ListEvaluatorPartialViewModel
    {
        #region Propiedades

        public List<EvaluatorModel> ListEvaluador { get; set; }
        public String MensajeRespuesta { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga la lista de docentes que fueron seleccionados para un determinado
        /// proyecto académico.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="DocentesSeleccionados">Lista de objetos de docentes seleccionados para un proyecto académico</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, List<EvaluatorModel> DocentesSeleccionados)
        {
            ListEvaluador = new List<EvaluatorModel>();
            ListEvaluador.AddRange(DocentesSeleccionados);
        }

        #endregion
    }
}
