﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using UPC.CA.ABET.Presentation.Controllers;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject
{
    public class ListAcademicProjectViewModel
    {
        #region Propiedades

        public List<EmpresaVirtual> ListEmpresaVirtual { get; set; }
        public Int32 EmpresaId { get; set; }

        public List<CustomCurseModel> LstCurso { get; set; }
        /*CURSO->CODCURSO*/
        public String Curso { get; set; }


        public String CodigoAlumno { get; set; }
        public List<Alumno> LstAlumnos { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de las Empresas Virtuales del presente ciclo académico.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, int? idPeriodoAcademico, int? idEscuela)
        {
            
            ListEmpresaVirtual = DataContext.context.EmpresaVirtual.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && x.IdEscuela== idEscuela)
                    .OrderByDescending(x => x.Nombre).ToList();

            //VARIABLESTEMPORALES

       
            int idsubmoda = DataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == idPeriodoAcademico).IdSubModalidad;
          
            var dataCursosEvaluar = DataContext.context.GETCURSOSRUBRICAS(idsubmoda, 0, idPeriodoAcademico, idEscuela).ToList();

  


            LstAlumnos = DataContext.context.Alumno.ToList();
            this.LstCurso = new List<CustomCurseModel>();

            LstCurso.AddRange(dataCursosEvaluar.Select(x => new CustomCurseModel
            {
                NombreCompleto = x.NombreEspanol,
                Abreviatura = x.NombreEspanol,
                Codigo = x.Codigo
            }));            
        }

        #endregion
    }
}
