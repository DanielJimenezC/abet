﻿#region Imports

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web.Services.Description;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel;
using UPC.CA.ABET.Models.Areas.Rubric.CustomModel;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.AcademicProject;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject
{ 
    public class TipoEvaluadorCombobox{

        public int idTipoEvaluador { get; set; }
        public string nombre { get; set; }
        public string codigoRol { get; set; }
    }
    public class AddEditAcademicProjectViewModel
    {
        #region Propiedades

        public Int32? ProyectoAcademicoId { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(50)]
        [Display(Name = "Codigo", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String Codigo { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(50)]
        [Display(Name = "Curso", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String Curso { get; set; }
        public List<CustomCurseModel> LstCurso { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(250)]
        [Display(Name = "Nombre", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String Nombre { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [StringLength(1000)]
        [Display(Name = "Descripcion", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String Descripcion { get; set; }

        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        [Display(Name = "Empresa", ResourceType = typeof(AddEditAcademicProjectResource))]
        public Int32 EmpresaVirtualId { get; set; }

        public List<EmpresaVirtual> ListEmpresaVirtual { get; set; }

        [Display(Name = "CodigoAlumno", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String CodigoAlumno { get; set; }

        [Display(Name = "CodigoDocente", ResourceType = typeof(AddEditAcademicProjectResource))]
        public String CodigoEvaluador { get; set; }
        
        public List<TipoEvaluadorCombobox> ListRoles { get; set; }
        public Int32 RolId { get; set; }

        public String TituloPagina { get; set; }
        public String Icono { get; set; }

        public List<Alumno> LstALumno { get; set; }
        public List<Docente> LstDocente { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de un Proyecto Académico, donde se diferencia si la página
        /// es Nuevo o Editar, devolviendo el Titúlo de Página e Icono respectivo.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <returns></returns>
        public void CargarDatos(CargarDatosContext DataContext, Int32? ProyectoAcademicoId,int? idPeriodoAcademico, int? idEscuela)
        {
            LstALumno = new List<Alumno>();
            LstDocente = new List<Docente>();

            ListEmpresaVirtual = DataContext.context.EmpresaVirtual.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && x.IdEscuela==idEscuela)
                                                .OrderByDescending(x => x.Nombre).ToList();

            //PRUEBA
            int idsubmoda = DataContext.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault().IdSubModalidad;


            var dataCursosEvaluar = DataContext.context.GETCURSOSRUBRICAS(idsubmoda, 0, idPeriodoAcademico, idEscuela).ToList();

            this.LstCurso = new List<CustomCurseModel>();
            LstCurso.AddRange(dataCursosEvaluar.Select(x => new CustomCurseModel
            {
                NombreCompleto = x.NombreEspanol,
                Abreviatura = x.NombreEspanol,
                Codigo = x.Codigo
            }));

            if (ProyectoAcademicoId.HasValue)
            {
                var proyectoAcademico = DataContext.context.ProyectoAcademico.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).FirstOrDefault(x=> x.IdProyectoAcademico == ProyectoAcademicoId && x.EmpresaVirtual.IdEscuela==idEscuela);

                this.ProyectoAcademicoId = proyectoAcademico.IdProyectoAcademico;
                Codigo = proyectoAcademico.Codigo;
                Nombre = proyectoAcademico.Nombre;
                Descripcion = proyectoAcademico.Descripcion;
                Curso = proyectoAcademico.Curso.Codigo;
                EmpresaVirtualId = proyectoAcademico.IdEmpresaVirtual;

                TituloPagina = AddEditAcademicProjectResource.TituloEditar;
                Icono = "edit";
            }
            else
            {
                TituloPagina = AddEditAcademicProjectResource.TituloCrear;
                Icono = "plus-circle";
            }

            

            if (this.Curso!=null) {

                if(this.Curso.Equals("") == false)
                { 
                    ListRoles = DataContext.context.TipoEvaluador.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico 
                    && x.TipoEvaluadorMaestra.IdEscuela==idEscuela && x.Curso.Codigo==this.Curso).Select(x=>new TipoEvaluadorCombobox
                    {
                        idTipoEvaluador = x.IdTipoEvaluador,
                        nombre= x.TipoEvaluadorMaestra.Nombre

                    }).ToList();

                    return;
                }
            }
             ListRoles = new List<TipoEvaluadorCombobox>();
                                  


            

        }

        /// <summary>
        /// Obtiene una Lista de IDs de los Alumnos de un determinado Proyecto Académico,
        /// teniendo en cuenta los alumnos matriculado y que esten alguna sección.
        /// </summary>
        /// <param name="DataContext">Context de la aplicación</param>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <returns>Lista de IDs de alumnos</returns>
        public List<Int32> ObtenerAlumnosProyecto(CargarDatosContext DataContext, Int32? ProyectoAcademicoId, int? idPeriodoAcademico, int? idEscuela)
        {
            if (ProyectoAcademicoId.HasValue)
            {
                var respuesta = DataContext.context.AlumnoSeccion.
                                    Where(x => x.AlumnoMatriculado.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico &&
                                            x.IdProyectoAcademico == ProyectoAcademicoId
                                            && x.AlumnoMatriculado.Carrera.IdEscuela==idEscuela)
                                    .Select(x => x.IdAlumnoSeccion).ToList();
                    
                    //(from a in DataContext.context.AlumnoMatriculado
                    //             join b in DataContext.context.AlumnoSeccion
                    //                 on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                    //             where
                    //                 a.IdPeriodoAcademico == periodoAcademicoActualId &&
                    //                 b.IdProyectoAcademico == ProyectoAcademicoId
                    //             select new { a.IdAlumno }
                    //    ).Select(x => x.IdAlumno).Distinct().ToList();

                if (respuesta.Any())
                    return respuesta;
            }
            
            return null;
        }

        public List<EvaluatorModel> ObtenerEvaluadoresProyecto(CargarDatosContext DataContext, Int32? ProyectoAcademicoId, int? idPeriodoAcademico, int? idEscuela)
        {
            if (ProyectoAcademicoId.HasValue)
            {
                var lstEvaluadores = DataContext.context.Evaluador
                    .Where(x => x.IdProyectoAcademico == ProyectoAcademicoId.Value && x.TipoEvaluador.TipoEvaluadorMaestra.IdEscuela== idEscuela).Select(x => new EvaluatorModel
                    {
                        DocenteId = x.IdDocente,
                        NombreDocente = x.Docente.Nombres + " " + x.Docente.Apellidos,
                        CodigoDocente = x.Docente.Codigo,
                        PesoEvaluador = SqlFunctions.StringConvert((double)x.TipoEvaluador.Peso * 100).Trim(),
                        RolEvaluador = x.TipoEvaluador.TipoEvaluadorMaestra.Nombre,
                        CodigoTipoEvaluador = x.TipoEvaluador.TipoEvaluadorMaestra.Codigo,
                        TipoEvaluadorId = x.TipoEvaluador.IdTipoEvaluador

                    }).ToList();

                lstEvaluadores.ForEach(x => x.PesoEvaluador.ToString(CultureInfo.InvariantCulture));
                return lstEvaluadores;
            }

            return null;
        }

        #endregion
    }
}
