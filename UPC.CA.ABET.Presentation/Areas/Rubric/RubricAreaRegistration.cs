﻿using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Rubric
{
    public class RubricAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Rubric";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Rubric_default",
                "Rubric/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}