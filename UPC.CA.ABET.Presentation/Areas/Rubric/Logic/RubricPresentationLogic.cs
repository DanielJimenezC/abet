﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Models;
using System.Transactions;
using System.Data.Entity;
using UPC.CA.ABET.Helpers;
using System.Data;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Logic
{
    public class RubricPresentationLogic
    {

        public static RubricDetailViewModel CreateGetRubric(CargarDatosContext dataContext, RubricViewModel viewModel)
        {

            RubricDetailViewModel returnModel = new RubricDetailViewModel();

            if (RubricLogic.RubricExists(dataContext.context, viewModel.IdEvaluacion))
            {
                returnModel.CargarRubrica(dataContext, viewModel.IdEvaluacion, viewModel);
            }
            else
            {
                returnModel = CreateRubric(dataContext, viewModel);
            }
            return returnModel;
        }

        #region RUBRICAS ABET & WASC 2017-02
        public static void CrearRubricaABET(CargarDatosContext ctx, RubricViewModel viewModel)
        {
            var context = ctx.context;
            var IdPeriodoAcademicoActivo = ctx.session.GetPeriodoAcademicoId();
            var CarreraCicloCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);
            
            var IdComisiones = context.CarreraComision.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActivo && !x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC)).Select(x => x.IdComision).ToList();
            var NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("StudentOutcome") && x.IdPeriodoAcademico == IdPeriodoAcademicoActivo).OrderBy(x => x.PuntajeMayor).ToList();

            var MallasCurricular = context.MallaCurricular.FirstOrDefault(mc => mc.IdCarrera == CarreraCicloCurso.IdCarrera && mc.MallaCurricularPeriodoAcademico.Any(mcpa => mcpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActivo));
            var CursoMallaCurricular = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == MallasCurricular.IdMallaCurricular);
            var MallaCocosDetalles = context.MallaCocosDetalle.Where(x => x.IdCursoMallaCurricular == CursoMallaCurricular.IdCursoMallaCurricular &&
                                                                     IdComisiones.Contains(x.OutcomeComision.IdComision) && x.EsCalificado.Value == true);

            if (!MallaCocosDetalles.Any())
                throw new Exception("You must configure the outcomes that will be evaluated for the course.");

            if (!NivelesDesempenio.Any())
                throw new Exception("You must create the performance levels");

            Rubrica nuevaRubrica = new Rubrica
            {
                IdEvaluacion = viewModel.IdEvaluacion,
                Descripcion = string.Empty,
                EsNotaEvaluada = false,
                SePuedeEditar = true,
                NotaMaxima = 0
            };
            context.Rubrica.Add(nuevaRubrica);

            foreach (var mallaCocosDetalle in MallaCocosDetalles)
            {
                OutcomeRubrica OutcomeRubrica = new OutcomeRubrica
                {
                    Rubrica = nuevaRubrica,
                    OutcomeComision = mallaCocosDetalle.OutcomeComision,
                    NotaOutcome = 0,
                    NivelMaximo = 3,
                    Visible = true
                };
                context.OutcomeRubrica.Add(OutcomeRubrica);
            }
            foreach (var NivelDesempenio in NivelesDesempenio)
            {
                RubricaNivelDesempenio RubricaNivelDesempenio = new RubricaNivelDesempenio
                {
                    NivelDesempenio = NivelDesempenio,
                    Rubrica = nuevaRubrica
                };
                context.RubricaNivelDesempenio.Add(RubricaNivelDesempenio);
            }
            context.SaveChanges();
        }

        public static void CrearRubricaWASC(CargarDatosContext ctx, RubricViewModel viewModel)
        {
            var context = ctx.context;
            var IdPeriodoAcademicoActivo = ctx.session.GetPeriodoAcademicoId();
            var CarreraCicloCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);

            var IdComisiones = context.CarreraComision.Where(x => x.Carrera.IdCarrera == CarreraCicloCurso.IdCarrera && x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC) && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActivo).Select(x => x.IdComision).ToList();
            var NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("Competences") && x.IdPeriodoAcademico == IdPeriodoAcademicoActivo).OrderBy(x => x.PuntajeMayor).ToList();

            var MallasCurricular = context.MallaCurricular.FirstOrDefault(mc => mc.IdCarrera == CarreraCicloCurso.IdCarrera && mc.MallaCurricularPeriodoAcademico.Any(mcpa => mcpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdPeriodoAcademicoActivo));
            var CursoMallaCurricular = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == MallasCurricular.IdMallaCurricular);
            var MallaCocosDetalles = context.MallaCocosDetalle.Where(x => x.IdCursoMallaCurricular == CursoMallaCurricular.IdCursoMallaCurricular &&
                                                                     IdComisiones.Contains(x.OutcomeComision.IdComision) && x.EsCalificado.Value == true);

            if (!MallaCocosDetalles.Any())
                throw new Exception("You must configure the outcomes that will be evaluated for the course.");

            if (!NivelesDesempenio.Any())
                throw new Exception("You must create the performance levels");

            Rubrica nuevaRubrica = new Rubrica
            {
                IdEvaluacion = viewModel.IdEvaluacion,
                Descripcion = string.Empty,
                EsNotaEvaluada = false,
                SePuedeEditar = true,
                NotaMaxima = 0
            };
            context.Rubrica.Add(nuevaRubrica);

            foreach (var mallaCocosDetalle in MallaCocosDetalles)
            {
                OutcomeRubrica OutcomeRubrica = new OutcomeRubrica
                {
                    Rubrica = nuevaRubrica,
                    OutcomeComision = mallaCocosDetalle.OutcomeComision,
                    NivelMaximo = 3,
                    Visible = true
                };
                context.OutcomeRubrica.Add(OutcomeRubrica);

                CriterioOutcome CriterioOutcome = new CriterioOutcome
                {
                    OutcomeRubrica = OutcomeRubrica,
                    Descripcion = OutcomeRubrica.OutcomeComision.Outcome.DescripcionEspanol,
                    ValorMinimo = NivelesDesempenio.Min(x => x.PuntajeMenor.Value),
                    ValorMaximo = NivelesDesempenio.Max(x => x.PuntajeMayor.Value),
                    EsEvaluadoPorDefecto = false,
                    Nombre = "Criterio"
                };
                OutcomeRubrica.NotaOutcome = CriterioOutcome.ValorMaximo;
                nuevaRubrica.NotaMaxima += CriterioOutcome.ValorMaximo;
                context.CriterioOutcome.Add(CriterioOutcome);

                foreach (var NivelDesempenio in NivelesDesempenio)
                {
                    NivelCriterio NivelCriterio = new NivelCriterio
                    {
                        CriterioOutcome = CriterioOutcome,
                        NotaMenor = NivelDesempenio.PuntajeMenor,
                        NotaMayor = NivelDesempenio.PuntajeMayor,
                        Nombre = "Nivel",
                        DescripcionNivel = NivelDesempenio.Descripcion,
                        Color = NivelDesempenio.Color
                    };
                    context.NivelCriterio.Add(NivelCriterio);
                }
            }

            foreach (var NivelDesempenio in NivelesDesempenio)
            {
                RubricaNivelDesempenio RubricaNivelDesempenio = new RubricaNivelDesempenio
                {
                    NivelDesempenio = NivelDesempenio,
                    Rubrica = nuevaRubrica
                };
                context.RubricaNivelDesempenio.Add(RubricaNivelDesempenio);
            }
            context.SaveChanges();
        }
        #endregion

        public static void CrearRubrica(CargarDatosContext ctx, RubricViewModel viewModel)
        {
            var context = ctx.context;
            var IdCiclo = ctx.session.GetPeriodoAcademicoId();
            var CarreraCicloCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);
            String TipoEvaluacion = context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).TipoEvaluacion.Tipo;
            List<Int32> IdComisiones;
            List<NivelDesempenio> NivelesDesempenio;
            
            if (!TipoEvaluacion.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
            {
                IdComisiones = context.CarreraComision.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && !x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC)).Select(x => x.IdComision).ToList();
                NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("StudentOutcome")).ToList();
            }
            else
            {
                IdComisiones = context.CarreraComision.Where(x => x.Carrera.IdCarrera == CarreraCicloCurso.IdCarrera && x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC) && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo).Select(x => x.IdComision).ToList();
                NivelesDesempenio = context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("Competences")).ToList();
            }

            var MallasCurricular = context.MallaCurricular.FirstOrDefault(mc => mc.IdCarrera == CarreraCicloCurso.IdCarrera && mc.MallaCurricularPeriodoAcademico.Any(mcpa => mcpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo));
            var CursoMallaCurricular = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == MallasCurricular.IdMallaCurricular);
            var OutcomesCurso = context.MallaCocosDetalle.Where(x => x.IdCursoMallaCurricular == CursoMallaCurricular.IdCursoMallaCurricular && 
                                                                     IdComisiones.Contains(x.OutcomeComision.IdComision) && x.EsCalificado.Value == true);
            
            if (!OutcomesCurso.Any())
                throw new Exception("You must configure the outcomes that will be evaluated for the course.");

            Rubrica nuevaRubrica = new Rubrica
            {
                IdEvaluacion = viewModel.IdEvaluacion,
                Descripcion = String.Empty,
                EsNotaEvaluada = false,
                SePuedeEditar = true,
                NotaMaxima = 0
            };
            context.Rubrica.Add(nuevaRubrica);

            foreach (var OutcomeComision in OutcomesCurso)
            {
                OutcomeRubrica OutcomeRubrica = new OutcomeRubrica
                {
                    Rubrica = nuevaRubrica,
                    OutcomeComision = OutcomeComision.OutcomeComision,
                    NotaOutcome = 0,
                    NivelMaximo = 3,
                    Visible = true
                };
                context.OutcomeRubrica.Add(OutcomeRubrica);
            }
            
            if (NivelesDesempenio != null)
            {
                foreach (var NivelDesempenio in NivelesDesempenio)
                {
                    RubricaNivelDesempenio RubricaNivelDesempenio = new RubricaNivelDesempenio
                    {
                        NivelDesempenio = NivelDesempenio,
                        Rubrica = nuevaRubrica
                    };
                    context.RubricaNivelDesempenio.Add(RubricaNivelDesempenio);
                }
            }
            context.SaveChanges();
        }

        private static RubricDetailViewModel CreateRubric(CargarDatosContext dataContext, RubricViewModel viewModel)
        {
            RubricDetailViewModel returnModel = new RubricDetailViewModel();
            Carrera carrera = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == viewModel.IdCarrera).Carrera;
            Int32 idPeriodoAcademico = dataContext.session.GetPeriodoAcademicoId().Value;
            List<Int32> comisiones;
            String nombreEvaluacion = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).TipoEvaluacion.Tipo;
            String tipoOutcomeCursoTofind = string.Empty;
            List<NivelDesempenio> nivelesDesempenio;

            if (!nombreEvaluacion.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
            {
                comisiones = dataContext.context.CarreraComision.Where(x => x.IdCarrera == carrera.IdCarrera && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && !x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC)).Select(x => x.IdComision).ToList();
                nivelesDesempenio = dataContext.context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("StudentOutcome")).ToList();
                tipoOutcomeCursoTofind = "Verifica";
            }
            else
            {
                comisiones = dataContext.context.CarreraComision.Where(x => x.Carrera.IdCarrera == carrera.IdCarrera && x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC) && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).Select(x => x.IdComision).ToList();
                nivelesDesempenio = dataContext.context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("Competences")).ToList();
                tipoOutcomeCursoTofind = "Control";
            }
            

            var CarreraCicloCurso = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);
            var MallasxCarrera = dataContext.context.MallaCurricular.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera);
            Int32 idMalla = -1;
            foreach (var item in MallasxCarrera)
            {
                var MallasxPeriodo = dataContext.context.MallaCurricularPeriodoAcademico.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && x.IdMallaCurricular == item.IdMallaCurricular);
                if (MallasxPeriodo != null)
                {
                    idMalla = item.IdMallaCurricular;
                    break;
                }
            }
            var idCursoMallaCurricualar = dataContext.context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == idMalla);

            List<MallaCocosDetalle> OutcomesCurso =
                dataContext.context.MallaCocosDetalle.Include(x => x.OutcomeComision)
                    .Where(
                        x => x.IdCursoMallaCurricular == idCursoMallaCurricualar.IdCursoMallaCurricular
                        && comisiones.Contains(x.OutcomeComision.IdComision)
                        && x.EsCalificado.Value == true
                        /*&& x.TipoOutcomeCurso.DescripcionEspanol.Contains(tipoOutcomeCursoTofind)*/)
                    .ToList();

            if (OutcomesCurso.Count == 0)
            {
                throw new Exception("El curso no tiene outcomes dentro de la malla de cocos");
            }
            Rubrica nuevaRubrica = new Rubrica();
            nuevaRubrica.EsNotaEvaluada = viewModel.EsNotaEvaluada;
            nuevaRubrica.IdEvaluacion = viewModel.IdEvaluacion;
            nuevaRubrica.Descripcion = String.Empty;
            nuevaRubrica.SePuedeEditar = true;

            dataContext.context.Rubrica.Add(nuevaRubrica);
            dataContext.context.SaveChanges();

            foreach (var item in OutcomesCurso)
            {
                OutcomeRubrica OutRubrica = new OutcomeRubrica();
                OutRubrica.IdRubrica = nuevaRubrica.IdRubrica;
                OutRubrica.IdOutcomeComision = item.IdOutcomeComision;
                OutRubrica.NotaOutcome = 0;
                OutRubrica.NivelMaximo = 3;
                OutRubrica.Visible = true;
                dataContext.context.OutcomeRubrica.Add(OutRubrica);
            }
            dataContext.context.SaveChanges();

            if (nivelesDesempenio != null)
            {
                foreach (var nivel in nivelesDesempenio)
                {
                    RubricaNivelDesempenio objRubricaNivelDesempenio = new RubricaNivelDesempenio();
                    objRubricaNivelDesempenio.NivelDesempenio = nivel;
                    objRubricaNivelDesempenio.Rubrica = nuevaRubrica;
                    dataContext.context.RubricaNivelDesempenio.Add(objRubricaNivelDesempenio);
                    dataContext.context.SaveChanges();
                }
            }

            returnModel.CargarRubrica(dataContext, viewModel.IdEvaluacion, viewModel);
            returnModel.Comisiones = dataContext.context.Comision.Where(x => comisiones.Contains(x.IdComision)).ToList();
            return returnModel;
        }

        public static RubricDetailViewModel DeleteAsociatioOutcomesRubric(CargarDatosContext dataContext, RubricDetailViewModel viewModel)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    RubricDetailViewModel returnModel = new RubricDetailViewModel();
                    Int32 idRubrica = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).Rubrica.First().IdRubrica;
                    Carrera carrera = dataContext.context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == viewModel.IdCarrera).Carrera;
                    Int32 idPeriodoAcademico = dataContext.session.GetPeriodoAcademicoId().Value;
                    var NivelesRubrica = dataContext.context.NivelCriterio.Where(x => x.CriterioOutcome.OutcomeRubrica.IdRubrica == idRubrica);
                    var CriteriosRubrica = dataContext.context.CriterioOutcome.Where(x => x.OutcomeRubrica.IdRubrica == idRubrica);
                    var OutcomesEvaluador = dataContext.context.OutcomeEvaluador.Where(x => x.OutcomeRubrica.IdRubrica == idRubrica);
                    var OutcomesRubrica = dataContext.context.OutcomeRubrica.Where(x => x.IdRubrica == idRubrica);
                    dataContext.context.OutcomeEvaluador.RemoveRange(OutcomesEvaluador);
                    dataContext.context.NivelCriterio.RemoveRange(NivelesRubrica);
                    dataContext.context.CriterioOutcome.RemoveRange(CriteriosRubrica);
                    dataContext.context.OutcomeRubrica.RemoveRange(OutcomesRubrica);
                    String tipoOutcomeCursoTofind = string.Empty;

                    List<Int32> comisiones = viewModel.SelectedComsiones.ToList();
                    String nombreEvaluacion = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).TipoEvaluacion.Tipo;
                    if (!nombreEvaluacion.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
                    {
                        comisiones = dataContext.context.CarreraComision.Where(x => x.IdCarrera == carrera.IdCarrera && comisiones.Contains(x.IdComision) && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).Select(x => x.IdComision).ToList();
                        tipoOutcomeCursoTofind = "Verifica";
                    }
                    else
                    {
                        comisiones = dataContext.context.CarreraComision.Where(x => x.Carrera.IdCarrera == carrera.IdCarrera && x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC) && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico).Select(x => x.IdComision).ToList();
                        tipoOutcomeCursoTofind = "Control";
                    }
                    var CarreraCicloCurso = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);
                    var MallasxCarrera = dataContext.context.MallaCurricular.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera);
                    Int32 idMalla = -1;
                    foreach (var item in MallasxCarrera)
                    {
                        var MallasxPeriodo = dataContext.context.MallaCurricularPeriodoAcademico.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademico && x.IdMallaCurricular == item.IdMallaCurricular);
                        if (MallasxPeriodo != null)
                        {
                            idMalla = item.IdMallaCurricular;
                            break;
                        }
                    }
                    var idCursoMallaCurricualar = dataContext.context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == idMalla);

                    List<MallaCocosDetalle> OutcomesCurso =
                        dataContext.context.MallaCocosDetalle.Include(x => x.OutcomeComision)
                            .Where(
                                x => x.IdCursoMallaCurricular == idCursoMallaCurricualar.IdCursoMallaCurricular
                                && comisiones.Contains(x.OutcomeComision.IdComision)
                                && x.EsCalificado.Value == true
                                /*&& x.TipoOutcomeCurso.DescripcionEspanol.Contains(tipoOutcomeCursoTofind)*/)
                            .ToList();

                    if (OutcomesCurso.Count == 0)
                    {
                        throw new Exception("El curso no tiene outcomes dentro de la malla de cocos");
                    }

                    foreach (var item in OutcomesCurso)
                    {
                        OutcomeRubrica OutRubrica = new OutcomeRubrica();
                        OutRubrica.IdRubrica = idRubrica;
                        OutRubrica.IdOutcomeComision = item.IdOutcomeComision;
                        OutRubrica.NivelMaximo = 3;
                        OutRubrica.Visible = true;
                        dataContext.context.OutcomeRubrica.Add(OutRubrica);
                    }
                    dataContext.context.SaveChanges();

                    returnModel.CargarRubrica(dataContext, viewModel.IdEvaluacion, viewModel);
                    returnModel.Comisiones = dataContext.context.Comision.Where(x => comisiones.Contains(x.IdComision)).ToList();
                    transaction.Complete();
                    return returnModel;

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }


        }

        public static RubricDetailViewModel EditRubrica(CargarDatosContext dataContext, RubricDetailViewModel viewModel)
        {
            try
            {
                RubricDetailViewModel returnModdel = new RubricDetailViewModel();

                if (viewModel.SelectedComsiones.Length > 0)
                    if (!RubricLogic.RubricAlredyHasOutcomesForComission(dataContext, viewModel.IdEvaluacion, viewModel.SelectedComsiones))
                        returnModdel = DeleteAsociatioOutcomesRubric(dataContext, viewModel);

                Rubrica rubrica = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).Rubrica.First();
                rubrica.Descripcion = viewModel.DescripcionRubrica;
                rubrica.EsNotaEvaluada = viewModel.EsNotaEvaluada;
                dataContext.context.SaveChanges();

                returnModdel.CargarRubrica(dataContext, viewModel.IdEvaluacion, viewModel);

                return returnModdel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// No se guardan los roles de evaluador, ya que estos pueden no existir para el presente ciclo o haberse agregado nuevos roles
        /// con diferente peso de evaluacion, por lo tanto, la relacion entre los roles y los outcomes se obvia para evitar la inconsistencia
        /// de información.
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>

        public static RubricDetailViewModel CreateRubricFromLastCicle(CargarDatosContext dataContext, RubricViewModel viewModel, int IdSubModalidad)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    Rubrica rubricaPasada =
                    dataContext.context.Rubrica.Include(
                        x => x.OutcomeRubrica.Select(y => y.CriterioOutcome.Select(z => z.NivelCriterio)))
                        .FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion);

                    if (rubricaPasada == null)
                    {
                        throw new Exception("No existe una rúbrica para ese curso en el ciclo seleccionado.");
                    }
                    var CarreraCicloCurso = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == viewModel.IdCurso);
                    Int32 idCarrera = CarreraCicloCurso.IdCarrera;
                    Int32 idCurso = CarreraCicloCurso.CursoPeriodoAcademico.IdCurso;
                    String codigoCurso = CarreraCicloCurso.CursoPeriodoAcademico.Curso.Codigo;
                    IdSubModalidad = 1;
                    //Int32 idCiclo = dataContext.context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
                    Int32 idCiclo = dataContext.context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad == IdSubModalidad).IdPeriodoAcademico;

                    var cursoPeriodo = dataContext.context.CursoPeriodoAcademico.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idCiclo && x.Curso.Codigo == codigoCurso);
                    if (cursoPeriodo == null)
                    {
                        throw new Exception("Si el curso a cambiado de código no se podrá duplicar la rúbrica");
                    }
                    Int32 idCicloCurso = cursoPeriodo.IdCursoPeriodoAcademico;

                    Int32 idCursoActual = dataContext.context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == idCarrera && x.IdCursoPeriodoAcademico == idCicloCurso).IdCarreraCursoPeriodoAcademico;
                    var TipoEvaluacion = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion).TipoEvaluacion.Tipo;
                    Evaluacion evaluacion = dataContext.context.Evaluacion.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == idCursoActual && x.TipoEvaluacion.Tipo == TipoEvaluacion);

                    if (evaluacion.Rubrica.FirstOrDefault() != null)
                    {
                        throw new Exception("Ya hay una rúbrica creada para la evaluación eliminela para poder realizar la carga");
                    }

                    Rubrica nuevaRubrica = new Rubrica();
                    nuevaRubrica.IdEvaluacion = evaluacion.IdEvaluacion;
                    nuevaRubrica.Descripcion = rubricaPasada.Descripcion;
                    nuevaRubrica.EsNotaEvaluada = rubricaPasada.EsNotaEvaluada;
                    nuevaRubrica.NotaMaxima = rubricaPasada.NotaMaxima;
                    nuevaRubrica.SePuedeEditar = true;

                    dataContext.context.Rubrica.Add(nuevaRubrica);
                    dataContext.context.SaveChanges();

                    foreach (var outcome in rubricaPasada.OutcomeRubrica)
                    {
                        var OutcomeComisionActual = dataContext.context.OutcomeComision
                            .FirstOrDefault(x => x.Outcome.Nombre == outcome.OutcomeComision.Outcome.Nombre
                            && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idCiclo && x.IdComision == outcome.OutcomeComision.IdComision);

                        if (OutcomeComisionActual != null)
                        {
                            OutcomeRubrica nuevoOutcome = new OutcomeRubrica();
                            nuevoOutcome.IdRubrica = nuevaRubrica.IdRubrica;
                            nuevoOutcome.NivelMaximo = outcome.NivelMaximo;
                            nuevoOutcome.NotaOutcome = outcome.NotaOutcome;
                            nuevoOutcome.IdOutcomeComision = OutcomeComisionActual.IdOutcomeComision;
                            nuevoOutcome.Visible = true;
                            dataContext.context.OutcomeRubrica.Add(nuevoOutcome);
                            dataContext.context.SaveChanges();

                            foreach (var criterio in outcome.CriterioOutcome)
                            {
                                CriterioOutcome nuevoCriterio = new CriterioOutcome();
                                nuevoCriterio.Descripcion = criterio.Descripcion;
                                nuevoCriterio.EsEvaluadoPorDefecto = criterio.EsEvaluadoPorDefecto;
                                nuevoCriterio.IdOutcomeRubrica = nuevoOutcome.IdOutcomeRubrica;
                                nuevoCriterio.Nombre = criterio.Nombre;
                                nuevoCriterio.ValorMaximo = criterio.ValorMaximo;
                                nuevoCriterio.ValorMinimo = criterio.ValorMinimo;
                                
                                dataContext.context.CriterioOutcome.Add(nuevoCriterio);
                                dataContext.context.SaveChanges();

                                foreach (var nivel in criterio.NivelCriterio)
                                {
                                    NivelCriterio nuevoNivel = new NivelCriterio();
                                    nuevoNivel.DescripcionNivel = nivel.DescripcionNivel;
                                    nuevoNivel.IdCriterioOutcome = nuevoCriterio.IdCriterioOutcome;
                                    nuevoNivel.Nombre = nivel.Nombre;
                                    nuevoNivel.NotaMayor = nivel.NotaMayor;
                                    nuevoNivel.NotaMenor = nivel.NotaMenor;

                                    dataContext.context.NivelCriterio.Add(nuevoNivel);
                                    dataContext.context.SaveChanges();
                                }
                            }
                        }
                    }



                    RubricDetailViewModel model = new RubricDetailViewModel();
                    model.CargarRubrica(dataContext, nuevaRubrica.IdRubrica, evaluacion.IdEvaluacion, CarreraCicloCurso.IdCarreraCursoPeriodoAcademico);

                    transaction.Complete();
                    return model;

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
    }
}