﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class TipoEvaluacionController : BaseController
    {
        private AbetEntities db = new AbetEntities();

        // GET: Rubric/Maintenance
        public ActionResult Index()
        {

            var model = db.TipoEvaluacion.Include(t => t.SubModalidadPeriodoAcademico.PeriodoAcademico).Where(x=>x.SubModalidadPeriodoAcademico.IdPeriodoAcademico ==PeriodoAcademicoId);
            return View(model.ToList());
        }
        
        /*
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEvaluacion tipoEvaluacion = db.TipoEvaluacion.Find(id);
            if (tipoEvaluacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoEvaluacion);
        }
        */
        // GET: Rubric/Maintenance/Create
        public ActionResult Create()
        {            
            return View();
        }

         [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdTipoEvaluacion,Tipo,IdPeriodoAcademico")] TipoEvaluacion tipoEvaluacion)
        {
            //Obtener el IdSubModalidadPeriodoAcademico
            int idpa = this.PeriodoAcademicoId.Value;
            int idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idpa).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            tipoEvaluacion.IdSubModalidadPeriodoAcademico = idSubModalidadPeriodoAcademico;
            tipoEvaluacion.IdEscuela = this.EscuelaId;

            /*VERIFICAR QUE EL CÓDIGO NO EXISTA*/

            var tipo = Context.TipoEvaluacion.FirstOrDefault(x => x.Tipo == tipoEvaluacion.Tipo && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId && x.IdEscuela == this.EscuelaId);

            if(tipo!=null)
            {
                PostMessage(MessageType.Error, MessageResource.ElCodigoDeTipoEvaluacionYaSeEncuentraRegistrado);
            }
            else
            {
                db.TipoEvaluacion.Add(tipoEvaluacion);
                db.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueRealizadoConExito);
                return RedirectToAction("Index");

            }
            
            return View(tipoEvaluacion);
        }

        /*
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEvaluacion tipoEvaluacion = db.TipoEvaluacion.Find(id);
            if (tipoEvaluacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdPeriodoAcademico = new SelectList(db.PeriodoAcademico, "IdPeriodoAcademico", "CicloAcademico", tipoEvaluacion.IdPeriodoAcademico);
            return View(tipoEvaluacion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdTipoEvaluacion,Tipo,IdPeriodoAcademico")] TipoEvaluacion tipoEvaluacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoEvaluacion).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdPeriodoAcademico = new SelectList(db.PeriodoAcademico, "IdPeriodoAcademico", "CicloAcademico", tipoEvaluacion.IdPeriodoAcademico);
            return View(tipoEvaluacion);
        }
        */
        // GET: Rubric/Maintenance/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluacion tipoEvaluacion = db.TipoEvaluacion.Find(id);
            if (tipoEvaluacion == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(tipoEvaluacion);
        }

        // POST: Rubric/Maintenance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                TipoEvaluacion tipoEvaluacion = db.TipoEvaluacion.Find(id);
                db.TipoEvaluacion.Remove(tipoEvaluacion);
                db.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueEliminadoConExito);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                PostMessage(MessageType.Error, MessageResource.ElRegistroNoSePuedeEliminarPorqueDependeDeOtrosRegistros);
                return RedirectToAction("Delete", new { id = id });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
