﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AllowAnonymous]
    public class UploadFilesController : BaseController
    {
        // GET: Rubric/UploadFiles
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UploadDocenteCalificadoresProyectos(HttpPostedFileBase file,int cicloAcademicoId)
        {
            
            String fileExtension = System.IO.Path.GetExtension(file.FileName);
            if (fileExtension == ".xlsx")
            {
                CargarDatosContext ctx = CargarDatosContext();

                Int32 colCodigoProfesor = 0;
                Int32 colCodigoProyecto = 1;
                Int32 colRegistra = 2;
                Int32 colEdita = 3;
                Int32 colCoordinador = 4;

                var dataTable = ExcelLogic.ExtractExcelToDataTable(file);
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    if (String.IsNullOrEmpty(dataTable.Rows[i][colCodigoProfesor].ToString()) &&
                          String.IsNullOrEmpty(dataTable.Rows[i][colCodigoProyecto].ToString()) &&
                          String.IsNullOrEmpty(dataTable.Rows[i][colRegistra].ToString()) &&
                          String.IsNullOrEmpty(dataTable.Rows[i][colEdita].ToString()) &&
                          String.IsNullOrEmpty(dataTable.Rows[i][colCoordinador].ToString())
                    )
                    {
                        continue;
                    }
                    string codigoProfesor = dataTable.Rows[i][colCodigoProfesor].ToString();
                    string codigoProyecto = dataTable.Rows[i][colCodigoProyecto].ToString();
                    bool Register = false;
                    bool Edit = false;
                    bool Coordinador = false;
                    if (String.IsNullOrEmpty(dataTable.Rows[i][colRegistra].ToString()))
                    {
                        Register = false;
                    }
                    else
                    {
                        Register = true;
                    }
                    if (String.IsNullOrEmpty(dataTable.Rows[i][colEdita].ToString()))
                    {
                        Edit = false;
                    }
                    else
                    {
                        Edit = true;
                    }

                    if (String.IsNullOrEmpty(dataTable.Rows[i][colCoordinador].ToString()))
                    {
                        Coordinador = false;
                    }
                    else
                    {
                        Coordinador = true;
                    }

                    if(Coordinador)
                    {
                        var subModalidadPeriodoAcademicoId = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloAcademicoId)
                        .Select(x => x.IdSubModalidadPeriodoAcademico).FirstOrDefault();

                        var SubModalidadPeriodoAcademicoModuloId=
                            ctx.context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId)
                        .Select(x => x.IdSubModalidadPeriodoAcademicoModulo).FirstOrDefault();

                        var profesor = ctx.context.Docente.Where(x => x.Codigo.ToLower().Contains(codigoProfesor.ToLower())).FirstOrDefault();

                        var proyectoAcademico = ctx.context.ProyectoAcademico.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == SubModalidadPeriodoAcademicoModuloId).ToList();
                        foreach(ProyectoAcademico pa in proyectoAcademico)
                        {
                            var rubricaCalificaExiste = ctx.context.RubricaControlCalifica.SingleOrDefault(x =>
                                                 x.IdDocente == profesor.IdDocente && x.IdProyectoAcademico == pa.IdProyectoAcademico
                                                 && x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId)
                                                    ;
                            if (rubricaCalificaExiste == null)
                            {
                                RubricaControlCalifica rcc = new RubricaControlCalifica();
                                rcc.IdDocente = profesor.IdDocente;
                                rcc.IdProyectoAcademico = pa.IdProyectoAcademico;
                                rcc.IdSubModalidadPeriodoAcademico = subModalidadPeriodoAcademicoId;
                                rcc.Register = Register;
                                rcc.Edit = Edit;

                                ctx.context.RubricaControlCalifica.Add(rcc);
                            }
                            else
                            {
                                rubricaCalificaExiste.Register = Register;
                                rubricaCalificaExiste.Edit = Edit;
                            }
                        }

                    }
                    else
                    {
                        var subModalidadPeriodoAcademicoId = ctx.context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cicloAcademicoId)
                        .Select(x => x.IdSubModalidadPeriodoAcademico).FirstOrDefault();

                        var profesor = ctx.context.Docente.Where(x => x.Codigo.ToLower().Contains(codigoProfesor.ToLower())).FirstOrDefault(); ;
                        var proyectoAcademico = ctx.context.ProyectoAcademico.Where(x => x.Codigo.ToLower().Contains(codigoProyecto.ToLower())).FirstOrDefault();

                        var rubricaCalificaExiste = ctx.context.RubricaControlCalifica.SingleOrDefault(x =>
                         x.IdDocente == profesor.IdDocente && x.IdProyectoAcademico == proyectoAcademico.IdProyectoAcademico
                         && x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademicoId)
                            ;

                        if (rubricaCalificaExiste == null)
                        {
                            RubricaControlCalifica rcc = new RubricaControlCalifica();
                            rcc.IdDocente = profesor.IdDocente;
                            rcc.IdProyectoAcademico = proyectoAcademico.IdProyectoAcademico;
                            rcc.IdSubModalidadPeriodoAcademico = subModalidadPeriodoAcademicoId;
                            rcc.Register = Register;
                            rcc.Edit = Edit;

                            ctx.context.RubricaControlCalifica.Add(rcc);
                        }
                        else
                        {
                            rubricaCalificaExiste.Register = Register;
                            rubricaCalificaExiste.Edit = Edit;
                        }
                    }                    
                    
                }

                ctx.context.SaveChanges();

                var data = new { message = "Archivo cargado correctamente" };
                return Json(data);

            }
            else
            {
               var data = new { message = "Formato de archivo incorrecto" };
                return Json(data);
            }
            
            
        }

    }
}