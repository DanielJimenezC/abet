﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;



namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    
    public class HomeController : BaseController
    {
        [AuthorizeUserAttribute(AppRol.Docente, AppRol.CoordinadorCarrera,AppRol.GerenteGeneral)]
        public ActionResult Index(IndexViewModel model)
        {
            var viewModel = new IndexViewModel();
            viewModel.CargarDatos(CargarDatosContext(), model);
            return View(viewModel);
        }

        /*
        public JsonResult ListCursoCarrera(Int32 IdSelected)
        {
            var viewModel = new IndexViewModel();
            viewModel.CargarCursoCarrera(CargarDatosContext(), IdSelected);
            return Json(viewModel.Cursos);
        }
        public JsonResult ListEvaluacionCurso(Int32 IdSelected, Int32 IdCarrera)
        {
            IndexViewModel viewModel = new IndexViewModel();
            viewModel.CargarEvaluacionesCurso(CargarDatosContext(), IdSelected, IdCarrera);
            return Json(viewModel.Evaluaciones);
        }

        [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult EliminarRubrica(Int32 IdRubrica)
        {
            int resultado = RubricLogic.DeleteRubric(CargarDatosContext(), IdRubrica);
            if (resultado == 1) { PostMessage(MessageType.Error, RubricCreateResource.EliminarImposible); }
            else if (resultado == 2) { PostMessage(MessageType.Error, RubricCreateResource.NoExisteRubrica); }
            else if (resultado == 0) { PostMessage(MessageType.Info, RubricCreateResource.EliminarCorrecto); }
            return RedirectToAction("Index");
        }
        */
        /*
        public ActionResult ChangeIdioma(String Idioma)
        {
            var culture = new CultureInfo(Idioma);

            Session.Set(SessionKey.Culture, culture);
            CookieHelpers.Set(CookieKey.Culture, Idioma);

            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }*/

        //reportes 

        public JsonResult ActualizarReporteG1(IndexViewModel model)
        {
            try
            {
                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);
                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        /*
        public JsonResult ActualizarReporteG2(IndexViewModel model)
        {
            try
            {
                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG3(IndexViewModel model)
        {
            try
            {
                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG4(IndexViewModel model)
        {
            try
            {

                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG5(IndexViewModel model)
        {
            try
            {
                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG6(IndexViewModel model)
        {
            try
            {

                var resultado = model.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG7(IndexViewModel model)
        {
            try
            {

                var resultado = viewModel.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ActualizarReporteG8(IndexViewModel model)
        {
            try
            {
                var resultado = viewModel.CargarDatosRubrica(CargarDatosContext(), model);

                return Json(new { result = resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }*/
    }
}