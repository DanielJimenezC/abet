﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Score;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class NotificationController : BaseController
    {
        // GET: Rubric/Score

        [AuthorizeUserAttribute(AppRol.Docente)]
        public ActionResult index()
        {
            NotificationViewModel model = new NotificationViewModel();
            model.CargarDatos(CargarDatosContext(), this.PeriodoAcademicoId, this.EscuelaId);

            return View(model);
        }



        [HttpGet]
        public ActionResult _ListEvaluadoresNotificarPartialView(Int32? idEmpresa, Int32? idCurso, string codigoRol, string estado)
        {
            var viewModel = new NotificationPartialEvaluadorViewModel();
            viewModel.CargarDatos(CargarDatosContext(), this.PeriodoAcademicoId, this.EscuelaId, idEmpresa, idCurso, codigoRol, estado);
            return PartialView("PartialView/_ListEvaluadoresNotificarPartialView", viewModel);
        }

        [HttpPost]
        public JsonResult GuardarConfiguracionNotificacionEvaluadores(string asunto, string mensaje)
        {
            var objMensaje = Context.Mensaje.Where(x => EscuelaId == this.EscuelaId).FirstOrDefault();

            if(objMensaje!=null)
            {

                objMensaje.Asunto = asunto;
                objMensaje.Mensaje1 = mensaje;
                Context.Entry(objMensaje).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
            else
            {
                var notify = new Mensaje();
                notify.idEscuela = Session.GetEscuelaId();
                notify.idMensaje = 1;
                notify.Asunto = asunto;
                notify.Mensaje1 = mensaje;
                Context.Mensaje.Add(notify);
                Context.SaveChanges();
            }
            return Json("1");

        }


        [HttpPost]
        public JsonResult NotificarEvaluadores(Int32? idEmpresa, Int32? idCurso, string codigoRol, string estado)
        {
            // ENVIAR MENSAJE A TODOS LOS EVALUADORES
            try
            {
                EmailGRALogic mailLogic = new EmailGRALogic();

                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);

                /*
                IdEmpresa = IdEmpresa == null ? 0 : IdEmpresa;
                IdTipoEvaluadorMaestro = IdTipoEvaluadorMaestro == null ? 0 : IdTipoEvaluadorMaestro;
                CodCurso = CodCurso == null ? "" : CodCurso;
                var lstEvaluadores = context.Evaluador.Where(
                    x => x.ProyectoAcademico.IdPeriodoAcademico == PeriodoAcademicoId
                    && x.TipoEvaluador.TipoEvaluadorMaestra.IdEscuela == escuelaId
                    && x.ProyectoAcademico.EmpresaVirtual.IdEmpresa == (IdEmpresa == 0 ? x.ProyectoAcademico.EmpresaVirtual.IdEmpresa : IdEmpresa)
                    && x.TipoEvaluador.Curso.Codigo == (CodCurso.Equals("") == true ? x.TipoEvaluador.Curso.Codigo : CodCurso)
                    && x.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == (IdTipoEvaluadorMaestro == 0 ? x.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador : IdTipoEvaluadorMaestro)

                    ).Select(x => new EvaluadorRow
                    {
                        DocenteCodigo = x.Docente.Codigo,
                        DocenteNombres = x.Docente.Nombres

                    }).Distinct().ToList();
                    */

                //TEMPORAL

                int idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                int idSubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                var lstEvaluadores = Context.SP_LIST_ESTADO_CALIFICACION_EVALUACION_POR_ALUMNO(
                 idSubModalidadPeriodoAcademicoModulo,
                 idCurso,
                 codigoRol.Equals("") == true ? null : codigoRol,
                 idEmpresa,
                 estado.Equals("") == true ? null : estado
                 ).Select(x => new
                 {
                     DocenteCodigo = x.CodigoEvaluador,
                     DocenteNombres = x.NombreEvaluador
                 }).Distinct().ToList();
                

                var objMensaje = Context.Mensaje.Where(x => EscuelaId == this.EscuelaId).FirstOrDefault();

                foreach (var  evaluador in lstEvaluadores)
                {
                    mailLogic.SendMail(objMensaje.Asunto, mailLogic.GetHtmlEmailRubricEvaluacion(objMensaje.Mensaje1, urlImage, urlImage + "/Areas/Survey/Resources/Images/headerMailUPC_Calificacion.jpg", urlImage + "/Areas/Survey/Resources/Images/footerMailUPC.jpg"), "u201415738"+"@upc.edu.pe");
                }
                return  Json("1");
            }
            catch (Exception ex)
            {
                return Json("0");
            }

        }
    }
}