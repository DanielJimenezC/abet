﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class EvaluacionController : BaseController
    {
        private AbetEntities db = new AbetEntities();

       

        // GET: Rubric/Evaluacion
        public ActionResult Index()
        {

                        var data = (from e in db.Evaluacion
                                    join b in db.CarreraCursoPeriodoAcademico on e.IdCarreraCursoPeriodoAcademico equals b.IdCarreraCursoPeriodoAcademico
                                    join c in db.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico                                       
                                       where (
                                       c.SubModalidadPeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId &&
                                       b.Carrera.IdEscuela == this.EscuelaId)
                                       select e).Distinct().ToList();
            

            return View(data);
        }

        /*
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evaluacion evaluacion = db.Evaluacion.Find(id);
            if (evaluacion == null)
            {
                return HttpNotFound();
            }

            return View(evaluacion);
        }*/

        // GET: Rubric/Evaluacion/Create
        public ActionResult Create()
        {


            var idModalidad = Session.GetModalidadId();
            var model = new EvaluacionViewModel();
            model.CargarDatos(CargarDatosContext(), PeriodoAcademicoId, this.EscuelaId, idModalidad);


            ViewBag.IdCarrera = new SelectList(model.lstCarrera, "IdCarrera", "codigo");
            ViewBag.CodCurso = new SelectList(model.lstProjectCourse, "Value", "Text");


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EvaluacionViewModel model2)
        {
            /*VALIDAR SI EXISTE EL CODIGO DE TIPO DE EVALUACION*/

            var validarTipoRepetido = (from b in db.CarreraCursoPeriodoAcademico
                                       join c in db.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
                                       join d in db.Evaluacion on b.IdCarreraCursoPeriodoAcademico equals d.IdCarreraCursoPeriodoAcademico
                                       join e in db.Curso on c.IdCurso equals e.IdCurso
                                       where ((e.Codigo == model2.CodCurso) &&
                                       c.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId &&
                                       b.Carrera.IdCarrera == model2.IdCarrera
                                       && d.TipoEvaluacion.IdTipoEvaluacion == model2.evaluacion.IdTipoEvaluacion)
                                       select d).Distinct().ToList();

            if (validarTipoRepetido.Any())
            {
                PostMessage(MessageType.Error, MessageResource.ElTipoDeEvaluacionYaFueRegistradaParaElCursoyCarreraSeleccionada);
                return RedirectToAction("Create");
            }


            var idCarreraCursoPeriodoAcademico = (from b in db.CarreraCursoPeriodoAcademico
                                    join c in db.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico                                    
                                    join e in db.Curso on c.IdCurso equals e.IdCurso
                                    where ((e.Codigo == model2.CodCurso) &&
                                    c.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId &&
                                    b.Carrera.IdCarrera == model2.IdCarrera)
                                    select b.IdCarreraCursoPeriodoAcademico)
                                .FirstOrDefault();


            model2.evaluacion.IdCarreraCursoPeriodoAcademico = idCarreraCursoPeriodoAcademico;

            
             db.Evaluacion.Add(model2.evaluacion);
             db.SaveChanges();
            PostMessage(MessageType.Success, MessageResource.ElRegistroFueRealizadoConExito);
            return RedirectToAction("Index");
           
        }

       /*
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var model = new EvaluacionViewModel();
            model.CargarDatos(CargarDatosContext(), this.PeriodoAcademicoId, this.escuelaId);
            model.evaluacion = db.Evaluacion.Include(x => x.CarreraCursoPeriodoAcademico).Where(x => x.IdEvaluacion == id).FirstOrDefault();
            if (model.evaluacion == null)
            {
                return HttpNotFound();
            }

            ViewBag.IdCarrera = new SelectList(model.lstCarrera, "IdCarrera", "codigo", model.evaluacion.CarreraCursoPeriodoAcademico.Carrera.IdCarrera);
            ViewBag.CodCurso = new SelectList(model.lstProjectCourse, "Value", "Text", model.evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.Curso.Codigo);
           
            return View(model);
        }*/
        /*
          [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EvaluacionViewModel model)
        {
            var idCarreraCursoPeriodoAcademico = (from b in db.CarreraCursoPeriodoAcademico
                                                  join c in db.CursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
                                                  join d in db.Evaluacion on b.IdCarreraCursoPeriodoAcademico equals d.IdCarreraCursoPeriodoAcademico
                                                  join e in db.Curso on c.IdCurso equals e.IdCurso
                                                  where ((e.Codigo == model.CodCurso) &&
                                                  c.IdPeriodoAcademico == this.PeriodoAcademicoId &&
                                                  b.Carrera.IdCarrera == model.IdCarrera)
                                                  select b.IdCarreraCursoPeriodoAcademico)
                              .FirstOrDefault();

            model.evaluacion.IdCarreraCursoPeriodoAcademico = idCarreraCursoPeriodoAcademico;

            if (ModelState.IsValid)
            {
                db.Entry(model.evaluacion).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCarrera = new SelectList(model.lstCarrera, "IdCarrera", "codigo", model.evaluacion.CarreraCursoPeriodoAcademico.Carrera.IdCarrera);
            ViewBag.CodCurso = new SelectList(model.lstProjectCourse, "Value", "Text", model.evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.Curso.Codigo);

            return View(model);
        }
        */

        // GET: Rubric/Evaluacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Evaluacion evaluacion = db.Evaluacion.Find(id);
            if (evaluacion == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(evaluacion);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                Evaluacion evaluacion = db.Evaluacion.Find(id);
                db.Evaluacion.Remove(evaluacion);
                db.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueEliminadoConExito);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error, MessageResource.ElRegistroNoSePuedeEliminarPorqueDependeDeOtrosRegistros);
                return RedirectToAction("Delete", new { id = id });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
