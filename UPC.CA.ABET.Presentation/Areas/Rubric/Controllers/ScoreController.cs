using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.Score.PartialView;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Score;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric;
using System.Threading.Tasks;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class ScoreController : BaseController
    {
        // GET: Rubric/Score

        [AuthorizeUserAttribute(AppRol.Docente)]
        public ActionResult ScoreIndex()
        {
            var docenteId = Session.GetDocenteId();
            var viewModel = new ScoreIndexViewModel();
            viewModel.CargarDatos(CargarDatosContext(), docenteId);
            return View(viewModel);
        }

        #region NIVEL DE DESEMPE�O 2017-2
        public async Task<ActionResult> ScoreCreate(int? CycleId)
        {
            var model = new ScoreViewModel();

            await model.Fill(int.Parse(ModalityId), CycleId);

            ViewBag.Title = RubricManagmentResource.Titulo + model.Title;
            return View(model);
        }

        public JsonResult GetNivelDesempenio(Int32 IdNivelDesempenio)
        {
            try
            {
                var nivelDesempenio = Context.NivelDesempenio.FirstOrDefault(x => x.IdNivelDesempenio == IdNivelDesempenio);
                var rubricasCalificadas = nivelDesempenio.RubricaNivelDesempenio.SelectMany(x => x.Rubrica.RubricaCalificada).ToList();

                return Json(new {
                    nivelDesempenio.IdNivelDesempenio,
                    nivelDesempenio.Nombre,
                    nivelDesempenio.Descripcion,
                    nivelDesempenio.PuntajeMenor,
                    nivelDesempenio.PuntajeMayor,
                    nivelDesempenio.Color,
                    nivelDesempenio.TipoEvaluacion,
                    ExisteEvaluacion = (rubricasCalificadas.Count > 0 ) ? true : false
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public JsonResult SaveNivelDesempenio(ScoreViewModel model)
        {
            try     
            {
                var cycleId = model.CycleId;
                NivelDesempenio objNivelDesempenio;
                if (model.IdNivelDesempenio.HasValue)
                    objNivelDesempenio = Context.NivelDesempenio.FirstOrDefault(x => x.IdNivelDesempenio == model.IdNivelDesempenio);
                else
                {
                    objNivelDesempenio = new NivelDesempenio();
                    Context.NivelDesempenio.Add(objNivelDesempenio);
                    var existePuntaje = Context.NivelDesempenio.Any(x => x.PuntajeMayor == model.PuntajeMayor && x.TipoEvaluacion.Equals(model.TipoEvaluacion) && x.IdPeriodoAcademico== cycleId);
                    var existeNombreNivel = Context.NivelDesempenio.Any(x => x.Nombre.Contains(model.Nombre.Trim()) && x.TipoEvaluacion.Equals(model.TipoEvaluacion) && x.IdPeriodoAcademico== cycleId);

                    if (existeNombreNivel)
                        throw new Exception("Name error");
                    if (existePuntaje)
                        throw new Exception("Score error");
                }

                objNivelDesempenio.Nombre = model.Nombre;
                objNivelDesempenio.PuntajeMenor = model.PuntajeMenor;
                objNivelDesempenio.PuntajeMayor = model.PuntajeMayor;
                objNivelDesempenio.Color = String.Format("#{0}", model.Color);
                objNivelDesempenio.TipoEvaluacion = model.TipoEvaluacion;
                objNivelDesempenio.Descripcion = model.Descripcion;
                objNivelDesempenio.IdPeriodoAcademico = cycleId;
                Context.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new { Success = false, e.Message });
            }
            return Json(new { Success = true, Message = "Successful" });
        }

        public JsonResult DeleteNivelDesempenio(Int32 IdNivelDesempenio)
        {
            try
            {
                var objNivelDesempenio = Context.NivelDesempenio.FirstOrDefault(x => x.IdNivelDesempenio == IdNivelDesempenio);
                Context.NivelDesempenio.Remove(objNivelDesempenio);
                Context.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        public ActionResult _ListScorePartialView(int? CycleId)
        {
            try
            {
            var model = new ListScoreViewModel(Context);
            model.Fill(int.Parse(ModalityId), CycleId);
            return PartialView("PartialView/_ListScorePartialView", model);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region CALIFICACI�N CUALITATIVA 2017-2
        public ActionResult _QualitativeQualifyPartialView(Int32 IdProyectoAcademico, Int32 IdEvaluador, String TipoEvaluador, Int32 IdTipoEvaluacion)
        {
            var viewModel = new QualitativeQualifyViewModel();
            viewModel.Fill(CargarDatosContext(), IdProyectoAcademico, IdEvaluador, TipoEvaluador, IdTipoEvaluacion);
            return PartialView("PartialView/_QualitativeQualify", viewModel);
        }
        
        [HttpPost]
        public JsonResult _QualitativeQualifyPartialView(QualitativeQualifyViewModel model)
        {
            try
            {
                foreach (var alumno in model.alumnosProyecto)
                {
                    var Rubrica = Context.Rubrica.FirstOrDefault(x => x.IdRubrica == alumno.IdRubrica);
                    var RubricaCalificada = Context.RubricaCalificada.FirstOrDefault(x => x.IdAlumnoSeccion == alumno.IdAlumnoSeccion && x.IdRubrica == Rubrica.IdRubrica && x.IdEvaluador == model.IdEvaluador);

                    if (RubricaCalificada == null)
                    {
                        RubricaCalificada = new RubricaCalificada();
                        Context.RubricaCalificada.Add(RubricaCalificada);
                    }

                    RubricaCalificada.Rubrica = Rubrica;
                    RubricaCalificada.IdAlumnoSeccion = alumno.IdAlumnoSeccion;
                    RubricaCalificada.IdEvaluador = model.IdEvaluador;
                    RubricaCalificada.NotaMaxima = Rubrica.NotaMaxima;
                    RubricaCalificada.Observacion = alumno.Observacion;
                    RubricaCalificada.FechaCalifiacion = DateTime.Now;
                    RubricaCalificada.NotaMaxima = Rubrica.NotaMaxima;
                    RubricaCalificada.Guardada = true;
                    Rubrica.SePuedeEditar = false;
                    Context.SaveChanges();
                }
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        #endregion

        #region EVALUACI�N DE R�BRICA 2017-2
        [AuthorizeUserAttribute(AppRol.Docente)]
        
        public ActionResult RubricRate(Int32 IdProyectoAcademico, Int32 IdTipoEvaluacion, Int32 IdEvaluador)
        {
            
            var viewModel = new RubricRateViewModel();
            //int? IdDocenteActual = Session.GetDocenteId();
            Int32 RubricaGestion = viewModel.Fill(CargarDatosContext(), IdProyectoAcademico, IdTipoEvaluacion, IdEvaluador);

            if (RubricaGestion == 1) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaSinAlumnos); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else if (RubricaGestion == 2)
            { PostMessage(MessageType.Info, ScoreIndexResource.AlertaRubricaExiste); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else if(RubricaGestion == 3)
            { PostMessage(MessageType.Info, ScoreIndexResource.AlertaOutcomes); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else if(RubricaGestion == -1)
            { PostMessage(MessageType.Error); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }

            if (!viewModel.NombreEvaluacion.Equals("PA"))
                return View("RubricQualifyABET", viewModel);
            else
                return View("RubricQualifyWASC", viewModel);
        }

        public JsonResult SaveRate(int IdRubrica, int IdAlumnoSeccion, int IdOutcomeRubrica, int IdCriterioOutcome, int IdEvaluador, decimal NotaCriterio, int IdNivelCriterio)
        {
            if (IdNivelCriterio > 0)
            {
                var NivelCriterio = Context.NivelCriterio.FirstOrDefault(x => x.IdNivelCriterio == IdNivelCriterio);
                return Json(new { NotaAlumno = RubricLogic.SaveRate(CargarDatosContext(), IdRubrica, IdAlumnoSeccion, IdOutcomeRubrica, IdCriterioOutcome, IdEvaluador, NotaCriterio), NotaCriterio, NivelCriterio.NotaMenor, NivelCriterio.NotaMayor });
            }
            return Json(RubricLogic.SaveRate(CargarDatosContext(), IdRubrica, IdAlumnoSeccion, IdOutcomeRubrica, IdCriterioOutcome, IdEvaluador, NotaCriterio));
        }

        public JsonResult UpdateRate(int IdRubrica, int IdAlumnoSeccion, int IdOutcomeRubrica, int IdCriterioOutcome, int IdEvaluador, decimal NotaCriterio, int IdNivelCriterio)
        {
            if (IdNivelCriterio > 0)
            {
                var NivelCriterio = Context.NivelCriterio.FirstOrDefault(x => x.IdNivelCriterio == IdNivelCriterio);

                if (NotaCriterio > NivelCriterio.NotaMayor || NotaCriterio < NivelCriterio.NotaMenor)
                    return Json(new { success = false, responseText = "Puntaje ingresado fuera del rango seleccionado." }, JsonRequestBehavior.AllowGet);
                return Json(new { success = true, NotaAlumno = RubricLogic.SaveRate(CargarDatosContext(), IdRubrica, IdAlumnoSeccion, IdOutcomeRubrica, IdCriterioOutcome, IdEvaluador, NotaCriterio) });
            }
            return Json(RubricLogic.SaveRate(CargarDatosContext(), IdRubrica, IdAlumnoSeccion, IdOutcomeRubrica, IdCriterioOutcome, IdEvaluador, NotaCriterio));
        }

        public JsonResult SaveRateStudents(Int32 IdProyectoAcademico, Int32 IdTipoEvaluacion, Int32 IdEvaluador)
        {
            try
            {
                var Evaluador = Context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
                var Alumnos = Context.ProyectoAcademico.FirstOrDefault(x => x.IdProyectoAcademico == IdProyectoAcademico).AlumnoSeccion;
                Alumnos = Alumnos.Where(x => !x.AlumnoMatriculado.Equals("RTE")).ToList();
                foreach(var Alumno in Alumnos)
                {
                    Int32 IdCarrera = Alumno.AlumnoMatriculado.IdCarrera.Value;
                    Int32 IdCursoPeriodoAcademico = Alumno.Seccion.IdCursoPeriodoAcademico;
                    CarreraCursoPeriodoAcademico CarreraCurso = Context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdCursoPeriodoAcademico == IdCursoPeriodoAcademico);
                    Evaluacion Evaluacion = CarreraCurso.Evaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion);
                    Rubrica Rubrica = Context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == Evaluacion.IdEvaluacion);

                    RubricaCalificada RubricaCalificada = Context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == Rubrica.IdRubrica && x.IdAlumnoSeccion == Alumno.IdAlumnoSeccion &&
                                                                                                   x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador);
                    if (RubricaCalificada == null)
                        throw new Exception(MessageResource.DebeCalificarAmbosAlumnos);

                    if(RubricaCalificada.Observacion.Trim().Equals("") && !Evaluacion.TipoEvaluacion.Tipo.Equals("PA"))
                        throw new Exception(MessageResource.DebeIngresaryGuardarLasObservacionesParaAmbosAlumnos);

                    var criteriosRubrica = Rubrica.OutcomeRubrica.Select(x => x.CriterioOutcome.Count()).Sum();
                    var criteriosCalificado = RubricaCalificada.OutcomeCalificado.Select(x => x.CriterioCalificado.Count()).Sum();
                    /*
                    if (Rubrica.OutcomeRubrica.Count > RubricaCalificada.OutcomeCalificado.Count)
                        throw new Exception("You must qualify all outcomes of all students");
                        */
                    //var objAlumnoMatriculado = context.AlumnoMatriculado.Where(x => x.IdAlumnoMatriculado == Alumno.IdAlumnoMatriculado).FirstOrDefault();

                    if (criteriosRubrica > criteriosCalificado)
                        throw new Exception(MessageResource.DebeCalificarAmbosAlumnos); //+ objAlumnoMatriculado!=null?objAlumnoMatriculado.Alumno.Nombres:"1 and 2");

                    RubricaCalificada.Guardada = true;
                    Context.SaveChanges();
                }

                return Json(new { Result = true, Message = MessageResource.LaCalificacionFueGuardadaExitosamente });
                }
            catch (Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }

        public JsonResult SaveObservacion(Int32 IdRubrica, Int32 IdAlumnoSeccion, Int32 IdEvaluador, String Observacion)
        {
            try
            {
                var Evaluador = Context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
                RubricaCalificada RubricaCalificada;
                RubricaCalificada = Context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == IdRubrica && x.IdAlumnoSeccion == IdAlumnoSeccion);
                if (RubricaCalificada == null)
                {
                    RubricaCalificada = new RubricaCalificada();
                    Context.RubricaCalificada.Add(RubricaCalificada);
                    RubricaCalificada.IdRubrica = IdRubrica;
                    RubricaCalificada.IdAlumnoSeccion = IdAlumnoSeccion;
                    RubricaCalificada.IdEvaluador = IdEvaluador;
                    RubricaCalificada.FechaCalifiacion = DateTime.Now;
                    RubricaCalificada.Guardada = false;
                }

                RubricaCalificada.Observacion = Observacion.Trim();
                if (RubricaCalificada.Observacion.Equals(""))
                    RubricaCalificada.Guardada = false;

                Context.SaveChanges();
                return Json(true);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        #endregion

        public ActionResult _ListAcademicProjectPartialView(Int32 EmpresaVirtualId, Int32 DocenteId,String CodigoAlumno)
        {
            var viewModel = new ListAcademicProjectPartialViewModel();
            viewModel.CargarDatos(CargarDatosContext(), EmpresaVirtualId, DocenteId, CodigoAlumno);
            return PartialView("PartialView/_ListAcademicProjectPartialView", viewModel);
        }
        
        [AuthorizeUserAttribute(AppRol.Docente)]
        public ActionResult RubricQualify(Int32 TipoEvaluacionId, Int32 TipoEvaluadorId, Int32 ProyectoAcademicoId, Int32 EvaluadorId)
        {
            var viewModel = new RubricQualifyViewModel();
            Int32 RubricaGestion = viewModel.CargarDatos(CargarDatosContext(), TipoEvaluacionId, TipoEvaluadorId, ProyectoAcademicoId, EvaluadorId);


            if (RubricaGestion == 1) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaRubricaExiste); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else
                if (RubricaGestion == 2) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaRubricaCalificadaExiste); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else
                    if (RubricaGestion == 3) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaFaltaCalificaciones); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else
                        if (RubricaGestion == 4) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaComitePA); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else
                            if (RubricaGestion == 5) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaOutcomes); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }
            else
                                if (RubricaGestion == 6) { PostMessage(MessageType.Info, ScoreIndexResource.AlertaSinAlumnos); return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() }); }


            return View("RubricQualify", viewModel);
        }

        [HttpPost]
        public ActionResult SaveRubricQualify(FormCollection collection)
        {
            Dictionary<String, String> Matches = new Dictionary<string, string>();

            foreach (var item in collection.AllKeys)
            {
                var value = collection[item];
                Matches.Add(item, value);
            }
            RubricLogic.SaveRubricQualify(CargarDatosContext().context, Matches);
            PostMessage(MessageType.Success);
            return RedirectToAction("ScoreIndex", new { DocenteId = Session.GetDocenteId() });
        }
        [AuthorizeUserAttribute(AppRol.Docente, AppRol.CoordinadorCarrera,AppRol.GerenteGeneral)]
        public ActionResult ListRubricaAlumno()
        {
            var viewModel = new ListRubricaAlumnoViewModel();
            viewModel.CargarDatos(CargarDatosContext());
            return View(viewModel);
        }

        public ActionResult _ListAcademicTypeProjectPartialView(Int32? EmpresaVirtualId,String CodigoAlumno, Int32? FiltroAll)
        {
            var viewModel = new ListAcademicTypeProjectPartialViewModel();
            Int32? idDocente = Session.GetDocenteId();
            viewModel.CargarDatos(CargarDatosContext(), EmpresaVirtualId, idDocente,CodigoAlumno, FiltroAll);
            return PartialView("PartialView/_ListAcademicTypeProjectPartialView", viewModel);
        }
        [AuthorizeUserAttribute(AppRol.Docente, AppRol.CoordinadorCarrera)]
        public ActionResult ViewRubricQualify(Int32? IdRubricaCalificada,Int32 AlumnoSeccionId, String nombreEvaluacion)
        {
            var viewModel = new ViewRubricQualifyViewModel();
            viewModel.CargarDatos(CargarDatosContext(), IdRubricaCalificada, AlumnoSeccionId, nombreEvaluacion);
            return View(viewModel);
        }
        public JsonResult GetStudents(String CadenaBuscar)
        {
            Int32 periodoId = Session.GetPeriodoAcademicoId().Value;
            var query = Context.Alumno.AsQueryable();
            if (!String.IsNullOrEmpty(CadenaBuscar))
                foreach (var token in CadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));
            query = query.Where(
                x => x.AlumnoMatriculado.
                Any(y => y.AlumnoSeccion.
               Any(z => z.Seccion.CursoPeriodoAcademico.Curso.NombreEspanol.Contains("Taller de Proyecto")
               && z.Seccion.CursoPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoId)));

            var data = query.ToList().Select(x => new { id = x.Codigo, text = x.Nombres + " " + x.Apellidos });
            return Json(data);
        }

        public ActionResult LstAllRubrics(Int32? page)
        {
            var model = new LstScoreRubricViewModel();
            //SUBMODALIDAD=1
            model.CargarDatos(CargarDatosContext(),page,string.Empty,1);
            return View(model);
        }
                
        public ActionResult _LstRubricQualifyDetail(Int32? page,String CampoBuscar)
        {
            var model = new LstScoreRubricViewModel();
            TempData["CampoBuscar"] = CampoBuscar;
            //SUBMODALIDAD=1
            model.CargarDatos(CargarDatosContext(), page,CampoBuscar,1);
            return PartialView("PartialView/_LstRubricQualifyDetail",model.LstRubricasCalificadas);
        }

        [HttpPost]
        public ActionResult DeleteGradesProject(Int32 ProyectoID, String TipoEvaluacion, Int32 Page)
        {
            RubricLogic.DeleteGradesByProject(CargarDatosContext(), ProyectoID, TipoEvaluacion);
            var model = new LstScoreRubricViewModel();
            //SUBMODALIDAD=1
            model.CargarDatos(CargarDatosContext(), Page,string.Empty,1);
            return PartialView("PartialView/_LstRubricQualifyDetail", model.LstRubricasCalificadas);
        }
    }
}