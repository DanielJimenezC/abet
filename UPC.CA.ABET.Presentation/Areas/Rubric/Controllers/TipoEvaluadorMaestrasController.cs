﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class TipoEvaluadorMaestrasController : BaseController
    {
        private AbetEntities db = new AbetEntities();

        // GET: Rubric/TipoEvaluadorMaestras
        public async Task<ActionResult> Index()
        {
            var data = db.TipoEvaluadorMaestra.Where(x => x.IdEscuela == this.EscuelaId).ToListAsync();
            return View(await data);
         
        }

       /*
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEvaluadorMaestra tipoEvaluadorMaestra = await db.TipoEvaluadorMaestra.FindAsync(id);
            if (tipoEvaluadorMaestra == null)
            {
                return HttpNotFound();
            }
            return View(tipoEvaluadorMaestra);
        }
        */
        // GET: Rubric/TipoEvaluadorMaestras/Create
        public ActionResult Create()
        {
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idTipoEvaluador,Codigo,Nombre")] TipoEvaluadorMaestra tipoEvaluadorMaestra)
        {

            var tipo = db.TipoEvaluadorMaestra.Where(x => x.Codigo == tipoEvaluadorMaestra.Codigo && x.IdEscuela == this.EscuelaId).FirstOrDefault();

            if (tipo != null)
            {
                PostMessage(MessageType.Warning, MessageResource.ElRegistroYaExisteEnLaBaseDeDatos);
            }
            else
            {
                tipoEvaluadorMaestra.IdEscuela = this.EscuelaId;
                db.TipoEvaluadorMaestra.Add(tipoEvaluadorMaestra);
                await db.SaveChangesAsync();

                PostMessage(MessageType.Success, MessageResource.ElRegistroFueRealizadoConExito);

                return RedirectToAction("Index");

            }

            return View(tipoEvaluadorMaestra);
        }

        // GET: Rubric/TipoEvaluadorMaestras/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluadorMaestra tipoEvaluadorMaestra = await db.TipoEvaluadorMaestra.Where(x => x.idTipoEvaluador == id && x.IdEscuela == this.EscuelaId).FirstOrDefaultAsync();
            if (tipoEvaluadorMaestra == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(tipoEvaluadorMaestra);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idTipoEvaluador,Codigo,Nombre,IdEscuela")] TipoEvaluadorMaestra tipoEvaluadorMaestra)
        {

            try
            {

                db.Entry(tipoEvaluadorMaestra).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueActualizadoConExito);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error);
                return View(tipoEvaluadorMaestra);
            }
            
        }

        // GET: Rubric/TipoEvaluadorMaestras/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluadorMaestra tipoEvaluadorMaestra = await db.TipoEvaluadorMaestra.FindAsync(id);
            if (tipoEvaluadorMaestra == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(tipoEvaluadorMaestra);
        }

        // POST: Rubric/TipoEvaluadorMaestras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TipoEvaluadorMaestra tipoEvaluadorMaestra = await db.TipoEvaluadorMaestra.FindAsync(id);
            db.TipoEvaluadorMaestra.Remove(tipoEvaluadorMaestra);
            await db.SaveChangesAsync();
            PostMessage(MessageType.Success, MessageResource.ElRegistroFueEliminadoConExito);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
