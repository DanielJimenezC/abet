﻿
#region Imports

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.BulkInsert;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.BulkInsert;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelReports;
using ClosedXML.Excel;
using System.Linq;
using Ionic.Zip;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Models;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{

    public class BulkInsertController : BaseController
    {
        #region Get


        public ActionResult GetTemplate(BulkInsertType Type)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;
            String nombreReporte = string.Empty;

            try
            {
                Int32 periodoAcademicoActualId = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

                switch (Type)
                {
                    case BulkInsertType.OutcomeGoalCommission:
                        nombreReporte = "";
                        break;
                    case BulkInsertType.VirtualCompany:
                        nombreReporte = "5.EMPRESAS VIRTUALES CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.AcademicProject:
                        nombreReporte = "7.PROYECTOS ACADEMICOS CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.EvaluationType:
                        nombreReporte = "3.TIPO DE EVALUACIONES CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.EvaluatorType:
                        nombreReporte = "2.ROLES DE EVALUADORES CARGA MASIVA POR CURSO.xlsx";
                        break;
                    case BulkInsertType.Evaluation:
                        nombreReporte = "4.EVALUACIONES CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.Evaluator:
                        nombreReporte = "EVALUADORES CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.GrupoComite:
                        nombreReporte = "6.GRUPO COMITE CARGA MASIVA.xlsx";
                        break;  
                    case BulkInsertType.EvaluatorTypeMaster:
                        nombreReporte = "ROLES DE EVALUADORES CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.CoursesToEvaluation:
                        nombreReporte = "1.CURSOS PARA EVALUAR RUBRICA CARGA MASIVA.xlsx";
                        break;
                    case BulkInsertType.RubricToEvaluation:
                        nombreReporte = "Formato_Carga_RV.xlsx";
                        break;
                    default:
                        break;
                }
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + nombreReporte);

                objExcel = new XLWorkbook(path);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult IndexUpload()
        {
            return View();
        }


        [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        /// <summary>
        /// Retorna la página de Carga Masiva de Archivos Excel, la cual es renderizada según el
        /// tipo de carga elegida en el sidebar principal.
        /// </summary>
        /// <param name="Type">Enumerador que determina el tipo de carga masiva</param>
        /// <returns>View</returns>
        public ActionResult UploadExcelFiles(BulkInsertType Type = BulkInsertType.VirtualCompany)
        {
            var viewModel = new UploadExcelFilesViewModel();
            viewModel.CargarDatos(Type);

            if(Type == BulkInsertType.EvaluatorType)
            {
                viewModel.lstTipoEvaluadorMaestra = Context.TipoEvaluadorMaestra.ToList();
            }

            TempData["DicMensajes"] = viewModel.DicMensajes;
            return View(viewModel);
        }

        //[AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult OutcomeReport()
        {
            var viewModel = new OutcomeReportViewModel();
            viewModel.CargarDatos(CargarDatosContext(),this.EscuelaId);
            return View(viewModel);
        }


        //[AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult DownloadQualifyStatus()
        {

            return View();
        }
        //comentario de pruebas
        //[AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult DownloadReport (String Name)
        {
            MemoryStream objStream = null;
            List<String> lstRutas = null;
            try
            {
                var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.TEMPLATE_STATUS_REPORT));
                string nombreReporte = string.Empty;

                if (Name.Equals("Pendientes"))
                {
                    var proyectosPendientes = Context.sp_LstProyectosPendientes(this.EscuelaId).ToList();
                    var objExcel = new XLWorkbook(templatePath);
                    var worksheet = objExcel.Worksheet(1);
                    nombreReporte = "Estatus Calificaciones pendientes.xlsx";
                    worksheet.Name = "Pendientes de calificacion";
                    int cont = 2;
                    foreach (var item in proyectosPendientes)
                    {
                        worksheet.Cell("A" + cont).Value = item.Codigo;
                        worksheet.Cell("B" + cont).Value = item.Nombre;
                        worksheet.Cell("C" + cont).Value = item.Docente;
                        worksheet.Cell("D" + cont).Value = item.Evaluador;
                        cont++;
                    }

                    objStream = new MemoryStream();

                    objExcel.SaveAs(objStream);
                    byte[] fileBytes = objStream.ToArray();

                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
                }
                else
                {
                    var proyectosListosACalificart = Context.sp_LstProyectosACalificarComite().ToList();
                    var objExcel = new XLWorkbook(templatePath);
                    var worksheet = objExcel.Worksheet(1);
                    nombreReporte = "Estaus Proyectos listos.xlsx";
                    worksheet.Name = "Listos a calificar por Comite";
                    int cont = 2;
                    worksheet.Cell("C1").Value = "Curso";
                    worksheet.Cell("D1").Value = "Docente";
                    foreach (var item in proyectosListosACalificart)
                    {
                        worksheet.Cell("A" + cont).Value = item.Codigo;
                        worksheet.Cell("B" + cont).Value = item.Nombre;
                        worksheet.Cell("C" + cont).Value = item.TipoCurso;
                        worksheet.Cell("D" + cont).Value = item.Docente;
                        cont++;
                    }

                    objStream = new MemoryStream();

                    objExcel.SaveAs(objStream);
                    byte[] fileBytes = objStream.ToArray();

                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("DownloadQualifyStatus");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }

                if (lstRutas != null)
                {
                    foreach (var ruta in lstRutas)
                    {
                        if (System.IO.File.Exists(ruta))
                        {
                            System.IO.File.Delete(ruta);
                        }
                    }
                }
            }
        }

        [HttpGet]
        public ActionResult DownloadEvaluatorOutcomeReportGet(Int32? ProyectoId, Int32? EvaluacionId)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;

            try
            {
                Int32 periodoAcademicoActualId = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
                String nombreReporte = "Reporte de Outcomes por Evaluador.xlsx";
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_OUTCOME_EVALUADOR);

                objExcel = ExcelReportLogic.GenerateOutcomeEvaluatorReport(Context, path, periodoAcademicoActualId, ProyectoId.Value, EvaluacionId.Value, ref nombreReporte);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        #endregion

        #region EXCEL EVALUACIÓN 2017-2
        [HttpPost]
        public ActionResult DownloadEvaluationReport(Int32 IdProyectoAcademico,  Int32 IdAlumnoSeccion, Int32 IdRubrica, Int32 IdEvaluador, Int32 IdTipoEvaluacion, Int32 idcurso)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;
            
            try
            {
                var Evaluador = Context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
                RubricaCalificada RubricaCalificada = Context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == IdRubrica && x.IdAlumnoSeccion == IdAlumnoSeccion &&
                                                                                                      x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador == Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador);
                if (RubricaCalificada == null)
                    throw new Exception("Debe Calificar todos los Outcomes");



                int idsubmodadlida = Session.GetSubModalidadPeriodoAcademicoId().Value;
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_OUTCOME_EAC_CAC_ZIP);
                String nombreReporte = string.Empty;
                objExcel = ExcelReportLogic.GeneraExcelEvaluacion(CargarDatosContext(), IdProyectoAcademico, IdAlumnoSeccion, IdRubrica, IdEvaluador, IdTipoEvaluacion, path, ref nombreReporte, idsubmodadlida, idcurso);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("RubricRate", "Score", new { IdProyectoAcademico = IdProyectoAcademico, IdTipoEvaluacion = IdTipoEvaluacion, IdEvaluador = IdEvaluador});
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        [HttpPost]
        public ActionResult DownloadEvaluationWASCReport(Int32 IdProyectoAcademico, Int32 IdAlumnoSeccion, Int32 IdRubrica, Int32 IdEvaluador, Int32 IdTipoEvaluacion)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;

            try
            {
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_COMPETENCE_ZIP);
                String nombreReporte = string.Empty;
                objExcel = ExcelReportLogic.GeneraExcelEvaluacionWASC(CargarDatosContext(), IdProyectoAcademico, IdAlumnoSeccion, IdRubrica, IdEvaluador, IdTipoEvaluacion, path, ref nombreReporte);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Error, Presentation.Resources.Views.Shared.MessageResource.OcurrioUnErrorAlIntentarDescargarLaRubrica);
                return RedirectToAction("RubricRate", "Score", new { IdProyectoAcademico = IdProyectoAcademico, IdTipoEvaluacion = IdTipoEvaluacion, IdEvaluador = IdEvaluador });
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }
        #endregion

        public ActionResult DownloadReportAcademicProjectError(BulkInsertLogic.ReturnErrorProyecto data)
        {
            MemoryStream objStream = null;
            List<String> lstRutas = null;
            try
            {
                var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.PROYECTOS_ACADEMICOS_CARGA_MASIVA));
                string nombreReporte = string.Empty;

                
                   
                    var objExcel = new XLWorkbook(templatePath);
                    var worksheet = objExcel.Worksheet(1);
                    nombreReporte = "ERROR PROYECTOS ACADEMICOS CARGA MASIVA.xlsx";
                    worksheet.Name = "PROYECTOS";
                    int cont = 2;
                    var cant = data.lstErrorDescripcion.Count;

                    worksheet.Cell("K1").Value = "ERROR";
                    for (int i=0;i< cant; i++)
                        {
                            var item = data.lstErrorItem[i];
                            worksheet.Cell("A" + cont).Value = item.Codigo;
                            worksheet.Cell("B" + cont).Value = item.Nombre;
                            worksheet.Cell("C" + cont).Value = item.Descripcion;
                            worksheet.Cell("D" + cont).Value = item.Curso;
                            worksheet.Cell("E" + cont).Value = item.CodigoEmpresa;
                            worksheet.Cell("F" + cont).Value = item.CodigoAlumno01;
                            worksheet.Cell("G" + cont).Value = item.CodigoAlumno02;
                            worksheet.Cell("H" + cont).Value = item.CodDocenteCoautor;
                            worksheet.Cell("I" + cont).Value = item.CodDocenteCliente;
                            worksheet.Cell("J" + cont).Value = item.CodGerente;
                            worksheet.Cell("K" + cont).Value = data.lstErrorDescripcion[i];
                        cont++;
                        }

                    objStream = new MemoryStream();

                    objExcel.SaveAs(objStream);
                    byte[] fileBytes = objStream.ToArray();

                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
                
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("DownloadQualifyStatus");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }

                if (lstRutas != null)
                {
                    foreach (var ruta in lstRutas)
                    {
                        if (System.IO.File.Exists(ruta))
                        {
                            System.IO.File.Delete(ruta);
                        }
                    }
                }
            }
        }
        #region Post

        /// <summary>
        /// Realiza la inserción masiva de nuevos registros cargados de un archivol Excel.
        /// Se obtiene una ruta temporal para el copiado del archivo, el cual es eliminado
        /// al terminar el proceso de cargado.
        /// Se determina los mensajes de validación según sea el tipo de carga masiva, los
        /// cuales son almacenados en un tipo de dato diccionario.
        /// </summary>
        /// <param name="ViewModel">ViewModel de la Carga Masiva</param>
        /// <returns>View</returns>
        //[AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        [HttpPost]
        public ActionResult UploadExcelFiles(UploadExcelFilesViewModel ViewModel)
        {
            var serverPathFile = String.Empty;
            int? idPeriodoAcademico = Session.GetPeriodoAcademicoId();

            try
            {
                String fileExtension = ViewModel.Archivo != null
                    ? System.IO.Path.GetExtension(ViewModel.Archivo.FileName)
                    : String.Empty;
                Boolean respuestaExtension = fileExtension == ConstantHelpers.RUBRICFILETYPE.ExcelType.XLS
                                             || fileExtension == ConstantHelpers.RUBRICFILETYPE.ExcelType.XLSX;

                ViewModel.DicMensajes = ConvertHelpers.ToTempDataValue<Dictionary<String, String>>(TempData, "DicMensajes");
                TempData["DicMensajes"] = ViewModel.DicMensajes;

                if (!respuestaExtension)
                {
                    PostMessage(MessageType.Info, UploadFilesResource.TipoArchivo);
                    return View(ViewModel);
                }

                String respuesta = String.Empty;
                serverPathFile = Path.Combine(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS, ViewModel.Archivo.FileName);
                ViewModel.Archivo.SaveAs(serverPathFile);

                switch (ViewModel.TipoCargaMasiva)
                {
                    case BulkInsertType.VirtualCompany:
                        respuesta = BulkInsertLogic.VirtualCompanyBulkInsert(Context, serverPathFile, ViewModel.DicMensajes, this.EscuelaId);
                        break;
                    case BulkInsertType.AcademicProject:
                        //1 = submodalidad
                        BulkInsertLogic.ReturnErrorProyecto resultado = BulkInsertLogic.AcademicProjectBulkInsert(Context, serverPathFile, ViewModel.DicMensajes, this.EscuelaId, 1);

                        if (resultado.lstErrorItem.Count != 0 && resultado.Mensaje != "OK")
                        {
                           return this.DownloadReportAcademicProjectError(resultado);
                        }
                        else
                        {
                            respuesta = resultado.Mensaje;
                        }
                        break;
                    case BulkInsertType.EvaluationType:
                        respuesta = BulkInsertLogic.EvaluationTypeBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.Evaluation://4
                        respuesta = BulkInsertLogic.EvaluationBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.EvaluatorType://2 edit
                        respuesta = BulkInsertLogic.EvaluatorTypeBulkInsert(Context, serverPathFile, ViewModel.DicMensajes,idPeriodoAcademico);
                        break;
                    case BulkInsertType.EvaluatorTypeMaster:
                        respuesta = BulkInsertLogic.EvaluatorTypeMasterBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.GrupoComite:
                        respuesta = BulkInsertLogic.GrupoComiteBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.Evaluator:
                        respuesta = BulkInsertLogic.EvaluatorBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.CoursesToEvaluation:
                        respuesta = BulkInsertLogic.CoursesToEvaluationBulkInsert(Context, serverPathFile, ViewModel.DicMensajes);
                        break;
                    case BulkInsertType.OutcomeGoalCommission:
                        respuesta = BulkInsertLogic.OutcomeGoalCommissionBulkInsert(Context, serverPathFile, ViewModel.DicMensajes,1);
                        break;
                }

                if (respuesta != ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO)
                {
                    PostMessage(MessageType.Info, respuesta);
                    return RedirectToAction("UploadExcelFiles", new { type = ViewModel.TipoCargaMasiva });
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("IndexUpload", "BulkInsert");
            }
            finally
            {
                if (System.IO.File.Exists(serverPathFile))
                {
                    System.IO.File.Delete(serverPathFile);
                }
            }

            PostMessage(MessageType.Success);
            return RedirectToAction("IndexUpload", "BulkInsert");
        }


        /// <summary>
        /// Obtiene el reporte de notas de fin de ciclo de los alumnos de TP1 y TP2
        /// desacrgando un archivo Excel basado en una plantilla
        /// </summary>
        /// <returns>File Result</returns>
        //[AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult DownloadStudentGradeReport()
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;

            try
            {
                //Int32 periodoAcademicoActualId = context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

                Int32 periodoAcademicoActualId = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;


                String nombreReporte = "Reporte de Fin de Ciclo.xlsx";
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_NOTAS_FINALES);

                objExcel = ExcelReportLogic.GenerateStudentGradeReport(Context, path, periodoAcademicoActualId,this.EscuelaId);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        [HttpPost]
        public ActionResult DownloadStudentOutcomeReport(Int32? AlumnoSeccionId, String NombreEvaluacion)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;

            try
            {
                Int32 periodoAcademicoActualId = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
                String nombreReporte = string.Empty;

                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_OUTCOME_ZIP);

                objExcel = ExcelReportLogic.GenerateSingleStudentCriterioReport(Context, AlumnoSeccionId.Value, NombreEvaluacion, path, ref nombreReporte);

                if (objExcel == null)
                {
                    PostMessage(MessageType.Warning, Presentation.Resources.Views.Shared.MessageResource.InconsistenciaDeInformacion);
                    return RedirectToAction("Index", "Home");
                }
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        [HttpPost]
        public ActionResult DownloadEvaluatorOutcomeReport(Int32? ProyectoId, Int32? EvaluacionId)
        {
            XLWorkbook objExcel = null;
            MemoryStream objStream = null;

            try
            {
                Int32 periodoAcademicoActualId = Context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
                String nombreReporte = "Reporte de Outcomes por Evaluador.xlsx";
                var path = Server.MapPath(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS + ConstantHelpers.TEMPLATE_OUTCOME_EVALUADOR);

                objExcel = ExcelReportLogic.GenerateOutcomeEvaluatorReport(Context, path, periodoAcademicoActualId, ProyectoId.Value, EvaluacionId.Value,ref nombreReporte);
                objStream = new MemoryStream();

                objExcel.SaveAs(objStream);
                byte[] fileBytes = objStream.ToArray();

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }
                if (objExcel != null)
                {
                    objExcel.Dispose();
                    objExcel = null;
                }
            }
        }

        [HttpPost]
        public ActionResult DownloadZipReport(OutcomeReportViewModel ViewModel)
        {
            MemoryStream objStream = null;
            List<String> lstRutas = null;
            
            try
            {
                var objCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == ViewModel.IdCurso);
                string curso = objCurso.NombreEspanol;
                String ciclo =
                    Context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == ViewModel.IdPeriodoAcademico)
                        .CicloAcademico;

                String nombreReporte = curso + "_" + ciclo + ".zip";
              
                var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.TEMPLATE_OUTCOME_EAC_CAC_ZIP));
                var lstExcel = ExcelReportLogic.GenerateListCriterioReport(Context, ViewModel.IdCarrera, ViewModel.IdPeriodoAcademico,
                    ConstantHelpers.RUBRICFILETYPE.EvaluationType.TF, objCurso.Codigo, templatePath,this.EscuelaId, CargarDatosContext());

                if (lstExcel.Any())
                {
                    var objZip = new ZipFile();
                    lstRutas = new List<String>();

                    foreach (var objExcel in lstExcel)
                    {
                        var nombre = ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + objExcel.Worksheet(1).Name + ".xlsx";
                        objExcel.SaveAs(nombre);
                        objZip.AddFile(nombre);
                        lstRutas.Add(nombre);
                    }

                    objStream = new MemoryStream();
                    objZip.Save(objStream);
                    byte[] fileBytes = objStream.ToArray();
                    
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Zip, nombreReporte);
                }
                else
                {
                    PostMessage(MessageType.Info, OutcomeReportResource.Datos);
                    return RedirectToAction("OutcomeReport");
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("OutcomeReport");
            }
            finally
            {
                if (objStream != null)
                {
                    objStream.Dispose();
                    objStream = null;
                }

                if (lstRutas != null)
                {
                    foreach (var ruta in lstRutas)
                    {
                        if (System.IO.File.Exists(ruta))
                        {
                            System.IO.File.Delete(ruta);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
