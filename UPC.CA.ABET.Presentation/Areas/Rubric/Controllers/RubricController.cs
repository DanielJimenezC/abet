﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.PartialView;
using System.Linq;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Logic;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
    public class RubricController : BaseController
    {
        public ActionResult RubricIndex()
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarDatos(CargarDatosContext());
            return View(viewModel);
        }

        #region MANTENIMIENTO DE RÚBRICAS 2017-2
        public ActionResult MaintenanceRubric()
        {
            MaintenanceRubricViewModel viewModel = new MaintenanceRubricViewModel();
            viewModel.Fill(CargarDatosContext());
            return View(viewModel);
        }

        public ActionResult RubricCreateSummary()
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarDatos(CargarDatosContext());
            return View(viewModel);
        }

        public ActionResult VerRubrica(Int32 IdRubrica)
        {
            RubricViewModel model = new RubricViewModel();
            var Rubrica = Context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            var CarreraCursoPeriodoAcademico = Rubrica.Evaluacion.CarreraCursoPeriodoAcademico;
            model.IdEvaluacion = Rubrica.IdEvaluacion;
            model.IdCarrera = Context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == CarreraCursoPeriodoAcademico.IdCarrera && x.IdSubModalidadPeriodoAcademico== CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico).IdCarreraPeriodoAcademico;
            model.IdCurso = CarreraCursoPeriodoAcademico.IdCarreraCursoPeriodoAcademico;

            if (!Rubrica.Evaluacion.TipoEvaluacion.Tipo.Equals("PA"))
            {
                var viewModel = new RubricABETViewModel();
                viewModel.Fill(CargarDatosContext(), model);
                return View("RubricABETView", viewModel);
            }
            else
            {
                var viewModel = new RubricWASCViewModel();
                viewModel.Fill(CargarDatosContext(), model);
                return View("RubricWASCView", viewModel);
            }
        }

        public ActionResult EditRubrica(Int32 IdRubrica)
        {
            RubricViewModel model = new RubricViewModel();
            var Rubrica = Context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            var CarreraCursoPeriodoAcademico = Rubrica.Evaluacion.CarreraCursoPeriodoAcademico;
            model.IdEvaluacion = Rubrica.IdEvaluacion;
            model.IdCarrera = Context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == CarreraCursoPeriodoAcademico.IdCarrera && x.IdSubModalidadPeriodoAcademico== CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico).IdCarreraPeriodoAcademico;
            model.IdCurso = CarreraCursoPeriodoAcademico.IdCarreraCursoPeriodoAcademico;

            var TipoEvaluacion = Context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
            if (!TipoEvaluacion.Equals("PA"))
            {
                var viewModel = new RubricABETViewModel();
                viewModel.Fill(CargarDatosContext(), model);
                return View("RubricABETCreate", viewModel);
            }
            else
            {
                var viewModel = new RubricWASCViewModel();
                viewModel.Fill(CargarDatosContext(), model);
                return View("RubricWASCCreate", viewModel);
            }
        }

        public JsonResult DeleteRubrica(Int32 IdRubrica)
        {
            try
            {
                var objRubrica = Context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
                var NivelesDesempenio = Context.RubricaNivelDesempenio.Where(x => x.IdRubrica == IdRubrica);
                var NivelesCriterio = Context.NivelCriterio.Where(x => x.CriterioOutcome.OutcomeRubrica.IdRubrica == IdRubrica);
                var CriteriosRubrica = Context.CriterioOutcome.Where(x => x.OutcomeRubrica.IdRubrica == IdRubrica);
                var OutcomesEvaluador = Context.OutcomeEvaluador.Where(x => x.OutcomeRubrica.IdRubrica == IdRubrica);
                var OutcomesRubrica = Context.OutcomeRubrica.Where(x => x.IdRubrica == IdRubrica);
                Context.OutcomeEvaluador.RemoveRange(OutcomesEvaluador);
                Context.RubricaNivelDesempenio.RemoveRange(NivelesDesempenio);
                Context.NivelCriterio.RemoveRange(NivelesCriterio);
                Context.CriterioOutcome.RemoveRange(CriteriosRubrica);
                Context.OutcomeRubrica.RemoveRange(OutcomesRubrica);
                Context.Rubrica.Remove(objRubrica);
                Context.SaveChanges();
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        public ActionResult _ListRubricPartialView(Int32? IdCiclo, Int32? IdCarrera)
        {
            var model = new ListRubricViewModel();
            model.Fill(CargarDatosContext(), IdCiclo, IdCarrera);
            return PartialView("PartialView/_ListRubricPartialView", model);
        }

        public JsonResult GetCarreraByCiclo(Int32 IdCiclo)
        {
            try
            {
                var IdEscuela = Session.GetEscuelaId();
                var lstCarreras = Context.CarreraPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico== IdCiclo && x.Carrera.IdEscuela == IdEscuela)
                                    .Select(x => new { NombreEspanol = (currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.Carrera.NombreEspanol : x.Carrera.NombreIngles), x.IdCarrera }).ToList();
                return Json(lstCarreras);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        
        #endregion

        #region REGISTRO DE RÚBRICAS 2017-2
        public ActionResult CrearContinuarRubrica(RubricViewModel Rubrica)
        {
            try
            {
                if (!RubricLogic.RubricExists(CargarDatosContext().context, Rubrica.IdEvaluacion))
                {
                    PostMessage(MessageType.Success);
                    var TipoEvaluacion = Context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
                    if (TipoEvaluacion.Equals("PA"))
                    {
                        var viewModel = new RubricWASCViewModel();
                        RubricPresentationLogic.CrearRubricaWASC(CargarDatosContext(), Rubrica);
                        viewModel.Fill(CargarDatosContext(), Rubrica);
                        return View("RubricWASCCreate", viewModel);
                    }
                    else
                    {
                        var viewModel = new RubricABETViewModel();
                        RubricPresentationLogic.CrearRubricaABET(CargarDatosContext(), Rubrica);
                        viewModel.Fill(CargarDatosContext(), Rubrica);
                        return View("RubricABETCreate", viewModel);
                    }
                }
                else
                    PostMessage(MessageType.Info, MessageResource.YaExisteUnaRubricaDeLaEvaluacionSeleccionada);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
            }
            return RedirectToAction("RubricCreateSummary");
        }

        public JsonResult SaveCriterio(Int32? IdCriterio, Int32 IdRubrica, Int32? IdOutcome, String Descripcion)
        {
            try
            {
                CriterioOutcome objCriterio;
                var Rubrica = Context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
                var NivelesDesempenio = Rubrica.RubricaNivelDesempenio.Select(x => x.NivelDesempenio).ToList();

                if(!NivelesDesempenio.Any())
                {
                    String EvaluacionNombre = Context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == Rubrica.IdEvaluacion).TipoEvaluacion.Tipo;
                    if (!EvaluacionNombre.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
                        NivelesDesempenio = Context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("StudentOutcome")).ToList();
                    else
                        NivelesDesempenio = Context.NivelDesempenio.Where(x => x.TipoEvaluacion.Equals("Competences")).ToList();

                    foreach(var nivel in NivelesDesempenio)
                    {
                        RubricaNivelDesempenio objNivel = new RubricaNivelDesempenio();
                        objNivel.Rubrica = Rubrica;
                        objNivel.NivelDesempenio = nivel;
                        Context.RubricaNivelDesempenio.Add(objNivel);
                    }
                }

                //PARA EVITAR ERROR DE REFERENCIA CIRCULAR NO SOPORTADA POR JSON SERIALIZER AL ENVIARLO COMO RESPUESTA
                var lstNivelDesempenio = NivelesDesempenio
                                        .Select(x => new {
                                            Nombre = x.Nombre,
                                            Puntaje = x.PuntajeMayor,
                                            Descripcion = x.Descripcion,
                                            Color = x.Color
                                        });
                //--------------------------------------------------------------------------

                if (IdCriterio.HasValue)
                {
                    objCriterio = Context.CriterioOutcome.FirstOrDefault(x => x.IdCriterioOutcome == IdCriterio);
                    Context.NivelCriterio.RemoveRange(objCriterio.NivelCriterio); //LA ANTIGUA ESTRUCTURA GUARDABA EN LA TABLA NIVELCRITERIO
                }
                else
                {
                    objCriterio = new CriterioOutcome();
                    objCriterio.IdOutcomeRubrica = IdOutcome.Value;
                    Context.CriterioOutcome.Add(objCriterio);
                }
                objCriterio.Nombre = "Criterio";
                objCriterio.Descripcion = Descripcion;
                objCriterio.ValorMaximo = NivelesDesempenio.Max(x => x.PuntajeMayor).Value;
                objCriterio.EsEvaluadoPorDefecto = true;
                Context.SaveChanges();
                RubricLogic.ActualizaNotaMaximaRubrica(Context, IdRubrica);
                return Json(new { IdCriterio = objCriterio.IdCriterioOutcome, Descripcion = objCriterio.Descripcion, Puntaje = objCriterio.ValorMaximo, Niveles = lstNivelDesempenio });
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public JsonResult EditStatus(Int32 idOutcomeRubrica)
        {
            var query = (from a in Context.OutcomeRubrica
                         where a.IdOutcomeRubrica == idOutcomeRubrica
                         select a).FirstOrDefault();
            if (query.Visible)
            {
                query.Visible = false;
            }
            else
                query.Visible = true;

            Context.SaveChanges();
            return Json(true);
        }
        public JsonResult DeleteCriterio(Int32 IdCriterio, Int32 IdRubrica)
        {
            try
            {
                CriterioOutcome criterioEncontrado = Context.CriterioOutcome.FirstOrDefault(x => x.IdCriterioOutcome == IdCriterio);
                if (criterioEncontrado != null)
                {
                    Context.NivelCriterio.RemoveRange(criterioEncontrado.NivelCriterio);
                    Context.CriterioOutcome.Remove(criterioEncontrado);
                    Context.SaveChanges();
                    RubricLogic.ActualizaNotaMaximaRubrica(Context, IdRubrica);
                }
                return Json(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult SaveNivelCriterio(Int32 IdNivelCriterio, String Descripcion)
        {
            try
            {
                NivelCriterio NivelCriterio = Context.NivelCriterio.FirstOrDefault(x => x.IdNivelCriterio == IdNivelCriterio);
                NivelCriterio.DescripcionNivel = Descripcion;
                Context.SaveChanges();
                return Json(true);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region CONFIGURAR OUTCOMES A EVALUAR 2017-2
        public ActionResult OutcomeConfigure()
        {
            int parModalidadId = Session.GetModalidadId();
            int parIdSubModalidadPeriodoAcademicoActivo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == parModalidadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            var viewModel = new OutcomeConfigureViewModel();
            viewModel.Fill(CargarDatosContext(), parIdSubModalidadPeriodoAcademicoActivo);
            return View(viewModel);
        }

        public JsonResult GetCursosComisionesByCarrera(Int32 IdCarrera)
        {
            try
            {
                var IdCiclo = Session.GetPeriodoAcademicoId();
                var lstCursos = Context.CarreraCursoPeriodoAcademico.Where(x => x.IdCarrera == IdCarrera && x.CursoPeriodoAcademico.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo && x.CursoPeriodoAcademico.Curso.NombreEspanol.Contains("TALLER DE PROYECTO"))
                                .Select(x => new {
                                    IdCurso = x.CursoPeriodoAcademico.Curso.IdCurso,
                                    NombreEspanol = (currentCulture == ConstantHelpers.CULTURE.ESPANOL ? x.CursoPeriodoAcademico.Curso.NombreEspanol : x.CursoPeriodoAcademico.Curso.NombreIngles),
                                });
                var lstComisiones = Context.CarreraComision.Where(x => x.IdCarrera == IdCarrera && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == IdCiclo)
                                    .Select(x => new {
                                        IdComision = x.IdComision,
                                        Codigo = x.Comision.Codigo
                                    });

                return Json(new { Cursos = lstCursos, Comisiones = lstComisiones });
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public ActionResult _ListOutcomePartialView(Int32? IdCarrera, Int32? IdCurso, Int32? IdComision)
        {
            var model = new ListOutcomeConfigureViewModel();
            model.Fill(CargarDatosContext(), IdCarrera, IdCurso, IdComision);
            return PartialView("PartialView/_ListOutcomePartialView", model);
        }

        public JsonResult SaveConfiguredOutcome(Int32 IdMallaCocosDetalle, Int32 IdCarrera, Int32 IdCurso)
        {
            try
            {
                var IdCiclo = Session.GetSubModalidadPeriodoAcademicoId();
                var MallaCocosDetalle = Context.MallaCocosDetalle.FirstOrDefault(x => x.IdMallaCocosDetalle == IdMallaCocosDetalle);
                MallaCocosDetalle.EsCalificado = (MallaCocosDetalle.EsCalificado == null ? true : (!MallaCocosDetalle.EsCalificado.Value ? true : false));
                var Rubrica = Context.Rubrica.FirstOrDefault(x => x.Evaluacion.CarreraCursoPeriodoAcademico.IdCarrera == IdCarrera && x.Evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.IdCurso == IdCurso &&
                                                        x.OutcomeRubrica.Any(o => o.IdOutcomeComision == MallaCocosDetalle.IdOutcomeComision) && x.Evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == IdCiclo);

                if (Rubrica == null)
                {
                    var RubricasDisponibles = Context.Rubrica.Where(x => x.Evaluacion.CarreraCursoPeriodoAcademico.IdCarrera == IdCarrera && x.Evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.IdCurso == IdCurso &&
                                                                         x.Evaluacion.TipoEvaluacion.IdSubModalidadPeriodoAcademico == IdCiclo);

                    if (RubricasDisponibles.Any())
                    {
                        Rubrica RubricaDisponible;
                        if (!MallaCocosDetalle.OutcomeComision.Comision.Codigo.Equals("WASC"))
                            RubricaDisponible = RubricasDisponibles.FirstOrDefault(x => x.Evaluacion.TipoEvaluacion.Tipo.Equals("TF"));
                        else
                            RubricaDisponible = RubricasDisponibles.FirstOrDefault(x => x.Evaluacion.TipoEvaluacion.Tipo.Equals("PA"));

                        if (RubricaDisponible != null)
                        {
                            OutcomeRubrica OutcomeRubrica = new OutcomeRubrica();
                            OutcomeRubrica.Rubrica = RubricaDisponible;
                            OutcomeRubrica.IdOutcomeComision = MallaCocosDetalle.IdOutcomeComision;
                            OutcomeRubrica.NotaOutcome = 0;
                            OutcomeRubrica.NivelMaximo = 3;
                            OutcomeRubrica.Visible = true;
                            Context.OutcomeRubrica.Add(OutcomeRubrica);
                        }
                    }
                    Context.SaveChanges();
                    return Json(new { Result = true, Message = "Successful" });
                }
                else
                {
                    if (Rubrica.RubricaCalificada.Count > 0)
                        throw new Exception("There are already Rubrics Qualifications");
                    else if (MallaCocosDetalle.EsCalificado.Value)
                    {
                        OutcomeRubrica Outcome = new OutcomeRubrica();
                        Outcome.Rubrica = Rubrica;
                        Outcome.IdOutcomeComision = MallaCocosDetalle.IdOutcomeComision;
                        Outcome.NotaOutcome = 0;
                        Outcome.NivelMaximo = 3;
                        Outcome.Visible = true;
                        Context.OutcomeRubrica.Add(Outcome);
                        Context.SaveChanges();
                        return Json(new { Result = true, Message = "Successful" });
                    }
                    else
                    {
                        var OutcomeRubrica = Rubrica.OutcomeRubrica.FirstOrDefault(x => x.IdOutcomeComision == MallaCocosDetalle.IdOutcomeComision);
                        Context.CriterioOutcome.RemoveRange(OutcomeRubrica.CriterioOutcome);
                        Context.OutcomeRubrica.Remove(OutcomeRubrica);
                        Context.SaveChanges();
                        return Json(new { Result = true, Message = "Successful" });
                    }
                }
            }
            catch(Exception e)
            {
                return Json(new { Result = false, Message = e.Message });
            }
        }
        #endregion

        [HttpPost]
        public ActionResult RubricIndex(RubricViewModel model)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarDatos(CargarDatosContext(), model);
            return View(viewModel);
        }

        public ActionResult ComentarCapitulos()
        {
            ComentarCapitulosViewModel viewModel = new ComentarCapitulosViewModel();
            viewModel.CargarDatos(CargarDatosContext(), viewModel);

            return View(viewModel);
        }

        public ActionResult VerCargarRubrica(RubricViewModel Rubrica, string submit)
        {

            RubricDetailViewModel viewModel = new RubricDetailViewModel();
            try
            {

                //IDSUBMODALIDAD=1
                viewModel = RubricPresentationLogic.CreateRubricFromLastCicle(CargarDatosContext(), Rubrica,1);
                var idOutcome = TempData["LastOutcomeAffected"];
                var idComision = TempData["LastComisionAffected"];
                if (idOutcome == null)
                    TempData["LastOutcomeAffected"] = viewModel.Outcomes.First().IdOutcome;
                if (idComision == null)
                    TempData["LastComisionAffected"] = viewModel.Comisiones.First().IdComision;
                return View("RubricCreate", viewModel);
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("RubricIndex");
            }
        }

        public ActionResult CargarRubrica()
        {
            RubricDetailViewModel viewModel = new RubricDetailViewModel();
            try
            {
                var rubricaTemp = ConvertHelpers.ToTempDataValue<RubricViewModel>(TempData, "RubricViewModel");
                RubricaEntity rubrica = new RubricaEntity(rubricaTemp.IdCarrera, rubricaTemp.IdEvaluacion, rubricaTemp.IdCurso, rubricaTemp.IdCicloActual);
                int IdEvaluacionActual = RubricLogic.GetIdActualEvaluacion(CargarDatosContext().context, rubrica);//viewModel.ObtenerIdEvaluacionActual(CargarDatosContext(), rubricaTemp);
                if (RubricLogic.GenerarRubrica(CargarDatosContext().context, IdEvaluacionActual, rubricaTemp.IdEvaluacion))
                {
                    PostMessage(MessageType.Success);
                }
                return RedirectToAction("RubricIndex");
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("RubricIndex");
            }

        }

        public ActionResult EditRubric(RubricDetailViewModel viewModel)
        {
            RubricViewModel rubrica = new RubricViewModel();
            if (viewModel.SelectedComsiones.Length <= 0)
            {
                PostMessage(MessageType.Warning, RubricCreateResource.SinComision);
                rubrica.BindDataFromCreate(viewModel);
                return RedirectToAction("CrearContinuarRubrica", new { Rubrica = rubrica });

            }
            RubricDetailViewModel model = RubricPresentationLogic.EditRubrica(CargarDatosContext(), viewModel);
            rubrica = new RubricViewModel();
            rubrica.BindDataFromCreate(viewModel);
            PostMessage(MessageType.Success);
            return RedirectToAction("CrearContinuarRubrica", new { Rubrica = rubrica });
        }
        
        public ActionResult VerRubricIndex(RubricViewModel Rubrica, int IdEvaluacionRubricIndex, int IdCarreraRubricIndex, int IdCursoRubricIndex)
        {
            try
            {
                if (Rubrica == null)
                {
                    Rubrica = (RubricViewModel)TempData["RubricViweModel"];
                    TempData["RubricViweModel"] = Rubrica;
                }

                RubricDetailViewModel viewModel = new RubricDetailViewModel();
                Rubrica.IdEvaluacion = IdEvaluacionRubricIndex;
                Rubrica.IdCarrera = IdCarreraRubricIndex;
                Rubrica.IdCurso = IdCursoRubricIndex; //EN DURO ¿DESDE CUÁNDO?

                viewModel.BindDataFromIndex(Rubrica);

                if (RubricLogic.CanEditRubrica(CargarDatosContext().context, IdEvaluacionRubricIndex))
                {
                    //Rubrica.SelectedComsiones = new int[1];
                    //Rubrica.SelectedComsiones[0] = 6; //LE PONEN EN DURO

                    if (!RubricLogic.RubricAlredyHasOutcomesForComission(CargarDatosContext(), IdEvaluacionRubricIndex, Rubrica.SelectedComsiones))
                        viewModel = RubricPresentationLogic.DeleteAsociatioOutcomesRubric(CargarDatosContext(), viewModel);
                    else
                        viewModel = RubricPresentationLogic.CreateGetRubric(CargarDatosContext(), Rubrica);

                    var idOutcome = TempData["LastOutcomeAffected"];
                    var idComision = TempData["LastComisionAffected"];
                    if (idOutcome == null)
                        TempData["LastOutcomeAffected"] = viewModel.Outcomes.First().IdOutcome;
                    if (idComision == null)
                        TempData["LastComisionAffected"] = viewModel.Comisiones.First().IdComision;
                    return View("RubricCreate", viewModel);
                }
                else
                {
                    PostMessage(MessageType.Error, MessageResource.NoSePuedeEditarUnaRubricaQueYaHaSidoCalificada);
                    return RedirectToAction("RubricIndex");
                }

            }
            catch (Exception ex)
            {
                InvalidarContext();
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("RubricIndex");
            }
        }

        #region ComboBox
        public ActionResult ListCarrerasPeriodoAcademico(Int32 IdSelected)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarCarreras(CargarDatosContext(), IdSelected);
            return Json(viewModel.Carreras);
        }

        public JsonResult ListCursoCarrera(Int32 IdSelected)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarCursoCarrera(CargarDatosContext(), IdSelected);
            return Json(viewModel.Cursos);
        }

        public JsonResult ListEvaluacionCurso(Int32 IdSelected)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarEvaluacionesCurso(CargarDatosContext(), IdSelected);
            return Json(viewModel.Evaluaciones);
        }

        public JsonResult ListEvaluacionCursoOnlyTF(Int32 IdSelected)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarEvaluacionesCursoOnlyTF(CargarDatosContext(), IdSelected);
            return Json(viewModel.Evaluaciones);
        }

        public JsonResult ListComisionCarrera(Int32 EvaluacionId, Int32 CarreraId)
        {
            RubricViewModel viewModel = new RubricViewModel();
            viewModel.CargarComisionesCarrera(CargarDatosContext(), EvaluacionId, CarreraId);
            return Json(viewModel.Comisiones);
        }
        #endregion

        public ActionResult ListCriterios(int IdOutcome, string NombreCriterio, string DescripcionCriterio, float PuntajeCriterio)
        {
            CriterioViewModel viewModel = new CriterioViewModel(IdOutcome, NombreCriterio, DescripcionCriterio, PuntajeCriterio);
            var ListCriterios = viewModel.AddListCriterios(CargarDatosContext(), viewModel);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView(ListCriterios);
        }

        public ActionResult ListNiveles(int idCriterio, string NombreNivel, float NotaMaxima, float NotaMinima, string DescripcionNivel, int IdOutcome)
        {
            NivelViewModel viewModel = new NivelViewModel(idCriterio, NombreNivel, NotaMaxima, NotaMinima, DescripcionNivel);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView(viewModel.AddListNiveles(CargarDatosContext(), viewModel));

        }

        public ActionResult EliminarCriterio(int IdCriterio, int IdOutcome)
        {
            CriterioViewModel viewModel = new CriterioViewModel(IdOutcome, IdCriterio);
            var ListCriterios = viewModel.DeleteListCriterios(CargarDatosContext(), viewModel);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView("ListCriterios", ListCriterios);
        }

        public ActionResult EliminarCriteriosParticipacion(int IdOutcome)
        {
            CriterioViewModel viewModel = new CriterioViewModel(IdOutcome);
            var ListCriterios = viewModel.DeleteListCriteriosParticipacion(CargarDatosContext(), viewModel);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView("ListCriteriosParticipacion", ListCriterios);
        }

        public ActionResult EliminarNiveles(int IdCriterio, int IdOutcome)
        {
            NivelViewModel viewModel = new NivelViewModel();
            var ListNiveles = viewModel.DeleteListNiveles(CargarDatosContext(), IdCriterio);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView("ListNiveles", ListNiveles);
        }

        public ActionResult AgregarNivelesDefecto(int IdCriterio, int IdOutcome)
        {
            NivelViewModel viewModel = new NivelViewModel();
            var ListNiveles = viewModel.AddDefaulltNiveles(CargarDatosContext(), IdCriterio);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView("ListNiveles", ListNiveles);
        }

        public ActionResult EliminarNivelesDefecto(int IdCriterio, int IdOutcome)
        {
            NivelViewModel viewModel = new NivelViewModel();
            var ListNiveles = viewModel.DeleteListNiveles(CargarDatosContext(), IdCriterio, true);
            TempData["LastOutcomeAffected"] = IdOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(IdOutcome).OutcomeComision.IdComision;
            PostMessage(MessageType.Success);
            return PartialView("ListNiveles", ListNiveles);
        }

        //2017-2 CargarRoles toma otra funcionalidad, ya no carga los roles, ahora! automaticamente guarda el rol comité a evaluador de outcomes
        public ActionResult CargarRoles(RubricDetailViewModel model, string submit)
        {
            try
            {
                switch (submit)
                {
                    case "Regresar":
                    case "Return":
                        return RedirectToAction("RubricIndex");

                    case "Guardar": case "Save":
                        RubricLogic.CalcularNotaMaximaRubrica(CargarDatosContext().context, model.IdRubrica);
                        //Se elimina por cambio 2017-2 ya no habrá rúbrica para PA
                        /*if (model.EvaluacionNombre.Equals("PA"))
                        {
                            RubricLogic.AsociateOutcomesEvaluatorWASC(CargarDatosContext().context, model.IdRubrica);
                            PostMessage(MessageType.Success);
                            return RedirectToAction("RubricIndex");
                        }
                        else
                        {
                         EvaluatorsViewModel viewModel = new EvaluatorsViewModel();
                         viewModel.CargarDatos(CargarDatosContext(), model);
                         return View("ManageRoles", viewModel);*/

                        //Cambio por que ya no se cargará los roles por outcome a evaluar, toda la evaluación será por parte del comité
                        RubricLogic.SaveRoles(CargarDatosContext().context, model.IdRubrica);
                        PostMessage(MessageType.Success);
                        return RedirectToAction("RubricIndex");
                        //}
                }
                return RedirectToAction("RubricIndex");
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("RubricIndex");
            }
        }

        public ActionResult GuardarRoles(FormCollection collection)
        {
            Dictionary<String, String> Matches = new Dictionary<string, string>();
            foreach (var item in collection.AllKeys)
            {

                var value = collection[item];
                Matches.Add(item, value);
            }

            RubricLogic.SaveRoles(CargarDatosContext().context, Matches);
            PostMessage(MessageType.Success);
            return RedirectToAction("RubricIndex");
        }

        public ActionResult ListCriteriosParticipacion(int idOutcome, int notaMaxima)
        {
            CriterioEntity criterio = new CriterioEntity(idOutcome, notaMaxima, 0);
            var ListCriterios = RubricLogic.AddListCriteriosCompGen(CargarDatosContext().context, criterio);
            var viewModel = ListCriterios.Select(x => new CriterioViewModel
            {
                DescripcionCriterio = x.Descripcion,
                IdCriterio = x.IdCriterioOutcome,
                IdOutcome = x.IdOutcomeRubrica,
                NombreCriterio = x.Nombre,
                NotaMaxima = x.ValorMaximo,
                NotaMinima = x.ValorMinimo
            }).ToList();

            TempData["LastOutcomeAffected"] = idOutcome;
            TempData["LastComisionAffected"] = Context.OutcomeRubrica.Find(idOutcome).OutcomeComision.IdComision;

            if(viewModel.Count == 0)
            {
                PostMessage(MessageType.Warning, MessageResource.PorFavorVerifiqueQueSeRealizoLaCargaMasivaDeLosLogrosWAASCalSistema);
            }

            PostMessage(MessageType.Success);
            return PartialView("ListCriteriosParticipacion", viewModel);

        }
    }
}