﻿
#region Imports

using System;
using System.Transactions;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using System.Linq;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.VirtualCompany;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.VirtualCompany;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.VirtualCompany.PartialView;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
    public class VirtualCompanyController : BaseController
    {
        #region Get

        /// <summary>
        /// Retorna la página de Listar Empresas Virtuales.
        /// </summary>
        /// <returns>View</returns>
        public ActionResult ListVirtualCompany()
        {
            var viewModel = new ListVirtualCompanyViewModel();
            viewModel.CargarDatos(CargarDatosContext());
            return View(viewModel);
        }

        /// <summary>
        /// Retorna la página de Creación y Edición de Empresas Virtuales.
        /// </summary>
        /// <returns>View</returns>
        public ActionResult AddEditVirtualCompany(Int32? EmpresaVirtualId)
        {
            var viewModel = new AddEditVirtualCompanyViewModel();
            viewModel.CargarDatos(CargarDatosContext(), EmpresaVirtualId, this.EscuelaId);
            return View(viewModel);
        }

        /// <summary>
        /// Se elimina una empresa virtual solo si esta no tiene proyectos academicos
        /// </summary>
        /// <param name="EmpresaVirtualId"></param>
        /// <returns></returns>
        public ActionResult DeleteVirtualCompany(Int32 EmpresaVirtualId)
        {
            using (var transaction = new TransactionScope())
            {
                EmpresaVirtual empresa = Context.EmpresaVirtual.FirstOrDefault(x => x.IdEmpresa == EmpresaVirtualId);

                if (empresa.ProyectoAcademico.Count >= 1)
                {
                    PostMessage(MessageType.Warning, AddEditVirtualCompanyResource.NoSePuedeEliminar);
                }
                else
                {
                    Context.EmpresaVirtual.Remove(empresa);
                    Context.SaveChanges();
                    transaction.Complete();
                    PostMessage(MessageType.Success);
                }
            }
            return RedirectToAction("ListVirtualCompany");
        }
        #endregion

        #region PartialView

        /// <summary>
        /// Retorna la vista parcial que contiene las Empresas Virtuales
        /// de un determinado Periodo Académico.
        /// </summary>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Actual</param>
        /// <param name="PeriodoAcademicoId">ID del Periodo Académico Seleccionado</param>
        /// <returns>Partial View</returns>
        public ActionResult _ListVirtualCompanyPartialView(Int32 PeriodoAcademicoActualId, Int32 PeriodoAcademicoId, Int32? Pagina)
        {
            var viewModel = new ListVirtualCompanyPartialViewModel();
            viewModel.PeriodoAcademicoActualId = PeriodoAcademicoActualId;

            viewModel.CargarDatos(CargarDatosContext(), PeriodoAcademicoId, this.EscuelaId, Pagina);
            return PartialView("PartialView/_ListVirtualCompanyPartialView", viewModel);
        }

        #endregion

        #region Post

        /// <summary>
        /// Crea o Edita una Empresa Virtual, validando que el código de la empresa
        /// no se repita en el presente periodo académico.
        /// </summary>
        /// <param name="ViewModel">ViewModel de un Empresa Virtual</param>
        /// <returns>View</returns>
        [HttpPost]
        public ActionResult AddEditVirtualCompany(AddEditVirtualCompanyViewModel ViewModel)
        {
            try
            {
                //Int32 periodoAcademicoActualId = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
                //    .FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;


                Int32 periodoAcademicoActualId = Context.SubModalidadPeriodoAcademico.Where
                    (x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad==1).FirstOrDefault().IdPeriodoAcademico;

                Int32 submodalidadperiodoacademicomoduloActualId = Context.SubModalidadPeriodoAcademicoModulo.Where(
                    x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).
                    FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                Boolean codigoRepetido = Context.EmpresaVirtual.Any(
                    x =>
                        x.Codigo.ToUpper() == ViewModel.Codigo.Trim().ToUpper() && x.IdSubModalidadPeriodoAcademicoModulo == submodalidadperiodoacademicomoduloActualId &&
                        x.IdEmpresa != ViewModel.EmpresaVirtualId.Value);

                Boolean nombreRepetido = Context.EmpresaVirtual.Any(
                    x =>
                        x.Nombre.ToUpper() == ViewModel.Nombre.Trim().ToUpper() && x.IdSubModalidadPeriodoAcademicoModulo == submodalidadperiodoacademicomoduloActualId &&
                        x.IdEmpresa != ViewModel.EmpresaVirtualId.Value);

                if (!ModelState.IsValid || codigoRepetido || nombreRepetido)
                {
                    if (codigoRepetido)
                        PostMessage(MessageType.Info, AddEditVirtualCompanyResource.CodigoRepetido);
                    else if (nombreRepetido)
                        PostMessage(MessageType.Info, AddEditVirtualCompanyResource.NombreRepetido);
                    else
                        PostMessage(MessageType.Info);

                    return View(ViewModel);
                }

                using (var transaction = new TransactionScope())
                {
                    var empresaVirtual = Context.EmpresaVirtual.Find(ViewModel.EmpresaVirtualId);

                    if (empresaVirtual == null)
                    {
                        int pac = periodoAcademicoActualId;
                        int idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == pac).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                        int idSubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                        empresaVirtual = new EmpresaVirtual();
                        empresaVirtual.IdSubModalidadPeriodoAcademicoModulo = idSubModalidadPeriodoAcademicoModulo;
                        empresaVirtual.IdEscuela = this.EscuelaId;
                        Context.EmpresaVirtual.Add(empresaVirtual);
                    }

                    empresaVirtual.Codigo = ViewModel.Codigo.Trim();
                    empresaVirtual.Nombre = ViewModel.Nombre.Trim();
                    empresaVirtual.Descripcion = ViewModel.Descripcion.Trim();

                    Context.SaveChanges();
                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("ListVirtualCompany");
            }

            PostMessage(MessageType.Success);
            return RedirectToAction("ListVirtualCompany");
        }

        #endregion
    }
}
