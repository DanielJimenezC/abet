﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.AcademicProject;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.AcademicProject.PartialView;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.AcademicProject.PartialView;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
    public class AcademicProjectController : BaseController
    {
        #region Get

        /// <summary>
        /// Retorna la página de Listar Proyectos Académicos.
        /// </summary>
        /// <returns>View</returns>
        public ActionResult ListAcademicProject()
        {

            var viewModel = new ListAcademicProjectViewModel();
            viewModel.CargarDatos(CargarDatosContext(), this.PeriodoAcademicoId, this.EscuelaId);
            return View(viewModel);



        }

        /// <summary>
        /// Retorna la página de Creación y Edición de Proyectos Académicos,
        /// obteniendo los Alumnos de dicho Proyecto Académico guardando sus respectivos
        /// IDs en un TempData, así como también los evaluadores del proyecto.
        /// </summary>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <returns>View</returns>
        public ActionResult AddEditAcademicProject(Int32? ProyectoAcademicoId)
        {
            var viewModel = new AddEditAcademicProjectViewModel();

            viewModel.CargarDatos(CargarDatosContext(),ProyectoAcademicoId, this.PeriodoAcademicoId,this.EscuelaId);
            TempData["AlumnosSeleccionados"] = viewModel.ObtenerAlumnosProyecto(CargarDatosContext(), ProyectoAcademicoId,this.PeriodoAcademicoId, this.EscuelaId);
            TempData["DocentesSeleccionados"] = viewModel.ObtenerEvaluadoresProyecto(CargarDatosContext(), ProyectoAcademicoId, this.PeriodoAcademicoId, this.EscuelaId);

            return View(viewModel);
        }

        public JsonResult GetStudents(String CadenaBuscar, String Curso)
        {
            //Int32 periodoId = context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
            Int32 parIdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad == 1).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var query = Context.Alumno.AsQueryable();

            

            if (!String.IsNullOrEmpty(CadenaBuscar))
                foreach (var token in CadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));

            query = query.Where(
                x =>
                x.Carrera.IdEscuela == this.EscuelaId &&
                x.AlumnoMatriculado.
                Any(y => y.AlumnoSeccion.
               Any(z =>  z.Seccion.CursoPeriodoAcademico.Curso.Codigo.Equals(Curso)
               && z.Seccion.CursoPeriodoAcademico.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == parIdSubModalidadPeriodoAcademico)));

            var data = query.ToList().Select(x => new { id = x.Codigo, text = x.Nombres + " " + x.Apellidos });
            return Json(data);
        }
        [HttpPost]
        public JsonResult GetRolesPorCurso(String Curso)
        {

            List<TipoEvaluador> ListRoles = null;

            int? periodoAcademicoActualId = Session.GetPeriodoAcademicoId();

            ListRoles = Context.TipoEvaluador.Where(x =>x.TipoEvaluadorMaestra.IdEscuela==this.EscuelaId  && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId && x.Curso.Codigo == Curso).ToList();
            var data = ListRoles.Select(x => new { IdTipoEvaluador = x.IdTipoEvaluador, Nombre = x.TipoEvaluadorMaestra.Nombre });
            return Json(data);
        }

        
        public JsonResult GetDocentes(String cadenaBuscar)
        {
            var query = Context.Docente.AsQueryable();
            if (!String.IsNullOrEmpty(cadenaBuscar))
                foreach (var token in cadenaBuscar.Split(' '))
                    query = query.Where(x => x.Codigo.Contains(token) || x.Nombres.Contains(token) || x.Apellidos.Contains(token));

            var data = query.ToList().Select(x => new { id = x.Codigo, text = x.Nombres + " " + x.Apellidos + " | " + x.Codigo });
            return Json(data);
        }
        #endregion

        #region Ajax

        /// <summary>
        /// Agrega un alumno a la lista de IDs almacenados en el TempData,
        /// se valida la inserción de como máximo dos alumnos por proyecto y 
        /// se devuelve un mensaje en el TempData dependiendo del estado del alumno,
        /// luego se redirecciona al PartialView para su resectiva actualización.
        /// </summary>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <param name="CodigoAlumno">Código del Alumno a Buscar</param>
        /// <returns>Partial View</returns>
        public ActionResult AddStudent(Int32? ProyectoAcademicoId, String CodigoAlumno, String Curso)
        {
            var alumnosSeleccionados = ConvertHelpers.ToTempDataValue<List<Int32>>(TempData, "AlumnosSeleccionados");

            Int32 periodoAcademicoActualId = (from pa in Context.PeriodoAcademico
                                              join smpa in Context.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals smpa.IdPeriodoAcademico
                                              join sm in Context.SubModalidad on smpa.IdSubModalidad equals sm.IdSubModalidad
                                              join smo in Context.Modalidad on sm.IdModalidad equals smo.IdModalidad
                                              where pa.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && smo.IdModalidad == 1
                                              select new
                                              {
                                                  idSubModalidadPeriodoAcademico = smpa.IdSubModalidadPeriodoAcademico,
                                                  CicloAcademico = pa.CicloAcademico
                                              }).FirstOrDefault().idSubModalidadPeriodoAcademico;

            Alumno objAlumno = Context.Alumno.FirstOrDefault(x => x.Codigo == CodigoAlumno);

            if (objAlumno != null)
            {
                AlumnoSeccion alumnoSeccion = Context.AlumnoSeccion.FirstOrDefault(x => x.AlumnoMatriculado.IdAlumno == objAlumno.IdAlumno
                                                                                        //&& x.AlumnoMatriculado.IdPeriodoAcademico == periodoAcademicoActualId
                                                                                        && x.AlumnoMatriculado.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId
                                                                                        && x.Seccion.CursoPeriodoAcademico.Curso.Codigo.Equals(Curso));
                //BulkInsertLogic.GetAlumnoSeccionId(context, objAlumno.IdAlumno, periodoAcademicoActualId);

                if (alumnoSeccion != null)
                {
                    var objAlumnoSeccion =
                        Context.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == alumnoSeccion.IdAlumnoSeccion);

                    var objAlumnoMatriculado = Context.AlumnoMatriculado.FirstOrDefault(x => x.IdAlumnoMatriculado == objAlumnoSeccion.IdAlumnoMatriculado);

                    if (objAlumnoMatriculado.EstadoMatricula == ConstantHelpers.ESTADO_RETIRADO)
                    {
                        TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoRetirado;
                    }
                    else
                    {
                        if (objAlumnoSeccion.IdProyectoAcademico == null || (objAlumnoSeccion.IdProyectoAcademico != null && objAlumnoSeccion.IdProyectoAcademico == ProyectoAcademicoId))
                        {
                            if (alumnosSeleccionados.Contains(objAlumnoSeccion.IdAlumnoSeccion))
                            {
                                TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoYaExiste;
                            }
                            else
                            {
                                //if (alumnosSeleccionados.Count <= 6)// se permite 3 alumnos
                                //{
                                    alumnosSeleccionados.Add(objAlumnoSeccion.IdAlumnoSeccion);
                                //}
                                //else
                                //{
                                  //  TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoLimite;
                                //}
                            }
                        }
                        else
                        {
                            TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoYaExiste;
                        }
                    }
                }
                else
                {
                    TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoSinSeccion;
                }
            }
            else
            {
                TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoNoExiste;
            }

            TempData["AlumnosSeleccionados"] = alumnosSeleccionados;

            return RedirectToAction("_ListStudentPartialView");
        }

        /// <summary>
        /// Remueve un alumno de la lista de IDs almacenados en el TempData,
        /// luego se redirecciona al PartialView para su resectiva actualización.
        /// </summary>
        /// <param name="AlumnoId">ID del Alumno Seleccionado</param>
        /// <returns>Partial View</returns>
        public ActionResult RemoveStudent(Int32 AlumnoId)
        {
            //Int32 periodoAcademicoActualId = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
            //    .FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            Int32 periodoAcademicoActualId = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;



            var alumnosSeleccionados = ConvertHelpers.ToTempDataValue<List<Int32>>(TempData, "AlumnosSeleccionados");

            Int32 alumnoSeccionId = BulkInsertLogic.GetAlumnoSeccionId(Context, AlumnoId, periodoAcademicoActualId, this.EscuelaId).Value;

            if (Context.RubricaCalificada.Any(x => x.IdAlumnoSeccion == alumnoSeccionId))
            {
                TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.AlumnoRubrica;
            }
            else
            {
                alumnosSeleccionados.RemoveAll(x => x == alumnoSeccionId);
            }

            TempData["AlumnosSeleccionados"] = alumnosSeleccionados;

            return RedirectToAction("_ListStudentPartialView");
        }

        /// <summary>
        /// Agrega un docente a la lista de modelos almacenados en el TempData,
        /// se valida la existencia de los docentes y la repetición de roles, y
        /// se devuelve un mensaje en el TempData dependiendo de la validación encontrada,
        /// luego se redirecciona al PartialView para su resectiva actualización.
        /// </summary>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <param name="CodigoDocente">Código del Docente a Buscar</param>
        /// <param name="RolEvaluador">Rol del Evaluador seleccionado</param>
        /// <returns>Partial View</returns>
        /// 


        public ActionResult AddDocente(Int32? ProyectoAcademicoId, String CodigoDocente, Int32? RolEvaluador)
        {
            var docentesSeleccionados = ConvertHelpers.ToTempDataValue<List<EvaluatorModel>>(TempData, "DocentesSeleccionados");

            Int32 periodoAcademicoActualId = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == 1).FirstOrDefault().IdPeriodoAcademico;

            Docente objDocente = Context.Docente.FirstOrDefault(x => x.Codigo.ToUpper() == CodigoDocente.Trim().ToUpper());

            if (RolEvaluador.HasValue)
            {
                TipoEvaluador objRol = Context.TipoEvaluador.Where(
                    //x => x.IdPeriodoAcademico == periodoAcademicoActualId)
                    x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId)
                    .FirstOrDefault(x => x.IdTipoEvaluador == RolEvaluador.Value);

                if (objDocente != null)
                {
                    if (!docentesSeleccionados.Any(x => x.DocenteId == objDocente.IdDocente && x.TipoEvaluadorId == RolEvaluador.Value))
                    {
                        if (!(docentesSeleccionados.Any(x => x.CodigoDocente == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.CLI)&&
                        docentesSeleccionados.Any(x => x.CodigoDocente == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.GERENTE)&&
                        docentesSeleccionados.Any(x => x.CodigoDocente == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.COA)))
                        {
                            var objEvaluador = new EvaluatorModel
                            {
                                DocenteId = objDocente.IdDocente,
                                NombreDocente = objDocente.Nombres + " " + objDocente.Apellidos,
                                CodigoDocente = objDocente.Codigo,
                                PesoEvaluador = (objRol.Peso * 100).ToString(),
                                RolEvaluador = objRol.TipoEvaluadorMaestra.Nombre,
                                CodigoTipoEvaluador = objRol.TipoEvaluadorMaestra.Codigo,
                                TipoEvaluadorId = objRol.IdTipoEvaluador
                            };
                            docentesSeleccionados.Add(objEvaluador);
                        }
                        else
                        {
                            TempData["MensajeRespuesta"] = ListEvaluatorResource.RolYaElegido;
                        }
                    }
                    else
                    {
                    TempData["MensajeRespuesta"] = ListEvaluatorResource.DocenteYaElegido;
                    }
                }
                else
                {
                    TempData["MensajeRespuesta"] = ListEvaluatorResource.DocenteNoEncontrado;
                }
            }
            else
            {
                TempData["MensajeRespuesta"] = ListEvaluatorResource.SeleccioneRol;
            }

            TempData["DocentesSeleccionados"] = docentesSeleccionados;

            return RedirectToAction("_ListEvaluatorPartialView");
        }

        /// <summary>
        /// Remueve un docente de la lista de modelo almacenados en el TempData,
        /// luego se redirecciona al PartialView para su resectiva actualización.
        /// </summary>
        /// <param name="DocenteId">ID del Docente Seleccionado</param>
        /// <returns>Partial View</returns>
        public ActionResult RemoveDocente(int ProyectoId, int DocenteId, int TipoEvaluadorId)
        {
            var docentesSeleccionados = ConvertHelpers.ToTempDataValue<List<EvaluatorModel>>(TempData, "DocentesSeleccionados");
            Boolean elimina = false;
            var objEvaluador =
                Context.Evaluador.FirstOrDefault(x => x.IdDocente == DocenteId && x.IdProyectoAcademico == ProyectoId && x.IdTipoEvaluador == TipoEvaluadorId);

            if (objEvaluador != null)
            {
                if (Context.RubricaCalificada.Any(x => x.IdEvaluador == objEvaluador.IdEvaluador))
                {
                    TempData["MensajeRespuesta"] = AddEditAcademicProjectResource.DocenteRubrica;
                }
                else
                {
                    elimina = true;
                }
            }
            else
            {
                elimina = true;
            }

            if (elimina)
            {
                docentesSeleccionados.RemoveAll(x => x.DocenteId == DocenteId && x.TipoEvaluadorId == TipoEvaluadorId);
            }

            TempData["DocentesSeleccionados"] = docentesSeleccionados;

            return RedirectToAction("_ListEvaluatorPartialView");
        }

        #endregion

        #region PartialView

        /// <summary>
        /// Retorna la vista parcial que contiene los Proyectos Académicos
        /// de una determinada Empresa Virtual.
        /// </summary>
        /// <param name="EmpresaVirtualId">ID de la Empresa Virtual Seleccionada</param>
        /// <returns>Partial View</returns>
        public ActionResult _ListAcademicProjectPartialView(Int32 EmpresaVirtualId, String TipoCurso, Int32? Pagina, String CodigoAlumno)
        {
            var viewModel = new ListAcademicProjectPartialViewModel();
            viewModel.CargarDatos(CargarDatosContext(), EmpresaVirtualId, TipoCurso, Pagina,this.PeriodoAcademicoId,this.EscuelaId,CodigoAlumno);
            return PartialView("PartialView/_ListAcademicProjectPartialView", viewModel);
        }

        /// <summary>
        /// Retorna la vista parcial que contiene los Alumnos seleccionados para
        /// un proyecto académico, donde se mantinen los IDs de los alumnos en un TempData.
        /// </summary>
        /// <returns>Partial View</returns>
        public ActionResult _ListStudentPartialView()
        {
            var alumnosSeleccionados = ConvertHelpers.ToTempDataValue<List<Int32>>(TempData, "AlumnosSeleccionados");
            TempData["AlumnosSeleccionados"] = alumnosSeleccionados;

            var viewModel = new ListStudentPartialViewModel();
            //TEMPORAL SUBMODALIDAD=1 deberia recibir submodalidad desde la session del usuario
            viewModel.CargarDatos(CargarDatosContext(), alumnosSeleccionados,1);
            viewModel.MensajeRespuesta = (String)TempData["MensajeRespuesta"];

            return PartialView("PartialView/_ListStudentPartialView", viewModel);
        }

        /// <summary>
        /// Retorna la vista parcial que contiene los Evaluadores seleccionados para
        /// un proyecto académico, donde se mantinen los modelos de los docentes en un TempData.
        /// </summary>
        /// <returns>Partial View</returns>
        public ActionResult _ListEvaluatorPartialView()
        {
            var docentesSeleccionados = ConvertHelpers.ToTempDataValue<List<EvaluatorModel>>(TempData, "DocentesSeleccionados");
            TempData["DocentesSeleccionados"] = docentesSeleccionados;

            var viewModel = new ListEvaluatorPartialViewModel();
            viewModel.CargarDatos(CargarDatosContext(), docentesSeleccionados);
            viewModel.MensajeRespuesta = (String)TempData["MensajeRespuesta"];

            return PartialView("PartialView/_ListEvaluatorPartialView", viewModel);
        }

        #endregion

        #region Post

        /// <summary>
        /// Crea o Edita un Proyecto Académico, validando los datos correspondientes.
        /// Además se asignan los alumnos seleccionados al proyecto creado o editado,
        /// como también los evaluadores seleccionados.
        /// </summary>
        /// <param name="ViewModel">ViewModel de un Proyecto Académico</param>
        /// <returns>View</returns>
        [HttpPost]
        public ActionResult AddEditAcademicProject(AddEditAcademicProjectViewModel ViewModel)
        {
            int IdSubModalidad = 1;

            try
            {
                var alumnosSeleccionados = ConvertHelpers.ToTempDataValue<List<Int32>>(TempData, "AlumnosSeleccionados");
                var docentesSeleccionados = ConvertHelpers.ToTempDataValue<List<EvaluatorModel>>(TempData, "DocentesSeleccionados");

                // Int32 periodoAcademicoActualId = context.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

                Int32 periodoAcademicoActualIdM = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.PeriodoAcademico.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO) && x.IdSubModalidad == IdSubModalidad).IdPeriodoAcademico;

                Boolean codigoRepetido = Context.ProyectoAcademico.Any(
                    x =>
                      //x.Codigo.ToUpper() == ViewModel.Codigo.Trim().ToUpper() && x.IdPeriodoAcademico == periodoAcademicoActualId &&
                        x.Codigo.ToUpper() == ViewModel.Codigo.Trim().ToUpper() && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualIdM &&
                        x.IdProyectoAcademico != ViewModel.ProyectoAcademicoId.Value);

                Boolean nombreRepetido = Context.ProyectoAcademico.Any(
                    x =>
                      //x.Nombre.ToUpper() == ViewModel.Nombre.Trim().ToUpper() && x.IdPeriodoAcademico == periodoAcademicoActualId &&
                        x.Nombre.ToUpper() == ViewModel.Nombre.Trim().ToUpper() && x.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico == periodoAcademicoActualIdM &&
                        x.IdProyectoAcademico != ViewModel.ProyectoAcademicoId.Value);

                //DECLARAMOS idMODULO TEMPORAL HASTA SABER DONDE ESTA

               

                //var lstCursos = context.GETCURSOSRUBRICAS(SubModalidadPeriodoAcademicoId,idmodulo,PeriodoAcademicoId, escuelaId).Select(x=>x.Codigo).ToList();
                var lstCursos = Context.GETCURSOSRUBRICAS(IdSubModalidad, 0, this.PeriodoAcademicoId, EscuelaId).Select(x => x.Codigo).ToList();


                var curso = ViewModel.Curso != null ? ViewModel.Curso.Trim().ToUpper() : String.Empty;
                Boolean cursoProyecto = lstCursos.Any(x => x.ToUpper() == curso);

                if (!ModelState.IsValid || codigoRepetido || nombreRepetido || !cursoProyecto)
                {
                    ViewModel.CargarDatos(CargarDatosContext(), ViewModel.ProyectoAcademicoId,this.PeriodoAcademicoId,this.EscuelaId);
                    TryUpdateModel(ViewModel);

                    TempData["AlumnosSeleccionados"] = alumnosSeleccionados;
                    TempData["DocentesSeleccionados"] = docentesSeleccionados;

                    if (codigoRepetido)
                        PostMessage(MessageType.Info, AddEditAcademicProjectResource.CodigoRepetido);
                    else if (nombreRepetido)
                        PostMessage(MessageType.Info, AddEditAcademicProjectResource.NombreRepetido);
                    else if (!cursoProyecto)
                        PostMessage(MessageType.Info, Presentation.Resources.Views.Shared.MessageResource.DebesSeleccionarUnCursoValidoDeLaLista);
                    else
                        PostMessage(MessageType.Info);

                    return View(ViewModel);
                }

                using (var transaction = new TransactionScope())
                {
                    var proyectoAcademico = Context.ProyectoAcademico.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualIdM)
                                    .FirstOrDefault(x => x.IdProyectoAcademico == ViewModel.ProyectoAcademicoId);

                    if (proyectoAcademico == null)
                    {

                        int intsubmodapamodu = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualIdM).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                        proyectoAcademico = new ProyectoAcademico();
                        proyectoAcademico.IdSubModalidadPeriodoAcademicoModulo= intsubmodapamodu;
                        Context.ProyectoAcademico.Add(proyectoAcademico);
                    }

                    proyectoAcademico.Codigo = ViewModel.Codigo.Trim();
                    proyectoAcademico.Nombre = ViewModel.Nombre.Trim();
                    proyectoAcademico.Descripcion = ViewModel.Descripcion.Trim();
                    proyectoAcademico.IdEmpresaVirtual = ViewModel.EmpresaVirtualId;
                    var objCurso = Context.Curso.Where(x => x.Codigo == curso).FirstOrDefault();
                    proyectoAcademico.IdCurso = objCurso.IdCurso;

                    Context.SaveChanges();

                    if (ViewModel.ProyectoAcademicoId.HasValue)
                    {
                        var alumnosDeProyecto =
                            Context.AlumnoSeccion.Where(x => x.IdProyectoAcademico == ViewModel.ProyectoAcademicoId);
                        alumnosDeProyecto.Where(x => !alumnosSeleccionados.Contains(x.AlumnoMatriculado.IdAlumno)).ToList()
                            .ForEach(x => x.IdProyectoAcademico = null);

                        var docentesProyectoPorEliminar = new List<Evaluador>();
                        var lstIdEvaluadores = new List<Tuple<int, string>>();

                        var lstEvaluadores = Context.Evaluador.Where(x => x.IdProyectoAcademico == ViewModel.ProyectoAcademicoId.Value).ToList();

                        foreach (var item in lstEvaluadores) // creo que debería ser la otra lista docentesSeleccionados
                        {
                            if (!docentesSeleccionados.Any(x => x.DocenteId == item.IdDocente && x.RolEvaluador == item.TipoEvaluador.TipoEvaluadorMaestra.Nombre))
                            {
                                docentesProyectoPorEliminar.Add(item);
                            }
                            else
                            {
                                //lstIdEvaluadores.Add(item.IdDocente);
                                lstIdEvaluadores.Add(new Tuple<int, string>(item.IdDocente, item.TipoEvaluador.TipoEvaluadorMaestra.Nombre));
                            }
                        }
                        //docentesSeleccionados.RemoveAll(x => lstIdEvaluadores.Any(y => y == x.DocenteId));
                        docentesSeleccionados.RemoveAll(x => lstIdEvaluadores.Any(y => y.Item1.Equals(x.DocenteId) && y.Item2.Equals(x.RolEvaluador)));

                        Context.Evaluador.RemoveRange(docentesProyectoPorEliminar);
                    }
                    else
                    {
                        var lstEvaluacionProyectoBase = ConstantHelpers.RUBRICFILETYPE.ListEvaluationProject();
                        var lstEvaluacionProyecto =
                            Context.TipoEvaluacion.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualIdM &&
                                                                lstEvaluacionProyectoBase.Contains(x.Tipo));

                        foreach (var tipoEvaluacion in lstEvaluacionProyecto)
                        {
                            var objEvaluacionProyecto = new EvaluacionProyecto
                            {
                                IdProyecto = proyectoAcademico.IdProyectoAcademico,
                                IdTipoEvaluacion = tipoEvaluacion.IdTipoEvaluacion
                            };

                            Context.EvaluacionProyecto.Add(objEvaluacionProyecto);
                        }
                    }

                    var alumnosSeccion = new List<AlumnoSeccion>();
                    foreach (var item in alumnosSeleccionados)
                    {
                        AlumnoSeccion alumnoSeccion = Context.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == item);
                        alumnosSeccion.Add(alumnoSeccion);
                    }
                    alumnosSeccion.ForEach(x => x.IdProyectoAcademico = proyectoAcademico.IdProyectoAcademico);

                    foreach (var item in docentesSeleccionados)
                    {
                        var objEvaluador = new Evaluador
                        {
                            IdProyectoAcademico = proyectoAcademico.IdProyectoAcademico,
                            IdDocente = item.DocenteId,
                            IdTipoEvaluador = item.TipoEvaluadorId
                        };
                        Context.Evaluador.Add(objEvaluador);
                    }

                    Context.SaveChanges();
                    TempData["AlumnosSeleccionados"] = null;
                    TempData["DocentesSeleccionados"] = null;
                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("ListAcademicProject");
            }

            PostMessage(MessageType.Success);
            return RedirectToAction("ListAcademicProject");
        }

        #endregion
    }
}
