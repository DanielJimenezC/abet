﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Views
{
    public class GrupoComitesController : BaseController
    {
        private AbetEntities db = new AbetEntities();

        // GET: Rubric/GrupoComites
        public ActionResult Index()
        {
            int parModaldadId = Session.GetModalidadId();

            int parSubModalidadPeriodoAcademicoIdActivo = db.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == parModaldadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var grupoComite = db.GrupoComite.Where(x => x.IdSubModalidadPeriodoAcademico == parSubModalidadPeriodoAcademicoIdActivo);

            //var grupoComite = db.GrupoComite.Include(g => g.Docente).Include(g => g.SubModalidadPeriodoAcademico);

            return View(grupoComite.ToList());
        }

        // GET: Rubric/GrupoComites/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrupoComite grupoComite = db.GrupoComite.Find(id);
            if (grupoComite == null)
            {
                return HttpNotFound();
            }
            return View(grupoComite);
        }

        // GET: Rubric/GrupoComites/Create
        public ActionResult Create()
        {
            ViewBag.IdDocente = new SelectList(db.Docente, "IdDocente", "Codigo");
            
            return View();
        }

        // POST: Rubric/GrupoComites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdGrupoComite,IdDocente")] GrupoComite grupoComite)
        {
            if (ModelState.IsValid)
            {
                int parModaldadId = Session.GetModalidadId();

                int submodalidad = db.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == parModaldadId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                var codprofesor = grupoComite.IdDocente;
                GrupoComite isExistGrupoComite = db.GrupoComite.Where(x => x.IdDocente == codprofesor && x.IdSubModalidadPeriodoAcademico == submodalidad).FirstOrDefault();
                if (isExistGrupoComite != null)
                {
                    // ViewBag.mensaje = "Codigo ya utilizado";
                    PostMessage(MessageType.Error);
                    return RedirectToAction("Create");
                }
                else
                {
                    int parIdModalidad = Session.GetModalidadId();
                    int paridSubModalidadPeriodoAcademicoAct = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.Modalidad.IdModalidad == parIdModalidad).FirstOrDefault().IdSubModalidadPeriodoAcademico;

                    grupoComite.IdSubModalidadPeriodoAcademico = paridSubModalidadPeriodoAcademicoAct;
                    db.GrupoComite.Add(grupoComite);
                    db.SaveChanges();
                    PostMessage(MessageType.Success);
                    return RedirectToAction("Index");
                }
            }

        

            PostMessage(MessageType.Error, MessageResource.HuboUnProblemaAlRegistrarElDocenteLoSentimos + " ");
            ViewBag.IdDocente = new SelectList(db.Docente, "IdDocente", "Codigo", grupoComite.IdDocente);
            return View(grupoComite);
        }

        // GET: Rubric/GrupoComites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrupoComite grupoComite = db.GrupoComite.Find(id);
            if (grupoComite == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdDocente = new SelectList(db.Docente, "IdDocente", "Codigo", grupoComite.IdDocente);
            ViewBag.IdPeriodoAcademico = new SelectList(db.PeriodoAcademico, "IdPeriodoAcademico", "CicloAcademico", grupoComite.IdSubModalidadPeriodoAcademico);
            return View(grupoComite);
        }

        // POST: Rubric/GrupoComites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdGrupoComite,IdDocente,IdPeriodoAcademico")] GrupoComite grupoComite)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grupoComite).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdDocente = new SelectList(db.Docente, "IdDocente", "Codigo", grupoComite.IdDocente);
            ViewBag.IdPeriodoAcademico = new SelectList(db.PeriodoAcademico, "IdPeriodoAcademico", "CicloAcademico", grupoComite.IdSubModalidadPeriodoAcademico);
            return View(grupoComite);
        }

        // GET: Rubric/GrupoComites/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            GrupoComite grupoComite = db.GrupoComite.Find(id);
            if (grupoComite == null)
            {

                return RedirectToAction("NotFound", "Error");
            }
            return View(grupoComite);
        }

        // POST: Rubric/GrupoComites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                GrupoComite grupoComite = db.GrupoComite.Find(id);
                db.GrupoComite.Remove(grupoComite);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {

                PostMessage(MessageType.Error, MessageResource.ElRegistroNoSePuedeEliminarPorqueDependeDeOtrosRegistros);

                return RedirectToAction("Delete", new { id = id });
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
