﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class CursosEvaluarRubricasController : BaseController
    {
        private AbetEntities db = new AbetEntities();

        // GET: Rubric/CursosEvaluarRubricas
        public async Task<ActionResult> Index()
        {
            var idPeriodo = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId).FirstOrDefault().IdPeriodoAcademico;
         
            var cursosEvaluarRubrica = db.CursosEvaluarRubrica.Include(c => c.Curso).Include(c => c.Escuela).Where(x=>x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodo);
            return View(await cursosEvaluarRubrica.ToListAsync());
        }

        /*
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursosEvaluarRubrica cursosEvaluarRubrica = await db.CursosEvaluarRubrica.FindAsync(id);
            if (cursosEvaluarRubrica == null)
            {
                return HttpNotFound();
            }
            return View(cursosEvaluarRubrica);
        }*/

        // GET: Rubric/CursosEvaluarRubricas/Create
        public ActionResult Create()
        {
            
            var curso = (from c in Context.Curso
                             join cpa in Context.CursoPeriodoAcademico on c.IdCurso equals cpa.IdCurso
                             join ccpa in Context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                             join ca in Context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                             where cpa.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId
                             && ca.IdEscuela == this.EscuelaId
                             select new SelectListItem
                             {
                                 Value= c.IdCurso.ToString(),
                                 Text = c.Codigo + "-"  + c.NombreEspanol                          

                             }).Distinct().ToList();


            ViewBag.IdCurso = new SelectList(curso, "Value", "Text");
            
            return View();
        }

        // POST: Rubric/CursosEvaluarRubricas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdCurso,IdEscuela,IdPeriodoAcademico")] CursosEvaluarRubrica cursosEvaluarRubrica)
        {
            try
            {
                int idsubmodapamodu = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId.Value).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                cursosEvaluarRubrica.IdEscuela = EscuelaId;
                cursosEvaluarRubrica.IdSubModalidadPeriodoAcademicoModulo = idsubmodapamodu;

                db.CursosEvaluarRubrica.Add(cursosEvaluarRubrica);
                await db.SaveChangesAsync();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueRealizadoConExito);
                return RedirectToAction("Index");

            }
            catch (Exception)
            {
                PostMessage(MessageType.Warning, MessageResource.ElCursoYaExisteEnBaseDeDatosParaEsteCicloAcademico);


                var curso = (from c in Context.Curso
                             join cpa in Context.CursoPeriodoAcademico on c.IdCurso equals cpa.IdCurso
                             join ccpa in Context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                             join ca in Context.Carrera on ccpa.IdCarrera equals ca.IdCarrera
                             where cpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId
                             && ca.IdEscuela == EscuelaId
                             select new SelectListItem
                             {
                                 Value = c.IdCurso.ToString(),
                                 Text = c.Codigo + "-" + c.NombreEspanol

                             }).Distinct().ToList();

                ViewBag.IdCurso = new SelectList(curso, "Value", "Text", cursosEvaluarRubrica.IdCurso);

                return View(cursosEvaluarRubrica);
                
            }
            
        }
        
        /*
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursosEvaluarRubrica cursosEvaluarRubrica = await db.CursosEvaluarRubrica.FindAsync(id);
            if (cursosEvaluarRubrica == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCurso = new SelectList(db.Curso, "IdCurso", "Codigo", cursosEvaluarRubrica.IdCurso);
            ViewBag.IdEscuela = new SelectList(db.Escuela, "IdEscuela", "Codigo", cursosEvaluarRubrica.IdEscuela);
            return View(cursosEvaluarRubrica);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdCurso,IdEscuela,IdPeriodoAcademico")] CursosEvaluarRubrica cursosEvaluarRubrica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cursosEvaluarRubrica).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdCurso = new SelectList(db.Curso, "IdCurso", "Codigo", cursosEvaluarRubrica.IdCurso);
            ViewBag.IdEscuela = new SelectList(db.Escuela, "IdEscuela", "Codigo", cursosEvaluarRubrica.IdEscuela);
            return View(cursosEvaluarRubrica);
        }*/

        // GET: Rubric/CursosEvaluarRubricas/Delete/5
        public ActionResult Delete(int? IdCurso)
        {
            if (IdCurso == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            CursosEvaluarRubrica cursosEvaluarRubrica = db.CursosEvaluarRubrica.Where(x => x.IdCurso == IdCurso && EscuelaId == this.EscuelaId && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId).FirstOrDefault();
            
            if (cursosEvaluarRubrica == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(cursosEvaluarRubrica);
        }

        // POST: Rubric/CursosEvaluarRubricas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? IdCurso)
        {
            CursosEvaluarRubrica cursosEvaluarRubrica = null;
            try
            {
                cursosEvaluarRubrica = await db.CursosEvaluarRubrica.Where(x => x.IdCurso == IdCurso && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId && EscuelaId == this.EscuelaId).FirstOrDefaultAsync();
                db.CursosEvaluarRubrica.Remove(cursosEvaluarRubrica);
                await db.SaveChangesAsync();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueEliminadoConExito);
            }
            catch (Exception e)
            {
                PostMessage(MessageType.Success, MessageResource.ElRegistroNoSePuedeEliminarPorqueDependeDeOtrosRegistros);
                return View("Delete", cursosEvaluarRubrica);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
