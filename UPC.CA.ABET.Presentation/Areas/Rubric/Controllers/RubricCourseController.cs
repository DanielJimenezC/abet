﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.RubricCourse;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse;
using Ionic.Zip;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.Areas.Rubric.Models;
using UPC.CA.ABET.Logic.Areas.Survey;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using DocumentFormat.OpenXml.Office2010.Excel;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AuthorizeUserAttribute(AppRol.CoordinadorCarrera, AppRol.Docente)]
    public class RubricCourseController : BaseController
    {
        // GET: Rubric/RubricCourse
        public ActionResult RubricCourseIndex(bool? status, bool? endprocess)
        {

            int subModalidadPeriosoAcademico = session.GetSubModalidadPeriodoAcademicoId().Value;
            int escuelaId = session.GetEscuelaId();
            var lstCurso = from ua in Context.UnidadAcademica
                           join sua in Context.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                           join uar in Context.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                           join doc in Context.Docente on uar.IdDocente equals doc.IdDocente
                           join cpa in Context.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                           join cur in Context.Curso on cpa.IdCurso equals cur.IdCurso
                           //join cmc in Context.CursoMallaCurricular on cur.IdCurso equals cmc.IdCurso
                           //join mcd in Context.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                           where ua.IdSubModalidadPeriodoAcademico == subModalidadPeriosoAcademico && ua.IdEscuela == escuelaId
                           //&& uar.IdDocente == idDocente                           
                           select doc.IdDocente;
            bool isCoordinador = true;
            bool isEvaluadorCapstone = false;


            if (session.GetDocenteId().HasValue)
            {
                isCoordinador = lstCurso.Contains(session.GetDocenteId().Value);
                var idDocente = session.GetDocenteId().Value;
                var submodaPaId = session.GetSubModalidadPeriodoAcademicoId().Value;
                var lstEvaluadorCapstone = (from doc in Context.Docente
                                            join us in Context.Usuario on doc.IdDocente equals us.IdDocente
                                            join rus in Context.RolUsuario on us.IdUsuario equals rus.IdUsuario
                                            where doc.IdDocente == idDocente && rus.IdSubModalidadPeriodoAcademico == submodaPaId
                                            select us).ToList().Count();
                if (lstEvaluadorCapstone > 0) isEvaluadorCapstone = true;
            }

            if (endprocess == true)
            {
                PostMessage(MessageType.Success, "Proceso de rúbrica culminado exitosamente.");
            }
            if (endprocess == false)
            {
                PostMessage(MessageType.Error, "ocurrió un error al cerrar el proceso.");
            }

            if (status == true)
            {
                PostMessage(MessageType.Success, "Rúbrica(s) añadida(s) exitosamente.");
            }

            ViewBag.boolCoordinador = isCoordinador;
            ViewBag.boolEvaluadorCapstone = isEvaluadorCapstone;
            ViewBag.boolDocent = session.GetDocenteId().HasValue;
            return View();
        }

        public ActionResult EvaluateRubric(bool? add , int? idCourse, int? IdSection, int? rubricType)
        {
            if (add == true) {
                PostMessage(MessageType.Success, "Alumno evaluado correctamente.");
            }
            RubricCourseViewModel viewModel = new RubricCourseViewModel();

            viewModel.Fill(CargarDatosContext());
            if (idCourse.HasValue && IdSection.HasValue) {
                viewModel.idCurso = idCourse.Value;
                viewModel.idSeccion = IdSection.Value;     
                viewModel.tipoRubrica = rubricType.Value;
            }
            return View(viewModel);
        }

        public ActionResult EvaluateRubricCapstone(bool? add) {
            if (add == true)
            {
                PostMessage(MessageType.Success, "Alumno evaluado correctamente.");
            }
            EvaluateRubricCapstoneViewModel viewModel = new EvaluateRubricCapstoneViewModel();
            int ModalidadId = session.GetModalidadId();
            int? DocenteId = session.GetDocenteId();
            viewModel.docenteId = DocenteId;
            viewModel.subModalidadPeriodoAcademicoId = session.GetSubModalidadPeriodoAcademicoId().Value;
            viewModel.Fill(CargarDatosContext(),ModalidadId);
            return View(viewModel);
        }

        public ActionResult CreateRubricCapstoneEvaluation(Int32 IdCurso, Int32 IdProyectoAcademico) {

            var existRubric = Context.RubricaControl.Select(x => x.IdCurso).Contains(IdCurso);
            if (!existRubric) {
                PostMessage(MessageType.Warning, "Aún no se ha configurado una rúbrica para este curso.");
                return RedirectToAction("EvaluateRubricCapstone", true);
            }
            var idDocente = session.GetDocenteId().Value;
            var idSubmoda = session.GetSubModalidadPeriodoAcademicoId().Value;
            var canRegister = Context.RubricaControlCalifica.Where(x => x.IdDocente == idDocente && x.IdSubModalidadPeriodoAcademico == idSubmoda && x.Register == true).Select(x => x.Register);
            if (canRegister.Count() < 1) {
                PostMessage(MessageType.Warning, "No tiene permisos para evaluar este proyecto.");
                return RedirectToAction("EvaluateRubricCapstone", true);
            }
            var model = new RubricCapstoneEvaluationViewModel();
            model.Fill(CargarDatosContext(), IdProyectoAcademico, IdCurso);
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateRubricCapstoneEvaluation(string [][]arrayData,string [][] arrayData2, int idRubricaControl,int idProyectoAcademico,int idCurso, string observation) {
            var model = new RubricCapstoneEvaluationViewModel();
            model.Fill(CargarDatosContext(), idProyectoAcademico, idCurso);
            model.CreateRubricCapstoneEvaluation(CargarDatosContext(), arrayData, arrayData2, idRubricaControl, idProyectoAcademico, idCurso, observation);
            return View(model);
        }

        public ActionResult EditRubricCapstoneEvaluation(Int32 IdCurso, Int32 IdProyectoAcademico)
        {
            var existRubric = Context.RubricaControl.Select(x => x.IdCurso).Contains(IdCurso);
            if (!existRubric)
            {
                PostMessage(MessageType.Warning, "Aún no se ha configurado una rúbrica para este curso.");
                return RedirectToAction("EvaluateRubricCapstone", true);
            }
            var model = new RubricCapstoneEvaluationViewModel();            
            model.Fill(CargarDatosContext(), IdProyectoAcademico, IdCurso);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditRubricCapstoneEvaluation(string[][] arrayData, string[][] arrayData2, int idRubricaControl, int idProyectoAcademico, int idCurso, string observation)
        {
            var model = new RubricCapstoneEvaluationViewModel();
            model.Fill(CargarDatosContext(), idProyectoAcademico, idCurso);
            model.EditRubricCapstoneEvaluation(CargarDatosContext(), arrayData, arrayData2, idRubricaControl, idProyectoAcademico, idCurso, observation);
            return View(model);
        }

        public ActionResult MaintenanceRubric()
        {
            RubricCourseViewModel viewModel = new RubricCourseViewModel();
            int? idDocente = Session.GetDocenteId();
            viewModel.Fill(CargarDatosContext());
            return View(viewModel);
        }
        
        public ActionResult RubricChangeEstate(Int32 IdRubricControl)
        {
            MaintenanceListRubricViewModel viewModel = new MaintenanceListRubricViewModel();
            viewModel.Fill(CargarDatosContext());
            try
            {
                using (var transaction = new TransactionScope())
                {
                    
                    var rubricCourse = Context.RubricaControl.Find(IdRubricControl);
                    rubricCourse.IsDeleted = true;
                    var rubricCourseNA = Context.RubricaControlNivelAceptacion.Where(x => x.idRubricaControl == IdRubricControl).ToList();
                    foreach (var item in rubricCourseNA)
                    {
                        item.IsDeleted = true;
                    }
                    var rubricCoursePre = Context.RubricaControlPregunta.Where(x => x.idRubricaControl == IdRubricControl).ToList();
                    foreach (var item in rubricCoursePre)
                    {
                        var rubricControlCrit = Context.RubricaControlCriterio.Where(x => x.idRubricaControlPregunta == item.idRubricaControlPregunta).ToList();
                        foreach (var item2 in rubricControlCrit) {
                            item2.IsDeleted = true;
                        }
                        item.IsDeleted = true;
                    }
                    Context.SaveChanges();
                    transaction.Complete();
                }
            } catch (Exception)
            {
                PostMessage(MessageType.Error);
            }
            return RedirectToAction("MaintenanceRubric");
          
        }

        public ActionResult ConfigurationRubric()
        {
            ConfigurationRubricViewModel viewModel = new ConfigurationRubricViewModel();
            string currentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
            viewModel.currentCulture = currentCulture;
            viewModel.Fill(CargarDatosContext());
            if (viewModel.LstCurso.Count() < 1)
            {
                PostMessage(MessageType.Warning, "No se existen cursos para configurar");
                return RedirectToAction("MaintenanceRubric",true);
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ConfigurationRubric(string [][] dataSettingRubric,  int idRubricControl, List<int> lstIdRubricControlAcceptanceLevel)
        {
            var success = CreateRubricControlPregunta(dataSettingRubric, idRubricControl, lstIdRubricControlAcceptanceLevel);
            ConfigurationRubricViewModel viewModel = new ConfigurationRubricViewModel();            
            viewModel.Fill(CargarDatosContext());
            return RedirectToAction("RubricCourseIndex");
        }

        public JsonResult SaveCriteria(string [][] dataSettingRubric, int idCourse, int rubricType)
        {            
            var idRubricControl = CreateRubricControl(idCourse, rubricType);
            var lstIdRubricControlAcceptanceLevel = new List<int>();
            decimal pesoMax = 0;
            for (int i = 0; i < dataSettingRubric.Count(); i++)
            {
                var idRubricControlAcceptanceLevel = CreateRubriControlAcceptanceLevel(idRubricControl, dataSettingRubric[i]);
                lstIdRubricControlAcceptanceLevel.Add(idRubricControlAcceptanceLevel);
                if (pesoMax < dataSettingRubric[i][1].ToDecimal()) {
                    pesoMax = dataSettingRubric[i][1].ToDecimal();
                }
            }

            var data = new { idRubricControl, lstIdRubricControlAcceptanceLevel, pesoMax};
            return Json(data);
        }

        public int CreateRubricControl(int idCurso, int rubricType) {
            var idDocente = session.GetDocenteId();
            var submodalidaPA = session.GetSubModalidadPeriodoAcademicoId();
            ConfigurationRubricViewModel viewModel = new ConfigurationRubricViewModel();
            var result = viewModel.CreateRubricControl(idCurso, idDocente.ToInteger(), submodalidaPA.ToInteger(), rubricType);
            return result;

        }

        public int CreateRubriControlAcceptanceLevel(int idRubricControl, string [] dataAcceptanceLevel)
        {
            var idDocente = session.GetDocenteId();
            ConfigurationRubricViewModel viewModel = new ConfigurationRubricViewModel();
            var result = viewModel.CreateRubriControlAcceptanceLevel(idRubricControl, dataAcceptanceLevel);
            return result;
        }

        public bool CreateRubricControlPregunta(string[][] dataSettingRubric, int idRubricControl, List<int> lstIdRubricControlAcceptanceLevel) {
            ConfigurationRubricViewModel viewModel = new ConfigurationRubricViewModel();
            var result = viewModel.CreateRubricControlPregunta(dataSettingRubric, idRubricControl, lstIdRubricControlAcceptanceLevel);
            return result;
        }

        public ActionResult UploadRubricTemplate()
        {
            var model = new UploadRubricViewModel();
            int ModalidadId = session.GetModalidadId();

            string currentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
            model.currentCulture = currentCulture;
            model.Fill(CargarDatosContext(), ModalidadId);
            if (!model.isCoordinador)
            {
                PostMessage(MessageType.Warning, "No cuenta con permisos para realizar esta opción.");
                return RedirectToAction("RubricCourseIndex");
            }

            return View(model);
            
        }

        


        [HttpPost]
        public JsonResult UploadRubricControl(UploadRubricViewModel model)
        {
            // ENVIAR MENSAJE A DOCENTES
            try
            {
                bool IsFinal = false;
                if(model.tipoRubrica == 1)
                {
                    IsFinal = false;
                }
                else
                {
                    IsFinal = true;
                }
                
                model.subModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                int SubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId.Value ;

                String fileExtension = System.IO.Path.GetExtension(model.Archivo.FileName);

                if (fileExtension == ".xlsx")
                {

                    Int32 colCodigoCurso = 0;
                    Int32 colPregunta = 1;
                    Int32 colPuntajeMaximo = 2;
                    Int32 colNombreCriterio = 3;
                    Int32 colCriterio = 4;
                    Int32 colPeso = 5;

                    var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                    List<RubricControlExcelObj> excelList = new List<RubricControlExcelObj>();
                    //List<string> lstError = new List<string>();
                    string lstError = "";
                    for (int i = 2; i < dataTable.Rows.Count; i++)
                    {
                        if (String.IsNullOrEmpty(dataTable.Rows[i][colCodigoCurso].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPregunta].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximo].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colNombreCriterio].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colCriterio].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPeso].ToString())
                            )
                        {
                            continue;
                        }

                        if (String.IsNullOrEmpty(dataTable.Rows[i][colCodigoCurso].ToString()))
                        {
                            lstError += ($"Falta completar el campo código curso en la fila {i - 1}\n");

                        } else if (String.IsNullOrEmpty(dataTable.Rows[i][colPregunta].ToString()))
                        {
                            lstError += ($"Falta completar el campo pregunta en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximo].ToString()))
                        {
                            lstError += ($"Falta completar el campo puntaje máximo en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colNombreCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo descripción del criterio en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo criterio en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPeso].ToString()))
                        {
                            lstError += ($"Falta completar el campo peso del criterio en la fila {i - 1}\n");
                        }
                        else if (dataTable.Rows[i][colPeso].ToDecimal() > 100)
                        {
                            lstError += ($"El campo peso de la fila {i - 1} es inválido\n");
                        }
                        else if (dataTable.Rows[i][colPuntajeMaximo].ToDecimal() > 20)
                        {
                            lstError += ($"El puntaje máximo de la fila {i - 1} es inválido\n");
                        }


                        var CodigoCurso = dataTable.Rows[i][colCodigoCurso].ToString();
                        var PreguntaRubrica = dataTable.Rows[i][colPregunta].ToString();
                        decimal PuntajeMaximo = dataTable.Rows[i][colPuntajeMaximo].ToDecimal();
                        var NombreCriterio = dataTable.Rows[i][colNombreCriterio].ToString();
                        var Criterio = dataTable.Rows[i][colCriterio].ToString();
                        decimal Peso = dataTable.Rows[i][colPeso].ToDecimal();

                        RubricControlExcelObj obj = new RubricControlExcelObj(CodigoCurso, PreguntaRubrica, PuntajeMaximo, NombreCriterio, Criterio, Peso);
                        excelList.Add(obj);

                    }

                    var cursos = excelList.Select(x => x.CodigoCurso).Distinct();
                    foreach (string curso in cursos)
                    {
                        var listaCarga = excelList.Where(x => x.CodigoCurso == curso);                        

                        var preguntas = excelList.Where(x => x.CodigoCurso == curso)
                                    .GroupBy(x => new { x.Pregunta, x.PuntajeMaximo })
                                    .Select(gcs => new PreguntaGroupBy()
                                    {
                                        Pregunta = gcs.Key.Pregunta,
                                        PuntajeMaximo = gcs.Key.PuntajeMaximo,
                                        Children = gcs.ToList(),
                                    })
                                    .ToList();
                        decimal nota = 0;
                        foreach(PreguntaGroupBy pregunta in preguntas)
                        {
                          nota += pregunta.PuntajeMaximo;
                        }
                        
                        if(nota  != 20)
                        {
                            lstError += ($"La nota de la rúbrica del curso {curso} no tiene puntaje de 20\n");
                        }

                    }
                        

                    if (String.IsNullOrEmpty(lstError))
                    {
                        bool validRegister = true;
                        
                        string messageCursoCargado = "Los siguientes cursos ya cuentan con rúbrica:\n\n";
                        foreach(string curso in cursos)
                        {
                            var cursoObj = Context.CursoPeriodoAcademico.Include("Curso")
                                .Where(x => x.Curso.Codigo == curso && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico)
                                .Select(x => x.Curso)
                                .FirstOrDefault();
                            var IdCurso = cursoObj.IdCurso;
                            var IdDocente = session.GetDocenteId();

                            var rubricaExiste = Context.RubricaControl.Where(x => x.IdCurso == IdCurso
                            && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico
                            && x.IsFinal == IsFinal).FirstOrDefault();

                            if(rubricaExiste != null)
                            {
                                validRegister = false;
                                messageCursoCargado += $"{cursoObj.Codigo} - {cursoObj.NombreEspanol}\n";
                                continue;
                            }

                            RubricaControl rubricaControl = new RubricaControl();
                            rubricaControl.IdCurso = IdCurso;
                            rubricaControl.IdDocente = IdDocente.Value;
                            rubricaControl.fecha = DateTime.Now;
                            rubricaControl.IsDeleted = false;
                            rubricaControl.IsFinal = IsFinal;
                            rubricaControl.IdSubModalidadPeriodoAcademico = SubModalidadPeriodoAcademico;
                            Context.RubricaControl.Add(rubricaControl);

                            var preguntas = excelList.Where(x => x.CodigoCurso == curso)
                                        .GroupBy(x => new { x.Pregunta,x.PuntajeMaximo} )
                                        .Select(gcs=> new PreguntaGroupBy()
                                        {
                                            Pregunta = gcs.Key.Pregunta,
                                            PuntajeMaximo = gcs.Key.PuntajeMaximo,                                            
                                            Children = gcs.ToList(),
                                        })
                                        .ToList() ;
                            
                            List<RubricaControlPregunta> preguntasRegister = new List<RubricaControlPregunta>();

                            foreach(PreguntaGroupBy obj in preguntas)
                            {
                                RubricaControlPregunta rcp = new RubricaControlPregunta();
                                rcp.RubricaControl = rubricaControl;
                                rcp.preguntaEspanol = obj.Pregunta;
                                rcp.puntajeMaximo = obj.PuntajeMaximo;

                                Context.RubricaControlPregunta.Add(rcp);

                                preguntasRegister.Add(rcp);
                            }

                            var nivelAceptacion= excelList.Where(x => x.CodigoCurso == curso)
                                        .GroupBy(x => new { x.Criterio ,x.Peso})
                                        .Select(gcs => new NivelAceptacionGroupBy()
                                        {
                                            Criterio = gcs.Key.Criterio,
                                            Peso = gcs.Key.Peso,
                                            Children = gcs.ToList(),
                                        })
                                        .ToList();

                            List<RubricaControlNivelAceptacion> naRegister = new List<RubricaControlNivelAceptacion>();

                            foreach (NivelAceptacionGroupBy obj in nivelAceptacion)
                            {
                                RubricaControlNivelAceptacion rcn = new RubricaControlNivelAceptacion();
                                rcn.RubricaControl = rubricaControl;
                                rcn.peso = obj.Peso;
                                rcn.descripcionEspanol = obj.Criterio;

                                Context.RubricaControlNivelAceptacion.Add(rcn);

                                naRegister.Add(rcn);
                            }


                            var listaCarga = excelList.Where(x => x.CodigoCurso == curso);

                            foreach (RubricControlExcelObj row in listaCarga)
                            {
                                RubricaControlCriterio rcc = new RubricaControlCriterio();

                                rcc.RubricaControlPregunta = preguntasRegister.Where(x => x.preguntaEspanol == row.Pregunta && x.puntajeMaximo == row.PuntajeMaximo).FirstOrDefault();
                                rcc.RubricaControlNivelAceptacion = naRegister.Where(x => x.descripcionEspanol == row.Criterio && x.peso == row.Peso).FirstOrDefault();
                                rcc.descripcionEspanol = row.NombreCriterio;


                                Context.RubricaControlCriterio.Add(rcc);
                            }
                        }

                        Context.SaveChanges();

                        if (validRegister)
                        {
                            return Json(new
                            {
                                code = 1,
                                message = "Rúbrica cargada exitosamente"
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                code = 4,
                                message = messageCursoCargado
                            });
                        }

                     
                    }
                    else
                    {
                        return Json(new
                        {
                            code = 3,
                            message = lstError
                        });
                    }
                    
                }
                else
                {
                    return Json(new
                    {
                        code = 2,
                        message = "Archivo con extensión incorrecta"
                    });
                }

           
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    code = 0,
                    message = ex.Message
                });
            }

        }


        [HttpPost]
        public JsonResult UploadRubricControlCapstone(UploadRubricViewModel model)
        {
            // ENVIAR MENSAJE A DOCENTES
            try
            {
                bool IsFinal = false;

                model.subModalidadPeriodoAcademicoId = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == model.CicloId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                int SubModalidadPeriodoAcademico = model.subModalidadPeriodoAcademicoId.Value;

                String fileExtension = System.IO.Path.GetExtension(model.Archivo.FileName);

                if (fileExtension == ".xlsx")
                {

                    Int32 colCodigoCurso = 0;
                    Int32 colPregunta = 1;
                    Int32 colPuntajeMaximo = 2;
                    Int32 colNombreCriterio = 3;
                    Int32 colCriterio = 4;
                    Int32 colPuntajeMinimoCriterio = 5;
                    Int32 colPuntajeMaximoCriterio = 6;

                    var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                    List<RubricControlExcelObjCapstone> excelList = new List<RubricControlExcelObjCapstone>();
                    //List<string> lstError = new List<string>();
                    string lstError = "";
                    for (int i = 2; i < dataTable.Rows.Count; i++)
                    {
                        if (String.IsNullOrEmpty(dataTable.Rows[i][colCodigoCurso].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPregunta].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximo].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colNombreCriterio].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colCriterio].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMinimoCriterio].ToString()) &&
                            String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximoCriterio].ToString())
                            )
                        {
                            continue;
                        }

                        if (String.IsNullOrEmpty(dataTable.Rows[i][colCodigoCurso].ToString()))
                        {
                            lstError += ($"Falta completar el campo código curso en la fila {i - 1}\n");

                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPregunta].ToString()))
                        {
                            lstError += ($"Falta completar el campo pregunta en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximo].ToString()))
                        {
                            lstError += ($"Falta completar el campo puntaje máximo en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colNombreCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo descripción del criterio en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo criterio en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMinimoCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo puntaje mínimo del criterio en la fila {i - 1}\n");
                        }
                        else if (String.IsNullOrEmpty(dataTable.Rows[i][colPuntajeMaximoCriterio].ToString()))
                        {
                            lstError += ($"Falta completar el campo puntaje máximo del criterio en la fila {i - 1}\n");
                        }
                        else if (dataTable.Rows[i][colPuntajeMinimoCriterio].ToDecimal() > dataTable.Rows[i][colPuntajeMaximo].ToDecimal())
                        {
                            lstError += ($"El puntaje mínimero del criterio debe ser menor al puntaje máximo de la pregunta en la fila {i - 1}\n");
                        }
                        else if (dataTable.Rows[i][colPuntajeMaximoCriterio].ToDecimal() > dataTable.Rows[i][colPuntajeMaximo].ToDecimal())
                        {
                            lstError += ($"El puntaje máximo del criterio debe ser menor al puntaje máximo de la pregunta en la fila {i - 1}\n");
                        }
                        else if (dataTable.Rows[i][colPuntajeMaximo].ToDecimal() > 20)
                        {
                            lstError += ($"El puntaje máximo de la fila {i - 1} es inválido\n");
                        }


                        var CodigoCurso = dataTable.Rows[i][colCodigoCurso].ToString();
                        var PreguntaRubrica = dataTable.Rows[i][colPregunta].ToString();
                        decimal PuntajeMaximo = dataTable.Rows[i][colPuntajeMaximo].ToDecimal();
                        var NombreCriterio = dataTable.Rows[i][colNombreCriterio].ToString();
                        var Criterio = dataTable.Rows[i][colCriterio].ToString();
                        decimal PuntajeMinimoCriterio = dataTable.Rows[i][colPuntajeMinimoCriterio].ToDecimal();
                        decimal PuntajeMaximoCriterio = dataTable.Rows[i][colPuntajeMaximoCriterio].ToDecimal();

                        RubricControlExcelObjCapstone obj = new RubricControlExcelObjCapstone(CodigoCurso, PreguntaRubrica, PuntajeMaximo, NombreCriterio, Criterio, PuntajeMinimoCriterio, PuntajeMaximoCriterio);
                        excelList.Add(obj);

                    }

                    var cursos = excelList.Select(x => x.CodigoCurso).Distinct();
                    foreach (string curso in cursos)
                    {
                        var listaCarga = excelList.Where(x => x.CodigoCurso == curso);

                        var preguntas = excelList.Where(x => x.CodigoCurso == curso)
                                    .GroupBy(x => new { x.Pregunta, x.PuntajeMaximo })
                                    .Select(gcs => new PreguntaGroupBy()
                                    {
                                        Pregunta = gcs.Key.Pregunta,
                                        PuntajeMaximo = gcs.Key.PuntajeMaximo,
                                        //Children = gcs.ToList(),
                                    })
                                    .ToList();
                        decimal nota = 0;
                        foreach (PreguntaGroupBy pregunta in preguntas)
                        {
                            nota += pregunta.PuntajeMaximo;
                        }

                        if (nota != 20)
                        {
                            lstError += ($"La nota de la rúbrica del curso {curso} no tiene puntaje de 20\n");
                        }

                    }


                    if (String.IsNullOrEmpty(lstError))
                    {
                        bool validRegister = true;

                        string messageCursoCargado = "Los siguientes cursos ya cuentan con rúbrica:\n\n";
                        foreach (string curso in cursos)
                        {
                            var cursoObj = Context.CursoPeriodoAcademico.Include("Curso")
                                .Where(x => x.Curso.Codigo == curso && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico)
                                .Select(x => x.Curso)
                                .FirstOrDefault();
                            var IdCurso = cursoObj.IdCurso;
                            var IdDocente = session.GetDocenteId();

                            var rubricaExiste = Context.RubricaControl.Where(x => x.IdCurso == IdCurso
                            && x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico
                            && x.IsFinal == IsFinal).FirstOrDefault();

                            if (rubricaExiste != null)
                            {
                                validRegister = false;
                                messageCursoCargado += $"{cursoObj.Codigo} - {cursoObj.NombreEspanol}\n";
                                continue;
                            }

                            RubricaControl rubricaControl = new RubricaControl();
                            rubricaControl.IdCurso = IdCurso;
                            rubricaControl.IdDocente = IdDocente.Value;
                            rubricaControl.fecha = DateTime.Now;
                            rubricaControl.IsDeleted = false;
                            rubricaControl.IsFinal = IsFinal;
                            rubricaControl.IdSubModalidadPeriodoAcademico = SubModalidadPeriodoAcademico;
                            Context.RubricaControl.Add(rubricaControl);

                            var preguntas = excelList.Where(x => x.CodigoCurso == curso)
                                        .GroupBy(x => new { x.Pregunta, x.PuntajeMaximo })
                                        .Select(gcs => new PreguntaGroupBy()
                                        {
                                            Pregunta = gcs.Key.Pregunta,
                                            PuntajeMaximo = gcs.Key.PuntajeMaximo,
                                            ChildrenCapstone = gcs.ToList(),
                                        })
                                        .ToList();

                            List<RubricaControlPregunta> preguntasRegister = new List<RubricaControlPregunta>();

                            foreach (PreguntaGroupBy obj in preguntas)
                            {
                                RubricaControlPregunta rcp = new RubricaControlPregunta();
                                rcp.RubricaControl = rubricaControl;
                                rcp.preguntaEspanol = obj.Pregunta;
                                rcp.puntajeMaximo = obj.PuntajeMaximo;

                                Context.RubricaControlPregunta.Add(rcp);

                                //preguntasRegister.Add(rcp);

                                foreach(RubricControlExcelObjCapstone criterio in obj.ChildrenCapstone)
                                {
                                    RubricaControlCriterioCapstone criterioRegister = new RubricaControlCriterioCapstone();
                                    criterioRegister.RubricaControlPregunta = rcp;
                                    criterioRegister.nombreEspanol = criterio.Criterio;
                                    criterioRegister.nombreIngles = "";
                                    criterioRegister.descripcionEspanol = criterio.NombreCriterio;
                                    criterioRegister.descripcionIngles = "";
                                    criterioRegister.minimo = criterio.PuntajeMinimoCriterio;
                                    criterioRegister.maximo = criterio.PuntajeMaximoCriterio;

                                    Context.RubricaControlCriterioCapstone.Add(criterioRegister);
                                }
                            }


                            //var listaCarga = excelList.Where(x => x.CodigoCurso == curso);

                            //foreach (RubricControlExcelObj row in listaCarga)
                            //{
                            //    RubricaControlCriterio rcc = new RubricaControlCriterio();

                            //    rcc.RubricaControlPregunta = preguntasRegister.Where(x => x.preguntaEspanol == row.Pregunta && x.puntajeMaximo == row.PuntajeMaximo).FirstOrDefault();
                            //    rcc.RubricaControlNivelAceptacion = naRegister.Where(x => x.descripcionEspanol == row.Criterio && x.peso == row.Peso).FirstOrDefault();
                            //    rcc.descripcionEspanol = row.NombreCriterio;


                            //    Context.RubricaControlCriterio.Add(rcc);
                            //}
                        }

                        Context.SaveChanges();

                        if (validRegister)
                        {
                            return Json(new
                            {
                                code = 1,
                                message = "Rúbrica cargada exitosamente"
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                code = 4,
                                message = messageCursoCargado
                            });
                        }


                    }
                    else
                    {
                        return Json(new
                        {
                            code = 3,
                            message = lstError
                        });
                    }

                }
                else
                {
                    return Json(new
                    {
                        code = 2,
                        message = "Archivo con extensión incorrecta"
                    });
                }


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    code = 0,
                    message = ex.Message
                });
            }

        }

        public ActionResult DownloadSeccionbyCourse()
        {
            var viewModel = new DowloadRubricViewModel();
            viewModel.Fill(CargarDatosContext(),null);

            return View(viewModel);
        }

        public ActionResult BulkDownloadRubric()
        {
            var viewModel = new DowloadRubricViewModel();
            viewModel.Fill(CargarDatosContext(),null);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult DownloadZipRubric(int idCurso,int idSection,int tipoRubrica)
        {
            var viewModel = new DowloadRubricViewModel();
            string handle = Guid.NewGuid().ToString();

            MemoryStream objStream = null;
            List<String> lstRutas = null;
            var objCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == idCurso);
            string curso = objCurso.NombreEspanol;

            String nombreReporte = curso + ".zip";

          

            if ( curso == "TALLER DE PROYECTO 1" || curso == "TALLER DE PROYECTO 2")
            {
                var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.TEMPLATE_RUBRIC_COURCE_CAPSTONE));
                var lstExcel = ExcelRubricCourceCapstone.GenerateExcelRubricCourse_Capstone(Context, idCurso, objCurso.Codigo, templatePath, CargarDatosContext(), tipoRubrica);
                if (lstExcel.Any())
                {
                    var objZip = new ZipFile();
                    lstRutas = new List<String>();

                    foreach (var objExcel in lstExcel)
                    {
                        var nombre = ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + objExcel.Worksheet(1).Name + ".xlsx";
                        objExcel.SaveAs(nombre);
                        objZip.AddFile(nombre);
                        lstRutas.Add(nombre);
                    }

                    objStream = new MemoryStream();
                    objZip.Save(objStream);
                    //byte[] fileBytes = objStream.ToArray();
                    TempData[handle] = objStream.ToArray();

                    //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Zip, nombreReporte);
                    return new JsonResult()
                    {
                        Data = new { FileGuid = handle, FileName = nombreReporte }
                    };
                }
                else
                {

                    return RedirectToAction("OutcomeReport");
                }

            }
            else
            {
                var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.TEMPLATE_RUBRIC_COURCE));
                var lstExcel = ExcelRubricCource.GenerateExcelRubricCourse(Context, idCurso, idSection, objCurso.Codigo, templatePath, CargarDatosContext(), tipoRubrica);
                if (lstExcel.Any())
                {
                    var objZip = new ZipFile();
                    lstRutas = new List<String>();

                    foreach (var objExcel in lstExcel)
                    {
                        var nombre = ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + objExcel.Worksheet(1).Name + ".xlsx";
                        objExcel.SaveAs(nombre);
                        objZip.AddFile(nombre);
                        lstRutas.Add(nombre);
                    }

                    objStream = new MemoryStream();
                    objZip.Save(objStream);
                    //byte[] fileBytes = objStream.ToArray();
                    TempData[handle] = objStream.ToArray();

                    return new JsonResult()
                    {
                        Data = new { FileGuid = handle, FileName = nombreReporte }
                    };
                    //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Zip, nombreReporte);
                }
                else
                {
                    return RedirectToAction("OutcomeReport");
                }

            }
        }


        public ActionResult DownloadExcelSeccion(int idCurso)
        {
            string handle = Guid.NewGuid().ToString();

            var viewModel = new DowloadRubricViewModel();

            viewModel.Fill(CargarDatosContext(),idCurso);
            MemoryStream objStream = null;

            var objCurso = Context.Curso.FirstOrDefault(x => x.IdCurso == idCurso);
            string curso = objCurso.NombreEspanol;

            int submodal = session.GetSubModalidadPeriodoAcademicoId().Value;
            String nombreReporte = curso + ".xlsx";

            var templatePath = Server.MapPath(Path.Combine(ConstantHelpers.TEMP_PATH_REPORTS_RUBRICS, ConstantHelpers.TEMPLATE_CALIFICACION_COURCE));
            var lstExcel = ExcelSeccionByCourse.GenerateExcelSeccionByCourse(CargarDatosContext(), idCurso,viewModel.lstNotas,templatePath);

            objStream = new MemoryStream();

            lstExcel.SaveAs(objStream);
            //objStream.Position = 0;
            TempData[handle] = objStream.ToArray();
            //byte[] fileBytes = objStream.ToArray();

            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = nombreReporte }
            };

            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreReporte);

        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult _StudentListRubricPartialView(int IdSection, int IdCourse, int rubricType)
        {
            var viewModel = new StudentListRubricViewModel();
            viewModel.Fill(CargarDatosContext(), IdSection, IdCourse, rubricType);
            return PartialView("PartialView/_StudentListRubricPartialView", viewModel);
        }

        public ActionResult _MaintenanceListRubricPartialView()
        {
            var viewModel = new MaintenanceListRubricViewModel();
            viewModel.Fill(CargarDatosContext());
            return PartialView("PartialView/_MaintenanceListRubricPartialView", viewModel);
        }

        public ActionResult CreateRubricControlEvaluation(int idStudent, int idCourse, int idSection, int rubricType) {
            RubricControlEvaluationViewModel viewModel = new RubricControlEvaluationViewModel();
            viewModel.Fill(CargarDatosContext(), idStudent, idCourse, idSection, rubricType);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CreateRubricControlEvaluation(string [][] arrayData, int idRubricaControl,int idAlumno,int idDocenteCalificador, int idSeccion, int idCurso, int rubricType) {
            RubricControlEvaluationViewModel viewModel = new RubricControlEvaluationViewModel();
            viewModel.Fill(CargarDatosContext(), idAlumno, idCurso, idSeccion, rubricType);
            viewModel.CreateRubricControlEvaluation(arrayData, idRubricaControl, idAlumno, idDocenteCalificador, idSeccion);            
            return View(viewModel);
        }

        public ActionResult EditRubricControlEvaluation(int idRubric)
        {
            RubricControlViewModel viewModel = new RubricControlViewModel();
            viewModel.Fill(CargarDatosContext(), idRubric);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult EditRubricControlEvaluation(RubricControlViewModel viewModel)
        {
            try
            {
                var rubricControl = Context.RubricaControl.FirstOrDefault(x => x.idRubricaControl == viewModel.idRubricaControl);                
                var lstrcp = Context.RubricaControlPregunta.Where(x => x.idRubricaControl == viewModel.idRubricaControl).ToList();
                var lstrcc = Context.RubricaControlCriterio.Where(x => x.RubricaControlPregunta.idRubricaControl == viewModel.idRubricaControl).ToList();

                for (int i = 0; i < viewModel.lstRCPregunta.Count(); i++)
                {
                    lstrcp[i].preguntaEspanol = viewModel.lstRCPregunta[i].preguntaEspanol;
                    lstrcp[i].puntajeMaximo = viewModel.lstRCPregunta[i].puntajeMaximo;
                    for (int j = 0; j < viewModel.lstRCCriterio.Count(); j++)
                    {     
                        lstrcc[j].descripcionEspanol = viewModel.lstRCCriterio[j].descripcionEspanol;
                        if (lstrcp[i].idRubricaControlPregunta == lstrcc[j].idRubricaControlPregunta)
                        {
                            var rccId = lstrcc[j].idRubricaControlCriterio;
                            var rcc = Context.RubricaControlCriterio.FirstOrDefault(x => x.idRubricaControlCriterio == rccId);
                            rcc.descripcionEspanol = lstrcc[j].descripcionEspanol;
                        }
                    }    
                }              

                rubricControl.RubricaControlPregunta = lstrcp;
                Context.SaveChanges();
                PostMessage(MessageType.Success);
            }
            catch(DbEntityValidationException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                PostMessage(MessageType.Error, ex.ToString());
            }
            return RedirectToAction("MaintenanceRubric");
        }


        public ActionResult ManageStudent()
        {
            var viewModel = new ManagmentStudentViewModel();
            viewModel.Fill(CargarDatosContext());            
            return PartialView("PartialView/_ManageStudent", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageStudent(ManagmentStudentViewModel ViewModel)
        {

            if (ModelState.IsValid)
            {
                var success = SaveStudent(ViewModel);
                if (success)
                {
                    ViewModel.Fill(CargarDatosContext());
                    return Json(new { success = true, title = "¡Éxito!", message = "Alumno agregado al curso exitosamente." }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { success = false, title = "Ocurrió un error", message = "Error al agregar el alumno" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, title = "Ocurrió un error", message = "Modelo Erróneo" }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool SaveStudent(ManagmentStudentViewModel viewModel)
        {
            var success = viewModel.SaveStudent(viewModel.Codigo, viewModel.NombreAlumno, viewModel.ApellidoAlumno ,viewModel.IdCurso, viewModel.IdCarrera, viewModel.IdSeccion, Session.GetDocenteId().Value);
            if (success)
                return true;            
            else
                return false;                
        }

        public JsonResult GetSectionByCourse(int idCurso) {
            try {
                var idDocente = Session.GetDocenteId();
                var idSubmodalidad = Session.GetSubModalidadPeriodoAcademicoId();
                var hasRubricConfiguration = Context.RubricaControl.Select(x => x.IdCurso).Contains(idCurso);
                if (hasRubricConfiguration)
                {
                    var lstSection = (from mcd in Context.MallaCocosDetalle
                                      join cmc in Context.CursoMallaCurricular on mcd.IdCursoMallaCurricular equals cmc.IdCursoMallaCurricular
                                      join cur in Context.Curso on cmc.IdCurso equals cur.IdCurso
                                      join sc in Context.SeccionCurso on cur.IdCurso equals sc.IdCurso
                                      join lc in Context.LogCarga on sc.IdLogCarga equals lc.IdLogCarga
                                      join sec in Context.Seccion on sc.IdSeccion equals sec.IdSeccion
                                      join ds in Context.DocenteSeccion on sc.IdSeccion equals ds.IdSeccion
                                      where mcd.IdTipoOutcomeCurso == 1 &&
                                            lc.IdSubModalidadPeriodoAcademico == idSubmodalidad &&
                                            cur.IdCurso == idCurso &&
                                            ds.IdDocente == idDocente
                                      select sec).Distinct().Select(x => new { x.Codigo, x.IdSeccion }).ToList();


                    return Json(lstSection);
                }
                else
                {

                    return Json(null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        

        public JsonResult GetStudents(String codigo)
        {
            var query = Context.Alumno.Where(x => x.Codigo == codigo).FirstOrDefault();
            var data = new { NombreAlumno = query.Nombres, ApellidoAlumno = query.Apellidos };
            return Json(data);
        }

        public ActionResult NotificationRubric()
        {
            var viewModel = new NotificationRubricViewModel();
            viewModel.fill(CargarDatosContext(),this.PeriodoAcademicoId,this.EscuelaId);
            return View(viewModel);
        }

        public ActionResult LstNotificationTeach(int idCurso)
        {
            var viewModel = new LstNotificationTeachViewModel();
            viewModel.fill(CargarDatosContext(), idCurso);
            return PartialView("PartialView/_LstNotificationTeach", viewModel);
        }

        [HttpPost]
        public JsonResult GuardarConfiguracionNotificacionEvaluadores(string asunto, string mensaje)
        {
            var objMensaje = Context.Mensaje.Where(x => EscuelaId == this.EscuelaId).FirstOrDefault();

            if (objMensaje != null)
            {

                objMensaje.Asunto = asunto;
                objMensaje.Mensaje1 = mensaje;
                Context.Entry(objMensaje).State = System.Data.Entity.EntityState.Modified;
                Context.SaveChanges();
            }
            else
            {
                var notify = new Mensaje();
                notify.idEscuela = Session.GetEscuelaId();
                notify.idMensaje = 1;
                notify.Asunto = asunto;
                notify.Mensaje1 = mensaje;
                Context.Mensaje.Add(notify);
                Context.SaveChanges();
            }
            return Json("1");

        }

        [HttpPost]
        public JsonResult NotificarDocentes(int idCurso)
        {
            var cursoCreado = Context.RubricaControl.Where(x => x.IsDeleted == false).Select(x => x.IdCurso).ToList();
            var docenteId = Session.GetDocenteId();
            var subModalidadId = Session.GetSubModalidadPeriodoAcademicoId();
            var lenguage = Session.GetCulture().ToString();            
            var LstDocenteProcd = Context.GetListNotificationTeachRubric(docenteId, subModalidadId, idCurso, lenguage).Where(x=> cursoCreado.Contains(x.IdCurso));            
            int subModalidadPa = session.GetSubModalidadPeriodoAcademicoId().ToInteger();
            var date = DateTime.Now;
            var contra = "ABET" + date.Year;
            
            try
            {
                RubricCourseLogic logic = new RubricCourseLogic();

                String vdirectory = HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;

                String urlImage = String.Format("{0}://{1}{2}",
                                    Request.Url.Scheme,
                                    Request.Url.Authority,
                                    vdirectory);

                var objAsunto = "Evaluación de rúbrica";
                var dictionary = new Dictionary<int, String>();
                
                foreach (var docente in LstDocenteProcd)
                {

                    if (!dictionary.Keys.Contains(docente.IdDocente))
                    {
                        dictionary.Add(docente.IdDocente, docente.Nombre);
                    }
                    logic.SendMailRubric(objAsunto, logic.GetHtmlEmailRubric(docente.NombreCurso, urlImage+ "/Rubric/RubricCourse/EvaluateRubric", urlImage + "/Areas/Rubric/Resources/Images/headerMailUPCRubric.jpeg", urlImage + "/Areas/Rubric/Resources/Images/footerMailUPC.jpg",docente.Codigo,contra), "u201415738@upc.edu.pe");
                }

                foreach (var dic in dictionary)
                {
                    var isExist = Context.Usuario.Select(x => x.IdDocente).Contains(dic.Key);
                    var security = new SecurityLogic();
                    var password = security.MD5Hash(contra);

                    if (isExist)
                    {
                        var usuario = Context.Usuario.Where(x => x.IdDocente == dic.Key).FirstOrDefault();
                        using (var transaction = new TransactionScope())
                        {
                            usuario.Password = password;
                            Context.SaveChanges();
                            if (Context.RolUsuario.Where(x => x.IdUsuario == usuario.IdUsuario && x.IdSubModalidadPeriodoAcademico == subModalidadPa).Count() < 1)
                            {
                                var userRol = new RolUsuario();
                                userRol.IdSubModalidadPeriodoAcademico = subModalidadPa;
                                userRol.IdUsuario = usuario.IdUsuario;
                                userRol.FechaCreacion = DateTime.Now;
                                userRol.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                userRol.IdEscuela = 1;
                                userRol.IdRol = 3;
                                Context.RolUsuario.Add(userRol);
                                Context.SaveChanges();
                            }
                            transaction.Complete();
                        }                       
                    }
                    else
                    {
                        logic.CreateNewUser(dic.Key, password, subModalidadPa);
                    }

                }

                return Json("1");
            }
            catch (Exception ex)
            {
                return Json("0");
            }

        }

        public ActionResult CapstoneRubricFinalProcess()
        {
            var idSumodalidadPA = session.GetSubModalidadPeriodoAcademicoId().Value;
            bool response = RubricCourseLogic.CapstineRubricFinalProcess(CargarDatosContext(), idSumodalidadPA);
            if (response) return RedirectToAction("RubricCourseIndex", new { endprocess = true });
            else return RedirectToAction("RubricCourseIndex", new { endprocess = false });
        }
    }
}