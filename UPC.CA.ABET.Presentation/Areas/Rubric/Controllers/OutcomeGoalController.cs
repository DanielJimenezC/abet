﻿
#region Imports

using System;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.OutcomeGoal;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels.OutcomeGoal.PartialView;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
    public class OutcomeGoalController : BaseController
    {
        #region Get

        /// <summary>
        /// Retorna la página de Listar Logros Outcomes.
        /// </summary>
        /// <returns>View</returns>
        public ActionResult ListOutcomeGoal()
        {
            try
            {

                var viewModel = new ListOutcomeGoalViewModel();
                viewModel.CargarDatos(CargarDatosContext());
                return View(viewModel);
            }
            catch (Exception ex) 
            {
                PostMessage(MessageType.Warning, ex.Message);
                return RedirectToAction("ListOutcomeGoal");
            }
        }

        /// <summary>
        /// Retorna la página de Editar Logros Outcomes.
        /// </summary>
        /// <param name="OutcomeComisionId">ID del Outcome Comisión a Editar</param>
        /// <returns>View</returns>
        public ActionResult EditOutcomeGoal(Int32 OutcomeComisionId)
        {
            var viewModel = new EditOutcomeViewModel();
            viewModel.CargarDatos(CargarDatosContext(), OutcomeComisionId);
            return View(viewModel);
        }

        #endregion

        #region PartialView

        /// <summary>
        /// Retorna la vista parcial que contiene los logros de los Outcome
        /// para la comisión de WASC.
        /// </summary>
        /// <param name="OutcomeId">ID del Outcome Seleccionado</param>
        /// <param name="WascId">ID de la Comisión WASC</param>
        /// <returns>Partial View</returns>
        public ActionResult _ListOutcomeGoalPartialView(Int32? OutcomeId, Int32 WascId)
        {
            var viewModel = new ListOutcomeGoalPartialViewModel();
            viewModel.CargarDatos(CargarDatosContext(), OutcomeId, WascId);
            return PartialView("PartialView/_ListOutcomeGoalPartialView", viewModel);
        }

        #endregion

        #region Post

        /// <summary>
        /// Edita los Logros de un Outcome WASC, validando los datos correspondientes.
        /// </summary>
        /// <param name="ViewModel">ViewModel de un Outcome</param>
        /// <returns>View</returns>
        [HttpPost]
        public ActionResult EditOutcomeGoal(EditOutcomeViewModel ViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewModel.CargarDatos(CargarDatosContext(), ViewModel.OutcomeComisionId);
                    PostMessage(MessageType.Info);
                    return View(ViewModel);
                }

                using (var transaction = new TransactionScope())
                {
                    var lstLogros =
                        Context.LogroOutcomeComision.Where(x => x.IdOutcomeComision == ViewModel.OutcomeComisionId)
                            .ToList();

                    foreach (var item in lstLogros)
                    {
                        switch (item.Orden)
                        {
                            case 0:
                                item.Descripcion = ViewModel.LogroEsperado.Trim();
                                break;
                            case 1:
                                item.Descripcion = ViewModel.LogroSobresaliente.Trim();
                                break;
                            case 2:
                                item.Descripcion = ViewModel.LogroEjemplar.Trim();
                                break;
                        }
                    }
                    
                    Context.SaveChanges();
                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                return RedirectToAction("ListOutcomeGoal");
            }

            PostMessage(MessageType.Success);
            return RedirectToAction("ListOutcomeGoal");
        }

        #endregion
    }
}
