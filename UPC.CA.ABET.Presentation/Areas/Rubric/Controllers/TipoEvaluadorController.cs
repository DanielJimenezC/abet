﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class TipoEvaluadorController : BaseController
    {
        private AbetEntities db = new AbetEntities();

        // GET: Rubric/TipoEvaluador
        public ActionResult Index()
        {
            try
            {
                var tipoEvaluador = db.TipoEvaluador.Include(t => t.SubModalidadPeriodoAcademico).Where(x=>x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == this.PeriodoAcademicoId && x.TipoEvaluadorMaestra.IdEscuela == this.EscuelaId);                
                IEnumerable<TipoEvaluador> model = tipoEvaluador.ToList();

                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return View();               
            }
        }

        // GET: Rubric/TipoEvaluador/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluador tipoEvaluador = db.TipoEvaluador.Find(id);
            if (tipoEvaluador == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(tipoEvaluador);
        }

        // GET: Rubric/TipoEvaluador/Create
        public ActionResult Create()
        {
            var model = new TipoEvaluadorViewModel();
            model.CargarDatos(this.CargarDatosContext(), this.PeriodoAcademicoId, this.EscuelaId);
            return View(model);
            
        }

        [HttpPost]
        public ActionResult Create( TipoEvaluadorViewModel model)
        {
            /*VERIFICAR SI EXISTE OTRO REGISTRO ASIGNADO COMO COMITÉ*/

            var curso = Context.Curso.Where(x => x.Codigo == model.codCurso).FirstOrDefault();

            if (model.tipoEvaluador.Peso == 1) { 

                var rolExistenteCOMITE = Context.TipoEvaluador.FirstOrDefault(x => x.TipoEvaluadorMaestra.IdEscuela == this.EscuelaId &&
                x.IdCurso == curso.IdCurso && x.Peso == 1 && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico==this.PeriodoAcademicoId);

                if (rolExistenteCOMITE!=null)
                {
                    PostMessage(MessageType.Error, MessageResource.YaExisteUnRegistroConElRolDeComiteParaEvaluarLaRubricaEnElCursoSeleccionado);
                    return RedirectToAction("Create");

                }
            }

            /*VERIFICAR QUE SOLO EXISTA UN ROL REGISTRADO POR CURSO*/

            var rolExistentePorCurso  = Context.TipoEvaluador.Where(x => x.TipoEvaluadorMaestra.IdEscuela == this.EscuelaId &&
                x.IdCurso == curso.IdCurso && x.TipoEvaluadorMaestra.idTipoEvaluador == model.tipoEvaluador.idTipoEvaluadorMaestra && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico==PeriodoAcademicoId).FirstOrDefault();

            if(rolExistentePorCurso!=null)
            {
                PostMessage(MessageType.Error, MessageResource.YaExisteUnRegistroConElMismoCodigoDeRolParaElCursoSeleccionado);
                return RedirectToAction("Create");
            }

            int idpa = this.PeriodoAcademicoId.Value;
            int idSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idpa).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                                                

            model.tipoEvaluador.IdSubModalidadPeriodoAcademico= idSubModalidadPeriodoAcademico;
            model.tipoEvaluador.IdCurso = curso.IdCurso;
            db.TipoEvaluador.Add(model.tipoEvaluador);
            db.SaveChanges();
            PostMessage(MessageType.Success, MessageResource.ElRegistroFueRealizadoConExito);
            return RedirectToAction("Index");
            
        }
        
        /*
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluador tipoEvaluador = db.TipoEvaluador.Find(id);
            if (tipoEvaluador == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            var model = new TipoEvaluadorViewModel();
            model.tipoEvaluador = tipoEvaluador;
           
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TipoEvaluadorViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model.tipoEvaluador).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            return View(model.tipoEvaluador);
        }*/

        // GET: Rubric/TipoEvaluador/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {

                return RedirectToAction("NotFound", "Error");
            }
            TipoEvaluador tipoEvaluador = db.TipoEvaluador.Find(id);
            if (tipoEvaluador == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(tipoEvaluador);
        }

        // POST: Rubric/TipoEvaluador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                TipoEvaluador tipoEvaluador = db.TipoEvaluador.Find(id);
                db.TipoEvaluador.Remove(tipoEvaluador);
                db.SaveChanges();
                PostMessage(MessageType.Success, MessageResource.ElRegistroFueEliminadoConExito);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                PostMessage(MessageType.Error, MessageResource.ElRegistroNoSePuedeEliminarPorqueDependeDeOtrosRegistros);
                return RedirectToAction("Delete", new { id = id });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
