﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Rubric;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric;
using UPC.CA.ABET.Presentation.Areas.Rubric.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Controllers
{
    public class ConfigurarComentariosController : BaseController
    {
        // GET: Rubric/ConfigurarComentarios
        [AuthorizeUserAttribute(AppRol.CoordinadorCarrera)]
        public ActionResult IndexComentarios(ConfigCommentViewModel model)
        {
            var viewModel = new ConfigCommentViewModel();
            viewModel.CargarDatos(CargarDatosContext().context, model);
            return View(viewModel);
        }
    }
}