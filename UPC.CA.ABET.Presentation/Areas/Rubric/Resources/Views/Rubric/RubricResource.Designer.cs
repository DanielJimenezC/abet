﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RubricResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RubricResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric.RubricResource", typeof(RubricResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Rating assignment.
        /// </summary>
        public static string AsignacionDecalificacion {
            get {
                return ResourceManager.GetString("AsignacionDecalificacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a You must select a course to add the student.
        /// </summary>
        public static string DebeSeleccionarCursoParaAgregarAlumno {
            get {
                return ResourceManager.GetString("DebeSeleccionarCursoParaAgregarAlumno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Description.
        /// </summary>
        public static string Descripcion {
            get {
                return ResourceManager.GetString("Descripcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Failed to send data.
        /// </summary>
        public static string FalloEnvioDeDatos {
            get {
                return ResourceManager.GetString("FalloEnvioDeDatos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a The careers load failed.
        /// </summary>
        public static string FalloLaCargaDeCarreras {
            get {
                return ResourceManager.GetString("FalloLaCargaDeCarreras", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a There was a failure to perform the operation.
        /// </summary>
        public static string HuboFalloAlRealizarLaOperacion {
            get {
                return ResourceManager.GetString("HuboFalloAlRealizarLaOperacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Name.
        /// </summary>
        public static string Nombre {
            get {
                return ResourceManager.GetString("Nombre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a You forgot the description!.
        /// </summary>
        public static string OlvidasteLaDescripcion {
            get {
                return ResourceManager.GetString("OlvidasteLaDescripcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Top Score.
        /// </summary>
        public static string PuntajeMaximo {
            get {
                return ResourceManager.GetString("PuntajeMaximo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Major score.
        /// </summary>
        public static string PuntajeMayor {
            get {
                return ResourceManager.GetString("PuntajeMayor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Minor score.
        /// </summary>
        public static string PuntajeMenor {
            get {
                return ResourceManager.GetString("PuntajeMenor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Minimum score.
        /// </summary>
        public static string PuntajeMinimo {
            get {
                return ResourceManager.GetString("PuntajeMinimo", resourceCulture);
            }
        }
    }
}
