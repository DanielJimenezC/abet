﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Rubric {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RubricCourseResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RubricCourseResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.RubricCourse.RubricCourseResource", typeof(RubricCourseResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }                    
      
        public static string RubricByCourse
        {
            get {
                return ResourceManager.GetString("RubricByCourse", resourceCulture);
            }
        }


        public static string EditRubric
        {
            get
            {
                return ResourceManager.GetString("EditRubric", resourceCulture);
            }
        }

        public static string EvaluateRubric
        {
            get {
                return ResourceManager.GetString("EvaluateRubric", resourceCulture);
            }
        }

        public static string MaintenanceRubrics
        {
            get {
                return ResourceManager.GetString("MaintenanceRubrics", resourceCulture);
            }
        }
        
        public static string UploadRubricTemplate
        {
            get {
                return ResourceManager.GetString("UploadRubricTemplate", resourceCulture);
            }
        }

        public static string Date
        {
            get
            {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }
        public static string Options
        {
            get
            {
                return ResourceManager.GetString("Options", resourceCulture);
            }
        }

        public static string RubricList
        {
            get
            {
                return ResourceManager.GetString("RubricList", resourceCulture);
            }
        }

        public static string Delete
        {
            get
            {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }



        public static string BulkDownloadRubric
        {
            get {
                return ResourceManager.GetString("BulkDownloadRubric", resourceCulture);
            }
        }
    
        public static string EvaluateRubricDescription
        {
            get {
                return ResourceManager.GetString("EvaluateRubricDescription", resourceCulture);
            }
        }
        
        public static string MaintenanceRubricDescription
        {
            get {
                return ResourceManager.GetString("MaintenanceRubricDescription", resourceCulture);
            }
        }
        
        public static string UploadRubricTemplateDescription
        {
            get {
                return ResourceManager.GetString("UploadRubricTemplateDescription", resourceCulture);
            }
        }
     
        public static string BulkDownloadRubricDescription
        {
            get {
                return ResourceManager.GetString("BulkDownloadRubricDescription", resourceCulture);
            }
        }
     
        public static string See
        {
            get {
                return ResourceManager.GetString("See", resourceCulture);
            }
        }
                
        public static string Search
        {
            get {
                return ResourceManager.GetString("Search", resourceCulture);
            }
        }
        
        public static string Section
        {
            get {
                return ResourceManager.GetString("Section", resourceCulture);
            }
        }
         
        public static string Course
        {
            get {
                return ResourceManager.GetString("Course", resourceCulture);
            }
        }
         
        public static string Student
        {
            get {
                return ResourceManager.GetString("Student", resourceCulture);
            }
        }

         public static string StudentList
        {
            get {
                return ResourceManager.GetString("StudentList", resourceCulture);
            }
        }

        public static string Select
        {
            get {
                return ResourceManager.GetString("Select", resourceCulture);
            }
        }

        public static string Sede
        {
            get {
                return ResourceManager.GetString("Sede", resourceCulture);
            }
        }

        public static string Evalute
        {
            get {
                return ResourceManager.GetString("Evalute", resourceCulture);
            }
        }

        public static string AddStudent
        {
            get {
                return ResourceManager.GetString("AddStudent", resourceCulture);
            }
        }

        public static string RubricSetting
        {
            get
            {
                return ResourceManager.GetString("RubricSetting", resourceCulture);
            }
        }

        public static string RubricSettingDescription
        {
            get
            {
                return ResourceManager.GetString("RubricSettingDescription", resourceCulture);
            }
        }

        public static string CourseLoadingFailure
        {
            get
            {
                return ResourceManager.GetString("CourseLoadingFailure", resourceCulture);
            }
        }

        public static string SectionLoadingFailure
        {
            get
            {
                return ResourceManager.GetString("SectionLoadingFailure", resourceCulture);
            }
        }
        
        public static string RequiredCode
        {
            get
            {
                return ResourceManager.GetString("RequiredCode", resourceCulture);
            }
        }
        
        public static string RequiredStudentName
        {
            get
            {
                return ResourceManager.GetString("RequiredStudentName", resourceCulture);
            }
        }
        
        public static string RequiredStudentLastName
        {
            get
            {
                return ResourceManager.GetString("RequiredStudentLastName", resourceCulture);
            }
        }
        
        public static string RequiredCourse
        {
            get
            {
                return ResourceManager.GetString("RequiredCourse", resourceCulture);
            }
        }
        
        public static string RequiredSection
        {
            get
            {
                return ResourceManager.GetString("RequiredSection", resourceCulture);
            }
        }
        
        public static string RequiredCareer
        {
            get
            {
                return ResourceManager.GetString("RequiredCareer", resourceCulture);
            }
        } 

        public static string Code
        {
            get
            {
                return ResourceManager.GetString("Code", resourceCulture);
            }
        }

        public static string Career
        {
            get
            {
                return ResourceManager.GetString("Career", resourceCulture);
            }
        }
        
        public static string StudentLastName
        {
            get
            {
                return ResourceManager.GetString("StudentLastName", resourceCulture);
            }
        }

        
        public static string StudentName
        {
            get
            {
                return ResourceManager.GetString("StudentName", resourceCulture);
            }
        }

        public static string Evaluation
        {
            get
            {
                return ResourceManager.GetString("Evaluation", resourceCulture);
            }
        }

        public static string GeneralInformation
        {
            get
            {
                return ResourceManager.GetString("GeneralInformation", resourceCulture);
            }
        }

        public static string Teacher
        {
            get
            {
                return ResourceManager.GetString("Teacher", resourceCulture);
            }
        }

        public static string MaxScore
        {
            get
            {
                return ResourceManager.GetString("MaxScore", resourceCulture);
            }
        }

        public static string DownloadRubric 
        {
            get
            {
                return ResourceManager.GetString("DownloadRubric", resourceCulture);
            }
        }

        public static string Download
        {
            get 
            {
                return ResourceManager.GetString("Download", resourceCulture);
            }
        }
        public static string CycleAcademic
        {
            get
            {
                return ResourceManager.GetString("CycleAcademic", resourceCulture);
            }
        }

        public static string UploadRubric
        {
            get
            {
                return ResourceManager.GetString("UploadRubric", resourceCulture);
            }
        }

        public static string BulkUploadRubric
        {
            get
            {
                return ResourceManager.GetString("BulkUploadRubric", resourceCulture);
            }
        }

        public static string BulkUploadRubricDescription
        {
            get
            {
                return ResourceManager.GetString("BulkUploadRubricDescription", resourceCulture);
            }
        }
        
         public static string RubricTypeName
        {
            get
            {
                return ResourceManager.GetString("RubricTypeName", resourceCulture);
            }
        }
        
         public static string EvaluateRubricCapstone
        {
            get
            {
                return ResourceManager.GetString("EvaluateRubricCapstone", resourceCulture);
            }
        }        
        
         public static string CreateEvaluateRubricCapstone
        {
            get
            {
                return ResourceManager.GetString("CreateEvaluateRubricCapstone", resourceCulture);
            }
        }       
        
         public static string ProjectName
        {
            get
            {
                return ResourceManager.GetString("ProjectName", resourceCulture);
            }
        }      
        
         public static string Score
        {
            get
            {
                return ResourceManager.GetString("Score", resourceCulture);
            }
        }

        public static string ProjectList
        {
            get
            {
                return ResourceManager.GetString("ProjectList", resourceCulture);
            }
        }

        public static string AcademicProject
        {
            get
            {
                return ResourceManager.GetString("AcademicProject", resourceCulture);
            }
        }

        
        public static string Observation
        {
            get
            {
                return ResourceManager.GetString("Observation", resourceCulture);
            }
        }

        public static string DownloadNote
        {
            get
            {
                return ResourceManager.GetString("DownloadNote", resourceCulture);
            }
        }
        public static string DetailRubricControl

        {
            get
            {
                return ResourceManager.GetString("DetailRubricControl", resourceCulture);
            }
        }

        public static string CapstoneRubricFinalProcess
        {
            get
            {
                return ResourceManager.GetString("CapstoneRubricFinalProcess", resourceCulture);
            }
        }

        public static string CapstoneRubricFinalProcessDescription
        {
            get
            {
                return ResourceManager.GetString("CapstoneRubricFinalProcessDescription", resourceCulture);
            }
        }

        public static string EndProcess
        {
            get
            {
                return ResourceManager.GetString("EndProcess", resourceCulture);
            }
        }

    }
}
