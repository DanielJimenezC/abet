﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Score {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RubricQualifyResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RubricQualifyResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.Score.RubricQualifyResource" +
                            "", typeof(RubricQualifyResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code:.
        /// </summary>
        public static string AlumnoCodigo {
            get {
                return ResourceManager.GetString("AlumnoCodigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rate.
        /// </summary>
        public static string Calificar {
            get {
                return ResourceManager.GetString("Calificar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Career.
        /// </summary>
        public static string Carrera {
            get {
                return ResourceManager.GetString("Carrera", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code.
        /// </summary>
        public static string Codigo {
            get {
                return ResourceManager.GetString("Codigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Criterion.
        /// </summary>
        public static string Criterios {
            get {
                return ResourceManager.GetString("Criterios", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Course.
        /// </summary>
        public static string Curso {
            get {
                return ResourceManager.GetString("Curso", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string Descripcion {
            get {
                return ResourceManager.GetString("Descripcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string DescripcionNivel {
            get {
                return ResourceManager.GetString("DescripcionNivel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Assessment.
        /// </summary>
        public static string Evaluacion {
            get {
                return ResourceManager.GetString("Evaluacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter Grade.
        /// </summary>
        public static string IngresarNotaCriterio {
            get {
                return ResourceManager.GetString("IngresarNotaCriterio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to save this grades? Once it is saved it can&apos;t be modified..
        /// </summary>
        public static string MensajeConfirmacion {
            get {
                return ResourceManager.GetString("MensajeConfirmacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string NombreNivel {
            get {
                return ResourceManager.GetString("NombreNivel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grade:.
        /// </summary>
        public static string NotaAlumno {
            get {
                return ResourceManager.GetString("NotaAlumno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grade.
        /// </summary>
        public static string NotaMayorCriterio {
            get {
                return ResourceManager.GetString("NotaMayorCriterio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Higher Note.
        /// </summary>
        public static string NotaMayorNivel {
            get {
                return ResourceManager.GetString("NotaMayorNivel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lower Note.
        /// </summary>
        public static string NotaMenorNivel {
            get {
                return ResourceManager.GetString("NotaMenorNivel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not official grades.
        /// </summary>
        public static string NotasNoOficiales {
            get {
                return ResourceManager.GetString("NotasNoOficiales", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Observation about the student.
        /// </summary>
        public static string ObservacionAlumno {
            get {
                return ResourceManager.GetString("ObservacionAlumno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Get Grades.
        /// </summary>
        public static string ObtenerNotas {
            get {
                return ResourceManager.GetString("ObtenerNotas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confimation.
        /// </summary>
        public static string TituloMensajeConf {
            get {
                return ResourceManager.GetString("TituloMensajeConf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project rating.
        /// </summary>
        public static string TituloPagina {
            get {
                return ResourceManager.GetString("TituloPagina", resourceCulture);
            }
        }
    }
}
