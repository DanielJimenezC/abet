﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.BulkInsert {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UploadFilesResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UploadFilesResource() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.BulkInsert.UploadFilesResou" +
                            "rce", typeof(UploadFilesResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Download Template.
        /// </summary>
        public static string DescargarPlantilla {
            get {
                return ResourceManager.GetString("DescargarPlantilla", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Choose File.
        /// </summary>
        public static string ElegirArchivo {
            get {
                return ResourceManager.GetString("ElegirArchivo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Check the Headers.
        /// </summary>
        public static string HeadersInvalidos {
            get {
                return ResourceManager.GetString("HeadersInvalidos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Upload File.
        /// </summary>
        public static string SubirArchivo {
            get {
                return ResourceManager.GetString("SubirArchivo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Choose a Excel File.
        /// </summary>
        public static string TipoArchivo {
            get {
                return ResourceManager.GetString("TipoArchivo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Virtual Company Bulk Insert.
        /// </summary>
        public static string TituloEmpresa {
            get {
                return ResourceManager.GetString("TituloEmpresa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assessment Bulk Insert.
        /// </summary>
        public static string TituloEvaluacion {
            get {
                return ResourceManager.GetString("TituloEvaluacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assessor Bulk Insert.
        /// </summary>
        public static string TituloEvaluador {
            get {
                return ResourceManager.GetString("TituloEvaluador", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a WASC Outcome Goal Bulk Insert.
        /// </summary>
        public static string TituloOutcomeComision {
            get {
                return ResourceManager.GetString("TituloOutcomeComision", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Academic Project Bulk Insert.
        /// </summary>
        public static string TituloProyectoAcademico {
            get {
                return ResourceManager.GetString("TituloProyectoAcademico", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assessment Type Bulk Insert.
        /// </summary>
        public static string TituloTipoEvaluacion {
            get {
                return ResourceManager.GetString("TituloTipoEvaluacion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Assessment Type Bulk Insert.
        /// </summary>
        public static string TituloTipoEvaluador {
            get {
                return ResourceManager.GetString("TituloTipoEvaluador", resourceCulture);
            }
        }
    }
}
