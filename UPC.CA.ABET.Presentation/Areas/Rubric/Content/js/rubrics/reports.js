﻿

function cargarG1(dataFiltro)
{
    var dataLength = dataFiltro.result.length;
    var categoriesTemp = [];
    var cursosTemp = [];

    for (var i = 0; i < dataLength; i++)
    {
        categoriesTemp.push(dataFiltro.result[i].Empresa); 
        cursosTemp.push(dataFiltro.result[i].Curso); 
    }
    var categories = [];
    $.each(categoriesTemp, function (i, el) {
        if ($.inArray(el, categories) === -1) categories.push(el);
    });
    var cursos = [];
    $.each(cursosTemp, function (i, el) {
        if ($.inArray(el, cursos) === -1) cursos.push(el);
    });

    var series = [];
    for (i = 0; i < cursos.length; i++) {
        var element = {name:'',data:[]};
        element.name = cursos[i];
        var val = [];
        for (var j = 0; j < categories.length; j++)
            val.push(0);
        for (var j = 0; j < dataLength; j++)
        {
            var cont = 0;
            for(var k = 0;k < categories.length;k++)
            {
                if (categories[k] == dataFiltro.result[j].Empresa && cursos[i] == dataFiltro.result[j].Curso)
                    val[k]++;
            }
        }
        element.data = val;
        series.push(element);
    }

    $('#grafico1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: t1
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: totalP
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: series
    });
}
function cargarG2(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var cursosTemp = [];

    for (var i = 0; i < dataLength; i++) {
        cursosTemp.push(dataFiltro.result[i].Curso);
    }
    var cursos = [];
    $.each(cursosTemp, function (i, el) {
        if ($.inArray(el, cursos) === -1) cursos.push(el);
    });

    var data = [];
    for (var i = 0; i < cursos.length; i++) {
        var curso = { name: '', y: 0, drilldown: '' };
        curso.name = cursos[i];
        curso.drilldown = cursos[i];
        var count = 0;
        for (var j = 0; j < dataLength; j++) {
            if (dataFiltro.result[j].Curso == cursos[i])
                count++;
        }
        curso.y = count / dataLength * 100;
        data.push(curso);
    }

    $('#grafico2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: t2
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: PorcentajeP
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}%'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> ' + del + ' total<br/>'
        },
        series: [{
            name: Curso,
            colorByPoint: true,
            data: data
        }]
    });
}
/*
function cargarG3(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var cursosTemp = [];

    for (var i = 0; i < dataLength; i++) {
        cursosTemp.push(dataFiltro.result[i].Curso);
    }
    var cursos = [];
    $.each(cursosTemp, function (i, el) {
        if ($.inArray(el, cursos) === -1) cursos.push(el);
    });

    var data = [];
    for (var i = 0; i < cursos.length; i++) {
        var curso = { name: '', y: 0, drilldown: '' };
        curso.name = cursos[i];
        curso.drilldown = cursos[i];
        var count = 0;
        var suma = 0;
        for (var j = 0; j < dataLength; j++) {
            if (dataFiltro.result[j].Curso == cursos[i]) {
                count++;
                suma += Number(dataFiltro.result[j].Nota);
            }
        }
        curso.y = suma / count;
        data.push(curso);
    }
    
    $('#grafico3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Promedio de notas por curso'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Promedio de notas'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> de promedio en el curso<br/>'
        },
        series: [{
            name: 'Curso',
            colorByPoint: true,
            data: data
        }]
    });
}
function cargarG4(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var ciclosTemp = [];

    for (var i = 0; i < dataLength; i++) {
        ciclosTemp.push(dataFiltro.result[i].Ciclo);
    }
    var ciclos = [];
    $.each(ciclosTemp, function (i, el) {
        if ($.inArray(el, ciclos) === -1) ciclos.push(el);
    });

    var data = [];
    for (var i = 0; i < ciclos.length; i++) {
        var ciclo = { name: '', y: 0, drilldown: '' };
        ciclo.name = ciclos[i];
        ciclo.drilldown = ciclos[i];
        var count = 0;
        for (var j = 0; j < dataLength; j++) {
            if (dataFiltro.result[j].Ciclo == ciclos[i]) {
                if( dataFiltro.result[j].Nota >= 13)
                    count++;
            }
        }
        ciclo.y = count;
        data.push(ciclo);
    }
    
    $('#grafico4').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Cantidad de aprobados por ciclo'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Frecuencia de proyectos aprobados'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de proyectos con nota aprobatoria en el ciclo<br/>'
        },
        series: [{
            name: 'Curso',
            colorByPoint: true,
            data: data
        }]
    });
}
*/
function cargarG5(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var categoriesTemp = [];
    var estadosTemp = [];

    for (var i = 0; i < dataLength; i++) {
        categoriesTemp.push(dataFiltro.result[i].Empresa);
        estadosTemp.push(dataFiltro.result[i].NombreEstado);
    }
    var categories = [];
    $.each(categoriesTemp, function (i, el) {
        if ($.inArray(el, categories) === -1) categories.push(el);
    });
    var estados = [];
    $.each(estadosTemp, function (i, el) {
        if ($.inArray(el, estados) === -1) estados.push(el);
    });

    var series = [];
    for (var i = 0; i < estados.length; i++) {
        var estado = { name: '', data: [] };
        estado.name = estados[i];
        estado.data = [];
        for (var j = 0; j < categories.length; j++) {
            var count = 0;
            for (var k = 0; k < dataFiltro.result.length;k++) {
                if (dataFiltro.result[k].NombreEstado == estados[i] && dataFiltro.result[k].Empresa == categories[j]) {
                    count++;
                }
            }
            estado.data.push(count);
        }
        series.push(estado);
    }

    $('#grafico5').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: t5
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: PTP
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: series
    });
}
function cargarG6(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var estadosTemp = [];

    for (var i = 0; i < dataLength; i++) {
        console.log(dataFiltro.result[i].NombreEstado);
        estadosTemp.push(dataFiltro.result[i].NombreEstado);
    }
    var estados = [];
    $.each(estadosTemp, function (i, el) {
        if ($.inArray(el, estados) === -1) estados.push(el);
    });

    var data = [];
    for (var i = 0; i < estados.length; i++) {
        var estado = { name: '', y: 0.0};
        estado.name = estados[i];
        var count = 0;
        for (var k = 0; k < dataFiltro.result.length; k++) {
            if (dataFiltro.result[k].NombreEstado == estados[i]) {
                count++;
            }
        }
        estado.y = count * 100 / dataFiltro.result.length;
        data.push(estado);
    }

    $('#grafico6').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: t6
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: data
        }]
    });
}
/*
function cargarG7(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var categoriesTemp = [];

    for (var i = 0; i < dataLength; i++) {
        categoriesTemp.push(dataFiltro.result[i].Empresa);
    }
    var categories = [];
    $.each(categoriesTemp, function (i, el) {
        if ($.inArray(el, categories) === -1) categories.push(el);
    });

    var data = [];
    var series = [];
    for (var i = 0; i < categories.length; i++) {
        var serie = { name: categories[i], id: categories[i] };
        series.push(serie);
        var column = { name: categories[i], y: 50.00, drilldown: categories[i] };
        var suma = 0;
        var total = 0;
        for (var k = 0; k < dataFiltro.result.length; k++) {
            if (dataFiltro.result[k].Empresa == categories[i]) {
                total++;
                suma += Number(dataFiltro.result[k].Nota);
            }
        }
        column.y = suma / total;
        data.push(column);
    }
                
    $('#grafico7').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Distribución porcentual de promedio de proyectos según empresa'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Porcentajes'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: 'Empresa',
                colorByPoint: true,
                data: data
            }],
            drilldown: {
                series: series
            }
        });
}
function cargarG8(dataFiltro) {
    var dataLength = dataFiltro.result.length;
    var categoriesTemp = [];

    for (var i = 0; i < dataLength; i++) {
        categoriesTemp.push(dataFiltro.result[i].Empresa);
    }
    var categories = [];
    $.each(categoriesTemp, function (i, el) {
        if ($.inArray(el, categories) === -1) categories.push(el);
    });

    var data = [];
    var series = [];
    for (var i = 0; i < categories.length; i++) {
        var serie = { name: categories[i], id: categories[i] };
        series.push(serie);
        var column = { name: categories[i], y: 0.00, drilldown: categories[i] };
        var cont = 0;
        var total = 0;
        for (var k = 0; k < dataFiltro.result.length; k++) {
            if (dataFiltro.result[k].Empresa == categories[i]) {
                total++;
                if (dataFiltro.result[k].Nota >= 13)
                    cont++;
            }
        }
        column.y = cont * 100 / total;
        data.push(column);
    }

    $('#grafico8').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Distribución porcentual de proyectos aprobados por empresa'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Porcentajes'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Empresa',
            colorByPoint: true,
            data: data
        }],
        drilldown: {
            series: series
        }
    });
}
*/
function actualizarReporte() {

    console.log('actualizarReporte');

    var empresa = $("#IdEmpresa").val();
    var curso = $("#IdCurso").val();
    var ciclo = $("#IdPeriodoAcademico").val();
    var estado = $("#codEstado").val();


    var data = JSON.stringify({
        IdEmpresa: empresa,
        IdCurso: curso,
        IdPeriodoAcademico: ciclo,
        codEstado: estado
    });


    $.ajax({
        url: customRoutes.actualizarReporteG1,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
            showLoading();
        },
        complete: function (jqXHR, textStatus) {
            closeLoading();
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG1(dataFiltro);
                cargarG2(dataFiltro);
                cargarG5(dataFiltro);
                cargarG6(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(error);
        }
    });
    /*
    $.ajax({
        url: customRoutes.actualizarReporteG2,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG2(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    $.ajax({
        url: customRoutes.actualizarReporteG3,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG3(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    $.ajax({
        url: customRoutes.actualizarReporteG4,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG4(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    $.ajax({
        url: customRoutes.actualizarReporteG5,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG5(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    
    $.ajax({
        url: customRoutes.actualizarReporteG6,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG6(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    $.ajax({
        url: customRoutes.actualizarReporteG7,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG7(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    
    $.ajax({
        url: customRoutes.actualizarReporteG8,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function (jqXHR, settings) {
        },
        complete: function (jqXHR, textStatus) {
        },
        success: function (dataFiltro, textStatus, jqXHR) {
            if (dataFiltro.result != null) {
                cargarG8(dataFiltro);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });*/
}

window.onload = function () {

    /*
    var b = document.getElementById("selectCiclo");
    var c = b.options[0].value;
    var a = document.getElementById("select2-selectCiclo-container");

    selectCiclo.options[0].selected = false;
    for (var i = 0; i < b.options.length; i++) {
        if (b.options[i].text == c)
            selectCiclo.options[i].selected = true;
    }

    a.title = c;
    a.innerHTML = c;
     */
    actualizarReporte();
}