﻿function validOnlyText(event) {

    var regex = new RegExp("^[a-zA-ZÑñáéíóúÁÉÍÓÚ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}
function validPhoneHome(event) {
    var regex = new RegExp("^[0-9()]*$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function validAlphanumeric(event) {

    var regex = new RegExp("^[a-zA-ZÑñáéíóúÁÉÍÓÚ0-9_]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}
function validAlphanumericAndSpace(event) {

    var regex = new RegExp("^[a-zA-ZÑñáéíóúÁÉÍÓÚ0-9 ]*$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}
function validOnlyNumber(event) {

    var regex = new RegExp("^[0-9]*$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function validOnlyTextAndSpace(event) {
    var regex = new RegExp("[A-Za-zÑñ áéíóúÁÉÍÓÚ]");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}
/*
$(".alert-success").fadeTo(3300, 800).slideUp(500, function () {
    $(".alert-success").slideUp(500);
});
$(".alert-danger").fadeTo(3300, 800).slideUp(500, function () {
    $(".alert-danger").slideUp(500);
});*/