﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Rubric.Dto
{
    public class ProyectoAcademicoDto
    {
        public int IdProyectoAcademico { get; set; }
        public string Nombre { get; set; }
        public string CursoNombreEspanol { get; set; }
        public string CursoNombreIngles { get; set; }
        public int IdCurso { get; set; }
        public bool Register { get; set; }

        public bool Edit { get; set; }
        public string Alumnos { get; set; }
        
    }
}