﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class ReportViewModel
    {
        public IEnumerable<SelectListItem> Terms { get; set; }
        public string GetReportUrl { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
        public string Plugin { get; set; }
    }
}