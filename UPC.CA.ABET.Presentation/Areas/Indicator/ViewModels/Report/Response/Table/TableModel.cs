﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class TableViewModel
    {
        public string Title { get; set; }
        public List<string> Header { get; set; }
        public List<List<string>> Body { get; set; }
        public List<string> Footer { get; set; }

        public TableViewModel()
        {
            Header = new List<string>();
            Body = new List<List<string>>();
            Footer = new List<string>();
        }
    }
}