﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class C3ChartModel
    {
        public string Title { get; set; }
        public List<List<string>> Columns { get; set; }
        public List<string> Categories { get; set; }
        public string Type { get; set; }

        public C3ChartModel()
        {
            Columns = new List<List<string>>();
            Categories = new List<string>();
        }
    }
}