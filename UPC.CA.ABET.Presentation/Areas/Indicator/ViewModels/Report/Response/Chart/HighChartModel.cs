﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class HighChartModel
    {
        public Options chart { get; set; }
        public Title title { get; set; }
        public SubTitle subtitle { get; set; }
        public XAxis xAxis { get; set; }
        public YAxis yAxis { get; set; }
        public Tooltip tooltip { get; set; }
        public PlotOptions plotOptions { get; set; }
        public List<Serie> series { get; set; }
        public Credits credits { get; set; }

        public HighChartModel()
        {
            chart = new Options();
            title = new Title();
            subtitle = new SubTitle();
            xAxis = new XAxis();
            yAxis = new YAxis();
            tooltip = new Tooltip();
            plotOptions = new PlotOptions();
            series = new List<Serie>();
            credits = new Credits();
        }
    }

    public class Options
    {
        public string type { get; set; }
        public Style style { get; set; }

        public Options()
        {
            style = new Style();
        }
    }

    public class Title
    {
        public string text { get; set; }
    }

    public class SubTitle
    {
        public string text { get; set; }
    }

    public class XAxis
    {
        public List<string> categories { get; set; }
        public bool crosshair { get; set; }
        public string format { get; set; }
        public XAxis()
        {
            categories = new List<string>();
        }
    }

    public class YAxis
    {
        public double min { get; set; }
        public Title title { get; set; }

        public YAxis()
        {
            title = new Title();
        }
    }

    public class Tooltip
    {
        public string headerFormat { get; set; }
        public string pointFormat { get; set; }
        public string footerFormat { get; set; }
        public bool shared { get; set; }
        public bool useHTML { get; set; }
    }

    public class PlotOptions
    {
        public PlotOption line { get; set; }
        public PlotOption column { get; set; }
        public PlotOption pie { get; set; }

        public PlotOptions()
        {
            line = new PlotOption();
            column = new PlotOption();
            pie = new PlotOption();
        }
    }

    public class PlotOption
    {
        public double pointPadding { get; set; }
        public double borderWidth { get; set; }
        public DataLabels dataLabels { get; set; }
        public string stacking { get; set; }

        public PlotOption()
        {
            dataLabels = new DataLabels();
        }
    }

    public class Serie
    {
        public string name { get; set; }
        public List<Data> data { get; set; }
        public DataLabels dataLabels { get; set; }

        public Serie()
        {
            data = new List<Data>();
            dataLabels = new DataLabels();
        }
    }

    public class Data
    {
        public string name { get; set; }
        public double? y { get; set; }
    }

    public class DataLabels
    {
        public bool enabled { get; set; }
        public string color { get; set; }
        public string format { get; set; }
        public Style style { get; set; }
        public DataLabels()
        {
            enabled = true;
            style = new Style();
        }
    }

    public class Style
    {
        public string fontSize { get; set; }
        public string fontFamily { get; set; }
    }
    
    public class Credits
    {
        public bool enabled { get; set; }
    }
}