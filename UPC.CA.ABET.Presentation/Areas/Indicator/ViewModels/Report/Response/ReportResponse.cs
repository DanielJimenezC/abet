﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class ReportResponse<T>
    {
        public List<TableViewModel> Tables { get; set; }
        public List<T> Charts { get; set; }

        public ReportResponse()
        {
            Tables = new List<TableViewModel>();
            Charts = new List<T>();
        }
    }
}