﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class ReportRequest
    {
        public string Type { get; set; }
        public string Plugin { get; set; }
        public int TermId { get; set; }
    }
}