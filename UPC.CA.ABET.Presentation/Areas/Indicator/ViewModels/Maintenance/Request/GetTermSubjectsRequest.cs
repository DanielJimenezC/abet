﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class GetTermSubjectsRequest
    {
        public int AreaId { get; set; }
        public string FieldToUpdate { get; set; }
    }
}