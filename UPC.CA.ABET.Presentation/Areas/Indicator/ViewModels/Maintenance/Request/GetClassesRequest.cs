﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{ 
    public class GetClassesRequest
    {
        public int CursoPeriodoAcademicoId { get; set; }
        public int AreaId { get; set; }
    }
}