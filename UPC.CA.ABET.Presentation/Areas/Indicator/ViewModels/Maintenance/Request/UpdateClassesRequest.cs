﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{

    public class UpdateClassesRequest
    {
        public List<UpdateClassViewModel> Classes { get; set; }
    }

    public class UpdateClassViewModel
    {
        public int Id { get; set; }
        public bool InTimeSchedule { get; set; }
    }
}