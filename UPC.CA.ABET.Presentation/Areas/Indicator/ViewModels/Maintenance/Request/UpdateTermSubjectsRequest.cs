﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class UpdateTermSubjectsRequest
    {
        public List<UpdateTermSubjectViewModel> TermSubjects { get; set; }
    }

    public class UpdateTermSubjectViewModel
    {
        public int Id { get; set; }
        public bool FieldValue { get; set; }
        public string FieldName { get; set; }
    }
}