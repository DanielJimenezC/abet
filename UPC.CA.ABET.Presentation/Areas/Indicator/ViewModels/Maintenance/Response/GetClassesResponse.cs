﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class GetClassesResponse
    {
        public List<GetClassViewModel> Classes { get; set; }
    }

    public class GetClassViewModel
    {
        public int IdSeccion { get; set; }
        public string CodigoDeCurso { get; set; }
        public string NombreDeCurso { get; set; }
        public string Seccion { get; set; }
        public string Area { get; set; }
        public bool aTiempo { get; set; }
    }
}