﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class GetTermSubjectsResponse
    {
        public List<TermSubjectViewModel> TermSubjects { get; set; }
    }

    public class TermSubjectViewModel
    {
        public string CodigoCurso { get; set; }
        public string NombreCurso { get; set; }
        public string Area { get; set; }
        public int IdCursoPeriodoAcademico { get; set; }
        public bool ValorCheckbox { get; set; }
        public string CampoAActualizar { get; set; }
    }
}