﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class HiredProfessorsResponse
    {
        public int aTiempo { get; set; }
        public int aDestiempo { get; set; }
        public int ProfessorsCount { get; set; }
        public HiredProfessorsResponse()
        {
            aTiempo = 0;
            aDestiempo = 0;
            ProfessorsCount = 0;
        }
     }
}