﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels
{
    public class MaintenanceViewModel
    {
        public string GetAreasUrl { get; set; }
        public string GetClassUrl { get; set; }
        public string GetCursosPeriodoAcademicoUrl { get; set; }
        public string GetCursosUrl { get; set; }
        public string GetGuardarUrl { get; set; }
        public string GetGuardarIndicadorDocenteUrl { get; set; }
        public string GetCargarIndicadorDocenteUrl { get; set; }
        public IEnumerable<SelectListItem> Terms { get; set; }
        public IEnumerable<SelectListItem> Career { get; set; }
        public IEnumerable<SelectListItem> Modules { get; set; }
        public string FieldToUpdate { get; set; }
        public string ModalidadId { get; set; }

        //File to upload : Profesores Horas programadas
        public HttpPostedFileBase Archivo { get; set; }
    }
}