﻿$(document).ready(function () {
    $('#btn-guardar').fadeOut('fast');
});

$(document).on('change', '#termDropDown', function (e) {

    $('#btn-guardar').fadeOut('fast');
    $('#success-msg').fadeOut('fast');

    var termId = $(this).val();
    lockComponentsWhenNewTermIsSelected(termId);

    if (termId == 0) {

        return;
    }

    hideSuccessMessage();

    var JSONObject = { 'termId': termId }
    var getAreasUrl = $('#GetAreas').val();



    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getAreasUrl,
        success: function (json) {
            var areas = json.areas
            $.each(areas, function (index, element) {
                $('#areaDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
            });
        }
    });
});

function lockComponentsWhenNewTermIsSelected(termId) {
    $('#areaDropDown').empty();
    $('#classDropDown').empty();
    $('#areaDropDown').prop('disabled', termId == 0);
    $('#classDropDown').prop('disabled', true);
    $('#btnAsociate').prop('disabled', true);
}

$(document).on('change', '#areaDropDown', function (e) {

    $('#success-msg').fadeOut('fast');
    $('#btn-guardar').fadeIn('fast');
    $('#contenido-tabla').empty();

    var termId = $('#termDropDown').val();
    var areaId = $(this).val();
    var fieldToUpdate = $('#FieldToUpdate').val();

    var JSONObject = { 'request': { 'AreaId': areaId, 'FieldToUpdate': fieldToUpdate } }
    var getCursosUrl = $('#GetCursosPeriodoAcademico').val();

    $('#classDropDown').empty();
    $('#classDropDown').prop('disabled', areaId == 0);

    // Nay
    $('#btnAsociate').prop('disabled', true);

    if (termId == 0 || areaId == 0) {
        return;
    }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getCursosUrl,
        success: function (json) {
            $.each(json.TermSubjects, function (index, element) {
                $('#contenido-tabla').append(' <tr><td style="width: 15%;">'
                                             + element.CodigoCurso +
                                             '</td><td style="width: 30%;">'
                                             + element.NombreCurso +
                                             '</td><td style="width: 30%;">'
                                             + element.Area +
                                            '</td><td class="text-center"><div><input type="checkbox" name="' + element.IdCursoPeriodoAcademico + '"' + getCheckValue(element) + ' class="aTiempo" /></div></td></tr>')
            });
        }
    });
});
function getCheckValue(element) {

    if (element.ValorCheckbox)
        return checkBox = 'checked="checked"';
    else
        return checkBox = '';
}

$('#btn-guardar').click(function () {
    $('#success-msg').fadeOut('fast');
    var getGuardarUrl = $('#GetGuardar').val();

    values = jQuery("#tabla-form input[type=checkbox]:checked").map(
        function () {
            return { "Id": this.name, "FieldValue": true, "FieldName": $('#FieldToUpdate').val() }
        }).get()

    values = values.concat(
            jQuery('#tabla-form input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "Id": this.name, "FieldValue": false, "FieldName": $('#FieldToUpdate').val() }
                    }).get()
    );

    var json = {"TermSubjects": values}
    
    $.ajax({
        type: 'POST',
        data: JSON.stringify(json),
        contentType: 'application/json',
        url: getGuardarUrl,
        success: function (json) {
            $('#success-msg').fadeIn('fast');
        }
    });
})

function showSuccessMessage() {
    $('#success-msg').fadeIn('fast');
}

function hideSuccessMessage() {
    $('#success-msg').fadeOut('fast');
}
