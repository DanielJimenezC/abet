﻿$(document).on('change', '#termDropDown', function (e) {

    $('#btn-guardar').fadeOut('fast');
    $('#success-msg').fadeOut('fast');
    var termId = $(this).val();
    lockComponentsWhenNewTermIsSelected(termId);

    if (termId == 0) {

        return;
    }

    hideSuccessMessage();

    var JSONObject = { 'termId': termId }
    var getAreasUrl = $('#GetAreas').val();

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getAreasUrl,
        success: function (json) {
            var areas = json.areas
            $.each(areas, function (index, element) {
                $('#areaDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
            });
        }
    });
});

function lockComponentsWhenNewTermIsSelected(termId) {
    $('#areaDropDown').empty();
    $('#classDropDown').empty();
    $('#areaDropDown').prop('disabled', termId == 0);
    $('#classDropDown').prop('disabled', true);
    $('#btnAsociate').prop('disabled', true);
}

$(document).on('change', '#areaDropDown', function (e) {

    $('#btn-guardar').fadeOut('fast');
    var termId = $('#termDropDown').val();
    var areaId = $(this).val();
    var JSONObject = { 'areaId': areaId }
    var getCursosUrl = $('#GetCursos').val();

    $('#classDropDown').empty();
    $('#classDropDown').prop('disabled', areaId == 0);

    // Nay
    $('#btnAsociate').prop('disabled', true);
    $('#success-msg').fadeOut('fast');

    if (termId == 0 || areaId == 0) {
        return;
    }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getCursosUrl,
        success: function (json) {
            $.each(json.cursos, function (index, element) {
                $('#classDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
            });
        }
    });
});

$(document).on('change', '#classDropDown', function (e) {

    
    var termId = $('#termDropDown').val();
    var areaId = $('#areaDropDown').val();
    var classId = $(this).val();
    var JSONObject = { 'termId': termId, 'areaId': areaId, 'cursoPeriodoAcademicoId': classId }
    var getSeccionesUrl = $('#GetClasses').val();

    $('#btnAsociate').prop('disabled', true);
    $('#contenido-tabla').empty();
    $('#btn-guardar').fadeIn('fast');
    $('#success-msg').fadeOut('fast');


    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getSeccionesUrl,
        success: function (json) {
            console.log(json);
            $.each(json.Classes, function (index, element) {
                $('#contenido-tabla').append(' <tr><td style="width: 15%;">'
                                             + element.CodigoDeCurso +
                                             '</td><td style="width: 25%;">'
                                             + element.NombreDeCurso +
                                             '</td><td style="width: 15%;">'
                                             + element.Seccion +
                                             '</td><td>'
                                             + element.Area +
                                            '</td><td class="text-center"><div><input type="checkbox" name="' + element.IdSeccion + '"' + getCheckValue(element) + ' /></div></td></tr>')
            });
        }
    });
});

function getCheckValue(element) {

    if (element.aTiempo)
        return checkBox = 'checked="checked"';
    else
        return checkBox = '';
}

$('#btn-guardar').click(function () {
    $('#success-msg').fadeOut('fast');
    var getGuardarUrl = $('#GetGuardar').val();

    values = jQuery("#tabla-form input[type=checkbox]:checked").map(
        function () {
            return { "Id": this.name, "Value": "true" }
        }).get()
    values = values.concat(
            jQuery('#tabla-form input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "Id": this.name, "Value": "false" }
                    }).get()
    );

    var json = {"Classes" : values};

    $.ajax({
        type: 'POST',
        data: JSON.stringify(json),
        contentType: 'application/json',
        url: getGuardarUrl,
        success: function (json) {
            $('#success-msg').fadeIn('fast');
        }
    });
})

function showSuccessMessage() {
    $('#success-msg').fadeIn('fast');
}

function hideSuccessMessage() {
    $('#success-msg').fadeOut('fast');
}