﻿$(document).on('change', '#termDropDown', function (e) {
    var termId = $(this).val();
    if (termId == 0) { return; }
    getReport();
});

function getReport() {
    var termId = $('#termDropDown').val();
    var type = $('#type').val();
    var plugin = $('#plugin').val();
    var getReportUrl = $('#getReportUrl').val();
    var JSONObject = {
        "termId": termId,
        "type": type,
        "plugin": plugin,
    }
    hideChartsPanel();
    hideTablesPanel();
    showSpinner();
    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getReportUrl,
        success: function (json) {
            console.log(json);
            hideSpinner();
            drawTables(json.Tables);
            drawCharts(json.Charts);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideSpinner();
            hideChartsPanel();
            hideTablesPanel();
        }
    });
}

function drawTables(json) {
    $('#tables').empty();
    if (json.length == 0) {
        hideTablesPanel();
        return;
    }
    for (var i = 0; i < json.length; i++) {
        var chartId = 'table' + i;
        drawTable(chartId, json[i]);
    }
    showTablesPanel();
}

function drawTable(tableId, json) {

    var table = '';
    table += '<div style="margin-top: 20px;"></div>';
    table += '<div class="row">';
    table += '<div class="col-xs-10">';
    if (json.Title != null) {
        table += '<h4>' + json.Title + '</h4>';
    }
    table += '</div>'
    table += '<div class="col-xs-2">';
    table += '<a href="#" class="btn btn-primary btn-xs pull-right" id="test" onClick="exportTable(\'' + tableId + '\');"><i class="fa fa-download"/></a>';
    table += '</div>'
    table += '</div>'

    table += '<div class="table-responsive">'
    table += '<table id="' + tableId + '" class="table table-condensed table-bordered table-hover">';
    table += '<thead>';
    for (var i = 0; i < json.Header.length; i++) {
        table += '<th>' + json.Header[i] + '</th>';
    }
    table += '</thead>';

    table += '<tbody>';
    for (var i = 0; i < json.Body.length; i++) {
        table += '<tr>';
        for (var j = 0; j < json.Body[i].length; j++) {
            table += '<td>' + json.Body[i][j] + '</td>';
        }
        table += '</tr>';
    }
    table += '</tbody>';

    table += '<tfoot>'
    for (var i = 0; i < json.Footer.length; i++) {
        table += '<td>' + json.Footer[i] + '</td>';
    }
    table += '</tfoot>';
    table += '</table>';
    table += '</div>';
    
    $('#tables').append(table);
}

function drawCharts(json) {
    $('#charts').empty();
    if (json.length == 0) {
        hideChartsPanel();
        return;
    }
    showChartsPanel();
    for (var i = 0; i < json.length; i++) {
        var chartId = 'chart-' + i;
        var div = '<div style="margin-top: 20px;"></div>';
        if (json[i].Title != null) {
            div += '<h4>' + json[i].Title + '</h4>';
        }
        div +='<div id=' + chartId + '></div>'
        $('#charts').append(div);
        drawHighChart(json[i], chartId);
    }
}

function drawC3Chart(json, chartId) {
    var id = '#' + chartId;
    var chart = c3.generate({
        bindto: id,
        data: {
            columns: json.Columns,
            type: json.Type,
            labels: true
        },
        axis: {
            x: {
                type: 'category',
                categories: json.Categories
            }
        }
    })
}
function drawHighChart(json, chartId) {
    var id = '#' + chartId;
    json.tooltip = {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}:&nbsp</td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr> ',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    }
    $(id).highcharts(json);
}

function showSpinner() {
    $('#spinner-div').fadeIn();
}

function hideSpinner() {
    $('#spinner-div').fadeOut();
}

function showTablesPanel() {
    $('#tables-panel').fadeIn();
}

function hideTablesPanel() {
    $('#tables-panel').fadeOut();
}

function showChartsPanel() {
    $('#charts-panel').fadeIn();
}

function hideChartsPanel() {
    $('#charts-panel').fadeOut();
}

function exportTable(tableId) {
    var tab_text = "<table border='2px'><tr>";
    var textRange; var j = 0;
    //tab = $('#' + tableId); // id of table
    tab = document.getElementById(tableId);

    for (j = 0 ; j < tab.rows.length ; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";


    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);

}