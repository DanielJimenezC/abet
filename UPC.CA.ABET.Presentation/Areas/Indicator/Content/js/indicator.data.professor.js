﻿$(document).on('change', '#termDropDown', function (e) {
    var ModalidadId = $('#modalidadHidden').val();
    var termId = $('#termDropDown').val();
   
    if (termId == 0) {
        return;
    }

    if (ModalidadId == 1) {

        var getCargarIndicadorUrl = $('#GetCargarIndicador').val();
        var moduleId = 0;
        JSONObject = { 'termId': termId, 'moduleId': moduleId }

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCargarIndicadorUrl,
            success: function (json) {
                console.log(json);
                $('#docentesATiempo').val(json.aTiempo);
                $('#docentesADestiempo').val(json.aDestiempo);
                setProffesorsCount(json.ProfessorsCount);
                showIndicatorData();
            }
        });
    }
}).on('change', '#moduleDropDown', function (e) {
    var termId = $('#termDropDown').val();
    var moduleId = $('#moduleDropDown').val();
   

    if (termId == 0) {
        return;
    }

    if (moduleId == 0) {
        return;
    }

    var getCargarIndicadorUrl = $('#GetCargarIndicador').val();
    JSONObject = { 'termId': termId, 'moduleId': moduleId }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getCargarIndicadorUrl,
        success: function (json) {
            console.log(json);
            $('#docentesATiempo').val(json.aTiempo);
            $('#docentesADestiempo').val(json.aDestiempo);
            setProffesorsCount(json.ProfessorsCount);
            showIndicatorData();
        }
    });

    });

function showIndicatorData() {
    $('#rowDocentes').fadeIn('fast');
}

function setProffesorsCount(count) {
    $('#count-row').empty();
    var text = '</br><p>Máximo número de docentes: '+ count + '</p></br>';
    $('#count-row').append(text);
}

$('#btn-guardar').click(function () {
 
    $('#success-msg').fadeOut('fast');
    $('#error-msg').fadeOut('fast');

    var getGuardarIndicadorUrl = $('#GetGuardarIndicador').val();
    var aTiempo = $('#docentesATiempo').val();
    var aDestiempo = $('#docentesADestiempo').val();
    var termId = $('#termDropDown').val();
    var moduleId = $('#moduleDropDown').val();

    if (moduleId == null) { moduleId = 0; }
    
    var JSONObject = { 'aTiempo': aTiempo, 'aDestiempo': aDestiempo, 'termId': termId, 'moduleId': moduleId }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getGuardarIndicadorUrl,
        success: function (json) {
            if (json.error) {
                hideSuccessMessage();
                showErrorMessage(json.message);
            } else {
                hideErrorMessage();
                showSuccessMessage(json.message);
            }
        }
    });
    
})

function showSuccessMessage(message) {
    $('#success-msg').empty();
    $('#success-msg').append(getSuccessMessage(message));
    $('#success-msg').fadeIn('fast');
}

function hideSuccessMessage() {
    $('#success-msg').fadeOut('fast');
}

function showErrorMessage(message) {
    $('#error-msg').empty();
    $('#error-msg').append(getErrorMessage(message));
    $('#error-msg').fadeIn('fast');
}

function hideErrorMessage() {
    $('#error-msg').fadeOut('fast');
}

function getSuccessMessage(message) {
    return '<h5 class="text-success"><span class="fa fa-check-circle"></span>&nbsp;' + message + '</h5>';
}

function getErrorMessage(message) {
    return '<h5 class="text-danger"><span class="fa fa-times-circle"></span>&nbsp;' + message + '</h5>';
}

$(document).ready(function () {
    $("#docentesADestiempo").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#digit-msg").fadeIn().fadeOut("slow");
            return false;
        }
    });

    $("#docentesATiempo").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#digit-msg").fadeIn().fadeOut("slow");
            return false;
        }
    });
});