﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers.Common
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class CommonApiController : BaseController
    {
        private IndicatorLogic _interactor;
        private TermPresenter _termPresenter;
        private AreaPresenter _areaPresenter;
        private SubjectPresenter _subjectPresenter;

        public CommonApiController()
        {
            _interactor = new IndicatorLogic(Context);
            _termPresenter = new TermPresenter();
            _areaPresenter = new AreaPresenter();
            _subjectPresenter = new SubjectPresenter();
        }

        [HttpPost]
        public ActionResult GetTerms()
        {
            return Json(new { ciclos = GetTermsSelectList() });
        }

        [HttpPost]
        public ActionResult GetAreas(int termId)
        {
            return Json(new { areas = GetAreasSelectList(termId) });
        }

        [HttpPost]
        public ActionResult GetSubjects(int areaId)
        {
            return Json(new { cursos = GetSubjectsSelectList(areaId) });
        }

        private IEnumerable<SelectListItem> GetTermsSelectList()
        {
			int parModalidadId = Session.GetModalidadId();

            return _termPresenter.PresentTerms(_interactor.GetAcademicPeriods(parModalidadId));
        }

        private IEnumerable<SelectListItem> GetAreasSelectList(int termId)
        {
            return _areaPresenter.PresentAreas(_interactor.GetAreas(termId));
        }

        private IEnumerable<SelectListItem> GetSubjectsSelectList(int areaId)
        {
            return _subjectPresenter.PresentSubjects(_interactor.GetSubjects(areaId));
        }
    }
}