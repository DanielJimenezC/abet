﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Areas.Indicator.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class ReportResourceRetriever
    {
        public static ReportResource Get(string type)
        {
            switch (type)
            {
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_LEVEL:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.NivelDeAttrition,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_MODALITY:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.AttritionPorModalidad,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_RATIO:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.RatiosDeAttrition,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.CORRECT_EXAMS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.CalidadDeElaboracionDeExamenes,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.HIRED_PROFESSORS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.DocentesContratadosAtiempo,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_CD:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.EntregaDeCD,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_EXAMS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.EntregaDeExamenes,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_SCHEDULE:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.HorariosEntregadosaTiempo,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_SYLLABUS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.SilabosEntregadosaTiempo,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_VIRTUAL_CLASS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.ConfiguracionEnElAulaVirtual,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.COHORT:
                case ConstantHelpers.INDICATOR_REPORT_TYPE.STUDENT_COHORT:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.EgresadosPorCohorte,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_HOURS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.HorasProgramadasDeDocentes,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_PUNCTUALITY:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.PuntualidadDeDocentes,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_ATTENDANCE:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.AsistenciaDeDocentes,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.CERTIFICATED_STUDENTS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.Titulados,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.MEETINGS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.ReunionesDeCoordinacion,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.STUDENT_CAMPUS:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.AlumnosPorSede,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.STUDENT_SECTION:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.AlumnosPorSeccion,
                        Icon = "line-chart"
                    };
                case ConstantHelpers.INDICATOR_REPORT_TYPE.STUDENT_PERIOD:
                    return new ReportResource
                    {
                        Title = _IndicatorResource.AlumnosPorCiclo,
                        Icon = "line-chart"
                    };
                default:
                    throw new Exception(_IndicatorResource.TipoDeReporte + " '" + type + "' " + _IndicatorResource.NoSoportado);
            }
        }
    }

    public class ReportResource
    {
        public string Title { get; set; }
        public string Icon { get; set; }
    }
}