﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class ReportController : BaseController
    {
        private string SidebarType = "Indicator";
        private IndicatorLogic _interactor;
        private TermPresenter _termPresenter;

        public ReportController()
        {
            _interactor = new IndicatorLogic(Context);
            _termPresenter = new TermPresenter();
        }

        private IEnumerable<SelectListItem> GetTerms()
        {
			int parModalidadId = Session.GetModalidadId();
            return _termPresenter.PresentTerms(_interactor.GetAcademicPeriods(parModalidadId));
        }

        public ActionResult Default(string type)
        {
            TempData["SidebarType"] = SidebarType;
            return GetReportView(type);
        }

        private ViewResult GetReportView(string type)
        {
            var resource = ReportResourceRetriever.Get(type);
            var result = new ReportViewModel
            {
                Terms = GetTerms(),
                GetReportUrl = Url.AbsoluteAction("GetReport", "ReportApi"),
                Type = type,
                Plugin = "highcharts",
                Title = resource.Title,
                Icon = resource.Icon
            };
            return View(result);
        }
        
    }
}