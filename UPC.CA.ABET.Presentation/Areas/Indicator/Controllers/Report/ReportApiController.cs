﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Logic.Areas.Indicator.Report;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class ReportApiController : BaseController
    {

        [HttpPost]
        public ActionResult GetReport(ReportRequest request)
        {
            var report = ReportFactory.GetReport(request.Type, request.TermId, Context);
            
            switch(request.Plugin)
            {
                case "c3":
                    var c3presenter = new ReportPresenter<C3ChartModel>();
                    return Json(c3presenter.PresentReport(report));
                case "highcharts":
                    var highpresenter = new ReportPresenter<HighChartModel>();
                    return Json(highpresenter.PresentReport(report));
                default:
                    throw new Exception(Presentation.Resources.Views.Shared.MessageResource.PluginNoSoportado);
            }
            
        }

    }
}