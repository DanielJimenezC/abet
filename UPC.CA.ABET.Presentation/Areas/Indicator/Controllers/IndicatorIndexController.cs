﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Indicator.Resources.Views.IndicatorDataIndex;
namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class IndicatorIndexController : MaintenanceApiController
    {
        // GET: IndicatorIndex
        public ActionResult IndicatorDataIndex()
        {
            TempData["SidebarType"] = "Maintenance";
            return View();
        }

        public ActionResult ViewIndicatorIndex()
        {
            TempData["SidebarType"] = "Indicator";
            return View();
        }
    }
}