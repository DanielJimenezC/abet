﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class MaintenanceController : BaseController
    {
        private string SidebarType = "Maintenance";
        private IndicatorLogic _interactor;
        private TermPresenter _termPresenter;

        public MaintenanceController()
        {
            _interactor = new IndicatorLogic(Context);
            _termPresenter = new TermPresenter();
        }

        private IEnumerable<SelectListItem> GetTerms()
        {
			int parModalidadId = Session.GetModalidadId();
            TempData["SidebarType"] = SidebarType;
            return _termPresenter.PresentTerms(_interactor.GetActiveAcademicPeriods(parModalidadId));
        }

        private IEnumerable<SelectListItem> GetModules(int PeriodoAcademicoId)
        {
            int parModalidad = Session.GetModalidadId();
            TempData["SidebarType"] = SidebarType;
            return _termPresenter.PresentModules(_interactor.GetModulesforPeriod(PeriodoAcademicoId));
        }

        public ActionResult InTimeSyllabusCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceView(ConstantHelpers.INDICATOR_DATA_TYPE.SYLLABUS);
        }

        public ActionResult VirtualClassConfigurationCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceView(ConstantHelpers.INDICATOR_DATA_TYPE.VIRTUAL_CLASS);
        }

        public ActionResult HiredProfessorsCheck()
        {
            TempData["SidebarType"] = SidebarType;
            int ModalidadId = Session.GetModalidadId();
            int PeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.SubModalidad.IdModalidad == ModalidadId).First().IdPeriodoAcademico;
            var viewModel = new MaintenanceViewModel();
            viewModel.Terms = GetTerms();
            viewModel.Modules = GetModules(PeriodoAcademico);
            viewModel.GetGuardarIndicadorDocenteUrl = Url.AbsoluteAction("GuardarIndicadorDocentes", "HiredProfessorsApi");
            viewModel.GetCargarIndicadorDocenteUrl = Url.AbsoluteAction("GetDocentesContratados", "HiredProfessorsApi");
            viewModel.ModalidadId = Session.GetModalidadId().ToString();
            return View(viewModel);
        }

        public ActionResult TimeProfesor()
        {
            TempData["SidebarType"] = SidebarType;
            var viewModel = new MaintenanceViewModel();
            viewModel.Terms = GetTerms();
            //viewModel.GetGuardarIndicadorDocenteUrl = Url.AbsoluteAction("GuardarIndicadorDocentes", "HiredProfessorsApi");
            //viewModel.GetCargarIndicadorDocenteUrl = Url.AbsoluteAction("GetDocentesContratados", "HiredProfessorsApi");
            return View(viewModel);
        }

        public ActionResult InTimeScheduleCheck()
        {
            TempData["SidebarType"] = SidebarType;
            var viewModel = new MaintenanceViewModel();
            viewModel.GetAreasUrl = Url.AbsoluteAction("GetAreas", "CommonApi");
            viewModel.GetCursosUrl = Url.AbsoluteAction("GetSubjects", "CommonApi");
            viewModel.GetClassUrl = Url.AbsoluteAction("GetClasses", "MaintenanceApi");
            viewModel.GetGuardarUrl = Url.AbsoluteAction("UpdateClassesInTimeSchedule", "MaintenanceApi");
            viewModel.Terms = GetTerms();
            return View(viewModel);
        }

        public ActionResult InTimeMidTermsCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewMidTermExam(ConstantHelpers.INDICATOR_DATA_TYPE.MID_TERM_EXAM);
        }

        public ActionResult InTimeFinalExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewFinalExam(ConstantHelpers.INDICATOR_DATA_TYPE.FINAL_EXAM);
        }

        public ActionResult InTimeMakeUpExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewMakeUpExam(ConstantHelpers.INDICATOR_DATA_TYPE.MAKE_UP_EXAM);
        }

        public ActionResult CorrectFinalExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewFinalExam(ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_FINAL_EXAM);
        }

        public ActionResult CorrectMakeUpExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewMakeUpExam(ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_MAKE_UP_EXAM);
        }

        public ActionResult CorrectMidTermExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceViewMidTermExam(ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_MID_TERM_EXAM);
        }

        public ActionResult HasFinalExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceView(ConstantHelpers.INDICATOR_DATA_TYPE.HAS_FINAL);
        }

        public ActionResult HasMidTermExamCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceView(ConstantHelpers.INDICATOR_DATA_TYPE.HAS_MID_TERM);
        }

        public ActionResult InTimeCDCheck()
        {
            TempData["SidebarType"] = SidebarType;
            return GetMaintenanceView(ConstantHelpers.INDICATOR_DATA_TYPE.CD);
        }

        private ViewResult GetMaintenanceView(string fieldToUpdate)
        {
            var viewModel = new MaintenanceViewModel
            {
                GetAreasUrl = Url.AbsoluteAction("GetAreas", "CommonApi"),
                GetCursosPeriodoAcademicoUrl = Url.AbsoluteAction("GetTermSubjects", "MaintenanceApi"),
                GetGuardarUrl = Url.AbsoluteAction("UpdateTermSubjects", "MaintenanceApi"),
                FieldToUpdate = fieldToUpdate,
                Terms = GetTerms()
            };
            return View(viewModel);
        }

        private ViewResult GetMaintenanceViewMakeUpExam(string fieldToUpdate)
        {
            var viewModel = new MaintenanceViewModel
            {
                GetAreasUrl = Url.AbsoluteAction("GetAreas", "CommonApi"),
                GetCursosPeriodoAcademicoUrl = Url.AbsoluteAction("GetTermSubjectsWithMakeUp", "MaintenanceApi"),
                GetGuardarUrl = Url.AbsoluteAction("UpdateTermSubjects", "MaintenanceApi"),
                FieldToUpdate = fieldToUpdate,
                Terms = GetTerms()
            };
            return View(viewModel);
        }

        private ViewResult GetMaintenanceViewFinalExam(string fieldToUpdate)
        {
            var viewModel = new MaintenanceViewModel
            {
                GetAreasUrl = Url.AbsoluteAction("GetAreas", "CommonApi"),
                GetCursosPeriodoAcademicoUrl = Url.AbsoluteAction("GetTermSubjectsWithFinal", "MaintenanceApi"),
                GetGuardarUrl = Url.AbsoluteAction("UpdateTermSubjects", "MaintenanceApi"),
                FieldToUpdate = fieldToUpdate,
                Terms = GetTerms()
            };
            return View(viewModel);
        }

        private ViewResult GetMaintenanceViewMidTermExam(string fieldToUpdate)
        {
            var viewModel = new MaintenanceViewModel
            {
                GetAreasUrl = Url.AbsoluteAction("GetAreas", "CommonApi"),
                GetCursosPeriodoAcademicoUrl = Url.AbsoluteAction("GetTermSubjectsWithMidTerm", "MaintenanceApi"),
                GetGuardarUrl = Url.AbsoluteAction("UpdateTermSubjects", "MaintenanceApi"),
                FieldToUpdate = fieldToUpdate,
                Terms = GetTerms()
            };
            return View(viewModel);
        }

        #region Nuevo Indicador EPE

        public ActionResult IndicatorIndex()
        {
            return View();
        }
        #endregion

    }
}