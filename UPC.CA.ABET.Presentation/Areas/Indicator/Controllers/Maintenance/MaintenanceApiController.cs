﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class MaintenanceApiController : BaseController
    {
        private IndicatorLogic _interactor;
        private GetClassesPresenter _getClassesPresenter;
        private GetTermSubjectsPresenter _getTermSubjectsPresenter;

        public MaintenanceApiController()
        {
            _interactor = new IndicatorLogic(Context);
            _getClassesPresenter = new GetClassesPresenter();
            _getTermSubjectsPresenter = new GetTermSubjectsPresenter();
        }

        [HttpPost]
        public ActionResult GetClasses(GetClassesRequest request)
        {
            var secciones = _interactor.GetClasses(request.CursoPeriodoAcademicoId);
            var area = _interactor.GetUnidadAcademica(request.AreaId);

            return Json(_getClassesPresenter.PresentClasses(secciones, area));
        }

        [HttpPost]
        public ActionResult GetTermSubjects(GetTermSubjectsRequest request)
        {
            var cursosPeriodoAcademico = _interactor.getCursoPeriodoAcademico(request.AreaId);
            var area = _interactor.GetUnidadAcademica(request.AreaId);

            return Json(_getTermSubjectsPresenter.PresentTermSubjects(
                    cursosPeriodoAcademico, area, request.FieldToUpdate));
        }

        [HttpPost]
        public ActionResult GetTermSubjectsWithFinal(GetTermSubjectsRequest request)
        {
            var cursosPeriodoAcademico = _interactor.getCursoPeriodoAcademicoWithFinalExam(request.AreaId);
            var area = _interactor.GetUnidadAcademica(request.AreaId);

            return Json(_getTermSubjectsPresenter.PresentTermSubjects(
                    cursosPeriodoAcademico, area, request.FieldToUpdate));
        }

        [HttpPost]
        public ActionResult GetTermSubjectsWithMidTerm(GetTermSubjectsRequest request)
        {
            var cursosPeriodoAcademico = _interactor.getCursoPeriodoAcademicoWithMidTermExam(request.AreaId);
            var area = _interactor.GetUnidadAcademica(request.AreaId);

            return Json(_getTermSubjectsPresenter.PresentTermSubjects(
                    cursosPeriodoAcademico, area, request.FieldToUpdate));
        }

        [HttpPost]
        public ActionResult GetTermSubjectsWithMakeUp(GetTermSubjectsRequest request)
        {
            var cursosPeriodoAcademico = _interactor.getCursoPeriodoAcademicoWithMakeUpExam(request.AreaId);
            var area = _interactor.GetUnidadAcademica(request.AreaId);

            return Json(_getTermSubjectsPresenter.PresentTermSubjects(
                    cursosPeriodoAcademico, area, request.FieldToUpdate));
        }

        [HttpPost]
        public ActionResult UpdateClassesInTimeSchedule(UpdateClassesRequest request)
        {
            request.Classes.ForEach(updateModel =>
            {
                var seccion = Context.Seccion.FirstOrDefault(x => x.IdSeccion == updateModel.Id);
                seccion.RevisionHorarioATiempo = updateModel.InTimeSchedule;
            });

            Context.SaveChanges();
            return Json(new { mensaje = "Se grabaron los cambios" });
        }

        [HttpPost]
        public ActionResult UpdateTermSubjects(UpdateTermSubjectsRequest request)
        {
            request.TermSubjects.ForEach(updateModel =>
            {
                UpdateTermSubject(updateModel);
            });

            Context.SaveChanges();
            return Json(new { mensaje = "Se grabaron los cambios" });
        }

        private void UpdateTermSubject(UpdateTermSubjectViewModel updateModel)
        {
            var cursoPeriodoAcademico = Context.CursoPeriodoAcademico.FirstOrDefault(x => x.IdCursoPeriodoAcademico == updateModel.Id);
            #region ugly switch
            switch (updateModel.FieldName)
            {
                case "silabo":
                    cursoPeriodoAcademico.SilaboATiempo = updateModel.FieldValue;
                    break;
                case "aulaVirtual":
                    cursoPeriodoAcademico.AulaVirtualATiempo = updateModel.FieldValue;
                    break;
                case "examenFinal":
                    cursoPeriodoAcademico.FinalATiempo = updateModel.FieldValue;
                    break;
                case "examenParcial":
                    cursoPeriodoAcademico.ParcialATiempo = updateModel.FieldValue;
                    break;
                case "examenRecuperacion":
                    cursoPeriodoAcademico.RecuperacionATiempo = updateModel.FieldValue;
                    break;
                case "calidadFinal":
                    cursoPeriodoAcademico.FinalCorrecto = updateModel.FieldValue;
                    break;
                case "calidadParcial":
                    cursoPeriodoAcademico.ParcialCorrecto = updateModel.FieldValue;
                    break;
                case "calidadRecuperacion":
                    cursoPeriodoAcademico.RecuperacionCorrecto = updateModel.FieldValue;
                    break;
                case "CD":
                    cursoPeriodoAcademico.CDATiempo = updateModel.FieldValue;
                    break;
                case "tieneFinal":
                    cursoPeriodoAcademico.TieneFinal = updateModel.FieldValue;
                    break;
                case "tieneParcial":
                    cursoPeriodoAcademico.TieneParcial = updateModel.FieldValue;
                    break;
                default:
                    throw new Exception("No se encontró el campo " + updateModel.FieldName);
            }
            #endregion
        }   
    }
}