﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers.Maintenance
{
    // TO DO: Limpiar código
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class HiredProfessorsApiController : BaseController
    {
        private IndicatorLogic _interactor;

        public HiredProfessorsApiController()
        {
            _interactor = new IndicatorLogic(Context);
        }

        public int GetSubModalidadPeriodoAcademicoModulo(int termId, int moduleId)
        {
            int IdSubModalidadPeriodoAcademicoModulo;
            int IdSubModalidadPeriodoAcademico;

            IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == termId).IdSubModalidadPeriodoAcademico;

            if (moduleId != 0)
            {
                IdSubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.
                    FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdModulo == moduleId).IdSubModalidadPeriodoAcademicoModulo;
            }
            else
            {
                IdSubModalidadPeriodoAcademicoModulo = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdSubModalidadPeriodoAcademicoModulo;
            }

            return IdSubModalidadPeriodoAcademicoModulo;
        }

        [HttpPost]
        public ActionResult GuardarIndicadorDocentes(string aTiempo, string aDestiempo, int termId,int moduleId)
        {

            int tiempo;

            int IdSMPAM = GetSubModalidadPeriodoAcademicoModulo(termId, moduleId);

            if (!int.TryParse(aTiempo, out tiempo) || tiempo < 0)
            {
                return Json(new { error = true, message = "Debe ingresar un número entero mayor o igual a 0 en el campo 'Profesores contratados a tiempo'" });
            }

            int destiempo;
            if (!int.TryParse(aDestiempo, out destiempo) || destiempo < 0)
            {
                return Json(new { error = true, message = "Debe ingresar un número entero mayor o igual a 0 en el campo 'Profesores no contratados a tiempo'" });
            }

            int professorsCount = _interactor.GetProfessorsCount(IdSMPAM);

            if (tiempo + destiempo > professorsCount)
            {
                return Json(
                    new
                    {
                        error = true,
                        message = "El número total de docentes contratados no debe ser mayor a " + professorsCount
                    });
            }

            var indicadorDocente = Context.IndicadorDocente.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo== IdSMPAM);

            if (indicadorDocente == null)
            {
                int smpa = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
               // int smpam = context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == smpa).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

                indicadorDocente = new IndicadorDocente
                {
                    IdSubModalidadPeriodoAcademicoModulo = IdSMPAM,
                    IdSubModalidadPeriodoAcademico=smpa
                };
                Context.IndicadorDocente.Add(indicadorDocente);
            }

            indicadorDocente.DocentesContratadoDestiempo = destiempo;
            indicadorDocente.DocentesContratadoTiempo = tiempo;
            Context.SaveChanges();
            return Json(new { message = "Se guardaron los cambios correctamente" });
        }

       

        [HttpPost]
        public ActionResult GetDocentesContratados(int termId,int moduleId)
        {
            var response = new HiredProfessorsResponse();

            int IdSubModalidadPeriodoAcademicoModulo;
            int IdSubModalidadPeriodoAcademico;

            IdSubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == termId).IdSubModalidadPeriodoAcademico;

            IdSubModalidadPeriodoAcademicoModulo = GetSubModalidadPeriodoAcademicoModulo(termId, moduleId);


             var indicadorDocente = Context.IndicadorDocente.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == IdSubModalidadPeriodoAcademicoModulo);

            if (indicadorDocente == null)
            {
                indicadorDocente = new IndicadorDocente
                {
                    DocentesContratadoDestiempo = 0,
                    DocentesContratadoTiempo = 0,
                    IdSubModalidadPeriodoAcademico = IdSubModalidadPeriodoAcademico,
                    IdSubModalidadPeriodoAcademicoModulo = IdSubModalidadPeriodoAcademicoModulo
                };
                Context.IndicadorDocente.Add(indicadorDocente);
                Context.SaveChanges();
            }

            response.aTiempo = indicadorDocente.DocentesContratadoTiempo;
            response.aDestiempo = indicadorDocente.DocentesContratadoDestiempo;
            response.ProfessorsCount = _interactor.GetProfessorsCount(IdSubModalidadPeriodoAcademicoModulo);
            return Json(response);
        }
    }
}