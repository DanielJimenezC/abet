﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Excel;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models.Areas.Survey;
using UPC.CA.ABET.Models;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Data;
using Excel;
using System.Transactions;
using OfficeOpenXml;
using System.Drawing;
using System.Data.Entity;
using UPC.CA.ABET.Presentation.Filters;
using OfficeOpenXml.Style;
using System.Text.RegularExpressions;
using UPC.CA.ABET.Presentation.Areas.Survey.ViewModels.Email;
using System.Data.Entity.Core.Objects;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Controllers
{
    [AuthorizeUserAttribute(AppRol.Administrador, AppRol.CoordinadorCarrera)]
    public class ExcelController : BaseController
    {
        // GET: Indicator/Excel
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadHorasProfesor(MaintenanceViewModel model)
        {
            int ModalidadId = Session.GetModalidadId();
            if (ModalidadId == 1)
            {
                DataCarga data = new DataCarga();
                try
                {
                    Int32 colCodigo = 0;
                    Int32 colHoras = 1;
                    Int32 colSede = 2;

                    Int32 cantFail = 0;
                    Int32 cantSuccess = 0;

                    var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                    data.lstError = new List<string>();
                    for (int i = 1; i < dataTable.Rows.Count; i++)
                    {
                        try
                        {
                            var outputparameter = new ObjectParameter("Resultado", typeof(Int32));
                            var CodigoProfesor = dataTable.Rows[i][colCodigo].ToString().Trim();
                            var Horas = Convert.ToDouble(dataTable.Rows[i][colHoras].ToString().Trim());
                            var Sede = dataTable.Rows[i][colSede].ToString().Trim();
                            int IdSMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademicoModulo; 

                            Context.Usp_InsertarDocenteHorasProgramadas(CodigoProfesor, Sede, IdSMPAM, Horas, null, null,null,outputparameter);

                            Int32 resultado = (Int32)outputparameter.Value;
                            /*
                             -1:	Error en la inserción 
                             1:		Se registró con éxito
                             2:   	El código de docente no se encuentra
                             3:		La sede no existe 
                            */
                            switch (resultado)
                            {
                                case -1:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "Ocurrió un error en el procedimiento." : "An error occurred in the procedure."));
                                    break;
                                case 1:
                                    cantSuccess++;
                                    break;
                                case 2:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de docente no existe ." : "The professor does not exist."));
                                    break;
                                case 3:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de sede no existe." : "The campus code doest not exist."));
                                    break;
                                case 4:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código ya se registró para este ciclo." : "The code is already registered."));
                                    break;
                                  
                            }

                            data.CantSuccess = cantSuccess;
                            data.Error = false;
                        }
                        catch (Exception ex)
                        {
                            data.Error = true;
                        }
                    }
                    return Json(data);
                }
                catch (Exception ex)
                {
                    data.Error = true;
                    return Json(data);
                };
            }
            else
            {
                DataCarga data = new DataCarga();
                try
                {

                    Int32 codCodigoDocente = 0;
                    Int32 codCodigoCurso = 1;
                    Int32 codCodigoSeccion = 2;
                    Int32 colHorasPresenciales = 3;
                    Int32 colHorasBlended = 4;
                    Int32 colCodigoSede = 5;

                    Int32 cantFail = 0;
                    Int32 cantSuccess = 0;

                    var dataTable = ExcelLogic.ExtractExcelToDataTable(model.Archivo);

                    data.lstError = new List<string>();
                    for (int i = 1; i < dataTable.Rows.Count; i++)
                    {
                        try
                        {
                            var outputparameter = new ObjectParameter("Resultado", typeof(Int32));
                            var CodigoProfesor = dataTable.Rows[i][codCodigoDocente].ToString().Trim();
                            var CodigoCurso = dataTable.Rows[i][codCodigoCurso].ToString().Trim();
                            var CodigoSeccion= dataTable.Rows[i][codCodigoSeccion].ToString().Trim();
                            var HorasPresenciales = Convert.ToDouble(dataTable.Rows[i][colHorasPresenciales].ToString().Trim());
                            var HorasBlended = Convert.ToDouble(dataTable.Rows[i][colHorasBlended].ToString().Trim());
                            var CodigoSede = dataTable.Rows[i][colCodigoSede].ToString().Trim();
                         
                            int IdSMPAM = Context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.Estado == "ACT").IdSubModalidadPeriodoAcademicoModulo;

                            Context.Usp_InsertarDocenteHorasProgramadas(CodigoProfesor, CodigoSede, IdSMPAM, HorasPresenciales, HorasBlended, CodigoSeccion, CodigoCurso, outputparameter);

                            Int32 resultado = (Int32)outputparameter.Value;
                            /*
                             -1:	Error en la inserción 
                             1:		Se registró con éxito
                             2:   	El código de docente no se encuentra
                             3:		La sede no existe 
                            */
                            switch (resultado)
                            {
                                case -1:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "Ocurrió un error en el procedimiento." : "An error occurred in the procedure."));
                                    break;
                                case 1:
                                    cantSuccess++;
                                    break;
                                case 2:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de docente no existe ." : "The professor does not exist."));
                                    break;
                                case 3:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código de sede no existe." : "The campus code doest not exist."));
                                    break;
                                case 4:
                                    cantFail++;
                                    data.lstError.Add(CodigoProfesor + " " + ((currentCulture.Equals("es-PE")) ? "El código ya se registró para este ciclo." : "The code is already registered."));
                                    break;

                            }

                            data.CantSuccess = cantSuccess;
                            data.Error = false;
                        }
                        catch (Exception ex)
                        {
                            data.Error = true;
                        }
                    }
                    return Json(data);
                }
                catch (Exception ex)
                {
                    data.Error = true;
                    return Json(data);
                };
            }
        }

        [HttpPost]
        public ActionResult DownloadProfesorHProgramadas()
        {
            var fechaActual = DateTime.Now;

            int ModalidadId = Session.GetModalidadId();

            if (ModalidadId == 1)
            {
                String nombreArchivo = "Plantilla_CargaMasiva_ProfesorHorasProgramadasRegular" + fechaActual.ToString("ddMMyyyy") + ".xlsx";
                String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                try
                {
                    Int32 i;
                    var memoryStream = new MemoryStream();
                    FileInfo newFile = new FileInfo(nombreArchivo);
                    using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                    {
                        ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");

                        worksheet.Cells["A1"].Value = "CÓDIGO DOCENTE";
                        worksheet.Cells["A1"].AutoFitColumns();

                        worksheet.Cells["B1"].Value = "HORAS";
                        worksheet.Cells["B1"].AutoFitColumns();

                        worksheet.Cells["C1"].Value = "SEDE(MO,SI,VL,CS)";
                        worksheet.Cells["C1"].AutoFitColumns();


                        worksheet.Cells[2, 1].Value = "PCSIJABA";
                        worksheet.Cells[2, 1].AutoFitColumns();

                        worksheet.Cells[2, 2].Value = "18.5";
                        worksheet.Cells[2, 2].AutoFitColumns();

                        worksheet.Cells[2, 3].Value = "MO";
                        worksheet.Cells[2, 3].AutoFitColumns();



                        worksheet.Cells[1, 1, 1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, 1, 1, 3].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        worksheet.Cells[1, 1, 1, 3].Style.Font.Color.SetColor(Color.White);


                        var fileStream = new MemoryStream();
                        xlPackage.SaveAs(fileStream);
                        fileStream.Position = 0;

                        var fsr = new FileStreamResult(fileStream, contentType);
                        fsr.FileDownloadName = nombreArchivo;

                        return fsr;
                    }
                }
                catch (IOException ioe)
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("TimeProfesor", "Maintenance");
                }

            }
            else
            {
                String nombreArchivo = "Plantilla_CargaMasiva_ProfesorHorasProgramadasEPE" + fechaActual.ToString("ddMMyyyy") + ".xlsx";
                String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                try
                {
                    Int32 i;
                    var memoryStream = new MemoryStream();
                    FileInfo newFile = new FileInfo(nombreArchivo);
                    using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                    {
                        ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Plantilla Carga Masiva");

                        worksheet.Cells["A1"].Value = "CÓDIGO DOCENTE";
                        worksheet.Cells["A1"].AutoFitColumns();

                        worksheet.Cells["B1"].Value = "CÓDIGO CURSO";
                        worksheet.Cells["B1"].AutoFitColumns();

                        worksheet.Cells["C1"].Value = "CÓDIGO SECCIÓN";
                        worksheet.Cells["C1"].AutoFitColumns();

                        worksheet.Cells["D1"].Value = "HORAS PRESENCIALES";
                        worksheet.Cells["D1"].AutoFitColumns();

                        worksheet.Cells["E1"].Value = "HORAS BLENDED";
                        worksheet.Cells["E1"].AutoFitColumns();

                        worksheet.Cells["F1"].Value = "SEDE(MO,SI,VL,CS)";
                        worksheet.Cells["F1"].AutoFitColumns();



                        worksheet.Cells[2, 1].Value = "PCSIJABA";
                        worksheet.Cells[2, 1].AutoFitColumns();

                        worksheet.Cells[2, 2].Value = "MA420";
                        worksheet.Cells[2, 2].AutoFitColumns();

                        worksheet.Cells[2, 3].Value = "AV9A";
                        worksheet.Cells[2, 3].AutoFitColumns();

                        worksheet.Cells[2, 4].Value = "18.2";
                        worksheet.Cells[2, 4].AutoFitColumns();

                        worksheet.Cells[2, 5].Value = "18.5";
                        worksheet.Cells[2, 5].AutoFitColumns();         

                        worksheet.Cells[2, 6].Value = "MO";
                        worksheet.Cells[2, 6].AutoFitColumns();


                        worksheet.Cells[1, 1, 1, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, 1, 1, 6].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        worksheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(Color.White);


                        var fileStream = new MemoryStream();
                        xlPackage.SaveAs(fileStream);
                        fileStream.Position = 0;

                        var fsr = new FileStreamResult(fileStream, contentType);
                        fsr.FileDownloadName = nombreArchivo;

                        return fsr;
                    }
                }
                catch (IOException ioe)
                {
                    PostMessage(MessageType.Error);
                    return RedirectToAction("TimeProfesor", "Maintenance");
                }
            }
        }
    }
}