﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class GetTermSubjectsPresenter
    {
        public GetTermSubjectsResponse PresentTermSubjects(List<CursoPeriodoAcademico> termSubjects, UnidadAcademica area, string fieldToUpdate)
        {
            var termSubjectViewModels = new List<TermSubjectViewModel>();
            foreach (var termSubject in termSubjects)
            {
                termSubjectViewModels.Add(CreateTermSubjectViewModel(termSubject, area, fieldToUpdate));
            }
            return new GetTermSubjectsResponse
            {
                TermSubjects = termSubjectViewModels
            };
        }

        private TermSubjectViewModel CreateTermSubjectViewModel(
            CursoPeriodoAcademico termSubject, UnidadAcademica area, string fieldToUpdate)
        {
            return new TermSubjectViewModel
            {
                IdCursoPeriodoAcademico = termSubject.IdCursoPeriodoAcademico,
                Area = area.NombreEspanol,
                NombreCurso = termSubject.Curso.NombreEspanol,
                CodigoCurso = termSubject.Curso.Codigo,
                CampoAActualizar = fieldToUpdate,
                ValorCheckbox = CheckBoxState(termSubject, fieldToUpdate)
            };
        }

        private bool CheckBoxState(CursoPeriodoAcademico termSubject, string fieldToUpdate)
        {
            switch (fieldToUpdate)
            {
                case ConstantHelpers.INDICATOR_DATA_TYPE.SYLLABUS:
                    return termSubject.SilaboATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.CD:
                    return termSubject.CDATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.VIRTUAL_CLASS:
                    return termSubject.AulaVirtualATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.MID_TERM_EXAM:
                    return termSubject.ParcialATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.FINAL_EXAM:
                    return termSubject.FinalATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.MAKE_UP_EXAM:
                    return termSubject.RecuperacionATiempo.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_FINAL_EXAM:
                    return termSubject.FinalCorrecto.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_MAKE_UP_EXAM:
                    return termSubject.RecuperacionCorrecto.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.CORRECT_MID_TERM_EXAM:
                    return termSubject.ParcialCorrecto.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.HAS_FINAL:
                    return termSubject.TieneFinal.GetValueOrDefault();
                case ConstantHelpers.INDICATOR_DATA_TYPE.HAS_MID_TERM:
                    return termSubject.TieneParcial.GetValueOrDefault();
                default:
                    throw new Exception("Campo a actualizar inválido");
            }
        }
    }
}