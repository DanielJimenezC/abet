﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class GetClassesPresenter
    {
        public GetClassesResponse PresentClasses(List<Seccion> secciones, UnidadAcademica area)
        {
            var classes = new List<GetClassViewModel>();
            foreach (var seccion in secciones)
            {
                classes.Add(CreateClassViewModel(seccion, area));
            }
            return new GetClassesResponse
            {
                Classes = classes
            };
        }

        private GetClassViewModel CreateClassViewModel(Seccion seccion, UnidadAcademica area)
        {
            return new GetClassViewModel
            {
                IdSeccion = seccion.IdSeccion,
                NombreDeCurso = seccion.CursoPeriodoAcademico.Curso.NombreEspanol,
                Seccion = seccion.Codigo,
                Area = area.NombreEspanol,
                CodigoDeCurso = seccion.CursoPeriodoAcademico.Curso.Codigo,
                aTiempo = seccion.RevisionHorarioATiempo.GetValueOrDefault()
            };
        }
    }
}