﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public interface ChartParser<T>
    {
        T GetChartViewModel(Chart chart);
    }
}
