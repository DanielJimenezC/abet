﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Presentation.Areas.Indicator.Presenter.Report.HighCharts;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class HighChartParser: ChartParser<HighChartModel>
    {
        // TODO: Refactorizar
        public HighChartModel GetChartViewModel(Chart chart)
        {
            var model = BuildChart(chart);

            string value;
            if (chart.Title.TryGetValue("ES", out value))
            {
                model.title.text = chart.Title["ES"];
            }
            return model;
        }

        public HighChartModel BuildChart(Chart chart)
        {
            HighChartBuilder builder;
            switch (chart.Type)
            {
                case "":
                    builder = new HighChartBuilder()
                                    .BuildLine()
                                    .LoadDefault(chart);
                    break;
                case "bar":
                    builder = new HighChartBuilder()
                                    .BuildColumn()
                                    .LoadDefault(chart);
                    break;
                case "pie":
                    builder = new HighChartBuilder()
                                    .BuildPie()
                                    .LoadPie(chart);
                    break;
                case "stacked":
                    builder = new HighChartBuilder()
                                    .BuildStacked()
                                    .LoadStacked(chart);
                    break;
                default:
                    throw new Exception("Tipo de chart no soportado");
            }
            return builder.Model;
        }
    }
}