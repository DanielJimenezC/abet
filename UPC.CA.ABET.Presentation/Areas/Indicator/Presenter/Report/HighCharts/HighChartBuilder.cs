﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter.Report.HighCharts
{
    public class HighChartBuilder
    {
        public HighChartModel Model { get; set; }

        public HighChartBuilder() 
        {
            Model = new HighChartModel();
            ConfigureByDefault();
        }

        private void ConfigureByDefault()
        {
            Model.credits.enabled = false;
            Model.xAxis.crosshair = true;
            Model.yAxis.min = 0;
            Model.chart.style.fontFamily = "Verdana, sans-serif";
            Model.chart.style.fontSize = "9px";
        }

        public HighChartBuilder BuildLine()
        {
            Model.chart.type = "";
            Model.plotOptions.line.dataLabels.enabled = true;
            Model.plotOptions.line.dataLabels.color = "#1a1a1a";
            Model.plotOptions.line.dataLabels.style.fontSize = "9px";
            Model.plotOptions.line.dataLabels.style.fontFamily = "Verdana, sans-serif";
            Model.plotOptions.line.pointPadding = 0.2;
            Model.plotOptions.line.borderWidth = 0;
            return this;
        }

        public HighChartBuilder BuildColumn()
        {
            Model.chart.type = "column";
            Model.plotOptions.column.dataLabels.enabled = true;
            Model.plotOptions.column.dataLabels.color = "#1a1a1a";
            Model.plotOptions.column.dataLabels.style.fontSize = "8px";
            Model.plotOptions.column.dataLabels.style.fontFamily = "Verdana, sans-serif";
            Model.plotOptions.column.pointPadding = 0.2;
            Model.plotOptions.column.borderWidth = 0;
            return this;
        }

        public HighChartBuilder BuildStacked()
        {
            Model.chart.type = "column";
            Model.plotOptions.column.stacking = "percent";
            Model.plotOptions.column.dataLabels.enabled = true;
            Model.plotOptions.column.dataLabels.color = "#1a1a1a";
            Model.plotOptions.column.dataLabels.style.fontSize = "8px";
            Model.plotOptions.column.dataLabels.style.fontFamily = "Verdana, sans-serif";
            Model.plotOptions.column.pointPadding = 0.2;
            Model.plotOptions.column.borderWidth = 0;
            Model.xAxis.format = "{total}";
            return this;
        }

        public HighChartBuilder BuildPie()
        {
            Model.chart.type = "pie";
            Model.plotOptions.pie.dataLabels.enabled = true;
            Model.plotOptions.pie.dataLabels.color = "#1a1a1a";
            Model.plotOptions.pie.dataLabels.format = "<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)";
            Model.plotOptions.pie.dataLabels.style.fontSize = "12px";
            Model.plotOptions.pie.dataLabels.style.fontFamily = "Verdana, sans-serif";
            Model.plotOptions.pie.pointPadding = 0.2;
            Model.plotOptions.pie.borderWidth = 0;
            return this;
        }

        public HighChartBuilder LoadDefault(Chart chart)
        {
            foreach (var category in chart.Categories)
            {
                Model.xAxis.categories.Add(category.LocalizedName["ES"]);
            }

            for (int i = 0; i < chart.Columns.Count; i++)
            {
                Serie serie = GetDefaultSerie(chart, i);
                serie.dataLabels.style.fontFamily = "Verdana, sans-serif";
                serie.dataLabels.style.fontSize = "10px";
                Model.series.Add(serie);
            }
            return this;
        }

        public HighChartBuilder LoadStacked(Chart chart)
        {
            foreach (var category in chart.Categories)
            {
                Model.xAxis.categories.Add(category.LocalizedName["ES"]);
            }

            for (int i = 0; i < chart.Columns.Count; i++)
            {
                Serie serie = GetDefaultSerie(chart, i);
                serie.dataLabels.format = "{point.percentage:.1f} %";
                serie.dataLabels.style.fontFamily = "Verdana, sans-serif";
                serie.dataLabels.style.fontSize = "12px";
                Model.series.Add(serie);
            }
            return this;
        }

        private static Serie GetDefaultSerie(Chart chart, int i)
        {
            return new Serie
            {
                name = chart.Columns[i].LocalizedName["ES"],
                data = chart.Columns[i].Data
                .Select(x => x.HasValue ?
                new Data
                {
                    y = Math.Round(x.Value, 1)
                }
                : null)
                .ToList()
            };
        }

        public HighChartBuilder LoadPie(Chart chart)
        {
            var serie = new Serie
            {
                name = chart.Categories[0].LocalizedName["ES"],
                data = chart.Columns.Select( x => new Data {
                    name = x.LocalizedName["ES"],
                    y = x.Data[0]})
                    .ToList()
            };

            Model.series.Add(serie);
            return this;
        }

    }
}