﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class C3ChartParser: ChartParser<C3ChartModel>
    {
        public C3ChartModel GetChartViewModel(Chart chart)
        {
            var chartViewModel = new C3ChartModel();

            string value;
            if (chart.Title.TryGetValue("ES", out value))
            {
                chartViewModel.Title = chart.Title["ES"];
            }

            chartViewModel.Type = chart.Type;

            foreach (var chartColumn in chart.Columns)
            {
                var column = new List<string>();
                column.Add(chartColumn.LocalizedName["ES"]);
                column.AddRange(chartColumn.Data.Select(x => x.HasValue ? Math.Round(x.Value, 2).ToString() : null));
                chartViewModel.Columns.Add(column);
            }

            chartViewModel.Categories.AddRange(chart.Categories.Select(x => x.LocalizedName["ES"]));

            return chartViewModel;
        }
    }
}