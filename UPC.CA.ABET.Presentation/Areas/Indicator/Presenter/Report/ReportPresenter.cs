﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Report;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Presentation.Areas.Indicator.ViewModels;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class ReportPresenter<T>
    {

        private ChartParser<T> _parser;

        public ReportPresenter()
        {
            InstanceParser();
        }

        private void InstanceParser()
        {
            if (typeof(T) == typeof(HighChartModel))
            {
                _parser = (ChartParser<T>) new HighChartParser();
            }
            else if (typeof(T) == typeof(C3ChartModel))
            {
                _parser = (ChartParser<T>) new C3ChartParser();
            }
            else
            {
                throw new Exception("Tipo de modelo no soportado: " + typeof(T).FullName);
            }
        }

        public ReportResponse<T> PresentReport(Dashboard report)
        {
            var viewModel = new ReportResponse<T>();
            foreach (var table in report.Tables)
            {
                viewModel.Tables.Add(GetTableViewModel(table));
            }
            foreach (var chart in report.Charts)
            {
                viewModel.Charts.Add(_parser.GetChartViewModel(chart));
            }
            return viewModel;
        }

        private TableViewModel GetTableViewModel(Table table)
        {
            string value;
            var tableViewModel = new TableViewModel();
            if (table.Title.TryGetValue("ES", out value))
            {
                tableViewModel.Title = table.Title["ES"];
            }

            tableViewModel.Header.AddRange(table.Header.Columns.Select(x => x.LocalizedName["ES"]));

            foreach (var tableRow in table.Body)
            {
                tableViewModel.Body.Add(GetRow(tableRow));
            }

            foreach (var tableColumn in table.Footer.Columns)
            {
                tableViewModel.Footer.Add(GetProperValue(tableColumn.LocalizedName));
            }

            return tableViewModel;
        }

        private List<string> GetRow(Row tableRow)
        {
            List<string> row = new List<string>();
            foreach (var tableColumn in tableRow.Columns)
            {
                row.Add(GetProperValue(tableColumn.LocalizedName));
            }

            return row;
        }

        private string GetProperValue(Dictionary<string, string> localized)
        {
            string aux;
            return localized.TryGetValue("VAL", out aux) ? localized["VAL"] : localized["ES"];
        }
    }
}