﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class AreaPresenter
    {
        public IEnumerable<SelectListItem> PresentAreas(List<UnidadAcademica> areas)
        {
            var result = new List<SelectListItem>();
            result.Add(DefaultItem(areas.Count));
            foreach (var item in areas)
            {
               result.Add(
                   NewItem(
                       item.IdUnidadAcademica.ToString(),
                       item.NombreEspanol
                   ));
            }
            return result;
        }

        private SelectListItem DefaultItem(int areasCount)
        {
            return areasCount > 0 ? 
                NewItem("0", Presentation.Resources.Views.Shared.LayoutResource.Seleccione) : 
                NewItem("0", Presentation.Resources.Views.Shared.MessageResource.NoExistenAreasParaElCicloSeleccionado);
        }

        private SelectListItem NewItem(string value, string text)
        {
            return new SelectListItem
            {
                Value = value,
                Text = text
            };
        }
    }
}