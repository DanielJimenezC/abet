﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class TermPresenter
    {
        public IEnumerable<SelectListItem> PresentTerms(List<PeriodoAcademico> terms)
        {
            var result = new List<SelectListItem>();
            result.Add(DefaultItem(terms.Count));
            foreach (var item in terms)
            {
               result.Add(
                   NewItem(
                       item.IdPeriodoAcademico.ToString(),
                       item.CicloAcademico
                   ));
            }
            return result;
        }
        public IEnumerable<SelectListItem> PresentModules(List<Modulo> modules)
        {
            var result = new List<SelectListItem>();
            result.Add(DefaultItem(modules.Count));
            foreach (var item in modules)
            {
                result.Add(
                    NewItem(
                        item.IdModulo.ToString(),
                        item.NombreEspanol
                    ));
            }
            return result;
        }

        private SelectListItem DefaultItem(int termsCount)
        {
            return termsCount > 0 ? 
                NewItem("0", Presentation.Resources.Views.Shared.LayoutResource.Seleccione) : 
                NewItem("0", Presentation.Resources.Views.Shared.MessageResource.NoExistenCiclos);
        }

        private SelectListItem NewItem(string value, string text)
        {
            return new SelectListItem
            {
                Value = value,
                Text = text
            };
        }

        internal IEnumerable<SelectListItem> PresentTerms(List<Modulo> list)
        {
            throw new NotImplementedException();
        }
    }
}