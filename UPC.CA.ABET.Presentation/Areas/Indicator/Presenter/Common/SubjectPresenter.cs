﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Indicator.Presenter
{
    public class SubjectPresenter
    {
        public IEnumerable<SelectListItem> PresentSubjects(List<UnidadAcademica> subjects)
        {
            var result = new List<SelectListItem>();
            result.Add(DefaultItem(subjects.Count));
            foreach (var item in subjects)
            {
               result.Add(
                   NewItem(
                       item.CursoPeriodoAcademico.IdCursoPeriodoAcademico.ToString(),
                       item.NombreEspanol
                   ));
            }
            return result;
        }

        private SelectListItem DefaultItem(int subjectsCount)
        {
            return subjectsCount > 0 ? 
                NewItem("0", Presentation.Resources.Views.Shared.LayoutResource.Seleccione) : 
                NewItem("0", Presentation.Resources.Views.Shared.MessageResource.NoExistenCursosParaElArea);
        }

        private SelectListItem NewItem(string value, string text)
        {
            return new SelectListItem
            {
                Value = value,
                Text = text
            };
        }
    }
}