﻿using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Indicator
{
    public class IndicatorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Indicator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Indicator_default",
                "Indicator/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}