﻿$(function () {
    $(document).on('change', '#termDropDown', function (e) {

        var termId = $(this).val();
        lockComponentsWhenNewTermIsSelected(termId);

        if (termId == 0) {
            return;
        }

        hideSuccessMessage();
        hideErrorMessage();
        getCurriculums(termId);
        getCurriculumsAcademicPeriod(termId);
    });

    function lockComponentsWhenNewTermIsSelected(termId) {
        $('#curriculumDropDown').empty();
        $('#curriculumDropDown').prop('disabled', termId == 0);
        $('#btnAsociate').prop('disabled', true);
    }

    function getCurriculums(termId) {
        var JSONObject = { 'termId': termId }
        var getCurriculumsUrl = $('#GetCurriculums').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCurriculumsUrl,
            success: function (json) {
                var curriculums = json.curriculums
                $.each(curriculums, function (index, element) {
                    $('#curriculumDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
                });

                $('#curriculumDropDown')[0].selectedIndex = 0;
            }
        });
    }

    function getCurriculumsAcademicPeriod(termId) {
        var JSONObject = { 'termId': termId }
        var getCurriculumsAcademicPeriodUrl = $('#GetCurriculumsAcademicPeriodUrl').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCurriculumsAcademicPeriodUrl,
            success: function (json) {
                presentCurriculumsAcademicPeriod(json.curriculumsAcademicPeriod);
            }
        });
    }

    function presentCurriculumsAcademicPeriod(curriculumsAcademicPeriod) {
        if (curriculumsAcademicPeriod.length > 0) {
            $('#asociationsTable > tbody').empty();
            $.each(curriculumsAcademicPeriod, function (index, element) {
                $('#asociationsTable > tbody').append('<tr>' +
                                '<td class="text-center">' + element.Curriculum + '</td>' +
                                '<td class="text-center">' + element.Career + '</td>' +
                                '<td class="text-center">' + element.Term + '</td>' +
                                '<td class="text-center">' +
                                '<button id="' + element.Id + '" class="btn btn-primary btn-delete"><i class="fa fa-trash"></i></button>' +
                                '</td>' + '</tr>')
            });
            $('#curriculumsAcademicPeriodSelectMessage').fadeOut('fast');
            $('#curriculumsAcademicPeriodEmptyMessage').fadeOut('fast');
            $('#asociationsTable').fadeIn('fast');
        } else {
            showEmptyMessage();
            hideEliminationErrorMessage();
            hideSuccessMessage();
        }
    }

    $(document).on('change', '#curriculumDropDown', function (e) {

        var curriculumId = $(this).val();

        if (curriculumId == 0) {
            $('#btnAsociate').prop('disabled', true);
            return;
        }

        $('#btnAsociate').prop('disabled', false);
    });

    $("#btnAsociate").click(function () {
        var termId = $('#termDropDown').val();
        var curriculumId = $('#curriculumDropDown').val();
        var asociateCurriculumAndAcademicPeriodUrl = $('#AsociateCurriculumAndAcademicPeriod').val();

        var JSONObject = { 'termId': termId, 'curriculumId': curriculumId }

        if (termId == 0 || curriculumId == 0) {
            return;
        }

        $('#btnAsociate').prop('disabled', true);
        showSpinner();
        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: asociateCurriculumAndAcademicPeriodUrl,
            success: function (json) {
                $('#termDropDown')[0].selectedIndex = 0;
                lockComponentsWhenNewTermIsSelected("0");
                hideSpinner();
                showSuccessMessage();
            },
            error: function (json) {
                $('#termDropDown')[0].selectedIndex = 0;
                lockComponentsWhenNewTermIsSelected("0");
                hideSpinner();
                showErrorMessage();
            }
        });
    });

    $(document).on('click', '.btn-delete', function () {

        disableDeleteButtons();
        hideEliminationErrorMessage();
        var JSONObject = { 'id': $(this).attr('id') };
        var deleteCurriculumTermUrl = $('#DeleteCurriculumTermUrl').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: deleteCurriculumTermUrl,
            success: function (json) {
                $('#curriculumDropDown')[0].selectedIndex = 0;
                lockComponentsWhenNewCareerIsSelected(0);
                getCurriculumsAcademicPeriod($('#termDropDown').val());

                if (json.isError) {
                    enableDeleteButtons();
                    hideSuccessMessage();
                    showEliminationErrorMessage(json.message);
                } else {
                    hideEliminationErrorMessage();
                }
            }
        });
    });

    function showSelectMessage() {
        $('#curriculumsAcademicPeriodSelectMessage').fadeIn('fast');
        $('#asociationsTable').fadeOut('fast');
        $('#curriculumsAcademicPeriodEmptyMessage').fadeOut('fast');
    }

    function showEmptyMessage() {
        $('#curriculumsAcademicPeriodEmptyMessage').fadeIn('fast');
        $('#asociationsTable').fadeOut('fast');
        $('#curriculumsAcademicPeriodSelectMessage').fadeOut('fast');
    }

    function showSuccessMessage() {
        $('#success-msg').fadeIn('fast');
    }

    function hideSuccessMessage() {
        $('#success-msg').fadeOut('fast');
    }

    function showErrorMessage() {
        $('#error-msg').fadeIn('fast');
    }

    function hideErrorMessage() {
        $('#error-msg').fadeOut('fast');
    }

    function showSpinner() {
        $('#spinner').fadeIn('fast');
        $('#spinner-message').fadeIn('fast');
    }

    function hideSpinner() {
        $('#spinner').fadeOut('fast');
        $('#spinner-message').fadeOut('fast');
    }

    function showEliminationErrorMessage(message) {
        $('#eliminationErrorMessage').empty();
        $('#eliminationErrorMessage').append(message);
        $('#eliminationErrorMessage').fadeIn('fast');
    }

    function hideEliminationErrorMessage() {
        $('#eliminationErrorMessage').fadeOut('fast');
    }

    function lockComponentsWhenNewCareerIsSelected(careerId) {
        $('#btnAsociate').prop('disabled', true);
    }

    function disableDeleteButtons() {
        $('.btn-delete').prop('disabled', true);
    }

    function enableDeleteButtons() {
        $('.btn-delete').prop('disabled', false);
    }
});