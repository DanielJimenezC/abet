﻿$(function () {
    $(document).on("change",
        '#termDropDown',
        function(e) {
            var idTerm = $(this).val();
            if (idTerm == 0) {
                cleanEverything();
            } else {
                checkPeriodState(idTerm);
            }
        });



    $('#btnDownloadFile').click(function () {
        var downloadUrl = $('#downloadFileUrl').val();
        $.fileDownload(downloadUrl);
    });


    function cleanEverything() {
        hideSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function checkPeriodState(idTerm) {
        var uploadType = $('#uploadType').val();
        var urlUploadExists = $('#urlUploadExists').val();
        var JSONObject = { "idTerm": idTerm, "uploadType": uploadType }
        $.ajax({
            type: "POST",
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: urlUploadExists,
            success: function (json) {
                var isUploaded = json.isUploaded
                if (isUploaded) {
                    isUploadedCaseUpdate();
                } else {
                    isNotUploadedCaseUpdate();
                }
            }
        });
    }

    function getUploadButton() {
        return '<button class="btn btn-primary pull-right" id="buttonUploadExcel">' +
                   '<span class="fa fa-upload"/>&nbsp;&nbsp;Subir archivo' +
               '</button>';
    }

    function getUploadFormData() {
        var idTerm = $('#termDropDown').val();
        var idModal = $('#modalDropDown').val();
        var idSubModal = $('#subModalDropDown').val();
        var uploadType = $('#uploadType').val();
        return JSONObject = { "idTerm": idTerm,"idModal":idModal,"idSubModal":idSubModal, "uploadType": uploadType };
    }

    function isUploadedCaseUpdate() {
        showSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function isNotUploadedCaseUpdate() {
        generate();
        hideSuccessMessage();
        cleanUploadPanel();
        showUploadPanel();
    }


    function getExcelColumns(val, array) {
        var columns = [];
        $.each(array, function (index, value) {
            console.log(value.columns);
            if (value.name == val) {
                columns = value.columns;
            }
        })
        return columns;
    }

    function generate() {
        'use strict';

        /*Excel file structure*/
        var excelStructure = 
            [
                { name: "Docente", columns: [{ column1: "NOMBRE DE USUARIO" }, {column2: "NOMBRE DE USUARIO" }]}
            ]
            ;


        var uploadFileUrl = $('#uploadFileUrl').val();
        var url = uploadFileUrl,
            uploadButton = $(getUploadButton())
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $('#fileupload').fileupload({
            url: url,
            formData: getUploadFormData(),
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(xls)|(xlsx)$/i
        }).on('fileuploadadd', function (e, data) {     
            
            var file = data.files[0];
            var uploadType = $('#uploadType').val();
            var columns = getExcelColumns(uploadType, excelStructure);
            var flagValidColumns;

            cleanUploadPanel();

            if (!fileIsValid(file.name)) {
                $('#files').append('<p class="text-danger"><span class="fa fa-exclamation-circle"></span>&nbsp Error en el formato del archivo seleccionado.</p>');
                $('#buttonUploadExcel').attr('disabled', 'true');
                return;
            } else {
                $('#buttonUploadExcel').removeAttr("disabled");

            }
            
            readFile(file, columns).then(value => {
                if (!value) {
                    console.log(value);
                    $('#files').append('<p class="text-danger"><span class="fa fa-exclamation-circle"></span>&nbsp Error en el formato del archivo seleccionado.</p>');
                    $('#buttonUploadExcel').attr('disabled', 'true');
                    return;
                }
                else {
                    $('#buttonUploadExcel').removeAttr("disabled");
                }
            });


            $('#files')
                .append(
                    '<div class="well"><p><span class="fa fa-file-excel-o"></span>&nbsp;&nbsp;' +
                    file.name +
                    '&nbsp;&nbsp;/&nbsp;&nbsp;' +
                    (file.size / 1024).toFixed(2) +
                    '&nbsp;kB</p></div>')
                .append(uploadButton.clone(true).data(data));
            $('#failure-msg').fadeOut('fast');
            $('#failure-msg').empty();
        }).on('fileuploadprogressall', function (e, data) {
            $('#spinner').fadeIn('fast');
            $('#spinner-message').fadeIn('fast');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
            }).on('fileuploaddone', function (e, data) {
                $('#files').empty();
                $('#btn-add-file').fadeOut();
                showSuccessMessageWithState(data.result);
        }).on('fileuploadfail', function (e, data) {
            console.log(data);
            $('#spinner').fadeOut('fast');
            $('#spinner-message').fadeOut('fast');
            $('#files').empty();
            $('#files').append('<p class="text-danger"><span class="fa fa-fa-exclamation-circle"></span>&nbsp;Error al subir el archivo</p>')
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }

    function onFileProcessed() {
        alert('FUNCIONE');
    };

    function showSuccessMessage() {
        $('#success-msg').fadeIn('fast');
    }

    function showSuccessMessageWithState(state) {
        if (state.isError === false) {
            $('#success-msg').fadeIn('fast');
        }
        else {
            $('#failure-msg').empty();
            $('#failure-msg').append('<h5 class="text-danger"><span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;' + state.message + '<h5>');
            $('#failure-msg').fadeIn('fast');
            $('#btn-add-file').fadeIn();
            cleanUploadPanel();
        }
        $('#spinner').fadeOut('fast');
        $('#spinner-message').fadeOut('fast');
    }

    function hideSuccessMessage() {
        $('#success-msg').fadeOut('fast');
    }

    function cleanUploadPanel() {
        $('#btn-add-file').fadeIn('fast');
        $('#progress .progress-bar')
            .css('width', 0 + '%');
        $('#files').empty();
    }

    function showUploadPanel() {
        $('#fileUploadPanel').fadeIn('fast');
    }

    function hideUploadPanel() {
        $('#fileUploadPanel').fadeOut('fast');
    }

    function fileIsValid(name) {
        return '.xls' == name.substr(name.length - 4) || '.xlsx' == name.substr(name.length - 5);
    }

    /*Proccess Excel*/
    function ProcessExcel(data, columns) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
        //Read all rows from First Sheet into an JSON array0
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet], {
            header: 1
        })[0];

        //Validate number columns 

        var sizeColumns = columns.length;
        var sizeExcel = excelRows.length;

        console.log(excelRows.length);
        console.log(columns.length);
        console.log(sizeColumns == sizeExcel);
        return sizeColumns == sizeExcel;
    };


    function readFile(file,columns) {
        return new Promise((resolve, reject) => {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {

                    reader.onload = function (e) {
                        resolve(ProcessExcel(e.target.result, columns));
                    };
                    reader.readAsBinaryString(file);
                } else {
                    //For IE Browser.
                    flag = reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        resolve(ProcessExcel(data, columns));
                    };
                    console.log(flag)
                    reader.readAsArrayBuffer(file);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        })
    }



});