﻿$(function () {

    isNotUploadedCaseUpdate();

    $('#btnDownloadFile').click(function () {
        var downloadUrl = $('#downloadFileUrl').val();
        $.fileDownload(downloadUrl);
    });

    function cleanEverything() {
        hideSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function checkPeriodState(idTerm) {
        var uploadType = $('#uploadType').val();
        var urlUploadExists = $('#urlUploadExists').val();
        var JSONObject = { "idTerm": idTerm, "uploadType": uploadType }
        $.ajax({
            type: "POST",
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: urlUploadExists,
            success: function (json) {
                var isUploaded = json.isUploaded
                if (isUploaded) {
                    isUploadedCaseUpdate();
                } else {
                    isNotUploadedCaseUpdate();
                }
            }
        });
    }

    function getUploadButton() {
        return '<button class="btn btn-primary pull-right">' +
                   '<span class="fa fa-upload"/>&nbsp;&nbsp;Subir archivo' +
               '</button>';
    }

    function getUploadFormData() {
        var idTerm = '0'; // Not asociated to any term at all
        var uploadType = $('#uploadType').val();
        return JSONObject = { "idTerm": idTerm, "uploadType": uploadType }
    }

    function isUploadedCaseUpdate() {
        showSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function isNotUploadedCaseUpdate() {
        generate();
        hideSuccessMessage();
        cleanUploadPanel();
        showUploadPanel();
    }

    function generate() {
        'use strict';
        var uploadFileUrl = $('#uploadFileUrl').val();
        var url = uploadFileUrl,
            uploadButton = $(getUploadButton())
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $('#fileupload').fileupload({
            url: url,
            formData: getUploadFormData(),
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(xls)|(xlsx)$/i
        }).on('fileuploadadd', function (e, data) {
            var file = data.files[0];
            cleanUploadPanel();
            if (!fileIsValid(file.name)) {
                $('#files').append('<p class="text-danger"><span class="fa fa-exclamation-circle"></span>&nbsp Error en el formato del archivo seleccionado.</p>')
                return;
            }
            $('#files')
                .append(
                    '<div class="well"><p><span class="fa fa-file-excel-o"></span>&nbsp;&nbsp;' +
                    file.name +
                    '&nbsp;&nbsp;/&nbsp;&nbsp;' +
                    (file.size / 1024).toFixed(2) +
                    '&nbsp;kB</p></div>')
                .append(uploadButton.clone(true).data(data));
            $('#failure-msg').fadeOut('fast');
            $('#failure-msg').empty();
        }).on('fileuploadprogressall', function (e, data) {
            $('#spinner').fadeIn('fast');
            $('#spinner-message').fadeIn('fast');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }).on('fileuploaddone', function (e, data) {
            $('#files').empty();
            $('#btn-add-file').fadeOut();
            showSuccessMessageWithState(data.result);
        }).on('fileuploadfail', function (e, data) {
            $('#spinner').fadeOut('fast');
            $('#spinner-message').fadeOut('fast');
            $('#files').empty();
            $('#files').append('<p class="text-danger"><span class="fa fa-fa-exclamation-circle"></span>&nbsp;Error al subir el archivo</p>')
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }

    function showSuccessMessage() {
        $('#success-msg').fadeIn('fast');
        $('#spinner').fadeOut('fast');
        $('#spinner-message').fadeOut('fast');
    }

    function showSuccessMessageWithState(state) {
        if (state.isError === false) {
            $('#success-msg').fadeIn('fast');
        }
        else {
            $('#failure-msg').empty();
            $('#failure-msg').append('<h5 class="text-danger"><span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;' + state.message + '<h5>');
            $('#failure-msg').fadeIn('fast');
            $('#btn-add-file').fadeIn();
            cleanUploadPanel();
        }
        $('#spinner').fadeOut('fast');
        $('#spinner-message').fadeOut('fast');
    }

    function hideSuccessMessage() {
        $('#success-msg').fadeOut('fast');
    }

    function cleanUploadPanel() {
        $('#btn-add-file').fadeIn('fast');
        $('#progress .progress-bar')
            .css('width', 0 + '%');
        $('#files').empty();
    }

    function showUploadPanel() {
        $('#fileUploadPanel').fadeIn('fast');
    }

    function hideUploadPanel() {
        $('#fileUploadPanel').fadeOut('fast');
    }

    function fileIsValid(name) {
        return '.xls' == name.substr(name.length - 4) || '.xlsx' == name.substr(name.length - 5);
    }

});