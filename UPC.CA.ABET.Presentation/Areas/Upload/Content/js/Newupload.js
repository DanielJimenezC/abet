﻿$(function () {
    $(document).on("change",
        '#termDropDown',
        function (e) {
            var idTerm = $(this).val();
            if (idTerm == 0) {
                cleanEverything();
            } else {
                checkPeriodState(idTerm);
            }
        });



    $('#btnDownloadFile').click(function () {
        var downloadUrl = $('#downloadFileUrl').val();
        $.fileDownload(downloadUrl);
    });


    function cleanEverything() {
        hideSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function checkPeriodState(idTerm) {
        var uploadType = $('#uploadType').val();
        var urlUploadExists = $('#urlUploadExists').val();
        var JSONObject = { "idTerm": idTerm, "uploadType": uploadType }
        $.ajax({
            type: "POST",
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: urlUploadExists,
            success: function (json) {
                var isUploaded = json.isUploaded
                if (isUploaded) {
                    isUploadedCaseUpdate();
                } else {
                    isNotUploadedCaseUpdate();
                }
            }
        });
    }

    function getUploadButton() {
        return '<button class="btn btn-primary pull-right">' +
            '<span class="fa fa-upload"/>&nbsp;&nbsp;Subir archivo' +
            '</button>';
    }

    function getUploadFormData() {
        var idTerm = $('#termDropDown').val();
        var idModal = $('#modalDropDown').val();
        var idSubModal = $('#subModalDropDown').val();
        var uploadType = $('#uploadType').val();
        return JSONObject = { "idTerm": idTerm, "idModal": idModal, "idSubModal": idSubModal, "uploadType": uploadType };
    }

    function isUploadedCaseUpdate() {
        showSuccessMessage();
        cleanUploadPanel();
        hideUploadPanel();
    }

    function isNotUploadedCaseUpdate() {
        generate();
        hideSuccessMessage();
        cleanUploadPanel();
        showUploadPanel();
    }

    function generate() {
        'use strict';
        var uploadFileUrl = $('#uploadFileUrl').val();
        var url = uploadFileUrl,
            uploadButton = $(getUploadButton())
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
        $('#fileupload').fileupload({
            url: url,
            formData: getUploadFormData(),
            dataType: 'json',
            autoUpload: false,
            acceptFileTypes: /(xls)|(xlsx)$/i
        }).on('fileuploadadd', function (e, data) {

            var file = data.files[0];


            cleanUploadPanel();
            if (!fileIsValid(file.name)) {
                console.log("here!");
                $('#files').append('<p class="text-danger"><span class="fa fa-exclamation-circle"></span>&nbsp Error en el formato del archivo seleccionado.</p>');

                return;
            }

            $('#files')
                .append(
                    '<div class="well"><p><span class="fa fa-file-excel-o"></span>&nbsp;&nbsp;' +
                    file.name +
                    '&nbsp;&nbsp;/&nbsp;&nbsp;' +
                    (file.size / 1024).toFixed(2) +
                    '&nbsp;kB</p></div>')
                .append(uploadButton.clone(true).data(data));
        }).on('fileuploadprogressall', function (e, data) {
            $('#spinner').fadeIn('fast');
            $('#spinner-message').fadeIn('fast');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }).on('fileuploaddone', function (e, data) {
            $('#files').empty();
            $('#btn-add-file').fadeOut(); 

            //var downloadExcel = '<%=Session["name"]%>';
            //console.log(downloadExcel);
            //if (downloadExcel) {
            //    //document.location.href = '/Upload/DataUploadBase/SaveExcel';
            //    //    /*'/Upload/AfterEnrollment/SaveExcel';*/

            //    //$.fileDownload('/ABETSistemasPruebas/Upload/DataUploadBase/SaveExcel');
            //    $.fileDownload('/Upload/DataUploadBase/SaveExcel');

            //    var state = { isError: true, message: "Hubo un error en su Excel. Por favor revise los errores en el Excel generado." };
            //    showSuccessMessageWithState(state);
            //}
            //else {
            if (data.result.message === "Hubo un error en su Excel. Por favor revise los errores en el Excel generado.") {
                //$.fileDownload('/Upload/DataUploadBase/SaveExcel');
                $.fileDownload('/ABETSistemasPruebas/Upload/DataUploadBase/SaveExcel');
            }

            showSuccessMessageWithState(data.result);

          
            
        }).on('fileuploadfail', function (e, data) {
            console.log(data);
            $('#spinner').fadeOut('fast');
            $('#spinner-message').fadeOut('fast');
            $('#files').empty();
            $('#files').append('<p class="text-danger"><span class="fa fa-fa-exclamation-circle"></span>&nbsp;Error al subir el archivo</p>')
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }

    function onFileProcessed() {
        alert('FUNCIONE');
    };

    function showSuccessMessage() {
        $('#success-msg').fadeIn('fast');
    }

    function showSuccessMessageWithState(state) {
        if (state.isError === false) {
            $('#success-msg').fadeIn('fast');
        }
        else {
            $('#failure-msg').empty();
            $('#failure-msg').append('<h5 class="text-danger"><span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;' + state.message + '<h5>');
            $('#failure-msg').fadeIn('fast');
        }
        $('#spinner').fadeOut('fast');
        $('#spinner-message').fadeOut('fast');
    }

    function hideSuccessMessage() {
        $('#success-msg').fadeOut('fast');
    }

    function cleanUploadPanel() {
        $('#btn-add-file').fadeIn('fast');
        $('#progress .progress-bar')
            .css('width', 0 + '%');
        $('#files').empty();
    }

    function showUploadPanel() {
        $('#fileUploadPanel').fadeIn('fast');
    }

    function hideUploadPanel() {
        $('#fileUploadPanel').fadeOut('fast');
    }

    function fileIsValid(name) {
        return '.xls' == name.substr(name.length - 4) || '.xlsx' == name.substr(name.length - 5);
    }

});