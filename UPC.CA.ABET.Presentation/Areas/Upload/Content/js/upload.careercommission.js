﻿$(function () {
    $(document).on('change', '#termDropDown', function (e) {

        var termId = $(this).val();
        lockComponentsWhenNewTermIsSelected(termId);
        showSelectMessage();
        hideEliminationErrorMessage();
        hideSuccessMessage();

        if (termId == 0) {
            return;
        }

        getCareers(termId);
        getCareersCommissions(termId);
    });

    function lockComponentsWhenNewTermIsSelected(termId) {
        $('#careerDropDown').empty();
        $('#commissionDropDown').empty();
        $('#careerDropDown').prop('disabled', termId == 0);
        $('#commissionDropDown').prop('disabled', true);
        $('#btnAsociate').prop('disabled', true);
    }

    function lockComponentsWhenNewCareerIsSelected(careerId) {
        $('#commissionDropDown').empty();
        $('#commissionDropDown').prop('disabled', careerId == 0);
        $('#btnAsociate').prop('disabled', true);
    }

    function getCareers(termId) {

        var JSONObject = { 'termId': termId }
        var getCareersUrl = $('#GetCareers').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCareersUrl,
            success: function (json) {
                var carreras = json.carreras
                $.each(carreras, function (index, element) {
                    $('#careerDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
                });
            }
        });
    }

    function getCareersCommissions(termId) {
        var JSONObject = { 'termId': termId }
        var getCareersCommissionsUrl = $('#GetCareersCommissions').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCareersCommissionsUrl,
            success: function (json) {
                presentCareersCommissions(json.careerscommissions);
            }
        });
    }

    function presentCareersCommissions(careerscommissions) {
        if (careerscommissions.length > 0) {
            $('#asociationsTable > tbody').empty();
            $.each(careerscommissions, function (index, element) {
                $('#asociationsTable > tbody').append('<tr>' +
                                '<td class="text-center">' + element.Career + '</td>' +
                                '<td class="text-center">' + element.Commission + '</td>' +
                                '<td class="text-center">' + element.Term + '</td>' +
                                '<td class="text-center">' +
                                '<button id="' + element.Id + '" class="btn btn-primary btn-delete"><i class="fa fa-trash"></i></button>' +
                                '</td>' +
                            '</tr>')
            });
            $('#careersCommissionsSelectMessage').fadeOut('fast');
            $('#careersCommissionsEmptyMessage').fadeOut('fast');
            $('#asociationsTable').fadeIn('fast');
        } else {
            showEmptyMessage();
            hideEliminationErrorMessage();
            hideSuccessMessage();
        }
    }

    function showSelectMessage() {
        $('#careersCommissionsSelectMessage').fadeIn('fast');
        $('#asociationsTable').fadeOut('fast');
        $('#careersCommissionsEmptyMessage').fadeOut('fast');
    }

    function showEmptyMessage() {
        $('#careersCommissionsEmptyMessage').fadeIn('fast');
        $('#asociationsTable').fadeOut('fast');
        $('#careersCommissionsSelectMessage').fadeOut('fast');
    }

    $(document).on('change', '#careerDropDown', function (e) {

        var termId = $('#termDropDown').val();
        var careerId = $(this).val();
        var JSONObject = { 'termId': termId, 'careerId': careerId }
        var getCommissionsUrl = $('#GetCommissions').val();

        lockComponentsWhenNewCareerIsSelected(careerId);

        if (termId == 0 || careerId == 0) {
            return;
        }

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: getCommissionsUrl,
            success: function (json) {
                var commissions = json.commissions
                $.each(commissions, function (index, element) {
                    $('#commissionDropDown').append('<option value="' + element.Value + '">' + element.Text + '</option>')
                });
            }
        });
    });

    $(document).on('change', '#commissionDropDown', function (e) {

        var commissionId = $(this).val();

        if (commissionId == 0) {
            $('#btnAsociate').prop('disabled', true);
            return;
        }

        $('#btnAsociate').prop('disabled', false);

    });

    $('#btnAsociate').click(function () {
        var termId = $('#termDropDown').val();
        var careerId = $('#careerDropDown').val();
        var commissionId = $('#commissionDropDown').val();
        var JSONObject = { 'termId': termId, 'careerId': careerId, 'commissionId': commissionId }
        var asociateCareerAndCommissionUrl = $('#AsociateCareerAndCommission').val();

        if (termId == 0 || careerId == 0 || commissionId == 0) {
            return;
        }

        $('#btnAsociate').prop('disabled', true);

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: asociateCareerAndCommissionUrl,
            success: function (json) {
                $('#careerDropDown')[0].selectedIndex = 0;
                lockComponentsWhenNewCareerIsSelected(0);
                getCareersCommissions($('#termDropDown').val());
            }
        });
    });

    $(document).on('click', '.btn-delete', function () {

        hideEliminationErrorMessage();
        var JSONObject = { 'id': $(this).attr('id') };
        var deleteCareerCommissionUrl = $('#DeleteCareerCommission').val();

        $.ajax({
            type: 'POST',
            data: JSON.stringify(JSONObject),
            contentType: 'application/json',
            url: deleteCareerCommissionUrl,
            success: function (json) {
                $('#careerDropDown')[0].selectedIndex = 0;
                lockComponentsWhenNewCareerIsSelected(0);
                getCareersCommissions($('#termDropDown').val());

                if (json.isError) {
                    hideSuccessMessage();
                    showEliminationErrorMessage(json.message);
                } else {
                    hideEliminationErrorMessage();
                }
            }
        });
    });

    function showEliminationErrorMessage(message) {
        $('#eliminationErrorMessage').empty();
        $('#eliminationErrorMessage').append(message);
        $('#eliminationErrorMessage').fadeIn('fast');
    }

    function hideEliminationErrorMessage() {
        $('#eliminationErrorMessage').fadeOut('fast');
    }

    function showSuccessMessage() {
        $('#success-msg').fadeIn('fast');
    }

    function hideSuccessMessage() {
        $('#success-msg').fadeOut('fast');
    }
});