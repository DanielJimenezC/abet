﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Files
{
    public class PathMapper
    {
        public static string getFullPath(string uploadType)
        {
            return System.Web.Hosting.HostingEnvironment.MapPath(
                ConstantHelpers.UPLOAD.FILE_DOWNLOAD.PATH_TEMPLATES + 
                getFileName(uploadType));
        }

        public static string getFileName(string uploadType)
        {
            switch (uploadType)
            {
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_MATRICULADOS:
                    return ConstantHelpers.UPLOAD_FILE_NAME.ALUMNOS_MATRICULADOS;
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_POR_SECCION:
                    return ConstantHelpers.UPLOAD_FILE_NAME.ALUMNOS_POR_SECCION;
                case ConstantHelpers.UPLOAD_TYPE.ASISTENCIA_DOCENTES:
                    return ConstantHelpers.UPLOAD_FILE_NAME.ASISTENCIA_DOCENTES;
                case ConstantHelpers.UPLOAD_TYPE.ATTRITION:
                    return ConstantHelpers.UPLOAD_FILE_NAME.ATTRITION;
                case ConstantHelpers.UPLOAD_TYPE.COHORTE:
                    return ConstantHelpers.UPLOAD_FILE_NAME.COHORTE;
                case ConstantHelpers.UPLOAD_TYPE.COHORTE_INGRESANTES:
                    return ConstantHelpers.UPLOAD_FILE_NAME.COHORTE_INGRESANTES;
                case ConstantHelpers.UPLOAD_TYPE.DELEGADOS:
                    return ConstantHelpers.UPLOAD_FILE_NAME.DELEGADOS;
                case ConstantHelpers.UPLOAD_TYPE.DOCENTE:
                    return ConstantHelpers.UPLOAD_FILE_NAME.DOCENTE;
                case ConstantHelpers.UPLOAD_TYPE.EVALUACIONES:
                    return ConstantHelpers.UPLOAD_FILE_NAME.EVALUACIONES;
                case ConstantHelpers.UPLOAD_TYPE.MALLA_CURRICULAR:
                    return ConstantHelpers.UPLOAD_FILE_NAME.MALLA_CURRICULAR;
                case ConstantHelpers.UPLOAD_TYPE.ORGANIGRAMA:
                    return ConstantHelpers.UPLOAD_FILE_NAME.ORGANIGRAMA;
                case ConstantHelpers.UPLOAD_TYPE.OUTCOME:
                    return ConstantHelpers.UPLOAD_FILE_NAME.OUTCOME;
                case ConstantHelpers.UPLOAD_TYPE.PUNTUALIDAD_DOCENTES:
                    return ConstantHelpers.UPLOAD_FILE_NAME.PUNTUALIDAD_DOCENTES;
                case ConstantHelpers.UPLOAD_TYPE.SECCION:
                    return ConstantHelpers.UPLOAD_FILE_NAME.SECCION;
                case ConstantHelpers.UPLOAD_TYPE.TITULADOS:
                    return ConstantHelpers.UPLOAD_FILE_NAME.TITULADOS;
                case "Error":
                    return "Error.xlsx";           
                default:
                    throw new Exception("Tipo de carga no soportado");
            }
        }
    }
}