﻿#region Imports

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Rubric.Resources.Views.BulkInsert;

#endregion

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.BeforeEnrollment
{
    public class UploadExcelFilesViewModel
    {
        #region Propiedades

        [Required]
        public HttpPostedFileBase Archivo { get; set; }
        public BulkInsertType TipoCargaMasiva { get; set; }
        public String TituloPagina { get; set; }
        public String Icono { get; set; }
        public Dictionary<String, String> DicMensajes { get; set; }



        /*ROLES DISPONIBLES */
        public IEnumerable<TipoEvaluadorMaestra> lstTipoEvaluadorMaestra { get; set; }

        #endregion

        #region Metodos

        /// <summary>
        /// Carga los datos de la página correspondientes al tipo de carga masiva los cuales 
        /// son determinados según el tipo de carga masiva elegida en el sidebar principal,
        /// como también los mensajes de validaciones correspondientes almacenados en un 
        /// tipo de dato diccionario.
        /// </summary>
        /// <param name="TipoCargaMasiva">Enumerador que determina el tipo de carga masiva</param>
        /// <returns></returns>
        public void CargarDatos(BulkInsertType TipoCargaMasiva)
        {
            this.TipoCargaMasiva = TipoCargaMasiva;
            DicMensajes = new Dictionary<String, String>();
            DicMensajes.Add("HeadersInvalidos", UploadFilesResource.HeadersInvalidos);

            TituloPagina = "Carga de Notas Calificadas de RV";
            Icono = "plus-book";
        }

        #endregion
    }
}