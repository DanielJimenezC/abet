﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload
{
    public class CurriculumAcademicPeriodViewModel: BaseUploadViewModel
    {   
        public string GetCurriculumsUrl { get; set; }
        public string GetAsociatedCurriculumsAndAcademicPeriodUrl { get; set; }
        public string AsociateCurriculumAndAcademicPeriodUrl { get; set; }
        public string DeleteCurriculumTermUrl { get; set; }
        public string TermsListUrl { get; set; }
        public string TermsListUrlRegular { get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }
        public IEnumerable<SelectListItem> Terms 
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
};
            }
        }
        public IEnumerable<SelectListItem> TermsRegular
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
    }
}