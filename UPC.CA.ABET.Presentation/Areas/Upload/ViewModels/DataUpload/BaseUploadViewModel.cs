﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;
using UPC.CA.ABET.Presentation.Areas.Upload;
using UPC.CA.ABET.Logic.Areas.Upload;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload
{
    public class BaseUploadViewModel
    {
        public string UploadType { get; set; }
        public string UploadExistsUrl { get; set; }
        public string UploadFileUrl { get; set; }
        public string DownloadFileUrl { get; set; }
        public string SubModalitiesListUrl { get; set; }
        public string TermsListUrl { get; set; }
        public string TermsListUrlRegular { get; set; }
        public List<SelectListItem> Modal { get; set; }
       
        public IEnumerable<SelectListItem> Terms
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
        public IEnumerable<SelectListItem> TermsRegular
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
        public IEnumerable<SelectListItem> SubModal
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
        
    }
}