﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload
{
    public class AsociatedCareerCommissionViewModel
    {
        public int Id { get; set; }
        public string Career { get; set; }
        public string Commission { get; set; }
        public string Term { get; set; }
    }
}