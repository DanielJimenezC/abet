﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload
{
    public class CareerCommissionViewModel
    {
        public string GetCareersUrl { get; set; }
        public string GetCommissionsUrl { get; set; }
        public string GetCareersCommissionsUrl { get; set; }
        public string AsociateCareerAndCommissionUrl { get; set; }
        public string DeleteCareerAndCommissionUrl { get; set; }
        public string TermsListUrl { get; set; }
        public string TermsListUrlRegular { get; set; }
        public IEnumerable<SelectListItem> Modal { get; set; }
        public IEnumerable<SelectListItem> Terms
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
        public IEnumerable<SelectListItem> TermsRegular
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "-- Seleccione --",
                        Value = "0"
                    }
                };
            }
        }
    }
}