﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common
{
    public class UploadMaintenanceViewModel
    {
        public List<UploadViewModel> Uploads { get; set; }
        public MessageViewModel Message { get; set; }

        public UploadMaintenanceViewModel()
        {
            Uploads = new List<UploadViewModel>();
            Message = null;
        }
    }

    public class UploadViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string User { get; set; }
        public string Modalidad { get; set; }
        public int idmodalidad { get; set; }
    }

    public class MessageViewModel
    {
        public bool Success { get; set; }
        public string Content { get; set; }

        public static MessageViewModel WithSuccess(string message)
        {
            return new MessageViewModel
            {
                Success = true,
                Content = message
            };
        }

        public static MessageViewModel WithError(string message)
        {
            return new MessageViewModel
            {
                Success = false,
                Content = message
            };
        }

       
    }
}