﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common
{
    public class CareerViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}