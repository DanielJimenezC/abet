﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common
{
    public class AcademicPeriodViewModel
    {
        public int AcademicPeriodId { get; set; }
        public string Term { get; set; }
    }
}