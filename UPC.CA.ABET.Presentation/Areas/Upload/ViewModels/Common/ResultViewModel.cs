﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common
{
    public class ResultViewModel
    {
        public string message { get; set; }
        public bool isError { get; set; }

        public ResultViewModel()
        {
            isError = false;
        }
    }
}