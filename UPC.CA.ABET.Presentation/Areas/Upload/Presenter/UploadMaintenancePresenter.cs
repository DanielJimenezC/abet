﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Presenter
{
    public class UploadMaintenancePresenter
    {
        AbetEntities ab = new AbetEntities();
        public UploadMaintenanceViewModel GetViewModel(List<LogCarga> uploads, MessageViewModel message)
        {


            return new UploadMaintenanceViewModel
            {
                Uploads = uploads.Select(x => new UploadViewModel
                {
                    Id = x.IdLogCarga,
                    Date = x.FechaCarga.ToString(),
                    Type = x.LogTipoCarga.Nombre,
                    //User = x.Usuario.NombreCompleto
                    //Modalidad = x.IdSubModalidadPeriodoAcademico.ToString(),
                     Modalidad = x.IdSubModalidadPeriodoAcademico.ToString(), 
           
                    //Modalidad = Convert.ToString( ab.SubModalidadPeriodoAcademico.Where(y => y.IdSubModalidadPeriodoAcademico == x.IdSubModalidadPeriodoAcademico && x.IdSubModalidadPeriodoAcademico>0).FirstOrDefault().SubModalidad.Modalidad.NombreEspanol),

                    User = "Test"
                }).ToList(),
                Message = message
            };
        }
    }
}