﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert;
using UPC.CA.ABET.Presentation.Areas.Upload.Resources.Views.BeforeEnrollment;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.BeforeEnrollment;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelReports;
using System.Data.Entity.Infrastructure;
using UPC.CA.ABET.Logic.Areas.Upload;
using UPC.CA.ABET.Presentation.Filters;
using ClosedXML.Excel;
using System.Net;
using System.Net.Mime;
using Newtonsoft.Json;
using System.Threading.Tasks;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class BeforeEnrollmentController : DataUploadBaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OrganizationChart()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.ORGANIGRAMA));
        }

        public ActionResult Outcomes()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.OUTCOME));
        }

        public ActionResult Curriculum()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.MALLA_CURRICULAR));
        }

        public ActionResult CurriculumAcademicPeriod()
        {
			int parModalidadId = Session.GetModalidadId();


			

			

			var viewModel = new CurriculumAcademicPeriodViewModel
            {
                GetCurriculumsUrl = Url.AbsoluteAction("GetCurriculums", "BeforeEnrollment"),
                AsociateCurriculumAndAcademicPeriodUrl = Url.AbsoluteAction("AsociateCurriculumAndAcademicPeriod", "BeforeEnrollment"),
                GetAsociatedCurriculumsAndAcademicPeriodUrl = Url.AbsoluteAction("GetCurriculumAcademicPeriod", "BeforeEnrollment"),
                DeleteCurriculumTermUrl = Url.AbsoluteAction("DeleteCurriculumTerm", "BeforeEnrollment"),
                Modal = GetModelSelectList(),
                TermsListUrl = GetTermsListUrl,
				TermsListUrlRegular = GetTermsListUrlRegular
				//Terms = (from a in context.SubModalidadPeriodoAcademico
				//		 join b in context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
				//		 join c in context.PeriodoAcademico on a.IdPeriodoAcademico equals c.IdPeriodoAcademico
				//		 where b.IdModalidad == parModalidadId
				//		 select c
				//				   ).Select(x => new SelectListItem()
				//				   {
				//					   Value = x.IdPeriodoAcademico.ToString(),
				//					   Text = x.CicloAcademico
				//				   })

			};
            return View(viewModel);
        }

        public ActionResult CareerCommission()
        {
            var viewModel = new CareerCommissionViewModel
            {
                GetCareersUrl = Url.AbsoluteAction("GetCareers", "BeforeEnrollment"),
                Modal = GetModelSelectList(),
                TermsListUrl = GetTermsListUrl,
                TermsListUrlRegular = GetTermsListUrlRegular,
                //Terms = context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.NombreEspanol == "Regular" && x.SubModalidad.Modalidad.NombreEspanol == "Pregrado Regular").Select(x =>
                //                       new SelectListItem()
                //                       {
                //                           Value = x.IdPeriodoAcademico.ToString(),
                //                           Text = x.PeriodoAcademico.CicloAcademico
                //                       }),
                GetCommissionsUrl = Url.AbsoluteAction("GetCommissions", "BeforeEnrollment"),
                GetCareersCommissionsUrl = Url.AbsoluteAction("GetCareersCommissions", "BeforeEnrollment"),
                AsociateCareerAndCommissionUrl = Url.AbsoluteAction("AsociateCareerAndCommission", "BeforeEnrollment"),
                DeleteCareerAndCommissionUrl = Url.AbsoluteAction("DeleteCareerCommission", "BeforeEnrollment")
            };
            return View(viewModel);
        }


        #region GET_CAREERS
        [HttpPost]
        public ActionResult GetCareers(string termId)
        {
            return Json(new { carreras = GetCareersSelectList(termId) });
        }

        public IEnumerable<SelectListItem> GetCareersSelectList(string termId) {
            var result = new List<SelectListItem>();
            populateWithCareers(result, termId);
            return result;
        }

        private void populateWithCareers(List<SelectListItem> list, string termId)
        {
            AddNewDefaultItem(list);
            foreach (var item in uploadLogic.GetCareers(termId))
            {
                AddNewItem(list, item.NombreEspanol, item.IdCarrera.ToSafeString());
            }
        }
        #endregion

        #region GET_COMMISSIONS
        [HttpPost]
        public ActionResult GetCommissions(string termId, string careerId)
        {
            return Json(new { commissions = GetComissionsSelectList(termId, careerId) });
        }

        public IEnumerable<SelectListItem> GetComissionsSelectList(string termId, string careerId)
        {
            var result = new List<SelectListItem>();
            populateWithComissions(result, termId, careerId);
            return result;
        }

        private void populateWithComissions(List<SelectListItem> list, string termId, string careerId)
        {
            AddNewDefaultItem(list);
            foreach (var item in uploadLogic.GetCommissions(termId, careerId))
            {
                AddNewItem(list, item.NombreEspanol, item.IdComision.ToSafeString());
            }
        }
        #endregion

        #region GET_CURRICULUMS
        [HttpPost]
        public ActionResult GetCurriculums(int termId)
        {
            return Json(new { curriculums = GetCurriculumsSelectList(termId) });
        }

        public IEnumerable<SelectListItem> GetCurriculumsSelectList(int termId)
        {
            var result = new List<SelectListItem>();
            populateWithCurriculums(result, termId);
            return result;
        }

        private void populateWithCurriculums(List<SelectListItem> list, int termId)
        {
            int escuelaId = Session.GetEscuelaId();
            AddNewDefaultItem(list);
            foreach (var item in uploadLogic.GetCurriculums(termId, escuelaId))
            {
                AddNewItem(list, item.Codigo + " - " + item.Carrera.NombreEspanol, item.IdMallaCurricular.ToSafeString());
            }
        }
        #endregion

        #region GET_CAREERS_COMMISSIONS
        [HttpPost]
        public ActionResult GetCareersCommissions(string termId)
        {
            return Json(new { careerscommissions = GetCareersCommissionsSelectList(termId) });
        }

        public List<AsociatedCareerCommissionViewModel> GetCareersCommissionsSelectList(string termId)
        {
            var result = new List<AsociatedCareerCommissionViewModel>();
            populateWithCareersCommissions(result, termId);
            return result;
        }

        private void populateWithCareersCommissions(List<AsociatedCareerCommissionViewModel> list, string termId)
        {
            //int escuelaId = Session.GetEscuelaId();
            //foreach (var item in uploadLogic.GetCareerCommissions(termId, escuelaId))
            //{
            //    var careerCommission = new AsociatedCareerCommissionViewModel
            //    {
            //        Id = item.IdCarreraComision,
            //        Career = item.Carrera.NombreEspanol,
            //        Commission = item.Comision.Codigo,
            //        Term = item.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico
            //    };
            //    list.Add(careerCommission);
            //}
            foreach (var item in uploadLogic.GetCareerCommissions(termId))
            {
                var careerCommission = new AsociatedCareerCommissionViewModel
                {
                    Id = item.IdCarreraComision,
                    Career = item.Carrera.NombreEspanol,
                    Commission = item.Comision.Codigo,
                    Term = item.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico
                };
                list.Add(careerCommission);
            }
        }
        #endregion

        [HttpPost]
        public ActionResult AsociateCareerAndCommission(int termId, int careerId, int commissionId)
        {
           
         
            uploadLogic.AsociateCareerAndCommission(termId, careerId, commissionId);
            return Json(new { message = MessageResource.CarrerayComisionAsociadas});
        }

        [HttpPost]
        public ActionResult AsociateCurriculumAndAcademicPeriod(int termId, int curriculumId)
        {
            int idUsuario = SessionHelper.GetUsuarioId(Session).GetValueOrDefault();
            var asociator = new CurriculumAsociator(Context);
            asociator.AsociateCurriculumAndAcademicPeriod(termId, curriculumId, idUsuario);
            return Json(new { message = MessageResource.MallayPeriodoAcademicoAsociados });
        }

        [HttpPost]
        public ActionResult GetCurriculumAcademicPeriod(int termId)
        {
            return Json(new { curriculumsAcademicPeriod = GetCurriculumAcademicPeriodSelectList(termId) });
        }

        public List<AsociatedCurriculumAcademicPeriodViewModel> GetCurriculumAcademicPeriodSelectList(int termId)
        {
            var result = new List<AsociatedCurriculumAcademicPeriodViewModel>();
            populateWithCareersCommissions(result, termId);
            return result;
        }

        private void populateWithCareersCommissions(List<AsociatedCurriculumAcademicPeriodViewModel> list, int termId)
        {
            int escueldaId = Session.GetEscuelaId();
            foreach (var item in uploadLogic.GetCurriculumAcademicPeriod(termId,escueldaId))
            {
                var curriculumAcademicPeriod = new AsociatedCurriculumAcademicPeriodViewModel
                {
                    Id = item.IdMallaCurricularPeriodoAcademico,
                    Curriculum = item.MallaCurricular.Codigo,
                    Career = item.MallaCurricular.Carrera.NombreEspanol,
                    Term = item.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico
                };
                list.Add(curriculumAcademicPeriod);
            }
        }
        
        [HttpPost]
        public ActionResult DeleteCareerCommission(string id)
        {
           try
           {
              uploadLogic.DeleteCareerCommission(id);
           }
           catch(DbUpdateException)
           {
             return GetFormattedErrorResult(MessageResource.LaAsociacionNoSePudoEliminarPorqueEstaSiendoUtilizada);
           }
           return GetFormattedResult(MessageResource.AsociacionDeCarrerayComisionEliminada);
        }

        [HttpPost]
        public ActionResult DeleteCurriculumTerm(int id)
        {
            try
            {
                var deleter = new CurriculumAsociationDeleter(Context);
                deleter.DeleteAsociation(id);
            }
            catch (Exception)
            {
                return GetFormattedErrorResult(MessageResource.LaAsociacionNoSePudoEliminarPorqueEstaSiendoUtilizada);
            }
            return GetFormattedResult(MessageResource.AsociacionDeCarrerayComisionEliminada);
        }
        public ActionResult UploadExcelFiles(BulkInsertType Type)
        {
            var viewModel = new UploadExcelFilesViewModel();
            viewModel.CargarDatos(Type);

            if (Type == BulkInsertType.EvaluatorType)
            {
                viewModel.lstTipoEvaluadorMaestra = Context.TipoEvaluadorMaestra.ToList();
            }

            
            return View(viewModel);
        }

        [HttpGet]
        public virtual ActionResult DownloadRv(string fileName)
        {
            return File(
              GetFileError(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + fileName),
              System.Net.Mime.MediaTypeNames.Application.Octet,
              "ReporteOutput.xlsx");
        }

        byte[] GetFileError(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        [AsyncTimeout(3600000)]
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase FileUpload)
        {
            string fullPathUpload = "";
            string Resultado = "";
            string MensajeResultado = "";
            try
            {

                var basePath = AppSettingsHelper.GetAppSettings("TempPath");
                var fileNameUpload = "Excel_Carga_RV" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
                fullPathUpload = Path.Combine(basePath, fileNameUpload);
                FileUpload.SaveAs(fullPathUpload);

                string handle = Guid.NewGuid().ToString();
                string fileNameOutput = "ReporteOutput" + handle + ".xlsx";
                string fullPathOutput = ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + fileNameOutput;
                int IdSubModalidadPeriodoAcademico = Session.GetSubModalidadPeriodoAcademicoId().Value;
                bool EsMallaArizona = Context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).FirstOrDefault().EsMallaArizona;

                Resultado = BulkInsertLogic.UploadRVToSQL(Context, fullPathUpload, fullPathOutput,EsMallaArizona);

                
                MensajeResultado = "Mensaje de sp: " + Resultado + " y Path: " + fullPathUpload + " Salida: " + fullPathOutput;

                if (Resultado == "completado")
                {
                    return new JsonResult()
                    {
                        Data = new { FileName = fileNameOutput, estado = "cargado", mensaje = MensajeResultado }
                    };
                }
                else
                {
                    return new JsonResult()
                    {
                        Data = new { estado = "sincargar", mensaje = MensajeResultado }
                    };
                }
            }
            catch (Exception ex)
            {
                return new JsonResult()
                {
                    Data = new { estado = "error", mensaje = "Ocurrio un error en el controlador: " + ex.Message + " " + MensajeResultado }
                };
            }
            //finally
            //{
            //    if (System.IO.File.Exists(fullPath))
            //    {
            //        System.IO.File.Delete(fullPath);
            //    }
            //}

        }
    }


}