﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class EndOfTermController : DataUploadBaseController
    {
        // GET: Upload/EndOfTerm
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Attrition()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.ATTRITION));
        }
        public ActionResult Cohort()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.COHORTE));
        }
        public ActionResult ProfessorsAttendance()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.ASISTENCIA_DOCENTES));
        }
        public ActionResult Grades()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.EVALUACIONES));
        }

        public ActionResult CohortIndicator()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.COHORTE_INGRESANTES));
        }

        public ActionResult ProfessorPunctuality()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.PUNTUALIDAD_DOCENTES));
        }

        public ActionResult CertificatedStudents()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.TITULADOS));
        }
    }
}