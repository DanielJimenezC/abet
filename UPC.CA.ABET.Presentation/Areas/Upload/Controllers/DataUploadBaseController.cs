﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor;
using UPC.CA.ABET.Logic.Areas.Upload;
using UPC.CA.ABET.Helpers;
using System.Data;
using UPC.CA.ABET.Presentation.Areas.Upload.Files;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload;
using UPC.CA.ABET.Presentation.Filters;
using ClosedXML.Excel;
using System.Net.Mime;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class DataUploadBaseController : BaseController
    {

        protected UploadLogic uploadLogic;

        public DataUploadBaseController()
        {
            uploadLogic = new UploadLogic(CargarDatosContext().context);
        }

        [HttpPost]
        public JsonResult UploadFile(string idModal, string idSubModal, string idTerm, string uploadType)
        {

            var file = Request.Files[0];
            if (FileIsValid(file))
            {
                try
                {
                    idTerm = session.GetSubModalidadPeriodoAcademicoId().ToString();
                    ProcessFile(file, idTerm, idModal, idSubModal, uploadType);
                 
                }


                catch (Exception ex)
                {
                    return GetFormattedErrorResult(ex.Message.ToString());
                }
            }
            return GetFormattedResult("");
        }

        [HttpGet]
        public ActionResult SaveExcel()
        {
           

            var descargado = Session["DownloadExcelError"].ToBoolean();

            if (!descargado)
            {
                string[] dirs = Directory.GetFiles(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS);
                int cantidad = dirs.Length;

                string arch = "Error" + cantidad + ".xlsx";

                Session["DownloadExcelError"] = true;

                return File(
                  GetFileError(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + arch),
                  System.Net.Mime.MediaTypeNames.Application.Octet,
                  "Error.xlsx");
            }
            else
            {
                return null;
            }

           
        }

        byte[] GetFileError(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }


       
        private bool FileIsValid(HttpPostedFileBase file)
        {
            
            return Request.Files[0] != null && file.ContentLength > 0;
        }

        private void ProcessFile(HttpPostedFileBase file, string idTerm,string idModal,string idSubModal, string uploadType)
        {
            var idUsuario = Session.GetUsuarioId();
            var fileName = uploadType + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            var fullPath = Path.Combine(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS, fileName);

            file.SaveAs(fullPath);


            switch (uploadType)
            {
                case ConstantHelpers.UPLOAD_TYPE.OUTCOME:
                    outcomesUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.DOCENTE:
                    professorsUpload(fullPath, idTerm, idModal, idSubModal, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.MALLA_CURRICULAR:
                    curriculumUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.ORGANIGRAMA:
                    organizationChartUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_MATRICULADOS:
                    enrolledStudentsUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.SECCION:
                    classesUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_POR_SECCION:
                    studentsPerClassUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.DELEGADOS:
                    delegatesUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.ATTRITION:
                    attritionUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.ASISTENCIA_DOCENTES:
                    professorAttendanceUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.COHORTE:
                    cohortUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.EVALUACIONES:
                    gradesUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.COHORTE_INGRESANTES:
                    cohortIndicatorUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.PUNTUALIDAD_DOCENTES:
                    professorPunctualityUpload(fullPath, idTerm, idUsuario);
                    break;
                case ConstantHelpers.UPLOAD_TYPE.TITULADOS:
                    certificatedStudentsUpload(fullPath, idTerm, idUsuario);
                    break;
                default:
                    throw new Exception("El tipo de carga " + uploadType + " no existe");
            }
        }

        private void classesUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetClassesReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void professorsUpload(string fullPath, string idTerm,string idModal,string idSubModal, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetProfessorReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void outcomesUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetOutcomeReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void curriculumUpload(string fullPath, string idTerm, int? idUsuario)
        {

            //idTerm = "105";

            var factory = new DataProcessorFactory();
            var reader = factory.GetCurriculumReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void organizationChartUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetOrganizationChartReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void enrolledStudentsUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetEnrolledStudensReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void delegatesUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetDelegatesReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void studentsPerClassUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetStudentsPerClassReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void attritionUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetAttritionReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void cohortUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetCohortReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void professorAttendanceUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetProffessorAttendanceReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void gradesUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetGradesReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void cohortIndicatorUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetCohortIndicatorReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void professorPunctualityUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetProfessorPunctualityReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        private void certificatedStudentsUpload(string fullPath, string idTerm, int? idUsuario)
        {
            var factory = new DataProcessorFactory();
            var reader = factory.GetCertificatedStudentsReader();
            reader.ProcessData(fullPath, CargarDatosContext().context, idTerm, idUsuario.ToSafeString());
        }

        public JsonResult GetFormattedResult(string message)
        {
            ResultViewModel model = new ResultViewModel();
            model.isError = false;
            model.message = message;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFormattedErrorResult(string message)
        {
            ResultViewModel model = new ResultViewModel();
            model.isError = true;
            model.message = message;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public BaseUploadViewModel GetDefaultViewModel(string uploadType)
        {
            SelectListItem inicio = new SelectListItem() { Text = "--" + LayoutResource.Seleccione + "--", Value = "0" };

            List<SelectListItem> Ciclos = uploadLogic.ListCyclesToForModality(ModalityId, 0, 0).Select(i => new SelectListItem() { Text = i.Value, Value = i.Key.ToString() }).ToList();
            Ciclos.Insert(0, inicio);
            var viewModel = new BaseUploadViewModel
            {
                UploadType = uploadType,
                UploadFileUrl = Url.AbsoluteAction("UploadFile", "DataUploadBase"),
                UploadExistsUrl = Url.AbsoluteAction("UploadExists", "DataUploadBase"),
                DownloadFileUrl = Url.AbsoluteAction("Download", "DataUploadBase", new { uploadType }),
                Modal = Ciclos,
                SubModalitiesListUrl = GetUploadSubModalitiesListUrl,
                TermsListUrl = GetTermsListUrl,
                TermsListUrlRegular = GetTermsListUrlRegular
            };
            return viewModel;
        }

        [HttpPost]
        public void FixStudentsPlace(int termId)
        {
            var alumnosSeccion = Context.AlumnoSeccion.Where(x => x.AlumnoMatriculado.IdSubModalidadPeriodoAcademico == termId);

            foreach (var al in alumnosSeccion)
            {
                al.AlumnoMatriculado.Sede = al.Seccion.Sede;
                Context.Entry(al).State = System.Data.Entity.EntityState.Modified;
            }

            Context.SaveChanges();
        }

        [HttpPost]
        public ActionResult UploadExists(string idTerm, string uploadType)
        {
            return Json(new { isUploaded = uploadLogic.UploadExists(idTerm, uploadType) });
        }

        [HttpGet]
        public ActionResult Download(string uploadType)
        {
            return File(
                GetFile(PathMapper.getFullPath(uploadType)), 
                System.Net.Mime.MediaTypeNames.Application.Octet,
                PathMapper.getFileName(uploadType));
        }

        byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        protected IEnumerable<SelectListItem> GetModelSelectList()
        {
            var result = new List<SelectListItem>();
            PopulateWithModel(result);
            return result;
        }
        
        private void PopulateWithModel(List<SelectListItem> list)
        {
            AddNewDefaultItem(list);
            foreach (var item in uploadLogic.GetModalType())
            {
                AddNewItem(list, item.NombreEspanol, item.IdModalidad.ToSafeString());
            }
        }

        protected void AddNewDefaultItem(List<SelectListItem> list)
        {
            AddNewItem(list, "-- " + LayoutResource.Seleccione + " --", "0");
        }

        protected void AddNewItem(List<SelectListItem> list, string text, string value)
        {
            list.Add(new SelectListItem
            {
                Text = text,
                Value = value
            });
        }

    }
}