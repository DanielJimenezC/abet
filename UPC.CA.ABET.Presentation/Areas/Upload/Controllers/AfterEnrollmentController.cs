﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.DataUpload;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Filters;
using Newtonsoft.Json;
using ClosedXML.Excel;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.EncargadoCargas)]
    public class AfterEnrollmentController : DataUploadBaseController
    {
        // GET: Upload/AfterEnrollment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Professors()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.DOCENTE));
        }
        public ActionResult EnrolledStudents()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.ALUMNOS_MATRICULADOS));
        }
        public ActionResult Classes()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.SECCION));
        }
        
        public ActionResult StudentsPerClass()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.ALUMNOS_POR_SECCION));
        }

        public ActionResult Delegates()
        {
            return View(GetDefaultViewModel(ConstantHelpers.UPLOAD_TYPE.DELEGADOS));
        }
    }
}