﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Upload.ViewModels.Common;
using UPC.CA.ABET.Logic.Areas.Upload.List;
using UPC.CA.ABET.Presentation.Areas.Upload.Presenter;
using UPC.CA.ABET.Logic.Areas.Upload.DataDeleter;

namespace UPC.CA.ABET.Presentation.Areas.Upload.Controllers
{
    public class MaintenanceController : Controller
    {

        private ListUploadsInteractor _listInteractor;
        private UploadDeleterInteractor _deleteInteractor;
        private UploadMaintenancePresenter _presenter;

        public MaintenanceController()
        {
            _listInteractor = new ListUploadsInteractor();
            _deleteInteractor = new UploadDeleterInteractor();
            _presenter = new UploadMaintenancePresenter();
        }

        public ActionResult Index(MessageViewModel message)
        {
            var uploads = _listInteractor.ListAll();




            var viewModel = _presenter.GetViewModel(uploads, message);

            return View(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var uploads = _listInteractor.ListAll();

            try
            {
                _deleteInteractor.Delete(id);
            } catch (Exception ex)
            {
                return RedirectToAction("Index", MessageViewModel.WithError(ex.Message));
            }

            return RedirectToAction("Index", MessageViewModel.WithSuccess(Presentation.Resources.Views.Shared.MessageResource.SeEliminoLaCargaCorrectamente));
        }
    }
}
