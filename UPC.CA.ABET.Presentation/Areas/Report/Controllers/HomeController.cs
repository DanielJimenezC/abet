﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Controllers;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.DirectorCarrera, AppRol.ProfesorGerente, AppRol.Usuario)]
    public class HomeController : BaseController
    {
        // GET: Report/Home
        public ActionResult Index()
        {
            return RedirectToAction("ByHistoricCourse", "Control");
        }
    }
}