﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Verification;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class VerificationController : ReportBaseController
    {
        private VerificationModel model;

        public VerificationController()
        {
            this.model = new VerificationModel(reportBaseModel, this);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

        }

        public ActionResult Consolidate()
        {
            var viewModel = new ConsolidateViewModel();
            model.LoadDropDownListsConsolidate(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Consolidate(ConsolidateViewModel ViewModel)
        {
            model.ExistDataConsolidate(ViewModel);
            return PartialView("_ConsolidateReport", ViewModel);
        }

        public ActionResult ByCourse()
        {
            var viewModel = new ByCourseViewModel();
            model.LoadDropDownListsByCourse(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByCourse(ByCourseViewModel ViewModel)
        {
            model.ExistDataByCourse(ViewModel);
            return PartialView("_ByCourseReport", ViewModel);
        }

        public ActionResult ByAcceptanceLevel()
        {
            var viewModel = new ByAcceptanceLevelViewModel();
            model.LoadDropDownListsByAcceptanceLevel(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            model.ExistDataByAcceptanceLevel(ViewModel);
            return PartialView("_ByAcceptanceLevelReport", ViewModel);
        }

        public ActionResult ByHistoricOutcome()
        {
            var viewModel = new ByHistoricOutcomeViewModel();
            model.LoadDropDownListsByHistoricOutcome(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByHistoricOutcome(ByHistoricOutcomeViewModel ViewModel)
        {
            model.ExistDataByHistoricOutcome(ViewModel);
            return PartialView("_ByHistoricOutcomeReport", ViewModel);
        }
    }
}