﻿using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Control;
using UPC.CA.ABET.Presentation.Filters;
using System.Linq;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class ControlController : ReportBaseController
    {
        private ControlModel model;

        public ControlController()
        {
            this.model = new ControlModel(reportBaseModel, this);
        }
        public ActionResult Management()
        {
            return View();
        }

        public ActionResult ByHistoricCourse()
        {
            var viewModel = new ByHistoricCourseViewModel();
            model.LoadDropDownListsByHistoricCourse(viewModel);
            return View(viewModel);
        }

        [HandleError(ExceptionType = typeof(HttpAntiForgeryException), View = "Unauthorized")]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            model.ExistDataByHistoricCourse(ViewModel);
            return PartialView("_ByHistoricCourseReport", ViewModel);
        }

        public ActionResult ByLevel()
        {
            var viewModel = new ByLevelViewModel();
            viewModel.ModalidadId = Session.GetModalidadId();
            viewModel.SubModalidadPeriodoAcademicoID = Session.GetSubModalidadPeriodoAcademicoId().Value;
            model.LoadDropDownListsByLevel(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByLevel(ByLevelViewModel ViewModel)
        {
            model.ExistDataByLevel(ViewModel);
            return PartialView("_ByLevelReport", ViewModel);
        }

        public ActionResult ByAcceptanceLevel()
        {
            var viewModel = new ByAcceptanceLevelViewModel();
            model.LoadDropDownListsByAcceptanceLevel(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            model.ExistDataAcceptanceLevel(ViewModel);
            return PartialView("_ByAcceptanceLevelReport", ViewModel);
        }

        public ActionResult ByOutcome()
        {
            var viewModel = new ByOutcomeViewModel();
            model.LoadDropDownListsByOutcome(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByOutcome(ByOutcomeViewModel ViewModel)
        {
            model.ExistDataByOutcome(ViewModel);
            return PartialView("_ByOutcomeReport", ViewModel);
        }
    }
}