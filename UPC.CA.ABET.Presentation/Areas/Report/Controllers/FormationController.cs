﻿using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Formation;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class FormationController : ReportBaseController
    {
        private FormationModel model;

        public FormationController()
        {
            this.model = new FormationModel(reportBaseModel, this);
        }

        public ActionResult ByAcceptanceLevel()
        {
            var viewModel = new ByAcceptanceLevelViewModel();
            model.LoadDropDownListsByAcceptanceLevel(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            model.ExistDataAcceptanceLevel(ViewModel);
            return PartialView("_ByAcceptanceLevelReport", ViewModel);
        }

        public ActionResult ByHistoricCourse()
        {
            var viewModel = new ByHistoricCourseViewModel();
            model.LoadDropDownListsByHistoricCourse(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            model.ExistDataByHistoricCourse(ViewModel);
            return PartialView("_ByHistoricCourseReport", ViewModel);
        }

        
    }
}