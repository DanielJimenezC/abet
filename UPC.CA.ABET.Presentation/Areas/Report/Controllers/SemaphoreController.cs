﻿using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Semaphore;
using UPC.CA.ABET.Presentation.Filters;
using System.Linq;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class SemaphoreController : ReportBaseController
    {
        private SemaphoreModel model;

        public SemaphoreController()
        {
            this.model = new SemaphoreModel(reportBaseModel, this);
        }

        public ActionResult BySemaphoreReportControl()
        {
            var viewModel = new BySemaphoreReportControlViewModel();
            model.LoadDropDownListsBySemaphoreReportControl(viewModel);
            return View(viewModel);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult BySemaphoreReportControl(BySemaphoreReportControlViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataBySemaphoreControl(ViewModel);
            return PartialView("_BySemaphoreReportControl", ViewModel);
        }

        public ActionResult BySemaphoreReportVerification()
        {
            var viewModel = new BySemaphoreReportVerificationViewModel();
            model.LoadDropDownListsBySemaphoreReportVerification(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult BySemaphoreReportVerification(BySemaphoreReportVerificationViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataBySemaphoreVerification(ViewModel);
            return PartialView("_BySemaphoreReportVerification", ViewModel);
        }



        public ActionResult BySummarySemaphoreReportControl()
        {
            var viewModel = new BySemaphoreReportControlViewModel();
            model.LoadDropDownListsBySemaphoreReportControl(viewModel);
            return View(viewModel);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult BySummarySemaphoreReportControl(BySemaphoreReportControlViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataBySemaphoreControl(ViewModel);
            return PartialView("_BySummarySemaphoreReportControl", ViewModel);
        }
        public ActionResult BySummarySemaphoreReportVerification()
        {
            var viewModel = new BySemaphoreReportVerificationViewModel();
            model.LoadDropDownListsBySemaphoreReportVerification(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult BySummarySemaphoreReportVerification(BySemaphoreReportVerificationViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataBySemaphoreVerification(ViewModel);
            return PartialView("_BySummarySemaphoreReportVerification",ViewModel);
        }

        public ActionResult ByCourse()
        {
            var viewModel = new ByCourseViewModel();
            model.LoadDropDownListsByCourseRC(viewModel);
            return View(viewModel);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByCourse(ByCourseViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataByCourseRC(ViewModel);
            return PartialView("_ByCourse", ViewModel);
        }

        public ActionResult ByCourseRV()
        {
            var viewModel = new ByCourseRVViewModel();
            model.LoadDropDownListsByCourseRV(viewModel);
            return View(viewModel);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByCourseRV(ByCourseRVViewModel ViewModel)
        {
            ViewModel.getSubModalidad(CargarDatosContext());
            model.ExistDataByCourseRV(ViewModel);
            return PartialView("_ByCourseRV", ViewModel);
        }

     

    }
}