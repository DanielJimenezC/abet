﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Achievement;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.DirectorCarrera, AppRol.MiembroComite, AppRol.CoordinadorCarrera)]
    public class AchievementController : ReportBaseController
    {
        private AchievementModel model;

        public AchievementController()
        {
            this.model = new AchievementModel(reportBaseModel, this);
        }

        public ActionResult ByCompetences()
        {
            var viewModel = new ByCompetencesViewModel();
            model.LoadDropDownListsByCompetences(viewModel);
            return View(viewModel);
        }

        public void ExportByCompetences(ByCompetencesViewModel ViewModel)
        {
            model.ExportByCompetencesExcel(ViewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByCompetences(ByCompetencesViewModel ViewModel)
        {
            model.LoadByCompetencesData(ViewModel);
            return PartialView("_ByCompetencesReport", ViewModel);
        }


        public ActionResult EditComment(int QualifiedRubricId)
        {
            return QualifiedRubricId == 0 ?
                PartialView("_NoRubricQualifiedMessage") :
                PartialView("_EditComment", new EditCommentViewModel
                {
                    Comment = model.GetRubricComment(QualifiedRubricId),
                    QualifiedRubricId = QualifiedRubricId
                });
        }

        [HttpPost]
        public async Task<ActionResult> EditComment(EditCommentViewModel ViewModel)
        {
            await model.SaveRubricComment(ViewModel);
            ViewModel.Saved = true;
            return PartialView("_EditComment", ViewModel);
        }
    }
}