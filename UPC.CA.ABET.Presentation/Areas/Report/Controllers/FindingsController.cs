﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class FindingsController : ReportBaseController
    {
        private FindingsModel model;

        public FindingsController()
        {
            this.model = new FindingsModel(reportBaseModel, this);
        }

        public ActionResult Index()
        {
            var viewModel = new IndexViewModel();
            model.LoadDropDownListsIndex(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(IndexViewModel ViewModel)
        {
            model.LoadCoursesFindingsData(ViewModel);
            return PartialView("_ListCoursesFindings", ViewModel);
        }

        [HttpPost]
        public ActionResult Manage(IndexViewModel ViewModel, int? CourseId, int? FindingAcceptanceLevelId, string UpdateTargetId, string SubmitButtonId, int? FindingId, string FindingCode = null, string Description = null)
        { 
            System.Diagnostics.Debug.WriteLine("estoy en manage");
            if (ViewModel == null)
            {
                ViewModel = new IndexViewModel();
            }

            if (ViewModel.CampusId.HasValue == false)
            {
                ViewModel.CampusId = 0;
            }


            var viewModel = new ManageViewModel
            {
                CampusId = ViewModel.CampusId.Value,
                CareerId = ViewModel.CareerId,
                CycleId = ViewModel.CycleId,
                InstrumentId = ViewModel.InstrumentId,
                CourseId = CourseId ?? 0,
                AcceptanceLevelId = FindingAcceptanceLevelId ?? 0,
                UpdateTargetId = UpdateTargetId,
                SubmitButtonId = SubmitButtonId,
                FindingId = FindingId ?? 0,
                Description = Description,
                //IdCriticidad = ViewModel.CriticidadId,
                criticidadList = CargarDatosContext().context.Criticidad.ToList()
            };

            model.LoadManageViewModel(viewModel);
            return PartialView("_Manage", viewModel);
        }

       
        [HttpPost]
        public ActionResult ConfirmManage(ManageViewModel ViewModel, string Button)
        {
            if (!string.IsNullOrWhiteSpace(Button) && Button.ToUpper().Equals("CANCEL"))
            {
                ViewModel.Saved = true;
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var modelHallazgos = Context.Hallazgos.FirstOrDefault(x=> x.IdHallazgo == ViewModel.FindingId);
                    var language = Session.GetCulture().ToString();
                    if (language == "es-PE")
                    {
                        modelHallazgos.DescripcionEspanol = ViewModel.Description;
                        modelHallazgos.IdCriticidad = ViewModel.IdCriticidad ?? default(int);
                    }
                    else if (language == "en-US")
                    {
                        modelHallazgos.DescripcionIngles = ViewModel.Description;
                        modelHallazgos.IdCriticidad = ViewModel.IdCriticidad ?? default(int); ;
                    }
                    Context.SaveChanges();
                    //await model.Save(ViewModel);
                    ViewModel.Saved = true;
                    PostMessage(MessageType.Success, "Se edito correctamente el registro.");

                }
            }
            ViewModel.criticidadList = CargarDatosContext().context.Criticidad.ToList();

            var vm = new IndexViewModel();
            vm.CampusId = 0;
            var viewModel = new ManageViewModel
            {
                CampusId = vm.CampusId.Value,
                CareerId = vm.CareerId,
                CycleId = vm.CycleId,
                InstrumentId = ViewModel.InstrumentId,
                CourseId = 0,
                AcceptanceLevelId = 0,
                UpdateTargetId = null,
                SubmitButtonId = null,
                FindingId = 0,
                Description = "",
                //IdCriticidad = ViewModel.CriticidadId,
                criticidadList = CargarDatosContext().context.Criticidad.ToList()
            };

            model.LoadManageViewModel(viewModel);

            return PartialView("_Manage", viewModel);
        }

        [HttpPost]
        public ActionResult ListFindings(ManageViewModel ViewModel)
        {
            return PartialView("_ListFindings", model.ListFindings(ViewModel));
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", model.GetDeleteViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                await model.Delete(ViewModel);
                ViewModel.Saved = true;
            }

            return PartialView("_Delete", ViewModel);
        }


        public ActionResult ImprovementActions(int id)
        {
            return PartialView("_ImprovementActions", model.GetImprovementActions(id));
        }

        [HttpPost]
        public ActionResult ManageImprovementAction(int FindingId, string UpdateTargetId, string SubmitButtonId, int? ImprovementActionId)
        {
            var viewModel = new ManageImprovementActionViewModel
            {
                FindingId = FindingId,
                ImprovementActionId = ImprovementActionId ?? 0,
                UpdateTargetId = UpdateTargetId,
                SubmitButtonId = SubmitButtonId
            };

            model.LoadManageImprovementActionViewModel(viewModel);
            return PartialView("_ManageImprovementAction", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmManageImprovementAction(ManageImprovementActionViewModel ViewModel, string Button)
        {
            if (!string.IsNullOrWhiteSpace(Button) && Button.ToUpper().Equals("CANCEL"))
            {
                ViewModel.Saved = true;
            }
            else
            {
                if (ModelState.IsValid)
                {
                    await model.SaveImprovementAction(ViewModel);
                    ViewModel.Saved = true;
                }
            }

            return PartialView("_ManageImprovementAction", ViewModel);
        }

        [HttpPost]
        public ActionResult ListImprovementActions(int FindingId)
        {
            ViewData["FindingId"] = FindingId;
            return PartialView("_ListImprovementActions", model.LoadImprovementActions(FindingId));
        }

        public ActionResult DeleteImprovementAction(int FindingId, string UpdateTargetId, int ImprovementActionId)
        {
            var viewModel = model.GetDeleteImprovementViewModel(FindingId, UpdateTargetId, ImprovementActionId);
            viewModel.Saved = false;
            return PartialView("_DeleteImprovementAction", viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteImprovementAction(DeleteViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                await model.DeleteImprovementAction(ViewModel);
                ViewModel.Saved = true;
            }

            return PartialView("_DeleteImprovementAction", ViewModel);
        }

        public ActionResult UpdateStatusImprovementAction(int FindingId, int ImprovementActionId)
        {
            var viewModel = model.GetUpdateStatusImprovementActionViewModel(FindingId, ImprovementActionId);
            model.LoadUpdateStatusImprovementActionViewModel(viewModel);
            return PartialView("_UpdateStatusImprovementAction", viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateStatusImprovementAction(UpdateStatusImprovementActionViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                await model.UpdateStatusImprovementAction(ViewModel);
                ViewModel.Saved = true;
            }

            model.LoadUpdateStatusImprovementActionViewModel(ViewModel);
            return PartialView("_UpdateStatusImprovementAction", ViewModel);
        }
    }
}