﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Controllers;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    public class ReportBaseController : BaseController
    {
        public ReportBaseModel reportBaseModel;
        public int SkipCycles;

        //public ReportBaseController(int? SkipCycles)
        //{
        //    this.reportBaseModel = new ReportBaseModel(context, Session);
        //    this.SkipCycles = SkipCycles.HasValue ? SkipCycles.Value : 0;
        //}

        public ReportBaseController()
        {
            this.reportBaseModel = new ReportBaseModel(Context);
            this.SkipCycles = 0;
        }

        //public JsonResult GetAccreditationTypes(int CycleId = 0)
        //{
        //    return reportBaseModel.GetAccreditationTypes(CycleId);
        //}

        public JsonResult GetAccreditationTypes(int CycleId = 0)
        {
            return reportBaseModel.GetAccreditationTypes(CycleId);
        }

        //public JsonResult GetAccreditationTypesBetweenCycles(int CycleFromId = 0, int CycleToId = 0)
        //{
        //    return reportBaseModel.GetAccreditationTypesBetweenCycles(CycleFromId, CycleToId);
        //}

        public JsonResult GetAccreditationTypesBetweenCycles(int CycleFromId = 0, int CycleToId = 0)
        {
            return reportBaseModel.GetAccreditationTypesBetweenCycles(CycleFromId, CycleToId);
        }

        //public JsonResult GetComissions(int AccreditationTypeId = 0, int CycleId = 0)
        //{
        //    return reportBaseModel.GetComissions(AccreditationTypeId, CycleId);
        //}

        public JsonResult GetComissions(int AccreditationTypeId = 0, int CycleId = 0)
        {
            return reportBaseModel.GetComissions(AccreditationTypeId, CycleId);
        }


        //public JsonResult GetComissionsByCareer(int AccreditationTypeId = 0, int CareerId = 0, int CycleId = 0)
        //{
        //    return reportBaseModel.GetComissionsByCareer(AccreditationTypeId, CareerId, CycleId);
        //}

        public JsonResult GetComissionsByCareer(int AccreditationTypeId = 0, int CareerId = 0, int CycleId = 0)
        {
            return reportBaseModel.GetComissionsByCareer(AccreditationTypeId, CareerId, CycleId);
        }


        //public JsonResult GetComissionsBetweenCycles(int AccreditationTypeId = 0, int CycleFromId = 0, int CycleToID = 0, int CareerId = 0)
        //{
        //    return reportBaseModel.GetComissionsBetweenCycles(AccreditationTypeId, CycleFromId, CycleToID, CareerId);
        //}

        public JsonResult GetComissionsBetweenCycles(int AccreditationTypeId = 0, int CycleFromId = 0, int CycleToId = 0, int CareerId = 0)
        {
            return reportBaseModel.GetComissionsBetweenCycles(AccreditationTypeId, CycleFromId, CycleToId, CareerId);
        }

        public JsonResult GetCareers()
        {
            return reportBaseModel.GetCareers();
        }
        public JsonResult GetCareersByCommission(int CommissionId = 0)
        {
            return reportBaseModel.GetCareersByCommission(CommissionId);
        }
        //public JsonResult GetControlCourses(int CareerId = 0, int StudentOutcomeId = 0, int CampusId = 0, int CommissionId = 0, int CycleFromId = 0, int CycleToId = 0)
        //{
        //    return reportBaseModel.GetControlCourses(CareerId, StudentOutcomeId, CampusId, CommissionId, CycleFromId, CycleToId);
        //}

        public JsonResult GetControlCourses(int CareerId = 0, int StudentOutcomeId = 0, int CampusId = 0, int CommissionId = 0, int CycleFromId = 0, int CycleToId = 0)
        {
            return reportBaseModel.GetControlCourses(CareerId, StudentOutcomeId, CampusId, CommissionId, CycleFromId, CycleToId);
        }


        //public JsonResult GetVerificationCourses(int CareerId = 0, int CampusId = 0, int StudentOutcomeId = 0, int CommissionId = 0, int CycleFromId = 0, int CycleToId = 0)
        //{
        //    return reportBaseModel.GetVerificationCourses(CareerId, CampusId, StudentOutcomeId, CommissionId, CycleFromId, CycleToId);
        //}

        public JsonResult GetVerificationCourses(int CareerId = 0, int CampusId = 0, int StudentOutcomeId = 0, int CommissionId = 0, int CycleFromId = 0, int CycleToId = 0)
        {
            return reportBaseModel.GetVerificationCourses(CareerId, CampusId, StudentOutcomeId, CommissionId, CycleFromId, CycleToId);
        }


        //public JsonResult GetDiscoverCourses(int ConstituentId = 0, int CareerId = 0, int CycleId = 0, int CampusId = 0, int CommissionId = 0, int StudentOutcomeId = 0)
        //{
        //    return reportBaseModel.GetDiscoverCourses(ConstituentId, CareerId, CycleId, CampusId, CommissionId, StudentOutcomeId);
        //}

        public JsonResult GetDiscoverCourses(int ConstituentId = 0, int CareerId = 0, int CycleId = 0, int CampusId = 0, int CommissionId = 0, int StudentOutcomeId = 0)
        {
            return reportBaseModel.GetDiscoverCourses(ConstituentId, CareerId, CycleId, CampusId, CommissionId, StudentOutcomeId);
        }


        //public JsonResult GetFormationCourses(int CareerId = 0, int CampusId = 0, int CycleFromId = 0, int CycleToId = 0)
        //{
        //    return reportBaseModel.GetFormationCourses(CareerId, CampusId, CycleFromId, CycleToId);
        //}

        public JsonResult GetFormationCourses(int CareerId = 0, int CampusId = 0, int CycleFromId = 0, int CycleToId = 0)
        {
            return reportBaseModel.GetFormationCourses(CareerId, CampusId, CycleFromId, CycleToId);
        }

        //public JsonResult GetCourseByOutcomeType(int CareerID = 0, int CampusID = 0, int StudentOutcomeID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0, string OutcomeTypeName = "")
        //{
        //    return reportBaseModel.GetCourseByOutcomeType(CareerID, CampusID, StudentOutcomeID, CommissionID, CycleFromID, CycleToID, OutcomeTypeName);
        //}

        public JsonResult GetCourseByOutcomeType(int CareerID = 0, int CampusID = 0, int StudentOutcomeID = 0, int CommissionID = 0, int CycleFromId = 0, int CycleToId = 0, string OutcomeTypeName = "")
        {
            return reportBaseModel.GetCourseByOutcomeType(CareerID, CampusID, StudentOutcomeID, CommissionID, CycleFromId, CycleToId, OutcomeTypeName);
        }

        public JsonResult GetInstruments(int ConstituentId = 0, string Acronyms = "")
        {
            return reportBaseModel.GetInstruments(ConstituentId, Acronyms);
        }


        //public JsonResult GetStudentOutcomes(int CommissionId = 0, int? CycleId = null, int? CareerId = null,  int? CampusId = null, string OutcomeTypeName = "")
        //{
        //    return reportBaseModel.GetStudentOutcomes(CommissionId, CycleId, CareerId, CampusId, OutcomeTypeName);
        //}
        public JsonResult GetStudentOutcomes(int CommissionId = 0, int? CycleId = null, int? CareerId = null, int? CampusId = null, string OutcomeTypeName = "",string Semaphore = "")
        {

            if (String.Equals(Semaphore, "Yes"))
            {
                return reportBaseModel.GetStudentOutcomesSemaphore(CommissionId, CycleId, CareerId, CampusId, OutcomeTypeName);
            }
            else
            {
                return reportBaseModel.GetStudentOutcomes(CommissionId, CycleId, CareerId, CampusId, OutcomeTypeName);
            }
        }

        public JsonResult GetSOSemaphore(int CommissionId = 0, int? CycleId = null, int? CareerId = null, int? CampusId = null, string OutcomeTypeName = "")
        {
            return reportBaseModel.GetStudentOutcomesSemaphore(CommissionId, CycleId, CareerId, CampusId, OutcomeTypeName);
        }

        //public JsonResult GetStudentOutcomesBetweenCycles(int CommissionId = 0, int? CycleFromId = null, int? CycleToId = null, int? CareerId = null, int? CampusId = null, string OutcomeTypeName = "")
        //{
        //    return reportBaseModel.GetStudentOutcomesBetweenCycles(CommissionId, CycleFromId, CycleToId, CareerId, CampusId, OutcomeTypeName);
        //}

        public JsonResult GetStudentOutcomesBetweenCycles(int CommissionId = 0, int? CycleFromId = null, int? CycleToId = null, int? CareerId = null, int? CampusId = null, string OutcomeTypeName = "")
        {
            return reportBaseModel.GetStudentOutcomesBetweenCycles(CommissionId, CycleFromId, CycleToId, CareerId, CampusId, OutcomeTypeName);
        }

        public JsonResult GetStudentOutcomeByCommission(int? CommissionID = null, int? CycleId = null, int? AccreditationTypeId = null)
        {
            return reportBaseModel.GetStudentOutcomeByCommission(CommissionID ?? 0, CycleId ?? 0, AccreditationTypeId ?? 0);
        }
        public JsonResult GetCampus(int CycleId = 0)
        {
            return reportBaseModel.GetCampus(CycleId);
        }

        #region ExportPlan

        public JsonResult GetAnio()
        {
            int IdEscuela = Session.GetEscuelaId();
            return reportBaseModel.GetAnio(IdEscuela);
        }


        public JsonResult GetModalidades()
        {
            return reportBaseModel.GetModalidades();
        }

        public JsonResult GetCarreras(int IdModalidad=0)
        {
            int ModalidadId = Session.GetModalidadId();
            int EscuelaId = Session.GetEscuelaId();
            string Idioma = Session.GetCulture().ToString();

            return reportBaseModel.GetCarreras(Idioma,ModalidadId,EscuelaId);

        }
        public JsonResult GetComisiones(int CareerId = 0, int Anio = 0)
        {
            return reportBaseModel.GetComisiones(CareerId, Convert.ToString(Anio));
        }
        public JsonResult GetInstrumentos(int Anio = 0,int IdModalidad = 0,int CareerId=0)
        {
            int PlanId = 0;
            return reportBaseModel.GetInstrumentos(PlanId, CareerId);
        }
        public JsonResult GetContituyentes(int InstrumentId=0)
        {
            return reportBaseModel.GetContituyentes(InstrumentId);
        }

        #endregion

        public JsonResult GetCampusBetweenCycles(int CycleFromId = 0, int CycleToId = 0)
        {
            return reportBaseModel.GetCampusBetweenCycles(CycleFromId, CycleToId);
        }
        public JsonResult GetExitStudents(int CycleID = 0, int CareerId = 0, int CampusId = 0, string Text = "")
        {
            return reportBaseModel.GetExitStudents(CycleID, CareerId, CampusId, Text);
        }
        public JsonResult GetAcceptanceLevel(string ReportCode = "")
        {
            return reportBaseModel.GetAcceptanceLevel(ReportCode);
        }

        public JsonResult GetInitialMessageReport()
        {
            return Json(FieldsResource.TextInitialReport, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCyclesFromForModality()
        {
            return reportBaseModel.GetCyclesFromForModality();
        }
        public JsonResult GetCyclesToForModality(int cycleFromId = 0)
        {
            return reportBaseModel.GetCyclesToForModality(cycleFromId);
        }
        public JsonResult GetModulesBetweenCyclesForModality(int cycleFromId = 0, int cycleToId = 0)
        {
            return reportBaseModel.GetModulesBetweenCyclesForModality(cycleFromId, cycleToId);
        }
        public JsonResult GetModulesForCycleForModality(int cycleId = 0)
        {
            return reportBaseModel.GetModulesForCycleForModality(cycleId);
        }
        public JsonResult GetJsonUrls()
        {
            return Json(new
            {
                getAccreditationTypes = Url.Action("GetAccreditationTypes"),
                getAccreditationTypesBetweenCycles = Url.Action("GetAccreditationTypesBetweenCycles"),
                getComissions = Url.Action("GetComissions"),
                getComissionsByCareer = Url.Action("GetComissionsByCareer"),
                getComissionsBetweenCycles = Url.Action("GetComissionsBetweenCycles"),
                getCareers = Url.Action("GetCareers"),
                getCareersByCommission = Url.Action("GetCareersByCommission"),
                getControlCourses = Url.Action("GetControlCourses"),
                getVerificationCourses = Url.Action("GetVerificationCourses"),
                getDiscoverCourses = Url.Action("GetDiscoverCourses"),
                getFormationCourses = Url.Action("GetFormationCourses"),
                getInstruments = Url.Action("GetInstruments"),
                getStudentOutcomes = Url.Action("GetStudentOutcomes"),
                getSOSemaphore = Url.Action("GetSOSemaphore"),
                getStudentOutcomesBetweenCycles = Url.Action("GetStudentOutcomesBetweenCycles"),
                getStudentOutcomeByCommission = Url.Action("GetStudentOutcomeByCommission"),
                getCampus = Url.Action("GetCampus"),
                getCampusBetweenCycles = Url.Action("GetCampusBetweenCycles"),
                getExitStudents = Url.Action("GetExitStudents"),
                getAcceptanceLevel = Url.Action("GetAcceptanceLevel"),
                getCourseByOutcomeType = Url.Action("GetCourseByOutcomeType"),
                getCyclesFromForModality = Url.Action("GetCyclesFromForModality"),
                getCyclesToForModality = Url.Action("GetCyclesToForModality"),
                getModulesBetweenCyclesForModality = Url.Action("GetModulesBetweenCyclesForModality"),
                getModulesForCycleForModality = Url.Action("GetModulesForCycleForModality"),
                getAnio = Url.Action("GetAnio") ,
                getModalidades = Url.Action("GetModalidades"),
                GetCarreras = Url.Action("GetCarreras"),
                GetComisiones = Url.Action("GetComisiones"),
                GetInstrumentos = Url.Action("GetInstrumentos"),
                GetContituyentes = Url.Action("GetContituyentes")
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLanguageConfiguration()
        {
            return Json(new
            {
                Datatable = new {
                    sInfo = DatatableResource.sInfo,
                    sInfoEmpty = DatatableResource.sInfoEmpty,
                    sInfoFiltered = DatatableResource.sInfoFiltered,
                    sEmptyTable = DatatableResource.sEmptyTable,
                    sLengthMenu = DatatableResource.sLengthMenu,
                    sSearch = DatatableResource.sSearch,
                    sSearchPlaceholder = DatatableResource.sSearchPlaceholder,
                    sProcessing = DatatableResource.sProcessing,
                    sLoadingRecords = DatatableResource.sLoadingRecords,
                    sZeroRecords = DatatableResource.sZeroRecords,
                    oPaginate = new
                    {
                        sFirst = DatatableResource.sFirst,
                        sLast = DatatableResource.sLast,
                        sNext = DatatableResource.sNext,
                        sPrevious = DatatableResource.sPrevious
                    },
                    oAria = new
                    {
                        sSortAscending = DatatableResource.sSortAscending,
                        sSortDescending = DatatableResource.sSortDescending
                    }
                },
                Select2 = new
                {
                    noResults = Select2Resource.noResults,
                    inputTooShort = Select2Resource.inputTooShort,
                    searching = Select2Resource.searching
                }
            }, JsonRequestBehavior.AllowGet);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!Request.IsAjaxRequest())
            {
                ViewBag.DropDownListLanguage = reportBaseModel.DropDownListLanguage();
                string idmodalidad = Convert.ToString(Session.GetModalidadId());

                //var cycles = (from a in context.PeriodoAcademico
                //              join b in context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
                //              where b.IdSubModalidadPeriodoAcademico == idsubmodalidadPeriodoAcademico
                //              select a.CicloAcademico).ToList();
                var ant = reportBaseModel.DropDownListCycleforModality(idmodalidad,SkipCycles);
                ViewBag.DropDownListCycle = ant;
                ViewBag.DropDownListModalities = reportBaseModel.DropDownListModalities();
                ViewBag.DropDownListCareer = reportBaseModel.DropDownListCareers();
            }

            base.OnActionExecuting(filterContext);
        }

        protected int GetPageLengthDataTableJS(DataTableRequestViewModel param, int totalLenght)
        {
            return (param.start + param.length) > totalLenght
                ? totalLenght - param.start
                : param.length;
        }
        protected object GetDataTableJSData(int draw, int recordsTotal, int recordsfiltered, object data)
        {
            return new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsfiltered,
                data = data
            };
        }

    }
}