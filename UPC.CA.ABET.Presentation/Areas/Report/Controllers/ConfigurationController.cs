﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Administrador, AppRol.DirectorCarrera, AppRol.CoordinadorCarrera, AppRol.MiembroComite)]
    public class ConfigurationController : ReportBaseController
    {
        private ConfigurationModel model;

        public ConfigurationController()
        {
            this.model = new ConfigurationModel(reportBaseModel, this);
        }

        public ActionResult Index()
        {
            ViewBag.Culture = currentCulture;
            return View();
        }

        #region Signatures

        public ActionResult Signatures()
        {
            return View(new SignaturesViewModel 
            {
                Signatures = model.ListSignatures()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signatures(int? page, SignaturesViewModel ViewModel)
        {

            if (ViewModel.aux == "signature")
            {
                PostMessage(MessageType.Success,"Firma creada satisfactoriamente");
                //return RedirectTo(model.ListSignatures(null,ViewModel));
                //return RedirectToAction("Signatures","Configuration",ViewModel);
                return PartialView("_ListSignatures", model.ListSignatures(page, ViewModel));
            }
            else
            {
                return PartialView("_ListSignatures", model.ListSignatures(page, ViewModel));
            }
        }

        public ActionResult ManageSignature(int? id)
        {
            var modalidadId = Session.GetModalidadId();
            var viewModel = model.GetSignature(id);
            model.LoadCombosManageSignatureViewModel(viewModel, modalidadId);
            return PartialView("_ManageSignature", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageSignature(ManageSignatureViewModel ViewModel)
        {
            model.ValidSignaturesPerCycle(ViewModel);

            if (ModelState.IsValid)
            {
                await model.SaveSignature(ViewModel);
                ViewModel.Saved = true;
            }

            var modalidadId = Session.GetModalidadId();
            model.LoadCombosManageSignatureViewModel(ViewModel, modalidadId);
            PostMessage(MessageType.Success, "Firma Creada Satisfactoriamente");
            return PartialView("_ManageSignature", ViewModel);
        }

        public ActionResult DeleteSignature(int? id)
        {
            return PartialView("_DeleteSignature", new DeleteSignatureViewModel 
            {
                SignatureId = id ?? 0,
                Culture = currentCulture
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteSignature(DeleteSignatureViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                await model.DeleteSignature(ViewModel);
                ViewModel.Deleted = true;
            }

            return PartialView("_DeleteSignature", ViewModel);
        }

        #endregion

        #region Process Information

        public ActionResult ProcessInformation()
        {
            if (TempData.ContainsKey("Success") && TempData["Success"] != null)
            {
                var message = currentCulture == ConstantHelpers.CULTURE.INGLES ? 
                    "The conversion process was done successfully." : 
                    "Se realizó el proceso de conversión con éxito.";

                PostMessage(MessageType.Success, message);
            }
            else if (TempData.ContainsKey("Error") && TempData["Error"] != null)
            {
                var message = currentCulture == ConstantHelpers.CULTURE.INGLES ?
                    "The conversion process was not done successfully." :
                    "No se realizó el proceso de conversión correctamente.";

                PostMessage(MessageType.Error, message);
            }
            return View(new ProcessInformationViewModel 
            {
                Conversions = model.ListConversions(),
                Culture = currentCulture,
                RubricCourseNames = currentCulture == ConstantHelpers.CULTURE.INGLES ?
                    "Workshop" : "de Taller"
            });
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessVerificationReportData(ProcessInformationViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                var subModalidadPeriodoAcademicoId = Session.GetSubModalidadPeriodoAcademicoId();
                var result = model.ProcessVerificationReportData(subModalidadPeriodoAcademicoId);
                if (result)
                    TempData["Success"] = true;
                else
                    TempData["Error"] = true;
            }

            return RedirectToAction("ProcessInformation");
        }
        #endregion
    }
}