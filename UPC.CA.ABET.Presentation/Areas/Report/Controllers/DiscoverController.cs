﻿using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class DiscoverController : ReportBaseController
    {
        private DiscoverModel model;

        public DiscoverController()
        {
            this.model = new DiscoverModel(reportBaseModel, this);
        }

        public ActionResult ABET()
        {
            var viewModel = new AbetViewModel();
            model.LoadDropDownListsABET(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ABET(AbetViewModel ViewModel)
        {
            ViewModel.ExistData = model.ExistData(ViewModel);
            return PartialView("_ABETReport", ViewModel);
        }

        public ActionResult EISC()
        {
            var viewModel = new EiscViewModel();
            model.LoadDropDownListsEISC(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult EISC(EiscViewModel ViewModel)
        {
            ViewModel.ExistData = model.ExistData(ViewModel);
            return PartialView("_EISCReport", ViewModel);
        }

        public ActionResult ByInstrument()
        {
            var viewModel = new ByInstrumentViewModel();
            model.LoadDropDownListsByInstrument(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ByInstrument(ByInstrumentViewModel ViewModel)
        {
            ViewModel.ExistData = model.ExistData(ViewModel);
            return PartialView("_ByInstrumentReport", ViewModel);
        }

        public ActionResult ImprovementActions()
        {
            var viewModel = new ImprovementActionsViewModel();
            model.LoadDropDownListsImprovementActions(viewModel);
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ImprovementActions(ImprovementActionsViewModel ViewModel)
        {
            ViewModel.IncludeImprovementActions = true;
            ViewModel.ExistData = model.ExistData(ViewModel);
            return PartialView("_ImprovementActionsReport", ViewModel);
        }

        [HttpPost]
        public JsonResult GetDataFindings(DiscoverViewModel ViewModel, DataTableRequestViewModel DataTableViewModel)
        {
            return Json(model.LoadTable(ViewModel, DataTableViewModel), JsonRequestBehavior.AllowGet);
        }
    }
}