﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ImprovementActions;
using UPC.CA.ABET.Presentation.Filters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Controllers
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class ImprovementActionsController : ReportBaseController
    {
        private ImprovementActionsModel model;

        public ImprovementActionsController()
        {
            this.model = new ImprovementActionsModel(reportBaseModel, this);
        }

        public ActionResult ConsultControlReport()
        {
            var viewModel = new ConsultControlReportViewModel();
            model.LoadDropDownListsConsultControlReport(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConsultControlReport(int? page, ConsultControlReportViewModel ViewModel)
        {
            var temp = model.LoadDataConsultControlReport(page, ViewModel);
            if (temp.Count == 0)
            {
                PostMessage(MessageType.Info, "No se encontraron registros para la busqueda.");
            }
            return PartialView("_ConsultControlReportTable", temp);
        }

        public ActionResult ManageControlReport(int? id)
        {
            var viewModel = (TempData["ViewModel"] as ManageViewModel) ?? model.LoadManageViewModel(id);
            model.LoadDropDownListsManageControlReport(viewModel);

            if (TempData.ContainsKey("Error"))
            {
                PostMessage(MessageType.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageControlReport(ManageViewModel viewModel)
        {
        ModelState.Remove("LanguageCulture");

            if (!ModelState.IsValid)
            {
                try
                {
                    await model.SaveImprovementActions(viewModel);
                    PostMessage(MessageType.Success, "Datos guardados satisfactoriamente.");
                    return RedirectToAction("ConsultControlReport");
                }
                catch (Exception)
                {
                    TempData["Error"] = true;
                }
            }
            PostMessage(MessageType.Warning, "Datos no guardados.");
            TempData["ViewModel"] = viewModel;
            return RedirectToAction("ManageControlReport");
        }

        public ActionResult DeleteControlReport(int id)
        {
            var viewModel = model.GetDeleteImprovementViewModel(id);
            viewModel.Saved = false;
            return PartialView("_DeleteControlReport", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteControlReport(DeleteViewModel viewModal)
        {
            if (ModelState.IsValid)
            {
                await model.DeleteImprovementAction(viewModal);
                viewModal.Saved = true;
            }

            return PartialView("_DeleteControlReport", viewModal);
        }

        public ActionResult ConsultVerificationReport()
        {
            var viewModel = new ConsultVerificationReportViewModel();
            model.LoadDropDownListsConsultVerificationReport(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConsultVerificationReport(int? page, ConsultVerificationReportViewModel ViewModel)
        {

            var temp = model.LoadDataConsultVerificationReport(page, ViewModel);
            if (temp.Count == 0) {
          //      PostMessage(MessageType.Info, "No se encontraron registros para la busqueda.");
            }
            return PartialView("_ConsultVerificationReportTable", temp);
        }

        
        public ActionResult ManageVerificationReport(int? id)
        {
            var viewModel = (TempData["ViewModel"] as ManageViewModel) ?? model.LoadManageViewModel(id);
            model.LoadDropDownListsManageVerificationReport(viewModel);

            if (TempData.ContainsKey("Error"))
            {
                PostMessage(MessageType.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageVerificationReport(ManageViewModel viewModel)
        {
            ModelState.Remove("LanguageCulture");

            if (!ModelState.IsValid)
            {
                try
                {
                    await model.SaveImprovementActions(viewModel);
                    PostMessage(MessageType.Success, "Se guardo satisfactoriamente.");
                    return RedirectToAction("ConsultVerificationReport");
                }
                catch (Exception)
                {
                    TempData["Error"] = true;
                }
            }
            PostMessage(MessageType.Warning, "Datos no guardados.");
            TempData["ViewModel"] = viewModel;
            return RedirectToAction("ManageVerificationReport");
        }
    }
}