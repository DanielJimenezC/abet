﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ReportPlanMejoraCurso : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "ReportPlanMejoraC");
                var CodComision = Request.QueryString["CodComision"];
                var IdCarrera = Request.QueryString["CareerId"];
                var anio = Request.QueryString["Anio"];
                //String codigo_archivo = context.Usp_GetCodigoNomenclatura_ReporteSemaforo(Convert.ToInt32(IdSubModalidad), Convert.ToInt32(IdCiclo), Convert.ToInt32(IdModulo), Convert.ToInt32(IdCarrera)).FirstOrDefault().ToString();
                //ReportViewer1.ServerReport.DisplayName = "PLANMEJORA" + codigo_archivo.ToString();
                ReportViewer1.ServerReport.DisplayName = "PLAN_MEJORA_CURSO";
                SetReportViewerParametersAndRefresh(ReportViewer1);

            }

        }
    }
}