﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class VerificationReportByHistoricOutcome : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "VerificationByHistoricOutcome");
                var IdCicloDesde = Request.QueryString["CycleFromId"];
                var IdCicloHasta = Request.QueryString["CycleToId"];
                var IdCampus = Request.QueryString["CampusId"];
                var IdAcreditacion = Request.QueryString["AccreditationTypeId"];
                var IdComision = Request.QueryString["CommissionId"];
                var IdCarrera = Request.QueryString["CareerId"];
                var IdOutcome = Request.QueryString["StudentOutcomeId"];
                String codigo_archivo = context.Usp_GetCodigoNomenclatura_RV_HystoricOutcome(Convert.ToInt32(IdCicloDesde), Convert.ToInt32(IdCicloHasta), Convert.ToInt32(IdCampus),
                    Convert.ToInt32(IdAcreditacion), Convert.ToInt32(IdComision), Convert.ToInt32(IdCarrera), Convert.ToInt32(IdOutcome)).FirstOrDefault().ToString();
                ReportViewer1.ServerReport.DisplayName = "RV_HYSTORIC_OUTCOME_" + codigo_archivo;
                SetReportViewerParametersAndRefresh(ReportViewer1);
            }
        }
    }
}