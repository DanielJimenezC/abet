﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ControlReportByHistoricCourse : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "ControlByHistoricCourse");
                var IdCicloDesde = Request.QueryString["CycleFromId"];
                var IdCicloHasta = Request.QueryString["CycleToId"];
                var IdComision = Request.QueryString["CommissionId"];
                var IdCampus = Request.QueryString["CampusId"];
                var IdAcreditacion = Request.QueryString["AccreditationTypeId"];
                var IdCurso = Request.QueryString["CourseId"];
                var IdCarrera = Request.QueryString["CareerId"];
                String codigo_archivo = context.Usp_GetCodigoNomenclatura_RC_HystoricCourse(Convert.ToInt32(IdCicloDesde), Convert.ToInt32(IdCicloHasta), Convert.ToInt32(IdComision), Convert.ToInt32(IdCampus),
                    Convert.ToInt32(IdAcreditacion), Convert.ToInt32(IdCurso), Convert.ToInt32(IdCarrera)).FirstOrDefault().ToString();
                ReportViewer1.ServerReport.DisplayName = "RC_HYSTORIC_COURSE_" + codigo_archivo;
                SetReportViewerParametersAndRefresh(ReportViewer1);
            }
        }
    }
}