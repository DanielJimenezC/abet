﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Filters;
using Query = UPC.CA.ABET.Helpers.ConstantHelpers.QueryStringParameters;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    [AuthorizeUser(AppRol.Acreditador, AppRol.Administrador, AppRol.CoordinadorCarrera, AppRol.DirectorCarrera, AppRol.Docente, AppRol.ProfesorGerente, AppRol.MiembroComite)]
    public class BasePage : System.Web.UI.Page
    {
        public readonly AbetEntities context;

        public BasePage()
        {
            context = new AbetEntities();
        }

        public void InitializeReportViewer(ReportViewer ReportViewerControl, string ReportName)
        {
            // Options
            ReportViewerControl.ProcessingMode = ProcessingMode.Remote;
            ReportViewerControl.ZoomMode = ZoomMode.PageWidth;
            ReportViewerControl.ShowFindControls = false;
            ReportViewerControl.ShowCredentialPrompts = false;
            ReportViewerControl.ShowParameterPrompts = false;
            ReportViewerControl.ShowRefreshButton = false;
            ReportViewerControl.AsyncRendering = true;
            ReportViewerControl.SizeToReportContent = false;
            ReportViewerControl.ClientIDMode = ClientIDMode.Static;

            // Values
            var serverUrl = AppSettingsHelper.GetAppSettings("ReportServerUrl", "http://abetdesarrollo.upc.edu.pe/ReportServer");
           // var reportPath = AppSettingsHelper.GetAppSettings("ReportServerPath", "/Abet2017Dev/Reports/");
            var reportPath = AppSettingsHelper.GetAppSettings("ReportServerPath", "/ReportesABETSistemasPruebas/Reports/");
            var username = AppSettingsHelper.GetAppSettings("ReportServerUsername", "reportadmin");
            var password = AppSettingsHelper.GetAppSettings("ReportServerPassword", "X@!9?uaC%m4pN64");
            var domain = AppSettingsHelper.GetAppSettings("ReportServerDomain", "srvdesabet");

            // Connect configurations
            ReportViewerControl.ServerReport.ReportServerUrl = new Uri(serverUrl);
            ReportViewerControl.ServerReport.ReportPath = reportPath + ReportName;
            ReportViewerControl.ServerReport.ReportServerCredentials = new CustomReportCredentials(username, password, domain);
        }

        public void SetReportViewerParametersAndRefresh(ReportViewer ReportViewerControl)
        {
            SetReportViewerParameters(ReportViewerControl);
            ReportViewerControl.ServerReport.Refresh();
        }

        public void SetReportViewerParametersAndExport(ReportViewer ReportViewerControl, string filename, string format)
        {
            SetReportViewerParameters(ReportViewerControl);

            Warning[] warnings;
            string[] streamids;
            string mimeType, encoding, extension, deviceInfo;

            var bytes = ReportViewerControl.ServerReport.Render(format, null, out mimeType, out encoding, out extension, out streamids, out warnings);

            Response.Clear();
            Response.ContentType = mimeType;
            Response.Charset = string.Empty;
            Response.BufferOutput = true;
            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.{1}", filename, extension));
            Response.End();
        }

        private void SetReportViewerParameters(ReportViewer ReportViewerControl)
        {
            var queryStringKeys = Request.QueryString.AllKeys;
            var excludedParameters = new string[] { "FORMAT", "INCLUDEIMPROVEMENTACTIONS" };

            if (queryStringKeys.Length > 0)
            {
                var parameters = new List<ReportParameter>();

                foreach (var key in queryStringKeys)
                {
                    if (excludedParameters.Contains(key.ToUpper())) continue;

                    parameters.Add(new ReportParameter(key, Request.QueryString[key]));
                }

                ReportViewerControl.ServerReport.SetParameters(parameters);   
            }
        }

        public string GetReportNameByConstituent(int ConstituentId, string ReportType = "")
        {
            var constituent = context.Constituyente.Find(ConstituentId);

            if (constituent != null)
            {
                var name = constituent.NombreIngles.ToUpper();
                switch (name)
                {
                    case "PROFESOR": return CheckImprovementActionsReportName("DiscoverProfesor", ReportType);
                    case "STUDENT": return CheckImprovementActionsReportName("DiscoverStudent", ReportType);
                    case "EMPLOYER": return CheckImprovementActionsReportName("DiscoverEmployer", ReportType);
                    case "GRADUATES": return CheckImprovementActionsReportName("DiscoverGraduates", ReportType);
                }    
            }

            return CheckImprovementActionsReportName("Discover", ReportType);
        }

        private string CheckImprovementActionsReportName(string ReportName, string ReportType)
        {
            return ReportName + (!string.IsNullOrWhiteSpace(ReportType) && ReportType.ToUpper() == "AM" ? "_AM" : string.Empty);
        }

        /// <summary>
        /// Gets value from query string given specified key
        /// </summary>
        /// <returns></returns>
        public string GetQueryString(string Key, object DefaultValue)
        {
            return Request.QueryString.AllKeys.Contains(Key) && !string.IsNullOrWhiteSpace(Request.QueryString[Key]) ?
                Request.QueryString[Key] : (DefaultValue ?? "0").ToString();
        }

        #region Parameters from query string

        public string AccreditationTypeId 
        {
            get 
            {
                return GetQueryString(Query.ACCREDITATION_TYPE_ID, "0");
            }
        }

        public string CommissionId
        {
            get
            {
                return GetQueryString(Query.COMMISSION_ID, "0");
            }
        }

        public string CareerId
        {
            get
            {
                return GetQueryString(Query.CAREER_ID, "0");
            }
        }

        public string CampusId
        {
            get
            {
                return GetQueryString(Query.CAMPUS_ID, "0");
            }
        }

        public string CycleId
        {
            get
            {
                return GetQueryString(Query.CYCLE_ID, "0");
            }
        }

        public string CycleFromId
        {
            get
            {
                return GetQueryString(Query.CYCLE_FROM_ID, "0");
            }
        }

        public string CycleToId
        {
            get
            {
                return GetQueryString(Query.CYCLE_TO_ID, "0");
            }
        }

        public string ConstituentId
        {
            get
            {
                return GetQueryString(Query.CONSTITUENT_ID, "0");
            }
        }

        public string CourseId
        {
            get
            {
                return GetQueryString(Query.COURSE_ID, "0");
            }
        }

        public string StudentOutcomeId
        {
            get
            {
                return GetQueryString(Query.STUDENT_OUTCOME_ID, "0");
            }
        }

        public string AcceptanceLevelId
        {
            get
            {
                return GetQueryString(Query.ACCEPTANCE_LEVEL_ID, "0");
            }
        }

        public string InstrumentId
        {
            get
            {
                return GetQueryString(Query.INSTRUMENT_ID, "0");
            }
        }

        public string LanguageCulture
        {
            get
            {
                return GetQueryString(Query.LANGUAGE_CULTURE, "es_PE");
            }
        }

        public string ModalidadId
        {
            get
            {
                return GetQueryString(Query.SUBMODALIDAD_ID, "0");
            }
        }

        public string ModuloId
        {
            get
            {
                return GetQueryString(Query.MODULO_ID, "0");
            }
        }

        public string DelegateVirtualSurveyId
        {
            get
            {
                return GetQueryString(Query.DELEGATEDVIRTUALSURVEY_ID, "0");
            }
        }

        public string SubModalidadId
        {
            get
            {
                return GetQueryString(Query.SUBMODALIDADID, "0");
            }
        }

        public string IdIdioma
        {
            get
            {
                return GetQueryString(Query.LENGUAJEID, "0");
            }
        }
        public string IdCarrera
        {
            get
            {
                return GetQueryString(Query.CARRERA_ID, "0");
            }
        }
        public string IdInstrumento
        {
            get
            {
                return GetQueryString(Query.INSTRUMENTO_ID, "0");
            }
        }


        public string IdCostituyente
        {
            get
            {
                return GetQueryString(Query.COSTITUYENTE_ID, "0");
            }
        }

        public string Anio
        {
            get
            {
                return GetQueryString(Query.ANIO, "0");
            }
        }

 
        public string Modalidad
        {
            get
            {
                return GetQueryString(Query.MODALIDAD_ID, "0");
            }
        }
        
        public string CodComision
        {
            get
            {
                return GetQueryString(Query.CODIGOCOMISION, "0");
            }
        }
        #endregion
    }
}