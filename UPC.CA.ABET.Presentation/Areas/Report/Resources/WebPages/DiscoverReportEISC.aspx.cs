﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class DiscoverReportEISC : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeReportViewer(ReportViewer1, GetReportNameByConstituent(Convert.ToInt32(ConstituentId)));
            SetReportViewerParametersAndExport(ReportViewer1, "ReporteHallazgosEISC", GetQueryString("Format", "PDF"));
        }
    }
}