﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class EVDReportStatus : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "CommentDelegateVirtualSurvey");
                var IdDelegateVirtualSurvey = Request.QueryString["DelegateVirtualSurveyId"];
                String codigo_archivo = context.Usp_GetCodigoNomenclatura(Convert.ToInt32(IdDelegateVirtualSurvey)).FirstOrDefault().ToString();
                ReportViewer1.ServerReport.DisplayName = "EVD_" + codigo_archivo.ToString();
                SetReportViewerParametersAndRefresh(ReportViewer1);
                
            }
        }
    }
}

