﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ReportSemaforoRV : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "ReportSemaforoRV");
                var IdSubModalidad = Request.QueryString["SubModalidadId"];
                var IdCiclo = Request.QueryString["CycleId"];
                var IdModulo = Request.QueryString["ModuloId"];
                var IdCarrera = Request.QueryString["CareerId"];
                String codigo_archivo = context.Usp_GetCodigoNomenclatura_ReporteSemaforo(Convert.ToInt32(IdSubModalidad), Convert.ToInt32(IdCiclo), Convert.ToInt32(IdModulo), Convert.ToInt32(IdCarrera)).FirstOrDefault().ToString();
                ReportViewer1.ServerReport.DisplayName = "RS_RV_CANTIDAD_" + codigo_archivo.ToString();
                SetReportViewerParametersAndRefresh(ReportViewer1);

            }
        }
    }
}