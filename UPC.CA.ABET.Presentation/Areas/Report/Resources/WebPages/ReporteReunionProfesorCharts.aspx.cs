﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ReporteReunionProfesorCharts : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "RP_Reuniones");

                var idEscuela = Request.QueryString["idEscuela"];
                var idSubmodalidadPeriodoAcademicoModulo = Request.QueryString["idSubmodalidadPeriodoAcademicoModulo"];
                var nivel = Request.QueryString["nivel"];
                var NumSemanaDesde = Request.QueryString["numSemanaDesde"];
                var NumSemanaHasta = Request.QueryString["numSemanaHasta"];
                var fechaDesde = Request.QueryString["fechaDesde"];
                var fechaHasta = Request.QueryString["fechaHasta"];
                var idIdioma = Request.QueryString["idioma"];

                // Export file name

                var idsmpa = int.Parse(idSubmodalidadPeriodoAcademicoModulo);

                var submodalidadperiodoacademicomodulo = context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsmpa).FirstOrDefault();
                var modalidad = submodalidadperiodoacademicomodulo.SubModalidadPeriodoAcademico.SubModalidad.NombreEspanol;
                var modulo = submodalidadperiodoacademicomodulo.Modulo.NombreEspanol;
                var periodoAcademico = submodalidadperiodoacademicomodulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;

                var dispName = "Reporte_Reuniones_Chart" + "_" + modalidad.Replace(" ", "") + "_" + modulo.Replace(" ", "") + "_" + periodoAcademico.Replace(" ", "");

                ReportViewer1.ServerReport.DisplayName = dispName;
                SetReportViewerParametersAndRefresh(ReportViewer1);
            }
        }
    }
}