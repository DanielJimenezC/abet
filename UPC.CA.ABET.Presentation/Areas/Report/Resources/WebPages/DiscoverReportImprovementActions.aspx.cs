﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class DiscoverReportImprovementActions : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, GetReportNameByConstituent(Convert.ToInt32(ConstituentId), "AM"));
                SetReportViewerParametersAndExport(ReportViewer1, "ReporteHallazgosYAccionesDeMejora", GetQueryString("Format", "PDF"));
            }
        }
    }
}