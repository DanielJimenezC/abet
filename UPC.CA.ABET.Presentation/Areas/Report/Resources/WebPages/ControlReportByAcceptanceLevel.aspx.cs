﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ControlReportByAcceptanceLevel : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "ControlByAcceptanceLevel");
                var IdCiclo = Request.QueryString["CycleId"];
                var IdCampus = Request.QueryString["CampusId"];
                var IdAcreditacion = Request.QueryString["AccreditationTypeId"];
                var IdNivel = Request.QueryString["AcceptanceLevelId"];
                var IdCarrera = Request.QueryString["CareerId"];
                var IdComision = Request.QueryString["CommissionId"];
                var IdOutcome = Request.QueryString["StudentOutcomeId"];
                var IdModulo = Request.QueryString["ModuloId"];
                var IdModalidad = Request.QueryString["ModalidadId"];
                String codigo_archivo = context.Usp_GetCodigoNomenclatura_RC_AcceptanceLevel(Convert.ToInt32(IdCiclo), Convert.ToInt32(IdCampus), Convert.ToInt32(IdAcreditacion), Convert.ToInt32(IdNivel), 
                    Convert.ToInt32(IdCarrera), Convert.ToInt32(IdComision), Convert.ToInt32(IdOutcome),  Convert.ToInt32(IdModulo), Convert.ToInt32(IdModalidad)).FirstOrDefault().ToString();
                ReportViewer1.ServerReport.DisplayName = "RC_ACCEPTANCE_LEVEL_" + codigo_archivo;
                SetReportViewerParametersAndRefresh(ReportViewer1);
            }
        }
    }
}