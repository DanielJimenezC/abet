﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UPC.CA.ABET.Presentation.Areas.Report.Resources.WebPages
{
    public partial class ReporteReunionProfesorTareas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeReportViewer(ReportViewer1, "RP_ReporteTareas");

                var idSubmodalidadPeriodoAcademicoModulo = Request.QueryString["IdsubmodalidadPeriodoAcademicoModulo"];
                var NumSemanaDesde = Request.QueryString["NumSemanaDesde"];
                var NumSemanaHasta = Request.QueryString["NumSemanaHasta"];
                var IdAreaUnidadAcademica = Request.QueryString["IdAreaUnidadAcademica"];
                var IdSubareaUnidadAcademica = Request.QueryString["IdSubareaUnidadAcademica"];
                var Estado = Request.QueryString["Estado"];
                var idioma = Request.QueryString["idioma"];
                var IdEscuela = Request.QueryString["IdEscuela"];
                var IdCursoPeriodoAcademico = Request.QueryString["IdCursoPeriodoAcademico"];
                var IdSede = Request.QueryString["IdSede"];
                var IdSeccion = Request.QueryString["IdSeccion"];

                // Export file name

                var idsmpa = int.Parse(idSubmodalidadPeriodoAcademicoModulo);

                var submodalidadperiodoacademicomodulo = context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsmpa).FirstOrDefault();
                var modalidad = submodalidadperiodoacademicomodulo.SubModalidadPeriodoAcademico.SubModalidad.NombreEspanol;
                var modulo = submodalidadperiodoacademicomodulo.Modulo.NombreEspanol;
                var periodoAcademico = submodalidadperiodoacademicomodulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico;

                var dispName = "ReporteTareas" + "_" + modalidad.Replace(" ", "") + "_" + modulo.Replace(" ", "") + "_" + periodoAcademico.Replace(" ", "");

                ReportViewer1.ServerReport.DisplayName = dispName;
                SetReportViewerParametersAndRefresh(ReportViewer1);
            }
        }
    }
}