﻿using System.ComponentModel.DataAnnotations;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Verification
{
    public class ByCourseViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAccreditationType")]
        public int AccreditationTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCommission")]
        public int CommissionId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleId { get; set; }

        public int? CourseId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAcceptanceLevel")]
        //public int AcceptanceLevelId { get; set; }
    }
}