﻿using System.ComponentModel.DataAnnotations;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Verification
{
    public class ByHistoricOutcomeViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAccreditationType")]
        public int AccreditationTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCommission")]
        public int CommissionId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageStudentOutcome")]
        public int? StudentOutcomeId { get; set; }
    }
}