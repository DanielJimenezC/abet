﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration
{
    public class ProcessInformationViewModel
    {
        public List<ConversionOutcomeComision> Conversions { get; set; }

        public string RubricCourseNames { get; set; }

        public string Culture { get; set; }
    }
}