﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.Configuration;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration
{
    public class ManageSignatureViewModel
    {
        public int PositionId { get; set; }

        //NEW
        [Required(ErrorMessageResourceName = "RequiredCycle", ErrorMessageResourceType = typeof(ReportResource))]
        public int SubModalidadPeriodoAcademicoId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredCycle", ErrorMessageResourceType = typeof(ReportResource))]
        public int CycleId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredPosition", ErrorMessageResourceType = typeof(ReportResource))]
        public string Position { get; set; }

        [Required(ErrorMessageResourceName = "RequiredCode", ErrorMessageResourceType = typeof(ReportResource))]
        public string Code { get; set; }

        [Required(ErrorMessageResourceName = "RequiredFirstNames", ErrorMessageResourceType = typeof(ReportResource))]
        public string FirstNames { get; set; }

        [Required(ErrorMessageResourceName = "RequiredLastNames", ErrorMessageResourceType = typeof(ReportResource))]
        public string LastNames { get; set; }

        public bool Saved { get; set; }

        public SelectList Cycles { get; set; }
    }
}