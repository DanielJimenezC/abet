﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration
{
    public class DeleteSignatureViewModel
    {
        public int SignatureId { get; set; }

        public bool Deleted { get; set; }

        public string Culture { get; set; }
    }
}