﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration
{
    public class SignaturesViewModel
    {
        public int? CycleId { get; set; } = 0;

        public int? SubModalidadPeriodoAcademicoId { get; set; }

        public string aux { get; set; }

        public IPagedList<ListSignaturesViewModel> Signatures { get; set; }
    }
}