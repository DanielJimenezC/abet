﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration
{
    public class ListSignaturesViewModel
    {
        public int PositionId { get; set; }

        public string AcademicPeriod { get; set; }

        public string Position { get; set; }

        public string Code { get; set; }

        public string FullName { get; set; }

        public string LastNames { get; set; }
    }
}