﻿using PagedList;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ImprovementActions
{
    public class ConsultVerificationReportViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCommission")]
        public int CommissionId { get; set; }

        public int? CycleId { get; set; }

        public int? CourseId { get; set; }

        public int? StudentOutcomeId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAcceptanceLevel")]
        public int? AcceptanceLevelId { get; set; }

        public string OutcomeTypeName { get; set; }

        public IPagedList<ListImprovementActionViewModel> ImprovementActions { get; set; }

        public ConsultVerificationReportViewModel()
        {
            this.ImprovementActions = new List<ListImprovementActionViewModel>().ToPagedList(1, 10);
        }
    }
}