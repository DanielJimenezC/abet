﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ImprovementActions
{
    public class ListImprovementActionViewModel
    {
        public int IdImprovementAction {    get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }
    }
}