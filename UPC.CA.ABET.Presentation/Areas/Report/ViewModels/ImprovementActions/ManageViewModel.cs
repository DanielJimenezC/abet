﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ImprovementActions
{
    public class ManageViewModel : ReportBaseViewModel
    {
        public int ImprovementActionId { get; set; }

        public int InstrumentId { get; set; }

        public string Code { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageImprovementAction")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageFindingsIds")]
        public int[] FindingsIds { get; set; }

        public SelectList DropDownListFindings { get; set; }

        public string SelectedValues { get; set; }
    }
}