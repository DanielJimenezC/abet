﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Achievement
{
    public class ByCompetencesViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageGraduate")]
        public int StudentId { get; set; }

        public int AccreditationTypeId { get; set; }

        public IEnumerable<Alumno> Students { get; set; }


        // Report properties
        public StudentViewModel Student { get; set; }
        public IList<string> TotalVerificationCourses { get; set; }
        public IList<RubricApproved> RubricApproveds { get; set; }
        public List<CompetenceViewModel> GeneralCompetences { get; set; }
        public List<CompetenceViewModel> SpecificCompetences { get; set; }
        public List<AchievementLevelViewModel> Levels { get; set; }
        public string AppreciativeSummary { get; set; }
        public IList<ApproverViewModel> Firms { get; set; }

        public ByCompetencesViewModel()
        {
            this.GeneralCompetences = new List<CompetenceViewModel>();
            this.SpecificCompetences = new List<CompetenceViewModel>();
            this.Levels = new List<AchievementLevelViewModel>();
            this.Firms = new List<ApproverViewModel>();
        }
    }

    public class StudentViewModel
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Career { get; set; }
        public string ExitCycle { get; set; }
    }

    public class RubricApproved
    {
        public string Course { get; set; }
        public string EvaluatorCode { get; set; }
        public string EvaluationType { get; set; }
        public decimal Grade { get; set; }
        public int QualifiedRubricId { get; set; }
        public string Cycle { get; set; }
    }

    public class CompetenceViewModel
    {
        public int IdOutcome { get; set; }
        public string Outcome { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public int ReachedLevel { get; set; }
        public int MaximunLevel { get; set; }
        public decimal ReachedGrade { get; set; }
        public decimal MaximunGrade { get; set; }
        public string Accreditation { get; set; }
        public string RomanNumber { get; set; }
        public List<CourseViewModel> Courses { get; set; }
        public int CellRange { get; set; }
        public string Commission { get; set; }

        public CompetenceViewModel()
        {
            this.Courses = new List<CourseViewModel>();
            this.CellRange = 3;
        }
    }

    public class CourseViewModel
    {
        public string Name { get; set; }
        public decimal Grade { get; set; }
        public int Level { get; set; }
    }

    public class ApproverViewModel
    {
        public string FullName { get; set; }
        public string JobPosition { get; set; }
        public string UrlFirm { get; set; }
    }

    public class AchievementLevelViewModel
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int LevelNumber { get; set; }
    }
}