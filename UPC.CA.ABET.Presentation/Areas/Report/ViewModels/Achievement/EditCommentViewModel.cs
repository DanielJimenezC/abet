﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.Achievement;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Achievement
{
    public class EditCommentViewModel
    {
        public int QualifiedRubricId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredComment")]
        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }

        public bool Saved { get; set; }
    }
}