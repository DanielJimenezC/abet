﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class DeleteViewModel
    {
        public int Id { get; set; }

        public string LanguageCulture { get; set; }

        public string UpdateTargetId { get; set; }
        
        public int FindingId { get; set; }

        public bool? Saved { get; set; }

        public bool CanDelete { get; set; }
    }
}