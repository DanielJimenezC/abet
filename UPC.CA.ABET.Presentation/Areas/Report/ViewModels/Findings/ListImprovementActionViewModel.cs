﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class ListImprovementActionViewModel
    {
        public int IdAccionMejora { get; set; }

        public string Codigo { get; set; }

        public string Descripcion { get; set; }

        public string Estado { get; set; }
    }
}