﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class ImprovementActionsViewModel
    {
        public int FindingId { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public IEnumerable<ListImprovementActionViewModel> ImprovementActions { get; set; }
    }
}