﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class UpdateStatusImprovementActionViewModel
    {
        public int FindingId { get; set; }

        public int ImprovementActionId { get; set; }

        public string Status { get; set; }

        public bool? Saved { get; set; }

        public SelectList StatusCombo { get; set; }

        public string LanguageCulture { get; set; }

        public string UpdateTargetId { get; set; }
    }
}