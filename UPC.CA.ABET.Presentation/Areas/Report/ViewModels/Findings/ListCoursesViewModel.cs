﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class ListCoursesViewModel
    {
        public int CourseId { get; set; }

        public string CourseCode { get; set; }

        public string CourseName { get; set; }

        public List<ListAcceptanceLevelViewModel> AcceptanceLevels { get; set; }

        public ListCoursesViewModel()
        {
            this.AcceptanceLevels = new List<ListAcceptanceLevelViewModel>();
        }
    }

    public class ListAcceptanceLevelViewModel
    {
        public int FindingAcceptanceLevelId { get; set; }

        public int StudentQuantity { get; set; }

        public string AcceptanceLevelName { get; set; }

        public string Color { get; set; }

        public List<ListFindingsViewModel> Findings { get; set; }

        public ListAcceptanceLevelViewModel()
        {
            this.Findings = new List<ListFindingsViewModel>();
        }
    }

    public class ListFindingsViewModel
    {
        public int FindingId { get; set; }

        public string FindingCode { get; set; }

        public string FindingDescription { get; set; }

        public int FindingCriticalityLevel { get; set; }

        public string FindingCriticalityLevelName { get; set; }

        public string CycleID{ get; set; }

    }
}