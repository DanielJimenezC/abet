﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class ManageImprovementActionViewModel
    {
        public int FindingId { get; set; }

        public string SubmitButtonId { get; set; }

        public string UpdateTargetId { get; set; }

        public int ImprovementActionId { get; set; }

        public string Code { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageImprovementAction")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string Status { get; set; }

        public bool? Saved { get; set; }

        public SelectList StatusCombo { get; set; }

        public string LanguageCulture { get; set; }
    }
}