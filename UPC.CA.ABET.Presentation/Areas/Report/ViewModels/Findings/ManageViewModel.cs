﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class ManageViewModel
    {
        public int CampusId { get; set; }

        public int CareerId { get; set; }

        public int CycleId { get; set; }

        public int InstrumentId { get; set; }

        public int CourseId { get; set; }

        public int AcceptanceLevelId { get; set; }

        public int FindingId { get; set; }

        public string Code { get; set; }

        public string UpdateTargetId { get; set; }

        public string SubmitButtonId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageFinding")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool? Saved { get; set; }

       // [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageFinding")]
        public List<Criticidad> criticidadList { get; set; }

        public Int32? IdCriticidad { get; set; }
        
    }
}