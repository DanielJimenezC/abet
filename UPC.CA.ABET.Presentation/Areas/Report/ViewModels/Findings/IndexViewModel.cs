﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings
{
    public class IndexViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleId { get; set; }

        public int? AcceptanceLevelId { get; set; }

        public int InstrumentId { get; set; }

        public int CriticidadId { get; set; }

        public int RCInstrumentId { get; set; }

        public int RVInstrumentId { get; set; }

        public string OutcomeTypeName { get; set; }

        public IList<ListCoursesViewModel> Courses { get; set; }

        public IndexViewModel()
        {
            this.Courses = new List<ListCoursesViewModel>();
        }
    }
}