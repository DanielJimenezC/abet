﻿using System.ComponentModel.DataAnnotations;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Formation
{
    public class ByHistoricCourseViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleFromId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleToId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCourse")]
        public int CourseId { get; set; }
    }
}