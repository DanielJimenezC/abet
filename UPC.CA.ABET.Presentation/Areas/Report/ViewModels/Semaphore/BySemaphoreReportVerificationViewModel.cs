﻿using System.ComponentModel.DataAnnotations;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
using UPC.CA.ABET.Models;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Semaphore
{
    public class BySemaphoreReportVerificationViewModel : ReportBaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAccreditationType")]
        public int AccreditationTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCommission")]
        public int CommissionId { get; set; }

        public int? CycleId { get; set; }

        public int? StudentOutcomeId { get; set; }

        public int IdSubModalidad { get; set; }

        public void getSubModalidad(CargarDatosContext DataContext)
        {
            int? parIdSubModalidad = (from s in DataContext.context.SubModalidadPeriodoAcademico
                                      where s.IdPeriodoAcademico == this.CycleId
                                      select s.IdSubModalidad).FirstOrDefault();
            var modalidad = DataContext.context.Modalidad.Where(x => x.IdModalidad == this.ModalityId).FirstOrDefault();

            if (!parIdSubModalidad.HasValue)
            {
                IdSubModalidad = DataContext.context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.IdModalidad == this.ModalityId && x.PeriodoAcademico.Estado == "ACT").FirstOrDefault().IdSubModalidadPeriodoAcademico;
            }
            else
            {
                IdSubModalidad = parIdSubModalidad.GetValueOrDefault();
            }

        }
    }
}
