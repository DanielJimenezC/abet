﻿namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase
{
    public class OptionViewModel
    {
        public int Value { get; set; }
        public string TextContent { get; set; }
    }
}