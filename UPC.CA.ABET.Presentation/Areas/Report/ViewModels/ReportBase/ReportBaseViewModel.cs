﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase
{
    public class ReportBaseViewModel
    {
        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCampus")]
        public int? CampusId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageLanguage")]
        public string LanguageCulture { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageModality")]
        public int? ModalityId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageModule")]
        public int? ModuleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleFromId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCycle")]
        public int CycleToId { get; set; }

        public bool? ExistData { get; set; }

        public string Message { get; set; }

        public int CollegeId { get; private set; }

        public ReportBaseViewModel()
        {
            this.CollegeId = HttpContext.Current.Session.GetEscuelaId();
        }

        public SelectList DropDownListAccreditationType { get; set; }
        public SelectList DropDownListCommission { get; set; }
        public SelectList DropDownListModules { get; set; }
        public SelectList DropDownListCareer { get; set; }
        public SelectList DropDownListCourse { get; set; }
        public SelectList DropDownListCampus { get; set; }
        public SelectList DropDownListCycle { get; set; }
        public SelectList DropDownListLevel { get; set; }
        public SelectList DropDownListStudentOutcome { get; set; }
        public SelectList DropDownListAcceptanceLevel { get; set; }
        public SelectList DropDownListStudents { get; set; }
        public SelectList DropDownListConstituent { get; set; }
        public SelectList DropDownListInstrument { get; set; }
        public SelectList DropDownListCyclesFromForModality { get; set; }
        public SelectList DropDownListCyclesToForModality { get; set; }
        //criticidad
        public SelectList DropDownListCriticity { get; set; }
        public SelectList DropDownListCriticality { get; set; }

        //EXPORTAR PLAN DE MEJORA Y RESULTADO
        public SelectList DropDownListAnioForEscuela { get; set; }
        public SelectList DropDownListModalityForExport { get; set; }
        public SelectList DropDownListCareerForModalityExport{ get; set; }
        public SelectList DropDownListCommissionForCareerandAnio { get; set; }
        public SelectList DropDownListInstrumentForPlanandCarrer { get; set; }
        public SelectList DropDownListConstituentForInstrument { get; set; }

   
  

    }
}