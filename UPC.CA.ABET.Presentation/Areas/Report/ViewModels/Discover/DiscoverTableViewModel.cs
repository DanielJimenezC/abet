﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover
{
    public class DiscoverTableViewModel
    {
        public string Codigo { get; set; }

        public string Descripcion { get; set; }

        public string CodigoAccionMejora { get; set; }

        public string DescripcionAccionMejora { get; set; }

        public int TotalRows { get; set; }
    }
}