﻿using System.Collections.Generic;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover
{
    public class ByInstrumentViewModel : DiscoverViewModel
    {
        public override int AccreditationTypeId { get; set; }
        public override int CareerId { get; set; }
        public override int? CommissionId { get; set; }
        public override int? CycleId { get; set; }
        public override int? ConstituentId { get; set; }
        public override int? CourseId { get; set; }
        public override int? StudentOutcomeId { get; set; }
        public override int? AcceptanceLevelId { get; set; }
        public override int? InstrumentId { get; set; }
        public override int? CriticalityId { get; set; }
    }
}