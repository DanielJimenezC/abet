﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.ReportBase;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;
namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover
{
    public class AbetViewModel : DiscoverViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAccreditationType")]
        public override int AccreditationTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCareer")]
        public override int CareerId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageCommission")]
        public override int? CommissionId { get; set; }

        public override int? CycleId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageConstituent")]
        public override int? ConstituentId { get; set; }

        public override int? CourseId { get; set; }

        public override int? StudentOutcomeId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ReportResource), ErrorMessageResourceName = "RequiredMessageAcceptanceLevel")]
        public override int? AcceptanceLevelId { get; set; }

        public int CriticityId { get; set; }

        public override int? CriticalityId { get; set; }
    }
}