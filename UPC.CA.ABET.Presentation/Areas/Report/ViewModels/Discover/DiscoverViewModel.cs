﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;

namespace UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover
{
    public class DiscoverViewModel : ReportBaseViewModel
    {
        public virtual int AccreditationTypeId { get; set; }
        public virtual int CareerId { get; set; }
        public virtual int? CommissionId { get; set; }
        public virtual int? CycleId { get; set; }
        public virtual int? ConstituentId { get; set; }
        public virtual int? CourseId { get; set; }
        public virtual int? StudentOutcomeId { get; set; }
        public virtual int? AcceptanceLevelId { get; set; }
        public virtual int? InstrumentId { get; set; }
        public virtual int? CriticalityId { get; set; }

        public int ConstituentProfesorId { get; set; }
        public int ConstituentGraduatesId { get; set; }
        public bool? IncludeImprovementActions { get; set; }
    }
}