﻿/// <reference path="jspdf.debug.js" />
/// <reference path="~/Content/vendor/jquery/jquery.min.js" />

var doc,
    reportData,
    matrixData = [],
    reportInterval;

$(document).ready(function () {
    doc = new jsPDF();
});

$(document).on('click', '#btnPdfReport', function (e) {
    $('#main-loader').fadeIn();
    getGraphicReport();
    
    reportInterval = setInterval(function () {
        console.log('report data:' + (reportData != undefined) + '   matrix data:' + (matrixData != undefined));

        if (reportData != undefined && matrixData != undefined) {
            
            clearInterval(reportInterval);


            $('#main-loader').fadeOut();

            doc.addImage(reportData, 'JPEG', 10, 10, 190, 250);

            
            for (var i in matrixData) {
                doc = doc.addPage('a4', 'l');
                doc.addImage(matrixData[i].data, 'JPEG', 10, 10); //, 277, 190
            }

            var studentFullName = $('#studentFullName').html().trim().replace(/ /g, '_');

            doc.save('AchievementByCompetences.pdf');

            doc = new jsPDF();
            reportData = null;
            reportInterval = null;
        }

    }, 400);
})


function getGraphicReport() {
    html2canvas($('#competences-report'), { async: false }).then(function (canvas) {
        reportData = canvas
            .toDataURL("image/jpeg", 1.0)
            .replace(/^data:image\/(png|jpg);base64,/, '');
    });
}

function getMatrixReport() {
    matrixData = [];
    html2canvas($('#matriz-container'), { async: false }).then(function (canvas) {

        $('#matriz-container').hide();

        var heightCut = 681,
            originalHeight = canvas.height,
            initialHeight = 0,
            width = 1020,
            height = heightCut,
            isLastPage = false;
            
        while (true) {
            var _canvas = document.createElement('canvas');
            _canvas.width = width;
            _canvas.height = height;

            var ctx = _canvas.getContext('2d');
            ctx.drawImage(canvas, 0, initialHeight, width, height, 0, 0, width, height);

            matrixData.push({
                width: width,
                height: height,
                data: _canvas
                    .toDataURL("image/jpeg", 1.0)
                    .replace(/^data:image\/(png|jpg);base64,/, '')
            });

            if (isLastPage) {
                break;
            }

            initialHeight += height;

            if (initialHeight + height >= originalHeight && !isLastPage) {
                height = originalHeight - initialHeight;
                isLastPage = true;
            } 
        }
        
        for (var i = matrixData.length - 1; i > -1; i--) {
            if (matrixData[i].data.indexOf('image') < 0) {
                matrixData.splice(i, 1);
            }
        }

        //matrixData.push(canvas
        //    .toDataURL("image/jpeg", 1.0)
        //    .replace(/^data:image\/(png|jpg);base64,/, ''));

        //718
    });
}