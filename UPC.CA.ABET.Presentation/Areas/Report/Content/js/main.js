﻿/// <reference path="~/Content/vendor/jquery/jquery.min.js" />

var reportCombosUrls = {
    getAccreditationTypes: '',
    getAccreditationTypesBetweenCycles: '',
    getComissions: '',
    getComissionsByCareer: '',
    getComissionsBetweenCycles: '',
    getCareers: '',
    getCareersByCommission: '',
    getControlCourses: '',
    getVerificationCourses: '',
    getDiscoverCourses: '',
    getFormationCourses: '',
    getInstruments: '',
    getSOSemaphore: '',
    getStudentOutcomes: '',
    getStudentOutcomesBetweenCycles: '',
    getStudentOutcomeByCommission: '',
    getCampus: '',
    getCampusBetweenCycles: '',
    getExitStudents: '',
    getAcceptanceLevel: '',
    getCourseByOutcomeType: '',
    getCyclesFromForModality: '',
    getCyclesToForModality: '',
    getModulesBetweenCyclesForModality: '',
    getModulesForCycleForModality: '',
    getAnio: '',
    getModalidades: '',
    GetCarreras: '',
    GetComisiones: '',
    GetInstrumentos: '',
    GetContituyentes: ''
}

//var reportCombos = [
//    '#CareerId',
//    '#CommissionId',
//    '#CycleId',
//    '#CycleFromId',
//    '#CycleToId',
//    '#CourseId',
//    '#CampusId',
//    '#LevelId',
//    '#AccreditationTypeId',
//    '#LanguageCulture',
//    '#ModuleId'
//];

var dataTableConfiguration = {
        language: {}
    };

var canInitDatatable = false,
    datatableInstance;


$.validator.setDefaults({ ignore: '.ignore' });

Object.defineProperty(Array.prototype, 'hasElements', {
    get: function () {
        return this != undefined && this.length > 0;
    }
})

Object.defineProperty(Array.prototype, 'firstValue', {
    get: function () {
        return this.hasElements ? this[0].Value : -1;
    }
})

jQuery.prototype.resetCombos = function (propNames, emptyProps) {
    $(this).on('click', function (e) {
        resetCombos(propNames, emptyProps);
        e.preventDefault();
    });
}

$(document).ready(function () {
    //reportCombos.forEach(x => {
    //    $(x).attr('disabled', true);
    //});
    $('#modal-message').on('hidden.bs.modal', function () {
        $('#modal-title').html('');
        $('#modal-body').html('');
    })
    
    if (urlRoute.substring(urlRoute.length - 1) != '/') {
        urlRoute += '/';
    }

    $('.panel.filters select').css('width', '100%');

    $.getJSON(urlRoute + 'Report/ReportBase/GetJsonUrls').done(function (data) {
        for (var key in data) {
            reportCombosUrls[key] = data[key];
        }
    });

    $.getJSON(urlRoute + 'Report/ReportBase/GetLanguageConfiguration')
    .done(function (data) {
        if (data.Datatable && $.fn.dataTable) {
            $.extend(true, $.fn.dataTable.defaults.oLanguage, data.Datatable);
        }

        if (data.Select2 && $.fn.select2) {
            var _language = {};
            for (var key in data.Select2) {
                _language[key] = function () { return data.Select2[key]; };
            }

            $.extend(true, $.fn.select2.defaults.defaults.language, _language);
        }
    })
    .complete(function () {
        if ($.fn.dataTable) {
            $.extend(true, $.fn.dataTable.defaults, {
                'sPaginationType': 'full_numbers',
                'pageLength': 10,
                'responsive': true,
                'bServerSide': true,
                'bProcessing': true,
                'fixedHeader': {
                    'header': true,
                    'headerOffset': $(".site-navbar").eq(0).innerHeight()
                },
                'dom': "<'row margin-b-10'<'col-xs-12 col-md-7'f><'col-xs-12 col-md-5 option-content text-right'>><'row'<'col-xs-12'tr>><'row'<'col-xs-4'i><'col-xs-8'p>>",
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': '<<',
                        'sLast': '>>',
                        'sNext': '>',
                        'sPrevious': '<'
                    }
                },
                'fnDrawCallback': function () {
                    $('.dataTables_empty').addClass('text-center');
                }
            });
        }

        if ($.fn.select2) {
            $('select').select2({ minimumResultsForSearch: Infinity });
            setTimeout(function () { $('[data-plugin="select2"]').select2(); }, 100);
        }

        canInitDatatable = true;
    });
});


$(document).on('show.bs.dropdown', '.table-responsive', function () {
    $('.table-responsive').css("overflow", "inherit");
})


$(document).on('hide.bs.dropdown', '.table-responsive', function () {
    $('.table-responsive').css("overflow", "auto");
})

function initDataTable(url, columns) {

    setTimeout(function () {
        if (canInitDatatable) {
            
            datatableInstance = $('.dataTable').dataTable({
                columns: columns,
                ajax: {
                    url: url,
                    type: 'POST',
                    data: function (d) {
                        $.extend(true, d, getData());
                        return d;
                    }
                }
            });
                        

        } else {
            setTimeout(initDataTable, 100, url, columns);
        }
    }, 100);
}
function selectMenu(liIdParent, liIdChild) {
    $('#' + liIdParent).addClass('open');
    if (liIdChild) {
        $('#' + liIdChild).addClass('active');
    }
}
function loadCombo(target, url, params, _allOption, searchOption) {
    var select = $(target),
        button = $('#btnGenerarReporte'),
        allOption = _allOption != undefined && _allOption.length > 0;
    //console.log(allOption, _allOption);
    //reportCombos.forEach(x => {
    //    $(x).attr('disabled', false);
    //})
    button.attr('disabled', true);
    select.attr('disabled', true);
    select.html(allOption ? '<option value="">' + _allOption + '</option>' : '');

    console.log('loadCombo: ' + url);
    return $.getJSON(url, params)
            .complete(function () {
                button.attr('disabled', false);
                select.select2(allOption || searchOption ? {} : { minimumResultsForSearch: Infinity });
            })
            .pipe(function (data) {
                if (data.length <= 1) {
                    select.html('');
                }

                //if (target == '#CampusId') {
                //    console.log(target + " " + data.length);
                //    select.append('<option value=0>' + ' Todos ' + '</option>');
                //}

                //if (target == '#ModuleId') {
                //    console.log(target + " " + data.length);
                //    if (data.length > 1) {
                //        select.append('<option value=0>' + ' Todos ' + '</option>');
                //    }
                    
                //}

                //if (target == '#CycleId') {
                //    console.log(target + " " + data.length);
                //}

                //if (target == '#CycleFromId') {
                //    console.log(target + " " + data.length);
                //}

                //if (target == '#AccreditationTypeId') {
                //    console.log(target + " " + data.length);
                //}

                //if (target == '#StudentOutcomeId') {
                //    console.log(target + " " + data.length);
                //}
                console.log(data)
                $.each(data, function (i, e) {
                    select.append('<option value="' + e.Value + '">' + e.TextContent + '</option>');
                });
                
                

                

                select.attr('disabled', false);
                return data;
            });
}
function resetCombos(propNames, emptyProps) {
    for (var i in propNames) {
        if (propNames.hasOwnProperty(i)) {
            var prop = propNames[i];

            var options = $('#' + prop + ' > option');
            if (options.length > 0) {
                var value = emptyProps != undefined && emptyProps.indexOf(prop) > -1 ? '' : options[0].value;
                $('#' + prop).val(value).trigger('change');
            }
        }
    }

    $('#main-loader').fadeIn();
    $.getJSON(urlRoute + 'Report/ReportBase/GetInitialMessageReport')
        .done(function (message) { $('#report-container').html('<div class="report-init-text text-center\">' + message + '</div>'); })
        .complete(function () { $('#main-loader').fadeOut(); })

    $('html,body').animate({ scrollTop: $('#page-title-container').offset().top - 60 }, 1000);
}
function validateAllCycles(allOptionText) {
    if ($('#CycleId').val() == '') {
        $('#CommissionId').html('<option value="">' + allOptionText + '</option>');
        $('#CommissionId').attr('disabled', true);
        $('#CommissionId').select2();
        return true;
    } else {
        $('#CommissionId').attr('disabled', false);
        return false;
    }
}
function validateAllCareers(allOptionText) {
    if ($('#CareerId').val() == '') {
        $('#CommissionId').html('<option value="">' + allOptionText + '</option>');
        $('#CommissionId').attr('disabled', true);
        $('#CommissionId').select2();
        return true;
    } else {
        $('#CommissionId').attr('disabled', false);
        return false;
    }
}
function showMessage(title, body) {
    $('#modal-title').html(title);
    $('#modal-body').html(body);
    $('#modal-message').modal('show');
}
function scrollTopReportContainer() {
    $('html,body').animate({ scrollTop: $('#report-container').offset().top - 150 }, 1000);
}
function iframeLoad() {
    scrollTopReportContainer();
    var timeout = setTimeout(function () { $('#main-loader').fadeIn(); }, 100);
    $('iframe').load(function () {
        clearTimeout(timeout);
	    setTimeout(function () { $('#main-loader').fadeOut(); }, 200);
    });
}
function scrollSidebarTo(liId) {
    var container = $('.scrollable-container');
    container.animate({
        scrollTop: $('#' + liId).offset().top - container.height()
    }, 500);
}
function scrollToTable() {
    $('html,body').animate({ scrollTop: $('#table-container').offset().top - 150 }, 1000);
}
function getData() {
    var data = $('.panel.filters form').serializeObject();
    delete data.__RequestVerificationToken;
    return data;
}
function bindChange(selector, callback) {
    $(document).on('change', selector, callback);
}
function setFirstValue(data, property, array) {
    if (data && property) {
        if (array != undefined && array.hasOwnProperty('firstValue')) {
            data[property] = array.firstValue;
        }
    }
}
function setFirstValueWithCallback(data, property, array, setValueCallback) {
    if (data && property) {
        if (array != undefined && array.hasOwnProperty('firstValue')) {
            data[property] = setValueCallback != undefined && typeof(setValueCallback) === 'function' ? setValueCallback(array.firstValue, array) : array.firstValue;
        }
    }
}
function setExportUrls() {
    var exportUrl = $('#report-container').data('webPageUrl'),
        data = getData();

    $('select[name]').each(function (i, e) {
        if (!data.hasOwnProperty(e.name)) {
            data[e.name] = '0';
        }
        //data[e.name] = e.value || '0';
    })

    for (var key in data) {
        if (!data[key]) {
            data[key] = '0';
        }
    }

    delete data.AccreditationTypeId;
    delete data.OutcomeTypeName;

    console.log(data);

    var query = exportUrl + '?Format={0}&' + $.param(data);

    $('[export-link]').each(function (i, e) {
        e.href = query.replace('{0}', e.dataset.format);
    })

    //$('#lnkPdf').attr('href', query.replace('{0}', 'PDF'));
    //$('#lnkExcel').attr('href', query.replace('{0}', 'EXCEL'));
    //$('#lnkWord').attr('href', query.replace('{0}', 'WORD'));
}
function submitSearchDatatable() {
    $('#btnGenerateReport').click(function () {
        if (datatableInstance) {
            datatableInstance.fnFilter();

            $('html,body').animate({ scrollTop: $('#table-container').offset().top - 150 }, 1000);
        }
        return false;
    });

    $('form').submit(function () {
        return false;
    })
}
function openModal() {
    $('#custom-modal-container').modal('show');
}
function closeModal() {
    $('#custom-modal-container').modal('hide');
}
function openModalSec() {
    $('#custom-modal-improv-container').modal('show');
}
function closeModalSec() {
    $('#custom-modal-improv-container').modal('hide');
}
function reDrawDatatable() {
    if (datatableInstance) {
        datatableInstance.fnPageChange(getCurrentPage() - 1);
    }
}
function getCurrentPage() {
    return parseInt($('.pagination .paginate_button.active a').html());
}


// load combos callbacks

function getCampus(data) {
    return loadCombo('#CampusId', reportCombosUrls.getCampus, getData());
}

function getAnio(data) {
    return loadCombo('#Anio', reportCombosUrls.getAnio, getData());
}

function getModalidades(data) {
    return loadCombo('#IdModalidad', reportCombosUrls.getModalidades, getData());
}

function getCampusBetweenCycles(data) {
    return loadCombo('#CampusId', reportCombosUrls.getCampusBetweenCycles, getData());
}
function getCyclesForModality(data) {
    var _data = getData();
    setFirstValue(_data, 'ModalityId', data);
    return loadCombo('#CycleId', reportCombosUrls.getCyclesFromForModality, _data);
}
function getCyclesFromForModality(data) {
    var _data = getData();
    console.log(_data);
    console.log(data);
    setFirstValue(_data, 'ModalityId', data);
    return loadCombo('#CycleFromId', reportCombosUrls.getCyclesFromForModality, _data);
}
function getCyclesToForModality(data) {
    var _data = getData();
    setFirstValue(_data, 'ModalityId', data);
    setFirstValue(_data, 'CycleFromId', data);
    return loadCombo('#CycleToId', reportCombosUrls.getCyclesToForModality, _data);
}
function getModulesForCycleForModality(data) {
    var _data = getData();
    setFirstValue(_data, 'ModalityId', data);
    setFirstValue(_data, 'CycleId', data);
    return loadCombo('#ModuleId', reportCombosUrls.getModulesForCycleForModality, _data);
}
function getModulesBetweenCyclesForModality(data) {
    var _data = getData();
    setFirstValue(_data, 'ModalityId', data);
    setFirstValue(_data, 'CycleFromId', data);
    setFirstValue(_data, 'CycleToId', data);
    return loadCombo('#ModuleId', reportCombosUrls.getModulesBetweenCyclesForModality, _data);
}

function GetModalidades(data) {
    var _data = getData();
    setFirstValue(_data, 'IdModalidad', data);
    return loadCombo('#CareerId', reportCombosUrls.GetCarreras, _data);

}

function GetComisiones(data) {
    var _data = getData();
    setFirstValue(_data, 'CareerId', data);
    setFirstValue(_data, 'Anio', data);
    return loadCombo('#CommissionId', reportCombosUrls.GetComisiones, _data);
}

function GetInstrumentos(data) {
    var _data = getData();
    setFirstValue(_data, 'Anio', data);
    setFirstValue(_data, 'IdModalidad', data);
    setFirstValue(_data, 'CareerId', data);
    return load('#InstrumentId', reportCombosUrls.GetInstrumentos, _data);
}
function GetContituyentes(data) {
    var _data = getData();
    setFirstValue(_data, 'InstrumentId', data);
    return load('#ConstituentId', reportCombosUrls.GetContituyentes, _data);
}
function GetCarreras(data) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#CareerId', reportCombosUrls.getCareers, _data);
}

function getCareers(data) {
    var _data = getData();
    setFirstValue(_data, 'ModalityId', data);
    return loadCombo('#CareerId', reportCombosUrls.GetCarreras, _data);
}
function getAccreditationTypes(data) {
    var _data = getData();
    //console.log(_data);
    //console.log(data);
    setFirstValue(_data, 'CycleId', data);
    return loadCombo('#AccreditationTypeId', reportCombosUrls.getAccreditationTypes, _data);
}

function getAccreditationTypesBetweenCycles(data) {
    var _data = getData();
    setFirstValue(_data, 'CycleFromId', data);
    setFirstValue(_data, 'CycleToId', data);
    return loadCombo('#AccreditationTypeId', reportCombosUrls.getAccreditationTypesBetweenCycles, _data);
}
function getCommissionsByCareer(data) {
    var _data = getData();
    
    setFirstValue(_data, 'AccreditationTypeId', data);
    setFirstValue(_data, 'CareerId', data);
    setFirstValue(_data, 'CycleId', data);
    return loadCombo('#CommissionId', reportCombosUrls.getComissionsByCareer, _data);
}
function getComissionsBetweenCycles(data) {
    var _data = getData();
    setFirstValue(_data, 'AccreditationTypeId', data);
    return loadCombo('#CommissionId', reportCombosUrls.getComissionsBetweenCycles, _data);
}
function getFormationCourses(data) {
    console.log(data)
    return loadCombo('#CourseId', reportCombosUrls.getFormationCourses, getData(), '', true);
}
function getVerificationCourses(data) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#CourseId', reportCombosUrls.getVerificationCourses, _data, '', true);
}
function getControlCourses(data) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#CourseId', reportCombosUrls.getControlCourses, _data, '', true);
}
function getDiscoverCourses(data) {
    var course = $('#CourseId'),
        constituentProfessor = $('#constituent-profesor'),
        constituentGraduates = $('#constituent-graduates'),
        constituent = $('#ConstituentId');

    if (constituent.val() == constituentProfessor.data('id')) {
        course.attr('disabled', true);
        course.html('<option value="">' + constituentProfessor.data('message') + '<option>');
        course.select2();
        var d = $.Deferred();
        setTimeout(function () { d.resolve(data); }, 100);
        return d.promise();
    } else if (constituent.val() == constituentGraduates.data('id')) {
        course.attr('disabled', true);
        course.html('<option value="">' + constituentGraduates.data('message') + '<option>');
        course.select2();
        var d = $.Deferred();
        setTimeout(function () { d.resolve(data); }, 100);
        return d.promise();
    } else if (constituent.val() == '') {
        course.attr('disabled', true);
        course.html('<option value="">' + $('#option-all').val() + '</option>');
        var d = $.Deferred();
        setTimeout(function () { d.resolve(data); }, 100);
        return d.promise();
    } else {
        course.removeAttr('disabled');
    }

    var _data = getData();
    setFirstValueWithCallback(_data, 'StudentOutcomeId', data, function (firstValue, array) {
        return array.length > 1 ? 0 : firstValue;
    });
    return loadCombo('#CourseId', reportCombosUrls.getDiscoverCourses, _data, $('#option-all').val(), true);
}
function getCourseByOutcomeType(data, allOption) {
    if (!allOption) {
        allOption = '';
    }
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#CourseId', reportCombosUrls.getCourseByOutcomeType, _data, '', true);
}
function getStudentOutcomesBetweenCycles(data) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#StudentOutcomeId', reportCombosUrls.getStudentOutcomesBetweenCycles, _data); 
}
function getStudentOutcomes(data, allOption) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#StudentOutcomeId', reportCombosUrls.getStudentOutcomes, _data, allOption);
}
function getSOSemaphore(data, allOption) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#StudentOutcomeId', reportCombosUrls.getSOSemaphore, _data, allOption);
}
function getStudentOutcomesByConstituent(data, allOption) {
    var _data = getData();
    switch ($('#ConstituentId').val()) {
        case '1': case '3':
            _data.OutcomeTypeName = 'Control';
            break;
        case '4':
            _data.OutcomeTypeName = 'Verification';
            break;
    }
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#StudentOutcomeId', reportCombosUrls.getStudentOutcomes, _data, allOption);
}
function getStudentOutcomeByCommission(data, allOption) {
    var _data = getData();
    setFirstValue(_data, 'CommissionId', data);
    return loadCombo('#StudentOutcomeId', reportCombosUrls.getStudentOutcomeByCommission, _data, allOption);
}
function getAcceptanceLevel(data) {
    return loadCombo('#AcceptanceLevelId', reportCombosUrls.getAcceptanceLevel, getData());
}
function getInstruments(data, allOption) {
    if (!allOption) {
        allOption = true;
    }
    return loadCombo('#InstrumentId', reportCombosUrls.getInstruments, getData(), allOption);
}