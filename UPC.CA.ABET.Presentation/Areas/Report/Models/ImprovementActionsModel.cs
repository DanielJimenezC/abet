﻿using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ImprovementActions;
using IFCStatus = UPC.CA.ABET.Helpers.ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class ImprovementActionsModel : GenericModel
    {
        public ImprovementActionsModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
        }

        public void LoadDropDownListsConsultControlReport(ConsultControlReportViewModel ViewModel)
        {
            ViewModel.OutcomeTypeName = "Control";
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(0, FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer, 0, CommissionID: model.commissions.FirstKey(), CycleFromID: FirstIdCycle, CycleToID: FirstIdCycle, CourseOutcomeTypeName: "Control");
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycle, FirstIdCareer);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
        }
        public void LoadDropDownListsConsultVerificationReport(ConsultVerificationReportViewModel ViewModel)
        {
            ViewModel.OutcomeTypeName = "Verification";
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(0, FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer, 0, CommissionID: model.commissions.FirstKey(), CycleFromID: FirstIdCycle, CycleToID: FirstIdCycle, CourseOutcomeTypeName: "Verification");
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycle, FirstIdCareer);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
        }
        public void LoadDropDownListsManageControlReport(ManageViewModel ViewModel)
        {
            ViewModel.InstrumentId = model.DbContext.Instrumento.Where(i => i.Acronimo.ToUpper() == "RC").Select(i => i.IdInstrumento).FirstOrDefault();
            ViewModel.DropDownListFindings = model.DbContext.Database
                .SqlQuery<ComboItem>("ListarComboHallazgosPorInstrumento {0}, {1}", ViewModel.InstrumentId, model.CurrentCulture)
                .ToSelectList();
        }
        public void LoadDropDownListsManageVerificationReport(ManageViewModel ViewModel)
        {
            ViewModel.InstrumentId = model.DbContext.Instrumento.Where(i => i.Acronimo.ToUpper() == "RV").Select(i => i.IdInstrumento).FirstOrDefault();
            ViewModel.DropDownListFindings = model.DbContext.Database
                .SqlQuery<ComboItem>("ListarComboHallazgosPorInstrumento {0}, {1}", ViewModel.InstrumentId, model.CurrentCulture)
                .ToSelectList();
        }

        public ManageViewModel LoadManageViewModel(int? ImprovementActionId)
        {
            var viewModel = new ManageViewModel();

            if (ImprovementActionId.HasValue)
            {
                var improvementAction = model.DbContext.AccionMejora.Find(ImprovementActionId);
                if (improvementAction != null)
                {
                    viewModel.ImprovementActionId = ImprovementActionId.Value;
                    viewModel.Code = improvementAction.Codigo;
                    viewModel.Description = improvementAction.DescripcionEspanol;
                    viewModel.FindingsIds = improvementAction.HallazgoAccionMejora
                        .Select(ham => ham.Hallazgos)
                        .GroupBy(h => h.Codigo)
                        .Select(gh => gh.Max(h => h.IdHallazgo))
                        .ToArray();
                    viewModel.SelectedValues = string.Join(",", viewModel.FindingsIds);
                }
            }

            return viewModel;
        }

        public IPagedList<ListImprovementActionViewModel> LoadDataConsultControlReport(int? page = null, ConsultControlReportViewModel ViewModel = null)
        {
            if (ViewModel == null)
            {
                ViewModel = new ConsultControlReportViewModel();
            }

            var data = model.DbContext.Database.SqlQuery<ListImprovementActionViewModel>(
                "ListarConsultarAccionesDeMejora 'RC', {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                model.CurrentCulture,
                ViewModel.CycleId ?? 0,
                ViewModel.AcceptanceLevelId ?? 0,
                ViewModel.CareerId,
                ViewModel.CommissionId,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.CourseId ?? 0).AsEnumerable();

            return data.ToPagedList(page ?? 1, 10);
        }
        public IPagedList<ListImprovementActionViewModel> LoadDataConsultVerificationReport(int? page = null, ConsultVerificationReportViewModel ViewModel = null)
        {
            if (ViewModel == null)
            {
                ViewModel = new ConsultVerificationReportViewModel();
            }

            var data = model.DbContext.Database.SqlQuery<ListImprovementActionViewModel>(
                "ListarConsultarAccionesDeMejora 'RV', {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                model.CurrentCulture,
                ViewModel.CycleId ?? 0,
                ViewModel.AcceptanceLevelId ?? 0,
                ViewModel.CareerId,
                ViewModel.CommissionId,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.CourseId ?? 0).AsEnumerable();

            return data.ToPagedList(page ?? 1, 10);
        }

        public DeleteViewModel GetDeleteImprovementViewModel(int instrumentId)
        {
            return new DeleteViewModel
            {
                Id = instrumentId,
                LanguageCulture = model.CurrentCulture,
                CanDelete = model.DbContext.AccionMejora.Any(am => am.IdAccionMejora == instrumentId && am.Estado == IFCStatus.PENDIENTE.CODIGO && !am.AccionMejoraPlanAccion.Any())
            };
        }

        public Task DeleteImprovementAction(DeleteViewModel ViewModel)
        {
            var improvementAction = model.DbContext.AccionMejora.Find(ViewModel.Id);

            if (improvementAction != null)
            {
                var improvementActions = model.DbContext.AccionMejora.Where(am => am.Codigo == improvementAction.Codigo).ToList();

                for (int i = improvementActions.Count - 1; i > -1; i--)
                {
                    var item = improvementActions[i];

                    if (item.HallazgoAccionMejora.Any())
                    {
                        var findingImprovementAction = item.HallazgoAccionMejora.First();
                        model.DbContext.HallazgoAccionMejora.Remove(findingImprovementAction);
                        model.DbContext.AccionMejora.Remove(item);
                    }
                }
            }

            return model.DbContext.SaveChangesAsync();
        }
        public Task SaveImprovementActions(ManageViewModel ViewModel)
        {
            if (ViewModel.FindingsIds.Any())
            {
                if (ViewModel.ImprovementActionId == 0)
                {
                    var firstFindingId = ViewModel.FindingsIds.First();
                    var finding = model.DbContext.Hallazgos.Find(firstFindingId);
                    //var find = model.DbContext.Hallazgo.Find(firstFindingId);

                    if (finding != null)
                    {
                        var instrumentAcronym = model.DbContext.Instrumento.Find(finding.ConstituyenteInstrumento.IdInstrumento).Acronimo;
                        //var instrumentAcronym = model.DbContext.Instrumento.Find(find.IdInstrumento).Acronimo;

                        var courseCode = model.DbContext.Curso.Find(finding.IdCurso).Codigo;
                        //var courseCode = model.DbContext.Curso.Find(find.IdCurso).Codigo;

                        // var cycle = model.DbContext.PeriodoAcademico.Find(finding.IdPeriodoAcademico).CicloAcademico;
                        var cycle = model.DbContext.PeriodoAcademico.Find(finding.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico).CicloAcademico;
                        //var cycle = model.DbContext.PeriodoAcademico.Find(find.SubModalidadPeriodoAcademico.IdPeriodoAcademico).CicloAcademico;

                        var lastImprovementActionCode = model.DbContext.HallazgoAccionMejora
                            .Where(ham => ham.IdHallazgo == finding.IdHallazgo)
                            .Select(ham => ham.AccionMejora.Codigo)
                            .Distinct()
                            .OrderByDescending(c => c)
                            .FirstOrDefault();
                        var correlativeNumber = (!string.IsNullOrWhiteSpace(lastImprovementActionCode) && lastImprovementActionCode.Length > 3 ? lastImprovementActionCode.Substring(lastImprovementActionCode.Length - 3, 3).ToInteger() : 0) + 1;


                        var accionMejora = model.DbContext.AccionMejora.Add(new AccionMejora
                        {
                            DescripcionEspanol = ViewModel.Description,
                            DescripcionIngles = ViewModel.Description,
                            Estado = IFCStatus.PENDIENTE.CODIGO, //ViewModel.Status,
                            Codigo = string.Format("{0}-A-{1}-{2}-{3}", instrumentAcronym, courseCode, cycle.Insert(4, "-"), correlativeNumber.ToString("D3"))
                        });

                        var findings = new List<Hallazgos> { finding };
                        //var findings = new List<Hallazgo> { find };

                        for (int i = 1; i < ViewModel.FindingsIds.Length; i++)
                        {
                            var _finding = model.DbContext.Hallazgos.Find(ViewModel.FindingsIds[i]);
                            //var _finding = model.DbContext.Hallazgo.Find(ViewModel.FindingsIds[i]);
                            if (_finding != null)
                            {
                                findings.Add(_finding);
                            }
                        }

                        foreach (var f in findings)
                        {
                            foreach (var item in model.DbContext.Hallazgos.Where(h => h.Codigo == f.Codigo).AsEnumerable())
                            {
                                item.HallazgoAccionMejora.Add(new HallazgoAccionMejora
                                {
                                    IdHallazgo = f.IdHallazgo,                    
                                    AccionMejora = accionMejora
                                });
                            }
                        }
                    }
                }
                else
                {
                    var improvementAction = model.DbContext.AccionMejora.Find(ViewModel.ImprovementActionId);
                    if (improvementAction != null)
                    {
                        improvementAction.Codigo = ViewModel.Code;
                        improvementAction.DescripcionEspanol = ViewModel.Description;
                        improvementAction.DescripcionIngles = ViewModel.Description;

                        var currentFindingsIds = improvementAction.HallazgoAccionMejora.Select(ham => ham.IdHallazgo).ToArray();

                        var deleteFindingsIds = currentFindingsIds.Except(ViewModel.FindingsIds).ToArray();
                        var findingsToDelete = model.DbContext.HallazgoAccionMejora.Where(ham => deleteFindingsIds.Contains(ham.IdHallazgo) && ham.IdAccionMejora == ViewModel.ImprovementActionId);
                        model.DbContext.HallazgoAccionMejora.RemoveRange(findingsToDelete);

                        var newFingindsIds = ViewModel.FindingsIds.Except(currentFindingsIds).ToArray();
                        foreach (var id in newFingindsIds)
                        {
                            var finding = model.DbContext.Hallazgo.Where(h => h.IdHallazgo == id).FirstOrDefault();
                            foreach (var item in model.DbContext.Hallazgo.Where(h => h.Codigo == finding.Codigo).AsEnumerable())
                            {
                                improvementAction.HallazgoAccionMejora.Add(new HallazgoAccionMejora
                                {
                                    IdHallazgo = item.IdHallazgo,
                                    IdOutcome = item.IdOutcome
                                });
                            }
                        }

                    }
                }
            }
            


            return model.DbContext.SaveChangesAsync();
        }
    }
}