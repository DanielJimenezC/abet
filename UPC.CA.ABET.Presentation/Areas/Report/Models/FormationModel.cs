﻿using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Formation;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class FormationModel : GenericModel
    {
        public FormationModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "RF";
            this.model.OutcomeTypeName = "Formacion";
        }

        public void LoadDropDownListsByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListCyclesToForModality = model.DropDownListCyclesToForModality(model.cyclesFrom.FirstKey(), 0);
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(model.cyclesFrom.FirstKey());
            ViewModel.DropDownListCampus = model.DropDownListCampus(model.cyclesFrom.FirstKey());
            ViewModel.DropDownListCourse = model.DropDownListCourseOfFormation(FirstIdCareer, model.campus.FirstKey(), FirstIdCycle, FirstIdCycle);
        }
        public void LoadDropDownListsByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(model.cyclesFrom.FirstKey());
            ViewModel.DropDownListCampus = model.DropDownListCampus(model.cyclesFrom.FirstKey());
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevel();
        }

        public void ExistDataByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteFormacionCursoHistorico {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                ViewModel.LanguageCulture,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CourseId,
                ViewModel.CycleFromId,
                ViewModel.CycleToId,
                0,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteFormacionPorNivelDeAceptacion {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                ViewModel.LanguageCulture,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleId,
                ViewModel.AcceptanceLevelId.HasValue ? ViewModel.AcceptanceLevelId.Value : 0,
                0,
                ViewModel.ModuleId.HasValue ? ViewModel.ModuleId.Value : 0
                ).Any();
        }

        private class ProcedureResult
        {
            public int IdSubModalidadPeriodoAcademico { get; set; }
            public string CodigoCurso { get; set; }
            public string Curso { get; set; }
            public int IdCurso { get; set; }
            public int IdModulo { get; set; }
            public string Modulo { get; set; }
            public string CicloAcademico { get; set; }
            public string CodigoSeccion { get; set; }
            public string Sede { get; set; }
            public decimal? NotaPromedio { get; set; }
            public string Color { get; set; }
            public int? NivelAlcanzado { get; set; }
            public string NombreNivelAlcanzado { get; set; }
        }
    }
}