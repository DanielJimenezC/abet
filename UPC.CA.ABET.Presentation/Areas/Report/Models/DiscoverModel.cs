﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Discover;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.ReportBase;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class DiscoverModel : GenericModel
    {
        public DiscoverModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "RH";
        }

        public void LoadDropDownListsABET(AbetViewModel ViewModel)
        {
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycle);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(model.accreditationTypes.FirstKey(), FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListConstituent = model.DropDownListConstituent();
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcomeByCommission(model.commissions.FirstKey(), FirstIdCycle, model.accreditationTypes.FirstKey());
            ViewModel.DropDownListCourse = model.DropDownListCourseByConstituent(model.constituents.FirstKey(), FirstIdCareer, FirstIdCycle, model.campus.FirstKey(), model.commissions.FirstKey(), 0);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();

            ViewModel.DropDownListCriticality = model.DropDownListCriticality();


            ViewModel.AcceptanceLevelId = 1;

            /*string nombre = "NombreEspanol";
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var CurrentCulture = cookie == null ? Culture.ESPANOL : cookie.Value ?? Culture.ESPANOL;
            if (CurrentCulture == Culture.INGLES) nombre = "NombreIngles";

            ViewModel.DropDownListCriticity = new SelectList( model.DbContext.Criticidad , "IdCriticidad", nombre);*/

            LoadConstituentConstantsId(ViewModel);
        }
        public void LoadDropDownListsEISC(EiscViewModel ViewModel)
        {
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycle);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(model.accreditationTypes.FirstKey(), FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListConstituent = model.DropDownListConstituent();
            ViewModel.DropDownListCourse = model.DropDownListCourseByConstituent(model.constituents.FirstKey(), FirstIdCareer, FirstIdCycle, model.campus.FirstKey(), model.commissions.FirstKey(), 0);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
            ViewModel.DropDownListCriticality = model.DropDownListCriticality();
            ViewModel.AcceptanceLevelId = 1;
            LoadConstituentConstantsId(ViewModel);
        }
        public void LoadDropDownListsByInstrument(ByInstrumentViewModel ViewModel)
        {
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycle);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(model.accreditationTypes.FirstKey(), FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListConstituent = model.DropDownListConstituent();
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcomeByCommission(model.commissions.FirstKey(), FirstIdCycle, model.accreditationTypes.FirstKey());
            ViewModel.DropDownListCourse = model.DropDownListCourseByConstituent(model.constituents.FirstKey(), FirstIdCareer, FirstIdCycle, model.campus.FirstKey(), model.commissions.FirstKey(), 0);
            ViewModel.DropDownListInstrument = model.DropDownListInstrument(model.constituents.FirstKey());
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
            ViewModel.DropDownListCriticality = model.DropDownListCriticality();
            ViewModel.AcceptanceLevelId = 1;
            LoadConstituentConstantsId(ViewModel);
        }
        public void LoadDropDownListsImprovementActions(ImprovementActionsViewModel ViewModel)
        {
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType();
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(model.accreditationTypes.FirstKey(), FirstIdCareer, FirstIdCycle);
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListConstituent = model.DropDownListConstituent();
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcomeByCommission(model.commissions.FirstKey(), FirstIdCycle, model.accreditationTypes.FirstKey());
            ViewModel.DropDownListCourse = model.DropDownListCourseByConstituent(model.constituents.FirstKey(), FirstIdCareer, FirstIdCycle, model.campus.FirstKey(), model.commissions.FirstKey(), 0);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
            ViewModel.AcceptanceLevelId = 1;
            ViewModel.IncludeImprovementActions = true;
            LoadConstituentConstantsId(ViewModel);
        }

        public DataTableResponseViewModel<DiscoverTableViewModel> LoadTable<T>(T ViewModel, DataTableRequestViewModel DataTableViewModel) where T : class
        {
            var findings = GetFindingsTable(ViewModel as DiscoverViewModel, DataTableViewModel).ToList();

            var totalRecords = findings.Any() ? findings.First().TotalRows : 0;

            return new DataTableResponseViewModel<DiscoverTableViewModel>
            {
                data = findings,
                draw = DataTableViewModel.draw,
                recordsTotal = totalRecords,
                recordsFiltered = totalRecords
            };
        }

        public bool ExistData(DiscoverViewModel ViewModel)
        {
            return GetFindingsTable(ViewModel).Any();
        }

        private DbRawSqlQuery<DiscoverTableViewModel> GetFindingsTable(DiscoverViewModel ViewModel, DataTableRequestViewModel DataTableViewModel = null)
        {
            if (DataTableViewModel == null)
            {
                DataTableViewModel = new DataTableRequestViewModel();
            }

            var order = DataTableViewModel.order.CurrentOrdered();

            return model.DbContext.Database.SqlQuery<DiscoverTableViewModel>(
                "ReporteHallazgosTabla {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                ViewModel.CommissionId ?? 0,
                ViewModel.CareerId,
                ViewModel.CampusId?? 0,
                ViewModel.CycleId ?? 0,
                ViewModel.ConstituentId ?? 0,
                ViewModel.CourseId ?? 0,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.AcceptanceLevelId ?? 0,
                ViewModel.CriticalityId ?? 0,
                ViewModel.InstrumentId ?? 0,
                ViewModel.LanguageCulture,
                ViewModel.IncludeImprovementActions.HasValue && ViewModel.IncludeImprovementActions.Value ? 1 : 0,
                DataTableViewModel.start,
                DataTableViewModel.length,
                DataTableViewModel.search.value,
                order.column,
                order.dir);
        }

        private void LoadConstituentConstantsId(DiscoverViewModel ViewModel)
        {
            ViewModel.ConstituentProfesorId = model.DbContext.Constituyente.Where(c => c.NombreEspanol.ToUpper() == "EMPLEADOR").Select(c => c.IdConstituyente).FirstOrDefault();
            ViewModel.ConstituentGraduatesId = model.DbContext.Constituyente.Where(c => c.NombreEspanol.ToUpper() == "GRADUADOS").Select(c => c.IdConstituyente).FirstOrDefault();
        }
    }
}