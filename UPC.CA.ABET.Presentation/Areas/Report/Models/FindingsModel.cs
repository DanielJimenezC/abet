﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Findings;
using UPC.CA.ABET.Helpers;
using IFCStatus = UPC.CA.ABET.Helpers.ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class FindingsModel : GenericModel
    {
        public FindingsModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
        }

        public void LoadDropDownListsIndex(IndexViewModel ViewModel)
        {
            ViewModel.DropDownListInstrument = model.DropDownListInstrument(Acronyms: "RC,RV");
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevelFindings();
            ViewModel.AcceptanceLevelId = 1;
            ViewModel.RCInstrumentId = model.DbContext.Instrumento.Where(i => i.Acronimo == "RC").Select(i => i.IdInstrumento).FirstOrDefault();
            ViewModel.RVInstrumentId = model.DbContext.Instrumento.Where(i => i.Acronimo == "RV").Select(i => i.IdInstrumento).FirstOrDefault();
        }
        public void LoadDropDownListsImprovementActions(ManageImprovementActionViewModel ViewModel)
        {
            var status = new Dictionary<string, string> 
            {
                { IFCStatus.IMPLEMENTADO.CODIGO, IFCStatus.IMPLEMENTADO.TEXTO },
                { IFCStatus.PENDIENTE.CODIGO, IFCStatus.PENDIENTE.TEXTO },
                { IFCStatus.PROCESADO.CODIGO, IFCStatus.PROCESADO.TEXTO },
                { IFCStatus.RECHAZADO.CODIGO, IFCStatus.RECHAZADO.TEXTO }
            };

            ViewModel.StatusCombo = new SelectList(status, "Key", "Value");
        }

        public DeleteViewModel GetDeleteViewModel(int id)
        {
            var finding = model.DbContext.Hallazgos.Find(id) ?? new Hallazgos();
            return new DeleteViewModel
            {
                Id = id,
                LanguageCulture = model.CurrentCulture,
                CanDelete = !finding.HallazgoAccionMejora.Any(ham => ham.AccionMejora.Estado != IFCStatus.PENDIENTE.CODIGO)
            };
        }

        public DeleteViewModel GetDeleteImprovementViewModel(int FindingId, string UpdateTargetId, int ImprovementActionId)
        {
            return new DeleteViewModel
            {
                Id = ImprovementActionId,
                LanguageCulture = model.CurrentCulture,
                FindingId = FindingId,
                UpdateTargetId = string.Format("table-body-ia-{0}", FindingId),
                CanDelete = model.DbContext.AccionMejora.Any(am => am.IdAccionMejora == ImprovementActionId && am.Estado == IFCStatus.PENDIENTE.CODIGO)
            };
        }

        public UpdateStatusImprovementActionViewModel GetUpdateStatusImprovementActionViewModel(int FindingId, int ImprovementActionId)
        {
            return new UpdateStatusImprovementActionViewModel
            {
                ImprovementActionId = ImprovementActionId,
                LanguageCulture = model.CurrentCulture,
                UpdateTargetId = string.Format("table-body-ia-{0}", FindingId),
                Status = (GetAccionMejora(ImprovementActionId) ?? new AccionMejora()).Estado
            };
        }
        public void LoadUpdateStatusImprovementActionViewModel(UpdateStatusImprovementActionViewModel ViewModel)
        {
            ViewModel.StatusCombo = new SelectList(new Dictionary<string, string>
            {
                { IFCStatus.IMPLEMENTADO.CODIGO, IFCStatus.IMPLEMENTADO.TEXTO },
                { IFCStatus.PENDIENTE.CODIGO, IFCStatus.PENDIENTE.TEXTO },
                { IFCStatus.PROCESADO.CODIGO, IFCStatus.PROCESADO.TEXTO },
                { IFCStatus.RECHAZADO.CODIGO, IFCStatus.RECHAZADO.TEXTO }
            }, "Key", "Value");
        }

        public void LoadCoursesFindingsData(IndexViewModel ViewModel)
        {
            var data = new List<ListCoursesViewModel>();

            var connection = model.DbContext.Database.Connection;
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "ListarCursosHallazgos";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@IdCiclo", ViewModel.CycleId));
                command.Parameters.Add(new SqlParameter("@IdSede", ViewModel.CampusId?? 0));
                command.Parameters.Add(new SqlParameter("@IdCarrera", ViewModel.CareerId));
                command.Parameters.Add(new SqlParameter("@IdInstrumento", ViewModel.InstrumentId));
                command.Parameters.Add(new SqlParameter("@IdNivelAceptacion", ViewModel.AcceptanceLevelId ?? 0));
                command.Parameters.Add(new SqlParameter("@TipoOutcome", ViewModel.OutcomeTypeName));
                command.Parameters.Add(new SqlParameter("@IdIdioma", model.CurrentCulture));

                connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    var lastCourseId = 0;
                    var course = default(ListCoursesViewModel);
                    var acceptanceLevel = default(ListAcceptanceLevelViewModel);
                    while (reader.Read())
                    {
                        var courseId = reader.Value<int>("IdCurso");

                        System.Diagnostics.Debug.WriteLine(""+courseId);

                        if (course == null || course.CourseId != courseId)
                        {
                            course = new ListCoursesViewModel
                            {
                                CourseId = courseId,
                                CourseCode = reader.Value<string>("CodigoCurso"),
                            CourseName = reader.Value<string>("NombreCurso")
                            };
                            System.Diagnostics.Debug.WriteLine("-1. " + course.CourseCode);
                            System.Diagnostics.Debug.WriteLine("-2. " + course.CourseName);
                         
                            data.Add(course);

                            acceptanceLevel = new ListAcceptanceLevelViewModel();
                        }

                   
                        var acceptanceLevelName = reader.Value<string>("NivelAceptacion");
                        System.Diagnostics.Debug.WriteLine("-3. " + acceptanceLevelName);
                        if (courseId != lastCourseId || acceptanceLevel == null || acceptanceLevel.AcceptanceLevelName != acceptanceLevelName)
                        {
                            acceptanceLevel = new ListAcceptanceLevelViewModel
                            {
                                AcceptanceLevelName = acceptanceLevelName,
                                FindingAcceptanceLevelId = reader.Value<int>("IdNivelAceptacionHallazgo"),
                                Color = reader.Value<string>("Color"),
                                StudentQuantity = reader.Value<int>("CantidadAlumnos")
                            };
                            System.Diagnostics.Debug.WriteLine("-4. " + acceptanceLevel.FindingAcceptanceLevelId);
                            System.Diagnostics.Debug.WriteLine("-5. " + acceptanceLevel.Color);
                            System.Diagnostics.Debug.WriteLine("-6. " + acceptanceLevel.StudentQuantity);
                            course.AcceptanceLevels.Add(acceptanceLevel);
                        }
                        
                        var findingId = reader.ValueNullable<int>("IdHallazgo");
                        System.Diagnostics.Debug.WriteLine("-7. " + findingId);
                        if (findingId.HasValue)
                        {
                            string nombreCriticidad= "";
                            int IdCriticidad = reader.Value<int>("Criticidad");
                            System.Diagnostics.Debug.WriteLine("-7.1 " + IdCriticidad);
                            if (model.CurrentCulture == "en-US")
                            {
                                 nombreCriticidad = model.DbContext.Criticidad.FirstOrDefault(x => x.IdCriticidad == IdCriticidad).NombreIngles;
                            }
                            else
                            {
                                nombreCriticidad = model.DbContext.Criticidad.FirstOrDefault(x => x.IdCriticidad == IdCriticidad).NombreEspanol;
                            }

                            acceptanceLevel.Findings.Add(new ListFindingsViewModel 
                            {
                                FindingId = findingId.Value,
                                FindingCode = reader.Value<string>("Codigo"),
                                FindingDescription = reader.Value<string>("Descripcion"),
                                FindingCriticalityLevel = IdCriticidad,
                                FindingCriticalityLevelName = nombreCriticidad
                            });    
                        }

                        lastCourseId = courseId;

                        System.Diagnostics.Debug.WriteLine("FIN");
                    }
                }

                connection.Close();
            }

            ViewModel.Courses = data;
        }
        public void LoadManageViewModel(ManageViewModel ViewModel)
        {
            if (ViewModel.FindingId > 0)
            {
                var finding = GetHallazgo(ViewModel.FindingId);
                if (finding != null)
                {
                    ViewModel.CampusId = finding.IdSede ?? 0;
                    ViewModel.CareerId = finding.IdCarrera ?? 0;
                    //ViewModel.CycleId = finding.IdPeriodoAcademico;
                    ViewModel.CycleId = finding.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                    ViewModel.InstrumentId = finding.IdInstrumento;
                    ViewModel.CourseId = finding.IdCurso ?? 0;
                    ViewModel.AcceptanceLevelId = finding.IdNivelAceptacionHallazgo ?? 0;
                    ViewModel.FindingId = finding.IdHallazgo;
                    ViewModel.Code = finding.Codigo;
                    ViewModel.IdCriticidad = finding.IdCriticidad;
                    ViewModel.Description = finding.DescripcionEspanol;
                    ViewModel.SubmitButtonId = string.Format("button-finding-{0}{1}", finding.IdCurso, finding.IdNivelAceptacionHallazgo);
                    ViewModel.UpdateTargetId = string.Format("table-body-finding-{0}{1}", finding.IdCurso, finding.IdNivelAceptacionHallazgo);
                }
            }
        }
        public Task Save(ManageViewModel ViewModel)
        {
            if (ViewModel.FindingId == 0)
            {
                //var curriculumIds = model.DbContext.MallaCurricularPeriodoAcademico
                //     .Where(mcpa => mcpa.MallaCurricular.IdCarrera == ViewModel.CareerId && mcpa.IdPeriodoAcademico == ViewModel.CycleId)
                //     .Select(mcpa => mcpa.IdMallaCurricular)
                //     .ToList();

                var curriculumIds = model.DbContext.MallaCurricularPeriodoAcademico
                    .Where(mcpa => mcpa.MallaCurricular.IdCarrera == ViewModel.CareerId && mcpa.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId)
                    .Select(mcpa => mcpa.IdMallaCurricular).ToList();

                //var carrerCurrentCommissions = model.DbContext.CarreraComision
                //    .Where(cc => cc.IdCarrera == ViewModel.CareerId && cc.IdPeriodoAcademico == ViewModel.CycleId)
                //    .Select(cc => cc.Comision)
                //    .Distinct()
                //    .ToList();

                var carrerCurrentCommissions = model.DbContext.CarreraComision.Where(cc => cc.IdCarrera == ViewModel.CareerId && cc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId)
                    .Select(cc => cc.Comision)
                    .Distinct()
                    .ToList(); 

                var idsCommissions = carrerCurrentCommissions.Select(ccc => ccc.IdComision);


                //var outcomeCommissions = model.DbContext.MallaCocosDetalle
                //    .Where(mcd => curriculumIds.Contains(mcd.CursoMallaCurricular.IdMallaCurricular) && mcd.CursoMallaCurricular.IdCurso == ViewModel.CourseId)
                //    .Select(mcd => mcd.OutcomeComision)
                //    .Where(oc => oc.IdPeriodoAcademico == ViewModel.CycleId && idsCommissions.Contains(oc.IdComision))
                //    .ToList();

                var outcomeCommissions = model.DbContext.MallaCocosDetalle
              .Where(mcd => curriculumIds.Contains(mcd.CursoMallaCurricular.IdMallaCurricular) && mcd.CursoMallaCurricular.IdCurso == ViewModel.CourseId)
              .Select(mcd => mcd.OutcomeComision)
              .Where(oc => oc.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId && idsCommissions.Contains(oc.IdComision))
              .ToList();


                var constituentId = model.DbContext.Instrumento
                    .Where(i => i.IdInstrumento == ViewModel.InstrumentId)
                    .SelectMany(i => i.ConstituyenteInstrumento.Select(ci => ci.Constituyente))
                    .Select(c => c.IdConstituyente)
                    .FirstOrDefault();

                var instrumentAcronym = model.DbContext.Instrumento.Find(ViewModel.InstrumentId).Acronimo;
                var courseCode = model.DbContext.Curso.Find(ViewModel.CourseId).Codigo;
                var cycle = model.DbContext.PeriodoAcademico.Find(ViewModel.CycleId).CicloAcademico;
                var campus = model.DbContext.Sede.Find(ViewModel.CampusId).Codigo;



                //var lastFindingCode = model.DbContext.Hallazgo
                //    .Where(h =>
                //        h.IdInstrumento == ViewModel.InstrumentId
                //        && h.IdCarrera == ViewModel.CareerId
                //        && h.IdCurso == ViewModel.CourseId
                //        && h.IdSede == ViewModel.CampusId
                //        && h.IdPeriodoAcademico == ViewModel.CycleId)
                //    .Select(h => h.Codigo)
                //    .Distinct()
                //    .OrderByDescending(c => c)
                //    .FirstOrDefault();

                var lastFindingCode = model.DbContext.Hallazgo
                    .Where(h =>
                        h.IdInstrumento == ViewModel.InstrumentId
                        && h.IdCarrera == ViewModel.CareerId
                        && h.IdCurso == ViewModel.CourseId
                        && h.IdSede == ViewModel.CampusId
                        && h.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId)
                    .Select(h => h.Codigo)
                    .Distinct()
                    .OrderByDescending(c => c)
                    .FirstOrDefault();//

                var correlativeNumber = (!string.IsNullOrWhiteSpace(lastFindingCode) && lastFindingCode.Length > 3 ? lastFindingCode.Substring(lastFindingCode.Length - 3, 3).ToInteger() : 0) + 1;


                int idsubmodalida = (from smpa in model.DbContext.SubModalidadPeriodoAcademico
                                    where smpa.IdPeriodoAcademico == ViewModel.CycleId
                                    select smpa.IdSubModalidadPeriodoAcademico).FirstOrDefault();
                foreach (var outcomeComision in outcomeCommissions)
                {
                    model.DbContext.Hallazgo.Add(new Hallazgo
                    {
                        IdAcreditadora = outcomeComision.Comision.AcreditadoraPeriodoAcademico.IdAcreditadora,
                        IdConstituyente = constituentId,
                        IdInstrumento = ViewModel.InstrumentId,
                        IdCarrera = ViewModel.CareerId,
                        IdCriticidad = ViewModel.IdCriticidad.Value,
                        IdComision = outcomeComision.IdComision,
                        //IdPeriodoAcademico = ViewModel.CycleId,
                        IdSubModalidadPeriodoAcademico = idsubmodalida,                                                              
                        IdCurso = ViewModel.CourseId,
                        IdOutcome = outcomeComision.IdOutcome,
                        IdNivelAceptacionHallazgo = ViewModel.AcceptanceLevelId,
                        IdSede = ViewModel.CampusId,
                        DescripcionEspanol = ViewModel.Description,
                        DescripcionIngles = ViewModel.Description,
                        FechaRegistro = DateTime.Now,
                        Codigo = string.Format("{0}-F-{1}-{2}-{3}-{4}", instrumentAcronym, courseCode, cycle.Insert(4, "-"), campus, correlativeNumber.ToString("D3"))
                    });
                }
            }
            else
            {
                var finding = GetHallazgo(ViewModel.FindingId);
                if (finding != null)
                {
                    foreach (var item in model.DbContext.Hallazgo.Where(h => h.Codigo == finding.Codigo).AsEnumerable())
                    {
                        item.DescripcionEspanol = ViewModel.Description;
                        item.DescripcionIngles = ViewModel.Description;
                        item.IdCriticidad = ViewModel.IdCriticidad.Value;
                    } 
                }
            }

            return model.DbContext.SaveChangesAsync();
        }
        public IEnumerable<ListFindingsViewModel> ListFindings(ManageViewModel ViewModel)
        {
            int idsubmodalida = (from smpa in model.DbContext.SubModalidadPeriodoAcademico
                                 where smpa.IdPeriodoAcademico == ViewModel.CycleId
                                 select smpa.IdSubModalidadPeriodoAcademico).FirstOrDefault();
            return model.DbContext.Database
                .SqlQuery<FindingCourseAcceptanceLevel>("ListarHallazgosPorCursoNivelAceptacion {0}, {1}, {2}, {3}, {4}, {5}",
                    idsubmodalida, ViewModel.CampusId, ViewModel.CareerId, ViewModel.InstrumentId, ViewModel.CourseId, ViewModel.AcceptanceLevelId)
                .ToList()
                .Select(f => new ListFindingsViewModel
                {
                    FindingId = f.IdHallazgo,
                    FindingCode = f.Codigo,
                    FindingDescription = f.Descripcion,
                    FindingCriticalityLevel = f.Criticidad,
                    FindingCriticalityLevelName = model.DbContext.Criticidad.FirstOrDefault(x => x.IdCriticidad == f.Criticidad).NombreEspanol

                });

        }
        public Task Delete(DeleteViewModel ViewModel)
        {
            var finding = GetHallazgo(ViewModel.Id);

            if (finding != null)
            {
                var findings = model.DbContext.Hallazgos.Where(h => h.Codigo == finding.Codigo).ToList();
                for (int i = findings.Count -1; i > -1; i--)
                {
                    var item = findings[i];
                    for (int j = item.HallazgoAccionMejora.Count - 1; j > -1; j--)
                    {
                        var hallazgoAccionMejora = item.HallazgoAccionMejora.ElementAt(j);
                        var accionMejora = hallazgoAccionMejora.AccionMejora;
                        model.DbContext.HallazgoAccionMejora.Remove(hallazgoAccionMejora);
                        if (accionMejora != null)
                        {
                            model.DbContext.AccionMejora.Remove(accionMejora);   
                        }
                    }

                    model.DbContext.Hallazgos.Remove(item);   
                }
            }

            return model.DbContext.SaveChangesAsync();
        }

        public ImprovementActionsViewModel GetImprovementActions(int FindingId)
        {
            var viewModel = new ImprovementActionsViewModel();
            var finding = GetHallazgo(FindingId);

            if (finding != null)
            {
                viewModel.FindingId = finding.IdHallazgo;
                viewModel.Code = finding.Codigo;
                viewModel.Description = finding.DescripcionEspanol;
            }

            viewModel.ImprovementActions = LoadImprovementActions(FindingId);

            return viewModel;
        }
        public void LoadManageImprovementActionViewModel(ManageImprovementActionViewModel ViewModel)
        {
            if (ViewModel.ImprovementActionId > 0)
            {
                var improvementAction = GetAccionMejora(ViewModel.ImprovementActionId);

                if (improvementAction != null)
                {
                    ViewModel.ImprovementActionId = improvementAction.IdAccionMejora;
                    ViewModel.Code = improvementAction.Codigo;
                    ViewModel.Status = improvementAction.Estado;
                    ViewModel.Description = improvementAction.DescripcionEspanol;
                    ViewModel.UpdateTargetId = string.Format("table-body-ia-{0}", ViewModel.FindingId);
                    ViewModel.SubmitButtonId = string.Format("button-ia-{0}", ViewModel.FindingId); ;
                }
            }
        }
        public IEnumerable<ListImprovementActionViewModel> LoadImprovementActions(int FindingId)
        {
            return model.DbContext.Database.SqlQuery<ListImprovementActionViewModel>("ListarAccionesDeMejora {0}", FindingId).AsEnumerable();
        }
       
        public Task SaveImprovementAction(ManageImprovementActionViewModel ViewModel)
        {
            if (ViewModel.ImprovementActionId == 0)
            {
                var finding = GetHallazgo(ViewModel.FindingId);

                if (finding != null)
                {
                    var constituentId = model.DbContext.Instrumento
                        .Where(i => i.IdInstrumento == finding.IdInstrumento)
                        .SelectMany(i => i.ConstituyenteInstrumento.Select(ci => ci.Constituyente))
                        .Select(c => c.IdConstituyente)
                        .FirstOrDefault();

                    var instrumentAcronym = model.DbContext.Instrumento.Find(finding.IdInstrumento).Acronimo;
                    var courseCode = model.DbContext.Curso.Find(finding.IdCurso).Codigo;

                    //var cycle = model.DbContext.PeriodoAcademico.Find(finding.IdPeriodoAcademico).CicloAcademico;
                    var cycle = model.DbContext.PeriodoAcademico.Find(finding.SubModalidadPeriodoAcademico.IdPeriodoAcademico).CicloAcademico;

                    var campus = model.DbContext.Sede.Find(finding.IdSede).Codigo;
                    var lastImprovementActionCode = model.DbContext.HallazgoAccionMejora
                        .Where(ham => ham.IdHallazgo == ViewModel.FindingId)
                        .Select(ham => ham.AccionMejora.Codigo)
                        .Distinct()
                        .OrderByDescending(c => c)
                        .FirstOrDefault();
                    var correlativeNumber = (!string.IsNullOrWhiteSpace(lastImprovementActionCode) && lastImprovementActionCode.Length > 3 ? lastImprovementActionCode.Substring(lastImprovementActionCode.Length - 3, 3).ToInteger() : 0) + 1;


                    var accionMejora = model.DbContext.AccionMejora.Add(new AccionMejora
                    {
                        DescripcionEspanol = ViewModel.Description,
                        DescripcionIngles = ViewModel.Description,
                        ParaPlan= true,
                        Estado = IFCStatus.PENDIENTE.CODIGO, //ViewModel.Status,
                        Codigo = string.Format("{0}-A-{1}-{2}-{3}", instrumentAcronym, courseCode, cycle.Insert(4, "-"), correlativeNumber.ToString("D3"))
                    });

                    foreach (var item in model.DbContext.Hallazgos.Where(h => h.Codigo == finding.Codigo).AsEnumerable())
                    {
                        item.HallazgoAccionMejora.Add(new HallazgoAccionMejora
                        {
                            IdHallazgo = finding.IdHallazgo,
                            IdOutcome = finding.IdOutcome,
                            AccionMejora = accionMejora
                        });
                    } 
                }
            }
            else
            {
                var improvementAction = GetAccionMejora(ViewModel.ImprovementActionId);

                if (improvementAction != null)
                {
                    foreach (var item in model.DbContext.AccionMejora.Where(am => am.Codigo == improvementAction.Codigo).AsEnumerable())
                    {
                        item.DescripcionEspanol = ViewModel.Description;
                        item.DescripcionIngles = ViewModel.Description;   
                    }
                }
            }

            return model.DbContext.SaveChangesAsync();
        }
        public Task DeleteImprovementAction(DeleteViewModel ViewModel)
        {
            var improvementAction = GetAccionMejora(ViewModel.Id);

            if (improvementAction != null)
            {
                var improvementActions = model.DbContext.AccionMejora.Where(am => am.Codigo == improvementAction.Codigo).ToList();

                for (int i = improvementActions.Count - 1; i > -1; i--)
                {
                    var item = improvementActions[i];

                    if (item.HallazgoAccionMejora.Any())
                    {
                        var findingImprovementAction = item.HallazgoAccionMejora.First();
                        model.DbContext.HallazgoAccionMejora.Remove(findingImprovementAction);
                        model.DbContext.AccionMejora.Remove(item);
                    }
                }
            }

            return model.DbContext.SaveChangesAsync();
        }
        public Task UpdateStatusImprovementAction(UpdateStatusImprovementActionViewModel ViewModel)
        {
            var improvementAction = GetAccionMejora(ViewModel.ImprovementActionId);

            if (improvementAction != null)
            {
                var improvementActions = model.DbContext.AccionMejora.Where(am => am.Codigo == improvementAction.Codigo).ToList();
                improvementActions.ForEach(ia => ia.Estado = ViewModel.Status);
            }

            return model.DbContext.SaveChangesAsync();
        }

        private Hallazgo GetHallazgo(int id)
        {
            return model.DbContext.Hallazgo.Find(id);
        }
        private AccionMejora GetAccionMejora(int id)
        {
            return model.DbContext.AccionMejora.Find(id);
        }
    }

    public class FindingCourseAcceptanceLevel
    {
        public int IdHallazgo { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Criticidad { get; set; }
    }
}