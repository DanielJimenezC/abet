﻿using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Verification;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class VerificationModel : GenericModel
    {
        public VerificationModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "RV";
            this.model.OutcomeTypeName = "Verification";
        }

        public void LoadDropDownListsConsolidate(ConsolidateViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus(FirstIdCycleFrom);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevel();
        }
        public void LoadDropDownListsByCourse(ByCourseViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus(FirstIdCycleFrom);
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer, CampusID: model.campus.FirstKey(), CommissionID: model.commissions.FirstKey(), CycleFromID: FirstIdCycleFrom);
        }
        public void LoadDropDownListsByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);

            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus(FirstIdCycleFrom);
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevel();
        }
        public void LoadDropDownListsByHistoricOutcome(ByHistoricOutcomeViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListCyclesToForModality = model.DropDownListCyclesToForModality(FirstIdCycleFrom);
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationTypeBetweenCycles(FirstIdCycleFrom, FirstIdCycleTo);
            ViewModel.DropDownListCommission = model.DropDownListCommissionBetweenCycles(FirstIdAccreditationType, FirstIdCycleFrom, FirstIdCycleTo, FirstIdCareer);

            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampusBetweenCycles(FirstIdCycleFrom, FirstIdCycleTo);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcomeBetweenCycles(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCycleTo, FirstIdCareer, model.campus.FirstKey());
        }

        public void ExistDataByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteVerificacionPorNivelDeAceptacion {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId, 
                ViewModel.CommissionId, 
                ViewModel.CareerId, 
                ViewModel.CampusId ?? 0, 
                ViewModel.CycleId, 
                ViewModel.AcceptanceLevelId,
                ViewModel.CollegeId,
                0,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataByHistoricOutcome(ByHistoricOutcomeViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteVerificacionPorOutcomeHistorico {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleFromId,
                ViewModel.CycleToId,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.CollegeId,
                0, //SubModalidad
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataConsolidate(ConsolidateViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteVerificacionConsolidado {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId ?? 0,
                ViewModel.CareerId ?? 0,
                ViewModel.CampusId ?? 0,
                ViewModel.AcceptanceLevelId ?? 0,
                ViewModel.CycleId ?? 0,
                ViewModel.CollegeId,
                0,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataByCourse(ByCourseViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteVerificacionPorCurso {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleId,
                ViewModel.CourseId).Any();
        }

        public void ValidateTablesForReport()
        {
            model.DbContext.Database.ExecuteSqlCommand("EXEC ValidacionReporteVerificacion");
        }
        
        private class ProcedureResult
        {
            public int IdSubModalidadPeriodoAcademico { get; set; }
            public string Codigo { get; set; }
            public string Curso { get; set; }
            public int IdCurso { get; set; }
            public string Outcome { get; set; }
            public string DescripcionOutcome { get; set; }
            public int IdModulo { get; set; }
            public string Modulo { get; set; }
            public int IdSeccion { get; set; }
            public int IdAlumnoSeccion { get; set; }
            public int IdNivelAceptacion { get; set; }
            public int NivelAlcanzado { get; set; }
            public string Color { get; set; }
            public int NivelMaximo { get; set; }
            public string CicloAcademico { get; set; }
            public int Orden { get; set; }
        }
    }
}