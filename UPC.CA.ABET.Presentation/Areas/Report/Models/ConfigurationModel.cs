﻿using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Configuration;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class ConfigurationModel : GenericModel
    {
        public ConfigurationModel(ReportBaseModel Model, Controller Controller)
            : base(Model, Controller)
        {
        }

        #region Signatures

        public IPagedList<ListSignaturesViewModel> ListSignatures(int? page = null, SignaturesViewModel ViewModel = null)
        {
            if (ViewModel == null)
            {
                ViewModel = new SignaturesViewModel();
            }

            //var submodapa = ViewModel.SubModalidadPeriodoAcademicoId ?? 0;
            var cycleId = ViewModel.CycleId ?? 0;
            int idsubmoda = 0;
            if(cycleId != 0)
            {
                using (var context = new AbetEntities())
                {
                    idsubmoda = context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == cycleId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
                }
            }




            return model.DbContext.CargoReporteLogro
                .Where(x => idsubmoda == 0 || idsubmoda == x.IdSubModalidadPeriodoAcademico)
                .ToList()
                .Select(x => new ListSignaturesViewModel()
                {
                    PositionId = x.IdCargo,
                    AcademicPeriod = x.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico,
                    Position = x.Cargo,
                    Code = x.Codigo,
                    FullName = string.Format("{0} {1}", x.Nombres, x.Apellidos)

                })
                .ToPagedList(page ?? 1, 10);
        }

        public ManageSignatureViewModel GetSignature(int? signatureId)
        {
            var viewModel = new ManageSignatureViewModel();

            if (signatureId.HasValue)
            {
                var item = GetCargoReporteLogro(signatureId.Value);
                viewModel.PositionId = item.IdCargo;
                viewModel.CycleId = item.SubModalidadPeriodoAcademico.IdPeriodoAcademico;
                //Ver
                viewModel.SubModalidadPeriodoAcademicoId = item.IdSubModalidadPeriodoAcademico.ToInteger();
                //Ver
                viewModel.Position = item.Cargo;
                viewModel.Code = item.Codigo;
                viewModel.FirstNames = item.Nombres;
                viewModel.LastNames = item.Apellidos;
            }

            return viewModel;
        }

        public void LoadCombosManageSignatureViewModel(ManageSignatureViewModel ViewModel , int modalidad)
        {
           
            ViewModel.Cycles = model.DropDownListCycleforModality(modalidad.ToString(), 0);
        }

        public void ValidSignaturesPerCycle(ManageSignatureViewModel ViewModel)
        {
            // var semesterCount = model.DbContext.CargoReporteLogro.Where(x => x.IdPeriodoAcademico == ViewModel.CycleId && x.IdCargo != ViewModel.PositionId).Count();
            var semesterCount = model.DbContext.CargoReporteLogro.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId && x.IdCargo != ViewModel.PositionId).Count();

            var maxItems = AppSettingsHelper.GetAppSettings("ReportMaxItems").ToInteger();

            if (semesterCount >= maxItems)
            {
                (controller as Controller).ModelState.AddModelError("PositionId", string.Format("El máximo número de registros por ciclo es {0}", maxItems));
            }
        }

        public Task SaveSignature(ManageSignatureViewModel ViewModel)
        {
           
                if (ViewModel.PositionId == 0)
                {
                    var item = new CargoReporteLogro()
                    {
                        //IdPeriodoAcademico = ViewModel.CycleId,
                        IdSubModalidadPeriodoAcademico=  (from smpa in model.DbContext.SubModalidadPeriodoAcademico
                                                         where smpa.IdPeriodoAcademico==ViewModel.CycleId
                                                         select smpa.IdSubModalidadPeriodoAcademico).FirstOrDefault().ToInteger(),
 
                        Cargo = ViewModel.Position,
                        Nombres = ViewModel.FirstNames,
                        Apellidos = ViewModel.LastNames,
                        Codigo = ViewModel.Code
                    };

                    model.DbContext.CargoReporteLogro.Add(item);
                }
                else
                {
                    var item = GetCargoReporteLogro(ViewModel.PositionId);
                //item.IdPeriodoAcademico = ViewModel.CycleId;
                item.IdSubModalidadPeriodoAcademico = ViewModel.CycleId;

                    item.Cargo = ViewModel.Position;
                    item.Nombres = ViewModel.FirstNames;
                    item.Apellidos = ViewModel.LastNames;
                    item.Codigo = ViewModel.Code;
                }

            return model.DbContext.SaveChangesAsync();
        }

        public Task DeleteSignature(DeleteSignatureViewModel ViewModel)
        {
            var item = model.DbContext.CargoReporteLogro.FirstOrDefault(x => x.IdCargo == ViewModel.SignatureId);
            model.DbContext.CargoReporteLogro.Remove(item);
            return model.DbContext.SaveChangesAsync();
        }

        #endregion


        #region Process Information

        public List<ConversionOutcomeComision> ListConversions()
        {
            return model.DbContext.ConversionOutcomeComision.ToList();
        }

        public bool ProcessVerificationReportData(int? subModalidadPeriodoAcademicoId)
        {
            var resultadoActualizarTabla = model.DbContext.ValidacionReporteVerificacion(true, subModalidadPeriodoAcademicoId);
            var resultadoHallazgos = model.DbContext.Usp_CrearHallazgosRVAutomaticos(subModalidadPeriodoAcademicoId);

            if (resultadoActualizarTabla > 0 && resultadoHallazgos > 0)
                return true;

            return false;
        }

        public void ProcessControlReportData()
        {
            model.DbContext.Database.ExecuteSqlCommand("EXEC ValidacionReporteControl @p0", 1);
        }

        public void ProcessFormationReportData()
        {
            model.DbContext.Database.ExecuteSqlCommand("EXEC ValidacionReporteFormacion @p0", 1);
        }

        #endregion


        private CargoReporteLogro GetCargoReporteLogro(int id)
        {
            return model.DbContext.CargoReporteLogro.Find(id);
        }
    }
}