﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Report;
using UPC.CA.ABET.Models;
//using UPC.CA.ABET.Presentation.Areas.Report.Resources.Views.Achievement;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Achievement;
using UPC.CA.ABET.Presentation.Resources.Areas.Report;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class AchievementModel : GenericModel
    {

        public AchievementModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        { 
        }

        public void LoadDropDownListsByCompetences(ByCompetencesViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(model.cyclesFrom.FirstKey());
            ViewModel.DropDownListCareer = model.DropDownListCareers();
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            //ViewModel.DropDownListCycle = model.DropDownListCycle();
            ViewModel.DropDownListStudents = new List<ComboItem>().ToSelectList();
            ViewModel.Students = model.loadCombosLogic.ListExitStudents(FirstIdCycle, FirstIdCareer, model.campus.FirstKey());
        }

        public void LoadByCompetencesData(ByCompetencesViewModel ViewModel)
        {
            AbetEntities abet = new AbetEntities();
            var connection = model.DbContext.Database.Connection;
            var command = connection.CreateCommand();
            int idsubmoda = abet.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == ViewModel.CycleId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            command.CommandText = "ReporteLogroPorCompetencias";
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@IdCarrera", ViewModel.CareerId));
            command.Parameters.Add(new SqlParameter("@IdSede", ViewModel.CampusId?? 0));
            command.Parameters.Add(new SqlParameter("@IdSubModalidadPeriodoAcademico", idsubmoda));
            command.Parameters.Add(new SqlParameter("@IdAlumno", ViewModel.StudentId));
            command.Parameters.Add(new SqlParameter("@IdIdioma", ViewModel.LanguageCulture));
            command.Parameters.Add(new SqlParameter("@NombreProfesores", AppSettingsHelper.GetAppSettings("ProfesorsFullNames")));

            connection.Open();

            var reader = command.ExecuteReader();

            // Student information
            if (reader.Read())
            {
                ViewModel.Student = new StudentViewModel
                {
                    Career = reader["Carrera"] != DBNull.Value ? reader["Carrera"].ToString() : string.Empty,
                    Code = reader["Codigo"] != DBNull.Value ? reader["Codigo"].ToString() : string.Empty,
                    ExitCycle = reader["AnioEgreso"] != DBNull.Value ? reader["AnioEgreso"].ToString() : string.Empty,
                    FullName = reader["NombreCompleto"] != DBNull.Value ? reader["NombreCompleto"].ToString() : string.Empty
                };
            }

            #region Total verification courses
		 
            if (reader.NextResult())
	        {
		        ViewModel.TotalVerificationCourses = new List<string>();

                while (reader.Read())
	            {
                    ViewModel.TotalVerificationCourses.Add(reader["TotalCursosVerificacion"] != DBNull.Value ? reader["TotalCursosVerificacion"].ToString() : string.Empty);
	            }
	        }

	        #endregion

            #region Rubrics approveds
		 
            if (reader.NextResult())
	        {
                ViewModel.RubricApproveds = new List<RubricApproved>();

                while (reader.Read())
                {
                    ViewModel.RubricApproveds.Add(new RubricApproved 
                    {
                        Course = reader["Curso"] != DBNull.Value ? reader["Curso"].ToString() : string.Empty,
                        EvaluatorCode = reader["CodigoEvaluador"] != DBNull.Value ? reader["CodigoEvaluador"].ToString() : string.Empty,
                        EvaluationType = reader["TipoEvaluacion"] != DBNull.Value ? reader["TipoEvaluacion"].ToString() : string.Empty,
                        Grade = reader["NotaVigesimal"] != DBNull.Value ? reader["NotaVigesimal"].ToDecimal() : 0,
                        QualifiedRubricId = reader["IdRubricaCalificada"] != DBNull.Value ? reader["IdRubricaCalificada"].ToInteger() : 0,
                        Cycle = reader["CicloAcademico"] != DBNull.Value ? reader["CicloAcademico"].ToString() : string.Empty
                    });
                }
	        }

	        #endregion


            if (!(ViewModel.RubricApproveds != null && ViewModel.RubricApproveds.Any()))
            {
                ViewModel.Message = ReportResource.MessageNoRubricsApproved;
                goto CERRAR_CONEXION;
            }
            else
            {
                //var careersApproved = ViewModel.RubricApproveds.Select(ra => ra.Course);
                //var _careersNotApproved = ViewModel.TotalVerificationCourses.Except(careersApproved);

                //if (_careersNotApproved.Any())
                //{
                //    ViewModel.ExistData = false;
                //    ViewModel.Message = ReportResource.MessageMissingCareersApproved;
                //    goto CERRAR_CONEXION;
                //}

                var evaluationTypes = ViewModel.RubricApproveds.Select(ra => ra.EvaluationType);

                if (!evaluationTypes.Contains(ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA))
                {
                    ViewModel.ExistData = true;
                    ViewModel.Message = ReportResource.MessageMissingGeneralCompetences;
                    goto CERRAR_CONEXION;
                }

                if (!evaluationTypes.Contains(ConstantHelpers.RUBRICFILETYPE.EvaluationType.TF))
                {
                    ViewModel.ExistData = true;
                    ViewModel.Message = ReportResource.MessageMissingSpecificCompetences;
                    goto CERRAR_CONEXION;
                }
            }


            #region Competences

            var competences = new List<CompetenceViewModel>();

            if (reader.NextResult())
            {
                var competenceViewModel = default(CompetenceViewModel);

                while (reader.Read())
                {
                    var outcome = reader["Outcome"] != DBNull.Value ? reader["Outcome"].ToString() : string.Empty;
                    var commission = reader["Comision"] != DBNull.Value ? reader["Comision"].ToString() : string.Empty;

                    if (competenceViewModel == null || competenceViewModel.Commission != commission ||  competenceViewModel.Outcome != outcome)
                    {
                        competenceViewModel = new CompetenceViewModel
                        {
                            IdOutcome = reader["IdOutcome"] != DBNull.Value ? reader["IdOutcome"].ToInteger() : 0,
                            Outcome = outcome,
                            Color = reader["Color"] != DBNull.Value ? reader["Color"].ToString() : "transparent",
                            Description = reader["DescripcionOutcome"] != DBNull.Value ? reader["DescripcionOutcome"].ToString() : string.Empty,
                            MaximunLevel = reader["NivelMaximo"] != DBNull.Value ? reader["NivelMaximo"].ToInteger() : 0,
                            MaximunGrade = reader["NotaMaxima"] != DBNull.Value ? reader["NotaMaxima"].ToDecimal() : 0,
                            Accreditation = reader["Acreditadora"] != DBNull.Value ? reader["Acreditadora"].ToString() : string.Empty,
                            Commission = commission
                        };
                        competences.Add(competenceViewModel);
                    }

                    competenceViewModel.Courses.Add(new CourseViewModel
                    {
                        Name = reader["Curso"] != DBNull.Value ? reader["Curso"].ToString() : string.Empty,
                        Grade = reader["NotaAlcanzada"] != DBNull.Value ? reader["NotaAlcanzada"].ToDecimal() : 0,
                        Level = reader["NivelAlcanzado"] != DBNull.Value ? reader["NivelAlcanzado"].ToInteger() : 0
                    });
                }
            }

            #endregion

            #region Achievement Levels

            if (reader.NextResult())
            {
                ViewModel.Levels = new List<AchievementLevelViewModel>();
                while (reader.Read())
                {
                    ViewModel.Levels.Add(new AchievementLevelViewModel
                    {
                        Color = reader["Color"] != DBNull.Value ? reader["Color"].ToString() : "transparent",
                        Name = reader["Nombre"] != DBNull.Value ? reader["Nombre"].ToString() : string.Empty,
                        LevelNumber = reader["NumeroNivel"] != DBNull.Value ? reader["NumeroNivel"].ToInteger() : 0
                    });
                }
            }

            #endregion

            if (competences.Any())
            {
                ViewModel.GeneralCompetences = competences.Where(c => c.Accreditation == "WASC").ToList();
                ViewModel.SpecificCompetences = competences.Where(c => c.Accreditation == "ABET").ToList();

                int number = 1;
                ViewModel.GeneralCompetences.ForEach(gc =>
                {
                    gc.ReachedGrade = Convert.ToInt32(Math.Round(gc.Courses.Sum(c => c.Grade) / gc.Courses.Count.ToDecimal(), MidpointRounding.AwayFromZero));
                    gc.ReachedLevel = Convert.ToInt32(Math.Round(gc.Courses.Sum(c => c.Level).ToDecimal() / gc.Courses.Count.ToDecimal(), MidpointRounding.AwayFromZero));
                    gc.Color = ViewModel.Levels.Where(l => l.LevelNumber == gc.ReachedLevel).Select(l => l.Color).FirstOrDefault();
                    gc.RomanNumber = number.ToRoman();
                    number++;
                    if (gc.Courses.Count > 3)
                    {
                        gc.CellRange = gc.Courses.Count;
                    }
                });

                if (!ViewModel.GeneralCompetences.Any())
                {
                    ViewModel.ExistData = true;
                    ViewModel.Message = ReportResource.MessageMissingGeneralCompetences;
                    goto CERRAR_CONEXION;
                }

                number = 1;
                ViewModel.SpecificCompetences.ForEach(sc =>
                {
                    sc.ReachedGrade = Convert.ToInt32(Math.Round(sc.Courses.Sum(c => c.Grade) / sc.Courses.Count.ToDecimal(), MidpointRounding.AwayFromZero));
                    sc.ReachedLevel = Convert.ToInt32(Math.Round(sc.Courses.Sum(c => c.Level).ToDecimal() / sc.Courses.Count.ToDecimal(), MidpointRounding.AwayFromZero));
                    sc.Color = ViewModel.Levels.Where(l => l.LevelNumber == sc.ReachedLevel).Select(l => l.Color).FirstOrDefault();
                    sc.RomanNumber = number.ToRoman();
                    number++;
                    if (sc.Courses.Count > 3)
                    {
                        sc.CellRange = sc.Courses.Count;
                    }
                });

                if (!ViewModel.SpecificCompetences.Any())
                {
                    ViewModel.ExistData = true;
                    ViewModel.Message = ReportResource.MessageMissingSpecificCompetences;
                    goto CERRAR_CONEXION;
                }
            }

            #region Comment

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    ViewModel.AppreciativeSummary = reader["Observacion"] != DBNull.Value ? reader["Observacion"].ToString() : string.Empty;
                }
            }
            
            #endregion

            #region Firms

            if (reader.NextResult())
            {
                ViewModel.Firms = new List<ApproverViewModel>();

                while (reader.Read())
                {
                    ViewModel.Firms.Add(new ApproverViewModel
                    {
                        FullName = reader["NombreCompleto"] != DBNull.Value ? reader["NombreCompleto"].ToString() : string.Empty,
                        JobPosition = reader["Cargo"] != DBNull.Value ? reader["Cargo"].ToString() : string.Empty,
                        UrlFirm = reader["Firma"] != DBNull.Value ? ConstantHelpers.DEFAULT_SERVER_PATH + "/" + reader["Firma"].ToString() : string.Empty,
                    });
                }
            }

            #endregion

            ViewModel.SpecificCompetences = ViewModel.SpecificCompetences.OrderBy(sc => sc.Outcome).ToList();

            ViewModel.ExistData = true;

        CERRAR_CONEXION: {
            connection.Close();
        }

        }

        public void ExportByCompetencesExcel(ByCompetencesViewModel ViewModel)
        {
            LoadByCompetencesData(ViewModel);

            var currentUICulture = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(ViewModel.LanguageCulture);

            var path = HttpContext.Current.Server.MapPath(@"~/Areas/Report/Resources/Templates/template.xlsx");
            var workbook = new Workbook();
            workbook.LoadFromFile(path);

            var columns = new
            {
                Roman = 1,
                Description = 2,
                CourseAxis = 3,
                Grade = 4,
                AproximateGrade = 5,
                Level = 6,
                LevelReached = 7,
                MaximunGrade = 8,
                MaximunLevel = 9,
                Percentage = 10
            };
            var currentTableRow = 2;
            var currentDataRow = 2;

            var reportSheet = workbook.Worksheets.First(w => w.Name == "Informe") as Worksheet;
            var tableSheet = workbook.Worksheets.First(w => w.Name == "Registro") as Worksheet;
            var dataSheet = workbook.Worksheets.First(w => w.Name == "ChartData") as Worksheet;


            // Fill Report static data
            reportSheet.SetCellValue("B2", ReportResource.TitleCompetences);
            reportSheet.SetCellValue("C4", ReportResource.LabelCode);
            reportSheet.SetCellValue("D4", ViewModel.Student.Code);
            reportSheet.SetCellValue("E4", ReportResource.LabelName);
            reportSheet.SetCellValue("F4", ViewModel.Student.FullName);
            reportSheet.SetCellValue("C6", ReportResource.LabelCareer);
            reportSheet.SetCellValue("D6", ViewModel.Student.Career);
            reportSheet.SetCellValue("H6", ReportResource.LabelExit);
            reportSheet.SetCellValue("I6", ViewModel.Student.ExitCycle);
            reportSheet.SetCellValue("B9", ReportResource.SubtitleGeneralSkills);
            reportSheet.SetCellValue("C11", ReportResource.HeaderGeneralCompetences);
            reportSheet.SetCellValue("E11", ReportResource.HeaderAchievement);
            reportSheet.SetCellValue("B20", ReportResource.SubtitleSpecificSkills);
            reportSheet.SetCellValue("C22", ReportResource.HeaderGeneralCompetences);
            reportSheet.SetCellValue("E22", ReportResource.HeaderAchievement);
            reportSheet.SetCellValue("C37", ReportResource.LabelAchievementLevels);
            reportSheet.SetCellValue("B40", ReportResource.SubtitleAppreciativeSummary);
            reportSheet.SetCellValue("C42", ViewModel.AppreciativeSummary);
            var levelsOrdered = ViewModel.Levels.OrderBy(l => l.LevelNumber).ToList();
            reportSheet.SetCellValue("D37", levelsOrdered[0].Name);
            reportSheet.SetCellValue("E37", levelsOrdered[1].Name);
            reportSheet.SetCellValue("H37", levelsOrdered[2].Name);

            if (ViewModel.Firms.Count > 0)
            {
                reportSheet.SetCellValue("C51", ViewModel.Firms[0].FullName);
                reportSheet.SetCellValue("C52", ViewModel.Firms[0].JobPosition);

                if (ViewModel.Firms.Count > 1)
                {
                    reportSheet.SetCellValue("G51", ViewModel.Firms[1].FullName);
                    reportSheet.SetCellValue("G52", ViewModel.Firms[1].JobPosition);
                }
            }

            // Fill table
            tableSheet.InsertRow(currentTableRow, ViewModel.SpecificCompetences.Sum(sc => sc.CellRange) + 1);

            var extraSpecificCompetetences = ViewModel.SpecificCompetences.Count - 11;

            if (extraSpecificCompetetences > 0)
            {
                reportSheet.InsertRow(23 + 11, extraSpecificCompetetences);
            }

            int specificCompetencesReportIndex = 23;
            ViewModel.SpecificCompetences.ForEach(sc => 
            {
                WriteCompetences(tableSheet ,dataSheet, reportSheet, columns, sc, currentTableRow, currentDataRow, specificCompetencesReportIndex, ViewModel.Levels);
                currentTableRow += sc.CellRange;
                currentDataRow++;
                specificCompetencesReportIndex++;
            });

            tableSheet.SetBorderRange(2, 2, currentTableRow - 1, columns.Percentage);

            currentTableRow += 3;
            currentDataRow++;

            var _currentTableRow = currentTableRow;
            int generalCompetencesReportIndex = 12;
            ViewModel.GeneralCompetences.ForEach(gc =>
            {
                WriteCompetences(tableSheet, dataSheet, reportSheet, columns, gc, currentTableRow, currentDataRow, generalCompetencesReportIndex, ViewModel.Levels);
                currentTableRow += gc.CellRange;
                currentDataRow++;
                generalCompetencesReportIndex++;
            });

            tableSheet.SetBorderRange(_currentTableRow, 2, currentTableRow - 1, columns.Percentage);

            // Format table
            tableSheet.Columns[columns.Roman - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.Roman - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.Description - 1].Style.HorizontalAlignment = HorizontalAlignType.Left;
            tableSheet.Columns[columns.Description - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.Description - 1].Style.WrapText = true;
            tableSheet.Columns[columns.Grade - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.Grade - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.AproximateGrade - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.AproximateGrade - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.Level - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.Level - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.LevelReached - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.LevelReached - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.MaximunGrade - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.MaximunGrade - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.MaximunLevel - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.MaximunLevel - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.Percentage - 1].Style.HorizontalAlignment = HorizontalAlignType.Center;
            tableSheet.Columns[columns.Percentage - 1].Style.VerticalAlignment = VerticalAlignType.Center;
            tableSheet.Columns[columns.Percentage - 1].Style.NumberFormat = "0%";


            // Format report table font
            var textSpecificCompetences = reportSheet.Range[23, 3, 23 + ViewModel.SpecificCompetences.Count, 3];
            textSpecificCompetences.Style.Font.FontName = "Calibri";
            textSpecificCompetences.Style.Font.Size = 4;

            var indicatorSpecificCompetences = reportSheet.Range[23, 5, 23 + ViewModel.SpecificCompetences.Count, 5];
            indicatorSpecificCompetences.Style.Font.FontName = "Wingdings";
            indicatorSpecificCompetences.Style.Font.Size = 16;
            indicatorSpecificCompetences.Style.HorizontalAlignment = HorizontalAlignType.Center;

            for (int i = 23; i < 23 + ViewModel.SpecificCompetences.Count; i++)
            {
                var bottomBorder = reportSheet.Range[i, 3, i, 5].Style.Borders[BordersLineType.EdgeBottom];
                bottomBorder.LineStyle = LineStyleType.Thin;
                bottomBorder.Color = Color.Black;
            }


            var textGeneralCompetences = reportSheet.Range[12, 3, 12 + ViewModel.GeneralCompetences.Count, 3];
            textGeneralCompetences.Style.Font.FontName = "Calibri";
            textGeneralCompetences.Style.Font.Size = 10;

            var indicatorGeneralCompetences = reportSheet.Range[12, 5, 12 + ViewModel.GeneralCompetences.Count, 5];
            indicatorGeneralCompetences.Style.Font.FontName = "Wingdings";
            indicatorGeneralCompetences.Style.Font.Size = 16;
            indicatorGeneralCompetences.Style.HorizontalAlignment = HorizontalAlignType.Center;

            for (int i = 12; i < 12 + ViewModel.GeneralCompetences.Count; i++)
            {
                var bottomBorder = reportSheet.Range[i, 3, i, 5].Style.Borders[BordersLineType.EdgeBottom];
                bottomBorder.LineStyle = LineStyleType.Thin;
                bottomBorder.Color = Color.Black;
            }


            // Charts Ranges
            var chartGeneralCompetences = reportSheet.Charts[0];
            SetChartOptions(dataSheet, chartGeneralCompetences, ViewModel, 2 + ViewModel.SpecificCompetences.Count + 1, ViewModel.GeneralCompetences.Count);

            var chartSpecificCompetences = reportSheet.Charts[1];
            SetChartOptions(dataSheet, chartSpecificCompetences, ViewModel, 2, ViewModel.SpecificCompetences.Count);
            


            //workbook.SelectedTab = 1;
            reportSheet.Activate();

            Thread.CurrentThread.CurrentUICulture = currentUICulture;
            var response = HttpContext.Current.Response;

            using (var ms = new MemoryStream())
            {
                workbook.SaveToStream(ms);
                ms.Seek(0, SeekOrigin.Begin);

                var studentName = ViewModel.Student.FullName.Trim().Replace(" ", "_");

                response.Clear();
                response.Buffer = true;
                response.AddHeader("content-disposition", "attachment; filename=AchievementByCompetences_" + studentName + ".xlsx");
                response.ContentType = "application/vnd.ms-excel";
                response.BinaryWrite(ms.ToArray());
                response.End();
            }
        }

        public string GetRubricComment(int QualifiedRubricId)
        {
            return (model.DbContext.RubricaCalificada.Find(QualifiedRubricId) ?? new RubricaCalificada()).Observacion;
        }

        public Task SaveRubricComment(EditCommentViewModel ViewModel)
        {
            model.DbContext.RubricaCalificada.Find(ViewModel.QualifiedRubricId).Observacion = ViewModel.Comment;
            return model.DbContext.SaveChangesAsync();
        }

        #region Private methods
        
        private void WriteCompetences(Worksheet TableSheet, Worksheet DataSheet, Worksheet ReportSheet, dynamic Columns, CompetenceViewModel VM, int CurrentTableRow, int CurrentDataRow, int CurrentReportRow, List<AchievementLevelViewModel> Levels)
        {
            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.Roman, VM.RomanNumber, VM);

            var description = string.Empty;

            switch (VM.Accreditation)
            {
                case "WASC":
                    description = VM.Outcome;
                    break;
                case "ABET":
                    description = VM.Outcome + ". " + VM.Description;
                    break;
            }

            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.Description, description, VM);

            var courseRow = CurrentTableRow;
            VM.Courses.ForEach(c =>
            {
                TableSheet.Range[courseRow, Columns.CourseAxis].Value = c.Name;
                TableSheet.Range[courseRow, Columns.Grade].NumberValue = c.Grade > 0 ? Convert.ToDouble(c.Grade) : 0;
                TableSheet.Range[courseRow, Columns.Level].Value = c.Level > 0 ? c.Level.ToString() : string.Empty;
                
                courseRow++;
            });

            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.AproximateGrade, "=ROUND(SUM(D[row]:D[bottomRow])/COUNT(D[row]:D[bottomRow]),0)".Replace("[row]", CurrentTableRow.ToString()).Replace("[bottomRow]", (CurrentTableRow + VM.CellRange - 1).ToString()), VM, true);
            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.LevelReached, "=ROUND(SUM(F[row]:F[bottomRow])/COUNT(F[row]:F[bottomRow]),0)".Replace("[row]", CurrentTableRow.ToString()).Replace("[bottomRow]", (CurrentTableRow + VM.CellRange - 1).ToString()), VM, true);
            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.MaximunGrade, VM.MaximunGrade, VM);
            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.MaximunLevel, VM.MaximunLevel, VM);
            SetMergeCellValue(TableSheet, CurrentTableRow, Columns.Percentage, "=E[row]/H[row]".Replace("[row]", CurrentTableRow.ToString()), VM, true);

            // Chart Data
            DataSheet.Range[CurrentDataRow, 1].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.Roman].RangeGlobalAddress;
            DataSheet.Range[CurrentDataRow, 2].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.Description].RangeGlobalAddress;
            DataSheet.Range[CurrentDataRow, 3].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.MaximunLevel].RangeGlobalAddress;
            DataSheet.Range[CurrentDataRow, 4].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.LevelReached].RangeGlobalAddress;

            // Report Data
            switch (VM.Accreditation)
            {
                case "WASC":
                    ReportSheet.Range[CurrentReportRow, 3].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.Description].RangeGlobalAddress;
                    break;
                case "ABET":
                    ReportSheet.Range[CurrentReportRow, 3].Value = "=" + TableSheet.Range[CurrentTableRow, Columns.Description].RangeGlobalAddress + "&\".\"&" + TableSheet.Range[CurrentTableRow, Columns.Roman].RangeGlobalAddress;
                    break;
            }

            ReportSheet.Range[CurrentReportRow, 5].Value = "l";

            // Conditional formats
            Levels.ForEach(l =>
            {
                var conditionalFormat = ReportSheet.Range[CurrentReportRow, 5].ConditionalFormats.AddCondition();
                conditionalFormat.FormatType = ConditionalFormatType.Formula;
                conditionalFormat.BackColor = ColorTranslator.FromHtml("#D0CECE");
                conditionalFormat.FontColor = ColorTranslator.FromHtml(l.Color);
                conditionalFormat.FirstFormula = TableSheet.Range[CurrentTableRow, Columns.LevelReached].RangeGlobalAddress + "=" + l.LevelNumber.ToString();
                conditionalFormat.Operator = ComparisonOperatorType.Equal;
            });
        }
        private int BottomRow(int Row, CompetenceViewModel ViewModel)
        {
            return Row + ViewModel.CellRange - 1;
        }
        private void SetMergeCellValue(Worksheet Sheet, int Row, int Col, object Value, CompetenceViewModel ViewModel, bool IsFormula = false)
        {
            if (IsFormula)
            {
                Sheet.Range[Row, Col].Formula = Value.ToString();
                //Sheet.SetFormula(Row - 1, Col - 1, Value.ToString());
            }
            else
            {
                Sheet.Range[Row, Col].Value = Value.ToString();
                //Sheet.SetCellValue(Row - 1, Col - 1, Value.ToString());
            }
            
            Sheet.Range[Row, Col, BottomRow(Row, ViewModel), Col].Merge();
        }
        private void SetChartOptions(Worksheet DataSheet, Chart ChartReport, ByCompetencesViewModel ViewModel, int DataSheetInitRow, int ListCount)
        {
            for (int i = ChartReport.Series.Count - 1; i >= 0; i--)
            {
                ChartReport.Series.RemoveAt(i);
            }

            ChartReport.DataRange = DataSheet.Range[DataSheetInitRow, 3, DataSheetInitRow + ListCount - 1, 4];
            ChartReport.SeriesDataFromRange = false;

            var maximunLevelSeries = ChartReport.Series[0];

            maximunLevelSeries.CategoryLabels = DataSheet.Range[DataSheetInitRow, 2, DataSheetInitRow + ListCount - 1, 2];
            maximunLevelSeries.Format.Pattern = ExcelPatternType.Solid;
            maximunLevelSeries.DataFormat.LineProperties.UseDefaultFormat = false;
            maximunLevelSeries.DataFormat.LineProperties.UseDefaultLineColor = false;
            maximunLevelSeries.DataFormat.LineProperties.Color = Color.FromArgb(255, 192, 192, 192);
            maximunLevelSeries.DataFormat.LineProperties.KnownColor = ExcelColors.Gray25Percent;
            maximunLevelSeries.DataFormat.LineProperties.Pattern = ChartLinePatternType.Solid;
            maximunLevelSeries.DataFormat.LineProperties.Weight = ChartLineWeightType.Hairline;
            maximunLevelSeries.DataFormat.MarkerStyle = ChartMarkerType.None;
            maximunLevelSeries.SerieType = ExcelChartType.Radar;

            var reachedLevelSeries = ChartReport.Series[1];

            reachedLevelSeries.CategoryLabels = DataSheet.Range[DataSheetInitRow, 2, DataSheetInitRow + ListCount - 1, 2];
            reachedLevelSeries.Format.Pattern = ExcelPatternType.Solid;
            reachedLevelSeries.DataFormat.LineProperties.UseDefaultFormat = false;
            reachedLevelSeries.DataFormat.LineProperties.UseDefaultLineColor = false;
            reachedLevelSeries.DataFormat.LineProperties.Color = Color.Blue;
            reachedLevelSeries.DataFormat.LineProperties.KnownColor = ExcelColors.Blue;
            reachedLevelSeries.DataFormat.LineProperties.Pattern = ChartLinePatternType.Solid;
            reachedLevelSeries.DataFormat.LineProperties.Weight = ChartLineWeightType.Hairline;
            reachedLevelSeries.DataFormat.MarkerStyle = ChartMarkerType.None;
            reachedLevelSeries.SerieType = ExcelChartType.Radar;
        }

        #endregion
    }

    public static class ExcelExtensions
    {
        public static void SetCellValue(this Worksheet Sheet, string Cell, string Value)
        {
            Sheet.Range[Cell].Value = Value;
        }
        public static void SetBorderRange(this Worksheet Sheet, int Row, int Col, int LastRow, int LastCol)
        {
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeBottom].LineStyle = LineStyleType.Thin;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeBottom].Color = Color.Black;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeLeft].LineStyle = LineStyleType.Thin;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeLeft].Color = Color.Black;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeRight].LineStyle = LineStyleType.Thin;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeRight].Color = Color.Black;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeTop].LineStyle = LineStyleType.Thin;
            Sheet.Range[Row, Col, LastRow, LastCol].Style.Borders[BordersLineType.EdgeTop].Color = Color.Black;
        }
    }
}
