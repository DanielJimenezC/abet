﻿using System;
using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Semaphore;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class SemaphoreModel : GenericModel
    {
        public SemaphoreModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "SM";
            this.model.OutcomeTypeName = "Control";
        }
        public void LoadDropDownListsBySemaphoreReportControl(BySemaphoreReportControlViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);

            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCareer, 6);
        }

        public void LoadDropDownListsBySemaphoreReportVerification(BySemaphoreReportVerificationViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCareer, 6);
        }

        public void LoadDropDownListsByCourseRC(ByCourseViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCareer, 6);
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer,0, model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCycleTo);
        }

        public void LoadDropDownListsByCourseRV(ByCourseRVViewModel ViewModel)
        {
            

            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCareer, 6, "Verificación");
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer, 0,0,model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCycleTo, "Verificación");
        }

        public void ExistDataBySemaphoreControl(BySemaphoreReportControlViewModel ViewModel)
        {
            try
            {
                if (ViewModel.ModalityId == 1)
                {
                    ViewModel.ModuleId = model.DbContext.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId).IdModulo;
                }

                ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRC  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                    ViewModel.LanguageCulture,
                    ViewModel.IdSubModalidad,
                    ViewModel.CycleId,
                    ViewModel.ModuleId,
                    ViewModel.CareerId,
                    ViewModel.AccreditationTypeId,
                    ViewModel.CommissionId,
                    ViewModel.StudentOutcomeId
                    ).Any();

            }
            catch (Exception e)
            {
                ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRC  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                   ViewModel.LanguageCulture,
                   ViewModel.IdSubModalidad,
                   ViewModel.CycleId,
                   ViewModel.ModuleId,
                   ViewModel.CareerId,
                   ViewModel.AccreditationTypeId,
                   ViewModel.CommissionId,
                   ViewModel.StudentOutcomeId
                   ).Any();
            }
           
        }

        public void ExistDataBySemaphoreVerification(BySemaphoreReportVerificationViewModel ViewModel)
        {

            try
            {
                if (ViewModel.ModalityId == 1)
                {
                    ViewModel.ModuleId = model.DbContext.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId).IdModulo;
                }

                    ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRV  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                    ViewModel.LanguageCulture,
                    ViewModel.IdSubModalidad,
                    ViewModel.CycleId,
                    ViewModel.ModuleId,
                    ViewModel.CareerId,
                    ViewModel.AccreditationTypeId,
                    ViewModel.CommissionId,
                    ViewModel.StudentOutcomeId
                    ).Any();
            }
            catch (Exception e)
            {
                ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRV  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                    ViewModel.LanguageCulture,
                    ViewModel.IdSubModalidad,
                    ViewModel.CycleId,
                    ViewModel.ModuleId,
                    ViewModel.CareerId,
                    ViewModel.AccreditationTypeId,
                    ViewModel.CommissionId,
                    ViewModel.StudentOutcomeId
                    ).Any();
            }
         
        }
        public void ExistDataByCourseRC(ByCourseViewModel ViewModel)
        {

            try
            {

                if (ViewModel.ModalityId == 1)
                {
                    ViewModel.ModuleId = model.DbContext.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId).IdModulo;
                }

                ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRC_Curso  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                ViewModel.LanguageCulture,
                ViewModel.IdSubModalidad,
                ViewModel.CycleId,
                ViewModel.ModuleId,
                ViewModel.CareerId,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.StudentOutcomeId,
                ViewModel.CourseId
                ).Any();

            }
            catch (Exception e)
            {
              ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRC_Curso  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
              ViewModel.LanguageCulture,
              ViewModel.IdSubModalidad,
              ViewModel.CycleId,
              ViewModel.ModuleId,
              ViewModel.CareerId,
              ViewModel.AccreditationTypeId,
              ViewModel.CommissionId,
              ViewModel.StudentOutcomeId,
              ViewModel.CourseId
              ).Any();

            }
           
        }
        public void ExistDataByCourseRV(ByCourseRVViewModel ViewModel)
        {

            try
            {
                if (ViewModel.ModalityId == 1)
                {
                    ViewModel.ModuleId = model.DbContext.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == ViewModel.CycleId).IdModulo;
                }

                    ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRV_Curso  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                    ViewModel.LanguageCulture,
                    ViewModel.IdSubModalidad,
                    ViewModel.CycleId,
                    ViewModel.ModuleId,
                    ViewModel.CareerId,
                    ViewModel.AccreditationTypeId,
                    ViewModel.CommissionId,
                    ViewModel.StudentOutcomeId,
                    ViewModel.CourseId
                    ).Any();
            }
            catch (Exception e)
            {
                    ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteSemaforoRV_Curso  {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                    ViewModel.LanguageCulture,
                    ViewModel.IdSubModalidad,
                    ViewModel.CycleId,
                    ViewModel.ModuleId,
                    ViewModel.CareerId,
                    ViewModel.AccreditationTypeId,
                    ViewModel.CommissionId,
                    ViewModel.StudentOutcomeId,
                    ViewModel.CourseId
                    ).Any();
            }
           
        }

        public void ValidateTablesForReport()
        {
            model.DbContext.Database.ExecuteSqlCommand("EXEC ValidacionReporteVerificacion");
        }

        private class ProcedureResult
        {
            public int IdSubModalidadPeriodoAcademico { get; set; }
            public string Codigo { get; set; }
            public string Curso { get; set; }
            public int IdCurso { get; set; }
            public string Outcome { get; set; }
            public string DescripcionOutcome { get; set; }
            public int IdModulo { get; set; }
            public string Modulo { get; set; }
            public int IdSeccion { get; set; }
            public int IdAlumnoSeccion { get; set; }
            public int IdNivelAceptacion { get; set; }
            public int NivelAlcanzado { get; set; }
            public string Color { get; set; }
            public int NivelMaximo { get; set; }
            public string CicloAcademico { get; set; }
            public int Orden { get; set; }
        }
    }
}