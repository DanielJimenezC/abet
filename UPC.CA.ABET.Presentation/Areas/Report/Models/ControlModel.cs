﻿using System.Linq;
using System.Web.Mvc;
using UPC.CA.ABET.Presentation.Areas.Report.ViewModels.Control;

namespace UPC.CA.ABET.Presentation.Areas.Report.Models
{
    public class ControlModel : GenericModel
    {
        public ControlModel(ReportBaseModel Model, ControllerBase Controller)
            : base(Model, Controller)
        {
            this.model.ReportCode = "RC";
            this.model.OutcomeTypeName = "Control";
        }

        public void LoadDropDownListsByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationTypeBetweenCycles(FirstIdCycleFrom, FirstIdCycleTo);
            ViewModel.DropDownListCommission = model.DropDownListCommissionBetweenCycles(FirstIdAccreditationType, FirstIdCycleFrom, FirstIdCycleTo, FirstIdCareer);


            ViewModel.DropDownListCyclesToForModality = model.DropDownListCyclesToForModality(FirstIdCycleFrom);

            ViewModel.DropDownListModules = model.DropDownListModulesBetweenCyclesForModality(FirstIdCycleFrom, FirstIdCycleTo);

            

            ViewModel.DropDownListCampus = model.DropDownListCampusBetweenCycles(FirstIdCycleFrom, FirstIdCycleTo);

            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(CareerID: FirstIdCareer, CampusID: model.campus.FirstKey(), CommissionID: model.commissions.FirstKey(), CycleFromID: FirstIdCycleFrom, CycleToID: FirstIdCycleTo);
        }
        public void LoadDropDownListsByLevel(ByLevelViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer,0);

            
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus(FirstIdCycleFrom);
            //ViewModel.DropDownListLevel = model.DropDownListLevel();

         
            ViewModel.DropDownListLevel = model.DropDownListLevel(ViewModel.CycleId,ViewModel.SubModalidadPeriodoAcademicoID);
        }
        public void LoadDropDownListsByAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, 0);
            
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus(FirstIdCycleFrom);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), 0, FirstIdCareer, model.campus.FirstKey());
            ViewModel.DropDownListAcceptanceLevel = model.DropDownListAcceptanceLevel();
        }
        public void LoadDropDownListsByOutcome(ByOutcomeViewModel ViewModel)
        {
            ViewModel.DropDownListCyclesFromForModality = model.DropDownListCyclesFromForModality();
            ViewModel.DropDownListAccreditationType = model.DropDownListAccreditationType(FirstIdCycleFrom);
            ViewModel.DropDownListCommission = model.DropDownListCommissionByCareer(FirstIdAccreditationType, FirstIdCareer, FirstIdCycleFrom);
            
            ViewModel.DropDownListModules = model.DropDownListModulesForCycleForModality(FirstIdCycleFrom);
            ViewModel.DropDownListCampus = model.DropDownListCampus();
            ViewModel.DropDownListCourse = model.DropDownListCourseByOutcomeType(FirstIdCareer, CampusID: model.campus.FirstKey(), CommissionID: model.commissions.FirstKey(), CycleFromID: FirstIdCycleFrom, CycleToID: FirstIdCycleTo);
            ViewModel.DropDownListStudentOutcome = model.DropDownListStudentOutcome(model.commissions.FirstKey(), FirstIdCycleFrom, FirstIdCareer, model.campus.FirstKey());
        }

        public void ExistDataByHistoricCourse(ByHistoricCourseViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteControlCursoHistorico {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CourseId,
                ViewModel.CycleFromId,
                ViewModel.CycleToId,
                0,
                //ViewModel.ModalityId,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataByLevel(ByLevelViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteControlPorNivel {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleId,
                ViewModel.LevelId,
                0,
                //ViewModel.ModalityId,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataAcceptanceLevel(ByAcceptanceLevelViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteControlPorNivelDeAceptacion {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleId,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.AcceptanceLevelId ?? 0,
                //ViewModel.ModalityId,
                0,
                ViewModel.ModuleId ?? 0
                ).Any();
        }
        public void ExistDataByOutcome(ByOutcomeViewModel ViewModel)
        {
            ViewModel.ExistData = model.DbContext.Database.SqlQuery<ProcedureResult>("ReporteControlPorOutcome {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                ViewModel.LanguageCulture,
                ViewModel.AccreditationTypeId,
                ViewModel.CommissionId,
                //ViewModel.CareerId, Falta arreglar esto, no sale nada en la funcion ni procedure
                ViewModel.CareerId,
                ViewModel.CampusId ?? 0,
                ViewModel.CycleId ?? 0,
                ViewModel.StudentOutcomeId ?? 0,
                ViewModel.CourseId ?? 0,
                //ViewModel.ModalityId,
                0,
                ViewModel.ModuleId ?? 0
                ).Any();
        }

        private class ProcedureResult
        {
            public int IdSubModalidadPeriodoAcademico { get; set; }
            public string Codigo { get; set; }
            public string Curso { get; set; }
            public string Outcome { get; set; }
            public int IdCurso { get; set; }
            public int IdModulo { get; set; }
            public string Modulo { get; set; }
            public int IdSeccion { get; set; }
            public int IdAlumnoSeccion { get; set; }
            public string Nota { get; set; }
            public string CicloAcademico { get; set; }
            public string Color { get; set; }
            public int Orden { get; set; }
        }
    }
}