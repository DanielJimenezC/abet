﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPC.CA.ABET.Presentation.Models
{
    public class HallazgoAccionMejoraEvidenciaModel
    {
        public int hallazgoid { get; set; }
        public int accionmejoraid { get; set; }
        public int? evidenciaid { get; set; }
        public string hallazgodescripcion { get; set; }
        public string accionmejoradescripcion { get; set; }
        public string evidenciadescripcion { get; set; }
        public int? docenteid { get; set; }
        public string codigohallazgo { get; set; }
        public string codigoaccionmejora { get; set; }
        public string nombrecurso { get; set; }
    }
}