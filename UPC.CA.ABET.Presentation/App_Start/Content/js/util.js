﻿$(document).on('change', '#modalDropDown', function (e) {



    // $('#subModalDropDown option:gt(0)').remove();
    $('#termDropDown option:gt(0)').remove();
    $('#fileUploadPanel').css("display", "none");

    var ModalityId = $(this).val();
    //console.log(ModalityId);
    //var getSubModalitiesListUrl = $('#getSubModalitiesListUrl').val();
    var getTermsListUrlRegular = $('#getTermsListUrlRegular').val();
    var JSONObject = { 'ModalityId': ModalityId }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(JSONObject),
        contentType: 'application/json',
        url: getTermsListUrlRegular,
        success: function (response) {
            //console.log(response);
            $.each(response, function (key, value) {
                $("#termDropDown")
                    .append($("<option></option>")
                        .attr("value", value.key).text(value.value));
            });
        }
    });
});

var data = ["Todas", "Pregrado Regular", "Pregrado EPE"];

//cache the variables for better performance
var s = $("<select id=\"modals\" class=\"form-control\"/>");
var text = $("#text");
var editLink = $('#show_select');
var selectedOption = $('#modalityId').val();

s.hide();
for (var val in data) {
    $("<option />", {
        value: val,
        text: data[val]
    }).appendTo(s);
}

text.html(data[selectedOption])
s.insertAfter(text);
$("#modals").val(selectedOption);

s.on('change', function (e) {
    s.fadeOut("fast", function () {
        var selectedText = s.find(":selected").first().text();
        var selectedValue = s.find(":selected").first().val();
        document.location.replace('./ListPeriodoAcademico?m=' + selectedValue);
        text.html(selectedText).fadeIn("fast");
        editLink.parent().fadeIn("fast");
    });
});

editLink.on('click', function () {
    $(this).parent().fadeOut("fast", function () {
        s.fadeIn("fast");
    });
});

