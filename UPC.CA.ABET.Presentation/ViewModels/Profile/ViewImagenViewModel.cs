﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Resources.Views.Profile;

namespace UPC.CA.ABET.Presentation.ViewModels.Profile
{
    public class ViewImagenViewModel
    {

        [Display(Name = "Imagen", ResourceType = typeof(ProfileResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public HttpPostedFileBase Imagen { get; set; }

        public String RutaImagen { get; set; }
        public ViewImagenViewModel()
        {

        }
        public void CargarDatos(CargarDatosContext dataContext, Int32 IdUsuario)
        {
            var Usuario = dataContext.context.Usuario.First(x => x.IdUsuario == IdUsuario);
            this.RutaImagen = Usuario.Imagen;
        }
    }
}