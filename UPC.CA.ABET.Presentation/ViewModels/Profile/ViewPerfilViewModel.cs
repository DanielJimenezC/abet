﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Presentation.Resources.Views.Error;

namespace UPC.CA.ABET.Presentation.ViewModels.Profile
{
    public class ViewPerfilViewModel
    {
        public String Codigo { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String RutaImagen { get; set; }
        public String RutaImagenFirma { get; set; }
        public Int32 CantidadAccesos { get; set; }
        public String UltimoAcceso { get; set; }

        public ViewPerfilViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, Int32 idUsuario)
        {
            var Usuario = dataContext.context.Usuario.Include(x=> x.LogAcceso).FirstOrDefault(x => x.IdUsuario == idUsuario);
            CantidadAccesos = Usuario.LogAcceso.Count();
            var logAcceso = Usuario.LogAcceso.OrderByDescending(x => x.FechaAcceso).FirstOrDefault();

            if (logAcceso != null)
            {
                var fechaAcceso = logAcceso.FechaAcceso;
                UltimoAcceso = (DateTime.Now - fechaAcceso).ToDiferenceTimeString(System.Threading.Thread.CurrentThread.CurrentUICulture.ToString());
            } 

            this.Codigo = Usuario.Codigo;
            this.Nombres = Usuario.Nombres;
            this.Apellidos = Usuario.Apellidos;
            this.RutaImagen = Usuario.Imagen;
            this.RutaImagenFirma = Usuario.Firma;
        }
    }
}