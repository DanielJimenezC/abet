﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Resources.Views.Profile;

namespace UPC.CA.ABET.Presentation.ViewModels.Profile
{
    public class ChangePasswordViewModel
    {
        [Display(Name = "PasswordActual", ResourceType = typeof(ProfileResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String PasswordActual { get; set; }

        [Display(Name = "PasswordNuevo", ResourceType = typeof(ProfileResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String PasswordNuevo { get; set; }

        [Display(Name = "PasswordNuevo1", ResourceType = typeof(ProfileResource))]
        [Required(ErrorMessageResourceName = "CampoRequerido", ErrorMessageResourceType = typeof(ErrorResource))]
        public String PasswordNuevoComprobar { get; set; }

    }
}