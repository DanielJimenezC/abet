﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.ViewModels.Home
{
    public class GraphDashboardViewModel
    {
        public int IdNumeroPracticas { get; set; }
        public int IdOutcome { get; set; }
        public int IdComision { get; set; }
        public int IdCarrera { get; set; }
        public int IdCurso { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public IEnumerable<SelectListItem> AvailablePeriodoAcademicos { get; set; }
        public IEnumerable<SelectListItem> NumeroPracticas { get; set; }
    }
}