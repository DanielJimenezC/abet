﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Models;

namespace UPC.CA.ABET.Presentation.ViewModels.Home
{
    public class EvidenciaIFCViewModel
    {
        public string codigodocente { get; set; }
        public string docenteid { get; set; }
        public List<SelectListItem> listdocente { get; set; }
        public List<HallazgoAccionMejoraEvidenciaModel> listtodo { get; set; }

        public EvidenciaIFCViewModel()
        {
            listdocente = new List<SelectListItem>();
            listtodo = new List<HallazgoAccionMejoraEvidenciaModel>();
        }

        public void CargarData(AbetEntities context)
        {
           
            listdocente = (from a in context.UnidadAcademica
                           join b in context.SedeUnidadAcademica on a.IdUnidadAcademica equals b.IdUnidadAcademica
                           join c in context.UnidadAcademicaResponsable on b.IdSedeUnidadAcademica equals c.IdSedeUnidadAcademica
                           join d in context.Docente on c.IdDocente equals d.IdDocente
                           join e in context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                           join f in context.PeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                           where f.CicloAcademico=="201802" || f.CicloAcademico=="201801"
                           select new
                           {
                               IdDocente = c.IdDocente,
                               Codigo = d.Codigo,
                               Nombres = d.Nombres,
                               Apellidos = d.Apellidos
                           }).Distinct().ToList().Select(x => new SelectListItem { Value = x.IdDocente.ToString(), Text = x.Codigo.ToUpper() + " - " + x.Nombres.ToUpper() + ", " + x.Apellidos.ToUpper() }).ToList();



        }
        public void CargarDatosDocente(AbetEntities context)
        {
            var l = context.ObtDocentePorIFC("201702").ToList();

            listtodo = l.Select(x => new HallazgoAccionMejoraEvidenciaModel
            {
                hallazgoid = x.IdHallazgo,
                accionmejoraid = x.idAccionmejora,
                hallazgodescripcion = x.Hallazgo,
                accionmejoradescripcion = x.AccionMejora,
                docenteid = x.IdDocente,
                codigohallazgo = x.CodigoHallazgo,
                codigoaccionmejora = x.CodigoAccionMejora,
                evidenciadescripcion = x.Evidencia,
                nombrecurso = x.Curso

            }).Distinct().ToList();

        }
    }
}