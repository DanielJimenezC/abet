﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Presentation.Resources.Views.Home;

namespace UPC.CA.ABET.Presentation.ViewModels.Home
{
	public class LoginViewModel
	{
		[Required]
		[Display(Name = "Usuario", ResourceType = typeof(LoginResource))]
		public String Usuario { get; set; }
		[Required]
		[Display(Name = "Password", ResourceType = typeof(LoginResource))]
		public String Password { get; set; }

		public String Token { get; set; }
    }
}