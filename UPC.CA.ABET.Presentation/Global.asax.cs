﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ModelBinders.Binders.Add(typeof(Decimal), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(Decimal?), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new MultilanguajeDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new MultilanguajeDateTimeModelBinder());
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo(ConstantHelpers.CULTURE.ESPANOL);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(ConstantHelpers.CULTURE.ESPANOL);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                var name = String.Empty;
                var cultureInfo = new CultureInfo(ConstantHelpers.CULTURE.ESPANOL);
                if (cookie != null)
                {
                    name = cookie.Value.ToSafeString();
                    if (!String.IsNullOrEmpty(name))
                        cultureInfo = new CultureInfo(name);

                    CookieHelpers.Set(CookieKey.Culture, cultureInfo.ToString());
                    System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(ConstantHelpers.CULTURE.ESPANOL);
                }
                else
                {
                    CookieHelpers.Set(CookieKey.Culture, cultureInfo.ToString());
                    System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
                }
            }catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
            }


            //if (System.Threading.Thread.CurrentThread.CurrentUICulture.ToString().Equals(ConstantHelpers.CULTURE.ESPANOL))
            //{
            //    System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            //    System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            //}
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Session.Set(SessionKey.ExUrlReferer, Request.UrlReferrer);
            Exception ex = Server.GetLastError();
            if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 404)
                Response.Redirect("~/Error/NotFound");
            else
            {
                var statusCode = 500;
                var httpException = ex as HttpException;
                if (httpException != null)
                {
                    statusCode = httpException.GetHttpCode();
                }
                else if (!Session.IsLoggedIn())
                {
                    statusCode = 401;
                }

                Session.Set(SessionKey.ExStatusCode, statusCode);
                Session.Set(SessionKey.ExMessage, ex.Message);
                Session.Set(SessionKey.ExInnerMessage, ex.InnerException == null ? null : ex.InnerException.Message);
                Session.Set(SessionKey.ExSource, ex.Source == null ? null : ex.Source);
                Session.Set(SessionKey.ExTargetSite, ex.TargetSite == null ? null : ex.TargetSite.ToString());
                Session.Set(SessionKey.ExTrace, ex.StackTrace);
                Response.Redirect("~/Error/InternalServerError");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}
