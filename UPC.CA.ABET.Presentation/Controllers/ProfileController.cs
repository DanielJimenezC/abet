﻿#region Import
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.ViewModels;
using UPC.CA.ABET.Presentation.ViewModels.Profile;
#endregion

namespace UPC.CA.ABET.Presentation.Controllers
{
    [AuthorizeUserAttribute(AppRol.Usuario)]
    public class ProfileController : BaseController
    {
        // GET: Profile
        public ActionResult ViewPerfil()
        {
            var usuarioId = Session.GetUsuarioId().Value;
            var viewModel = new ViewPerfilViewModel();
            viewModel.CargarDatos(CargarDatosContext(), usuarioId);
            return View(viewModel);
        }

        public ActionResult ChangePassword()
        {
            var viewModel = new ChangePasswordViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                var usuarioId = Session.GetUsuarioId();
                using (var transaction = new TransactionScope())
                {
                    var security = new SecurityLogic();
                    var usuario = Context.Usuario.Find(usuarioId);

                    if (!security.AreEqualMD5(model.PasswordActual, usuario.Password))
                    {
                        PostMessage(MessageType.Error);
                        model.PasswordActual = String.Empty;
                        return View(model);
                    }
                    if (model.PasswordNuevo != model.PasswordNuevoComprobar)
                    {
                        PostMessage(MessageType.Error);
                        model.PasswordNuevo = model.PasswordNuevoComprobar = String.Empty;
                        return View(model);
                    }
                    usuario.Password = security.MD5Hash(model.PasswordNuevo);

                    Context.SaveChanges();
                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                model = new ChangePasswordViewModel();
                return View(model);
                throw;
            }

            return RedirectToAction("ViewPerfil");
        }

        public ActionResult ViewFirma()
        {
            var usuarioId = Session.GetUsuarioId().Value;
            var viewModel = new ViewFirmaViewModel();
            viewModel.CargarDatos(CargarDatosContext(), usuarioId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ViewFirma(ViewFirmaViewModel model)
        {
            var usuarioId = Session.GetUsuarioId().Value;
            try
            {
                using (var transaction = new TransactionScope())
                {
                    var usuario = Context.Usuario.Find(usuarioId);
                    var guid = Guid.NewGuid().ToString().Substring(0, 6);

                    if (model.Imagen != null && model.Imagen.ContentLength != 0)
                    {
                        var fileName = guid + "_Firma_" + Path.GetFileName(model.Imagen.FileName);
                        var path = Server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var image = System.Drawing.Image.FromStream(model.Imagen.InputStream);
                        image = image.GetThumbnailImage(320, (int)(320 * ((float)image.Height / image.Width)), null, IntPtr.Zero);
                        image.Save(Path.Combine(path, fileName));
                        usuario.Firma = fileName;
                    }
                    PostMessage(MessageType.Success);
                    Context.SaveChanges();
                    transaction.Complete();
                }

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                model.CargarDatos(CargarDatosContext(), usuarioId);
                TryUpdateModel(model);
                return View(model);
                throw;
            }

            return RedirectToAction("ViewPerfil");
        }

        public ActionResult ViewImagen()
        {
            var usuarioId = Session.GetUsuarioId().Value;
            var viewModel = new ViewImagenViewModel();
            viewModel.CargarDatos(CargarDatosContext(), usuarioId);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ViewImagen(ViewImagenViewModel model)
        {
            var usuarioId = Session.GetUsuarioId().Value;
            try
            {
                using (var transaction = new TransactionScope())
                {
                    var usuario = Context.Usuario.Find(usuarioId);
                    var guid = Guid.NewGuid().ToString().Substring(0, 6);

                    if (model.Imagen != null && model.Imagen.ContentLength != 0)
                    {
                        var fileName = guid + "_Firma_" + Path.GetFileName(model.Imagen.FileName);
                        var path = Server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        var image = System.Drawing.Image.FromStream(model.Imagen.InputStream);
                        image = image.GetThumbnailImage(320, (320 * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                        image.Save(Path.Combine(path, fileName));
                        usuario.Imagen = fileName;
                    }
                    PostMessage(MessageType.Success);
                    Context.SaveChanges();
                    transaction.Complete();
                }

            }
            catch (Exception ex)
            {
                PostMessage(MessageType.Error, ex.Message);
                model.CargarDatos(CargarDatosContext(), usuarioId);
                TryUpdateModel(model);
                return View(model);
                throw;
            }

            return RedirectToAction("ViewPerfil");
        }

    }
}