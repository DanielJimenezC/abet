﻿#region Import
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.DirectoryServices;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Logic.Areas.Professor;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Areas.Meeting.ViewModels.Meeting;
using UPC.CA.ABET.Presentation.Filters;
using UPC.CA.ABET.Presentation.Helper;
using UPC.CA.ABET.Presentation.Resources.Views.Error;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using UPC.CA.ABET.Presentation.ViewModels.Home;
#endregion

namespace UPC.CA.ABET.Presentation.Controllers
{
    public class HomeController : BaseController
    {

        public String MENSAJEALERTA = "";
        public string CurrentCulture { get; set; }

        public ActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }
        // GET: Login
        public ActionResult Login()
        {
            if (Session.IsLoggedIn())
                return RedirectToAction("Dashboard");

            var viewModel = new LoginViewModel();
            return View(viewModel);
        }
        [HttpGet]
        public ActionResult Login(String Token)
        {

            if (Token == "EvidenciaDocenteIFC")
            {
                return RedirectToAction("EvidenciaDocenteIFC");
            }

            try
            {
                if (Token != null)
                {
                    Session.Set(SessionKey.Token, Token);
                    int IdEncuestaToken = Context.EncuestaToken.Where(x => x.Token == Token).First().IdEncuestaToken;
                    Session.Set(SessionKey.EncuestaTokenId, IdEncuestaToken);

                    EncuestaToken objencuestaToken = new EncuestaToken();

                    if (IdEncuestaToken != 0)
                    {
                        objencuestaToken = Context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == IdEncuestaToken);
                    }
                    else
                    {
                        objencuestaToken = null;
                    }

                    if (objencuestaToken != null)
                    {
                        if (objencuestaToken.Tipo == "EVD")
                        {
                            if (objencuestaToken.Estado && DateTime.Now <= objencuestaToken.FechaFin.AddDays(1))
                            {
                                var roles = new AppRol[1];
                                roles[0] = AppRol.Invitado;
                                var PeriodoEPE = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado EPE" && x.PeriodoAcademico.Estado == "ACT").PeriodoAcademico;
                                var alumnomatriculado = (from a in Context.AlumnoMatriculado
                                                         join b in Context.Carrera on a.IdCarrera equals b.IdCarrera
                                                         join c in Context.Escuela on b.IdEscuela equals c.IdEscuela
                                                         join d in Context.Alumno on a.IdAlumno equals d.IdAlumno
                                                         join e in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                                                         join f in Context.PeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                                                         where a.IdSubModalidadPeriodoAcademico == objencuestaToken.IdSubModalidadPeriodoAcademico && a.IdAlumno == objencuestaToken.IdAlumno
                                                         select new
                                                         {
                                                             IdAlumnoMatriculado = a.IdAlumnoMatriculado,
                                                             IdAlumno = a.IdAlumno,
                                                             IdSede = a.IdSede,
                                                             IdCarrera = a.IdCarrera,
                                                             EstadoMatricula = a.EstadoMatricula,
                                                             IdLogCarga = a.IdLogCarga,
                                                             IdSubModalidadPeriodoAcademico = a.IdSubModalidadPeriodoAcademico,
                                                             IdEscuela = c.IdEscuela,
                                                             EscuelaNombre = c.Nombre,
                                                             CicloAcademico = f.CicloAcademico,
                                                             IdPeriodoAcademico = e.IdPeriodoAcademico
                                                         }).FirstOrDefault();


                                var dictRoles = new Dictionary<int, AppRol[]>();
                                dictRoles.Add((int)alumnomatriculado.IdEscuela, roles);

                                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                                var lang = ConstantHelpers.CULTURE.ESPANOL;
                                if (cookie != null)
                                    lang = cookie.Value;

                                var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
                                var moda = ConstantHelpers.MODALITY.PREGRADO_EPE;
                                if (modalityCookie != null)
                                    moda = modalityCookie.Value;
                                var userInvitado = Context.Usuario.FirstOrDefault(x => x.Codigo.StartsWith(ConstantHelpers.ENCUESTA.USUARIO_INVITADO));
                                var modality = CacheHelperImp.GetListModalidades(lang).FirstOrDefault(x => x.Value == moda);
                                Session.Set(SessionKey.Rol, AppRol.Invitado);
                                Session.Set(SessionKey.Roles, roles);
                                Session.Set(SessionKey.DictRoles, dictRoles);
                                Session.Set(SessionKey.UsuarioId, userInvitado.IdUsuario);
                                Session.Set(SessionKey.Codigo, userInvitado.Codigo);
                                Session.Set(SessionKey.Nombre, userInvitado.Nombres);
                                Session.Set(SessionKey.Apellido, userInvitado.Apellidos);
                                Session.Set(SessionKey.DocenteId, userInvitado.IdDocente);
                                Session.Set(SessionKey.Email, userInvitado.Nombres);
                                Session.Set(SessionKey.Imagen, userInvitado.Imagen);
                                Session.Set(SessionKey.PeriodoAcademicoId, alumnomatriculado.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoAcademico, alumnomatriculado.CicloAcademico);
                                Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, alumnomatriculado.IdSubModalidadPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPEId, PeriodoEPE.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPE, PeriodoEPE.CicloAcademico);
                                Session.Set(SessionKey.EscuelaId, alumnomatriculado.IdEscuela);
                                Session.Set(SessionKey.NombreEscuela, alumnomatriculado.EscuelaNombre);
                                Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
                                Session.Set(SessionKey.NombreModalidad, modality.Text);
                                Session.Set(SessionKey.EncuestaTokenId, objencuestaToken.IdEncuestaToken);
                                Session.Set(SessionKey.EncuestVirtualDelegadoId, objencuestaToken.IdEncuestaVirtualDelegado);

                                return RedirectToAction("AddSurveyEVDToken", "RegisterToken", new { Area = "Survey", Token = objencuestaToken.Token, IdEncuestaVirtualDelegado = objencuestaToken.IdEncuestaVirtualDelegado });
                            }
                            else
                                PostMessage(MessageType.Error, MessageResource.EstaEncuestaVIRTUALDELEGADOhaVencidoEl +" "+ ErrorResource.FechaExpiracion);
                        }


                        if (objencuestaToken.Tipo == "GRA")
                        {
                            if (objencuestaToken.Estado && DateTime.Now <= objencuestaToken.FechaFin.AddDays(1))
                            {
                                var userInvitado = Context.Usuario.FirstOrDefault(x => x.Codigo.StartsWith(ConstantHelpers.ENCUESTA.USUARIO_INVITADO));
                                var PeriodoEPE = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado EPE" && x.PeriodoAcademico.Estado == "ACT").PeriodoAcademico;
                                var alumno = Context.Alumno.Where(x => x.IdAlumno == objencuestaToken.IdAlumno).FirstOrDefault();
                                var alumnomatriculado = (from a in Context.AlumnoMatriculado
                                                         join b in Context.Carrera on a.IdCarrera equals b.IdCarrera
                                                         join c in Context.Escuela on b.IdEscuela equals c.IdEscuela
                                                         join d in Context.Alumno on a.IdAlumno equals d.IdAlumno
                                                         join e in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                                                         join f in Context.PeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                                                         where a.IdSubModalidadPeriodoAcademico == objencuestaToken.IdSubModalidadPeriodoAcademico && a.IdAlumno == objencuestaToken.IdAlumno
                                                         select new
                                                         {
                                                             IdAlumnoMatriculado = a.IdAlumnoMatriculado,
                                                             IdAlumno = a.IdAlumno,
                                                             IdSede = a.IdSede,
                                                             IdCarrera = a.IdCarrera,
                                                             EstadoMatricula = a.EstadoMatricula,
                                                             IdLogCarga = a.IdLogCarga,
                                                             IdSubModalidadPeriodoAcademico = a.IdSubModalidadPeriodoAcademico,
                                                             IdEscuela = c.IdEscuela,
                                                             EscuelaNombre = c.Nombre,
                                                             CicloAcademico = f.CicloAcademico,
                                                             IdPeriodoAcademico = e.IdPeriodoAcademico
                                                         }).FirstOrDefault();

                                var roles = new AppRol[1];
                                roles[0] = AppRol.Invitado;
                                var dictRoles = new Dictionary<int, AppRol[]>();
                                dictRoles.Add((int)alumnomatriculado.IdEscuela, roles);

                                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                                var lang = ConstantHelpers.CULTURE.ESPANOL;
                                if (cookie != null)
                                    lang = cookie.Value;

                                var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
                                var moda = ConstantHelpers.MODALITY.PREGRADO_REGULAR;
                                if (modalityCookie != null)
                                    moda = modalityCookie.Value;
                                var modality = CacheHelperImp.GetListModalidades(lang).FirstOrDefault(x => x.Value == moda);
                                Session.Set(SessionKey.Rol, AppRol.Invitado);
                                Session.Set(SessionKey.Roles, roles);
                                Session.Set(SessionKey.DictRoles, dictRoles);
                                Session.Set(SessionKey.UsuarioId, userInvitado.IdUsuario);
                                Session.Set(SessionKey.Codigo, userInvitado.Codigo);
                                Session.Set(SessionKey.Nombre, userInvitado.Nombres);
                                Session.Set(SessionKey.Apellido, userInvitado.Apellidos);
                                Session.Set(SessionKey.DocenteId, userInvitado.IdDocente);
                                Session.Set(SessionKey.Email, userInvitado.Nombres);
                                Session.Set(SessionKey.Imagen, userInvitado.Imagen);
                                Session.Set(SessionKey.PeriodoAcademicoId, alumnomatriculado.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoAcademico, alumnomatriculado.CicloAcademico);
                                Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, alumnomatriculado.IdSubModalidadPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPEId, PeriodoEPE.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPE, PeriodoEPE.CicloAcademico);
                                Session.Set(SessionKey.EscuelaId, alumnomatriculado.IdEscuela);
                                Session.Set(SessionKey.NombreEscuela, alumnomatriculado.EscuelaNombre);
                                Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
                                Session.Set(SessionKey.NombreModalidad, modality.Text);
                                PostMessage(MessageType.Success, (currentCulture.Equals("es-PE")) ? "Bienvenido " + alumno.Nombres + " " + alumno.Apellidos + " - " + "u" + alumno.Codigo : "Welcome " + alumno.Nombres + " " + alumno.Apellidos + " - " + "u" + alumno.Codigo);
                                return RedirectToAction("AddSurveyGRAToken", "RegisterToken", new { Area = "Survey", Token = objencuestaToken.Token });
                            }
                            else
                                PostMessage(MessageType.Error, MessageResource.EstaEncuestaGRADUANDOhaVencidoEl + " " + ErrorResource.FechaExpiracion);
                        }
                    }

                }

                if (Session.IsLoggedIn())
                    return RedirectToAction("Dashboard");

                var viewModel = new LoginViewModel();
                return View(viewModel);

            }
            catch (Exception e)
            {
                if (Session.IsLoggedIn())
                    return RedirectToAction("Dashboard");

                var viewModel = new LoginViewModel();
                return View(viewModel);
            }


        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {

            if (model.Usuario == "docente" && model.Password == "docente18")
            {
                return RedirectToAction("EvidenciaTemporal");
            }

            if (!ModelState.IsValid)
            {
                PostMessage(MessageType.Info);
                return View(model);
            }

            try
            {   //Buscar Periodo Academico si existe

                PeriodoAcademico objpaRegular = new PeriodoAcademico();
                PeriodoAcademico objpaEPE = new PeriodoAcademico();
                SubModalidadPeriodoAcademico objsubmodapaRegular = new SubModalidadPeriodoAcademico();
                SubModalidadPeriodoAcademico objsubmodapaEPE = new SubModalidadPeriodoAcademico();
                EncuestaToken objencuestaToken = new EncuestaToken();


                var SubmodapaRegular = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado Regular" && x.PeriodoAcademico.Estado == "ACT");
                var SubmodapaEPE = Context.SubModalidadPeriodoAcademico.Where(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado EPE" && x.PeriodoAcademico.Estado == "ACT");

                if (SubmodapaRegular.Count() >= 1)
                {
                    objpaRegular = SubmodapaRegular.First().PeriodoAcademico;
                }
                if (SubmodapaEPE.Count() >= 1)
                {
                    objpaEPE = SubmodapaEPE.First().PeriodoAcademico;
                }


                model.Usuario = model.Usuario.ToLower();
                var security = new SecurityLogic();
                var isLDAP = AppSettingsHelper.GetAppSettings("IsLDAP", "false").ToBoolean();

                bool esUsuarioIntranet = false;
                string tipoUsuarioIntranet = "NID";

                if (isLDAP)
                {
                    var sesionLDAP = IniciarSesionLDAP(model.Usuario, model.Password);
                    esUsuarioIntranet = sesionLDAP.Value;
                    tipoUsuarioIntranet = sesionLDAP.Key;
                }

                if (tipoUsuarioIntranet.Equals("Alumno"))
                {
                    model.Usuario = GetCodigo(model.Usuario);


                    int IdAlumno = Context.Alumno.FirstOrDefault(x => x.Codigo == model.Usuario).IdAlumno;

                    int? EncuestaTokenId = session.GetEncuestaTokenId();

                    if (EncuestaTokenId != 0)
                    {
                        objencuestaToken = Context.EncuestaToken.FirstOrDefault(x => x.IdEncuestaToken == EncuestaTokenId);
                    }
                    else
                    {
                        objencuestaToken = null;
                    }

                    if (objencuestaToken != null)
                    {
                        if (objencuestaToken.Tipo == "EVD")
                        {
                            if (objencuestaToken.Estado && DateTime.Now <= objencuestaToken.FechaFin.AddDays(1))
                            {
                                var roles = new AppRol[1];
                                roles[0] = AppRol.Invitado;
                                var PeriodoEPE = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado EPE" && x.PeriodoAcademico.Estado == "ACT").PeriodoAcademico;
                                var alumnomatriculado = (from a in Context.AlumnoMatriculado
                                                         join b in Context.Carrera on a.IdCarrera equals b.IdCarrera
                                                         join c in Context.Escuela on b.IdEscuela equals c.IdEscuela
                                                         join d in Context.Alumno on a.IdAlumno equals d.IdAlumno
                                                         join e in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                                                         join f in Context.PeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                                                         where a.IdSubModalidadPeriodoAcademico == objencuestaToken.IdSubModalidadPeriodoAcademico && a.IdAlumno == objencuestaToken.IdAlumno
                                                         select new
                                                         {
                                                             IdAlumnoMatriculado = a.IdAlumnoMatriculado,
                                                             IdAlumno = a.IdAlumno,
                                                             IdSede = a.IdSede,
                                                             IdCarrera = a.IdCarrera,
                                                             EstadoMatricula = a.EstadoMatricula,
                                                             IdLogCarga = a.IdLogCarga,
                                                             IdSubModalidadPeriodoAcademico = a.IdSubModalidadPeriodoAcademico,
                                                             IdEscuela = c.IdEscuela,
                                                             EscuelaNombre = c.Nombre,
                                                             CicloAcademico = f.CicloAcademico,
                                                             IdPeriodoAcademico = e.IdPeriodoAcademico
                                                         }).FirstOrDefault();


                                var dictRoles = new Dictionary<int, AppRol[]>();
                                dictRoles.Add((int)alumnomatriculado.IdEscuela, roles);

                                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                                var lang = ConstantHelpers.CULTURE.ESPANOL;
                                if (cookie != null)
                                    lang = cookie.Value;

                                var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
                                var moda = ConstantHelpers.MODALITY.PREGRADO_EPE;
                                if (modalityCookie != null)
                                    moda = modalityCookie.Value;
                                var userInvitado = Context.Usuario.FirstOrDefault(x => x.Codigo.StartsWith(ConstantHelpers.ENCUESTA.USUARIO_INVITADO));
                                var modality = CacheHelperImp.GetListModalidades(lang).FirstOrDefault(x => x.Value == moda);
                                Session.Set(SessionKey.Rol, AppRol.Invitado);
                                Session.Set(SessionKey.Roles, roles);
                                Session.Set(SessionKey.DictRoles, dictRoles);
                                Session.Set(SessionKey.UsuarioId, userInvitado.IdUsuario);
                                Session.Set(SessionKey.Codigo, userInvitado.Codigo);
                                Session.Set(SessionKey.Nombre, userInvitado.Nombres);
                                Session.Set(SessionKey.Apellido, userInvitado.Apellidos);
                                Session.Set(SessionKey.DocenteId, userInvitado.IdDocente);
                                Session.Set(SessionKey.Email, userInvitado.Nombres);
                                Session.Set(SessionKey.Imagen, userInvitado.Imagen);
                                Session.Set(SessionKey.PeriodoAcademicoId, alumnomatriculado.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoAcademico, alumnomatriculado.CicloAcademico);
                                Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, alumnomatriculado.IdSubModalidadPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPEId, PeriodoEPE.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPE, PeriodoEPE.CicloAcademico);
                                Session.Set(SessionKey.EscuelaId, alumnomatriculado.IdEscuela);
                                Session.Set(SessionKey.NombreEscuela, alumnomatriculado.EscuelaNombre);
                                Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
                                Session.Set(SessionKey.NombreModalidad, modality.Text);
                                Session.Set(SessionKey.EncuestaTokenId, objencuestaToken.IdEncuestaToken);
                                Session.Set(SessionKey.EncuestVirtualDelegadoId, objencuestaToken.IdEncuestaVirtualDelegado);

                                return RedirectToAction("AddSurveyEVDToken", "RegisterToken", new { Area = "Survey", Token = objencuestaToken.Token, IdEncuestaVirtualDelegado = objencuestaToken.IdEncuestaVirtualDelegado });
                            }
                            else
                                PostMessage(MessageType.Error, MessageResource.EstaEncuestaVIRTUALDELEGADOhaVencidoEl + " " + ErrorResource.FechaExpiracion);
                        }


                        if (objencuestaToken.Tipo == "GRA")
                        {
                            if (objencuestaToken.Estado && DateTime.Now <= objencuestaToken.FechaFin.AddDays(1))
                            {
                                var userInvitado = Context.Usuario.FirstOrDefault(x => x.Codigo.StartsWith(ConstantHelpers.ENCUESTA.USUARIO_INVITADO));
                                var PeriodoEPE = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.NombreEspanol == "Pregrado EPE" && x.PeriodoAcademico.Estado == "ACT").PeriodoAcademico;
                                var alumnomatriculado = (from a in Context.AlumnoMatriculado
                                                         join b in Context.Carrera on a.IdCarrera equals b.IdCarrera
                                                         join c in Context.Escuela on b.IdEscuela equals c.IdEscuela
                                                         join d in Context.Alumno on a.IdAlumno equals d.IdAlumno
                                                         join e in Context.SubModalidadPeriodoAcademico on a.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                                                         join f in Context.PeriodoAcademico on e.IdPeriodoAcademico equals f.IdPeriodoAcademico
                                                         where a.IdSubModalidadPeriodoAcademico == objencuestaToken.IdSubModalidadPeriodoAcademico && a.IdAlumno == objencuestaToken.IdAlumno
                                                         select new
                                                         {
                                                             IdAlumnoMatriculado = a.IdAlumnoMatriculado,
                                                             IdAlumno = a.IdAlumno,
                                                             IdSede = a.IdSede,
                                                             IdCarrera = a.IdCarrera,
                                                             EstadoMatricula = a.EstadoMatricula,
                                                             IdLogCarga = a.IdLogCarga,
                                                             IdSubModalidadPeriodoAcademico = a.IdSubModalidadPeriodoAcademico,
                                                             IdEscuela = c.IdEscuela,
                                                             EscuelaNombre = c.Nombre,
                                                             CicloAcademico = f.CicloAcademico,
                                                             IdPeriodoAcademico = e.IdPeriodoAcademico
                                                         }).FirstOrDefault();

                                var roles = new AppRol[1];
                                roles[0] = AppRol.Invitado;
                                var dictRoles = new Dictionary<int, AppRol[]>();
                                dictRoles.Add((int)alumnomatriculado.IdEscuela, roles);

                                var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                                var lang = ConstantHelpers.CULTURE.ESPANOL;
                                if (cookie != null)
                                    lang = cookie.Value;

                                var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
                                var moda = ConstantHelpers.MODALITY.PREGRADO_REGULAR;
                                if (modalityCookie != null)
                                    moda = modalityCookie.Value;
                                var modality = CacheHelperImp.GetListModalidades(lang).FirstOrDefault(x => x.Value == moda);
                                Session.Set(SessionKey.Rol, AppRol.Invitado);
                                Session.Set(SessionKey.Roles, roles);
                                Session.Set(SessionKey.DictRoles, dictRoles);
                                Session.Set(SessionKey.UsuarioId, userInvitado.IdUsuario);
                                Session.Set(SessionKey.Codigo, userInvitado.Codigo);
                                Session.Set(SessionKey.Nombre, userInvitado.Nombres);
                                Session.Set(SessionKey.Apellido, userInvitado.Apellidos);
                                Session.Set(SessionKey.DocenteId, userInvitado.IdDocente);
                                Session.Set(SessionKey.Email, userInvitado.Nombres);
                                Session.Set(SessionKey.Imagen, userInvitado.Imagen);
                                Session.Set(SessionKey.PeriodoAcademicoId, alumnomatriculado.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoAcademico, alumnomatriculado.CicloAcademico);
                                Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, alumnomatriculado.IdSubModalidadPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPEId, PeriodoEPE.IdPeriodoAcademico);
                                Session.Set(SessionKey.PeriodoEPE, PeriodoEPE.CicloAcademico);
                                Session.Set(SessionKey.EscuelaId, alumnomatriculado.IdEscuela);
                                Session.Set(SessionKey.NombreEscuela, alumnomatriculado.EscuelaNombre);
                                Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
                                Session.Set(SessionKey.NombreModalidad, modality.Text);
                                return RedirectToAction("AddSurveyGRAToken", "RegisterToken", new { Area = "Survey", Token = objencuestaToken.Token });
                            }
                            else
                                PostMessage(MessageType.Error, MessageResource.EstaEncuestaGRADUANDOhaVencidoEl +" " + ErrorResource.FechaExpiracion);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    var accesoExterno = esUsuarioIntranet && isLDAP;
                    var usuario = Context.Usuario.Include(x => x.RolUsuario).Include(x => x.RolUsuario.Select(r => r.Rol)).FirstOrDefault(x => x.Codigo == model.Usuario && x.Estado == ConstantHelpers.ESTADO.ACTIVO);

                    var usuarioLogic = new UsuarioLogic(Context);


                    if (usuario != null && (accesoExterno || security.AreEqualMD5(model.Password, usuario.Password)))
                    {
                        var LstcicloActualxModalidad = (from smpa in Context.SubModalidadPeriodoAcademico
                                                        join pa in Context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                        where pa.Estado == ConstantHelpers.ESTADO.ACTIVO
                                                        select new
                                                        {
                                                            CicloAcademico = pa.CicloAcademico,
                                                            IdSubModalidadPeriodoAcademico = smpa.IdSubModalidadPeriodoAcademico
                                                        }).ToList();

                        int IdSubModalidadPeriodoAcademico = (from smp in Context.SubModalidadPeriodoAcademico
                                                              join smpm in Context.SubModalidadPeriodoAcademicoModulo on smp.IdSubModalidadPeriodoAcademico equals smpm.IdSubModalidadPeriodoAcademico
                                                              join sm in Context.SubModalidad on smp.IdSubModalidad equals sm.IdSubModalidad
                                                              join m in Context.Modalidad on sm.IdModalidad equals m.IdModalidad
                                                              join pa in Context.PeriodoAcademico on smp.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                                              join ru in Context.RolUsuario on smp.IdSubModalidadPeriodoAcademico equals ru.IdSubModalidadPeriodoAcademico
                                                              join r in Context.Rol on ru.IdRol equals r.IdRol
                                                              join u in Context.Usuario on ru.IdUsuario equals u.IdUsuario
                                                              where u.IdUsuario == usuario.IdUsuario && pa.Estado == "ACT" && m.NombreEspanol == "Pregrado Regular"
                                                              select new
                                                              {
                                                                  IdSubModalidadPeriodoAcademico = smp.IdSubModalidadPeriodoAcademico
                                                              }).Distinct().FirstOrDefault().IdSubModalidadPeriodoAcademico;

                        var lstRolesUsuario = (from lstSMPA in LstcicloActualxModalidad
                                               join b in usuario.RolUsuario on lstSMPA.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                                               join c in Context.Rol on b.IdRol equals c.IdRol
                                               select new
                                               {
                                                   IdRolUsuario = b.IdRolUsuario,
                                                   IdRol = b.IdRol,
                                                   FechaCreacion = b.FechaCreacion,
                                                   Estado = b.Estado,
                                                   IdEscuela = b.IdEscuela,
                                                   IdSubModalidadPeriodoAcademico = b.IdSubModalidadPeriodoAcademico,
                                                   Acronimo = c.Acronimo
                                               }).ToList();



                        AppRol[] roles = null;

                        var dictRoles = new Dictionary<int, AppRol[]>();
                        int escuelaId;
                        JsonEntity escuela = null;
                        foreach (var item in CacheHelperImp.GetLstEscuelas())
                        {
                            escuelaId = item.Value.ToInteger();
                            var lstRolesUsuarioEscuela = lstRolesUsuario.Where(x => x.IdEscuela == escuelaId);
                            if (lstRolesUsuarioEscuela.Count() > 0)
                            {
                                var i = 0;
                                var rolesEscuela = new AppRol[lstRolesUsuarioEscuela.Count() + 1];
                                rolesEscuela[i] = AppRol.Usuario;

                                foreach (var rolUsuario in lstRolesUsuarioEscuela)
                                {
                                    var appRol = ConstantHelpers.ROL.GetAppRol(rolUsuario.Acronimo);
                                    if (appRol.HasValue)
                                    {
                                        i++;
                                        rolesEscuela[i] = appRol.Value;
                                    }
                                }
                                dictRoles.Add(escuelaId, rolesEscuela);
                                roles = rolesEscuela;
                                escuela = item;
                            }
                        }

                        if (dictRoles.Count() == 0)
                        {
                            Session.Clear();
                            PostMessage(MessageType.Warning, ErrorResource.UsuarioLimitado);
                            return View(model);
                        }

                        var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
                        var lang = ConstantHelpers.CULTURE.ESPANOL;
                        if (cookie != null)
                            lang = cookie.Value;

                        var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
                        var moda = ConstantHelpers.MODALITY.PREGRADO_REGULAR;
                        //if (modalityCookie != null)
                        //    moda = modalityCookie.Value;

                        var modality = CacheHelperImp.GetListModalidades(lang).FirstOrDefault(x => x.Value == moda);
                        Session.Set(SessionKey.Culture, lang);
                        Session.Set(SessionKey.Modality, moda);
                        Session.Set(SessionKey.Rol, AppRol.Usuario);
                        Session.Set(SessionKey.DictRoles, dictRoles);
                        Session.Set(SessionKey.Roles, roles);
                        Session.Set(SessionKey.UsuarioId, usuario.IdUsuario);
                        Session.Set(SessionKey.Codigo, usuario.Codigo.ToUpper());
                        Session.Set(SessionKey.Nombre, usuario.Nombres);
                        Session.Set(SessionKey.Apellido, usuario.Apellidos);
                        Session.Set(SessionKey.DocenteId, usuario.IdDocente);
                        Session.Set(SessionKey.Email, usuario.Nombres);
                        Session.Set(SessionKey.Imagen, usuario.Imagen);
                        //Corregido
                        Session.Set(SessionKey.PeriodoAcademicoId, objpaRegular.IdPeriodoAcademico);
                        Session.Set(SessionKey.PeriodoAcademico, objpaRegular.CicloAcademico);
                        Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, IdSubModalidadPeriodoAcademico);

                        if (objpaEPE.CicloAcademico != null)
                        {
                            Session.Set(SessionKey.PeriodoEPEId, objpaEPE.IdPeriodoAcademico);
                            Session.Set(SessionKey.PeriodoEPE, objpaEPE.CicloAcademico);
                        }

                        Session.Set(SessionKey.EscuelaId, escuela.Value);
                        //Session.Set(SessionKey.NombreEscuela, escuela.Text);
                        Session.Set(SessionKey.NombreEscuela, escuela.Name);
                        Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
                        Session.Set(SessionKey.NombreModalidad, modality.Text);


                        if (usuario.IdUsuarioSecundario.HasValue)
                        {
                            Session.Set(SessionKey.UsuarioSecundarioId, usuario.IdUsuarioSecundario);
                            Session.Set(SessionKey.CodigoSecundario, usuario.Usuario2.Codigo);
                        }

                        using (var transaction = new TransactionScope())
                        {
                            var logAcceso = new LogAcceso
                            {
                                FechaAcceso = DateTime.Now,
                                IdUsuario = usuario.IdUsuario,
                                IpAcceso = security.GetIPAddress()
                            };
                            Context.LogAcceso.Add(logAcceso);
                            Context.SaveChanges();
                            transaction.Complete();
                        }

                        return RedirectToAction("Dashboard");
                    }
                }
                PostMessage(MessageType.Error, ErrorResource.CredencialesNoValidas + " " + esUsuarioIntranet.ToString() + MENSAJEALERTA);
            }
            catch (Exception ex)
            {
                Session.Clear();
                PostMessage(MessageType.Error, ex.Message);
                return View(model);
                throw;
            }
            Session.Clear();
            return View(model);
        }

        [AuthorizeUser(AppRol.Usuario)]
        public ActionResult Dashboard()
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            var vm = new GraphDashboardViewModel();
            var ciclos = GedServices.GetAllMyPeriodosAcademicosServices(Context, @Session.GetModalidadId()).OrderByDescending(x => x.IdPeriodoAcademico);
            var items = new List<SelectListItem>();
            foreach (var ciclo in ciclos)
            {
                if (ciclo.IdPeriodoAcademico == periodo)
                    items.Add(new SelectListItem { Value = ciclo.IdPeriodoAcademico.ToString(), Text = ciclo.CicloAcademico, Selected = true });
                else
                    items.Add(new SelectListItem { Value = ciclo.IdPeriodoAcademico.ToString(), Text = ciclo.CicloAcademico, Selected = false });
            }
            vm.AvailablePeriodoAcademicos = items;

            vm.NumeroPracticas = new List<SelectListItem>(){
                new SelectListItem() { Value = "1", Text = "1" },
                new SelectListItem() { Value = "2", Text = "2" }
            };
            if (Session.IsLoggedIn())
                return View(vm);
            return RedirectToAction("Index", "Home", new { Area = ConstantHelpers.AREAS.RUBRIC });
        }

        //Dashboard
        public JsonResult GraduadosPorCiclo(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var response = Context.USP_SEL_DASH_CantidadGraduandos(PeriodoAcademico);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HomeReporteVerificacion(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var response = Context.USP_SEL_DASH_HOME_ReporteVerificacion(PeriodoAcademico);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HomeReporteControl(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var response = Context.USP_SEL_DASH_HOME_ReporteControl(PeriodoAcademico);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HomeProyectosAcademicos(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var subModalidadModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidad.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            var result = Context.USP_SEL_DASH_HOME_ProyectosAcademicos(subModalidadModulo.IdSubModalidadPeriodoAcademicoModulo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HomeInformeIFC(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();

            var result = Context.USP_SEL_DASH_HOME_InformeIFC(subModalidad.IdSubModalidadPeriodoAcademico);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult HomeEncuestaGraduandos(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();

            var result = Context.USP_SEL_DASH_EncuestaGraduandos("es-PE", subModalidad.IdSubModalidadPeriodoAcademico);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InformeIFC(Int32? PeriodoAcademico, Int32? Carrera, string Idioma)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            if (Carrera == null || Carrera == 0)
                Carrera = 0;
            var result = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_CantidadIFCPorSubAreas(Idioma, result.IdSubModalidadPeriodoAcademico, Carrera);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReporteVerificacion(string Idioma, Int32? PeriodoAcademico, Int32? Comision, Int32? Outcome)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            if (Comision == null || Comision == 0)
                Comision = 0;
            if (Outcome == null || Outcome == 0)
                Outcome = 0;
            var response = Context.USP_SEL_DASH_ReporteVerificacion(Idioma, PeriodoAcademico, Comision, Outcome);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReporteControl(string Idioma, Int32? PeriodoAcademico, Int32? Curso)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            if (Curso == null || Curso == 0)
                Curso = 0;
            var response = Context.USP_SEL_DASH_ReporteControl(Idioma, PeriodoAcademico, Curso);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ConAccionMejora(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;

            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_CantidadHallazgosAccionMejora(subModalidad.IdSubModalidadPeriodoAcademico);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HallazgoPorInstrumento(string Idioma, Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;

            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var subModalidadModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidad.IdSubModalidadPeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_CantidadHallazgosPorInstrumento(Idioma, subModalidadModulo.IdSubModalidadPeriodoAcademicoModulo);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RubricaVerificacion(string Idioma, Int32? PeriodoAcademico, Int32? Curso)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            if (Curso == null || Curso == 0)
                Curso = 0;

            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_PromedioPuntajeRubricaVerificacion(Idioma, subModalidad.IdSubModalidadPeriodoAcademico, Curso);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EncuestaGraduados(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;

            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_EncuestaGraduando(subModalidad.IdSubModalidadPeriodoAcademico);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PPPporSede(Int32? PeriodoAcademico, Int32? NumeroPractica)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            if (NumeroPractica == null || NumeroPractica == 0)
                NumeroPractica = 0;

            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var response = Context.USP_SEL_DASH_CantidadPPPPorSede1(subModalidad.IdSubModalidadPeriodoAcademico, NumeroPractica);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AccionMejoraAprobadas(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;

            var response = Context.USP_SEL_DASH_AccionMejoraAprobada(PeriodoAcademico);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PlanMejora(string Idioma, Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;

            var periodoAcademico = Context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var anio = periodoAcademico.FechaFinPeriodo.ToDateTime().Year;

            var response = Context.USP_SEL_DASH_PlanDeMejora_AccionMejoraCant(Idioma, 1, anio);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InformeResultados(string Idioma, Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var periodoAcademico = Context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var anio = periodoAcademico.FechaFinPeriodo.ToDateTime().Year;

            var response = Context.USP_SEL_DASH_InformeResultado_AccionMejoraCant(Idioma, 1, anio);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InformeDelegados(Int32? PeriodoAcademico)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();

            var response = Context.USP_SEL_DASH_InformeDelegados(subModalidad.IdSubModalidadPeriodoAcademico);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReporteCapstone(string Idioma, Int32? PeriodoAcademico, Int32? Sede, Int32? Carrera)
        {
            int? periodo = Session.GetPeriodoAcademicoId();
            if (PeriodoAcademico == null || PeriodoAcademico == 0)
                PeriodoAcademico = periodo;
            var subModalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademico).FirstOrDefault();
            var subModalidadModulo = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidad.IdSubModalidadPeriodoAcademico).FirstOrDefault();

            var response = Context.USP_SEL_DASH_CapstoneAprobadosDesaprobados(Idioma, subModalidadModulo.IdSubModalidadPeriodoAcademicoModulo, Sede, Carrera);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarreras(Int32? IdPeriodoAcademico)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var carreras = Context.Carrera.Where(x => x.IdEscuela == 1 && x.IdSubmodalidad == 1).ToList();
            var items = new List<SelectListItem>();

            foreach (var carrera in carreras)
            {
                items.Add(new SelectListItem { Value = carrera.IdCarrera.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? carrera.NombreEspanol : carrera.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetComisiones(Int32? IdPeriodoAcademico)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var items = new List<SelectListItem>();
            var subModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault();
            var acreditadoraPeriodoAcademico = Context.AcreditadoraPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico).FirstOrDefault();
            if(acreditadoraPeriodoAcademico == null)
                return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);

            var comisiones = Context.Comision.Where(x => x.IdAcreditadoraPeriodoAcademico == acreditadoraPeriodoAcademico.IdAcreditadoraPreiodoAcademico).ToList();

            foreach (var comision in comisiones)
            {
                items.Add(new SelectListItem { Value = comision.IdComision.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? comision.NombreEspanol : comision.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOutcomes(Int32? IdComision)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var outcomes = Context.USP_SEL_OutcomesPorComision(IdComision).ToList();
            var items = new List<SelectListItem>();

            foreach (var outcome in outcomes)
            {
                items.Add(new SelectListItem { Value = outcome.IdOutcome.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? outcome.Nombre : outcome.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetCursos(Int32? IdPeriodoAcademico)
        {
            var cookie = CookieHelpers.GetCookie(CookieKey.Culture);
            this.CurrentCulture = cookie == null ? ConstantHelpers.CULTURE.ESPANOL : cookie.Value ?? ConstantHelpers.CULTURE.ESPANOL;

            var submodalidad = Context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault();
            var cursos = Context.USP_SEL_CursoPorPeriodoAcademico(submodalidad.IdSubModalidadPeriodoAcademico);
            var items = new List<SelectListItem>();

            foreach (var curso in cursos)
            {
                items.Add(new SelectListItem { Value = curso.IdCurso.ToString(), Text = CurrentCulture == ConstantHelpers.CULTURE.ESPANOL ? curso.NombreEspanol : curso.NombreIngles });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSedes()
        {
            var sedes = Context.Sede.ToList();
            var items = new List<SelectListItem>();

            foreach(var sede in sedes)
            {
                items.Add(new SelectListItem { Value = sede.IdSede.ToString(), Text = sede.Nombre });
            }

            return Json(new SelectList(items, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public JsonResult Reuniones(string start = null, string end = null)
        {
            if (!DocenteId.HasValue)
            {
                return Json(new List<ReunionesViewModel>(), JsonRequestBehavior.AllowGet);
            }

            var reunionesLst = ReunionesViewModel.ConsultarReuniones(CargarDatosContext(), DocenteId.Value, start, end, Session.GetSubModalidadPeriodoAcademicoId(), Session.GetEscuelaId());
            return Json(reunionesLst, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ReunionDetalle(int IdReunionProfesores)
        {
            var reunionesDetalleViewModel = ReunionesDetalleViewModel.ReunionDetalle(CargarDatosContext(), IdReunionProfesores);
            return PartialView("_ReunionDetalle", reunionesDetalleViewModel);
        }

        [AuthorizeUser(AppRol.Usuario, AppRol.Invitado)]
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }

        [AuthorizeUser(AppRol.Usuario)]
        public ContentResult KeepAlive()
        {
            Session.Set(SessionKey.UsuarioId, Session.GetUsuarioId());
            return Content(string.Empty);
        }

        public ActionResult ChangeEscuela(Int32? EscuelaId)
        {
            try
            {
                var lstCurrentRol = Session.GetDictRoles().Where(x => x.Key.Equals(EscuelaId)).Select(x => x.Value).FirstOrDefault();
                var escuela = CacheHelperImp.GetLstEscuelas().FirstOrDefault(x => x.Value == EscuelaId.ToString());
                if (EscuelaId.HasValue && lstCurrentRol != null)
                {
                    Session.Set(SessionKey.EscuelaId, escuela.Value.ToInteger());
                    //Session.Set(SessionKey.NombreEscuela, escuela.Text);
                    Session.Set(SessionKey.NombreEscuela, escuela.Name);

                    Session.Set(SessionKey.Roles, lstCurrentRol);
                    PostMessage(MessageType.Info, MessageResource.AhoraSeEncuentaConLaEscuela +" " + escuela.Text);
                }
                else
                {
                    PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoTieneAccesoaLaEscuela + " " + escuela.Text);
                }

            }
            catch (Exception)
            {
                PostMessage(MessageType.Warning, MessageResource.ElUsuarioNoTieneAccesoaLaEscuelaSeleccionada);
            }
            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }
        public ActionResult ChangeUsuario()
        {

            var usuarioId = Session.GetUsuarioSecundarioId().Value;
            var periodoAcademicoId = Session.GetPeriodoAcademicoId();

            var usuario = Context.Usuario.Include(x => x.RolUsuario).Include(x => x.RolUsuario.Select(r => r.Rol)).FirstOrDefault(x => x.IdUsuario == usuarioId);

            // var lstRolesUsuario = usuario.RolUsuario.Where(x => x.IdPeriodoAcademico.Value == periodoAcademicoId).ToList();
            var SubModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == periodoAcademicoId).IdSubModalidadPeriodoAcademico;

            //var lstRolesUsuario = (from us  in context.Usuario
            //                       join b in usuario.RolUsuario on SM.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
            //                       join c in context.Rol on b.IdRol equals c.IdRol
            //                       select new
            //                       {
            //                           IdRolUsuario = b.IdRolUsuario,
            //                           IdRol = b.IdRol,
            //                           FechaCreacion = b.FechaCreacion,
            //                           Estado = b.Estado,
            //                           IdEscuela = b.IdEscuela,
            //                           IdSubModalidadPeriodoAcademico = b.IdSubModalidadPeriodoAcademico,
            //                           Acronimo = c.Acronimo
            //                       }).ToList();
            var lstRolesUsuario = usuario.RolUsuario.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademico).ToList();

            var size = lstRolesUsuario.Count() + 1;

            var roles = new AppRol[size];

            var i = 0;
            roles[i] = AppRol.Usuario;
            foreach (var rolUsuario in lstRolesUsuario)
            {
                var appRol = ConstantHelpers.ROL.GetAppRol(rolUsuario.Rol.Acronimo);
                if (appRol.HasValue)
                {
                    i++;
                    roles[i] = appRol.Value;
                }
            }

            Session.Set(SessionKey.Roles, roles);
            Session.Set(SessionKey.UsuarioId, usuario.IdUsuario);
            Session.Set(SessionKey.Codigo, usuario.Codigo);
            Session.Set(SessionKey.Nombre, usuario.Nombres);
            Session.Set(SessionKey.Apellido, usuario.Apellidos);
            Session.Set(SessionKey.DocenteId, usuario.IdDocente);
            Session.Set(SessionKey.Email, usuario.Nombres);
            Session.Set(SessionKey.Imagen, usuario.Imagen);

            if (usuario.IdUsuarioSecundario.HasValue)
            {
                Session.Set(SessionKey.UsuarioSecundarioId, usuario.IdUsuarioSecundario);
                Session.Set(SessionKey.CodigoSecundario, usuario.Usuario2.Codigo);
            }

            using (var transaction = new TransactionScope())
            {
                var security = new SecurityLogic();
                var logAcceso = new LogAcceso();
                logAcceso.FechaAcceso = DateTime.Now;
                logAcceso.IdUsuario = usuario.IdUsuario;
                logAcceso.IpAcceso = security.GetIPAddress();
                Context.LogAcceso.Add(logAcceso);
                Context.SaveChanges();
                transaction.Complete();
            }

            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }
        public ActionResult ChangeIdioma(String Idioma)
        {
            if (Idioma != ConstantHelpers.CULTURE.ESPANOL)
                Idioma = ConstantHelpers.CULTURE.INGLES;

            var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
            var moda = ConstantHelpers.MODALITY.PREGRADO_REGULAR;
            if (modalityCookie != null)
                moda = modalityCookie.Value;

            var modality = CacheHelperImp.GetListModalidades(Idioma).FirstOrDefault(x => x.Value == moda);
            Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
            Session.Set(SessionKey.NombreModalidad, modality.Text);
            CookieHelpers.Set(CookieKey.Modality, modality.Value);

            var culture = new CultureInfo(Idioma);
            Session.Set(SessionKey.Culture, culture);
            CookieHelpers.Set(CookieKey.Culture, Idioma);



            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }
        public ActionResult About()
        {
            return View();
        }
        public String GetCodigo(String codigo)
        {
            if (codigo.Count() == 7)
                return codigo.Replace("u", "200");
            else
                return codigo.Replace("u", "");
        }


        #region EVIDENCIA
        public ActionResult EvidenciaDocenteIFC()
        {
            EvidenciaIFCViewModel viewmodel = new EvidenciaIFCViewModel();
            viewmodel.CargarData(Context);

            return View(viewmodel);
        }

        public ActionResult ObtenerHallazgoAccionMejoraTodo()
        {
            EvidenciaIFCViewModel viewmodel = new EvidenciaIFCViewModel();
            viewmodel.CargarDatosDocente(Context);
            var data = viewmodel.listtodo.ToList();

            return Json(new { data = data}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AgregarEvidencia(int accionmejoraid, string descripcionevidencia)
        {
            try
            {
                var hallazgoaccionmejora = Context.HallazgoAccionMejora.FirstOrDefault(x => x.IdAccionMejora == accionmejoraid);

                hallazgoaccionmejora.DescripcionEspanol = descripcionevidencia.ToString();

                //FALTA AGREGAR LA DE INGLES

                Context.SaveChanges();

                return Json("True", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public JsonResult GetEvidencia(int accionmejoraid)
        {
            try
            {
                var hallazgoaccionmejora = Context.HallazgoAccionMejora.FirstOrDefault(x => x.IdAccionMejora == accionmejoraid);

                string descripEsp = hallazgoaccionmejora.DescripcionEspanol;

                string ret;

                if (descripEsp == null)
                {
                    ret = "NULL";
                }
                else
                    ret = descripEsp;


                //FALTA AGREGAR LA DE INGLES


                return Json(ret, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        #region ADFS

        private KeyValuePair<string, bool> IniciarSesionLDAP(String codigo, String password)
        {
            var endpoint = AppSettingsHelper.GetAppSettings("Endpoint", "LDAP://10.10.0.22");
            var domain = AppSettingsHelper.GetAppSettings("Domain", "UPC");
            var tipoUsuario = "NID";
            var esIntranet = false;

            try
            {
                var directoryEntry = new DirectoryEntry(endpoint, domain + @"\" + codigo, password);

                var directorySearcher = new DirectorySearcher(directoryEntry);
                directorySearcher.Filter = "(SAMAccountName=" + codigo + ")";
                directorySearcher.PropertiesToLoad.Add("givenName");
                directorySearcher.PropertiesToLoad.Add("sn");
                directorySearcher.PropertiesToLoad.Add("cn");

                if (directorySearcher.FindOne() != null)
                {
                    if (directorySearcher.FindOne().Path.Contains("Alumnos"))
                    {
                        codigo = GetCodigo(codigo);
                        var alumno = Context.Alumno.FirstOrDefault(x => x.Codigo == codigo);
                        if (alumno != null)
                        {
                            tipoUsuario = "Alumno";
                            esIntranet = true;
                        }
                    }
                    else
                    {
                        var usuarioLogic = new UsuarioLogic(Context);
                        if (usuarioLogic.CreateAccessRoles(codigo, CacheHelperImp.GetLstEscuelas()))
                        {
                            tipoUsuario = "Docente";
                            esIntranet = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MENSAJEALERTA = "LDAP: " + ex.Message;
            }
            return new KeyValuePair<string, bool>(tipoUsuario, esIntranet);
        }

        #endregion
        #region CAMBIOS DE MODALIDAD
        [AuthorizeUserAttribute(AppRol.Usuario)]
        public ActionResult ChangeModality(String ModalidadId)
        {
            if (ModalidadId != ConstantHelpers.MODALITY.PREGRADO_REGULAR)
                ModalidadId = ConstantHelpers.MODALITY.PREGRADO_EPE;

            var modality = CacheHelperImp.GetListModalidades().FirstOrDefault(x => x.Value == ModalidadId);
            var modalityId = int.Parse(ModalidadId);
            var subModalidadPeriodoAcademico = Context.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.SubModalidad.Modalidad.IdModalidad == modalityId && x.PeriodoAcademico.Estado == "ACT");

            Session.Set(SessionKey.Modality, modality);
            Session.Set(SessionKey.ModalidadId, modality.Value.ToInteger());
            Session.Set(SessionKey.NombreModalidad, modality.Text);
            Session.Set(SessionKey.SubModalidadPeriodoAcademicoId, subModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico);
            CookieHelpers.Set(CookieKey.Modality, ModalidadId);
            PostMessage(MessageType.Info, MessageResource.MostrandoLasFuncionalidadesParaLaModalidad + ": " + modality.Text);
            var lastUrl = Request.UrlReferrer.AbsoluteUri;
            return Redirect(lastUrl);
        }

        #endregion
    }
}