﻿#region Import
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Presentation.Resources.Views.Shared;
using UPC.CA.ABET.Presentation.Areas.Survey.Models;
using System.Text;
using System.Web.UI;
using System.IO;
using MODALITY = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;
#endregion



namespace UPC.CA.ABET.Presentation.Controllers
{
    public class BaseController : Controller
    {
        public ReportBaseModel reportBaseModel;
        public int SkipCycles;
        private CargarDatosContext cargarDatosContext;
        public AbetEntities Context { get; set; }
        public HttpSessionStateBase session { get; set; }
        public String currentCulture { get; set; }
        public String ModalityId { get; set; }
        public IEnumerable<Modulo> Modulos { get; set; }

        public SubModalidadPeriodoAcademicoModulo ActualSubModalidadPeriodoAcademicoModulo { get; set; }
        public class ComboItem
        {
            public int Key { get; set; }
            public string Value { get; set; }
        }
        public String GetAdminSubModalitiesListUrl
        {
            get
            {
                return Url.AbsoluteAction("GetSubModalitiesList", "AdminModularityPeriodApi");
            }
        }
        public String GetUploadSubModalitiesListUrl
        {
            get
            {
                return Url.AbsoluteAction("GetSubModalitiesList", "UploadModularityPeriodApi");
            }
        }
        public String GetTermsListUrl
        {
            get
            {
                return Url.AbsoluteAction("GetTermsList", "UploadModularityPeriodApi");
            }
        }
        // un cambio
        public String GetTermsListUrlRegular
        {
            get
            {
                return Url.AbsoluteAction("GetTermsListRegular", "UploadModularityPeriodApi");
            }
        }
        public int EscuelaId
        {
            get
            {
                return Session.GetEscuelaId();

            }
        }
        public int ModalidadId
        {
            get
            {
                return Session.GetModalidadId();

            }
        }

        public int? PeriodoAcademicoId
        {
            get
            {
                return Session.GetPeriodoAcademicoId();
            }
        }
        public int? SubModalidadPeriodoAcademicoId
        {
            get
            {
                return Session.GetSubModalidadPeriodoAcademicoId();
            }
        }

        public int? DocenteId
        {
            get
            {
                return Session.GetDocenteId();
            }
        }
        public BaseController()
        {

            this.reportBaseModel = new ReportBaseModel(Context);
            this.SkipCycles = 0;

            if (Context == null)
            {
                Context = new AbetEntities();
                
                Context.Database.CommandTimeout = 60;
            }

            if (session == null)
            {
                session = new HttpSessionStateWrapper(System.Web.HttpContext.Current.Session);
            }

            if (String.IsNullOrEmpty(currentCulture))
                currentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
            var modalityCookie = CookieHelpers.GetCookie(CookieKey.Modality);
            ModalityId = MODALITY.PREGRADO_REGULAR;
            if (modalityCookie != null)
                ModalityId = modalityCookie.Value;

            ActualSubModalidadPeriodoAcademicoModulo = GetActualSubModalityAcademicPeriodModule(ModalityId);
        }

        private SubModalidadPeriodoAcademicoModulo GetActualSubModalityAcademicPeriodModule(string modalityId)
        {
            var now = DateTime.Now;
            var parsedModalityId = int.Parse(modalityId);
            var subModalityAcademicPeriodModules = Context.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.IdModalidad == parsedModalityId).ToList();
            return subModalityAcademicPeriodModules.FirstOrDefault(x => x.Modulo.FechaInicio.Value < now && now < x.Modulo.FechaFin.Value);
        }

        public void InvalidarContext()
        {
            Context = new AbetEntities();
            Context.Database.CommandTimeout = 60;
        }

        public CargarDatosContext CargarDatosContext()
        {
            if (cargarDatosContext == null)
                cargarDatosContext = new CargarDatosContext { context = Context, session = session, currentCulture = currentCulture };

            return cargarDatosContext;
        }

        public void PostMessage(MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Success: TempData["FlashMessage"] = MessageResource.Success; break;
                case MessageType.Error: TempData["FlashMessage"] = MessageResource.Error; break;
                case MessageType.Info: TempData["FlashMessage"] = MessageResource.Info; break;
                case MessageType.Warning: TempData["FlashMessage"] = MessageResource.Warning; break;
            }
            TempData["MessageType"] = messageType;
        }

        public void PostMessage(MessageType messageType, String body = null)
        {
            TempData["MessageType"] = messageType;
            TempData["FlashMessage"] = body;
        }
    }
}