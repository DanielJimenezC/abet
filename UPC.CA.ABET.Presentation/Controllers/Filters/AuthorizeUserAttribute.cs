﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Presentation.Filters
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        private HttpContext _httpContext { get { return HttpContext.Current; } }
        private AppRol[] _rolesPermitidos;
        private bool _tienePermiso;

        public AuthorizeUserAttribute(params AppRol[] rol)
        {
            _rolesPermitidos = rol;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var escuelaId = httpContext.Session.GetEscuelaId();
            var lstCurrentRol = httpContext.Session.GetDictRoles().Where(x => x.Key.Equals(escuelaId)).Select(x => x.Value).FirstOrDefault();
            
            foreach (var rol in _rolesPermitidos)
            {
                foreach (var rolUsuario in lstCurrentRol)
                {
                    if (rolUsuario == rol)
                    {
                        _tienePermiso = true;
                        return _tienePermiso;
                    }
                }
            }

            _tienePermiso = false;
            return _tienePermiso;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            if (!filterContext.HttpContext.Session.IsLoggedIn())
            {

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "Controller", "Home" },
                    { "Action", "Login" },
                    { "Area", String.Empty }
                });
            }
            else
            {
                base.OnAuthorization(filterContext);
                if (!_tienePermiso)
                    filterContext.Result = new ViewResult { ViewName = "_PermisoDenegado" };
            }
        }

    }

    //public class SessionExpireAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        HttpContext httpContext = HttpContext.Current;

    //        // check if session is supported
    //        var lstCurrentRol = httpContext.Session.GetRoles();
    //        if (lstCurrentRol == null)
    //        {
    //            filterContext.HttpContext.Response.StatusCode = 401;
    //            //// check if a new session id was generated
    //            //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
    //            //{
    //            //    { "Controller", "Home" },
    //            //    { "Action", "Login" },
    //            //    { "Area", String.Empty }
    //            //});
    //            return;
    //        }

    //        base.OnActionExecuting(filterContext);
    //    }
    //}
}