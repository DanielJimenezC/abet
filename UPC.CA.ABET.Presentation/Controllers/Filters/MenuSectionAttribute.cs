﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPC.CA.ABET.Presentation.Filters
{
    public class MenuSectionAttribute : ActionFilterAttribute
    {
        private String _icon = String.Empty;
        private String _section = String.Empty;
        private String _subSection = String.Empty;

        public MenuSectionAttribute(String section)
        {
            _section = section;
        }

        public MenuSectionAttribute(String section, String subSection)
        {
            _section = section;
            _subSection = subSection;
        }

        public MenuSectionAttribute(String section, String subSection, String icon)
        {
            _icon = icon;
            _section = section;
            _subSection = subSection;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.Icon = _icon;
            filterContext.Controller.ViewBag.Section = _section;
            filterContext.Controller.ViewBag.Subsection = _subSection;
            base.OnActionExecuting(filterContext);
        }

    }
}