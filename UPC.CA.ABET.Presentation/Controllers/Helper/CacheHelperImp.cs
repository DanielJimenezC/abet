﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;

namespace UPC.CA.ABET.Presentation.Helper
{
    public static class CacheHelperImp
    {
        private static AbetEntities _context { get { return new AbetEntities(); } }

        public static List<JsonEntity> GetLstEscuelas()
        {
            List<JsonEntity> escuelas = null;
            if (CacheHelper.Exists(CacheKey.LstEscuela))
            {
                escuelas = CacheHelper.Get<List<JsonEntity>>(CacheKey.LstEscuela);
            }
            else
            {
                if(_context.Escuela.Where(x =>x.Codigo == "EIC" || x.Codigo == "EII").Count() != 0)
                    escuelas = _context.Escuela.Where(x => !x.Codigo.Contains("EPE")).Select(x => new JsonEntity() { Text = x.Nombre, Value = x.IdEscuela.ToString(), Name = x.Codigo }).ToList();
                else
                    escuelas = _context.Escuela.Select(x => new JsonEntity() { Text = x.Nombre, Value = x.IdEscuela.ToString(), Name = x.Codigo }).ToList();
                var success = CacheHelper.Set(escuelas, CacheKey.LstEscuela);
            }
            return escuelas;
        }
        public static void DeleteLstEscuelas()
        {
            CacheHelper.Clear(CacheKey.LstEscuela);
        }

        public static List<JsonEntity> GetListModalidades()
        {
            List<JsonEntity> modalidades = null;
            var cultureCookie = CookieHelpers.GetCookie(CookieKey.Culture);
            var CurrentCulture = cultureCookie == null ? Culture.ESPANOL : cultureCookie.Value ?? Culture.ESPANOL;
            if (CacheHelper.Exists(CacheKey.LstModalidad))
            {
                modalidades = CacheHelper.Get<List<JsonEntity>>(CacheKey.LstModalidad);
            }
            else
            {
                if (CurrentCulture == Culture.ESPANOL)
                {
					//modalidades = _context.Modalidad.OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreEspanol, Value = x.IdModalidad.ToString() }).ToList();

					//var x = (from a in _context.PeriodoAcademico
					//		 join b in _context.SubModalidadPeriodoAcademico on a.IdPeriodoAcademico equals b.IdPeriodoAcademico
					//		 join c in _context.SubModalidad on b.IdSubModalidad equals c.IdSubModalidad
					//		 join d in _context.Modalidad on c.IdModalidad equals d.IdModalidad
					//		 where a.Estado == "ACT"
					//		 select d).ToList();

					modalidades = _context.Usp_ListModalities().OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreEspanol, Value = x.IdModalidad.ToString() }).ToList();
					
                }
                else
                {
					//modalidades = _context.Modalidad.OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreIngles, Value = x.IdModalidad.ToString() }).ToList();
					modalidades = _context.Usp_ListModalities().OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreIngles, Value = x.IdModalidad.ToString() }).ToList();
				}
                
            }
            return modalidades;
        }
        public static List<JsonEntity> GetListModalidades(string currentCulture)
        {
            List<JsonEntity> modalidades = null;
            if (CacheHelper.Exists(CacheKey.LstModalidad))
            {
                modalidades = CacheHelper.Get<List<JsonEntity>>(CacheKey.LstModalidad);
            }
            else
            {
                if (currentCulture == Culture.ESPANOL)
                {
                    modalidades = _context.Modalidad.OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreEspanol, Value = x.IdModalidad.ToString() }).ToList();
                }
                else
                {
                    modalidades = _context.Modalidad.OrderByDescending(x => x.IdModalidad).Select(x => new JsonEntity() { Text = x.NombreIngles, Value = x.IdModalidad.ToString() }).ToList();
                }

            }
            return modalidades;
        }
        public static void DeleteLstModalidades()
        {
            CacheHelper.Clear(CacheKey.LstModalidad);
        }

        public static List<JsonFilterEntity> GetLstCarreras()
        {
            List<JsonFilterEntity> carreras = null;
            if (CacheHelper.Exists(CacheKey.LstCarrera))
            {
                carreras = CacheHelper.Get<List<JsonFilterEntity>>(CacheKey.LstCarrera);
            }
            else
            {
                carreras = _context.Carrera.Select(x => new JsonFilterEntity() { Text = x.NombreEspanol, Value = x.IdCarrera.ToString(), Filter = x.IdEscuela.ToString() }).ToList();
                var success = CacheHelper.Set(carreras, CacheKey.LstCarrera);
            }
            return carreras;
        }
        public static void DeleteLstCarreras()
        {
            CacheHelper.Clear(CacheKey.LstCarrera);
        }
    }
}
