﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using UPC.CA.ABET.Presentation;


namespace UPC.CA.ABET.Presentation.Helper
{
    public class YandexTranslatorAPIHelper
    {
        //INFO CAMBIAR APIKEY: https://tech.yandex.com/translate/

        public YandexTranslatorAPIHelper()
        {
            key = "trnsl.1.1.20180609T041203Z.57b0f738d560afd7.22725a9f4f05b9c776358e5c1295c76c34b40270";
        }

        public string key { get; set; }

        public string url { get; set; }

        public string textoEntrada { get; set; }

        public string textoSalida { get; set; }

        public string Translate(string text, string fromTo = "es-en")
        {
            if (text.Trim() == "") return "";

            key = "trnsl.1.1.20180609T041203Z.57b0f738d560afd7.22725a9f4f05b9c776358e5c1295c76c34b40270";
            string textEncode;
            string requestString;
   

            if (text.Length > 10000)
            {
                int count = text.Length / 2;
                char[] delimiterChars = { '"' };
                string[] words = text.Split(delimiterChars, count);
                string resp = "";
                
                for (int i = 0; i < words.Length; i++)
                {
                    textEncode = HttpUtility.UrlEncode(words[i]);
                    requestString = String.Format("https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}&format={3}", key, textEncode, fromTo, "plain");
                    var request = WebRequest.Create(requestString);
                    request.ContentType = "application/json; charset=utf-8";
                    var response = request.GetResponse();
                   
                    string content = string.Empty;
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            content = sr.ReadToEnd();
                        }
                    }

                    string output = JsonConvert.SerializeObject(content);

                    dynamic results = JsonConvert.DeserializeObject<dynamic>(content);

                    var textotraducido = results.text;
                    var result = textotraducido.ToObject<List<string>>()[0];
                    resp = String.Concat(resp , result);

                }
                return resp;
                //string content = string.Empty;
                //using (var stream = resp.GetResponseStream())
                //{
                //    using (var sr = new StreamReader(stream))
                //    {
                //        content = sr.ReadToEnd();
                //    }
                //}

                //string output = JsonConvert.SerializeObject(content);

                //dynamic results = JsonConvert.DeserializeObject<dynamic>(content);

                //var textotraducido = results.text;

                //return textotraducido.ToObject<List<string>>()[0];
            }
            else
            {
                textEncode = HttpUtility.UrlEncode(text);

                 requestString =
                    String.Format("https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}&format={3}",
                    key, textEncode, fromTo, "plain");

                var request = WebRequest.Create(requestString);
                request.ContentType = "application/json; charset=utf-8";

                request.Method = "POST";
                if ((requestString.Length > 10240) && (request.Method.StartsWith("POST")))
                    throw new ArgumentException("Texto demasiado largo (>10Kb)");

                var response = request.GetResponse();
                string content = string.Empty;
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                    }
                }

                string output = JsonConvert.SerializeObject(content);

                dynamic results = JsonConvert.DeserializeObject<dynamic>(content);

                var textotraducido = results.text;

                return textotraducido.ToObject<List<string>>()[0];

            }

            return "";
            
        }

        
       
    }
}