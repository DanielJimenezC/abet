﻿$(document).on('click', '[data-type=modal-link]', function (e) {
    e.preventDefault();
    var $link = $(this);

    var sourceUrl = $link.attr('data-source-url');
    var onClose = $link.attr('data-on-close');
    var modalSize = $link.attr('data-modal-size');

    var $modalLoading = $('#default-modal-loading');
    var $modal = $('<div class="modal fade modal-fade-in-scale-up" id="modalId" role="dialog"><div class="modal-dialog ' + modalSize + '"><div class="modal-content"></div></div></div>');
    var $modalContent = $modal.find('.modal-content');

    $modalContent.html($modalLoading.html());

    var $modalContainer = $('#default-modal-container');
    $modalContainer.append($modal);

    var bootModal = $modal.modal('show');

    $modalContent.load(sourceUrl, function (response, status, xhr) {
        if (status == 'error')
            $modalContent.html($('#default-modal-loading-error').html());
    });
});

$('input[type=checkbox]').on('change', function () {
    $(this).val($(this).prop('checked'));
});

$('textarea.wysihtml5').wysihtml5({ toolbar: { "fa": true, "font-styles": true, "emphasis": true, "lists": true, "html": false, "link": false, "image": false, "color": false, "blockquote": true, "size": 'sm' } });

var KeepAlive = function () {
    http_request = new XMLHttpRequest();
    if (urlRoute === '/')
        http_request.open('GET', "/Home/KeepAlive");
    else
        http_request.open('GET', urlRoute + "/Home/KeepAlive");
    http_request.send(null);
};

$('[data-plugin=select2]').select2();

setInterval(KeepAlive, 200000);