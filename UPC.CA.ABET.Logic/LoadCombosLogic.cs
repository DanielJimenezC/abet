﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;

namespace UPC.CA.ABET.Logic
{
    public class LoadCombosLogic
    {
        private readonly AbetEntities _context;

        public LoadCombosLogic(AbetEntities Context)
        {
            _context = Context;
            if (_context == null)
            {
                _context = new AbetEntities();
            }
        }
        public IEnumerable<ComboItem> ListCyclesForModality(string ModalityId = Modality.PREGRADO_REGULAR, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclos {0}, {1}, {2}", Skip, ModalityId, 0).AsEnumerable();
        }
    }

    public static class LoadCombosLogicExtensions
    {
        public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new SelectList(dictionary, "Key", "Value");
        }
        public static SelectList ToSelectList(this IEnumerable<ComboItem> data)
        {
            return new SelectList(data, "Key", "Value");
        }
    }
}
