﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using UPC.CA.ABET.Helpers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UPC.CA.ABET.Logic.Areas.Survey
{
	public class EmailGRALogic
	{
		public bool SendMail(String asunto, String msjCuerpo, String email)
		{
			try
			{
                var abetEmailServerHost = AppSettingsHelper.GetAppSettings("AbetEmailServerHost", "10.10.3.119");
                var abetEmailServerPort = AppSettingsHelper.GetAppSettings("AbetEmailServerPort", "25");
                var abetEmailServerUserCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserCredential", "usreiscabet@upc.edu.pe");
                var abetEmailServerUserPassword = AppSettingsHelper.GetAppSettings("AbetEmailServerUserPassword", "escuelasistemas");
                var abetEmailServerSSLEnabled= AppSettingsHelper.GetAppSettings("AbetEmailServerSSLEnabled", "false");
                var abetEmailServerUserDefaultCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserDefaultCredential", "false");

                SmtpClient abetEmailServer = new SmtpClient
                {
                    Host = abetEmailServerHost,
                    Port = int.Parse(abetEmailServerPort),
                    EnableSsl = abetEmailServerSSLEnabled.ToBoolean(),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = abetEmailServerUserDefaultCredential.ToBoolean(),
                    Credentials = new System.Net.NetworkCredential(abetEmailServerUserCredential, abetEmailServerUserPassword)
                };

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(abetEmailServerUserCredential)
                };
                mail.To.Add(email);
                mail.Subject = asunto;
				mail.Body = msjCuerpo;
				mail.IsBodyHtml = true;
                abetEmailServer.Send(mail);
			}
			catch (Exception ex)
			{
				Console.Write(ex.Message);
				return false;
			}
			return true;
		}
        public bool SendMailArc(String asunto, String msjCuerpo, String email)
        {
            try
            {
                var abetEmailServerHost = AppSettingsHelper.GetAppSettings("AbetEmailServerHost", "10.10.3.119");
                var abetEmailServerPort = AppSettingsHelper.GetAppSettings("AbetEmailServerPort", "25");
                var abetEmailServerUserCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserCredential", "usreiscabet@upc.edu.pe");
                var abetEmailServerUserPassword = AppSettingsHelper.GetAppSettings("AbetEmailServerUserPassword", "escuelasistemas");
                var abetEmailServerSSLEnabled = AppSettingsHelper.GetAppSettings("AbetEmailServerSSLEnabled", "false");
                var abetEmailServerUserDefaultCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserDefaultCredential", "false");
                SmtpClient abetEmailServer = new SmtpClient
                {
                    Host = abetEmailServerHost,
                    Port = int.Parse(abetEmailServerPort),
                    EnableSsl = abetEmailServerSSLEnabled.ToBoolean(),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = abetEmailServerUserDefaultCredential.ToBoolean(),
                    Credentials = new System.Net.NetworkCredential(abetEmailServerUserCredential, abetEmailServerUserPassword)
                };

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(abetEmailServerUserCredential)
                };
                mail.To.Add(email);
                mail.Subject = asunto;
                mail.Body = msjCuerpo;
                mail.IsBodyHtml = true;
                abetEmailServer.Send(mail);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
            return true;
        }


        public String GetHtmlEmailEVD(String mensajeCuerpo, String urlSurvey, String urlHeader, String urlFooter, String codigo)
		{

			return
			"<!DOCTYPE html>" +
			"<html lang='es'>" +
				"<head>" +
					"<meta charset='utf-8'>" +
					"<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
					"<meta name='viewport' content='width=device-width, initial-scale=1'>" +
					"<title>'UPC-SA -- Encuesta VirtualDelegado -- </title>" +

				"</head>" +
				"<body style = 'font-family: Calibri; font-size:14.5px;' >" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='1' cellspacing='0' cellpadding='0' style='width:472.5pt;border:1pt solid white;'>" +
					"<tbody><tr>" +
					"<td style='padding:0;border:1pt solid #CCCCCC;'>" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='0' cellspacing='0' cellpadding='0' style='width:472.5pt;background-color:white;'>" +
					"<tbody><tr height='31' style='height:18.75pt;'>" +
					"<td style='height:18.75pt;padding:0;'></td>" +
					"<td colspan='4' style='height:18.75pt;padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Arial,sans-serif' size='2' color='#3A3A3A' style='font-family: Arial, sans-serif, serif, EmojiFont;'><span style='font-size:8.5pt;'><b>Por favor, no responder este mensaje.</b></span></font></span></font></div>" +
					"</td>" +
					"</tr>" +
					"<tr height='31' style='height:18.75pt;'>" +
					"<td width='50' style='width:30pt;height:18.75pt;padding:0;'></td>" +
					"<td width='412' style='width:247.5pt;height:18.75pt;padding:0;'></td>" +
					"<td width='25' style='width:15pt;height:18.75pt;padding:0;'></td>" +

					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<img src='" + urlHeader + "'>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<hr width='88%' style='background:gray'/>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'></td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<div style='margin-top:0;margin-bottom:0;'>" +
					"<font face='Arial,sans-serif' size='2' color='#434343' style='font-family: Arial, sans-serif, serif, EmojiFont;'>" +
					mensajeCuerpo +
					"<a target='_blank' href='" + urlSurvey + "' style='font-weight:normal;font-size:15px;padding:7px 10px 7px 10px;text-decoration:none;text-align:center;white-space:nowrap;vertical-align:middle;cursor:pointer;border-radius:0px;border:none;text-shadow:0 0px 0px rgba(255, 255, 255, 0.75);color:#ffffff;background-color:#363636;'>Ingresa Aquí</a>" +
					"</font>" +
					"</div>" +
					"</td>" +
					"<td width='50' style='width:30pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='37' style='height:22.5pt;'>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"<td colspan='3' style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Lucida Sans,sans-serif' size='2' color='white' style='font-family: &quot;Lucida Sans&quot;, sans-serif, serif, EmojiFont;'><span style='font-size:10pt;'>&nbsp;</span></font></span></font></div>" +
					"</span></td>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<img src='" + urlFooter + "'>" +
					"</td>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +

				"</body >" +
		  "</html>";
		}

		public String GetHtmlEmail(String mensajeCuerpo, String urlSurvey, String urlHeader, String urlFooter)
		{

			return
			"<!DOCTYPE html>" +
			"<html lang='es'>" +
				"<head>" +
					"<meta charset='utf-8'>" +
					"<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
					"<meta name='viewport' content='width=device-width, initial-scale=1'>" +
					"<title>'UPC-SA -- Encuesta Graduando -- </title>" +

				"</head>" +
				"<body style = 'font-family: Calibri; font-size:14.5px;' >" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='1' cellspacing='0' cellpadding='0' style='width:472.5pt;border:1pt solid white;'>" +
					"<tbody><tr>" +
					"<td style='padding:0;border:1pt solid #CCCCCC;'>" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='0' cellspacing='0' cellpadding='0' style='width:472.5pt;background-color:white;'>" +
					"<tbody><tr height='31' style='height:18.75pt;'>" +
					"<td style='height:18.75pt;padding:0;'></td>" +
					"<td colspan='4' style='height:18.75pt;padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Arial,sans-serif' size='2' color='#3A3A3A' style='font-family: Arial, sans-serif, serif, EmojiFont;'><span style='font-size:8.5pt;'><b>Por favor, no responder este mensaje.</b></span></font></span></font></div>" +
					"</td>" +
					"</tr>" +
					"<tr height='31' style='height:18.75pt;'>" +
					"<td width='50' style='width:30pt;height:18.75pt;padding:0;'></td>" +
					"<td width='412' style='width:247.5pt;height:18.75pt;padding:0;'></td>" +
					"<td width='25' style='width:15pt;height:18.75pt;padding:0;'></td>" +

					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<img src='" + urlHeader + "'>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<hr width='88%' style='background:gray'/>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'></td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<div style='margin-top:0;margin-bottom:0;'>" +
					"<font face='Arial,sans-serif' size='2' color='#434343' style='font-family: Arial, sans-serif, serif, EmojiFont;'>" +
					mensajeCuerpo +
					"<a target='_blank' href='" + urlSurvey + "' style='font-weight:normal;font-size:15px;padding:7px 10px 7px 10px;text-decoration:none;text-align:center;white-space:nowrap;vertical-align:middle;cursor:pointer;border-radius:0px;border:none;text-shadow:0 0px 0px rgba(255, 255, 255, 0.75);color:#ffffff;background-color:#363636;'>Ingresa Aquí</a>" +
					"</font>" +
					"</div>" +
					"</td>" +
					"<td width='50' style='width:30pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='37' style='height:22.5pt;'>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"<td colspan='3' style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Lucida Sans,sans-serif' size='2' color='white' style='font-family: &quot;Lucida Sans&quot;, sans-serif, serif, EmojiFont;'><span style='font-size:10pt;'>&nbsp;</span></font></span></font></div>" +
					"</span></td>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<img src='" + urlFooter + "'>" +
					"</td>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +

				"</body >" +
		  "</html>";
		}


		public String GetHtmlEmailRubricEvaluacion(String mensajeCuerpo, String urlSurvey, String urlHeader, String urlFooter)
		{

			return
		   "<!DOCTYPE html>" +
			"<html lang='es'>" +
				"<head>" +
					"<meta charset='utf-8'>" +
					"<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
					"<meta name='viewport' content='width=device-width, initial-scale=1'>" +
					"<title>'CALIFICACIÓN EVALUADOR PROYECTO ACADÉMICOS UPC</title>" +

				"</head>" +
				"<body style = 'font-family: Calibri; font-size:14.5px;' >" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='1' cellspacing='0' cellpadding='0' style='width:472.5pt;border:1pt solid white;'>" +
					"<tbody><tr>" +
					"<td style='padding:0;border:1pt solid #CCCCCC;'>" +
					"<div align='center' style='text-align:center;'>" +
					"<table width='787' border='0' cellspacing='0' cellpadding='0' style='width:472.5pt;background-color:white;'>" +
					"<tbody><tr height='31' style='height:18.75pt;'>" +
					"<td style='height:18.75pt;padding:0;'></td>" +
					"<td colspan='4' style='height:18.75pt;padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Arial,sans-serif' size='2' color='#3A3A3A' style='font-family: Arial, sans-serif, serif, EmojiFont;'><span style='font-size:8.5pt;'><b>Por favor, no responder este mensaje.</b></span></font></span></font></div>" +
					"</td>" +
					"</tr>" +
					"<tr height='31' style='height:18.75pt;'>" +
					"<td width='50' style='width:30pt;height:18.75pt;padding:0;'></td>" +
					"<td width='412' style='width:247.5pt;height:18.75pt;padding:0;'></td>" +
					"<td width='25' style='width:15pt;height:18.75pt;padding:0;'></td>" +

					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<img src='" + urlHeader + "'>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td colspan='5' style='padding:0;'>" +
					"<hr width='88%' style='background:gray'/>" +
					"</td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'></td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<div style='margin-top:0;margin-bottom:0;'>" +
					"<font face='Arial,sans-serif' size='2' color='#434343' style='font-family: Arial, sans-serif, serif, EmojiFont;'>" +
					"<p style='text - align: justify; '>" +
					mensajeCuerpo +
					"</p>" +
					"<a target='_blank' href='" + urlSurvey + "' style='font-weight:normal;font-size:15px;padding:7px 10px 7px 10px;text-decoration:none;text-align:center;white-space:nowrap;vertical-align:middle;cursor:pointer;border-radius:0px;border:none;text-shadow:0 0px 0px rgba(255, 255, 255, 0.75);color:#ffffff;background-color:#363636;'>Ingresa Aquí</a>" +
					"</font>" +
					"</div>" +
					"</td>" +
					"<td width='50' style='width:30pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='25' style='height:15pt;'>" +
					"<td colspan='5' style='height:15pt;padding:0;'></td>" +
					"</tr>" +
					"<tr height='37' style='height:22.5pt;'>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"<td colspan='3' style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Lucida Sans,sans-serif' size='2' color='white' style='font-family: &quot;Lucida Sans&quot;, sans-serif, serif, EmojiFont;'><span style='font-size:10pt;'>&nbsp;</span></font></span></font></div>" +
					"</span></td>" +
					"<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
					"</tr>" +
					"<tr>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"<td colspan='3' style='padding:0;'>" +
					"<img src='" + urlFooter + "'>" +
					"</td>" +
					"<td style='padding:0;'>" +
					"<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +
					"</td>" +
					"</tr>" +
					"</tbody></table>" +
					"</div>" +

				"</body >" +
		  "</html>";
		}
	}
}
