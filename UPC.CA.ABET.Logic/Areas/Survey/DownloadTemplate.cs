﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Survey
{
    public class DownloadTemplate
    {
        public List<TemplateEncuesta> FilterTemplate(AbetEntities context,Int32? acreditadoraId, Int32? carreraId, Int32? cicloId, String Tipo)
        {
            Int32? carrera,ciclo,acreditadora;

            carrera = carreraId;
            ciclo = cicloId;
            acreditadora = acreditadoraId;

            var archivos = context.TemplateEncuesta.Where(x => x.Estado != ConstantHelpers.ESTADO.INACTIVO && x.TipoEncuesta.Acronimo == Tipo).ToList();
            if (carrera.HasValue)
            {
                archivos = archivos.Where(x => x.IdCarrera == carrera).ToList();
            }
            if (acreditadora.HasValue)
            {
                archivos = archivos.Where(x => x.IdAcreditadora == acreditadora).ToList();
            }
            if (ciclo.HasValue)
            {
                archivos = archivos.Where(x => x.IdSubModalidadPeriodoAcademico == ciclo).ToList();
            }

            return archivos;
        }
    }
}
