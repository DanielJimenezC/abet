﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Survey;
using UPC.CA.ABET.Models;
using System.IO;
using System.Text;
using System.Data;
using Excel;
using System.Transactions;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Survey
{
    public class ExcelLogic
    {
        public static DataTable ExtractExcelToDataTable(HttpPostedFileBase file)
        {

            file.InputStream.Position = 0;

            String fileExtension = System.IO.Path.GetExtension(file.FileName);

            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                IExcelDataReader excelReader = null;

                if (fileExtension == ".xls")
                    excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                else if (fileExtension == ".xlsx")
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);

                DataSet result = excelReader.AsDataSet();

                file.InputStream.Position = 0;
                return result.Tables[0];
            }
            return new DataTable();
        }

   
    }
}
