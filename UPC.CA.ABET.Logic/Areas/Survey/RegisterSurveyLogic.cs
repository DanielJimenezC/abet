﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;
using System.Web.Mvc;

namespace UPC.CA.ABET.Logic.Areas.Survey
{
    public class GrupoPPP
    {
        public int IdCarrera;
        public int IdComision;
        public int IdOutcome;
        public int IdPeriodoAcademico;
    }

    public class RegisterSurveyLogic
    {
        public Decimal ConvertirOutcome_DesdeEAC_ParaCAC(String OutcomeDestino, Dictionary<String, Int32> DicPerformance)
        {
            Decimal valor = 0;

            switch(OutcomeDestino)
            {
                case "A": valor = DicPerformance["AEAC"];
                    break;
                case "B":
                    if (DicPerformance["BEAC"] < 0)
                    {
                        valor = (Convert.ToDecimal(DicPerformance["CEAC"]));
                    }
                    else if (DicPerformance["CEAC"] < 0)
                    {
                        valor = (Convert.ToDecimal(DicPerformance["BEAC"]));
                    }
                    else if (DicPerformance["BEAC"] + DicPerformance["CEAC"] < 0)
                    {
                        valor = -1;
                    }
                    else
                        valor = (Convert.ToDecimal(DicPerformance["BEAC"] + DicPerformance["CEAC"])) / 2;
                    break;
                case "C": valor = DicPerformance["CEAC"];
                    break;
                case "D": valor = DicPerformance["DEAC"];
                    break;
                case "E": valor = DicPerformance["FEAC"]; 
                    break;
                case "F": valor = DicPerformance["GEAC"];
                    break;
                case "G":
                    if (DicPerformance["JEAC"] < 0)
                    {
                        valor = (Convert.ToDecimal(DicPerformance["HEAC"]));
                    }
                    else if (DicPerformance["HEAC"] < 0)
                    {
                        valor = (Convert.ToDecimal(DicPerformance["JEAC"]));
                    }
                    else if (DicPerformance["HEAC"] + DicPerformance["JEAC"] < 0)
                    {
                        valor = -1;
                    }
                    else
                        valor = (Convert.ToDecimal(DicPerformance["JEAC"] + DicPerformance["HEAC"])) / 2;
                    
                    break;
                case "H": valor = DicPerformance["IEAC"];
                    break;
                case "I": valor = DicPerformance["KEAC"];
                    break;
            }
            return valor;
        }
        public void RegisterPerformancePPP(AbetEntities context, Encuesta encuesta, FormCollection frm, Boolean edit)
        {
            try
            {
                int EscuelaId = context.Carrera.FirstOrDefault(x => x.IdCarrera == encuesta.IdCarrera).IdEscuela.Value;

                var lstOutcome = context.OutcomeEncuestaPPPConfig.Where(x=>x.IdEscuela == EscuelaId && x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO).OrderBy(x => x.Orden).AsQueryable();
                List<OutcomeEncuestaPPPConfig> lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList();
                List<PreguntaAdicional> lstPreguntaAdicional = context.PreguntaAdicional.Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.PPP).ToList();
                List<OutcomeEncuestaPPPConfig> lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && x.EsVisible == true).ToList();

                if (!edit)
                {
                    PerformanceEncuestaPPP performance = null;
                    var idencuesta = encuesta.IdEncuesta;
                    Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                    foreach (var item in lstCE.Where(x => x.EsVisible == true))
                    {
                        performance = new PerformanceEncuestaPPP();
                        performance.IdEncuesta = idencuesta;
                        performance.IdOutcomeEncuestaPPPConfig = item.IdOutcomeEncuestaPPPConfig;
                        performance.PuntajeOutcome = frm["CE-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();
                        context.PerformanceEncuestaPPP.Add(performance);
                        if (performance.PuntajeOutcome < 0)
                            DicPerformance[item.NombreEspanol] = -1;
                        else
                            DicPerformance[item.NombreEspanol] = frm["CE-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();
                    }
                    foreach (var item in lstCE.Where(x => x.EsVisible == false))//CAC
                    {
                        performance = new PerformanceEncuestaPPP();
                        performance.IdEncuesta = idencuesta;
                        performance.IdOutcomeEncuestaPPPConfig = item.IdOutcomeEncuestaPPPConfig;
                        performance.PuntajeOutcome = ConvertirOutcome_DesdeEAC_ParaCAC(item.NombreEspanol, DicPerformance);
                        context.PerformanceEncuestaPPP.Add(performance);
                    }
                    foreach (var item in lstCG)
                    {
                        performance = new PerformanceEncuestaPPP();
                        performance.IdEncuesta = idencuesta;
                        performance.IdOutcomeEncuestaPPPConfig = item.IdOutcomeEncuestaPPPConfig;
                        performance.PuntajeOutcome = frm["CG-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();
                        context.PerformanceEncuestaPPP.Add(performance);
                    }
                    foreach (var item in lstPreguntaAdicional)
                    {
                        performance = new PerformanceEncuestaPPP();
                        performance.IdEncuesta = idencuesta;
                        performance.IdPreguntaAdicional = item.IdPreguntaAdicional;
                        performance.PuntajeOutcome = frm["PA-" + item.IdPreguntaAdicional].ToInteger();
                        context.PerformanceEncuestaPPP.Add(performance);
                    }
                }
                else
                {
                    var lstPerformance = context.PerformanceEncuestaPPP.Include(x => x.OutcomeEncuestaPPPConfig).Include(x => x.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta).Where(x => x.IdEncuesta == encuesta.IdEncuesta).OrderBy(x => x.OutcomeEncuestaPPPConfig.Orden).ToList();
                    Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                    List<PerformanceEncuestaPPP> lstPerfomanceCAC = new List<PerformanceEncuestaPPP>();//lstPerformance.Where(x => x.OutcomeEncuestaConfig.EsVisible == false).ToList();
                    foreach (var item in lstPerformance)
                    {
                        if (item.OutcomeEncuestaPPPConfig != null)
                        {
                            if (item.OutcomeEncuestaPPPConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                            {
                                if (item.OutcomeEncuestaPPPConfig.EsVisible == true)
                                {
                                    item.PuntajeOutcome = frm["CE-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();
                                    //if (item.PuntajeOutcome < 0)
                                    //    DicPerformance[item.OutcomeEncuestaConfig.NombreEspanol + item.OutcomeEncuestaConfig.Comision.Codigo] = 0;
                                    //else
                                    DicPerformance[item.OutcomeEncuestaPPPConfig.NombreEspanol] = frm["CE-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();

                                }
                                else
                                {
                                    lstPerfomanceCAC.Add(item);
                                }
                            }
                            else
                            {
                                if (item.OutcomeEncuestaPPPConfig.EsVisible == true)
                                    item.PuntajeOutcome = frm["CG-" + item.IdOutcomeEncuestaPPPConfig].ToInteger();
                            }
                        }
                        else
                        {
                            item.PuntajeOutcome = frm["PA-" + item.IdPreguntaAdicional].ToInteger();
                        }
                    }
                    foreach (var item in lstPerfomanceCAC)
                    {
                        item.PuntajeOutcome = ConvertirOutcome_DesdeEAC_ParaCAC(item.OutcomeEncuestaPPPConfig.NombreEspanol, DicPerformance);
                    }
                }
                context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw;
            }
            
        }
        public void RegisterPerformanceGRA(AbetEntities context, Encuesta encuesta, FormCollection frm, Boolean edit)
        {
            var lstOutcome = context.OutcomeEncuestaConfig.Include(x => x.TipoOutcomeEncuesta).Include(x => x.TipoEncuesta).Where(x => x.TipoEncuesta.Acronimo == ConstantHelpers.ENCUESTA.GRA && x.Estado == ConstantHelpers.ENCUESTA.ESTADO_ACTIVO && x.IdSubModalidadPeriodoAcademico == encuesta.IdSubModalidadPeriodoAcademico && x.IdCarrera == encuesta.IdCarrera).OrderBy(x => x.IdComision).ThenBy(x => x.Orden).AsQueryable();
            List<OutcomeEncuestaConfig> lstCE = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA).ToList(); 
            List<OutcomeEncuestaConfig> lstCG = lstOutcome.Where(x => x.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_GENERAL && x.EsVisible == true).ToList(); 

            if (!edit)
            {
                PerformanceEncuesta performance = null;
                var idencuesta = encuesta.IdEncuesta;
                Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                foreach (var item in lstCE.Where( x => x.EsVisible == true))
                {
                    performance = new PerformanceEncuesta();
                    performance.IdEncuesta = idencuesta;
                    performance.IdOutcomeEncuestaConfig = item.IdOutcomeEncuestaConfig;
                    performance.PuntajeOutcome = frm["CE-" + item.IdOutcomeEncuestaConfig].ToInteger();
                    context.PerformanceEncuesta.Add(performance);
                    //if (performance.PuntajeOutcome < 0)
                    //    DicPerformance[item.NombreEspanol + item.Comision.Codigo] = 0;
                    //else
                        DicPerformance[item.NombreEspanol + item.Comision.Codigo] = frm["CE-" + item.IdOutcomeEncuestaConfig].ToInteger();
                }
                foreach (var item in lstCE.Where(x => x.EsVisible == false))
                {
                    performance = new PerformanceEncuesta();
                    performance.IdEncuesta = idencuesta;
                    performance.IdOutcomeEncuestaConfig = item.IdOutcomeEncuestaConfig;
                    performance.PuntajeOutcome = ConvertirOutcome_DesdeEAC_ParaCAC(item.NombreEspanol, DicPerformance);
                    context.PerformanceEncuesta.Add(performance);
                }
                foreach (var item in lstCG)
                {
                    performance = new PerformanceEncuesta();
                    performance.IdEncuesta = idencuesta;
                    performance.IdOutcomeEncuestaConfig = item.IdOutcomeEncuestaConfig;
                    performance.PuntajeOutcome = frm["CG-" + item.IdOutcomeEncuestaConfig].ToInteger();
                    context.PerformanceEncuesta.Add(performance);
                }
            }
            else
            {
                var lstPerformance = context.PerformanceEncuesta
                        .Include(x => x.OutcomeEncuestaConfig)
                        .Include(x => x.OutcomeEncuestaConfig.TipoOutcomeEncuesta)
                        .Where(x => x.IdEncuesta == encuesta.IdEncuesta)
                        .OrderBy(x => x.OutcomeEncuestaConfig.IdComision)
                        .ThenBy(x => x.OutcomeEncuestaConfig.Orden)
                        .AsQueryable();
                //var lstPerfomanceCAC = lstPerformance.Where(x => x.OutcomeEncuestaConfig.EsVisible == false).AsQueryable();
                List<PerformanceEncuesta> lstPerfomanceCAC = new List<PerformanceEncuesta>();
                Dictionary<String, Int32> DicPerformance = new Dictionary<String, Int32>();
                foreach (var item in lstPerformance)
                {
                    if (item.OutcomeEncuestaConfig.TipoOutcomeEncuesta.NombreEspanol == ConstantHelpers.ENCUESTA.COMPETENCIA_ESPECIFICA)
                    {
                        if (item.OutcomeEncuestaConfig.EsVisible == true)
                        {
                            item.PuntajeOutcome = frm["CE-" + item.IdOutcomeEncuestaConfig].ToInteger();
                            //if (item.PuntajeOutcome < 0)
                            //    DicPerformance[item.OutcomeEncuestaConfig.NombreEspanol + item.OutcomeEncuestaConfig.Comision.Codigo] = 0;
                            //else
                            DicPerformance[item.OutcomeEncuestaConfig.NombreEspanol + item.OutcomeEncuestaConfig.Comision.Codigo] = frm["CE-" + item.IdOutcomeEncuestaConfig].ToInteger();
                        }
                        else
                        {
                            lstPerfomanceCAC.Add(item);
                        }
                    }
                    else
                    {
                        item.PuntajeOutcome = frm["CG-" + item.IdOutcomeEncuestaConfig].ToInteger();
                    }
                }
                foreach(var item in lstPerfomanceCAC)
                {
                    item.PuntajeOutcome = ConvertirOutcome_DesdeEAC_ParaCAC(item.OutcomeEncuestaConfig.NombreEspanol, DicPerformance);
                }
            }
            context.SaveChanges();
        }


        public List<Usp_Ins_RegistarPerformancePPP_Result> ObtenerNuevosPuntajesArizona(AbetEntities context,List<decimal> Outcomes, int idSubModalidadPeriodoAcademico, int idCarrera)
        {
            
            List<Usp_Ins_RegistarPerformancePPP_Result> lista = new List<Usp_Ins_RegistarPerformancePPP_Result>();

            List<Usp_Ins_RegistarPerformancePPP_Result> aux = context.Usp_Ins_RegistarPerformancePPP(Outcomes[0],
                Outcomes[1], Outcomes[2], Outcomes[3], Outcomes[4], Outcomes[5], Outcomes[6], Outcomes[7], Outcomes[8]
                , Outcomes[9], Outcomes[10], Outcomes[11], Outcomes[12], Outcomes[13], idSubModalidadPeriodoAcademico, idCarrera).ToList();

            if (aux.Count > 0)
            {
                lista = aux;
            }


            return lista;

        }
    }
}
