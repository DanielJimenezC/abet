﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace UPC.CA.ABET.Logic.Areas.Survey
{
    public class ReportLogic
    {
        public FileContentResult GenerarReporte(String Type, String Path, String NombreArchivo, List<ReportParameter> ReportParameters)
        {

            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Path;

            if (ReportParameters != null)
            {
                localReport.SetParameters(ReportParameters);
            }

            string reportType = Type;
            string mimeType;
            string encoding;
            string fileNameExtension;


            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + Type + "</OutputFormat>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            FileContentResult File = new FileContentResult(renderedBytes, mimeType) { FileDownloadName = NombreArchivo + ".pdf" };
            return File;

        }
        public FileContentResult GenerarReporte(List<ReportDataSource> ReportDataSource, String Type, String Path, String NombreArchivo, List<ReportParameter> ReportParameters)
        {
            
            try
            {
                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Path;

                if (ReportParameters != null)
                {
                    localReport.SetParameters(ReportParameters);
                }

                foreach (var item in ReportDataSource)
                {
                    localReport.DataSources.Add(item);
                }


                string reportType = Type;
                string mimeType;
                string encoding;
                string fileNameExtension;


                string deviceInfo =

                "<DeviceInfo>" +
                "  <OutputFormat>" + Type + "</OutputFormat>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);
                var extension = "";
                switch (Type)
                {
                    case "PDF":
                        extension = ".pdf";
                        break;
                    case "EXCEL":
                        extension = ".xls";
                        break;
                    case "WORD":
                        extension = ".doc";
                        break;
                }
                FileContentResult File = new FileContentResult(renderedBytes, mimeType) { FileDownloadName = NombreArchivo + extension };
                return File;
            }
            catch(Exception ex)
            {
                throw;
            }
            

        }
    }
}
