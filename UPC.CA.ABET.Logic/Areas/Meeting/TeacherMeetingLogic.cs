﻿
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Admin;
using System.Data.Entity.Infrastructure;
using System;
using static UPC.CA.ABET.Helpers.ConstantHelpers;
using static UPC.CA.ABET.Helpers.ConstantHelpers.TEACHER_MEETING.ESTADO;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Meeting
{
    public class TeacherMeetingLogic
    {
        private readonly AbetEntities AbetEntities;
        private readonly ImprovementActionLogic ImprovementActionLogic;

        public TeacherMeetingLogic()
        {
            AbetEntities = new AbetEntities();
            ImprovementActionLogic = new ImprovementActionLogic();
        }

        public ReunionProfesor FindTeacherMeetingById(int idReunionProfesor)
        {
            return AbetEntities.ReunionProfesor.FirstOrDefault(x => x.IdReunionProfesor == idReunionProfesor);
        }

        public IEnumerable<ReunionProfesorConfiguracion> GetTeacherMeetingConfigurationsByModalitydId(int modalityId)
        {
            return AbetEntities.ReunionProfesorConfiguracion
                .Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.IdModalidad == modalityId)
                .ToList();
        }

        public IEnumerable<int> GetWeeksLeftForTheSameTypeOfTeacherMeeting(ReunionProfesor teacherMeeting)
        {
            return AbetEntities.ReunionProfesor.Where(x => x.Nivel == teacherMeeting.Nivel
            && x.IdSubModalidadPeriodoAcademicoModulo == teacherMeeting.IdSubModalidadPeriodoAcademicoModulo
            && x.IdUnidadAcademica == teacherMeeting.IdUnidadAcademica
            && x.NumSemana > teacherMeeting.NumSemana
            ).Select(x => (int)x.NumSemana);
        }

        public IEnumerable<SubModalidadPeriodoAcademicoModulo> GetSubModalityAcademicPeriodModulesNotConfiguredForTeacherMeetings(int modalityId)
        {
            var filterDate = new DateTime(2019, 1, 1);
            var sMAPM = AbetEntities.SubModalidadPeriodoAcademicoModulo
                .Where(x => x.SubModalidadPeriodoAcademico.SubModalidad.IdModalidad == modalityId && x.Modulo.FechaInicio.Value > filterDate).ToList();
            var fltered = sMAPM.Where(x => !x.ReunionProfesorConfiguracion.Select(y => y.SubModalidadPeriodoAcademicoModulo).Contains(x))
                .ToList();
            return fltered;
        }

        public ReunionProfesorConfiguracion GetConfigurationForSubModalityAcademicPeriodModule(int subModalityAcademicPeriodModuleId)
        {
            return AbetEntities.ReunionProfesorConfiguracion.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == subModalityAcademicPeriodModuleId);
        }

        public async Task<bool> CreateTeacherMeetingConfigurationForAreaAndSubArea(int subModalityAcademicPeriodModuleId, string FirstLevelWeeks, string SecondLevelWeeks, int FirstLevelDueDate, int SecondLevelDueDate)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                var firstLevel = new ReunionProfesorConfiguracion
                {
                    IdSubModalidadPeriodoAcademicoModulo = subModalityAcademicPeriodModuleId,
                    Nivel = (int)ORGANIZATION_CHART_LEVELS.FIRST_LEVEL,
                    PlazoEnDias = FirstLevelDueDate,
                    Semanas = FirstLevelWeeks,
                    FechaCreacion = dateFormatted.ToDateTime(),
                    FechaActualizacion = dateFormatted.ToDateTime()
                };

                var secondLevel = new ReunionProfesorConfiguracion
                {
                    IdSubModalidadPeriodoAcademicoModulo = subModalityAcademicPeriodModuleId,
                    Nivel = (int)ORGANIZATION_CHART_LEVELS.SECOND_LEVEL,
                    PlazoEnDias = SecondLevelDueDate,
                    Semanas = SecondLevelWeeks,
                    FechaCreacion = dateFormatted.ToDateTime(),
                    FechaActualizacion = dateFormatted.ToDateTime()
                };
                AbetEntities.ReunionProfesorConfiguracion.AddRange(new List<ReunionProfesorConfiguracion> { firstLevel, secondLevel });
                var result = await AbetEntities.SaveChangesAsync();
                if (result == 2)
                    return true;
                return false;
            } catch (Exception)
            {
                throw;
            }
            
        }

        public async Task<Tuple<bool, string>> CreateTeacherMeetingWithGuests(int? idSubModalidadPeriodoAcademico, int idModulo, int? idEscuela)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var subModalityAcademicPeriodModule = AbetEntities.SubModalidadPeriodoAcademicoModulo
                .FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico.Value
                && x.IdModulo == idModulo);

            if (subModalityAcademicPeriodModule == null)
                return new Tuple<bool, string>(false, "No existe el período indicado.");

            var subModalityAcademicPeriodModuleId = subModalityAcademicPeriodModule.IdSubModalidadPeriodoAcademicoModulo;

            using (var dbContextTransaction = AbetEntities.Database.BeginTransaction())
            {
                try
                {
                    //var teachersResult = await Task.Run(() => AbetEntities.Usp_CrearReunionesProfesor(idSubModalidadPeriodoAcademico.Value, idEscuela.Value));

                    var teacherMeetingConfiguration = await Task.Run(() => AbetEntities.ReunionProfesorConfiguracion.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == subModalityAcademicPeriodModule.IdSubModalidadPeriodoAcademicoModulo));
                    if (teacherMeetingConfiguration == null)
                    {
                        return new Tuple<bool, string>(false, "Se desconoce en qué semanas deberán crearse las reuniones. Por favor, configure las semanas primero.");
                    }
                    var createTeacherMeetingsResult = await Task.Run(() => AbetEntities.Usp_CrearReunionesProfesor(subModalityAcademicPeriodModuleId, idEscuela.Value));
                    if (createTeacherMeetingsResult < 1)
                    {
                        dbContextTransaction.Rollback();
                        return new Tuple<bool, string>(false, "Las reuniones de profesores no fueron creadas correctamente.");
                    }
                    var createTeacherMeetingParticipantsResult = await Task.Run(() => AbetEntities.Usp_CrearReunionProfesorParticipantes(subModalityAcademicPeriodModuleId, idEscuela.Value));
                    if (createTeacherMeetingParticipantsResult < 1)
                    {
                        dbContextTransaction.Rollback();
                        return new Tuple<bool, string>(false, "Los invitados de la reunión no fueron creados correctamente.");
                    }
                    var createTeacherMeetingOperativeReportResult = await Task.Run(() => AbetEntities.Usp_CrearReunionProfesorReporteOperativo(subModalityAcademicPeriodModuleId, idEscuela.Value));
                    if (createTeacherMeetingOperativeReportResult < 1)
                    {
                        dbContextTransaction.Rollback();
                        return new Tuple<bool, string>(false, "La sección de reporte operativo no fue creado correctamente.");
                    }
                    var createTeacherMeetingImprovementActionsResult = await Task.Run(() => AbetEntities.Usp_CrearReunionProfesorAccionMejora(subModalityAcademicPeriodModuleId, idEscuela.Value));
                    //if (createTeacherMeetingImprovementActionsResult < 1)
                    //{
                    //    dbContextTransaction.Rollback();
                    //    return new Tuple<bool, string>(false, "Las acciones de mejora no fueron vinculadas correctamente.");
                    //}
                    await AbetEntities.SaveChangesAsync();
                    dbContextTransaction.Commit();
                    return new Tuple<bool, string>(true, "El proceso fue completado exitosamente.");
                }
                catch (EntityCommandExecutionException ex)
                {
                    dbContextTransaction.Rollback();
                    var newErrorLog = new LogErrores
                    {
                        Detail = ex.Message,
                        FechaCreacion = dateFormatted.ToDateTime()
                    };
                    AbetEntities.LogErrores.Add(newErrorLog);
                    AbetEntities.SaveChanges();
                    return new Tuple<bool, string>(false, "El proceso no fue completado correctamente.");
                    throw;
                }
            }
        }

        public IEnumerable<PeriodoAcademico> GetAcademicPeriodsBySubModalityPeriod(int? idSubModalidadPeriodoAcademico)
        {
            var subModalityAcademicPeriods = AbetEntities.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico.Value);
            var academicPeriods = subModalityAcademicPeriods.Select(x => x.PeriodoAcademico).ToList();
            return academicPeriods;
        }

        public ReunionProfesor TeacherMeetingById(int idReunionProfesor)
        {
            return AbetEntities.ReunionProfesor.Include("ReunionProfesorParticipante").FirstOrDefault(x => x.IdReunionProfesor == idReunionProfesor);
        }

        public bool IsResponsibleForTheMeeting(int teacherMeetingId, int teacherId, int subModalityAcademicPeriodId)
        {
            var academicUnitIds = GetAllAcademicUnitIdsByTeacherId(teacherId, subModalityAcademicPeriodId);
            var result = AbetEntities.ReunionProfesor
                .Any(x => x.IdReunionProfesor == teacherMeetingId
                && academicUnitIds.Contains(x.IdUnidadAcademica)
                && x.UnidadAcademica.SedeUnidadAcademica.Any(y =>
                    y.UnidadAcademicaResponsable.Any(q => q.IdDocente == teacherId)));
            return result;
        }
        public IEnumerable<ReunionProfesorParticipante> GetAllTeacherMeetingParticipants(int idReunionProfesor)
        {
            var result = AbetEntities.ReunionProfesorParticipante.Include("ReunionProfesor").Where(x => x.IdReunionProfesor == idReunionProfesor).ToList();
            return result;
        }

        public List<ReunionProfesorParticipanteInvitado> GetAllGuestTeacherMeetingParticipants(int idReunionProfesor)
        {
            var result = AbetEntities.ReunionProfesorParticipanteInvitado.Include("ReunionProfesor").Where(x => x.IdReunionProfesor == idReunionProfesor).ToList();
            return result;
        }

        public IEnumerable<ReunionProfesorAccionMejora> GetAllTeacherMeetingImprovementActions(int idReunionProfesor)
        {
            var result = AbetEntities.Database.SqlQuery<ReunionProfesorAccionMejora>("Usp_ObtenerReunionProfesorAccionMejora {0}", idReunionProfesor).ToList();
            foreach (var item in result)
            {
                item.AccionMejora = ImprovementActionLogic.FindImprovementActionById(item.IdAccionMejora);
                if (item.IdReunionProfesor.HasValue)
                    item.ReunionProfesor = FindTeacherMeetingById(item.IdReunionProfesor.Value);
            }
            return result;
        }

        public IEnumerable<ReunionProfesorAcuerdo> GetPreviousTeacherMeetingAgreements(int idReunionProfesor)
        {
            var previousAgreements = AbetEntities.ReunionProfesorAcuerdo
                .Where(x => x.ReunionProfesorAcuerdoHistorico.Any(y => y.IdUltimaReunionProfesor == idReunionProfesor)).ToList();
            foreach (var item in previousAgreements)
            {
                item.ReunionProfesorAcuerdoHistorico = item.ReunionProfesorAcuerdoHistorico
                    .Where(x => x.IdUltimaReunionProfesor == idReunionProfesor).ToList();
            }
            return previousAgreements;
        }

        public IEnumerable<ReunionProfesorAcuerdoHistorico> GetPreviousTeacherMeetingAgreementsForHistoricReports(int idReunionProfesor)
        {
            return AbetEntities.ReunionProfesorAcuerdoHistorico.Where(y => y.IdUltimaReunionProfesor == idReunionProfesor).ToList();            
        }

        public IEnumerable<Curso> GetAllCoursesToCreateOperationalReports(int idReunionProfesor)
        {
            //var cursosAsignados = AbetEntities.ReunionProfesorReporteOperativo.Include("Curso").Where(x => x.IdReunionProfesor == idReunionProfesor).Select(x => x.Curso).Where(y => y.ReunionProfesorReporteOperativo.Any(z => z.IdReunionProfesor == idReunionProfesor)).Distinct().ToList();
            var cursos = AbetEntities.ReunionProfesorReporteOperativo.Include("Curso").Where(x => x.IdReunionProfesor == idReunionProfesor).Select(x => x.Curso).Distinct().ToList();
            foreach (var curso in cursos)
            {
                curso.ReunionProfesorReporteOperativo = curso.ReunionProfesorReporteOperativo.Where(x => x.IdReunionProfesor == idReunionProfesor).ToList();
            }
            cursos.OrderBy(x => x.NombreEspanol);
            return cursos;
        }

        public IEnumerable<ReunionProfesorHallazgo> GetTeacherMeetingFindingsByTeacherMeetingId(int idReunionProfesor)
        {
            return AbetEntities.ReunionProfesorHallazgo.Where(x => x.IdReunionProfesor == idReunionProfesor).ToList();
        }

        public bool ExistTeacherMeetingFindingsTaskForTMTaskAndTMFinding(int teacherMeetingTaskId, int teacherMeetingFindingId, int teacherMeetingAgreementId)
        {
            var exist = AbetEntities.ReunionProfesorHallazgoTarea
                .Where(x => x.IdReunionProfesorTarea == teacherMeetingTaskId && x.IdReunionProfesorHallazgo == teacherMeetingFindingId
                 && x.ReunionProfesorTarea.ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo == teacherMeetingAgreementId)
                .FirstOrDefault();

            if (exist == null)
            {
                return false;
            }

            return true;
        }

        public ReunionProfesorHallazgoTarea GetTeacherMeetingFindingsTaskByFindingAndTask(int findingId, int taskId)
        {
            return AbetEntities.ReunionProfesorHallazgoTarea
                .FirstOrDefault(x => x.IdReunionProfesorHallazgo == findingId && x.IdReunionProfesorTarea == taskId);
        }

        public IEnumerable<ReunionProfesorHallazgo> GetTeacherMeetingFindingsUnassignedByTeacherMeetingId(int idReunionProfesor)
        {
            return AbetEntities.Usp_GetHallazgosDesasignadosPorReunionProfesor(idReunionProfesor)
                .Select(x => new ReunionProfesorHallazgo
                {
                    IdReunionProfesor = x.IdReunionProfesor.Value,
                    IdReunionProfesorHallazgo = x.IdReunionProfesorHallazgo.Value,
                    DescripcionEspanol = x.DescripcionEspanol,
                    DescripcionIngles = x.DescripcionIngles
                })
                .ToList();
        }

        public ReunionProfesorHallazgo AddTeacherMeetingFinding(ReunionProfesorHallazgo entity)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var newEntity = new ReunionProfesorHallazgo()
            {
                IdReunionProfesor = entity.IdReunionProfesor,
                DescripcionEspanol = entity.DescripcionEspanol,
                DescripcionIngles = entity.DescripcionIngles,
                IdAreaUnidadAcademica = entity.IdAreaUnidadAcademica,
                IdSubAreaUnidadAcademica = entity.IdSubAreaUnidadAcademica,
                IdCursoPeriodoAcademico = entity.IdCursoPeriodoAcademico,
                IdSede = entity.IdSede,
                IdSeccion = entity.IdSeccion,
                FechaCreacion = dateFormatted.ToDateTime(),
                FechaActualizacion = dateFormatted.ToDateTime(),
            };
            try
            {
                var newReunionProfesorHallazgo = AbetEntities.ReunionProfesorHallazgo.Add(newEntity);
                AbetEntities.SaveChanges();
                return newReunionProfesorHallazgo;
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                AbetEntities.LogErrores.Add(newErrorLog);
                AbetEntities.SaveChanges();
                throw;
            }
        }

        public ReunionProfesorHallazgo UpdateTeacherMeetingFinding(ReunionProfesorHallazgo update)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var entity = new ReunionProfesorHallazgo()
            {
                IdReunionProfesorHallazgo = update.IdReunionProfesorHallazgo,
                IdReunionProfesor = update.IdReunionProfesor,
                DescripcionEspanol = update.DescripcionEspanol,
                DescripcionIngles = update.DescripcionIngles,
                IdAreaUnidadAcademica = update.IdAreaUnidadAcademica,
                IdSubAreaUnidadAcademica = update.IdSubAreaUnidadAcademica,
                IdCursoPeriodoAcademico = update.IdCursoPeriodoAcademico,
                IdSede = update.IdSede,
                IdSeccion = update.IdSeccion
            };
            try
            {
                var reunionProfesorHallazgo = AbetEntities.ReunionProfesorHallazgo.FirstOrDefault(x => x.IdReunionProfesorHallazgo == entity.IdReunionProfesorHallazgo);
                reunionProfesorHallazgo.DescripcionEspanol = entity.DescripcionEspanol;
                reunionProfesorHallazgo.DescripcionIngles = entity.DescripcionIngles;
                reunionProfesorHallazgo.IdAreaUnidadAcademica = entity.IdAreaUnidadAcademica;
                reunionProfesorHallazgo.IdSubAreaUnidadAcademica = entity.IdSubAreaUnidadAcademica;
                reunionProfesorHallazgo.IdCursoPeriodoAcademico = entity.IdCursoPeriodoAcademico;
                reunionProfesorHallazgo.IdSede = entity.IdSede;
                reunionProfesorHallazgo.IdSeccion = entity.IdSeccion;
                reunionProfesorHallazgo.FechaActualizacion = dateFormatted.ToDateTime();
                AbetEntities.SaveChanges();
                return reunionProfesorHallazgo;
            }
            catch (DbUpdateException ex)
            {
                var newErrorLog = new LogErrores
                {
                    Detail = ex.Message,
                    FechaCreacion = dateFormatted.ToDateTime()
                };
                AbetEntities.LogErrores.Add(newErrorLog);
                AbetEntities.SaveChanges();
                throw;
            }
        }

        public IEnumerable<ReunionProfesorAcuerdo> GetAllTeacherMeetingAgreements(int idReunionProfesor)
        {
            var teacherMeetingAgreements = AbetEntities.ReunionProfesorAcuerdo
                .Where(x => x.IdReunionProfesor == idReunionProfesor)
                .ToList();

            return teacherMeetingAgreements;
        }

        public ReunionProfesorAcuerdo GetTeacherMeetingAgreement(int teacherMeetingAgreementId, int forTeacherMeetingId)
        {
            var teacherMeetingAgreement = AbetEntities.ReunionProfesorAcuerdo.FirstOrDefault(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId);
            teacherMeetingAgreement.ReunionProfesorAcuerdoHistorico = teacherMeetingAgreement.ReunionProfesorAcuerdoHistorico.Where(x => x.IdUltimaReunionProfesor == forTeacherMeetingId).ToList();
            return teacherMeetingAgreement;
        }

        public IEnumerable<Docente> GetTeacherMeetingAgreementResponsiblesByAgreementId(int teacherMeetingAgreementId)
        {
            var responsibleTeachers = (from rpa in AbetEntities.ReunionProfesorAcuerdo
                                       join rpt in AbetEntities.ReunionProfesorTarea on rpa.IdReunionProfesorAcuerdo equals rpt.IdReunionProfesorAcuerdo
                                       join rpht in AbetEntities.ReunionProfesorHallazgoTarea on rpt.IdReunionProfesorTarea equals rpht.IdReunionProfesorTarea
                                       join rph in AbetEntities.ReunionProfesorHallazgo on rpht.IdReunionProfesorHallazgo equals rph.IdReunionProfesorHallazgo
                                       join rphtr in AbetEntities.ReunionProfesorHallazgoTareaResponsable on rpht.IdReunionProfesorHallazgoTarea equals rphtr.IdReunionProfesorHallazgoTarea
                                       join d in AbetEntities.Docente on rphtr.IdDocenteResponsable equals d.IdDocente
                                       where rpa.IdReunionProfesorAcuerdo == teacherMeetingAgreementId
                                       select d).Distinct().ToList();

            return responsibleTeachers;
        }


        public IEnumerable<ReunionProfesorParticipanteInvitado> GetGuestTeacherMeetingAgreementResponsiblesByAgreementId(int teacherMeetingAgreementId)
        {
            var responsibleTeachers = (from rpa in AbetEntities.ReunionProfesorAcuerdo
                                       join rpt in AbetEntities.ReunionProfesorTarea on rpa.IdReunionProfesorAcuerdo equals rpt.IdReunionProfesorAcuerdo
                                       join rpht in AbetEntities.ReunionProfesorHallazgoTarea on rpt.IdReunionProfesorTarea equals rpht.IdReunionProfesorTarea
                                       join rph in AbetEntities.ReunionProfesorHallazgo on rpht.IdReunionProfesorHallazgo equals rph.IdReunionProfesorHallazgo
                                       join rphtr in AbetEntities.ReunionProfesorInvitadoHallazgoTareaResponsable on rpht.IdReunionProfesorHallazgoTarea equals rphtr.IdReunionProfesorHallazgoTarea
                                       join d in AbetEntities.ReunionProfesorParticipanteInvitado on rphtr.IdReunionProfesorParticipanteInvitado equals d.IdReunionProfesorParticipanteInvitado
                                       where rpa.IdReunionProfesorAcuerdo == teacherMeetingAgreementId
                                       select d).Distinct().ToList();

            return responsibleTeachers;
        }


        public ReunionProfesorAcuerdo GetTeacherMeetingAgreement(int teacherMeetingAgreementId)
        {
            var teacherMeetingAgreement = AbetEntities
                .ReunionProfesorAcuerdo
                .FirstOrDefault(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId);
            return teacherMeetingAgreement;
        }

        public int AddTeacherMeetingAgreement(int teacherMeetingId, string descripcionEspanol, string descripcionIngles, string semanaDeadline, string fechaDeadline)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var actualTeacherMeeting = AbetEntities.ReunionProfesor.FirstOrDefault(x => x.IdReunionProfesor == teacherMeetingId);
            ReunionProfesorAcuerdo newRPA = new ReunionProfesorAcuerdo
            {
                IdReunionProfesor = teacherMeetingId,
                DescripcionEspanol = descripcionEspanol,
                DescripcionIngles = descripcionIngles,
                IdEstado = (int)ACUERDO.PENDIENTE,
                SemanaDeadline = int.Parse(semanaDeadline),
                FechaDeadline = fechaDeadline.ToDateTime(),
                FechaCreacion = dateFormatted.ToDateTime(),
                FechaActualizacion = dateFormatted.ToDateTime(),
            };

            AbetEntities.ReunionProfesorAcuerdo.Add(newRPA);

            var nextTeacherMeetings = AbetEntities.ReunionProfesor.Where(x =>
                x.IdSubModalidadPeriodoAcademicoModulo == actualTeacherMeeting.IdSubModalidadPeriodoAcademicoModulo &&
                x.IdUnidadAcademica == actualTeacherMeeting.IdUnidadAcademica &&
                x.Nivel == actualTeacherMeeting.Nivel &&
                x.NumSemana > actualTeacherMeeting.NumSemana
                ).OrderBy(x => x.NumSemana);

            var nextTeacherMeeting = nextTeacherMeetings.Count() > 0 ? nextTeacherMeetings.FirstOrDefault() : null;

            if (nextTeacherMeeting != null)
            {
                ReunionProfesorAcuerdoHistorico newRPAH = new ReunionProfesorAcuerdoHistorico
                {
                    IdUltimaReunionProfesor = nextTeacherMeeting.IdReunionProfesor,
                    IdReunionProfesorAcuerdo = newRPA.IdReunionProfesorAcuerdo,
                    IdEstado = (int)ACUERDO.PENDIENTE,
                    FechaCreacion = dateFormatted.ToDateTime(),
                    FechaActualizacion = dateFormatted.ToDateTime(),
                };
                AbetEntities.ReunionProfesorAcuerdoHistorico.Add(newRPAH);
            }
            AbetEntities.SaveChanges();

            return newRPA.IdReunionProfesorAcuerdo;
        }

        public ReunionProfesorHallazgoTarea AddTeacherMeetingFindingTaskAgreement(int teacherMeetingFindingId, int teacherMeetingTaskId, int teacherMeetingAgreementId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var rphtDb = AbetEntities.ReunionProfesorHallazgoTarea.FirstOrDefault(x => x.IdReunionProfesorHallazgo == teacherMeetingFindingId && x.IdReunionProfesorTarea == teacherMeetingTaskId);

            if (rphtDb != null)
            {
                return rphtDb;
            }

            ReunionProfesorHallazgoTarea newRPHT = new ReunionProfesorHallazgoTarea
            {
                IdReunionProfesorHallazgo = teacherMeetingFindingId,
                IdReunionProfesorTarea = teacherMeetingTaskId,
                FechaActualizacion = dateFormatted.ToDateTime(),
                FechaCreacion = dateFormatted.ToDateTime(),
            };

            AbetEntities.ReunionProfesorHallazgoTarea.Add(newRPHT);
            AbetEntities.SaveChanges();

            return newRPHT;
        }

        public IEnumerable<ReunionProfesorHallazgo> GetTeacherMeetingFindingsByAgreementId(int teacherMeetingAgreementId)
        {
            return (from rpa in AbetEntities.ReunionProfesorAcuerdo
                    join rpt in AbetEntities.ReunionProfesorTarea on rpa.IdReunionProfesorAcuerdo equals rpt.IdReunionProfesorAcuerdo
                    join rpht in AbetEntities.ReunionProfesorHallazgoTarea on rpt.IdReunionProfesorTarea equals rpht.IdReunionProfesorTarea
                    join rph in AbetEntities.ReunionProfesorHallazgo on rpht.IdReunionProfesorHallazgo equals rph.IdReunionProfesorHallazgo
                    where rpa.IdReunionProfesorAcuerdo == teacherMeetingAgreementId
                    select rph).Distinct().ToList();
        }

        public void AddTeacherMeetingFindingTaskAgreementResponsable(int teacherMeetingFindingTaskAgreementId, int teacherId)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            var rphtrDb = AbetEntities.ReunionProfesorHallazgoTareaResponsable.FirstOrDefault(x => x.IdReunionProfesorHallazgoTarea == teacherMeetingFindingTaskAgreementId && x.IdDocenteResponsable == teacherId);

            if (rphtrDb != null)
            {
                return;
            }

            ReunionProfesorHallazgoTareaResponsable newRPHTR = new ReunionProfesorHallazgoTareaResponsable
            {
                IdReunionProfesorHallazgoTarea = teacherMeetingFindingTaskAgreementId,
                IdDocenteResponsable = teacherId,
                FechaCreacion = dateFormatted.ToDateTime(),
                FechaActualizacion = dateFormatted.ToDateTime(),
            };

            AbetEntities.ReunionProfesorHallazgoTareaResponsable.Add(newRPHTR);
            AbetEntities.SaveChanges();
        }

        public void AddGuestTeacherMeetingFindingTaskAgreementResponsable(int teacherMeetingFindingTaskAgreementId, int teacherId)
        {
            
            var rphtrDb = AbetEntities.ReunionProfesorInvitadoHallazgoTareaResponsable.FirstOrDefault(x => x.IdReunionProfesorHallazgoTarea == teacherMeetingFindingTaskAgreementId && x.IdReunionProfesorParticipanteInvitado == teacherId);

            if (rphtrDb != null)
            {
                return;
            }

            ReunionProfesorInvitadoHallazgoTareaResponsable newRPHTR = new ReunionProfesorInvitadoHallazgoTareaResponsable
            {
                IdReunionProfesorHallazgoTarea = teacherMeetingFindingTaskAgreementId,
                IdReunionProfesorParticipanteInvitado = teacherId
            };

            AbetEntities.ReunionProfesorInvitadoHallazgoTareaResponsable.Add(newRPHTR);
            AbetEntities.SaveChanges();
        }




        public int AddTeacherMeetingTask(string spanishTaskDescription, string englishTaskDescription, int idReunionProfesorAcuerdo, string semanaDeadline, string fechaDeadline)
        {
            var now = DateTime.Now;
            var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            ReunionProfesorTarea newRPT = new ReunionProfesorTarea
            {
                IdReunionProfesorAcuerdo = idReunionProfesorAcuerdo,
                DescripcionEspanol = spanishTaskDescription,
                DescripcionIngles = englishTaskDescription,
                IdEstado = (int)TAREA.PENDIENTE,
                SemanaDeadline = int.Parse(semanaDeadline),
                FechaDeadline = fechaDeadline.ToDateTime(),
                FechaCreacion = dateFormatted.ToDateTime(),
                FechaActualizacion = dateFormatted.ToDateTime(),
            };

            AbetEntities.ReunionProfesorTarea.Add(newRPT);

            var actualTeacherMeetingAgreement = AbetEntities.ReunionProfesorAcuerdo.FirstOrDefault(x => x.IdReunionProfesorAcuerdo == idReunionProfesorAcuerdo);
            var originalTeacherMeeting = actualTeacherMeetingAgreement.ReunionProfesor;
            var nextTeacherMeetings = AbetEntities.ReunionProfesor.Where(x =>
                x.IdSubModalidadPeriodoAcademicoModulo == originalTeacherMeeting.IdSubModalidadPeriodoAcademicoModulo &&
                x.IdUnidadAcademica == originalTeacherMeeting.IdUnidadAcademica &&
                x.Nivel == originalTeacherMeeting.Nivel &&
                x.NumSemana > originalTeacherMeeting.NumSemana
                ).OrderBy(x => x.NumSemana);

            var nextTeacherMeeting = nextTeacherMeetings.Count() > 0 ? nextTeacherMeetings.FirstOrDefault() : null;

            if (nextTeacherMeeting != null)
            {
                ReunionProfesorTareaHistorico newRPTH = new ReunionProfesorTareaHistorico
                {
                    IdUltimaReunionProfesor = nextTeacherMeeting.IdReunionProfesor,
                    IdReunionProfesorTarea = newRPT.IdReunionProfesorTarea,
                    IdReunionProfesorAcuerdo = actualTeacherMeetingAgreement.IdReunionProfesorAcuerdo,
                    IdEstado = (int)TAREA.PENDIENTE,
                    FechaCreacion = dateFormatted.ToDateTime(),
                    FechaActualizacion = dateFormatted.ToDateTime(),
                };
                AbetEntities.ReunionProfesorTareaHistorico.Add(newRPTH);
            }
            AbetEntities.SaveChanges();

            return newRPT.IdReunionProfesorTarea;
        }

        private IEnumerable<int> GetAllAcademicUnitIdsByTeacherId(int teacherId, int subModalityAcademicPeriodId)
        {
            return (from uar in AbetEntities.UnidadAcademicaResponsable
                    join sua in AbetEntities.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join d in AbetEntities.Docente on uar.IdDocente equals d.IdDocente
                    join ua in AbetEntities.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where ua.IdSubModalidadPeriodoAcademico == subModalityAcademicPeriodId && d.IdDocente == teacherId
                    select ua.IdUnidadAcademica).Distinct().ToList();
        }

        public void DeleteTeacherMeetingFindingsTask(int hallazgoTareaId, int teacherMeetingAgreementId)
        {
            var docentes = AbetEntities.ReunionProfesorHallazgoTareaResponsable
                .Where(x => x.IdReunionProfesorHallazgoTarea == hallazgoTareaId)
                .ToList();

            AbetEntities.ReunionProfesorHallazgoTareaResponsable.RemoveRange(docentes);
            AbetEntities.SaveChanges();

            var hallazgoTarea = AbetEntities.ReunionProfesorHallazgoTarea
                .First(x => x.IdReunionProfesorHallazgoTarea == hallazgoTareaId);

            AbetEntities.ReunionProfesorHallazgoTarea.Remove(hallazgoTarea);
            AbetEntities.SaveChanges();
        }

        public void DeleteTeacherMeetingFindingTaskResponsableByTaskAndTeacher(int hallazgoTareaId, int docenteId)
        {
            var responsable = AbetEntities.ReunionProfesorHallazgoTareaResponsable
                .FirstOrDefault(x => x.IdReunionProfesorHallazgoTarea == hallazgoTareaId && x.IdDocenteResponsable == docenteId);

            if (responsable == null)
            {
                return;
            }

            AbetEntities.ReunionProfesorHallazgoTareaResponsable.Remove(responsable);
            AbetEntities.SaveChanges();
        }


        public void DeleteGuestTeacherMeetingFindingTaskResponsableByTaskAndTeacher(int hallazgoTareaId, int docenteId)
        {
            var responsable = AbetEntities.ReunionProfesorInvitadoHallazgoTareaResponsable
                .FirstOrDefault(x => x.IdReunionProfesorHallazgoTarea == hallazgoTareaId && x.IdReunionProfesorParticipanteInvitado == docenteId);

            if (responsable == null)
            {
                return;
            }

            AbetEntities.ReunionProfesorInvitadoHallazgoTareaResponsable.Remove(responsable);
            AbetEntities.SaveChanges();
        }



        public void DeleteTeacherMeetingTaskByTaskId(int tareaId)
        {
            var taskHistorico = AbetEntities.ReunionProfesorTareaHistorico
                .Where(x => x.IdReunionProfesorTarea == tareaId)
                .ToList();

            if (taskHistorico.Count != 0)
            {
                AbetEntities.ReunionProfesorTareaHistorico
                    .RemoveRange(taskHistorico);
                AbetEntities.SaveChanges();
            }

            var hallazgoTareas = AbetEntities.ReunionProfesorHallazgoTarea
                .Where(x => x.IdReunionProfesorTarea == tareaId);
            if (hallazgoTareas.Count() > 0)
            {
                var hallazgoTareaResponsables = AbetEntities.ReunionProfesorHallazgoTareaResponsable.Where(x => x.ReunionProfesorHallazgoTarea.IdReunionProfesorTarea == tareaId);
                if (hallazgoTareaResponsables.Count() > 0)
                    AbetEntities.ReunionProfesorHallazgoTareaResponsable.RemoveRange(hallazgoTareaResponsables);
                AbetEntities.ReunionProfesorHallazgoTarea.RemoveRange(hallazgoTareas);
                AbetEntities.SaveChanges();
            }

            var task = AbetEntities.ReunionProfesorTarea
                .FirstOrDefault(x => x.IdReunionProfesorTarea == tareaId);

            if (task == null)
            {
                return;
            }

            AbetEntities.ReunionProfesorTarea.Remove(task);
            AbetEntities.SaveChanges();
        }

        public void UpdateTeacherMeetingAgreement(int teacherMeetingAgreementId, string newDescripcionEspanol, string newDescripcionIngles, string semanaDeadline, string fechaDeadline)
        {
            var now = DateTime.Now;
            var formatDate = now.ToString("yyyy-MM-dd HH:mm:ss");
            var agreement = AbetEntities.ReunionProfesorAcuerdo
                .First(x => x.IdReunionProfesorAcuerdo == teacherMeetingAgreementId);

            agreement.DescripcionEspanol = newDescripcionEspanol;
            agreement.DescripcionIngles = newDescripcionIngles;
            agreement.SemanaDeadline = int.Parse(semanaDeadline);
            agreement.FechaDeadline = fechaDeadline.ToDateTime();
            agreement.FechaActualizacion = formatDate.ToDateTime();

            var tasks = agreement.ReunionProfesorTarea;
            foreach (var task in tasks)
            {
                task.SemanaDeadline = int.Parse(semanaDeadline);
                task.FechaDeadline = fechaDeadline.ToDateTime();
                task.FechaActualizacion = formatDate.ToDateTime();
            }

            AbetEntities.SaveChanges();
        }

        public bool UpdateTeacherMeetingTask(int taskId, string taskDescEspanol, string taskDescIngles)
        {
            var now = DateTime.Now;
            var formatDate = now.ToString("yyyy-MM-dd HH:mm:ss");
            var taskDb = AbetEntities.ReunionProfesorTarea
                .FirstOrDefault(x => x.IdReunionProfesorTarea == taskId);

            if (taskDb == null)
            {
                return false;
            }

            taskDb.DescripcionEspanol = taskDescEspanol;
            taskDb.DescripcionIngles = taskDescIngles;
            taskDb.FechaActualizacion = formatDate.ToDateTime();

            AbetEntities.SaveChanges();

            return true;
        }

        public IEnumerable<TeacherMeetingAgreementsPendingResult> GetPendingTeacherMeetingAgreements(int statusId, int weekNumber, int level, int subModalityAcademicPeriodModuleId, int academicUnitId)
        {
            var result = AbetEntities.Database.SqlQuery<TeacherMeetingAgreementsPendingResult>("Usp_ObtenerAcuerdosPendientes {0}, {1}, {2}, {3}, {4}",
                statusId,
                weekNumber,
                level,
                subModalityAcademicPeriodModuleId,
                academicUnitId
                ).AsEnumerable();
            return result;
        }

        public class TeacherMeetingAgreementsPendingResult
        {
            public int IdReunionProfesorAcuerdoHistorico { get; set; }
            public int IdReunionProfesorAcuerdo { get; set; }
            public string ComentarioCierreEspanol { get; set; }
            public string ComentarioCierreIngles { get; set; }
            public DateTime? FechaCierre { get; set; }
            public int IdEstado { get; set; }
        }
    }
}
    