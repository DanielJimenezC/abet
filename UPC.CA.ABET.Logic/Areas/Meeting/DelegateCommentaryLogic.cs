﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Meeting
{
    public class DelegateCommentaryLogic
    {
        private readonly AbetEntities _context;

        public DelegateCommentaryLogic()
        {
            this._context = new AbetEntities();
        }

        public IEnumerable<DelegateCommentary> GetDelegateCommentary(int? IdSubModalidadPeriodoAcademico,int? IdCarreraPeriodoAcademico, int? IdCarrera)
        {

            return _context.Database.SqlQuery<DelegateCommentary>("uspGetDelegateComentary {0}, {1} ,{2}", IdSubModalidadPeriodoAcademico.Value, IdCarreraPeriodoAcademico.Value,   IdCarrera.Value).AsEnumerable();
        }
    }
}
