﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.IO;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using System.Globalization;
using static UPC.CA.ABET.Helpers.ConstantHelpers.TEACHER_MEETING.ESTADO;

namespace UPC.CA.ABET.Logic.Areas.Meeting
{
    public class ExportTeacherMeetingLogic
    {
        private readonly ReunionProfesor ReunionProfesor;
        private readonly string LogoUpc;
        private readonly AbetEntities AbetEntities;
        private readonly string Culture;
        public string Area;
        Color Color_A_normal = new Color(21, 101, 192);

        public ExportTeacherMeetingLogic(AbetEntities AbetEntities, int idReunionProfesor, string pathLogoUpc, string culture)
        {
            LogoUpc = pathLogoUpc;
            ReunionProfesor = AbetEntities.ReunionProfesor.Where(x => x.IdReunionProfesor == idReunionProfesor).FirstOrDefault();
            Culture = culture;
            this.AbetEntities = AbetEntities;
        }

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if (ReunionProfesor == null)
            {
                return String.Empty;
            }

            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                default: return String.Empty;
            }
        }

        private string GetPdf(string directory)
        {
            Document document = CreateDocument();

            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            var area = (Culture == ConstantHelpers.CULTURE.INGLES ? ReunionProfesor.AreaEncargadaIngles ?? ReunionProfesor.AreaEncargada : ReunionProfesor.AreaEncargada);
            Area = area;
            var semana = ReunionProfesor.NumSemana;
            var periodoAcademico = ReunionProfesor.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            //   var subModalidad = ReunionProfesor.UnidadAcademica.SubModalidadPeriodoAcademico.SubModalidad;
            var modalidad = (
                Culture == ConstantHelpers.CULTURE.INGLES ? ReunionProfesor.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreIngles ?? ReunionProfesor.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreEspanol
                                                          : ReunionProfesor.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreEspanol);

            var modulo = (
                Culture == ConstantHelpers.CULTURE.INGLES ? ReunionProfesor.SubModalidadPeriodoAcademicoModulo.Modulo.NombreIngles ?? ReunionProfesor.SubModalidadPeriodoAcademicoModulo.Modulo.NombreEspanol
                                                          : ReunionProfesor.SubModalidadPeriodoAcademicoModulo.Modulo.NombreEspanol);
                       
            string filename;


            filename = "ActaDeReunionProfesor_" + "Semana" + semana.ToString() + "_" + area.Replace(" ", "") + "_" + modalidad.Replace(" ", "") + "_" + modulo.Replace(" ", "") +
           "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + ".pdf";

            filename = filename.DeleteSlashAndBackslash();
            string path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);

            return path;
        }

        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);
            SetContentSection(document);
            SetDetail(document);
            SetAttendance(document);
                document.LastSection.AddPageBreak();
            SetTareasPrevias(document);
            if (ReunionProfesor.Nivel == 2)
            {
                SetReporteOperativo(document);
            }
            SetHallazgos(document);
            SetAcuerdos(document);

            return document;
        }

        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }

        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }//


        private void SetDetail(Document document)
        {
            Section section = document.LastSection;

            Image image = section.AddImage(LogoUpc);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");

            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.PageBreakBefore = false;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph(ReunionProfesor.Nivel == 1 ? "AREA " + ReunionProfesor.AreaEncargada : "SUBAREA " + ReunionProfesor.AreaEncargada, "Heading1");
            }
            else
            {
                paragraph = section.AddParagraph(ReunionProfesor.Nivel == 1 ? "ÁREA DE " + ReunionProfesor.AreaEncargada : "SUBÁREA DE " + ReunionProfesor.AreaEncargada, "Heading1");
            }          
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.PageBreakBefore = false;


            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.PageBreakBefore = false;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("TEACHER MEETING MINUTE", "Heading2");
            }
            else
            {
                paragraph = section.AddParagraph("ACTA DE REUNIÓN DE PROFESORES", "Heading2");
            }
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Color_A_normal;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Font.Color = Color_A_normal;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("ACADEMIC PERIOD:\t\t\t", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("CICLO:\t\t\t\t\t", "Heading3");
            }
            
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var periodoAcademico = ReunionProfesor.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var cicloString = periodoAcademico.CicloAcademico.Insert(4, "-");
            paragraph.AddFormattedText(cicloString, "Heading3").Font.Color = Colors.Black;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("MODALITY / MODULE:\t\t", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("MODALIDAD / MÓDULO:\t\t", "Heading3");
            }

            
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var periodoAcademicoModuloString = ReunionProfesor.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.SubModalidad.Modalidad.NombreEspanol + " / " +
                                         ReunionProfesor.SubModalidadPeriodoAcademicoModulo.Modulo.NombreEspanol;            
            paragraph.AddFormattedText(periodoAcademicoModuloString, "Heading3").Font.Color = Colors.Black;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("WEEK:\t\t\t\t\t", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("SEMANA:\t\t\t\t", "Heading3");
            }

            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            paragraph.AddFormattedText(ReunionProfesor.NumSemana.ToString(), "Heading3").Font.Color = Colors.Black;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("COORDINATOR IN CHARGE:\t\t", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("COORDINADOR A CARGO:\t\t", "Heading3");
            }
            
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var coordinador = (from rp in AbetEntities.ReunionProfesor
                               join rpp in AbetEntities.ReunionProfesorParticipante on rp.IdReunionProfesor equals rpp.IdReunionProfesor
                               join ua in AbetEntities.UnidadAcademica on rp.IdUnidadAcademica equals ua.IdUnidadAcademica
                               join sua in AbetEntities.SedeUnidadAcademica on ua.IdUnidadAcademica equals sua.IdUnidadAcademica
                               join uar in AbetEntities.UnidadAcademicaResponsable on sua.IdSedeUnidadAcademica equals uar.IdSedeUnidadAcademica
                               join d in AbetEntities.Docente on uar.IdDocente equals d.IdDocente
                               where ua.IdUnidadAcademica == ReunionProfesor.IdUnidadAcademica
                               select d).Distinct().FirstOrDefault();
            paragraph.AddFormattedText(coordinador.Apellidos + ", " + coordinador.Nombres, "Heading3").Font.Color = Colors.Black;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("MEETING DATE:\t\t\t", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("FECHA DE REUNIÓN:\t\t\t", "Heading3");
            }

            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.KeepWithNext = true;
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph.AddFormattedText(ReunionProfesor.Fin.Value.ToString("MM/dd/yyyy"), "Heading3").Font.Color = Colors.Black;
            }
            else
            {
                paragraph.AddFormattedText(ReunionProfesor.Fin.Value.ToString("dd/MM/yyyy"), "Heading3").Font.Color = Colors.Black;
            }
            

        }

        public void SetAttendance(Document document)
        {
            TextInfo textinfo = new CultureInfo("es-US", false).TextInfo;

            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("", "Heading2");
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph.AddText("1. MEETING PARTICIPANTS");
            }
            else
            {
                paragraph.AddText("1. PARTICIPANTES DE LA REUNIÓN");
            }
            paragraph.Format.Font.Color = Color_A_normal;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Color_A_normal;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var asistentes = AbetEntities.ReunionProfesorParticipante.Where(x => x.IdReunionProfesor == ReunionProfesor.IdReunionProfesor && x.Asistio == true).ToList();
            var ausentes = AbetEntities.ReunionProfesorParticipante.Where(x => x.IdReunionProfesor == ReunionProfesor.IdReunionProfesor && x.Asistio == false).ToList();

            Style style = document.AddStyle("BulletList", "Normal");
            style.ParagraphFormat.LeftIndent = "0.3cm";
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            var idx = 0;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("Attendees:", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("Asistentes:", "Heading3");
            }
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;

            foreach (var item in asistentes)
            {
                String texto;
                texto = item.Docente.Apellidos.ToLower() + ", " + item.Docente.Nombres.ToLower();
                texto = textinfo.ToTitleCase(texto);
                ListInfo listinfo = new ListInfo();
                listinfo.ContinuePreviousList = idx > 0;
                listinfo.ListType = ListType.BulletList1;
                paragraph = section.AddParagraph(texto);
                paragraph.Style = "BulletList";
                paragraph.Format.ListInfo = listinfo;
                paragraph.Format.SpaceBefore = "1mm";
                idx++;
            }
            if (ausentes.Count() > 0)
            {
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph = section.AddParagraph("Absentees:", "Heading3");
                }
                else
                {
                    paragraph = section.AddParagraph("Ausentes:", "Heading3");
                }
                paragraph.Format.Alignment = ParagraphAlignment.Left;
                paragraph.Format.LeftIndent = 30;
                paragraph.Format.KeepWithNext = true;

                foreach (var item in ausentes)
                {
                    String texto;
                    texto = item.Docente.Apellidos.ToLower() + ", " + item.Docente.Nombres.ToLower();
                    texto = textinfo.ToTitleCase(texto);
                    ListInfo listinfo = new ListInfo();
                    listinfo.ContinuePreviousList = idx > 0;
                    listinfo.ListType = ListType.BulletList1;
                    paragraph = section.AddParagraph(texto);
                    paragraph.Style = "BulletList";
                    paragraph.Format.ListInfo = listinfo;
                    paragraph.Format.SpaceBefore = "1mm";
                    idx++;
                }
            }
            //document.LastSection.AddPageBreak();
        }

        public void SetTareasPrevias(Document document)
        {
            TeacherMeetingLogic TeacherMeetingLogic = new TeacherMeetingLogic();

            var accionesPlanMejoraAvancesActuales = AbetEntities.ReunionProfesorAccionMejora.Where(x => x.IdReunionProfesor == ReunionProfesor.IdReunionProfesor).ToList();

            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("", "Heading2");
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph.AddText("2. PREVIOUS TASKS");
            }
            else
            {
                paragraph.AddText("2. TAREAS PREVIAS");
            }

            paragraph.Format.Font.Color = Color_A_normal;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Color_A_normal;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = section.AddParagraph("2.1 ADVANCES IN THE IMPROVEMENT PLAN:", "Heading3");
            }
            else
            {
                paragraph = section.AddParagraph("2.1 AVANCES EN EL PLAN DE MEJORA:", "Heading3");
            }
            paragraph.Format.SpaceAfter = "4mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 70;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 180;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 180;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Color_A_normal;
            row.Height = 15;
            row.Format.Font.Bold = true;

            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph = row[0].AddParagraph("CODE");
                paragraph.Format.Font.Color = Colors.White;
                paragraph = row[1].AddParagraph("DESCRIPTION");
                paragraph.Format.Font.Color = Colors.White;
                paragraph = row[2].AddParagraph("ADVANCE");
                paragraph.Format.Font.Color = Colors.White;
            }
            else
            {
                paragraph = row[0].AddParagraph("CÓDIGO");
                paragraph.Format.Font.Color = Colors.White;
                paragraph = row[1].AddParagraph("DESCRIPCIÓN");
                paragraph.Format.Font.Color = Colors.White;
                paragraph = row[2].AddParagraph("AVANCE");
                paragraph.Format.Font.Color = Colors.White;
            }          

            if (accionesPlanMejoraAvancesActuales.Count == 0)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph = row[0].AddParagraph("No advances was made in improvement actions in the meeting.");
                }
                else
                {
                    paragraph = row[0].AddParagraph("No se realizaron avances de acciones de mejora en la reunión.");
                }
                paragraph.Format.Font.Color = Colors.Black;
                paragraph.Format.SpaceBefore = "4mm";
                document.LastSection.AddPageBreak();
            }
            else
            {
                foreach (var item in accionesPlanMejoraAvancesActuales)
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.HeadingFormat = false;
                    row.Shading.Color = Colors.White;
                    row.Format.Font.Bold = false;

                    paragraph = row[0].AddParagraph(item.AccionMejora.Codigo + "-" + item.AccionMejora.Identificador.ToString());                                                                         // RML 001
                    paragraph.Format.Font.Color = Colors.Black;
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        paragraph = row[1].AddParagraph(item.AccionMejora.DescripcionIngles??"");
                    }
                    else
                    {
                        paragraph = row[1].AddParagraph(item.AccionMejora.DescripcionEspanol);
                    }
                    row[1].Format.Alignment = ParagraphAlignment.Justify;
                    paragraph.Format.Font.Color = Colors.Black;
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        paragraph = row[2].AddParagraph(item.ComentarioCierreIngles??"");
                    }
                    else
                    {
                        paragraph = row[2].AddParagraph(item.ComentarioCierreEspanol);
                    }                    
                    row[2].Format.Alignment = ParagraphAlignment.Justify;
                    paragraph.Format.Font.Color = Colors.Black;
                }
                //document.LastSection.AddPageBreak();
            }

            // ACUERDOS PREVIOS
            var ReunionProfesorAcuerdosPrevios = TeacherMeetingLogic.GetPreviousTeacherMeetingAgreementsForHistoricReports(ReunionProfesor.IdReunionProfesor);

            if (ReunionProfesorAcuerdosPrevios.Any())
            {
                getAndPrintAcuerdosPrevios(document, ReunionProfesorAcuerdosPrevios, ReunionProfesor.IdReunionProfesor);
            }

            section.AddParagraph();
            paragraph.Format.SpaceAfter = "4mm";

        }

        public void SetReporteOperativo(Document document)
        {
            var lstReporteOperativo = (from rpro in AbetEntities.ReunionProfesorReporteOperativo
                                       join s in AbetEntities.Sede on rpro.IdSede equals s.IdSede
                                       join rp in AbetEntities.ReunionProfesor on rpro.IdReunionProfesor equals rp.IdReunionProfesor
                                       join c in AbetEntities.Curso on rpro.IdCurso equals c.IdCurso
                                       where rp.IdReunionProfesor == ReunionProfesor.IdReunionProfesor
                                       select new
                                       {
                                           CodigoCurso = c.Codigo,
                                           NombreCursoEsp = c.NombreEspanol,
                                           NombreCursoIng = c.NombreIngles,
                                           CodigoSede = s.Codigo,
                                           NombreSede = s.Nombre,
                                           AvanceSilabo = rpro.AvanceSilabo,
                                           UsoAulaVirtual = rpro.UsoAulaVirtual
                                       }
                                           ).ToList();

            // num 3 (solo para nivel 2)
            if (ReunionProfesor.Nivel == 2)
            {
                Section section = document.LastSection;

                Paragraph paragraph = section.AddParagraph("", "Heading2");
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph.AddText("3. OPERATIONAL REPORT");
                }
                else
                {
                    paragraph.AddText("3. REPORTE OPERATIVO");
                }                
                paragraph.Format.Font.Color = Color_A_normal;
                paragraph.Format.Borders.Bottom.Visible = true;
                paragraph.Format.Borders.Top.Visible = false;
                paragraph.Format.Borders.Left.Visible = false;
                paragraph.Format.Borders.Right.Visible = false;
                paragraph.Format.Borders.Color = Color_A_normal;
                paragraph.Format.SpaceBefore = "5mm";
                paragraph.Format.SpaceAfter = "5mm";
                paragraph.Format.Alignment = ParagraphAlignment.Left;


                Table table = section.AddTable();
                table.Format.PageBreakBefore = false;
                table.Borders.Visible = true;
                table.Rows.Height = 30;
                table.Format.Alignment = ParagraphAlignment.Center;

                Column column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 50;

                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 40;

                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 170;

                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 170;

                Row row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = true;
                row.Shading.Color = Color_A_normal;
                row.Height = 15;
                row.Format.Font.Bold = true;

                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph = row[0].AddParagraph("COURSE");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[1].AddParagraph("CAMPUS");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[2].AddParagraph("SYLLABO ADVANCE");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[3].AddParagraph("USE OF VIRTUAL CLASSROOM");
                    paragraph.Format.Font.Color = Colors.White;
                }
                else
                {
                    paragraph = row[0].AddParagraph("CURSO");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[1].AddParagraph("SEDE");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[2].AddParagraph("AVANCE DEL SÍLABO");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[3].AddParagraph("USO DEL AULA VIRTUAL");
                    paragraph.Format.Font.Color = Colors.White;
                }              

                foreach (var item in lstReporteOperativo)
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.HeadingFormat = false;
                    row.Shading.Color = Colors.White;
                    row.Format.Font.Bold = false;

                    paragraph = row[0].AddParagraph(item.CodigoCurso);
                    paragraph.Format.Font.Color = Colors.Black;
                    row[0].Format.Font.Bold = true;
                    paragraph = row[1].AddParagraph(item.CodigoSede);
                    paragraph.Format.Font.Color = Colors.Black;
                    if (item.AvanceSilabo == null)
                    {
                        paragraph = row[2].AddParagraph("N/A");
                        paragraph.Format.Font.Color = Colors.Black;
                    }
                    else
                    {
                        paragraph = row[2].AddParagraph(item.AvanceSilabo);
                        paragraph.Format.Font.Color = Colors.Black;
                        row[2].Format.Alignment = ParagraphAlignment.Justify;
                    }
                    if (item.UsoAulaVirtual == null)
                    {
                        paragraph = row[3].AddParagraph("N/A");
                        paragraph.Format.Font.Color = Colors.Black;
                    }
                    else
                    {
                        paragraph = row[3].AddParagraph(item.UsoAulaVirtual);
                        paragraph.Format.Font.Color = Colors.Black;
                        row[3].Format.Alignment = ParagraphAlignment.Justify;
                    }
                }
                section.AddParagraph();
                paragraph.Format.SpaceAfter = "4mm";

            }

            document.LastSection.AddPageBreak();
        }

        public void SetHallazgos(Document document)
        {
            // num 3 (para nivel 1) num 4 (para nivel 2)

            var hallazgos = AbetEntities.ReunionProfesorHallazgo.Where(x => x.IdReunionProfesor == ReunionProfesor.IdReunionProfesor).ToList();

            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("", "Heading2");
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph.AddText(ReunionProfesor.Nivel == 1 ? "3. FINDINGS" : "4. FINDINGS");
            }
            else
            {
                paragraph.AddText(ReunionProfesor.Nivel == 1 ? "3. HALLAZGOS" : "4. HALLAZGOS");
            }
            paragraph.Format.Font.Color = Color_A_normal;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Color_A_normal;
            paragraph.Format.SpaceBefore = "1mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            section = document.LastSection;
            paragraph = new Paragraph();

            if (hallazgos.Count == 0)
            {
                // CREA LA TABLA
                Table table = section.AddTable();
                table.Style = "Table";
                table.Borders.Visible = true;

                // ANTES DE AGREGAR LA FILA, SE DEFINE LAS COLUMNAS
                Column column = table.AddColumn("15cm");
                column.Format.Alignment = ParagraphAlignment.Justify;
                

                // CREAR LAS FILAS
                Row row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Height = 15;

                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    row.Cells[0].AddParagraph("FINDINGS");
                }
                else
                {
                    row.Cells[0].AddParagraph("HALLAZGOS");
                }
                row.Cells[0].Format.Font.Bold = true;
                row.Cells[0].Shading.Color = Color_A_normal;
                row.Cells[0].Format.Font.Color = Colors.White;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;                

                row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Justify;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    row.Cells[0].AddParagraph("No findings were added into the meeting.");
                }
                else
                {
                    row.Cells[0].AddParagraph("No se ingresaron hallazgos en la reunión.");
                }                
                row.Cells[0].Format.Font.Color = Colors.Black;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].Format.SpaceBefore = "4mm";
                row.Cells[0].Format.SpaceAfter = "4mm";

            }
            else
            {
                int number_hallazgos = 1;
                foreach (var item in hallazgos)
                {
                    // CREA LA TABLA
                    Table table = section.AddTable();
                    table.Style = "Table";

                    // ANTES DE AGREGAR LA FILA, SE DEFINE LAS COLUMNAS
                    Column column = table.AddColumn("2.5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;
                    column.Format.RightIndent = "0cm";

                    column = table.AddColumn("0.3cm");
                    column.Format.Alignment = ParagraphAlignment.Center;
                    column.Format.LeftIndent = "0cm";
                    column.Format.RightIndent = "0cm";

                    column = table.AddColumn("12.7cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    column = table.AddColumn("0mm");
                    column.Format.Alignment = ParagraphAlignment.Center;
                    column.Borders.Visible = false;

                    // CREAR LAS FILAS
                    Row row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.Height = 15;

                    table.Borders.Visible = false;
                    table.Rows.Table.Format.LeftIndent = "0.3cm";
                    table.Rows.Table.Format.RightIndent = "0.3cm";
                    table.Rows.Table.Format.SpaceBefore = "4mm";
                    table.Rows.Table.Format.SpaceAfter = "1mm";
                    table.Rows.Table.Format.Font.Color = Colors.Black;
                    table.Rows.Table.Shading.Color = Colors.White;
                    table.Rows.Table.Format.Alignment = ParagraphAlignment.Justify;
                    table.Rows.VerticalAlignment = VerticalAlignment.Center;
                    table.Rows.Height = 15;
                    table.Format.Font.Bold = true;
                                        
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("FINDING N° " + number_hallazgos.ToString());
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("HALLAZGO N° " + number_hallazgos.ToString());
                    }
                    row.Cells[0].MergeRight = 2;
                    row.Cells[0].Format.SpaceBefore = "1mm";
                    row.Cells[0].Format.SpaceBefore = "1mm";
                    row.Cells[0].Shading.Color = Color_A_normal;
                    row.Cells[0].Format.Font.Color = Colors.White;
                    row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[0].Borders.Right.Visible = true;
                    row.Cells[0].Borders.Top.Visible = true;
                    row.Cells[3].MergeDown = 7;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Area");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Área");
                    }                    
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[2].AddParagraph(item.IdAreaUnidadAcademica != null ? (item.UnidadAcademica.NombreIngles??"") : "For All");
                    }
                    else
                    {
                        row.Cells[2].AddParagraph(item.IdAreaUnidadAcademica != null ? item.UnidadAcademica.NombreEspanol : "Para Todas");
                    }
                    row.Cells[2].Format.Font.Bold = false;
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Subarea");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Subárea");
                    }
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[2].AddParagraph(item.IdSubAreaUnidadAcademica != null ? (item.UnidadAcademica1.NombreIngles??"") : "For All");
                    }
                    else
                    {
                        row.Cells[2].AddParagraph(item.IdSubAreaUnidadAcademica != null ? item.UnidadAcademica1.NombreEspanol : "Para Todas");
                    }                    
                    row.Cells[2].Format.Font.Bold = false;
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Course");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Curso");
                    }
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[2].AddParagraph(item.IdCursoPeriodoAcademico != null ? (item.CursoPeriodoAcademico.Curso.NombreIngles??"") : "For All");
                    }
                    else
                    {
                        row.Cells[2].AddParagraph(item.IdCursoPeriodoAcademico != null ? item.CursoPeriodoAcademico.Curso.NombreEspanol : "Para Todos");
                    }                    
                    row.Cells[2].Format.Font.Bold = false;
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Campus");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Sede");
                    }
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[2].AddParagraph(item.IdSede != null ? item.Sede.Nombre : "For All");
                    }
                    else
                    {
                        row.Cells[2].AddParagraph(item.IdSede != null ? item.Sede.Nombre : "Para Todas");
                    }
                    row.Cells[2].Format.Font.Bold = false;
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Section");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Sección");
                    }
                    
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[2].AddParagraph(item.IdSeccion != null ? item.Seccion.Codigo : "For All");
                    }
                    else
                    {
                        row.Cells[2].AddParagraph(item.IdSeccion != null ? item.Seccion.Codigo : "Para Todas");
                    }                    
                    row.Cells[2].Format.Font.Bold = false;
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph("Description");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("Descripción");
                    }
                    
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[1].AddParagraph(":");
                    row.Cells[2].Borders.Right.Visible = true;

                    row = table.AddRow();
                    
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph(item.DescripcionIngles??"");
                    }
                    else
                    {
                        row.Cells[0].AddParagraph(item.DescripcionEspanol);
                    }
                                        
                    row.Cells[0].Format.Font.Bold = false;
                    row.Cells[0].MergeRight = 2;
                    row.Cells[0].Format.SpaceAfter = "4mm";
                    row.Cells[0].Format.RightIndent = "4mm";
                    row.Cells[0].Borders.Left.Visible = true;
                    row.Cells[0].Borders.Right.Visible = true;
                    row.Cells[0].Borders.Bottom.Visible = true;
                    row.Cells[2].Borders.Right.Visible = true;

                    number_hallazgos++;

                    paragraph = section.AddParagraph();
                    paragraph.Format.SpaceAfter = "4mm";
                }
            }
            document.LastSection.AddPageBreak();
        }

        public void SetAcuerdos(Document document)
        {

            // num 4 (para nivel 1) num 5 (para nivel 2)
            Section section = document.LastSection;

            Paragraph paragraph = section.AddParagraph("", "Heading2");
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                paragraph.AddText(ReunionProfesor.Nivel == 1 ? "4. AGREEMENTS" : "5. AGREEMENTS");
            }
            else
            {
                paragraph.AddText(ReunionProfesor.Nivel == 1 ? "4. ACUERDOS" : "5. ACUERDOS");
            }
            paragraph.Format.Font.Color = Color_A_normal;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Color_A_normal;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            getAndPrintAcuerdos(document, ReunionProfesor, false);

            //  FOOTER FECHA DE CIERRE DEL ACTA
            string dia = ReunionProfesor.Fin.Value.Day.ToString();
            string anio = ReunionProfesor.Fin.Value.Year.ToString();
            string mes;
            string fecha = ReunionProfesor.Fin.Value.ToShortDateString(); ;
            
            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
            {
                mes = ReunionProfesor.Fin.Value.ToString("MMMM", CultureInfo.CreateSpecificCulture("en"));
                fecha = "Lima  " + mes + " " + dia + ", " + anio + ".";
            }
            else
            {
                mes = ReunionProfesor.Fin.Value.ToString("MMMM", CultureInfo.CreateSpecificCulture("es"));
                fecha = "Lima, " + dia + " de " + mes + " del " + anio + ".";                
            }
            //section.Footers.Primary.AddParagraph(fecha);
            TextFrame frame = section.AddTextFrame();
            frame.Height = "2.5cm";
            frame.Width = "15.0cm";
            frame.RelativeHorizontal = RelativeHorizontal.Margin;
            frame.Left = ShapePosition.Left;
            frame.RelativeVertical = RelativeVertical.Page;
            frame.Top = ShapePosition.Bottom;
            
            paragraph = frame.AddParagraph(fecha);
            paragraph.Format.Alignment = ParagraphAlignment.Right;
        }

        private void getAndPrintAcuerdos(Document document, ReunionProfesor _rp, bool esAcuerdosPrevios)
        {

            // num 4 (para nivel 1) num 5 (para nivel 2)


            var lstAcuerdos = AbetEntities.ReunionProfesorAcuerdo.Where(x => x.IdReunionProfesor == _rp.IdReunionProfesor).ToList();

            var lstTareas = (from rpt in AbetEntities.ReunionProfesorTarea
                             join rpa in AbetEntities.ReunionProfesorAcuerdo on rpt.IdReunionProfesorAcuerdo equals rpa.IdReunionProfesorAcuerdo
                             where rpa.IdReunionProfesor == _rp.IdReunionProfesor
                             select rpt).ToList();

            var lstHallazgos = AbetEntities.ReunionProfesorHallazgo.Where(x => x.IdReunionProfesor == _rp.IdReunionProfesor).ToList();

            var lstResponsables = (from rpt in AbetEntities.ReunionProfesorTarea
                                   join rpa in AbetEntities.ReunionProfesorAcuerdo on rpt.IdReunionProfesorAcuerdo equals rpa.IdReunionProfesorAcuerdo
                                   join rpht in AbetEntities.ReunionProfesorHallazgoTarea on rpt.IdReunionProfesorTarea equals rpht.IdReunionProfesorTarea
                                   join rphtr in AbetEntities.ReunionProfesorHallazgoTareaResponsable on rpht.IdReunionProfesorHallazgoTarea equals rphtr.IdReunionProfesorHallazgoTarea
                                   where rpa.IdReunionProfesor == _rp.IdReunionProfesor
                                   select rphtr).ToList();

            int idx = 0;

            var lstSelectedTechersId = new List<int>();


            Section section = document.LastSection;
            Paragraph paragraph = new Paragraph();

            if (lstAcuerdos.Count == 0)
            {
                Table table = section.AddTable();
                table.Format.PageBreakBefore = false;
                table.Borders.Visible = true;
                table.Rows.Height = 30;
                // table.Format.Alignment = ParagraphAlignment.Center;
                table.Format.Alignment = ParagraphAlignment.Center;

                Column column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 430;

                Row row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = true;
                row.Shading.Color = Color_A_normal;
                row.Height = 15;
                row.Format.Font.Bold = true;

                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph = row[0].AddParagraph("AGREEMENTS");
                }
                else
                {
                    paragraph = row[0].AddParagraph("ACUERDOS");
                }                
                paragraph.Format.Font.Color = Colors.White;
                paragraph.Format.Alignment = ParagraphAlignment.Center;


                column.Format.Alignment = ParagraphAlignment.Center;
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph = row[0].AddParagraph("No agreements were added in the meeting.");
                }
                else
                {
                    paragraph = row[0].AddParagraph("No se ingresaron acuerdos en la reunión.");
                }                
                paragraph.Format.Font.Color = Colors.Black;
                paragraph.Format.Alignment = ParagraphAlignment.Center;
                paragraph.Format.SpaceBefore = "2mm";
                paragraph.Format.SpaceAfter = "2mm";
            }
            else
            {
                foreach (var acuerdo in lstAcuerdos)
                {
                    // CREA LA TABLA
                    Table table = section.AddTable();
                    table.Style = "Table";
                    table.Borders.Visible = true;

                    // ANTES DE AGREGAR LA FILA, SE DEFINE LAS COLUMNAS
                    Column column = table.AddColumn("5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    column = table.AddColumn("6.5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    column = table.AddColumn("4cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    // CREAR LAS FILAS
                    Row row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;

                    //  Descripcion
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph(acuerdo.DescripcionEspanol != null ? (acuerdo.DescripcionIngles??"") : "AGREEMENT ");
                        row.Cells[0].AddParagraph();
                        row.Cells[0].AddParagraph("Deadline Week: " + acuerdo.SemanaDeadline.ToString());
                        row.Cells[0].AddParagraph("Deadline Date: " + acuerdo.FechaDeadline.Value.Date.ToShortDateString());
                    }
                    else
                    {
                        row.Cells[0].AddParagraph(acuerdo.DescripcionEspanol != null ? acuerdo.DescripcionEspanol : "ACUERDO ");
                        row.Cells[0].AddParagraph();
                        row.Cells[0].AddParagraph("Semana Límite: " + acuerdo.SemanaDeadline.ToString());
                        row.Cells[0].AddParagraph("Fecha Límite: " + acuerdo.FechaDeadline.Value.Date.ToShortDateString());
                    }
                    row.Cells[0].Format.Font.Bold = true;
                    row.Cells[0].Shading.Color = Color_A_normal;
                    row.Cells[0].Format.Font.Color = Colors.White;
                    row.Cells[0].Format.Alignment = ParagraphAlignment.Justify;
                    row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].MergeDown = 3;
                    row.Cells[0].Format.LeftIndent = "0.2cm";
                    row.Cells[0].Format.RightIndent = "0.2cm";
                    row.Cells[0].Format.SpaceAfter = "2mm";
                    row.Cells[0].Format.SpaceBefore = "2mm";

                    //  Hallazgos
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph("FINDING(S)");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph("HALLAZGO(S)");
                    }                    
                    row.Cells[1].Format.Font.Bold = true;
                    row.Cells[1].Shading.Color = Color_A_normal;
                    row.Cells[1].Format.Font.Color = Colors.White;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].MergeRight = 1;
                    row.Cells[1].Row.Height = 15;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Justify;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    row.Cells[1].MergeRight = 1;

                    foreach (var hallazgo in lstHallazgos)
                    {
                        if (hallazgo.ReunionProfesorHallazgoTarea.Where(x => x.ReunionProfesorTarea.IdReunionProfesorAcuerdo == acuerdo.IdReunionProfesorAcuerdo).FirstOrDefault() != null)
                        {
                            ListInfo listinfo = new ListInfo();
                            listinfo.ContinuePreviousList = idx > 0;
                            listinfo.ListType = ListType.BulletList1;
                            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                            {
                                row.Cells[1].AddParagraph(hallazgo.DescripcionIngles??"");
                            }
                            else
                            {
                                row.Cells[1].AddParagraph(hallazgo.DescripcionEspanol);
                            }                            
                            row.Cells[1].Format.Font.Color = Colors.Black;
                            row.Cells[1].Format.LeftIndent = "0.5cm";
                            row.Cells[1].Format.FirstLineIndent = "-0.5cm";
                            row.Cells[1].Format.RightIndent = "0.5cm";
                            row.Cells[1].Format.ListInfo.ListType = ListType.BulletList1;
                            row.Cells[1].Style = "BulletList";
                            row.Cells[1].Format.SpaceAfter = "2mm";
                            row.Cells[1].Format.SpaceBefore = "2mm";
                            idx++;
                        }
                    }

                    idx = 0;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;

                    //  Tareas
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph("TASK(S)");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph("TAREA(S)");
                    }                    
                    row.Cells[1].Format.Font.Bold = true;
                    row.Cells[1].Shading.Color = Color_A_normal;
                    row.Cells[1].Format.Font.Color = Colors.White;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].Row.Height = 15;

                    //  Responsables

                    row.Cells[2].AddParagraph("RESPONSABLE(S)");
                    
                    row.Cells[2].Format.Font.Bold = true;
                    row.Cells[2].Shading.Color = Color_A_normal;
                    row.Cells[2].Format.Font.Color = Colors.White;
                    row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[2].Row.Height = 15;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Justify;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    //  ENLISTAR TAREAS
                    foreach (var tarea in lstTareas)
                    {
                        if (tarea.ReunionProfesorAcuerdo.IdReunionProfesorAcuerdo == acuerdo.IdReunionProfesorAcuerdo)
                        {
                            ListInfo listinfo = new ListInfo();
                            listinfo.ContinuePreviousList = idx > 0;
                            listinfo.ListType = ListType.BulletList1;
                            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                            {
                                row.Cells[1].AddParagraph(tarea.DescripcionIngles??"");
                            }
                            else
                            {
                                row.Cells[1].AddParagraph(tarea.DescripcionEspanol);
                            }                            
                            row.Cells[1].Format.Font.Color = Colors.Black;
                            row.Cells[1].Format.LeftIndent = "0.5cm";
                            row.Cells[1].Format.FirstLineIndent = "-0.5cm";
                            row.Cells[1].Format.RightIndent = "0.5cm";
                            row.Cells[1].Format.ListInfo.ListType = ListType.BulletList1;
                            row.Cells[1].Style = "BulletList";
                            row.Cells[1].Format.SpaceAfter = "2mm";
                            row.Cells[1].Format.SpaceBefore = "2mm";
                            idx++;
                        }
                    }
                  
                    idx = 0;

                    //  ENLISTAR RESPONSABLES
                    foreach (var responsable in lstResponsables)
                    {
                        if (responsable.ReunionProfesorHallazgoTarea.ReunionProfesorTarea.IdReunionProfesorAcuerdo == acuerdo.IdReunionProfesorAcuerdo
                            && !(lstSelectedTechersId.Contains(responsable.IdDocenteResponsable)))
                        {
                            ListInfo listinfo = new ListInfo();
                            listinfo.ContinuePreviousList = idx > 0;
                            listinfo.ListType = ListType.BulletList1;

                            string _responsable = responsable.Docente.Apellidos.ToLower() + ", " + responsable.Docente.Nombres.ToLower();
                            TextInfo textInfo = new CultureInfo("es-US", false).TextInfo;
                            _responsable = textInfo.ToTitleCase(_responsable);

                            row.Cells[2].AddParagraph(_responsable);
                            row.Cells[2].Format.Font.Color = Colors.Black;
                            row.Cells[2].Format.LeftIndent = "0.5cm";
                            row.Cells[2].Format.FirstLineIndent = "-0.5cm";
                            row.Cells[2].Format.ListInfo.ListType = ListType.BulletList1;
                            row.Cells[2].Style = "BulletList";
                            row.Cells[2].Format.SpaceAfter = "2mm";
                            row.Cells[2].Format.SpaceBefore = "2mm";
                            idx++;

                            lstSelectedTechersId.Add(responsable.IdDocenteResponsable);
                        }
                    }

                    lstSelectedTechersId.Clear();

                    idx = 0;

                    paragraph = section.AddParagraph();
                    paragraph.Format.SpaceAfter = "4mm";
                }
            }
        }

        public void getAndPrintAcuerdosPrevios(Document document, IEnumerable<ReunionProfesorAcuerdoHistorico> ReunionProfesorAcuerdosHistoricosPrevios, int idUltimaReunionProfesor)
        {
            var entities = new AbetEntities();
            int idx = 0;

            if (ReunionProfesorAcuerdosHistoricosPrevios.Count() > 0)
            {
                //  DISEÑO DE TABLA ACUERDOS PREVIOS
                Section section = document.LastSection;
                Paragraph paragraph = new Paragraph();
                
                paragraph = section.AddParagraph("", "Heading3");
                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                {
                    paragraph.AddText("2.2 PREVIOUS AGREEMENTS:");
                }
                else
                {
                    paragraph.AddText("2.2 ACUERDOS PREVIOS:");
                }
                paragraph.Format.Alignment = ParagraphAlignment.Left;
                paragraph.Format.LeftIndent = 30;
                paragraph.Format.KeepWithNext = true;
                paragraph.Format.SpaceBefore = "4mm";
                paragraph.Format.SpaceAfter = "4mm";


                for (int i = 0; i < ReunionProfesorAcuerdosHistoricosPrevios.Count(); i++)
                {
                    var acuerdoPrevioHistorico = ReunionProfesorAcuerdosHistoricosPrevios.ToList()[i];

                    var comentarioCierreEstaReunion = acuerdoPrevioHistorico.ComentarioCierreEspanol; // ToList()[0].ComentarioCierreEspanol;
                    var tareas = acuerdoPrevioHistorico.ReunionProfesor.ReunionProfesorTareaHistorico.Where(x => x.IdUltimaReunionProfesor == idUltimaReunionProfesor && x.IdReunionProfesorAcuerdo == acuerdoPrevioHistorico.IdReunionProfesorAcuerdo).ToList();
                    var hallazgoList = new List<ReunionProfesorHallazgo>();
                    var docentes = new List<Docente>();
                    var hallazgosTareasList = new List<ReunionProfesorHallazgoTarea>();
                    var reunionProfesorHallazgoTareaResponsableList = new List<ReunionProfesorHallazgoTareaResponsable>();
                    foreach (var tarea in tareas)
                    {
                        var hallazgosTareas = entities.ReunionProfesorHallazgoTarea.Where(x => x.IdReunionProfesorTarea == tarea.IdReunionProfesorTarea).ToList();
                        hallazgosTareasList.AddRange(hallazgosTareas);
                    }
                    foreach (var hallazgoTarea in hallazgosTareasList)
                    {
                        hallazgoList.Add(hallazgoTarea.ReunionProfesorHallazgo);
                        reunionProfesorHallazgoTareaResponsableList.AddRange(hallazgoTarea.ReunionProfesorHallazgoTareaResponsable);
                    }
                    hallazgoList = hallazgoList.Distinct().ToList();
                    reunionProfesorHallazgoTareaResponsableList = reunionProfesorHallazgoTareaResponsableList.Distinct().ToList();
                    docentes = reunionProfesorHallazgoTareaResponsableList.Select(x => x.Docente).Distinct().ToList();

                    // CREA LA TABLA
                    Table table = section.AddTable();
                    table.Style = "Table";
                    table.Borders.Visible = true;

                    // ANTES DE AGREGAR LA FILA, SE DEFINE LAS COLUMNAS
                    Column column = table.AddColumn("5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    column = table.AddColumn("5.5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    column = table.AddColumn("5.5cm");
                    column.Format.Alignment = ParagraphAlignment.Justify;

                    // CREAR LAS FILAS
                    Row row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;

                    //  Descripcion
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[0].AddParagraph(acuerdoPrevioHistorico.ReunionProfesorAcuerdo.DescripcionIngles ?? "AGREEMENT ");
                        row.Cells[0].AddParagraph();
                        row.Cells[0].AddParagraph("Deadline Week: " + acuerdoPrevioHistorico.ReunionProfesorAcuerdo.SemanaDeadline.ToString());
                        row.Cells[0].AddParagraph("Deadline Date: " + acuerdoPrevioHistorico.ReunionProfesorAcuerdo.FechaDeadline.Value.Date.ToShortDateString());
                    }
                    else
                    {
                        row.Cells[0].AddParagraph(acuerdoPrevioHistorico.ReunionProfesorAcuerdo.DescripcionEspanol ?? "ACUERDO ");
                        row.Cells[0].AddParagraph();
                        row.Cells[0].AddParagraph("Semana Límite: " + acuerdoPrevioHistorico.ReunionProfesorAcuerdo.SemanaDeadline.ToString());
                        row.Cells[0].AddParagraph("Fecha Límite: " + acuerdoPrevioHistorico.ReunionProfesorAcuerdo.FechaDeadline.Value.Date.ToShortDateString());
                    }                    
                    row.Cells[0].Format.Font.Bold = true;
                    row.Cells[0].Shading.Color = Color_A_normal;
                    row.Cells[0].Format.Font.Color = Colors.White;
                    row.Cells[0].Format.Alignment = ParagraphAlignment.Justify;
                    row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].MergeDown = 3;
                    row.Cells[0].Format.LeftIndent = "0.2cm";
                    row.Cells[0].Format.RightIndent = "0.2cm";
                    row.Cells[0].Format.SpaceAfter = "4mm";
                    row.Cells[0].Format.SpaceBefore = "4mm";

                    //  Hallazgos
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph("FINDING(S)");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph("HALLAZGO(S)");
                    }                    
                    row.Cells[1].Format.Font.Bold = true;
                    row.Cells[1].Shading.Color = Color_A_normal;
                    row.Cells[1].Format.Font.Color = Colors.White;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].MergeRight = 1;
                    row.Cells[1].Row.Height = 15;

                    row = table.AddRow();
                    row.Cells[1].MergeRight = 1;
                    row.Format.Alignment = ParagraphAlignment.Justify;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    foreach (var hallazgo in hallazgoList)
                    {
                        ListInfo listinfo = new ListInfo();
                        listinfo.ContinuePreviousList = idx > 0;
                        listinfo.ListType = ListType.BulletList1;

                        row.Cells[1].Format.Font.Color = Colors.Black;

                        row.Cells[1].Format.LeftIndent = "0.5cm";
                        row.Cells[1].Format.FirstLineIndent = "-0.5cm";
                        row.Cells[1].Format.RightIndent = "0.5cm";
                        row.Cells[1].Format.ListInfo.ListType = ListType.BulletList1;
                        row.Cells[1].Style = "BulletList";
                        row.Cells[1].Format.SpaceAfter = "2mm";
                        row.Cells[1].Format.SpaceBefore = "2mm";
                        if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                        {
                            row.Cells[1].AddParagraph(hallazgo.DescripcionIngles??"");
                        }
                        else
                        {
                            row.Cells[1].AddParagraph(hallazgo.DescripcionEspanol);
                        }                        
                        idx++;
                    }
                    idx = 0;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;

                    //  Tareas
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph("TASK(S)");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph("TAREA(S)");
                    }                    
                    row.Cells[1].Format.Font.Bold = true;
                    row.Cells[1].Shading.Color = Color_A_normal;
                    row.Cells[1].Format.Font.Color = Colors.White;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].Row.Height = 15;

                    //  Responsables
                    row.Cells[2].AddParagraph("RESPONSABLE(S)");
                    row.Cells[2].Format.Font.Bold = true;
                    row.Cells[2].Shading.Color = Color_A_normal;
                    row.Cells[2].Format.Font.Color = Colors.White;
                    row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[2].Row.Height = 15;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Justify;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    //  ENLISTAR TAREAS

                    foreach (var tareaHistorica in tareas)
                    {
                        ListInfo listinfo = new ListInfo();
                        listinfo.ContinuePreviousList = idx > 0;
                        listinfo.ListType = ListType.BulletList1;

                        row.Cells[1].Format.Font.Color = Colors.Black;
                        row.Cells[1].Format.LeftIndent = "0.5cm";
                        row.Cells[1].Format.FirstLineIndent = "-0.5cm";
                        row.Cells[1].Format.RightIndent = "0.5cm";
                        row.Cells[1].Format.ListInfo.ListType = ListType.BulletList1;
                        row.Cells[1].Style = "BulletList";
                        row.Cells[1].Format.SpaceAfter = "2mm";
                        row.Cells[1].Format.SpaceBefore = "2mm";
                        idx++;

                        Paragraph paragraph_cell = row.Cells[1].AddParagraph("");

                        if (tareaHistorica.IdEstado == (int)TAREA.PENDIENTE)
                        {                         
                            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                            {
                                paragraph_cell.AddText(tareaHistorica.ReunionProfesorTarea.DescripcionIngles??"");
                            }
                            else
                            {
                                paragraph_cell.AddText(tareaHistorica.ReunionProfesorTarea.DescripcionEspanol);
                            }                      
                        }
                        else
                        {
                            if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                            {
                                paragraph_cell.AddText(tareaHistorica.ReunionProfesorTarea.DescripcionIngles ?? "");
                            }
                            else
                            {
                                paragraph_cell.AddText(tareaHistorica.ReunionProfesorTarea.DescripcionEspanol);
                            }
                            if (tareaHistorica.IdEstado == (int)TAREA.COMPLETADA)
                            {
                                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                                {
                                    paragraph_cell.AddText(" (Completed in the week " + tareaHistorica.ReunionProfesorTarea.SemanaDeadline.ToString() + " - " + tareaHistorica.ReunionProfesorTarea.FechaCierre.Value.Date.ToShortDateString() +")");
                                }
                                else
                                {
                                    paragraph_cell.AddText(" (Completada en la semana " + tareaHistorica.ReunionProfesorTarea.SemanaDeadline.ToString() + " - " + tareaHistorica.ReunionProfesorTarea.FechaCierre.Value.Date.ToShortDateString() +")");
                                }
                            }
                            else if (tareaHistorica.IdEstado == (int)TAREA.NO_COMPLETADA)
                            {
                                if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                                {
                                    paragraph_cell.Format.Font.Color = Colors.Red;
                                    paragraph_cell.AddText(" - (Expired task)");
                                }
                                else
                                {
                                    paragraph_cell.Format.Font.Color = Colors.Red;
                                    paragraph_cell.AddText(" - (Tarea vencida)");
                                }
                            }
                        }


                        /*
                        if (tareaHistorica.IdEstado == (int)TAREA.NO_COMPLETADA && tarea.IdReunionProfesorCompletada != acuerdoPrevio.IdReunionProfesor)
                        {
                            //paragraph_cell.AddText(" (No Completado) ");
                            paragraph_cell.Format.Font.Color = Colors.Red;
                            paragraph_cell.AddText(tarea.FechaDeadline.Value.Date.ToShortDateString());
                            //     idx++;
                        }
                        if (tareaHistorica.IdEstado == (int)TAREA.NO_COMPLETADA && tarea.IdReunionProfesorCompletada != acuerdoPrevio.IdReunionProfesor)
                        {
                            //paragraph_cell.AddText(" (No Completado) ");
                            paragraph_cell.Format.Font.Color = Colors.Red;
                            paragraph_cell.AddText(" " + "("+tarea.FechaDeadline.Value.Date.ToShortDateString()+")");
                            //     idx++;
                        }
                        */

                    }
                    idx = 0;

                    //  ENLISTAR RESPONSABLES
                    foreach (var responsable in docentes)
                    {

                        ListInfo listinfo = new ListInfo();
                        listinfo.ContinuePreviousList = idx > 0;
                        listinfo.ListType = ListType.BulletList1;

                        string _responsable = responsable.Apellidos.ToLower() + ", " + responsable.Nombres.ToLower();
                        TextInfo textInfo = new CultureInfo("es-US", false).TextInfo;
                        _responsable = textInfo.ToTitleCase(_responsable);

                        row.Cells[2].AddParagraph(_responsable);
                        row.Cells[2].Format.Font.Color = Colors.Black;
                        row.Cells[2].Format.LeftIndent = "0.5cm";
                        row.Cells[2].Format.FirstLineIndent = "-0.5cm";
                        row.Cells[2].Format.ListInfo.ListType = ListType.BulletList1;
                        row.Cells[2].Style = "BulletList";
                        row.Cells[2].Format.SpaceAfter = "2mm";
                        row.Cells[2].Format.SpaceBefore = "2mm";
                        idx++;
                    }

                    idx = 0;
                    //  MODIFICA CELDA DESCRIPCION DEL ACUERDO
                    table.Rows[0].Cells[0].MergeDown = 5;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Justify;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    //  COMENTARIOS DEL ACUERDO
                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph("COMMENTS OF AGREEMENT");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph("COMENTARIOS DEL ACUERDO");
                    }                    
                    row.Cells[1].Format.Font.Bold = true;
                    row.Cells[1].Shading.Color = Color_A_normal;
                    row.Cells[1].Format.Font.Color = Colors.White;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].MergeRight = 1;
                    row.Cells[1].Row.Height = 15;

                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;

                    if (Culture.Equals(ConstantHelpers.CULTURE.INGLES))
                    {
                        row.Cells[1].AddParagraph(acuerdoPrevioHistorico.ComentarioCierreIngles ?? " N/A ");
                    }
                    else
                    {
                        row.Cells[1].AddParagraph(acuerdoPrevioHistorico.ComentarioCierreEspanol ?? " N/A ");
                    }                    
                    row.Cells[1].Format.Font.Color = Colors.Black;
                    row.Cells[1].Format.Alignment = ParagraphAlignment.Justify;
                    row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[1].MergeRight = 1;
                    row.Cells[1].Format.LeftIndent = "0.5cm";
                    row.Cells[1].Format.RightIndent = "0.5cm";
                    row.Cells[1].Format.SpaceBefore = "2mm";
                    row.Cells[1].Format.SpaceAfter = "2mm";

                    paragraph = section.AddParagraph();
                    paragraph.Format.SpaceAfter = "4mm";

                }
            }
            document.LastSection.AddPageBreak();
        }
    }
}
