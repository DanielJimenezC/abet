﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Meeting
{
    public class DelegateCommentary
    {
        public int uasubareaIdUnidadAcademica { get; set; }
        public string uasubareaEspanol { get; set; }
        public string uasubareaIngles { get; set; }
        //public int UnidadAcademica { get; set; }
        public int uaareaIdUnidadAcademica { get; set; }
        public string uaareaEspanol { get; set; }
        public string uaareaIngles { get; set; }
        public string Comentario { get; set; }
        public string hEspanol { get; set; }
        public string hDescripcionIngles { get; set; }
        public int IdEscuela { get; set; }
        public string carEspanol { get; set; }
        public string carIngles { get; set; }
        public string Codigo { get; set; }
        public string cuEspanol { get; set; }
        public string cuIngles { get; set; }
        public string Nombre { get; set; }
        public string cdEspanol { get; set; }
        public string cdIngles { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
