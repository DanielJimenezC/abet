﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class CurriculumAsociator
    {
        private AbetEntities _context;
        private LogCarga _logCarga;

        public CurriculumAsociator(AbetEntities context)
        {
            _context = context;
            
        }

        public void AsociateCurriculumAndAcademicPeriod(int termId, int curriculumId, int userId)
        {
            //termID tiene el valor de submodalidadperiodoacademico


            //int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x=>x.IdSubModalidadPeriodoAcademico==termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsubmoda = termId;
          
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {


                _context.Configuration.AutoDetectChangesEnabled = false;
                _logCarga = LogCargaBuilder.GetLogCarga(_context, "MallaCiclo", idsubmoda, userId);

                var curriculum = FindCurriculum(curriculumId);
                _context.Entry(curriculum).State = EntityState.Modified;

                _context.MallaCurricularPeriodoAcademico.Add(CreateCurriculumAcademicPeriod(idsubmoda, curriculum));

                if (TermCarreerDoesNotExist(idsubmoda, curriculum.Carrera))
                {
                    _context.CarreraPeriodoAcademico.Add(CreateCarreerTerm(idsubmoda, curriculum.Carrera));
                }

                foreach (var curriculumSubject in curriculum.CursoMallaCurricular)
                {
                    var subjectAcademicPeriod = FindSubjectAcademicPeriod(idsubmoda, curriculumSubject.Curso);

                    if (subjectAcademicPeriod == null)
                    {
                        subjectAcademicPeriod = CreateSubjectAcademicPeriod(idsubmoda, curriculumSubject.Curso);
                    }

                    _context.CarreraCursoPeriodoAcademico.Add(CreateCareerSubjectAcademicPeriod(idsubmoda, curriculum.Carrera, subjectAcademicPeriod));
                }

                _context.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private CursoPeriodoAcademico FindSubjectAcademicPeriod(int idsubmoda, Curso subject)
        {
            return subject.CursoPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idsubmoda);
        }

        private MallaCurricular FindCurriculum(int curriculumId)
        {
            return _context.MallaCurricular.FirstOrDefault(x => x.IdMallaCurricular == curriculumId);
        }

        private CarreraCursoPeriodoAcademico CreateCareerSubjectAcademicPeriod(int idsubmoda, Carrera career, CursoPeriodoAcademico subjectAcademicPeriod)
        {
            return new CarreraCursoPeriodoAcademico
            {
                Carrera = career,
                CursoPeriodoAcademico = subjectAcademicPeriod,
                LogCarga = _logCarga
            };
        }

        private MallaCurricularPeriodoAcademico CreateCurriculumAcademicPeriod(int idsubmoda, MallaCurricular curriculum)
        {
            var mallacurricularPeriodoAcademico = new MallaCurricularPeriodoAcademico
            {
                MallaCurricular = curriculum,
                IdSubModalidadPeriodoAcademico = idsubmoda,
                LogCarga = _logCarga
            };
            _context.Entry(mallacurricularPeriodoAcademico).State = EntityState.Added;
            return mallacurricularPeriodoAcademico;
        }

        private bool TermCarreerDoesNotExist(int idsubmoda, Carrera carrera)
        {
            return _context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idsubmoda && x.IdCarrera == carrera.IdCarrera) == null;
        }

        private CarreraPeriodoAcademico CreateCarreerTerm(int idsubmoda, Carrera career)
        {
            var careerAcademicPeriod = new CarreraPeriodoAcademico
            {
                Carrera = career,
                IdSubModalidadPeriodoAcademico = idsubmoda,
                LogCarga = _logCarga
            };
            return careerAcademicPeriod;
        }

        private CursoPeriodoAcademico CreateSubjectAcademicPeriod(int idsubmoda, Curso subject)
        {
            var cursoPeriodoAcademico = new CursoPeriodoAcademico
            {
                IdSubModalidadPeriodoAcademico = idsubmoda,
                Curso = subject,
                CDATiempo = true,
                FinalATiempo = true,
                FinalCorrecto = true,
                AulaVirtualATiempo = true,
                ParcialATiempo = true,
                ParcialCorrecto = true,
                RecuperacionATiempo = true,
                RecuperacionCorrecto = true,
                SilaboATiempo = true,
                TieneFinal = true,
                TieneParcial = true,
                LogCarga = _logCarga
            };

            return cursoPeriodoAcademico;
        }
    }
}
