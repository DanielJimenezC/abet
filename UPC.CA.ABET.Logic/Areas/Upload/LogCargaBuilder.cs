﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class LogCargaBuilder
    {
        public static LogCarga GetLogCarga(AbetEntities context, string uploadType, int ? idsubmoda, int userId)
        {
            return new LogCarga
            {
                LogTipoCarga = GetLogTipoCarga(context, uploadType),
                IdSubModalidadPeriodoAcademico = idsubmoda,
                FechaCarga = DateTime.Now,
                IdUsuario = userId
            };
        }

        public static LogTipoCarga GetLogTipoCarga(AbetEntities context, string uploadType)
        {
            var logTipoCarga = context.LogTipoCarga.FirstOrDefault(x => x.Nombre == uploadType);
            if (logTipoCarga == null)
            {
                logTipoCarga = new LogTipoCarga { Nombre = uploadType };
            }
            return logTipoCarga;
        }
    }
}
