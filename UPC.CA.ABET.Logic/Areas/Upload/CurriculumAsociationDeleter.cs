﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class CurriculumAsociationDeleter
    {
        private AbetEntities _context;

        public CurriculumAsociationDeleter(AbetEntities context)
        {
            _context = context;
        }
        public void DeleteAsociation( int idCurriculumTerm )
        {
            var curriculumTerm = _context.MallaCurricularPeriodoAcademico
                .FirstOrDefault(x => x.IdMallaCurricularPeriodoAcademico == idCurriculumTerm);

            var subjectTerms = _context.CursoPeriodoAcademico.Where(x => x.IdLogCarga == curriculumTerm.IdLogCarga);
            var careerTerms = _context.CarreraPeriodoAcademico.Where(x => x.IdLogCarga == curriculumTerm.IdLogCarga);
            var careerSubjectTerms = _context.CarreraCursoPeriodoAcademico.Where(x => x.IdLogCarga == curriculumTerm.IdLogCarga);

            try
            {
                _context.CursoPeriodoAcademico.RemoveRange(subjectTerms);
                _context.CarreraPeriodoAcademico.RemoveRange(careerTerms);
                _context.CarreraCursoPeriodoAcademico.RemoveRange(careerSubjectTerms);
                _context.MallaCurricularPeriodoAcademico.Remove(curriculumTerm);

                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
