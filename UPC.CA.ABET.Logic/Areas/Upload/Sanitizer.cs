﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class Sanitizer
    {
        public static void SanitizeObjectStrings(object input)
        {
            foreach (PropertyInfo propertyInfo in input.GetType().GetProperties())
            {
                var propertyValue = propertyInfo.GetValue(input, null);
                if (PropertyIsString(propertyInfo) && propertyValue != null)
                {
                    propertyInfo.SetValue(input, Sanitize(propertyValue.ToString()));
                }
            }
        }

        private static bool PropertyIsString(PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType == typeof(string);
        }

        public static String Sanitize(String input)
        {
            return new String(input
                .Where(c => Char.IsLetterOrDigit(c) || Char.IsWhiteSpace(c) || c == ',' || c == '.')
                .ToArray())
                .Trim()
                .ToUpper();
        }
    }
}
