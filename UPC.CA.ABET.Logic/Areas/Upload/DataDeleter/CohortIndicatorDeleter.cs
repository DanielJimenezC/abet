﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class CohortIndicatorDeleter : UploadDeleter
    {

        private AbetEntities _context;

        public CohortIndicatorDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            RemoveIndicatorsFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void RemoveIndicatorsFromContext(int idLogCarga)
        {
            foreach (var indicator in _context.IndicadorCohorte.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.IndicadorCohorte.Remove(indicator);
            }
        }
    }
}
