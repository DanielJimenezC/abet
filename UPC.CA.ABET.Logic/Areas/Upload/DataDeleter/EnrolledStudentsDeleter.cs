﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class EnrolledStudentsDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public EnrolledStudentsDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteEnrolledStudents = GetEnrolledStudentsFromUpload(idLogCarga);
                var toDeleteStudents = GetStudentsFromUpload(idLogCarga);

                foreach (var enrolledStudent in toDeleteEnrolledStudents)
                {
                    _context.AlumnoMatriculado.Remove(enrolledStudent);
                }

                foreach (var student in toDeleteStudents)
                {
                    _context.Alumno.Remove(student);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AlumnoMatriculado> GetEnrolledStudentsFromUpload(int idLogCarga)
        {
            return _context.AlumnoMatriculado.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<Alumno> GetStudentsFromUpload(int idLogCarga)
        {
            return _context.Alumno.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
