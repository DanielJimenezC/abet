﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class GradesDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public GradesDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            UpdateClassStudentsWithOldInfo(idLogCarga);
            RemoveClassStudentsHistoricalDataFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void UpdateClassStudentsWithOldInfo(int idLogCarga)
        {
            var oldValues = _context.CargaAlumnoSeccionHistorico.Where(x => x.IdLogcarga == idLogCarga);

            foreach (var oldValue in oldValues)
            {
                var classStudentToUpdate = oldValue.AlumnoSeccion;
                classStudentToUpdate.Nota = oldValue.NotaHistorica;
                classStudentToUpdate.EsDelegado = oldValue.EsDelegado.Value;
                _context.Entry(classStudentToUpdate).State = EntityState.Modified;
            }
        }

        private void RemoveClassStudentsHistoricalDataFromContext(int idLogCarga)
        {
            foreach (var data in _context.CargaAlumnoSeccionHistorico.Where(x => x.IdLogcarga == idLogCarga))
            {
                _context.CargaAlumnoSeccionHistorico.Remove(data);
            }
        }
    }
}
