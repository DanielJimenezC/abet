﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class ProfessorsPunctualityDeleter : UploadDeleter
    {

        private AbetEntities _context;

        public ProfessorsPunctualityDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            RemoveProfessorsPunctualityFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void RemoveProfessorsPunctualityFromContext(int idLogCarga)
        {
            foreach (var punctuality in _context.PuntualidadDocente.Where(x => x.LogCarga == idLogCarga))
            {
                _context.PuntualidadDocente.Remove(punctuality);
            }
        }
    }
}
