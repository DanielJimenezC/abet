﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class StudentClassDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public StudentClassDeleter()
        {
            _context = new AbetEntities();
        }
        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteStudentClass = GetStudentClassFromUpload(idLogCarga);
                var toDeleteClasses = GetClassesFromUpload(idLogCarga);
                var toDeleteSubjectAcademicPeriod = GetSubjectAcademicPeriodFromUpload(idLogCarga);

                foreach (var studentClass in toDeleteStudentClass)
                {
                    _context.AlumnoSeccion.Remove(studentClass);
                }

                foreach (var dClass in toDeleteClasses)
                {
                    _context.Seccion.Remove(dClass);
                }

                foreach (var subjectAcademicPeriod in toDeleteSubjectAcademicPeriod)
                {
                    _context.CursoPeriodoAcademico.Remove(subjectAcademicPeriod);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }   
        }

        private List<AlumnoSeccion> GetStudentClassFromUpload(int idLogCarga)
        {
            return _context.AlumnoSeccion.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        private List<Seccion> GetClassesFromUpload(int idLogCarga)
        {
            return _context.Seccion.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        private List<CursoPeriodoAcademico> GetSubjectAcademicPeriodFromUpload(int idLogCarga)
        {
            return _context.CursoPeriodoAcademico.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
