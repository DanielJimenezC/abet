﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class ProfessorsAttendanceDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public ProfessorsAttendanceDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            RemoveProfessorsAttendanceFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void RemoveProfessorsAttendanceFromContext(int idLogCarga)
        {
            foreach (var attendance in _context.AsistenciaDocente.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.AsistenciaDocente.Remove(attendance);
            }
        }
    }
}
