﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class CurriculumDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public CurriculumDeleter()
        {
            _context = new AbetEntities();
        }
        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteSubjects = GetSubjectsFromUpload(idLogCarga);
                var toDeleteSubjectRequirements = GetSubjectRequirementsFromUpload(idLogCarga);
                var toDeleteCurriculumSubjects = GetCurriculumSubjectsFromUpload(idLogCarga);
                var toDeleteCurriculums = GetCurriculumFromUpload(idLogCarga);

                foreach (var subject in toDeleteSubjects)
                {
                    _context.Curso.Remove(subject);
                }

                foreach (var subjectRequirement in toDeleteSubjectRequirements)
                {
                    _context.RequisitoCurso.Remove(subjectRequirement);
                }

                foreach (var curriculumSubject in toDeleteCurriculumSubjects)
                {
                    _context.CursoMallaCurricular.Remove(curriculumSubject);
                }

                foreach (var curriculum in toDeleteCurriculums)
                {
                    _context.MallaCurricular.Remove(curriculum);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Curso> GetSubjectsFromUpload(int idLogCarga)
        {
            return _context.Curso.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<RequisitoCurso> GetSubjectRequirementsFromUpload(int idLogCarga)
        {
            return _context.RequisitoCurso.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<CursoMallaCurricular> GetCurriculumSubjectsFromUpload(int idLogCarga)
        {
            return _context.CursoMallaCurricular.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<MallaCurricular> GetCurriculumFromUpload(int idLogCarga)
        {
            return _context.MallaCurricular.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
