﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class DelegatesDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public DelegatesDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            UpdateClassStudentsWithOldInfo(idLogCarga);
            
            RemoveClassStudentsHistoricalDataFromContext(idLogCarga);
            RemoveClassStudentsFromContext(idLogCarga);
            RemoveClassesFromContext(idLogCarga);
            RemoveEnrolledStudentsFromContext(idLogCarga);
            RemoveStudentsFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void UpdateClassStudentsWithOldInfo(int idLogCarga)
        {
            var oldValues = _context.CargaAlumnoSeccionHistorico.Where(x => x.IdLogcarga == idLogCarga);

            foreach (var oldValue in oldValues)
            {
                var classStudentToUpdate = oldValue.AlumnoSeccion;
                classStudentToUpdate.Nota = oldValue.NotaHistorica;
                classStudentToUpdate.EsDelegado = oldValue.EsDelegado.Value;
                _context.Entry(classStudentToUpdate).State = EntityState.Modified;
            }
        }

        private void RemoveClassStudentsHistoricalDataFromContext(int idLogCarga)
        {
            foreach (var data in _context.CargaAlumnoSeccionHistorico.Where(x => x.IdLogcarga == idLogCarga))
            {
                _context.CargaAlumnoSeccionHistorico.Remove(data);
            }
        }

        private void RemoveClassesFromContext(int idLogCarga)
        {
            foreach (var data in _context.Seccion.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.Seccion.Remove(data);
            }
        }

        private void RemoveStudentsFromContext(int idLogCarga)
        {
            foreach (var data in _context.Alumno.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.Alumno.Remove(data);
            }
        }

        private void RemoveEnrolledStudentsFromContext(int idLogCarga)
        {
            foreach (var data in _context.AlumnoMatriculado.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.AlumnoMatriculado.Remove(data);
            }
        }

        private void RemoveClassStudentsFromContext(int idLogCarga)
        {
            foreach (var data in _context.AlumnoSeccion.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.AlumnoSeccion.Remove(data);
            }
        }
    }
}
