﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class UploadDeleterInteractor
    {
        private UploadRepository uploadRepository;

        public UploadDeleterInteractor()
        {
            uploadRepository = new UploadRepository();
        }

        public void Delete(int idLogCarga)
        {
            var logCarga = uploadRepository.Get(idLogCarga);
            var uploadDeleter = UploadDeleterFactory.Create(logCarga.LogTipoCarga.Nombre);

            uploadDeleter.DeleteAsociatedEntities(idLogCarga);
            uploadRepository.Delete(idLogCarga);
        }
    }
}
