﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public interface UploadDeleter
    {
        void DeleteAsociatedEntities(int idLogCarga);
    }
}
