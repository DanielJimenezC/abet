﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class CertificatedStudentsDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public CertificatedStudentsDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            UpdateStudentsWithOldInfo(idLogCarga);
            RemoveStudentsFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void UpdateStudentsWithOldInfo(int idLogCarga)
        {
            var oldValues = _context.CargaAlumnoHistorico.Where(x => x.IdLogCarga == idLogCarga);

            foreach (var oldValue in oldValues)
            {
                var studentToUpdate = oldValue.Alumno;
                studentToUpdate.IdCarreraEgreso = oldValue.IdCarreraEgreso;
                studentToUpdate.AnioEgreso = oldValue.AnioEgreso;
                studentToUpdate.AnioTitulacion = oldValue.AnioTitulacion;

                _context.Entry(studentToUpdate).State = EntityState.Modified;
            }
        }

        private void RemoveStudentsFromContext(int idLogCarga)
        {
            foreach (var student in _context.Alumno.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.Alumno.Remove(student);
            }
        }
    }
}
