﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class ClassesDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public ClassesDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteClassSubject = GetClassSubjectsFromUpload(idLogCarga);
                var toDeleteClassProfessors = GetClassProfessorsFromUpload(idLogCarga);
                var toDeleteClass = GetClassesFromUpload(idLogCarga);
                var toDeleteAcademicPeriodSubject = GetSubjectAcademicPeriodsFromUpload(idLogCarga);

                foreach (var classSubject in toDeleteClassSubject)
                {
                    _context.SeccionCurso.Remove(classSubject);
                }

                foreach (var classProfessor in toDeleteClassProfessors)
                {
                    _context.DocenteSeccion.Remove(classProfessor);
                }

                foreach (var dClass in toDeleteClass)
                {
                    _context.Seccion.Remove(dClass);
                }

                foreach (var academicPeriodSubject in toDeleteAcademicPeriodSubject)
                {
                    _context.CursoPeriodoAcademico.Remove(academicPeriodSubject);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocenteSeccion> GetClassProfessorsFromUpload(int idLogCarga)
        {
            return _context.DocenteSeccion.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<SeccionCurso> GetClassSubjectsFromUpload(int idLogCarga)
        {
            return _context.SeccionCurso.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<Seccion> GetClassesFromUpload(int idLogCarga)
        {
            return _context.Seccion.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<CursoPeriodoAcademico> GetSubjectAcademicPeriodsFromUpload(int idLogCarga)
        {
            return _context.CursoPeriodoAcademico.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
