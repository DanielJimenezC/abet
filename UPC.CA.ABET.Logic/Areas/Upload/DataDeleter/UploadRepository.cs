﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class UploadRepository
    {
        private AbetEntities _context;

        public UploadRepository()
        {
            _context = new AbetEntities();
        }

        public LogCarga Get(int id)
        {
            return _context.LogCarga.FirstOrDefault(x => x.IdLogCarga == id);
        }

        public List<LogCarga> GetSortedByDescendingId()
        {
            return _context.LogCarga.OrderByDescending(x => x.IdLogCarga).ToList();
        }

        public void Delete(int id)
        {
            var logCarga = _context.LogCarga.FirstOrDefault(x => x.IdLogCarga == id);
            _context.LogCarga.Remove(logCarga);
            _context.SaveChanges();
        }
    }
}
