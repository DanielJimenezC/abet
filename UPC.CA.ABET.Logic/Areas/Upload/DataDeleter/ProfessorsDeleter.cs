﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class ProfessorsDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public ProfessorsDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteProfessors = GetProfessorsFromUpload(idLogCarga);

                foreach (var professor in toDeleteProfessors)
                {
                    _context.Docente.Remove(professor);
                }

                _context.SaveChanges();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<Docente> GetProfessorsFromUpload(int idLogCarga)
        {
            return _context.Docente.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

    }
}
