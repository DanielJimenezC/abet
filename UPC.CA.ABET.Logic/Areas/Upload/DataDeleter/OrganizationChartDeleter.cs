﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class OrganizationChartDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public OrganizationChartDeleter()
        {
            _context = new AbetEntities();
        }
        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteAcademicUnitResponsible = GetAcademicUnitResponsibleFromUpload(idLogCarga);
                var toDeleteCampusAcademicUnit = GetCampusAcademicUnitFromUpload(idLogCarga);
                var toDeleteAcademicUnit = GetAcademicUnitFromUpload(idLogCarga);

                foreach (var academicUnitResponsible in toDeleteAcademicUnitResponsible)
                {
                    _context.UnidadAcademicaResponsable.Remove(academicUnitResponsible);
                }

                foreach (var campusAcademicUnit in toDeleteCampusAcademicUnit)
                {
                    _context.SedeUnidadAcademica.Remove(campusAcademicUnit);
                }

                foreach (var academicUnit in toDeleteAcademicUnit)
                {
                    _context.UnidadAcademica.Remove(academicUnit);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UnidadAcademicaResponsable> GetAcademicUnitResponsibleFromUpload(int idLogCarga)
        {
            return _context.UnidadAcademicaResponsable.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<SedeUnidadAcademica> GetCampusAcademicUnitFromUpload(int idLogCarga)
        {
            return _context.SedeUnidadAcademica.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<UnidadAcademica> GetAcademicUnitFromUpload(int idLogCarga)
        {
            return _context.UnidadAcademica.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
