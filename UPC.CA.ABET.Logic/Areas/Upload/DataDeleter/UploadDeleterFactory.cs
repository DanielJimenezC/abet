﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class UploadDeleterFactory
    {
        public static UploadDeleter Create(string tipoCarga)
        {
            switch (tipoCarga)
            {
                case ConstantHelpers.UPLOAD_TYPE.DOCENTE:
                    return new ProfessorsDeleter();
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_MATRICULADOS:
                    return new EnrolledStudentsDeleter();
                case ConstantHelpers.UPLOAD_TYPE.OUTCOME:
                    return new OutcomeDeleter();
                case ConstantHelpers.UPLOAD_TYPE.SECCION:
                    return new ClassesDeleter();
                case ConstantHelpers.UPLOAD_TYPE.MALLA_CURRICULAR:
                    return new CurriculumDeleter();
                case ConstantHelpers.UPLOAD_TYPE.ORGANIGRAMA:
                    return new OrganizationChartDeleter();
                case ConstantHelpers.UPLOAD_TYPE.ATTRITION:
                    return new AttritionDeleter();
                case ConstantHelpers.UPLOAD_TYPE.TITULADOS:
                    return new CertificatedStudentsDeleter();
                case ConstantHelpers.UPLOAD_TYPE.COHORTE:
                    return new CohortDeleter();
                case ConstantHelpers.UPLOAD_TYPE.COHORTE_INGRESANTES:
                    return new CohortIndicatorDeleter();
                case ConstantHelpers.UPLOAD_TYPE.ASISTENCIA_DOCENTES:
                    return new ProfessorsAttendanceDeleter();
                case ConstantHelpers.UPLOAD_TYPE.PUNTUALIDAD_DOCENTES:
                    return new ProfessorsPunctualityDeleter();
                case ConstantHelpers.UPLOAD_TYPE.ALUMNOS_POR_SECCION:
                    return new StudentClassDeleter();
                case ConstantHelpers.UPLOAD_TYPE.EVALUACIONES:
                    return new GradesDeleter();
                case ConstantHelpers.UPLOAD_TYPE.DELEGADOS:
                    return new DelegatesDeleter();
                default:
                    throw new Exception("Tipo de carga no soportado");
            }
        }
    }
}
