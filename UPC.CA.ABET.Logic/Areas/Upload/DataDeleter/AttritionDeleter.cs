﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class AttritionDeleter : UploadDeleter
    {

        private AbetEntities _context;

        public AttritionDeleter()
        {
            _context = new AbetEntities();
        }

        public void DeleteAsociatedEntities(int idLogCarga)
        {
            RemoveAttritionDetailsFromContext(idLogCarga);
            RemoveAttritionFromContext(idLogCarga);

            _context.SaveChanges();
        }

        private void RemoveAttritionDetailsFromContext(int idLogCarga)
        {
            foreach (var attritionDetail in _context.AttritionDetalle.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.AttritionDetalle.Remove(attritionDetail);
            }
        }

        private void RemoveAttritionFromContext(int idLogCarga)
        {
            foreach (var attrition in _context.Attrition.Where(x => x.IdLogCarga == idLogCarga))
            {
                _context.Attrition.Remove(attrition);
            }
        }
    }
}
