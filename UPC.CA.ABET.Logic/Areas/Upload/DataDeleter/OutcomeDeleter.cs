﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataDeleter
{
    public class OutcomeDeleter : UploadDeleter
    {
        private AbetEntities _context;

        public OutcomeDeleter()
        {
            _context = new AbetEntities();
        }
        public void DeleteAsociatedEntities(int idLogCarga)
        {
            try
            {
                var toDeleteOutcomeComissions = GetOutcomeComissionFromUpload(idLogCarga);
                var toDeleteOutcomes = GetOutcomeFromUpload(idLogCarga);

                foreach (var outcomeComission in toDeleteOutcomeComissions)
                {
                    _context.OutcomeComision.Remove(outcomeComission);
                }

                foreach (var outcome in toDeleteOutcomes)
                {
                    _context.Outcome.Remove(outcome);
                }

                _context.SaveChanges();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<OutcomeComision> GetOutcomeComissionFromUpload(int idLogCarga)
        {
            return _context.OutcomeComision.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }

        public List<Outcome> GetOutcomeFromUpload(int idLogCarga)
        {
            return _context.Outcome.Where(x => x.IdLogCarga == idLogCarga).ToList();
        }
    }
}
