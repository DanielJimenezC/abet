﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class ComboItem
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}
