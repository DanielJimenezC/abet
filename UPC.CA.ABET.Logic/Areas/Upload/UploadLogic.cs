﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UPC.CA.ABET.Models;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;

namespace UPC.CA.ABET.Logic.Areas.Upload
{
    public class UploadLogic
    {
        private AbetEntities _context;

        public UploadLogic(AbetEntities context)
        {
            _context = context;

        }
        public List<PeriodoAcademico> GetAcademicPeriods()
        {
            return _context
                .PeriodoAcademico.Select(x => x)
                .OrderByDescending(x => x.CicloAcademico)
                .ToList();
        }
        public bool UploadExists(string termId, string uploadType)
        {
            /*int id = int.Parse(termId);
            return _context.LogCarga.FirstOrDefault(
                x => x.LogTipoCarga.Nombre == uploadType &&
                x.IdPeriodoAcademico == id)
                != null;*/

            return false;
        }

        public List<Carrera> GetCareers(string termId)
        {
            int id = int.Parse(termId);
            //int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == id).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsubmoda = id;

            return _context.Carrera
                    .Where(x => x.CarreraPeriodoAcademico.Any(y => y.IdSubModalidadPeriodoAcademico == idsubmoda))
                    .Select(x => x).ToList();
        }

        public List<Comision> GetCommissions(string termId, string careerId)
        {
            // TODO: Seleccionar solo las comisiones que no están asociadas
            int tid = int.Parse(termId);
            // int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == tid).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsubmoda = tid;

            int cid = int.Parse(careerId);
            return _context.Comision
                    .Where(x => x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == idsubmoda)
                    .Where(x => !x.CarreraComision.Any(y => y.Carrera.IdCarrera == cid))
                    .Select(x => x).ToList();
        }

        public List<MallaCurricular> GetCurriculums(int termId,int? escuelaId)
        {

            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == termId).FirstOrDefault().IdSubModalidad;
            int idsubmodapa = termId;



            var lstCarrera = _context.Carrera.Where(x => x.Escuela.IdEscuela == escuelaId && x.IdSubmodalidad == idsubmoda).Select(x => x.IdCarrera).ToList();
            //int carreraid = _context.Carrera.Where(x => x.Escuela.IdEscuela == escuelaId && x.IdSubmodalidad == idsubmoda).FirstOrDefault().IdCarrera;
            //var idCarrerasMallaPeriodoActual = _context.MallaCurricularPeriodoAcademico
            //    .Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda)
            //    .Select(x => x.MallaCurricular.IdCarrera)
            //    .Distinct()
            //    .ToList();


            var idMallasCurricularesAsociadas = _context.MallaCurricularPeriodoAcademico
                .Where(x => x.IdSubModalidadPeriodoAcademico == idsubmodapa)
                .Select(x => x.IdMallaCurricular)
                .Distinct()
                .ToList();


            //var idMallaCurricularAsociada = _context.MallaCurricularPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda)
            //     .Select(x => x.MallaCurricular.IdMallaCurricular)
            //     .Distinct()
            //     .ToList();

            //return _context.MallaCurricular
            //    .Where(x => idCarrerasMallaPeriodoActual.Contains(x.IdCarrera) && !idMallaCurricularAsociada.Contains(x.IdMallaCurricular))
            //    .Select(x => x)
            //    .ToList();

            return _context.MallaCurricular
                .Where(x => !idMallasCurricularesAsociadas.Contains(x.IdMallaCurricular) &&  lstCarrera.Contains(x.IdCarrera) && x.LogCarga.IdSubModalidadPeriodoAcademico == idsubmodapa).ToList()
                .Select(x => x)
                .ToList();
        }

        public void AsociateCareerAndCommission(int termId, int careerId, int commissionId)
        {
            //termdID es el valor de la submodalidadPeriodoacademico
            // int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            int idsubmoda = termId;

            var carreraComision = CreateCareerCommission(idsubmoda, careerId, commissionId);
            _context.CarreraComision.Add(carreraComision);
            _context.SaveChanges();
        }

        private static CarreraComision CreateCareerCommission(int termId, int careerId, int commissionId)
        {

           //Entra Submoda

            var carreraComision = new CarreraComision
            {
                IdSubModalidadPeriodoAcademico = termId,
                IdCarrera = careerId,
                IdComision = commissionId
            };
            return carreraComision;
        }

        public List<CarreraComision> GetCareerCommissions(string termId)
        {
            int _termId = int.Parse(termId);
            //int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == _termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            int idsubmoda = _termId;

            return _context.CarreraComision.Select(x => x).Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).ToList();
        }

        //TODO
        //public List<CarreraComision> GetCareerCommissions(string termId, int? escuelaId)
        //{
        //    int _termId = int.Parse(termId);
        //    int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == _termId).FirstOrDefault().IdSubModalidad;

        //    int idsubmodapa = _termId;
        //    int carreraid = _context.Carrera.Where(x => x.Escuela.IdEscuela == escuelaId && x.IdSubmodalidad == idsubmoda).FirstOrDefault().IdCarrera;

        //    return _context.CarreraComision.Select(x => x).Where(x => x.IdSubModalidadPeriodoAcademico == idsubmodapa && x.IdCarrera == carreraid).ToList();
        //}

        public List<MallaCurricularPeriodoAcademico> GetCurriculumAcademicPeriod(int termId, int escuelaId)
        {


            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == termId).FirstOrDefault().IdSubModalidad;

            int idsubmodapa = termId;

            var lstCarrera = _context.Carrera.Where(x => x.Escuela.IdEscuela == escuelaId && x.IdSubmodalidad == idsubmoda).Select(x => x.IdCarrera).ToList();
            //int carreraid = _context.Carrera.Where(x => x.Escuela.IdEscuela == escuelaId && x.IdSubmodalidad == idsubmoda).FirstOrDefault().IdCarrera;
            var mallaAsociadas = _context.MallaCurricular.Where(x=> lstCarrera.Contains(x.IdCarrera)).Select(x=> x.IdMallaCurricular).ToList();

            return _context.MallaCurricularPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico== idsubmodapa && mallaAsociadas.Contains(x.IdMallaCurricular)).ToList();
        }

        public void DeleteCareerCommission(string careerCommissionId)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                int id = int.Parse(careerCommissionId);
                CarreraComision carreraComision = _context.CarreraComision.FirstOrDefault(x => x.IdCarreraComision == id);
                _context.CarreraComision.Remove(carreraComision);
                _context.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        public List<Modalidad> GetModalType()
        {
			var LstModalidad = _context.Usp_ListModalities().ToList();
			LstModalidad.OrderByDescending(x => x.IdModalidad);

			List<Modalidad> obj = new List<Modalidad>();

			foreach (var ob in LstModalidad)
			{
				Modalidad m = new Modalidad();

				m.IdModalidad = ob.IdModalidad;
				m.NombreEspanol = ob.NombreEspanol;
				m.NombreIngles = ob.NombreIngles;

				obj.Add(m);
			}


			return obj.ToList();

		}

        public List<SubModalidad> GetSubModal(int idModal)
        {
            return _context
                .SubModalidad.Select(x => x)
                .Where(x => x.IdModalidad == idModal)
                .OrderByDescending(x => x.IdSubModalidad)
                .ToList();
        }

        public List<PeriodoAcademico> GetAcademicPeriods(int idSubmodalidad)
        {
            return _context
                .PeriodoAcademico.Select(x => x)
                .Where(x => x.IdPeriodoAcademico == idSubmodalidad)
                .ToList();
        }
        public IEnumerable<ComboItem> ListCyclesToForModality(string ModalityId = Modality.PREGRADO_REGULAR, int CycleFromId = 0, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboSubModalidad {0}, {1}, {2}", Skip, ModalityId, CycleFromId).AsEnumerable();
        }
    }
}
