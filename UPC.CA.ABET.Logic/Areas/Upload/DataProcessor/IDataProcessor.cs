﻿using System.Collections.Generic;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor
{
    public abstract class IDataProcessor<T>
    {
        protected List<T> Data { get; set; }
        public void ProcessData(string path, AbetEntities entities, int termId, int userId) {
            LoadDataFromFile(path);
            ValidateData();
            SaveData(entities, termId, userId);
        }
        protected abstract void LoadDataFromFile(string path);
        protected abstract void ValidateData();
        protected abstract void SaveData(AbetEntities entities, int termId, int userId);
    }
}
