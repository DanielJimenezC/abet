﻿using System.Collections.Generic;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor
{
    public class DataProcessor<T>
    {
        private readonly IDataProcessor<T> _processor;

        public DataProcessor(IDataProcessor<T> processor)
        {
            _processor = processor;
        }

        public void ProcessData(string path, AbetEntities entities, string idCiclo, string IdUsuario)
        {
            _processor.ProcessData(path, entities, int.Parse(idCiclo), int.Parse(IdUsuario));
        }
    }
}