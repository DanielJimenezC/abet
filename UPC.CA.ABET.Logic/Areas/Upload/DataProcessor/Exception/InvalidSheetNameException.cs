﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception
{
    class InvalidSheetNameException: System.Exception
    {
        public InvalidSheetNameException(string message)
            : base(message)
        {
        }
    }
}
