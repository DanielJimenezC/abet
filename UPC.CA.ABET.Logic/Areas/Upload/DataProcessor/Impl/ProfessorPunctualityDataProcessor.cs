﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class ProfessorPunctualityDataProcessor : IDataProcessor<PuntualidadFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        List<PuntualidadDocente> listaPuntualidad;
        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<PuntualidadFileModel>("Hoja1").Select(x => x).Where(x => x.nombreDocente != "").ToList();
        }

        protected override void ValidateData()
        {
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            listaPuntualidad = new List<PuntualidadDocente>();

            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.PUNTUALIDAD_DOCENTES, 
                    termId, 
                    userId);

                foreach (var row in Data)
                {
                    //Sanitizer.SanitizeObjectStrings(row);
                    var docente = FindDocente(entities, row);

                    if(docente != null)
                    {
                        var puntualidadDocente = FindPuntualidadDocente(entities, docente, row, termId, logCarga);

                        if(puntualidadDocente == null)
                        {
                            CreatePuntualidadDocente(docente, row, termId, logCarga);
                        }
                        else
                        {
                            SumLateMinutes(puntualidadDocente, row, entities);
                        }
                    }
                }

                AddPunctualityToEntities(listaPuntualidad, entities);
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private void AddPunctualityToEntities(List<PuntualidadDocente> listaPuntualidad, AbetEntities entities)
        {
            foreach (var puntualidad in listaPuntualidad)
            {
                entities.PuntualidadDocente.Add(puntualidad);
            }
        }

        private Docente FindDocente(AbetEntities entities, PuntualidadFileModel row)
        {
            var firstSplit = row.nombreDocente.Split('-');
            var nombreCompleto = firstSplit[1].Trim();

            var secondSplit = nombreCompleto.Split(',');

            var apellidos = secondSplit[0].Trim();
            var nombres = secondSplit[1].Trim();

            return entities.Docente.FirstOrDefault(x => x.Nombres == nombres && x.Apellidos == apellidos);
        }

        private PuntualidadDocente FindPuntualidadDocente(AbetEntities entities, Docente docente, PuntualidadFileModel row, int termId, LogCarga logCarga)
        {
            return entities.PuntualidadDocente.FirstOrDefault(x => x.Docente.IdDocente == docente.IdDocente && x.IdSubModalidadPeriodoAcademicoModulo == termId) 
                ?? listaPuntualidad.FirstOrDefault(x => x.Docente.IdDocente == docente.IdDocente && x.IdSubModalidadPeriodoAcademicoModulo == termId);
        }

        private PuntualidadDocente CreatePuntualidadDocente(Docente docente, PuntualidadFileModel row, int termId, LogCarga logCarga)
        {
            var puntualidadDocente =  new PuntualidadDocente
            {
                Docente = docente,
                MinutosTardanza = Int32.Parse(row.minutosTardanza),
                LogCarga1 = logCarga,
                IdSubModalidadPeriodoAcademicoModulo = termId
            };

            listaPuntualidad.Add(puntualidadDocente);
            return puntualidadDocente;
        }

        private void SumLateMinutes(PuntualidadDocente puntualidadDocente, PuntualidadFileModel row, AbetEntities entities)
        {
            puntualidadDocente.MinutosTardanza += Int32.Parse(row.minutosTardanza);
            //entities.Entry(puntualidadDocente).State = EntityState.Modified;
        }
    }
}
