﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class DelegatesDataProcessor : IDataProcessor<DelegadosFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        List<Seccion> listaSeccionesNuevas;
        List<Alumno> listaAlumnosNuevos;
        List<AlumnoMatriculado> listaMatriculadosNuevos;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName
                    + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<DelegadosFileModel>("Hoja1")
                .Select(x => x)
                .Where(x => x.CodigoCurso != "")
                .ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => new { x.CodigoAlumno, x.CodigoCurso, x.CodigoSeccion }).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            listaSeccionesNuevas = new List<Seccion>();
            listaAlumnosNuevos = new List<Alumno>();
            listaMatriculadosNuevos = new List<AlumnoMatriculado>();

            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities,
                    ConstantHelpers.UPLOAD_TYPE.DELEGADOS,
                    termId,
                    userId);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);

                    var cursoPeriodoAcademico = FindCursoPeriodoAcademico(entities, row, termId);

                    if (cursoPeriodoAcademico != null)
                    {
                        var seccion = FindSeccion(cursoPeriodoAcademico, row, logCarga, entities, termId);
                        var alumnoSeccion = FindAlumnoSeccion(entities, seccion, row, logCarga, termId);

                        alumnoSeccion.EsDelegado = true;
                    }
                }
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private CursoPeriodoAcademico FindCursoPeriodoAcademico(AbetEntities entities, DelegadosFileModel row, int termId)
        {
            return entities.CursoPeriodoAcademico.FirstOrDefault(x => x.Curso.Codigo == row.CodigoCurso && x.IdSubModalidadPeriodoAcademico == termId);
        }

        private Seccion FindSeccion(CursoPeriodoAcademico cursoPeriodoAcademico, DelegadosFileModel row, LogCarga logCarga, AbetEntities entities, int termId)
        {
            var seccion = entities.Seccion.FirstOrDefault(x => x.Codigo == row.CodigoSeccion && x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == cursoPeriodoAcademico.IdCursoPeriodoAcademico) ??
                listaSeccionesNuevas.FirstOrDefault(x => x.Codigo == row.CodigoSeccion && x.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == termId);

            if (seccion == null)
            {
                seccion = new Seccion
                {
                    Codigo = row.CodigoSeccion,
                    RevisionHorarioATiempo = true,
                    LogCarga = logCarga
                };
                listaSeccionesNuevas.Add(seccion);
                cursoPeriodoAcademico.Seccion.Add(seccion);
            }
            return seccion;
        }

        private Alumno FindAlumno(AbetEntities entities, DelegadosFileModel row, LogCarga logCarga)
        {
            var alumno = entities.Alumno.FirstOrDefault(x => x.Codigo == row.CodigoAlumno) ??
                listaAlumnosNuevos.FirstOrDefault(x => x.Codigo == row.CodigoAlumno);

            if (alumno == null)
            {
                var nombreCompletoAlumno = row.NombreAlumno;
                var nombrePartidoAlumno = nombreCompletoAlumno.Split(',');
                var apellidosAlumno = nombrePartidoAlumno[0].Trim();
                var nombresAlumno = nombrePartidoAlumno[1].Trim();

                alumno = new Alumno
                {
                    Codigo = row.CodigoAlumno,
                    Nombres = nombresAlumno,
                    Apellidos = apellidosAlumno,
                    LogCarga = logCarga
                };
                listaAlumnosNuevos.Add(alumno);
            }

            return alumno;
        }

        private AlumnoMatriculado FindAlumnoMatriculado(AbetEntities entities, DelegadosFileModel row, LogCarga logCarga, int termId)
        {
            var alumno = FindAlumno(entities, row, logCarga);

            var alumnoMatriculado = entities.AlumnoMatriculado.FirstOrDefault(x => x.Alumno.IdAlumno == alumno.IdAlumno && x.IdSubModalidadPeriodoAcademico == termId) ??
                listaMatriculadosNuevos.FirstOrDefault(x => x.Alumno.IdAlumno == alumno.IdAlumno && x.IdSubModalidadPeriodoAcademico == termId);

            if (alumnoMatriculado == null)
            {
                alumnoMatriculado = new AlumnoMatriculado
                {
                    Alumno = alumno,
                    IdSubModalidadPeriodoAcademico = termId,
                    EstadoMatricula = "MRE",
                    LogCarga = logCarga
                };

                listaMatriculadosNuevos.Add(alumnoMatriculado);
            }

            return alumnoMatriculado;
        }

        private AlumnoSeccion FindAlumnoSeccion(AbetEntities entities, Seccion seccion, DelegadosFileModel row, LogCarga logCarga, int termId)
        {
            var alumnoSeccion = entities.AlumnoSeccion.FirstOrDefault(x => x.IdSeccion == seccion.IdSeccion && x.AlumnoMatriculado.Alumno.Codigo == row.CodigoAlumno);

            if (alumnoSeccion == null)
            {
                alumnoSeccion = new AlumnoSeccion
                {
                    Seccion = seccion,
                    AlumnoMatriculado = FindAlumnoMatriculado(entities, row, logCarga, termId),
                    LogCarga = logCarga
                };

                entities.Entry(alumnoSeccion).State = EntityState.Added;
            }
            else
            {
                entities.CargaAlumnoSeccionHistorico.Add(CreateCargaAlumnoSeccionHistorico(entities, logCarga, alumnoSeccion));
                entities.Entry(alumnoSeccion).State = EntityState.Modified;
            }
            return alumnoSeccion;
        }

        private CargaAlumnoSeccionHistorico CreateCargaAlumnoSeccionHistorico(AbetEntities entities, LogCarga logCarga, AlumnoSeccion alumnoSeccion)
        {
            return new CargaAlumnoSeccionHistorico
            {
                AlumnoSeccion = alumnoSeccion,
                LogCarga = logCarga,
                NotaHistorica = alumnoSeccion.Nota,
                EsDelegado = alumnoSeccion.EsDelegado
            };
        }
    }
}