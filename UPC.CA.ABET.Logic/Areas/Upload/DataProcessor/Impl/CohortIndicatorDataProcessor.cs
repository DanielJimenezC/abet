﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class CohortIndicatorDataProcessor : IDataProcessor<IndicadorCohorteFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<IndicadorCohorteFileModel>("Hoja1").Select(x => x).Where(x => x.cantidadEgresados != "").ToList();
        }
        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => new { x.ciclo, x.codigoCarrera }).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.COHORTE_INGRESANTES, 
                    termId, 
                    userId);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    CreateAndAddCohortIndicator(entities, termId, row, logCarga);
                }

                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private void CreateAndAddCohortIndicator(AbetEntities entities, int termId, IndicadorCohorteFileModel row, LogCarga logCarga)
        {
            //validar que recibe termID

            int idsubmoda = entities.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = entities.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;


            var cohortIndicator = new IndicadorCohorte
            {
                IdSubModalidadPeriodoAcademicoModulo = idsumodapamodu,
                CantidadEgresados = Int32.Parse(row.cantidadEgresados),
                CantidadIngresantes = Int32.Parse(row.cantidadIngresantes),
                LogCarga = logCarga,
                Ciclo = row.ciclo,
                Carrera = GetCareer(entities, row.codigoCarrera)
            };

            entities.IndicadorCohorte.Add(cohortIndicator);
        }

        private Carrera GetCareer(AbetEntities context, string code)
        {
            var career = context.Carrera.FirstOrDefault(x => x.Codigo == code);
            if (career == null)
            {
                throw new System.Exception("La carrera con código " + code + " no existe");
            }
            return career;
        }
    }
}
