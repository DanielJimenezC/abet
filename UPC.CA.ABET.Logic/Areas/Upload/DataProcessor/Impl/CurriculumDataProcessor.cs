﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class CurriculumDataProcessor : IDataProcessor<CurriculaFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        private LogCarga _logCarga;
        private List<NivelAcademico> _addedAcademicLevels;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<CurriculaFileModel>("Hoja1").Select(x => x).Where(x => x.CodigoDeCurso != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => new { x.CodigoDeCurso }).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities context, int termId, int userId)
        {
            _addedAcademicLevels = new List<NivelAcademico>();
            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                _logCarga = LogCargaBuilder.GetLogCarga(
                    context, 
                    ConstantHelpers.UPLOAD_TYPE.MALLA_CURRICULAR, 
                    termId, 
                    userId);

                ThrowExceptionIfCurriculumAlreadyExists(context);

                var curriculum = CreateCurriculum(context);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    curriculum.CursoMallaCurricular.Add(CreateCurriculumSubject(context, row));
                }

                foreach (var row in Data)
                {
                    if (SubjectHasRequirements(row))
                    {
                        AddRequirementsToSubject(curriculum, row);
                    }
                }

                context.MallaCurricular.Add(curriculum);
                context.SaveChanges();
                dbContextTransaction.Commit();
            }

        }

        private void ThrowExceptionIfCurriculumAlreadyExists(AbetEntities entities)
        {
            var firstRow = GetFirstRow();
            if (entities.MallaCurricular.FirstOrDefault(x => x.Codigo == firstRow.CodigoCurricula) != null)
            {
                throw new System.Exception("La malla curricular con código " + firstRow.CodigoCurricula + " ya existe");
            }

        }

        private MallaCurricular CreateCurriculum(AbetEntities context)
        {
            return new MallaCurricular
            {
                Codigo = GetFirstRow().CodigoCurricula,
                Carrera = GetCareer(context),
                LogCarga = _logCarga
               
            };
        }

        private Carrera GetCareer(AbetEntities context)
        {
            var firstRow = GetFirstRow();
            var career = context.Carrera.FirstOrDefault(x => x.Codigo == firstRow.Carrera);
            if (career == null)
            {
                throw new System.Exception("La carrera con código " + firstRow.Carrera + " no existe");
            }
            return career;
        }

        private CursoMallaCurricular CreateCurriculumSubject(AbetEntities context, CurriculaFileModel row)
        {
            return new CursoMallaCurricular
            {
                NivelAcademico = GetOrCreateAcademicLevel(context, row),
                Curso = GetOrCreateSubject(context, row),
                LogroFinCicloEspanol = row.Logro,
                LogroFinCicloIngles = row.LogroIngles,
                TipoEvaluacion = row.CampoEvaluacion,
                EsElectivo = IsElective(row),
                EsFormacion = true,
                SilaboEntregadoTiempo = true,
                LogCarga = _logCarga,
                NombreMalla = row.NombreMalla
               
            };
        }

        private NivelAcademico GetOrCreateAcademicLevel(AbetEntities context, CurriculaFileModel row)
        {
            var levelNumber = int.Parse(row.Nivel);
            var nivelAcademico = context.NivelAcademico.FirstOrDefault(x => x.Numero == levelNumber) ?? _addedAcademicLevels.FirstOrDefault(x => x.Numero == levelNumber);

            if (nivelAcademico == null)
            {
                nivelAcademico = new NivelAcademico
                {
                    Numero = levelNumber,
                };
                _addedAcademicLevels.Add(nivelAcademico);
            }

            return nivelAcademico;
        }

        private Curso GetOrCreateSubject(AbetEntities context, CurriculaFileModel row)
        {
            var curso = context.Curso.FirstOrDefault(x => x.Codigo == row.CodigoDeCurso);

            if (curso == null)
            {
                curso = new Curso
                {
                    Codigo = row.CodigoDeCurso,
                    NombreEspanol = row.NombreDeCurso,
                    NombreIngles = row.NombreEnIngles,
                    LogCarga = _logCarga
                };
            }

            return curso;
        }

        private bool IsElective(CurriculaFileModel row)
        {
            return string.Equals(row.EsElectivo, "SI", StringComparison.OrdinalIgnoreCase);
        }

        private void AddRequirementsToSubject(MallaCurricular curriculum, CurriculaFileModel row)
        {
            var subjectWithRequirement = curriculum.CursoMallaCurricular.FirstOrDefault(x => x.Curso.Codigo == row.CodigoDeCurso);

            string[] codes = GetCodes(row);

            foreach (var code in codes)
            {
                var addedSubject = FindAddedSubject(curriculum, code);

                if (addedSubject != null)
                {
                    var requiredSubject = CreateRequiredSubject(addedSubject);
                    subjectWithRequirement.RequisitoCurso.Add(requiredSubject);
                }
            }
        }

        private static string[] GetCodes(CurriculaFileModel row)
        {
            return row.Requisitos.Split(null);
        }

        private Curso FindAddedSubject(MallaCurricular curriculum, string code)
        {
            return curriculum.CursoMallaCurricular.Select(x => x.Curso).FirstOrDefault(x => x.Codigo == code);
        }

        private RequisitoCurso CreateRequiredSubject(Curso addedSubject)
        {
            return new RequisitoCurso
            {
                Curso = addedSubject,
                LogCarga = _logCarga
            };
        }

        private bool SubjectHasRequirements(CurriculaFileModel row)
        {
            return row.Requisitos != null && row.Requisitos != "";
        }

        private CurriculaFileModel GetFirstRow()
        {
            return Data.ElementAt(0);
        }
    }
}
