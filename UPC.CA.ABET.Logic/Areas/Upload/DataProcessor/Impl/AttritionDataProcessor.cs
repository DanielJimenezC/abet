﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class AttritionDataProcessor : IDataProcessor<AttritionFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<AttritionFileModel>("Hoja1").Select(x => x).Where(x => x.Baja != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => new { x.Ciclo, x.Carrera }).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.ATTRITION, 
                    termId,
                    userId);

                var attrition = CreateAttrition(termId, logCarga);
                entities.Attrition.Add(attrition);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    entities.AttritionDetalle.Add(CreateAttritionDetail(entities, row, attrition, logCarga));
                }
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private Attrition CreateAttrition(int termId, LogCarga logCarga)
        {
            return new Attrition
            {
                IdSubModalidadPeriodoAcademico = termId,
                LogCarga = logCarga
            };
        }

        private Carrera FindCarreraByNombre(AbetEntities entities, AttritionFileModel row)
        {
            var carrera = entities.Carrera.FirstOrDefault(x => x.Codigo == row.Carrera);

            if (carrera == null)
            {
                throw new System.Exception(
                    "No se encontró la carrera con código " + row.Carrera);
            }

            return carrera;

        }

        private AttritionDetalle CreateAttritionDetail(AbetEntities entities, AttritionFileModel row, Attrition attrition, LogCarga logCarga)
        {
            var carrera = row.Carrera;
            return new AttritionDetalle
            {
                Attrition = attrition,
                Baja = Int32.Parse(row.Baja),
                CambioModalidad = Int32.Parse(row.CambioDeModalidad),
                Desercion = Int32.Parse(row.Desercion),
                Reserva = Int32.Parse(row.Reserva),
                SoloIngles = Int32.Parse(row.SoloIngles),
                Ciclo = row.Ciclo,
                TotalMatriculados = Int32.Parse(row.TotalMatriculados),
                TotalAttrition = Int32.Parse(row.TotalAttrition),
                LogCarga = logCarga,
                Carrera = FindCarreraByNombre(entities, row)
            };
        }
    }
}
