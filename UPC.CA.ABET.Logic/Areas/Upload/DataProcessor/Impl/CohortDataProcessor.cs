﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class CohortDataProcessor : IDataProcessor<CohorteFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<CohorteFileModel>("Hoja1").Select(x => x).Where(x => x.Codigo != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => x.Codigo).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.COHORTE, 
                    termId, 
                    userId);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    var alumno = FindStudentByCode(entities, row);

                    if (alumno == null)
                    {
                        alumno = CreateAlumno(entities, row, logCarga);
                        entities.Alumno.Add(alumno);
                    }
                    else
                    {
                        entities.CargaAlumnoHistorico.Add(CreateAlumnoHistorico(entities, logCarga, alumno)); // save actual values before update
                        UpdateGraduateStudent(alumno, row, entities);
                    }
                }
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private Alumno FindStudentByCode(AbetEntities entities, CohorteFileModel row)
        {
            var student = entities.Alumno.FirstOrDefault(x => x.Codigo == row.Codigo);
            if (student == null)
            {
                throw new System.Exception("No se encontró al alumno con el código " + row.Codigo);
            }
            return student;
        }

        private Alumno CreateAlumno(AbetEntities entities, CohorteFileModel row, LogCarga logCarga)
        {
            var nombreCompletoAlumno = row.NombresApellidos;
            var nombrePartidoAlumno = nombreCompletoAlumno.Split(',');
            var apellidosAlumno = nombrePartidoAlumno[0].Trim();
            var nombresAlumno = nombrePartidoAlumno[1].Trim();

            var alumno =  new Alumno
            {
                Nombres = nombresAlumno,
                Apellidos = apellidosAlumno,
                Codigo = row.Codigo,
                CorreoElectronico = row.Email,
                CiclosMatriculados = Int32.Parse(row.CiclosMatriculados),
                AnioEgreso = row.PromocionEgreso,
                Carrera = GetCareer(entities, row.Estudio),
                LogCarga = logCarga
            };

            return alumno;
        }

        private void UpdateGraduateStudent(Alumno alumno, CohorteFileModel row, AbetEntities entities)
        {
            alumno.AnioEgreso = row.PromocionEgreso;
            alumno.CiclosMatriculados = Int32.Parse(row.CiclosMatriculados);
            alumno.Carrera = GetCareer(entities, row.Estudio);
            entities.Entry(alumno).State = EntityState.Modified;
        }

        private Carrera GetCareer(AbetEntities context, string careerCode)
        {
            var career = context.Carrera.FirstOrDefault(x => x.Codigo == careerCode);
            if (career == null)
            {
                throw new System.Exception("No se encontró la carrera con código " + careerCode);
            }
            return career;
        }

        private CargaAlumnoHistorico CreateAlumnoHistorico(AbetEntities entities, LogCarga logCarga, Alumno alumno)
        {
            return new CargaAlumnoHistorico
            {
                Alumno = alumno,
                LogCarga = logCarga,
                AnioEgreso = alumno.AnioEgreso,
                AnioTitulacion = alumno.AnioTitulacion,
                IdCarreraEgreso = alumno.IdCarreraEgreso
            };
        }
    }
}
