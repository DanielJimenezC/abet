﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Data.Entity;


namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class CertificatedStudentsDataProcessor : IDataProcessor<TituladosFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<TituladosFileModel>("Hoja1").Select(x => x).Where(x => x.codigo != "").ToList();
        }

        protected override void ValidateData()
        {
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            var logCarga = LogCargaBuilder.GetLogCarga(
                entities, 
                ConstantHelpers.UPLOAD_TYPE.TITULADOS, 
                termId, userId);

            foreach (var row in Data)
            {
                Sanitizer.SanitizeObjectStrings(row);
                var alumno = FindAlumno(entities, row);

                if (alumno == null)
                {
                    alumno = CreateAlumno(entities, row, logCarga, termId);
                    entities.Alumno.Add(alumno);
                }
                else
                {
                    entities.CargaAlumnoHistorico.Add(CreateAlumnoHistorico(entities, logCarga, alumno)); // Save old values
                    UpdateCertificatedStudent(alumno, row, entities, termId);
                }

            }
            entities.SaveChanges();
        }

        private Alumno FindAlumno(AbetEntities entities, TituladosFileModel row)
        {
            return entities.Alumno.FirstOrDefault(x => x.Codigo == row.codigo);
        }

        private Alumno CreateAlumno(AbetEntities entities, TituladosFileModel row, LogCarga logCarga, int termId)
        {
            var nombreCompletoAlumno = row.nombresApellidos;
            var nombrePartidoAlumno = nombreCompletoAlumno.Split(',');
            var nombresAlumno = nombrePartidoAlumno[0].Trim();
            var apellidosAlumno = nombrePartidoAlumno[1].Trim();

            return new Alumno
            {
                Nombres = nombresAlumno,
                Apellidos = apellidosAlumno,
                Codigo = row.codigo,
                Carrera = GetCareer(entities, row.estudio),
                AnioTitulacion = FindTerm(entities, termId).CicloAcademico,
                LogCarga = logCarga
            };
        }

        private Carrera GetCareer(AbetEntities context, string careerCode)
        {
            var career = context.Carrera.FirstOrDefault(x => x.Codigo == careerCode);
            if (career == null)
            {
                throw new System.Exception("No se encontró la carrera con código " + careerCode);
            }
            return career;
        }

        private PeriodoAcademico FindTerm(AbetEntities entities, int termId)
        {
            return entities.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == termId);
        }

        private void UpdateCertificatedStudent(Alumno alumno, TituladosFileModel row, AbetEntities entities, int termId)
        {
            alumno.AnioTitulacion = FindTerm(entities, termId).CicloAcademico;
            alumno.Carrera = GetCareer(entities, row.estudio);
            entities.Entry(alumno).State = EntityState.Modified;
        }

        private CargaAlumnoHistorico CreateAlumnoHistorico(AbetEntities entities, LogCarga logCarga, Alumno alumno)
        {
            return new CargaAlumnoHistorico
            {
                IdAlumno = alumno.IdAlumno,
                IdLogCarga = logCarga.IdLogCarga,
                AnioEgreso = alumno.AnioEgreso,
                AnioTitulacion = alumno.AnioTitulacion,
                IdCarreraEgreso = alumno.IdCarreraEgreso
            };
        }
    }
}
