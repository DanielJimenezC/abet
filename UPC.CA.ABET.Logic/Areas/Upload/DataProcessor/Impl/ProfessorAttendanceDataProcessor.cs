﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Globalization;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class ProfessorAttendanceDataProcessor : IDataProcessor<AsistenciaFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<AsistenciaFileModel>("Hoja1").Select(x => x).Where(x => x.NombreUsuario != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => x.NombreUsuario).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.ASISTENCIA_DOCENTES, 
                    termId, 
                    userId);
                List<AsistenciaDocente> listaAsistencia = new List<AsistenciaDocente>();

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    var nombreCompleto = row.NombreUsuario;
                    var nombrePartido = nombreCompleto.Split(',');
                    var apellidos = nombrePartido[0].Trim();
                    var nombres = nombrePartido[1].Trim();
                    var docente = FindProfessor(entities, row, apellidos, nombres);

                    if (docente != null)
                    {
                        var asistenciaDocente = AttendanceExistsInList(listaAsistencia, docente);

                        if (asistenciaDocente == null)
                        {
                            listaAsistencia.Add(CreateProfessorAttendance(docente, termId, row, logCarga));
                        }
                        else
                        {
                            SumAttendanceStatistics(asistenciaDocente, row, entities);
                        }
                    }
                }
                AddAttendanceToEntities(listaAsistencia, entities);
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }
        
        private void AddAttendanceToEntities(List<AsistenciaDocente> listaAsistencia, AbetEntities entities)
        {
            foreach (var asistencia in listaAsistencia)
            {
                entities.AsistenciaDocente.Add(asistencia);
            }
        }

        private void SumAttendanceStatistics(AsistenciaDocente asistenciaDocente, AsistenciaFileModel row, AbetEntities entities)
        {
            asistenciaDocente.TotalClases = asistenciaDocente.TotalClases + Int32.Parse(row.TotalClases);
            asistenciaDocente.Lista = asistenciaDocente.Lista + Int32.Parse(row.Lista);
            asistenciaDocente.ClasesInasistidas = asistenciaDocente.ClasesInasistidas + Int32.Parse(row.ClasesInasistidas);
            asistenciaDocente.RecuperacionesAsistidas = asistenciaDocente.RecuperacionesAsistidas + Int32.Parse(row.RecuperacionesAsistidas);
            asistenciaDocente.RecuperacionesInasistidas = asistenciaDocente.RecuperacionesInasistidas + Int32.Parse(row.RecuperacionesInasistidas);
            asistenciaDocente.AdicionalesAsistidas = asistenciaDocente.AdicionalesAsistidas + Int32.Parse(row.AdicionalesAsistidas);
            asistenciaDocente.AdicionalesInasistidas = asistenciaDocente.AdicionalesInasistidas + Int32.Parse(row.AdicionalesInasistidas);
            asistenciaDocente.AdelantosAsistidos = asistenciaDocente.AdelantosAsistidos + Int32.Parse(row.AdelantosAsistidos);
            asistenciaDocente.AdelantosInasistidos = asistenciaDocente.AdelantosInasistidos + Int32.Parse(row.AdelantosInasistidos);
        }

        private AsistenciaDocente AttendanceExistsInList(List<AsistenciaDocente> listaAsistencia, Docente docente)
        {
            foreach (var asistencia in listaAsistencia)
            {
                if (asistencia.Docente.IdDocente == docente.IdDocente)
                    return asistencia;
            }

            return null;
        }

        private Docente FindProfessor(AbetEntities entities, AsistenciaFileModel row, string apellidos, string nombres)
        {
            var fullName = apellidos + " " + nombres;

            foreach (var docente in entities.Docente)
            {
                string bdFullName = docente.Apellidos + " " + docente.Nombres;

                if (CompareIgnoringAccents(fullName, bdFullName))
                {
                    return docente;
                }
            }
            return null;
        }

        private bool CompareIgnoringAccents(string nombreCompleto, string nombreCompletoBd)
        {
            return string.Compare(nombreCompleto, nombreCompletoBd, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace) == 0;
        }

        private AsistenciaDocente CreateProfessorAttendance(Docente docente, int termId, AsistenciaFileModel row, LogCarga logCarga)
        {
            //VALIDAR SI TERMID RECIBE SUBMODALIDAD
            return new AsistenciaDocente
            {
                Docente = docente,
                IdSubModalidadPeriodoAcademicoModulo = termId,
                TotalClases = int.Parse(row.TotalClases),
                Total = decimal.Parse(row.Total),
                Lista = int.Parse(row.Lista),
                ClasesInasistidas = int.Parse(row.ClasesInasistidas),
                RecuperacionesAsistidas = int.Parse(row.RecuperacionesAsistidas),
                RecuperacionesInasistidas = int.Parse(row.RecuperacionesInasistidas),
                AdicionalesAsistidas = int.Parse(row.AdicionalesAsistidas),
                AdicionalesInasistidas = int.Parse(row.AdicionalesInasistidas),
                AdelantosAsistidos = int.Parse(row.AdelantosAsistidos),
                AdelantosInasistidos = int.Parse(row.AdelantosInasistidos),
                LogCarga = logCarga
            };
        }

    }
}
