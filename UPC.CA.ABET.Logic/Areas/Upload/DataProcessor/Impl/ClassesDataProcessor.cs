﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqToExcel;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using System.Globalization;
using System.Data.Entity;
using System.Data.SqlClient;
using ClosedXML.Excel;
using System.Runtime.InteropServices;
using System.Data.Entity.Core.EntityClient;
using System.Windows.Forms;
using System.IO;
using System.Web;
using Newtonsoft.Json;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class ClassesDataProcessor : IDataProcessor<SeccionFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        string path;

        XLWorkbook wb = new XLWorkbook();
      

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            
            string error = "Cabeceras Correctas: "+ ConstantHelpers.UPLOAD.SECCION.CODIGO_CURSO+", "+ ConstantHelpers.UPLOAD.SECCION.SECCIONES+", "+ ConstantHelpers.UPLOAD.SECCION.DOCENTE+", "
                + ConstantHelpers.UPLOAD.SECCION.LOCAL + "- Cabeceras Erróneas: ";
            bool errores = false;
            var cabeceras = excel.GetColumnNames(currentSheetName);

            foreach(var obj in cabeceras)
            {
                
                switch (obj.ToString())
                {
                    case ConstantHelpers.UPLOAD.SECCION.CODIGO_CURSO:
                        break;
                    case ConstantHelpers.UPLOAD.SECCION.SECCIONES:
                        break;
                    case ConstantHelpers.UPLOAD.SECCION.DOCENTE :
                        break;
                    case ConstantHelpers.UPLOAD.SECCION.LOCAL:
                        break;
                    default:
                        {
                            errores = true;
                           error = error + " " + obj.ToString();
                        }
                        break;

                }

            }


            if (errores == true)
            {
                throw new InvalidSheetNameException(error);

            }

            this.path = path;
        }
        protected override void ValidateData()
        {
         
        }
        public XLWorkbook GetExcel()
        {
            return wb;
        }
        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {

            bool Errores = false;

            var builder = new SqlConnectionStringBuilder(entities.Database.Connection.ConnectionString);

            using (SqlConnection cn = new SqlConnection(builder.ConnectionString))
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();

                    using (SqlCommand comm = new SqlCommand("Usp_Carga_Seccion", cn)
                    { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        comm.CommandTimeout = 0;


                        comm.Parameters.AddRange(new[]
                            {
                                new SqlParameter("@IdSubModalidadPeriodoAcademico", termId),
                                new SqlParameter("@UsuarioId", userId),
                                new SqlParameter("@TipoCarga", ConstantHelpers.UPLOAD_TYPE.SECCION),
                                new SqlParameter("@path", path),
                            }
                        );

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            using (DataSet ds = new DataSet())
                            {
                                da.SelectCommand = comm;
                                DataSet aux = new DataSet();
                                da.Fill(aux);
                                DataTable dt = new DataTable("Resultado");
                                dt.Columns.Add(new DataColumn("Fila", typeof(string)));
                                dt.Columns.Add(new DataColumn("Descripción Error", typeof(string)));
                                int rows = 0;
                                foreach (DataTable table in aux.Tables)
                                {
                                    foreach (DataRow row in table.Rows)
                                    {

                                        object id = row[0];
                                        object item = row[1];
                                        if (!item.Equals("OK"))
                                        {
                                            Errores = true;
                                            string item_result = JsonConvert.SerializeObject(item);
                                            int id_result = Convert.ToInt32(id);

                                            item_result = item_result.Substring(1);
                                            item_result = item_result.Remove(item_result.Length - 1);
                                            string[] result_parse = item_result.Split('|');
                                            foreach(string result in result_parse)
                                            {
                                                dt.Rows.Add(id_result+1, result);
                                                rows++;
                                            }
                                        }
                                        
                                    }
                                }


                                if (ds != null && Errores==true)
                                {
                                    if (rows != 0)
                                    {
                                        ds.Tables.Add(dt);
                                        wb.Worksheets.Add(ds);
                                        
                                        using (var ms = new MemoryStream())
                                        {
                                            wb.SaveAs(ms);
                                            var workbookBytes = new byte[0];
                                            workbookBytes = ms.ToArray();



                                            HttpContext context = HttpContext.Current;

                                           

                                                                    
                                            string[] dirs = Directory.GetFiles(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS);

                                            int cantidad = dirs.Length;
                                       
                                            cantidad = cantidad + 1;

                                            context.Session["DownloadExcelError"] = false;


                                            wb.SaveAs(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + "Error" + cantidad + ".xlsx");


                                            throw new InvalidSheetNameException("Hubo un error en su Excel. Por favor revise los errores en el Excel generado.");

                                        }


                                      
                                    }
                                    else
                                    {
                                        cn.Close();
                                        cn.Dispose();
                                        da.Dispose();
                                        wb.Dispose();
                                    }
                                   
                                }
                            }


                        }
                    }

                }
            }
        }


        public string  Path()
        {

            return path;
        }
    }
}