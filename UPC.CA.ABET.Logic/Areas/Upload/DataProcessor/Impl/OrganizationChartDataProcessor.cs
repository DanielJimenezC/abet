﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Web;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class OrganizationChartDataProcessor : IDataProcessor<OrganigramaFileModel>
    {
        private string _correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        private Dictionary<string, UnidadAcademica> _addedAcademicUnits;
        private LogCarga _logCarga;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != _correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + _correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<OrganigramaFileModel>("Hoja1").Select(x => x).Where(x => x.UnidadAcademica != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => x.Id).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            HttpContext context = HttpContext.Current;

            int IdEscuela = context.Session.GetEscuelaId();


            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                _logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.ORGANIGRAMA, 
                    termId, 
                    userId);
                _addedAcademicUnits = new Dictionary<string, UnidadAcademica>();
                    
                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    UnidadAcademica academicUnit = CreateAcademicUnit(entities, termId, row);
                    List<SedeUnidadAcademica> campusAcademicUnits = CreateCampusAcademicUnits(entities, row, academicUnit);
                    _addedAcademicUnits.Add(row.Id, academicUnit);
                    entities.UnidadAcademica.Add(academicUnit);
                    entities.SedeUnidadAcademica.AddRange(campusAcademicUnits);
                }

                entities.SaveChanges();
                dbContextTransaction.Commit();

                entities.Usp_DaraccesosUsuariosSistemas(termId, IdEscuela);


            }
        }

        #region Create academic unit

        private UnidadAcademica CreateAcademicUnit(AbetEntities entities, int termId, OrganigramaFileModel row)
        {
            var escuela = FindEscuela(entities, row);
            var ua = new UnidadAcademica
            {
                Nivel = int.Parse(row.Nivel),
                Tipo = row.Tipo,
                NombreEspanol = row.UnidadAcademica,
                NombreIngles = row.NombreIngles,
                CarreraPeriodoAcademico = GetCareerAcademicPeriod(entities, row, termId),
                CursoPeriodoAcademico = GetSubjectAcademicPeriod(entities, row, termId),
                IdSubModalidadPeriodoAcademico = termId,
                UnidadAcademica2 = GetFather(row),
                IdLogCarga = _logCarga.IdLogCarga
            };

            if (escuela != null)
                ua.IdEscuela = escuela.IdEscuela;

            return ua;
        }

        private CarreraPeriodoAcademico GetCareerAcademicPeriod(AbetEntities context, OrganigramaFileModel row, int termId)
        {
            return IsCareer(row) ? FindCareerAcademicPeriod(context, row, termId) : null;
        }

        private CarreraPeriodoAcademico FindCareerAcademicPeriod(AbetEntities context, OrganigramaFileModel row, int termId)
        {
            var careerAcademicPeriod = context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == termId && x.Carrera.Codigo == row.Carrera);
            if (careerAcademicPeriod == null)
            {
                throw new System.Exception("La carrera con código " + row.Carrera + "No existe en el ciclo ciclo seleccionado");
            }
            return careerAcademicPeriod;
        }

        private bool IsCareer(OrganigramaFileModel row)
        {
            return row.Carrera != null && row.Carrera != "";
        }

        private CursoPeriodoAcademico GetSubjectAcademicPeriod(AbetEntities context, OrganigramaFileModel row, int termId)
        {
            return IsSubject(row) ? FindSubject(context, row, termId) : null;
        }

        private static CursoPeriodoAcademico FindSubject(AbetEntities context, OrganigramaFileModel row, int termId)
        {
            var subject = context.CursoPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == termId && x.Curso.Codigo == row.Curso);
            if (subject == null)
            {
                throw new System.Exception("El curso con código " + row.Curso + " no existe");
            }
            return subject;
        }

        private bool IsSubject(OrganigramaFileModel row)
        {
            return row.Curso != null && row.Curso != "";
        }

        private UnidadAcademica GetFather(OrganigramaFileModel row)
        {
            return HasFather(row) ? _addedAcademicUnits[row.Padre] : null;
        }

        private bool HasFather(OrganigramaFileModel row)
        {
            return row.Padre != null && row.Padre != "";
        }

        private Escuela FindEscuela(AbetEntities context,OrganigramaFileModel row)
        {
            if(row.Escuela != null)
            {
                var escuela = context.Escuela.FirstOrDefault(x => x.Codigo == row.Escuela);

                if (escuela == null)
                {
                    throw new System.Exception("La escuela con código " + row.Escuela + " no existe");
                }

                return escuela;
            }

            return null;
        }
        #endregion

        #region Create campus academic units

        private List<SedeUnidadAcademica> CreateCampusAcademicUnits(AbetEntities context, OrganigramaFileModel row, UnidadAcademica academicUnit)
        {
            var campusAcademicUnits = new List<SedeUnidadAcademica>();

            foreach (var code in GetCampusCodes(row))
            {
                Sede campus = GetCampusByCode(context, code);
                SedeUnidadAcademica campusAcademicUnit = CreateCampusAcademicUnit(context, campus, academicUnit);
                Docente professor = GetProfessor(context, row);
                campusAcademicUnit.UnidadAcademicaResponsable.Add(CreateAcademicUnitResponsible(context, professor));
                campusAcademicUnits.Add(campusAcademicUnit);
            }
            return campusAcademicUnits;
        }

        private Sede GetCampusByCode(AbetEntities context, string code)
        {
            var campus = context.Sede.FirstOrDefault(x => x.Codigo == code);
            if (campus == null)
            {
                throw new System.Exception("La sede con código " + code + "no existe.");
            }
            return campus;
        }

        private SedeUnidadAcademica CreateCampusAcademicUnit(AbetEntities context, Sede campus, UnidadAcademica academicUnit)
        {
            return new SedeUnidadAcademica
            {
                Sede = campus,
                UnidadAcademica = academicUnit,
                LogCarga = _logCarga
            };
        }

        private UnidadAcademicaResponsable CreateAcademicUnitResponsible(AbetEntities context, Docente professor)
        {
            return new UnidadAcademicaResponsable
            {
                Docente = professor,
                EsActivo = true,
                FechaCreacion = DateTime.Now,
                LogCarga = _logCarga
            };
        }

        private Docente GetProfessor (AbetEntities context, OrganigramaFileModel row)
        {
            var professor = context.Docente.FirstOrDefault(x => x.Codigo == row.NombreUsuario);
            if (professor == null)
            {
                throw new System.Exception("El docente con código " + row.NombreUsuario + " no existe");
            }
            return professor;
        }

        private static string[] GetCampusCodes(OrganigramaFileModel row)
        {
            var sedes = row.Sede;
            string[] codigosSedes = sedes.Split(null);
            return codigosSedes;
        }
        #endregion
    }
}



