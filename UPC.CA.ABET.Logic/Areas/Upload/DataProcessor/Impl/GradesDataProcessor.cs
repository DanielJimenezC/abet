﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;
using System.Data.SqlClient;
using ClosedXML.Excel;
using System.Data;
using System.Web;
using System.IO;
using Newtonsoft.Json;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class GradesDataProcessor : IDataProcessor<EvaluacionesFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        string path;
        XLWorkbook wb = new XLWorkbook();

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }

            string error = "Cabeceras Correctas: " + ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_ALUMNO + ", " +
               ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_CURSO + ", " +
              ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_SECCION + ", "+
              ConstantHelpers.UPLOAD.EVALUACIONES.NOTA +
              " - Cabeceras Erróneas: ";

            bool errores = false;
            var cabeceras = excel.GetColumnNames(currentSheetName);

            foreach (var obj in cabeceras)
            {

                switch (obj.ToString())
                {
                    case ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_ALUMNO:
                        break;
                    case ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_CURSO:
                        break;
                    case ConstantHelpers.UPLOAD.EVALUACIONES.CODIGO_SECCION:
                        break;
                    case ConstantHelpers.UPLOAD.EVALUACIONES.NOTA:
                        break;
                    default:
                        {
                            errores = true;
                            error = error + " " + obj.ToString();
                        }
                        break;

                }

            }


            if (errores == true)
            {
                throw new InvalidSheetNameException(error);

            }



            this.path = path;

        }
        protected override void ValidateData()
        {
      
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {

            bool Errores = false;
            var builder = new SqlConnectionStringBuilder(entities.Database.Connection.ConnectionString);

            using (SqlConnection cn = new SqlConnection(builder.ConnectionString))
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();

                    using (SqlCommand comm = new SqlCommand("Usp_Carga_Evaluacion_RC", cn)
                    { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        comm.CommandTimeout = 0;


                        comm.Parameters.AddRange(new[]
                            {
                                new SqlParameter("@IdSubModalidadPeriodoAcademico", termId),
                                new SqlParameter("@UsuarioId", userId),
                                new SqlParameter("@TipoCarga", ConstantHelpers.UPLOAD_TYPE.EVALUACIONES),
                                new SqlParameter("@path", path),
                            }
                        );

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            using (DataSet ds = new DataSet())
                            {
                                da.SelectCommand = comm;
                                DataSet aux = new DataSet();
                                da.Fill(aux);
                                DataTable dt = new DataTable("Resultado");
                                dt.Columns.Add(new DataColumn("Fila", typeof(string)));
                                dt.Columns.Add(new DataColumn("Descripción Error", typeof(string)));
                                int rows = 0;
                                foreach (DataTable table in aux.Tables)
                                {
                                    foreach (DataRow row in table.Rows)
                                    {
                                        object id = row[0];
                                        object item = row[1];
                                        if (!item.Equals("OK"))
                                        {
                                            Errores = true;

                                            string item_result = JsonConvert.SerializeObject(item);
                                            int id_result = Convert.ToInt32(id);

                                            item_result = item_result.Substring(1);
                                            item_result = item_result.Remove(item_result.Length - 1);
                                            string[] result_parse = item_result.Split('|');
                                            foreach (string result in result_parse)
                                            {
                                                dt.Rows.Add(id_result + 1, result);
                                                rows++;
                                            }
                                        }

                                    }
                                }
                                if (ds != null && Errores == true)
                                {
                                    if (rows != 0)
                                    {
                                        ds.Tables.Add(dt);
                                        wb.Worksheets.Add(ds);

                                        using (var ms = new MemoryStream())
                                        {
                                            wb.SaveAs(ms);
                                            var workbookBytes = new byte[0];
                                            workbookBytes = ms.ToArray();
                                            HttpContext context = HttpContext.Current;


                                            string[] dirs = Directory.GetFiles(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS);

                                            int cantidad = dirs.Length;

                                            cantidad = cantidad + 1;

                                            context.Session["DownloadExcelError"] = false;


                                            wb.SaveAs(ConstantHelpers.TEMP_PATH_LOADS_RUBRICS + "Error" + cantidad + ".xlsx");

                                            throw new InvalidSheetNameException("Hubo un error en su Excel. Por favor revise los errores en el Excel generado.");

                                        }

                                    }
                                    else
                                    {
                                        cn.Close();
                                        cn.Dispose();
                                        da.Dispose();
                                        wb.Dispose();
                                    }

                                }
                            }


                        }
                    }

                }
            }
        }

        private string ParseGrade(string grade)
        {
           
            return grade;
        }

        
    }
}
