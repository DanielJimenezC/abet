﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class EnrolledStudentsDataProcessor : IDataProcessor<AlumnosMatriculadosFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        List<Alumno> listaAlumnosNuevos;
        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<AlumnosMatriculadosFileModel>("Hoja1").Select(x => x).Where(x => x.CodigoDeAlumno != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = Data.GroupBy(x => x.CodigoDeAlumno).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            listaAlumnosNuevos = new List<Alumno>();

            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                var logCarga = LogCargaBuilder.GetLogCarga(
                    entities, 
                    ConstantHelpers.UPLOAD_TYPE.ALUMNOS_MATRICULADOS, 
                    termId, 
                    userId);

                foreach (var row in Data)
                {
                    Sanitizer.SanitizeObjectStrings(row);
                    var alumno = FindAlumnoByCode(entities, row);

                    if (alumno == null)
                    {
                        alumno = CreateStudent(entities, row, logCarga);
                    }

                    var carrera = FindCarreraByCodigo(entities, row);

                    var sede = FindSedeByCodigo(entities, row);

                    entities.AlumnoMatriculado.Add(CreateEnrolledStudent(row, logCarga, alumno, termId, carrera,sede));
                }
                entities.SaveChanges();
                dbContextTransaction.Commit();
            }
        }

        private Alumno FindAlumnoByCode(AbetEntities entities, AlumnosMatriculadosFileModel row)
        {
            return entities.Alumno.FirstOrDefault(x => x.Codigo == row.CodigoDeAlumno) ?? 
                listaAlumnosNuevos.FirstOrDefault(x => x.Codigo == row.CodigoDeAlumno);
        }

        private Alumno CreateStudent(AbetEntities entities, AlumnosMatriculadosFileModel row, LogCarga logCarga)
        {
            var nombreCompletoAlumno = row.NombreCompleto;
            var nombrePartidoAlumno = nombreCompletoAlumno.Split(',');
            var apellidosAlumno = nombrePartidoAlumno[0].Trim();
            var nombresAlumno = nombrePartidoAlumno[1].Trim();
            var idcarrera = entities.Carrera.FirstOrDefault(x => x.Codigo == row.Carrera).IdCarrera;

            var alumno =  new Alumno
            {
                Nombres = nombresAlumno,
                Apellidos = apellidosAlumno,
                Codigo = row.CodigoDeAlumno,
                LogCarga = logCarga,
                IdCarreraEgreso = idcarrera
            };

            listaAlumnosNuevos.Add(alumno);
            return alumno;
        }

        private AlumnoMatriculado CreateEnrolledStudent(AlumnosMatriculadosFileModel row, LogCarga logCarga, Alumno alumno, int termId, Carrera carrera, Sede sede)
        {
            return new AlumnoMatriculado
            {
                Alumno = alumno,
                IdSubModalidadPeriodoAcademico = termId,
                EstadoMatricula = row.EstadoMatricula,
                Carrera = carrera,
                LogCarga = logCarga,
                Sede = sede
            };
        }

        private Carrera FindCarreraByCodigo(AbetEntities entities, AlumnosMatriculadosFileModel row)
        {
            var carrera =  entities.Carrera.FirstOrDefault(x => x.Codigo == row.Carrera);

            if(carrera == null)
            {
                throw new System.Exception("No se encontró la carrera con código " + row.Carrera);
            }

            return carrera;
        }


        private Sede FindSedeByCodigo(AbetEntities entities, AlumnosMatriculadosFileModel row)
        {
            var Sede = entities.Sede.FirstOrDefault(x => x.Codigo == row.Sede);

            if (Sede == null)
            {
                throw new System.Exception("No se encontró la sede con código " + row.Sede);
            }

            return Sede;
        }

    }
}
