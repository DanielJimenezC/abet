﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class ProfessorDataProcessor : IDataProcessor<DocentesFileModel>
    {
        private string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        private List<Docente> _addedProfessors; 


        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException(
                    "El nombre de la hoja debe ser \"" +  correctSheetName + 
                    "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<DocentesFileModel>("Hoja1").Where(x => x.NombreDeUsuario != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = RemoveDuplicates();
        }

        private List<DocentesFileModel> RemoveDuplicates()
        {
            return Data.GroupBy(x => x.NombreDeUsuario).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            _addedProfessors = new List<Docente>();

            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                try
                {
                    var logCarga = LogCargaBuilder.GetLogCarga(  //TODO Analizar donde mandar Modalidad y SubModalidad
                        entities, 
                        ConstantHelpers.UPLOAD_TYPE.DOCENTE, 
                        termId, 
                        userId);
                    entities.LogCarga.Add(logCarga);

                    foreach (var row in Data)
                    {
                        Sanitizer.SanitizeObjectStrings(row);

                        if (professorWithCodeDoesNotExist(entities, row.NombreDeUsuario) && correctFullNameFormat(row.NombreCompletoDelDocente))
                        {
                            var professor = CreateProfessor(row, logCarga);
                            entities.Docente.Add(professor);
                            _addedProfessors.Add(professor);
                        }
                    }

                    entities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (System.Exception)
                {
                    throw;
                }
            }
        }

        public bool professorWithCodeDoesNotExist(AbetEntities entities, string code)
        {
            return (entities.Docente.FirstOrDefault(x => x.Codigo == code) ?? 
                _addedProfessors.FirstOrDefault(x => x.Codigo == code)) == null;
        }

        public bool correctFullNameFormat(string fullName)
        {
            var fullNameSplitted = fullName.Split(',');
            return fullNameSplitted.Length == 2;
        }

        private static Docente CreateProfessor(DocentesFileModel row, LogCarga logCarga)
        {
            var splitName = row.NombreCompletoDelDocente.Split(',');

            return new Docente
            {
                Codigo = row.NombreDeUsuario,
                Apellidos = splitName[0].Trim(),
                Nombres = splitName[1].Trim(),
                EsAdministrativo = false,
                LogCarga = logCarga
            };
        }
    }
}
