﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;
using System.Data.Entity;
using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Exception;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl
{
    public class OutcomesDataProcessor : IDataProcessor<OutcomesFileModel>
    {
        string correctSheetName = ConstantHelpers.UPLOAD.ERRORES.NOMBRE_HOJA;
        private List<Outcome> _addedOutcomes;

        protected override void LoadDataFromFile(string path)
        {
            var excel = new ExcelQueryFactory(path);
            var currentSheetName = excel.GetWorksheetNames().FirstOrDefault();
            if (currentSheetName != correctSheetName)
            {
                throw new InvalidSheetNameException("El nombre de la hoja debe ser \"" + correctSheetName + "\", actualmente es \"" + currentSheetName + "\"");
            }
            Data = excel.Worksheet<OutcomesFileModel>("Hoja1").Select(x => x).Where(x => x.CodigoOutcome != "").ToList();
        }

        protected override void ValidateData()
        {
            Data = RemoveDuplicates();
        }

        private List<OutcomesFileModel> RemoveDuplicates()
        {
            return Data.GroupBy(x => new { x.CodigoOutcome, x.Comision }).Select(x => x.First()).ToList();
        }

        protected override void SaveData(AbetEntities entities, int termId, int userId)
        {
            _addedOutcomes = new List<Outcome>();
            using (var dbContextTransaction = entities.Database.BeginTransaction())
            {
                try
                {
                    var logCarga = LogCargaBuilder.GetLogCarga(
                        entities, 
                        ConstantHelpers.UPLOAD_TYPE.OUTCOME, 
                        termId, 
                        userId);

                    foreach (var row in Data)
                    {
                        Outcome outcome = CreateOrGetExistingOutcome(entities, logCarga, row);
                        Comision commission = GetExistingCommission(entities, termId, row);

                        bool existeOutcomeComision = ExisteOutcomeComision(entities, termId, outcome, commission);

                        if (!existeOutcomeComision)
                        {
                            OutcomeComision outcomeComision = CreateOutcomeCommission(termId, logCarga, outcome, commission);
                            entities.OutcomeComision.Add(outcomeComision);
                        }
                    }

                    entities.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (System.Exception)
                {
                    throw;
                }
            }
        }

        private OutcomeComision CreateOutcomeCommission(int termId, LogCarga logCarga, Outcome outcome, Comision commission)
        {    //termId = SubModalidadPeriodoAcademico

            return new OutcomeComision
            {
                Outcome = outcome,
                Comision = commission,
                IdSubModalidadPeriodoAcademico = termId,
                LogCarga = logCarga
            };
        }

        private bool ExisteOutcomeComision(AbetEntities entities,int termId, Outcome outcome, Comision comision)
        {
            try
            {
                var existe = entities.OutcomeComision.FirstOrDefault(x => x.IdComision == comision.IdComision && x.IdOutcome == outcome.IdOutcome && x.IdSubModalidadPeriodoAcademico == termId);

                if (existe != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
             
            }
            catch
            {
                return false;
            }
        }

        private Comision GetExistingCommission(AbetEntities entities, int termId, OutcomesFileModel row)
        { //termId = SubModalidadPeriodoAcademico
            var comision = entities.Comision.FirstOrDefault(x => x.Codigo == row.Comision && x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == termId);

            if (comision == null)
            {
                throw new System.Exception(
                    "No se encontró la comisión " + row.Comision + " en el ciclo indicado");
            }

            return comision;
        }

        private Outcome CreateOrGetExistingOutcome(AbetEntities entities, LogCarga logCarga, OutcomesFileModel row)
        {
            Outcome outcome = GetExistingOutcome(entities, row);

            if (outcome == null)
            {
                outcome = CreateOutcome(logCarga, row);
                entities.Entry(outcome).State = EntityState.Added;
                _addedOutcomes.Add(outcome);
            }
            else
            {
                entities.Entry(outcome).State = EntityState.Modified;
            }

            return outcome;
        }

        private Outcome CreateOutcome(LogCarga logCarga, OutcomesFileModel row)
        {
            return new Outcome
            {
                Nombre = row.NombreOutcome,
                NombreIngles = row.NombreOutcomeIngles,
                DescripcionEspanol = row.Descripcion,
                DescripcionIngles = row.DescripcionIngles,
                LogCarga = logCarga
            };
        }

        private  Outcome GetExistingOutcome(AbetEntities entities, OutcomesFileModel row)
        {
            return entities.Outcome.FirstOrDefault(x => x.Nombre == row.NombreOutcome && x.DescripcionEspanol == row.Descripcion && x.OutcomeComision.Any(y => y.Comision.Codigo == row.Comision)) ?? _addedOutcomes.FirstOrDefault(x => x.Nombre == row.NombreOutcome && x.DescripcionEspanol == row.Descripcion && x.OutcomeComision.Any(y => y.Comision.Codigo == row.Comision));
        }
    }
}

