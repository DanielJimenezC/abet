﻿using UPC.CA.ABET.Logic.Areas.Upload.DataProcessor.Impl;
using UPC.CA.ABET.Models.Areas.Upload.BulkInsertModel;
using UPC.CA.ABET.MODELS.Areas.Upload.BulkInsertModel;

namespace UPC.CA.ABET.Logic.Areas.Upload.DataProcessor
{
    public class DataProcessorFactory
    {

        public DataProcessor<OutcomesFileModel> GetOutcomeReader()
        {
            return new DataProcessor<OutcomesFileModel>(new OutcomesDataProcessor());
        }
        public DataProcessor<DocentesFileModel> GetProfessorReader()
        {
            return new DataProcessor<DocentesFileModel>(new ProfessorDataProcessor());
        }

        public DataProcessor<CurriculaFileModel> GetCurriculumReader()
        {
            return new DataProcessor<CurriculaFileModel>(new CurriculumDataProcessor());
        }

        public DataProcessor<OrganigramaFileModel> GetOrganizationChartReader()
        {
            return new DataProcessor<OrganigramaFileModel>(new OrganizationChartDataProcessor());
        }

        public DataProcessor<AlumnosMatriculadosFileModel> GetEnrolledStudensReader()
        {
            return new DataProcessor<AlumnosMatriculadosFileModel>(new EnrolledStudentsDataProcessor());
        }
        public DataProcessor<SeccionFileModel> GetClassesReader()
        {
            return new DataProcessor<SeccionFileModel>(new ClassesDataProcessor());
        }
        public DataProcessor<DelegadosFileModel> GetDelegatesReader()
        {
            return new DataProcessor<DelegadosFileModel>(new DelegatesDataProcessor());
        }
        public DataProcessor<AlumnosSeccionFileModel> GetStudentsPerClassReader()
        {
            return new DataProcessor<AlumnosSeccionFileModel>(new StudentsPerClassDataProcessor());
        }

        public DataProcessor<AttritionFileModel> GetAttritionReader()
        {
            return new DataProcessor<AttritionFileModel>(new AttritionDataProcessor());
        }

        public DataProcessor<CohorteFileModel> GetCohortReader()
        {
            return new DataProcessor<CohorteFileModel>(new CohortDataProcessor());
        }

        public DataProcessor<AsistenciaFileModel> GetProffessorAttendanceReader()
        {
            return new DataProcessor<AsistenciaFileModel>(new ProfessorAttendanceDataProcessor());
        }

        public DataProcessor<EvaluacionesFileModel> GetGradesReader()
        {
            return new DataProcessor<EvaluacionesFileModel>(new GradesDataProcessor());
        }

        public DataProcessor<IndicadorCohorteFileModel> GetCohortIndicatorReader()
        {
            return new DataProcessor<IndicadorCohorteFileModel>(new CohortIndicatorDataProcessor());
        }

        public DataProcessor<PuntualidadFileModel> GetProfessorPunctualityReader()
        {
            return new DataProcessor<PuntualidadFileModel>(new ProfessorPunctualityDataProcessor());
        }

        public DataProcessor<TituladosFileModel> GetCertificatedStudentsReader()
        {
            return new DataProcessor<TituladosFileModel>(new CertificatedStudentsDataProcessor());
        }
    }
}