﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Upload.DataDeleter;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Upload.List
{
    public class ListUploadsInteractor
    {
        private UploadRepository _repository;

        public ListUploadsInteractor()
        {
            _repository = new UploadRepository();
        }

        public List<LogCarga> ListAll()
        {
            return _repository.GetSortedByDescendingId();
        }
    }
}
