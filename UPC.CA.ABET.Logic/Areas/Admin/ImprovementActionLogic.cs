﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class ImprovementActionLogic
    {
        private readonly AbetEntities abetEntities;
        public ImprovementActionLogic()
        {
            abetEntities = new AbetEntities();
        }

        public AccionMejora FindImprovementActionById(int idAccionMejora)
        {
            return abetEntities.AccionMejora.FirstOrDefault(x => x.IdAccionMejora == idAccionMejora);
        }
    }
}
