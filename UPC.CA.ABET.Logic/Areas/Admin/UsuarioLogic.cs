﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class UsuarioLogic
    {
        private AbetEntities context { get; set; }
        public UsuarioLogic(AbetEntities context)
        {
            this.context = context;
        }

        public Boolean CreateAccessRoles(string codigoDocente, List<JsonEntity> lstEscuelas)
        {
            var result = false;
            var codigo = codigoDocente.ToUpper();
            var docente = context.Docente.FirstOrDefault(x => x.Codigo.ToUpper() == codigo.ToUpper());
            if (docente == null)
            {
                return result;
            }

            var periodoAcademico = context.PeriodoAcademico.FirstOrDefault(x => x.Estado == ConstantHelpers.ESTADO.ACTIVO);
            var fechaActual = DateTime.Now;

            var usuario = context.Usuario.FirstOrDefault(x => x.Codigo.ToUpper() == codigo);
            if (usuario == null)
            {
                var security = new SecurityLogic();

                usuario = new Usuario();

                usuario.IdDocente = docente.IdDocente;
                usuario.FechaCreacion = fechaActual;
                usuario.Estado = ConstantHelpers.ESTADO.ACTIVO;
                usuario.Password = security.MD5Hash(ConstantHelpers.DEFAULT_PASSWORD);
                usuario.Codigo = docente.Codigo.Trim().ToLower();
                usuario.Nombres = docente.Nombres;
                usuario.Apellidos = docente.Apellidos;
                usuario.FechaAlta = fechaActual;
                usuario.IdDocente = docente.IdDocente;
                //usuario.NombreCompleto = docente.Nombres + " " + docente.Apellidos;

                context.Usuario.Add(usuario);
                context.SaveChanges();
            }

            var qSedeUnidadAcademica = context.SedeUnidadAcademica.Include(x => x.UnidadAcademica).Where(x => x.UnidadAcademica.IdSubModalidadPeriodoAcademico == periodoAcademico.IdPeriodoAcademico).AsQueryable();
            var qUnidadAcademicaResponsable = context.UnidadAcademicaResponsable.Include(x => x.SedeUnidadAcademica).Where(x => qSedeUnidadAcademica.Any(q => q.IdSedeUnidadAcademica == x.IdSedeUnidadAcademica) && x.IdDocente == docente.IdDocente && (x.EsActivo.HasValue ? x.EsActivo.Value : false)).AsQueryable();

            var lstUnidadAcademicaDocente = qUnidadAcademicaResponsable.Select(x => x.SedeUnidadAcademica.UnidadAcademica).GroupBy(x => x.IdUnidadAcademica).Select(x => x.FirstOrDefault()).ToList();

            if (lstUnidadAcademicaDocente != null)
            {
                try
                {
                    var rolDocente = context.Rol.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ROL.DOCENTE);
                    foreach (var unidadAcademica in lstUnidadAcademicaDocente)
                    {
                        var lstRoles = new List<Rol>();
                        lstRoles.Add(rolDocente);
                        switch (unidadAcademica.Nivel)
                        {
                            case ConstantHelpers.NIVEL_UNIDAD_ACADEMICA.ESCUELA:
                                var rolDirectorCarrera = context.Rol.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ROL.DIRECTOR_CARRERA);
                                lstRoles.Add(rolDirectorCarrera);
                                break;
                            case ConstantHelpers.NIVEL_UNIDAD_ACADEMICA.AREA:
                                var rolCoordinadorCarrera = context.Rol.FirstOrDefault(x => x.Acronimo == ConstantHelpers.ROL.COORDINADOR_CARRERA);
                                lstRoles.Add(rolCoordinadorCarrera);
                                break;
                            default:
                                break;
                        }
                        foreach (var rol in lstRoles)
                        {
                            var rolUsuario = context.RolUsuario.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == periodoAcademico.IdPeriodoAcademico && x.IdRol == rol.IdRol && x.IdUsuario == usuario.IdUsuario && x.IdEscuela == unidadAcademica.IdEscuela);
                            if (rolUsuario == null)
                            {
                                rolUsuario = new RolUsuario();
                                rolUsuario.IdSubModalidadPeriodoAcademico = periodoAcademico.IdPeriodoAcademico;
                                rolUsuario.IdRol = rol.IdRol;
                                rolUsuario.IdUsuario = usuario.IdUsuario;
                                rolUsuario.IdEscuela = unidadAcademica.IdEscuela;
                                rolUsuario.FechaCreacion = fechaActual;
                                rolUsuario.Estado = ConstantHelpers.ESTADO.ACTIVO;
                                context.RolUsuario.Add(rolUsuario);
                                context.SaveChanges();
                            }
                        }
                    }
                    result = true;
                }
                catch (Exception ex)
                {
                    result = false;
                    //throw ex;
                }
            }
            else if (context.RolUsuario.Any(x => x.IdSubModalidadPeriodoAcademico == periodoAcademico.IdPeriodoAcademico && x.IdUsuario == usuario.IdUsuario))
            {
                result = true;
            }

            return result;
        }
    }
}
