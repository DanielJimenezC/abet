﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class AdminLogic
    {
        private AbetEntities _context;

        public AdminLogic(AbetEntities context)
        {
            _context = context;

        }

        public String SaveImage(HttpPostedFileBase file, HttpServerUtilityBase server, Int32 size = 320)
        {
            var result = String.Empty;
            if (file != null && file.ContentLength != 0)
            {
                var guid = Guid.NewGuid().ToString().Substring(0, 6);
                result = guid + "_" + Path.GetFileName(file.FileName);
                var path = server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                var image = System.Drawing.Image.FromStream(file.InputStream);
                image = image.GetThumbnailImage(size, (size * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                image.Save(Path.Combine(path, result));
            }
            return result;
        }

        public String SaveImage(HttpPostedFileBase file, HttpServerUtilityBase server)
        {
            return SaveImage(file, server, 320);
        }

        public List<Modalidad> GetModalType()
        {
			List<Modalidad> lstmodalidad = new List<Modalidad>();

			var lst = _context.Usp_ListModalities().OrderByDescending(x => x.IdModalidad).ToList();

			foreach (var obj in lst)
			{
				Modalidad m = new Modalidad();

                if (obj.IdModalidad == HttpContext.Current.Session.GetModalidadId() )
                {
                    m.IdModalidad = obj.IdModalidad;
                    m.NombreEspanol = obj.NombreEspanol;
                    m.NombreIngles = obj.NombreIngles;
                    lstmodalidad.Add(m);
                }
			}

			return lstmodalidad;

		}
    }
}
