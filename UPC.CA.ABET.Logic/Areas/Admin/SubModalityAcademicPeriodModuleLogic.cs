﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class SubModalityAcademicPeriodModuleLogic
    {
        private readonly AbetEntities abetEntities;

        public SubModalityAcademicPeriodModuleLogic()
        {
            abetEntities = new AbetEntities();
        }

        public SubModalidadPeriodoAcademicoModulo GetSubModalityAcademicPeriodModule(int subModalityAcademicPeriodModuleId)
        {
            var subModalityAcademicPeriodModule = abetEntities.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == subModalityAcademicPeriodModuleId);
            if (subModalityAcademicPeriodModule == null) return null;
            return subModalityAcademicPeriodModule;
        }
    }
}
