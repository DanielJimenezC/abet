﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class SubModalityAcademicPeriodLogic
    {
        private readonly AbetEntities abetEntities;

        public SubModalityAcademicPeriodLogic()
        {
            abetEntities = new AbetEntities();
        }

        public SubModalidadPeriodoAcademico GetSubModalityAcademicPeriod(int subModalityAcademicPeriodId)
        {
            return abetEntities.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == subModalityAcademicPeriodId);
        }
    }
}
