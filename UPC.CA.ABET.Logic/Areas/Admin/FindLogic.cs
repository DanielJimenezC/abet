﻿using DocumentFormat.OpenXml.InkML;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class FindLogic
    {
        private int? ConstituyenteInstrumentoId { get; set; }
        private int? SubModalidadPeriodoAcademicoId { get; set; }
        private CargarDatosContext ctx { get; set; }

        public FindLogic(CargarDatosContext context, int? constituyenteinstrumentoId, int? submodalidadperiodoacademicoId)
        {
            ConstituyenteInstrumentoId = constituyenteinstrumentoId;
            SubModalidadPeriodoAcademicoId = submodalidadperiodoacademicoId;
            ctx = context;

        }

        public int ObtenerIndiceHallazgo()
        {
            try
            {

                int identificador;

                var builder = new SqlConnectionStringBuilder(ctx.context.Database.Connection.ConnectionString);

                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Usp_GetSiguienteNumeroHallazgoPorInstrumento", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter siguiente = new SqlParameter("@Siguiente", SqlDbType.Int);
                    siguiente.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@IdConstituyenteInstrumento", SqlDbType.Int).Value = ConstituyenteInstrumentoId;
                    cmd.Parameters.Add("@IdSubModalidadPeriodoAcademico", SqlDbType.Int).Value = SubModalidadPeriodoAcademicoId;
                    cmd.Parameters.Add(siguiente);

                    cmd.ExecuteNonQuery();

                    identificador = (Convert.ToInt32(siguiente.Value));
                }

                return identificador;

            }
            catch (Exception ex)
            {
                return 0;
            }
           
        }

        public int ObtenerIndiceAccionMejora()
        {
            try
            {

                int identificador;

                var builder = new SqlConnectionStringBuilder(ctx.context.Database.Connection.ConnectionString);

                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Usp_GetSiguienteNumeroAccionMejoraPorInstrumento", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter siguiente = new SqlParameter("@Siguiente", SqlDbType.Int);
                    siguiente.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@IdConstituyenteInstrumento", SqlDbType.Int).Value = ConstituyenteInstrumentoId;
                    cmd.Parameters.Add("@IdSubModalidadPeriodoAcademico", SqlDbType.Int).Value = SubModalidadPeriodoAcademicoId;
                    cmd.Parameters.Add(siguiente);

                    cmd.ExecuteNonQuery();

                    identificador = (Convert.ToInt32(siguiente.Value));
                }

                return identificador;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

    }
       
}
