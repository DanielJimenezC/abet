﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class SecurityLogic
    {
        private String _IPAddress { get; set; }
        private String _Key { get; set; }

        public SecurityLogic()
        {
            _IPAddress = String.Empty;
            _Key = AppSettingsHelper.GetAppSettings("SecurityKey", "x2");
        }

        /// <summary>
        /// Encripta el string para enviarlo a MD5
        /// </summary>
        /// <param name="text">texto a encriptar</param>
        /// <returns>tyexto encriiptado en MD5</returns>        
        public String MD5Hash(String text)
        {
            var md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            var result = md5.Hash;

            var builder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
                builder.Append(result[i].ToString("x2"));

            return builder.ToString();
        }

        static readonly string PasswordHash = "P@@Sw0rd";
        static readonly string SaltKey = "S@LT&KEY";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";

        public static string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }
        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        /// <summary>
        /// Verifica si el valor del texto es igual a su equivalente MD5
        /// </summary>
        /// <param name="text">valor a encriptar y comparar</param>
        /// <param name="valorEncriptado">valor Encriptado</param>
        /// <returns>Si es igual el valo o no</returns>
        public bool AreEqualMD5(String text, String valorEncriptado)
        {
            return MD5Hash(text) == valorEncriptado;
        }

        /// <summary>
        /// Obtiene la ip del cliente que accede al sistema para loguearlo
        /// </summary>
        /// <returns></returns>
        public String GetIPAddress()
        {
            var host = default(IPHostEntry);
            string hostname = null;
            hostname = System.Environment.MachineName;
            host = Dns.GetHostEntry(hostname);
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    _IPAddress = ip.ToString();
                }
            }
            return _IPAddress;
        }
    }
}
