﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Admin
{
    public class AcademicPeriodLogic
    {
        private readonly AbetEntities AbetEntities;

        public AcademicPeriodLogic()
        {
            AbetEntities = new AbetEntities();
        }

        public PeriodoAcademico GetAcademicPeriod(int academicPeriodId)
        {
            return AbetEntities.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == academicPeriodId);
        }

        public PeriodoAcademico GetAcademicPeriodBySubModalityAcademicPeriod(int subModalityAcademicPeriodId)
        {
            var subModalityAcademicPeriod = AbetEntities.SubModalidadPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == subModalityAcademicPeriodId);
            if (subModalityAcademicPeriod == null) return null;
            return subModalityAcademicPeriod.PeriodoAcademico;
        }
    }
}
