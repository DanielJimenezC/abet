﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorPunctualityRankingTable : Table
    {
        private List<PuntualidadDocente> listaPuntualidad;
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Ranking de tardanza de docentes ");
            Title.Add("EN", "Professor lateness ranking ");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddPlaceColumn();
            AddLastNameColumn();
            AddNameColumn();
            AddMinuteColumn();   
        }

        private void AddLastNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Apellidos");
            column.LocalizedName.Add("EN", "Last names");
            Header.Columns.Add(column);
        }

        private void AddNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Nombres");
            column.LocalizedName.Add("EN", "First names");
            Header.Columns.Add(column);
        }

        private void AddPlaceColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Puesto");
            column.LocalizedName.Add("EN", "Place");
            Header.Columns.Add(column);
        }

        private void AddMinuteColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total minutos tardanza");
            column.LocalizedName.Add("EN", "Total late minutes");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            listaPuntualidad = GetPuntualidadDocentes();

            if (listaPuntualidad.Count() > 0)
                foreach (var puntualidad in listaPuntualidad)
                {
                    AddBodyRow(puntualidad, listaPuntualidad);
                }
            else
                AddDefaultBodyRow();
        }

        private void AddDefaultBodyRow()
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetDefaultColumn());
            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }
        private void AddBodyRow(PuntualidadDocente puntualidad, List<PuntualidadDocente> lista)
        {
            List<Column> columns = new List<Column>();

            columns.Add(GetProfessorRank(lista, puntualidad));
            columns.Add(GetProfessorLastNames(puntualidad));
            columns.Add(GetProfessorNames(puntualidad));
            columns.Add(GetMinutesValueColumn(puntualidad));
           
            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private List<PuntualidadDocente> GetPuntualidadDocentes()
        {
            return _context.PuntualidadDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == TermId).OrderByDescending(x => x.MinutosTardanza).ToList();
        }

        private Column GetProfessorLastNames(PuntualidadDocente puntualidad)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", puntualidad.Docente.Apellidos);
            column.LocalizedName.Add("EN", puntualidad.Docente.Apellidos);
            return column;
        }

        private Column GetProfessorRank(List<PuntualidadDocente> lista, PuntualidadDocente puntualidad)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", (lista.IndexOf(puntualidad)+1).ToString());
            column.LocalizedName.Add("EN", (lista.IndexOf(puntualidad)+1).ToString());
            return column;
        }

        private Column GetProfessorNames(PuntualidadDocente puntualidad)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", puntualidad.Docente.Nombres);
            column.LocalizedName.Add("EN", puntualidad.Docente.Nombres);
            return column;
        }

        private Column GetMinutesValueColumn(PuntualidadDocente puntualidad)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", puntualidad.MinutosTardanza.ToString());
            return column;
        }

        private Column GetDefaultColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "NO HAY INFORMACIÓN PARA ESTE CICLO");
            column.LocalizedName.Add("EN", "NO INFORMATION TO SHOW FOR THE CURRENT TERM");
            return column;
        }

        #endregion
    }
}
