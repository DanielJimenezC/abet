﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class StudentPeriodTable : Table
    {
        protected override void LoadData()
        {
            LoadHeader();
            LoadBody();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddCareerHeaders();
            AddNroStudentHeaders();
        }

        private void AddCareerHeaders()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Carrera");
            column.LocalizedName.Add("EN", "Career");
            Header.Columns.Add(column);
        }

        private void AddNroStudentHeaders()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Cantidad de estudiantes");
            column.LocalizedName.Add("EN", "Number of students");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {
            try
            {
                List<Column> columns;
                var totalQuantityData = _context.FuncionTotalAlumnosCarrera(TermId).ToList();
                int cantTotal = 0;
                foreach (var item in totalQuantityData)
                {
                    columns = new List<Column>();
                    columns.Add(GetValueColumn(item.Carrera));
                    columns.Add(GetValueColumn(item.Total_de_Alumnos_por_Carrera.ToString()));
                    cantTotal += Convert.ToInt32(item.Total_de_Alumnos_por_Carrera);
                    Row row = new Row();
                    row.Columns.AddRange(columns);
                    Body.Add(row);
                }
                Row rowTotal = new Row();
                columns = new List<Column>();
                columns.Add(GetValueColumn("Total"));
                columns.Add(GetValueColumn(cantTotal.ToString()));
                rowTotal.Columns.AddRange(columns);
                Body.Add(rowTotal);
            }
            catch (Exception ex)
            {

            }
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        #endregion
    }
}
