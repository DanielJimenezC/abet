﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public abstract class Table
    {
        public Row Header { get; set; }
        public List<Row> Body { get; set; }
        public Row Footer { get; set; }
        public Dictionary<string, string> Title { get; set; }

        protected AbetEntities _context;
        protected int TermId;
        protected IndicatorLogic IndicatorLogic;

        protected Table()
        {
            Header = new Row();
            Body = new List<Row>();
            Footer = new Row();
            Title = new Dictionary<string, string>();
        }
        public void Load(AbetEntities context, int termId)
        {
            _context = context;
            TermId = termId;
            IndicatorLogic = new IndicatorLogic(context);
            LoadData();
        }
        protected abstract void LoadData();
    }

    public class Column
    {
        public Dictionary<string, string> LocalizedName { get; set; }

        public Column()
        {
            LocalizedName = new Dictionary<string, string>();
        }
    }

    public class Row {
        public List<Column> Columns { get; set; }
        public Row ()
        {
            Columns = new List<Column>();
        }
    }
}
