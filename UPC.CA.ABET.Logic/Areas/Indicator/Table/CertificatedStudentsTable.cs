﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class CertificatedStudentsTable : Table
    {
        private Carrera _career;
        private int _careerId;
        private const int LAST_N_TERMS = 8;
        private List<PeriodoAcademico> _termRows;

        public CertificatedStudentsTable(int careerId)
        {
            _careerId = careerId;
        }

        protected override void LoadData()
        {
            _termRows = GetLastNTerms(LAST_N_TERMS);
            _career = GetCareer();
            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).ToList();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == _careerId);
        }

        private void SetTitle()
        {
            Title.Add("ES", _career.NombreEspanol);
            Title.Add("EN", _career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddTermHeader();
            AddGraduatesHeader();
            AddCertificatedHeader();
            AddPendingCertificationHeader();
        }

        private void AddTermHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo");
            column.LocalizedName.Add("EN", "Term");
            Header.Columns.Add(column);
        }

        private void AddGraduatesHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total egresados");
            column.LocalizedName.Add("EN", "Total graduates");
            Header.Columns.Add(column);
        }

        private void AddCertificatedHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total titulados");
            column.LocalizedName.Add("EN", "Total certificated students");
            Header.Columns.Add(column);
        }

        private void AddPendingCertificationHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Pendientes titular");
            column.LocalizedName.Add("EN", "Pending certifications");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var ciclo in _termRows)
            {
                AddBodyRow(ciclo);
            }
        }

        private void AddBodyRow(PeriodoAcademico ciclo)
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetTermName(ciclo));
            columns.Add(GetValueColumn(GetGraduatesByTermAndCarreer(ciclo)));
            //columns.Add(GetValueColumn(GetCertificatedStudentsByCohortTermAndCarreer(ciclo)));
            columns.Add(GetValueColumn(GetCertificatedUntilSelectedTerm(ciclo)));
            columns.Add(GetValueColumn(GetPendingCertificationsByTermAndCarreer(ciclo)));

            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private Column GetTermName(PeriodoAcademico ciclo)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", ciclo.CicloAcademico);
            column.LocalizedName.Add("EN", ciclo.CicloAcademico);
            return column;
        }

        private int GetGraduatesByTermAndCarreer(PeriodoAcademico ciclo)
        {
            return _context.Alumno.Where(x => x.AnioEgreso == ciclo.CicloAcademico && x.IdCarreraEgreso == _career.IdCarrera).Count();
        }

        private List<Alumno> GetGraduatesListByTermAndCarreer(PeriodoAcademico ciclo)
        {
            return _context.Alumno.Where(x => x.AnioEgreso == ciclo.CicloAcademico && x.IdCarreraEgreso == _career.IdCarrera).ToList();
        }

        private int GetCertificatedStudentsByCohortTermAndCarreer(PeriodoAcademico ciclo)
        {
            return GetGraduatesListByTermAndCarreer(ciclo).Where(x => x.AnioTitulacion != null).Count();
        }

        private int GetCertificatedUntilSelectedTerm(PeriodoAcademico ciclo)
        {
            var terms = _termRows.Select(t => t.CicloAcademico);

            var lista = GetGraduatesListByTermAndCarreer(ciclo);

            return lista.Where(x => terms.Contains(x.AnioTitulacion)).Count();
        }

        private int GetPendingCertificationsByTermAndCarreer(PeriodoAcademico ciclo)
        {
            //return GetGraduatesByTermAndCarreer(ciclo) - GetCertificatedStudentsByCohortTermAndCarreer(ciclo);

            return GetGraduatesByTermAndCarreer(ciclo) - GetCertificatedUntilSelectedTerm(ciclo);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var totalGraduates = 0;
            var totalCertificated = 0;
            var totalPendingCertification = 0;

            foreach (var ciclo in _termRows)
            {
                totalGraduates += GetGraduatesByTermAndCarreer(ciclo);
                //totalCertificated += GetCertificatedStudentsByCohortTermAndCarreer(ciclo);
                totalCertificated += GetCertificatedUntilSelectedTerm(ciclo);
                totalPendingCertification += GetPendingCertificationsByTermAndCarreer(ciclo);
            }

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(totalGraduates));
            Footer.Columns.Add(GetValueColumn(totalCertificated));
            Footer.Columns.Add(GetValueColumn(totalPendingCertification));
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
