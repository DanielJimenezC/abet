﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class MeetingsTable: Table
    {
        private string academicUnitType;

        private List<UnidadAcademica> _academicUnits;

        public MeetingsTable(string academicUnitType)
        {
            this.academicUnitType = academicUnitType;
        }

        protected override void LoadData()
        {
            _academicUnits = GetAreaAcademicUnits();
            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private void SetTitle()
        {
            if (academicUnitType == "AREA")
            {
                Title.Add("ES", "Reuniones por área");
                Title.Add("EN", "Meetings per area");
            }
            else if (academicUnitType == "SUBAREA")
            {
                Title.Add("ES", "Reuniones por sub área");
                Title.Add("EN", "Meetings per sub area");
            }
        }
        private List<UnidadAcademica> GetAreaAcademicUnits()
        {
            return _context.UnidadAcademica.Where(x => x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == TermId && x.Tipo == academicUnitType).ToList();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            if(academicUnitType == "AREA")
                AddAreasColumn();

            else if(academicUnitType == "SUBAREA")
                AddSubAreasColumn();

            AddScheduledColumn();
            AddRescheduledColumn();
            AddCanceledColumn();
            AddAccordingToSchedule();
        }

        private void AddAreasColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Área");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddSubAreasColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sub área");
            column.LocalizedName.Add("EN", "Sub area");
            Header.Columns.Add(column);
        }

        private void AddScheduledColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "# reuniones programadas");
            column.LocalizedName.Add("EN", "# scheduled meetings");
            Header.Columns.Add(column);
        }

        private void AddRescheduledColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "# reuniones reprogramadas");
            column.LocalizedName.Add("EN", "# re-scheduled meetings");
            Header.Columns.Add(column);
        }

        private void AddCanceledColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "# reuniones canceladas");
            column.LocalizedName.Add("EN", "# canceled meetings");
            Header.Columns.Add(column);
        }

        private void AddAccordingToSchedule()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "# reuniones realizadas según cronograma");
            column.LocalizedName.Add("EN", "# according to schedule meetings");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var academicUnit in _academicUnits)
            {
                AddBodyRow(academicUnit);
            }
        }

        private void AddBodyRow(UnidadAcademica academicUnit)
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetTermName(academicUnit));
            columns.Add(GetValueColumn(GetScheduled(academicUnit)));
            columns.Add(GetValueColumn(GetRescheduled(academicUnit)));
            columns.Add(GetValueColumn(GetCanceled(academicUnit)));
            columns.Add(GetValueColumn(GetAccordingToSchedule(academicUnit)));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetTermName(UnidadAcademica academicUnit)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", academicUnit.NombreEspanol);
            column.LocalizedName.Add("EN", academicUnit.NombreIngles);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }
        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private int GetScheduled(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null && 
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EsExtraordinaria == false);
        }

        private int GetRescheduled(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "REPROGRAMADO" && x.FueEjecutada == true
            && x.EsExtraordinaria == false);
        }

        private int GetCanceled(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "CANCELADO"
            && x.EsExtraordinaria == false);
        }

        private int GetAccordingToSchedule(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "PROGRAMADO" && x.FueEjecutada == true
            && x.EsExtraordinaria == false);
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var totalScheduled = 0;
            var totalRescheduled = 0;
            var totalCanceled = 0;
            var totalAccordingToSchedule = 0;

            foreach (var academicUnit in _academicUnits)
            {
                totalScheduled += GetScheduled(academicUnit);
                totalRescheduled += GetRescheduled(academicUnit);
                totalCanceled += GetCanceled(academicUnit);
                totalAccordingToSchedule += GetAccordingToSchedule(academicUnit);
            }

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(totalScheduled));
            Footer.Columns.Add(GetValueColumn(totalRescheduled));
            Footer.Columns.Add(GetValueColumn(totalCanceled));
            Footer.Columns.Add(GetValueColumn(totalAccordingToSchedule));
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
