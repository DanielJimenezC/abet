﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorAttendanceTable : Table
    {
        private const int LAST_N_TERMS = 3;
        private List<PeriodoAcademico> _termRows;
        protected override void LoadData()
        {
            _termRows = GetLastNTerms(LAST_N_TERMS);
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Control de asistencia de docentes");
            Title.Add("EN", "Professor attendance");
        }
        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).OrderByDescending(x => x.CicloAcademico).ToList();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddBlankColumn();
            AddTotalProfessors();
            AddProfessorsWithPendingClasses();
            AddPendingClasses();
        }

        private void AddBlankColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "");
            column.LocalizedName.Add("EN", "");
            Header.Columns.Add(column);
        }

        private void AddTotalProfessors()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total de docentes");
            column.LocalizedName.Add("EN", "Total professors");
            Header.Columns.Add(column);
        }

        private void AddProfessorsWithPendingClasses()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Núm docentes con sesiones pendientes");
            column.LocalizedName.Add("EN", "Professors with pending classes");
            Header.Columns.Add(column);
        }

        private void AddPendingClasses()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Número de sesiones pendientes");
            column.LocalizedName.Add("EN", "Pending classes");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var ciclo in _termRows)
            {
                AddBodyRow(ciclo);
            }
        }

        private void AddBodyRow(PeriodoAcademico ciclo)
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetTermName(ciclo));
            columns.Add(GetValueColumn(GetProfessorsByTerm(ciclo)));
            columns.Add(GetValueColumn(GetProfessorsWIthPendingClasses(ciclo)));
            columns.Add(GetValueColumn(GetPendingClassesSum(ciclo)));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetTermName(PeriodoAcademico ciclo)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", ciclo.CicloAcademico);
            column.LocalizedName.Add("EN", ciclo.CicloAcademico);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }
        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private int GetProfessorsByTerm(PeriodoAcademico ciclo)
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == ciclo.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            return _context.AsistenciaDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu);
        }

        private int GetProfessorsWIthPendingClasses(PeriodoAcademico ciclo)
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == ciclo.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            return _context.AsistenciaDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu && ((x.ClasesInasistidas - x.RecuperacionesAsistidas) > 0));
        }

        private int GetPendingClassesSum(PeriodoAcademico ciclo)
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == ciclo.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;
            int pendingClassesSum = 0;
            var listaAsistencia = _context.AsistenciaDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu);

            foreach (var asistencia in listaAsistencia)
            {
                var pendingClasses = asistencia.ClasesInasistidas - asistencia.RecuperacionesAsistidas;

                if (pendingClasses > 0)
                    pendingClassesSum += pendingClasses;
            }

            return pendingClassesSum;
        }
        #endregion
    }
}
