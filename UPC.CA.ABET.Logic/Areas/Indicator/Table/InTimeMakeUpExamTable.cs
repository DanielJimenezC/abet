﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class InTimeMakeUpExamTable : Table
    {
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Entrega de exámenes de recuperación");
            Title.Add("EN", "Make up exams check");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddAreaHeader();
            AddInTimeMakeUpExamHeader();
            AddNotInTimeMakeUpExamHeader();
            AddInTimeMidTermPercentageHeader();
            AddNotInTimeMidTermPercentageHeader();
        }

        private void AddAreaHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Area");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddInTimeMakeUpExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Recuperación a tiempo");
            column.LocalizedName.Add("EN", "In time make up exams");
            Header.Columns.Add(column);
        }

        private void AddNotInTimeMakeUpExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Recuperación fuera de tiempo");
            column.LocalizedName.Add("EN", "Not in time make up exams");
            Header.Columns.Add(column);
        }

        private void AddInTimeMidTermPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% recuperación a tiempo");
            column.LocalizedName.Add("EN", "% in time make up exams");
            Header.Columns.Add(column);
        }

        private void AddNotInTimeMidTermPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% recuperación fuera de tiempo");
            column.LocalizedName.Add("EN", "% not in time make up exams");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddBodyRow(area);
            }
        }

        private void AddBodyRow(UnidadAcademica area)
        {
            List<Column> columns = new List<Column>();
            List<CursoPeriodoAcademico> subjects = GetSubjects(area);

            int subjectsCount = subjects.Count;
            int inTimeSubjectsCount = GetInTimeSubjectsCount(subjects);
            int notInTimeSubjectsCount = GetNotInTimeSubjectsCount(subjects);
            double percentageInTime = inTimeSubjectsCount * 100.0 / subjectsCount;
            double percentageNotInTime = notInTimeSubjectsCount * 100.0 / subjectsCount;

            columns.Add(GetAreaNameColumn(area));
            columns.Add(GetValueColumn(inTimeSubjectsCount));
            columns.Add(GetValueColumn(notInTimeSubjectsCount));
            columns.Add(GetValueColumn(percentageInTime));
            columns.Add(GetValueColumn(percentageNotInTime));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }

        private int GetInTimeSubjectsCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.RecuperacionATiempo == true ? 1 : 0;
            }

            return count;
        }

        private int GetNotInTimeSubjectsCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.RecuperacionATiempo == false ? 1 : 0;
            }

            return count;
        }

        private List<CursoPeriodoAcademico> GetSubjects(UnidadAcademica area)
        {
            return IndicatorLogic.getCursoPeriodoAcademicoWithMakeUpExam(area.IdUnidadAcademica);
        }

        private Column GetAreaNameColumn(UnidadAcademica area)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", area.NombreEspanol);
            column.LocalizedName.Add("EN", area.NombreIngles);
            return column;
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var subjects = IndicatorLogic.GetIndicatorCursoPeriodoAcademicoWithMakeUp(TermId);
            var subjectsCount = subjects.Count;
            int inTimeSubjectsCount = GetInTimeSubjectsCount(subjects);
            int notInTimeSubjectsCount = GetNotInTimeSubjectsCount(subjects);
            double percentageInTime = inTimeSubjectsCount * 100.0 / subjectsCount;
            double percentageNotInTime = notInTimeSubjectsCount * 100.0 / subjectsCount;

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(inTimeSubjectsCount));
            Footer.Columns.Add(GetValueColumn(notInTimeSubjectsCount));
            Footer.Columns.Add(GetValueColumn(percentageInTime));
            Footer.Columns.Add(GetValueColumn(percentageNotInTime));
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
