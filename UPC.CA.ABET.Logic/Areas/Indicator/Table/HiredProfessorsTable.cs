﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class HiredProfessorsTable : Table
    {
        protected override void LoadData()
        {
            LoadHeader();
            LoadBody();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddInTimeHeader();
            AddNotInTimeHeader();
            AddInTimePercentageHeader();
            AddNotTimePercentageHeader();
        }

        private void AddInTimeHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Contratos registrados a tiempo");
            column.LocalizedName.Add("EN", "In time hired professors");
            Header.Columns.Add(column);
        }

        private void AddNotInTimeHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Contratos no registrados a tiempo");
            column.LocalizedName.Add("EN", "Not in time hired professors");
            Header.Columns.Add(column);
        }

        private void AddInTimePercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% contratos registrados a tiempo");
            column.LocalizedName.Add("EN", "% in time hired professors");
            Header.Columns.Add(column);
        }

        private void AddNotTimePercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% contratos no registrados a tiempo");
            column.LocalizedName.Add("EN", "% not in time hired professors");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {

            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;


            List<Column> columns = new List<Column>();
            var indicadorDocente = _context.IndicadorDocente.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == idsubmoda);
            if (indicadorDocente == null)
            {
                indicadorDocente = CreateProfessorIndicator();
            }
            int aTiempo = indicadorDocente.DocentesContratadoTiempo;
            int noATiempo = indicadorDocente.DocentesContratadoDestiempo;
            double porcentajeATiempo = (aTiempo * 100.0) / (aTiempo + noATiempo);
            double porcentajeNoATiempo = (noATiempo * 100.0) / (aTiempo + noATiempo);


            columns.Add(GetValueColumn(aTiempo));
            columns.Add(GetValueColumn(noATiempo));
            columns.Add(GetValueColumn(porcentajeATiempo));
            columns.Add(GetValueColumn(porcentajeNoATiempo));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private IndicadorDocente CreateProfessorIndicator()
        {
            int submoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            IndicadorDocente indicadorDocente = new IndicadorDocente
            {
                IdSubModalidadPeriodoAcademico = submoda,
                DocentesContratadoDestiempo = 0,
                DocentesContratadoTiempo = 0,
            };
            _context.IndicadorDocente.Add(indicadorDocente);
            _context.SaveChanges();
            return indicadorDocente;
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }
        #endregion
    }
}
