﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class CohortStudentTable : Table
    {
        private Carrera _career;
        private int _careerId;
        private int _termId;


        public CohortStudentTable(int termId, int careerId)
        {
            _termId = termId;
            _careerId = careerId;
        }

        protected override void LoadData()
        {
            _career = GetCareer();
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == _careerId);
        }


        private void SetTitle()
        {
            Title.Add("ES", _career.NombreEspanol);
            Title.Add("EN", _career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddStudentCode();
            AddStudentFirstName();
            AddStudentLastName();
            AddEnrollmentYearHeader();
            AddGraduationYear();
        }

        private void AddStudentFirstName()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Nombres");
            column.LocalizedName.Add("EN", "First Name");
            Header.Columns.Add(column);
        }

        private void AddStudentLastName()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Apellidos");
            column.LocalizedName.Add("EN", "Last name");
            Header.Columns.Add(column);
        }

        private void AddStudentCode()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Codigo");
            column.LocalizedName.Add("EN", "Code");
            Header.Columns.Add(column);
        }

        private void AddGraduationYear()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo de egreso");
            column.LocalizedName.Add("EN", "Graduation term");
            Header.Columns.Add(column);
        }

        private void AddEnrollmentYearHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo de ingreso");
            column.LocalizedName.Add("EN", "Enrollment term");
            Header.Columns.Add(column);
        }
        #endregion


        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {
            var totalData = _context.sp_AlumnosCohortePorCiclo(_termId).ToList();
            var data = totalData.Where(x => x.IdCarreraEgreso == GetCareer().IdCarrera).ToList();

            foreach (var student in data)
            {
                List<Column> columns = new List<Column>();
                columns.Add(GetValueColumn(student.Codigo));
                columns.Add(GetValueColumn(student.Nombres));
                columns.Add(GetValueColumn(student.Apellidos));
                columns.Add(GetValueColumn(student.AnioIngreso));
                columns.Add(GetValueColumn(student.AnioEgreso));

                Row row = new Row();
                row.Columns.AddRange(columns);
                Body.Add(row);
            }
        }


        private Column GetValueColumn(int value)
        {
            return GetValueColumn(value.ToString());
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        #endregion
  
    }
}
