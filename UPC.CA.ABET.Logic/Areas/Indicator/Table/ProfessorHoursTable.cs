﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class ProfessorHoursTable : Table
    {
        protected override void LoadData()
        {
            LoadHeader();
            LoadBody();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddProfessorHeaders();
            AddCampusHeaders();
            AddNroHoursHeaders();
        }

        private void AddProfessorHeaders()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Profesor");
            column.LocalizedName.Add("EN", "Professor");
            Header.Columns.Add(column);
        }

        public void AddCampusHeaders()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sede");
            column.LocalizedName.Add("EN", "Campus");
            Header.Columns.Add(column);
        }

        private void AddNroHoursHeaders()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Horas Programadas");
            column.LocalizedName.Add("EN", "Scheduled hours");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {
            try
            {
                List<Column> columns;
                var ciclo = _context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == TermId);
                var docentes = _context.DocenteHora.Where(x => x.idPeriodoAcademico.Equals(ciclo.CicloAcademico)).ToList();

                foreach (var item in docentes)
                {
                    columns = new List<Column>();
                    var docente = _context.Docente.FirstOrDefault(x => x.Codigo.Equals(item.codDocente));
                    columns.Add(GetValueColumn(docente.Nombres + " " + docente.Apellidos));
                    columns.Add(GetValueColumn(item.codSede));
                    columns.Add(GetValueColumn(item.Horas.ToString()));
                    Row row = new Row();
                    row.Columns.AddRange(columns);
                    Body.Add(row);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        #endregion
    }
}
