﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorPunctualityTable : Table
    {
        private const int LAST_N_TERMS = 3;
        private List<PeriodoAcademico> _termRows;
        protected override void LoadData()
        {
            _termRows = GetLastNTerms(LAST_N_TERMS);
            SetTitle();
            LoadHeader();
            LoadBody();
        }
        private void SetTitle()
        {
            Title.Add("ES", "Control de puntualidad de docentes");
            Title.Add("EN", "Professor punctuality");
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).OrderByDescending(x => x.CicloAcademico).ToList();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddBlankColumn();
            AddTotalProfessors();
            AddTotalLateProfessors();
            AddLateMinutes();
            AddLatenessRatio();
        }

        private void AddBlankColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "");
            column.LocalizedName.Add("EN", "");
            Header.Columns.Add(column);
        }

        private void AddTotalProfessors()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total de docentes");
            column.LocalizedName.Add("EN", "Total professors");
            Header.Columns.Add(column);
        }

        private void AddTotalLateProfessors()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Núm docentes con tardanza");
            column.LocalizedName.Add("EN", "Late professors");
            Header.Columns.Add(column);
        }

        private void AddLateMinutes()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Minutos de tardanza");
            column.LocalizedName.Add("EN", "Late minutes");
            Header.Columns.Add(column);
        }

        private void AddLatenessRatio()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ratio de tardanzas al semestre");
            column.LocalizedName.Add("EN", "Lateness ratio per semester");
            Header.Columns.Add(column);
        }

        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var ciclo in _termRows)
            {
                AddBodyRow(ciclo);
            }
        }

        private void AddBodyRow(PeriodoAcademico ciclo)
        {
            List<Column> columns = new List<Column>();

            columns.Add(GetTermName(ciclo));
            columns.Add(GetValueColumn(GetProfessorsByTerm(ciclo)));
            columns.Add(GetValueColumn(GetLateProfessors(ciclo)));
            columns.Add(GetValueColumn(GetLateMinutesSum(ciclo)));
            int sumaMinutos = GetLateMinutesSum(ciclo);
            int totalDocentes = GetProfessorsByTerm(ciclo);

            double ratio = sumaMinutos * 1.0 / totalDocentes;

            columns.Add(GetValueColumn(ratio));

            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);

        }

        private Column GetTermName(PeriodoAcademico ciclo)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", ciclo.CicloAcademico);
            column.LocalizedName.Add("EN", ciclo.CicloAcademico);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }
        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private int GetProfessorsByTerm(PeriodoAcademico ciclo)
        {
            return _context.PuntualidadDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == ciclo.IdPeriodoAcademico);
        }

        private int GetLateMinutesSum(PeriodoAcademico ciclo)
        {
            int minuteSum = 0;
            var listaPuntualidad = _context.PuntualidadDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == ciclo.IdPeriodoAcademico);

            if (listaPuntualidad.Count() <= 0)
                return 0;

            foreach (var puntualidad in listaPuntualidad)
            {
                minuteSum += puntualidad.MinutosTardanza;
            }

            return minuteSum;
        }

        private int GetLateProfessors(PeriodoAcademico ciclo)
        {
            return _context.PuntualidadDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == ciclo.IdPeriodoAcademico && x.MinutosTardanza > 0);
        }
        #endregion
    }
}
