﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class InTimeScheduleTable : Table
    {
        protected override void LoadData()
        {
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddCareerHeader();
            AddSubjectsHeader();
            AddClassesHeader();
            AddCampusHeaders();
            AddInTimeSchedulesHeader();
            AddInTimeSchedulesPercentageHeader();
        }

        private void AddCareerHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Area");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddSubjectsHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Cursos");
            column.LocalizedName.Add("EN", "Subjects");
            Header.Columns.Add(column);
        }

        private void AddClassesHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Secciones");
            column.LocalizedName.Add("EN", "Classes");
            Header.Columns.Add(column);
        }

        private void AddCampusHeaders()
        {

            var campusList = _context.Sede.Where(x => x.PeriodoAcademicoSede.Any(y => y.IdSubModalidadPeriodoAcademico == TermId)).ToList();
            foreach (var campus in campusList)
            {
                AddCampusHeader(campus.Nombre);
            }
        }

        private void AddCampusHeader(string campus)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Secciones " + campus);
            column.LocalizedName.Add("EN", campus + " classes");
            Header.Columns.Add(column);
        }

        private void AddInTimeSchedulesHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Horarios a tiempo");
            column.LocalizedName.Add("EN", "In time schedules");
            Header.Columns.Add(column);
        }

        private void AddInTimeSchedulesPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% horarios a tiempo");
            column.LocalizedName.Add("EN", "% in time schedules");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddBodyRow(area);
            }
        }

        private void AddBodyRow(UnidadAcademica area)
        {
            List<Column> columns = new List<Column>();
            List<UnidadAcademica> subjects = GetSubjects(area);

            int classesCount = GetClassesCount(subjects);
            int inTimeClassesCount = GetInTimeClassesCount(subjects);
            double percentage = inTimeClassesCount * 100.0 / classesCount;   

            columns.Add(GetAreaNameColumn(area));
            columns.Add(GetValueColumn(subjects.Count));
            columns.Add(GetValueColumn(classesCount));
            columns.AddRange(GetCampusClassesCountColumns(subjects));
            columns.Add(GetValueColumn(inTimeClassesCount));
            columns.Add(GetValueColumn(percentage));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }

        private List<Column> GetCampusClassesCountColumns(List<UnidadAcademica> subjects)
        {
            List<Column> columns = new List<Column>();

            foreach (var campus in _context.Sede.Select(x => x))
            {
                columns.Add(GetClassesByCampusCount(subjects, campus));
            }

            return columns;
        }

        private Column GetClassesByCampusCount(List<UnidadAcademica> subjects, Sede campus)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += GetClassesBySubjectAndCampusCount(subject, campus);
            }

            Column column = new Column();
            column.LocalizedName.Add("VAL", count.ToString());
            return column;
        }

        private int GetClassesCount(List<UnidadAcademica> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += GetClassesBySubjectCount(subject);
            }

            return count;
        }

        private int GetInTimeClassesCount(List<UnidadAcademica> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += GetInTimeClassesBySubjectCount(subject);
            }

            return count;
        }

        private List<UnidadAcademica> GetSubjects(UnidadAcademica area)
        {
            return IndicatorLogic.GetSubjects(area.IdUnidadAcademica);
        }

        private Column GetAreaNameColumn(UnidadAcademica area)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", area.NombreEspanol);
            column.LocalizedName.Add("EN", area.NombreIngles);
            return column;
        }

        private int GetClassesBySubjectCount(UnidadAcademica subject)
        {
            return _context.Seccion.Count(x => x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == subject.IdCursoPeriodoAcademico);
        }

        private int GetInTimeClassesBySubjectCount(UnidadAcademica subject)
        {
            return _context.Seccion.Count(x => x.RevisionHorarioATiempo == true && x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == subject.IdCursoPeriodoAcademico);
        }

        private int GetClassesBySubjectAndCampusCount(UnidadAcademica subject, Sede campus)
        {
            return _context.Seccion.Count(x => x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == subject.IdCursoPeriodoAcademico && x.Sede.IdSede == campus.IdSede);
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var subjects = GetSubjects();
            int classesCount = GetClassesCount(subjects);
            int inTimeClassesCount = GetInTimeClassesCount(subjects);
            double percentage = inTimeClassesCount * 100.0 / classesCount;

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(subjects.Count));
            Footer.Columns.Add(GetValueColumn(classesCount));
            Footer.Columns.AddRange(GetCampusClassesCountColumns(subjects));
            Footer.Columns.Add(GetValueColumn(inTimeClassesCount));
            Footer.Columns.Add(GetValueColumn(percentage));
        }

        private List<UnidadAcademica> GetSubjects()
        {
            return _context.UnidadAcademica.Where(x => x.Tipo == "CURSO" && x.IdSubModalidadPeriodoAcademico == TermId).ToList();
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
