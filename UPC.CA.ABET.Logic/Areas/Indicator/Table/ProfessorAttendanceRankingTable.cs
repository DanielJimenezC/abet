﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorAttendanceRankingTable : Table
    {
        private List<AsistenciaDocente> listaAsistencia;
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Ranking de inasistencia de docentes");
            Title.Add("EN", "Professor non attendance ranking ");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddPlaceColumn();
            AddLastNameColumn();
            AddNameColumn();
            AddMissedClasses();
            AddRetrievedClasses();
            AddPendingClasses();
        }

        private void AddPlaceColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Puesto");
            column.LocalizedName.Add("EN", "Place");
            Header.Columns.Add(column);
        }

        private void AddLastNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Apellidos");
            column.LocalizedName.Add("EN", "Last names");
            Header.Columns.Add(column);
        }

        private void AddNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Nombres");
            column.LocalizedName.Add("EN", "First names");
            Header.Columns.Add(column);
        }

        private void AddMissedClasses()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sesiones inasistidas");
            column.LocalizedName.Add("EN", "Missed classes");
            Header.Columns.Add(column);
        }

        private void AddRetrievedClasses()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sesiones recuperadas");
            column.LocalizedName.Add("EN", "Retrieved classes");
            Header.Columns.Add(column);
        }

        private void AddPendingClasses()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sesiones pendientes");
            column.LocalizedName.Add("EN", "Pending classes");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY

        private void LoadBody()
        {
            listaAsistencia = GetAsistenciaDocentes();

            if (listaAsistencia.Count() > 0)
                foreach (var asistencia in listaAsistencia)
                {
                    AddBodyRow(asistencia, listaAsistencia);
                }
            else
                AddDefaultBodyRow();
        }

        private void AddDefaultBodyRow()
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetDefaultColumn());
            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private void AddBodyRow(AsistenciaDocente asistencia, List<AsistenciaDocente> lista)
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetProfessorRank(lista, asistencia));
            columns.Add(GetProfessorLastNames(asistencia));
            columns.Add(GetProfessorNames(asistencia));
            columns.Add(GetProfessorMissedClasses(asistencia));
            columns.Add(GetValueColumn(GetRetrievedClasses(asistencia)));
            columns.Add(GetValueColumn(GetPendingClasses(asistencia)));

            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private List<AsistenciaDocente> GetAsistenciaDocentes()
        {

            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;



            return _context.AsistenciaDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu).OrderByDescending(x => x.ClasesInasistidas - x.RecuperacionesAsistidas).ToList();
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private int GetPendingClasses(AsistenciaDocente asistencia)
        {
            int pendingClasses;

            if (asistencia.RecuperacionesAsistidas > asistencia.ClasesInasistidas)
                pendingClasses = 0;
            else
                pendingClasses = asistencia.ClasesInasistidas - asistencia.RecuperacionesAsistidas;

            return pendingClasses;
        }
        private int GetRetrievedClasses(AsistenciaDocente asistencia)
        {
            int retrievedClasses;

            if (asistencia.RecuperacionesAsistidas > asistencia.ClasesInasistidas)
                retrievedClasses = asistencia.ClasesInasistidas;
            else
                retrievedClasses = asistencia.RecuperacionesAsistidas;

            return retrievedClasses;
        }
        private Column GetProfessorRank(List<AsistenciaDocente> lista, AsistenciaDocente asistencia)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", (lista.IndexOf(asistencia) + 1).ToString());
            column.LocalizedName.Add("EN", (lista.IndexOf(asistencia) + 1).ToString());
            return column;
        }

        private Column GetProfessorLastNames(AsistenciaDocente asistencia)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", asistencia.Docente.Apellidos);
            column.LocalizedName.Add("EN", asistencia.Docente.Apellidos);
            return column;
        }

        private Column GetProfessorNames(AsistenciaDocente asistencia)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", asistencia.Docente.Nombres);
            column.LocalizedName.Add("EN", asistencia.Docente.Nombres);
            return column;
        }

        private Column GetProfessorMissedClasses(AsistenciaDocente asistencia)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", asistencia.ClasesInasistidas.ToString());
            column.LocalizedName.Add("EN", asistencia.ClasesInasistidas.ToString());
            return column;
        }

        private Column GetDefaultColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "NO HAY INFORMACIÓN PARA ESTE CICLO");
            column.LocalizedName.Add("EN", "NO INFORMATION TO SHOW FOR THE CURRENT TERM");
            return column;
        }
        #endregion
    }
}
