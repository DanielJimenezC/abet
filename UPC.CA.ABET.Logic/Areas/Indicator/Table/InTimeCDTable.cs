﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class InTimeCDTable : Table
    {
        protected override void LoadData()
        {
            LoadHeader();
            LoadBody();
            LoadFooter();
            SetTitle();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Entrega de CD");
            Title.Add("EN", "In time CD");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddAreaHeader();
            AddInTimeCDHeader();
            AddNotInTimeCDHeader();
            AddInTimeCDPercentageHeader();
            AddNotInTimeCDPercentageHeader();
        }

        private void AddAreaHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Área");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddInTimeCDHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "CD a tiempo");
            column.LocalizedName.Add("EN", "In time CD");
            Header.Columns.Add(column);
        }

        private void AddNotInTimeCDHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "CD fuera de tiempo");
            column.LocalizedName.Add("EN", "Not in time CD");
            Header.Columns.Add(column);
        }

        private void AddInTimeCDPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% CD a tiempo");
            column.LocalizedName.Add("EN", "% in time CD");
            Header.Columns.Add(column);
        }

        private void AddNotInTimeCDPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% CD fuera de tiempo");
            column.LocalizedName.Add("EN", "% not in time CD");
            Header.Columns.Add(column);
        }
        #endregion
        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddBodyRow(area);
            }
        }

        private void AddBodyRow(UnidadAcademica area)
        {
            List<Column> columns = new List<Column>();
            List<UnidadAcademica> subjects = GetSubjects(area);

            int subjectsCount = subjects.Count;
            int inTimeCDCount = GetInTimeCDCount(subjects);
            int notInTimeCDCount = GetNotInTimeCDCount(subjects);
            double percentageInTime = inTimeCDCount * 100.0 / subjectsCount;
            double percentageNotInTime = notInTimeCDCount * 100.0 / subjectsCount;

            columns.Add(GetAreaNameColumn(area));
            columns.Add(GetValueColumn(inTimeCDCount));
            columns.Add(GetValueColumn(notInTimeCDCount));
            columns.Add(GetValueColumn(percentageInTime));
            columns.Add(GetValueColumn(percentageNotInTime));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }

        private int GetInTimeCDCount(List<UnidadAcademica> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.CursoPeriodoAcademico.CDATiempo == true ? 1 : 0;
            }

            return count;
        }

        private int GetNotInTimeCDCount(List<UnidadAcademica> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.CursoPeriodoAcademico.CDATiempo == false ? 1 : 0;
            }

            return count;
        }

        private List<UnidadAcademica> GetSubjects(UnidadAcademica area)
        {
            return IndicatorLogic.GetSubjects(area.IdUnidadAcademica);
        }

        private Column GetAreaNameColumn(UnidadAcademica area)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", area.NombreEspanol);
            column.LocalizedName.Add("EN", area.NombreIngles);
            return column;
        }
        #endregion
        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var subjects = GetSubjects();
            var subjectsCount = subjects.Count;
            int inTimeSubjectsCount = GetInTimeCDCount(subjects);
            int notInTimeSubjectsCount = GetNotInTimeCDCount(subjects);
            double percentageInTime = inTimeSubjectsCount * 100.0 / subjectsCount;
            double percentageNotInTime = notInTimeSubjectsCount * 100.0 / subjectsCount;

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(inTimeSubjectsCount));
            Footer.Columns.Add(GetValueColumn(notInTimeSubjectsCount));
            Footer.Columns.Add(GetValueColumn(percentageInTime));
            Footer.Columns.Add(GetValueColumn(percentageNotInTime));
        }

        private List<UnidadAcademica> GetSubjects()
        {
            return _context.UnidadAcademica.Where(x => x.Tipo == "CURSO" && x.IdSubModalidadPeriodoAcademico == TermId).ToList();
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
