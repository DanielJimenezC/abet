﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class StudentCampusTable : Table
    {
        private Carrera _career;
        
        private int _careerId;
        private int _termId;

        public StudentCampusTable(int careerId, int termId)
        {
            _careerId = careerId;
            _termId = termId;
        }

        protected override void LoadData()
        {
            _career = GetCareer();

            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == _careerId);
        }

        private void SetTitle()
        {
            Title.Add("ES", _career.NombreEspanol);
            Title.Add("EN", _career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddCampusHeaders();
            AddCampusPercentHeaders();
        }

        private void AddCampusHeaders()
        {
            var campuses = _context.Sede.ToList().OrderBy(x => x.Nombre);

            foreach (var campus in campuses)
            {
                Column column = new Column();
                column.LocalizedName.Add("ES", campus.Nombre);
                Header.Columns.Add(column);
            }
        }

        private void AddCampusPercentHeaders()
        {
            var campuses = _context.Sede.ToList().OrderBy(x => x.Nombre);

            foreach (var campus in campuses)
            {
                Column column = new Column();
                column.LocalizedName.Add("ES", "% " + campus.Nombre);
                Header.Columns.Add(column);
            }
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {
            try
            {
                List<Column> columns = new List<Column>();
                var totalQuantityData = _context.FuncionTotalAlumnosCarrera(_termId).ToList();
                var totalQuantityCareerData = totalQuantityData.Where(x => x.Carrera == GetCareer().NombreEspanol).ToList().First().Total_de_Alumnos_por_Carrera.ToString();

                var IdMO = _context.Sede.Where(x => x.Nombre.Equals("MONTERRICO")).ToList().First().IdSede;
                var IdSI = _context.Sede.Where(x => x.Nombre.Equals("SAN ISIDRO")).ToList().First().IdSede;
                var IdSM = _context.Sede.Where(x => x.Nombre.Equals("SAN MIGUEL")).ToList().First().IdSede;
                var IdVL = _context.Sede.Where(x => x.Nombre.Equals("VILLA")).ToList().First().IdSede;

                var MOQuantity = _context.sp_CantAlumnosSede(_termId, IdMO, _careerId).ToList().First().ToString();
                columns.Add(GetValueColumn(MOQuantity));
                var SIQuantity = _context.sp_CantAlumnosSede(_termId, IdSI, _careerId).ToList().First().ToString();
                columns.Add(GetValueColumn(SIQuantity));
                var SMQuantity = _context.sp_CantAlumnosSede(_termId, IdSM, _careerId).ToList().First().ToString();
                columns.Add(GetValueColumn(SMQuantity));
                var VLQuantity = _context.sp_CantAlumnosSede(_termId, IdVL, _careerId).ToList().First().ToString();
                columns.Add(GetValueColumn(VLQuantity));

                var MOPercent = Math.Round(Convert.ToDouble(MOQuantity) * 100.00 / Convert.ToDouble(totalQuantityCareerData), 2) + "%";
                columns.Add(GetValueColumn(MOPercent.ToString()));
                var SIPercent = Math.Round(Convert.ToDouble(SIQuantity) * 100.00 / Convert.ToDouble(totalQuantityCareerData), 2) + "%";
                columns.Add(GetValueColumn(SIPercent.ToString()));
                var SMPercent = Math.Round(Convert.ToDouble(SMQuantity) * 100.00 / Convert.ToDouble(totalQuantityCareerData), 2) + "%";
                columns.Add(GetValueColumn(SMPercent.ToString()));
                var VLPercent = Math.Round(Convert.ToDouble(VLQuantity) * 100.00 / Convert.ToDouble(totalQuantityCareerData), 2) + "%";
                columns.Add(GetValueColumn(VLPercent.ToString()));

                Row row = new Row();
                row.Columns.AddRange(columns);

                Body.Add(row);
            }
            catch(Exception ex)
            {
                
            }
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        #endregion
    }
}
