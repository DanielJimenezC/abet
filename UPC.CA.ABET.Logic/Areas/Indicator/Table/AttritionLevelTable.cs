﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class AttritionLevelTable : Table
    {
        private Carrera Career;
        private int CareerId;

        public AttritionLevelTable(int careerId)
        {
            CareerId = careerId;
        }

        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Career = _context.Carrera.FirstOrDefault(x => x.IdCarrera == CareerId);
            Title.Add("ES", Career.NombreEspanol);
            Title.Add("EN", Career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddTermHeader();
            AddSeparated();
            AddModalityChange();
            AddDesertion();
            AddReservation();
            AddEnglish();
            AddAttritionTotal();
            AddRegisteredTotal();
            AddTotal();
            AddAttritionVsTotal();
        }

        private void AddTermHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo");
            column.LocalizedName.Add("EN", "Term");
            Header.Columns.Add(column);
        }

        private void AddSeparated()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Baja");
            column.LocalizedName.Add("EN", "Separated");
            Header.Columns.Add(column);
        }

        private void AddModalityChange()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Cambio de modalidad");
            column.LocalizedName.Add("EN", "Modality change");
            Header.Columns.Add(column);
        }

        private void AddDesertion()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Deserción");
            column.LocalizedName.Add("EN", "Desertion");
            Header.Columns.Add(column);
        }

        private void AddReservation()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Reserva");
            column.LocalizedName.Add("EN", "Reservation");
            Header.Columns.Add(column);
        }

        private void AddEnglish()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Solo inglés");
            column.LocalizedName.Add("EN", "English");
            Header.Columns.Add(column);
        }

        private void AddAttritionTotal()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total attrition");
            column.LocalizedName.Add("EN", "Attrition total");
            Header.Columns.Add(column);
        }

        private void AddRegisteredTotal()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total matriculados");
            column.LocalizedName.Add("EN", "Registered total");
            Header.Columns.Add(column);
        }

        private void AddTotal()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            Header.Columns.Add(column);
        }

        private void AddAttritionVsTotal()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% Attrition vs total");
            column.LocalizedName.Add("EN", "% Attrition vs total");
            Header.Columns.Add(column);
        }
        #endregion

        
        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var detalle in _context.AttritionDetalle.Where(x => x.Attrition.IdSubModalidadPeriodoAcademico == TermId && x.IdCarrera == Career.IdCarrera))
            {
                AddBodyRow(detalle);
            }
        }

        private void AddBodyRow(AttritionDetalle detalle)
        {
            List<Column> columns = new List<Column>();
            int totalAttrition = detalle.TotalAttrition;
            int totalMatriculados = detalle.TotalMatriculados;
            int total = totalAttrition + totalMatriculados;
            double attritionVsTotal = totalAttrition * 100.0 / total; 

            columns.Add(GetValueColumn(detalle.Ciclo));
            columns.Add(GetValueColumn(detalle.Baja));
            columns.Add(GetValueColumn(detalle.CambioModalidad));
            columns.Add(GetValueColumn(detalle.Desercion));
            columns.Add(GetValueColumn(detalle.Reserva));
            columns.Add(GetValueColumn(detalle.SoloIngles));
            columns.Add(GetValueColumn(totalAttrition));
            columns.Add(GetValueColumn(totalMatriculados));
            columns.Add(GetValueColumn(total));
            columns.Add(GetValueColumn(attritionVsTotal));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            return GetValueColumn(value.ToString());
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            return GetValueColumn(string.Format("{0:0.##}%", value));
        }
        #endregion
    }
}
