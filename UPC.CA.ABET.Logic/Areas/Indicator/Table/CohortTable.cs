﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class CohortTable : Table
    {
        private const int LAST_N_TERMS = 5;
        private Carrera _career;
        private int _careerId;
        private List<PeriodoAcademico> _headerTerms;

        public CohortTable(int careerId)
        {
            _careerId = careerId;
        }

        protected override void LoadData()
        {
            _career = GetCareer();
            _headerTerms = GetLastNTerms(LAST_N_TERMS);

            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == _careerId);
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId) {
                    break;
                }
            }
            return terms.Take(n).ToList();
        }

        private void SetTitle()
        {
            Title.Add("ES", _career.NombreEspanol);
            Title.Add("EN", _career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddEnrollmentYearHeader();
            AddEnrolledStudentsHeader();
            AddGraduationYearHeaders();
            AddGraduatesHeader();
            AddGraduatesVsTerm();
        }

        private void AddEnrollmentYearHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Año ingreso");
            column.LocalizedName.Add("EN", "Enrollment year");
            Header.Columns.Add(column);
        }

        private void AddEnrolledStudentsHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total ingresantes");
            column.LocalizedName.Add("EN", "Enrolled students");
            Header.Columns.Add(column);
        }

        private void AddGraduationYearHeaders()
        {
            foreach (var term in _headerTerms)
            {
                Column column = new Column();
                column.LocalizedName.Add("ES", term.CicloAcademico);
                column.LocalizedName.Add("EN", term.CicloAcademico);
                Header.Columns.Add(column);
            }
        }

        private void AddGraduatesHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Egresados por cohorte");
            column.LocalizedName.Add("EN", "Graduates");
            Header.Columns.Add(column);
        }

        private void AddGraduatesVsTerm()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Egresados por ciclo de ingreso");
            column.LocalizedName.Add("EN", "Graduates per term");
            Header.Columns.Add(column);
        }

        private void AddReservation()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Reserva");
            column.LocalizedName.Add("EN", "Reservation");
            Header.Columns.Add(column);
        }
        #endregion


        #region LOAD_BODY
        private void LoadBody()
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            foreach (var term in _context.IndicadorCohorte.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu && x.Carrera.IdCarrera == _careerId).OrderByDescending(x => x.Ciclo))
            {
                AddBodyRow(term);
            }

        }

        private void AddBodyRow(IndicadorCohorte term)
        {

            List<Column> columns = new List<Column>();
            int graduate = 0;

            columns.Add(GetValueColumn(term.Ciclo));
            columns.Add(GetValueColumn(term.CantidadIngresantes.Value));

            foreach (var headerTerm in _headerTerms)
            {
                int count = _context.Alumno.Count(x => 
                x.AnioEgreso == headerTerm.CicloAcademico && 
                x.Carrera.IdCarrera == _careerId && 
                term.Ciclo.Remove(4, 1) == x.Codigo.Remove(5));

                columns.Add(GetValueColumn(count));
                graduate += count;
            }

            columns.Add(GetValueColumn(graduate));

            double graduateVsEnrolled = graduate * 100.0 / term.CantidadIngresantes.Value;
            columns.Add(GetValueColumn(graduateVsEnrolled));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);

        }


        private Column GetValueColumn(int value)
        {
            return GetValueColumn(value.ToString());
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            return GetValueColumn(string.Format("{0:0.##}%", value));
        }
        #endregion

        private void LoadFooter()
        {
            Footer.Columns.Add(GetEmptyColumn());
            Footer.Columns.Add(GetTotalNameColumn());

            foreach (var headerTerm in _headerTerms)
            {
                var termGraduates = _context.Alumno.Count(x => 
                x.AnioEgreso == headerTerm.CicloAcademico &&
                x.Carrera.IdCarrera == _careerId);

                Footer.Columns.Add(GetValueColumn(termGraduates));
            }
            Footer.Columns.Add(GetEmptyColumn());
            Footer.Columns.Add(GetEmptyColumn());
        }

        private Column GetEmptyColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "");
            column.LocalizedName.Add("EN", "");
            return column;
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Egresados ciclo");
            column.LocalizedName.Add("EN", "Egresados ciclo");
            return column;
        }
    }
}
