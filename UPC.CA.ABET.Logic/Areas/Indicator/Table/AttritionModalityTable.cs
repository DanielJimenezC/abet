﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class AttritionModalityTable : Table
    {
        private Carrera Career;
        private int CareerId;

        public AttritionModalityTable(int careerId)
        {
            CareerId = careerId;
        }

        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Career = _context.Carrera.FirstOrDefault(x => x.IdCarrera == CareerId);
            Title.Add("ES", Career.NombreEspanol);
            Title.Add("EN", Career.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddTermHeader();
            AddSeparated();
            AddModalityChange();
            AddDesertion();
            AddReservation();
            AddEnglish();
        }

        private void AddTermHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo");
            column.LocalizedName.Add("EN", "Term");
            Header.Columns.Add(column);
        }

        private void AddSeparated()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Baja");
            column.LocalizedName.Add("EN", "Separated");
            Header.Columns.Add(column);
        }

        private void AddModalityChange()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Cambio de modalidad");
            column.LocalizedName.Add("EN", "Modality change");
            Header.Columns.Add(column);
        }

        private void AddDesertion()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Deserción");
            column.LocalizedName.Add("EN", "Desertion");
            Header.Columns.Add(column);
        }

        private void AddReservation()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Reserva");
            column.LocalizedName.Add("EN", "Reservation");
            Header.Columns.Add(column);
        }

        private void AddEnglish()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Solo inglés");
            column.LocalizedName.Add("EN", "English");
            Header.Columns.Add(column);
        }
        #endregion

        
        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var detalle in _context.AttritionDetalle.Where(x => x.Attrition.IdSubModalidadPeriodoAcademico == TermId && x.IdCarrera == Career.IdCarrera))
            {
                AddBodyRow(detalle);
            }
        }

        private void AddBodyRow(AttritionDetalle detalle)
        {
            List<Column> columns = new List<Column>();
            double separated = detalle.Baja * 100.0 / detalle.TotalAttrition;
            double modalityChange = detalle.CambioModalidad * 100.0 / detalle.TotalAttrition;
            double desertion = detalle.Desercion * 100.0 / detalle.TotalAttrition;
            double reservation = detalle.Reserva * 100.0 / detalle.TotalAttrition;
            double english = detalle.SoloIngles * 100.0 / detalle.TotalAttrition;

            columns.Add(GetValueColumn(detalle.Ciclo));
            columns.Add(GetValueColumn(separated));
            columns.Add(GetValueColumn(modalityChange));
            columns.Add(GetValueColumn(desertion));
            columns.Add(GetValueColumn(reservation));
            columns.Add(GetValueColumn(english));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            return GetValueColumn(value.ToString());
        }

        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }

        private Column GetValueColumn(double value)
        {
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            return GetValueColumn(string.Format("{0:0.##}%", value));
        }
        #endregion
    }
}
