﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class AttritionRatioTable : Table
    {
        private Carrera carreer;
        private int carreerId;
        private Attrition attrition;
        private List<AttritionDetalle> attritionDetalle;


        public AttritionRatioTable(int carreerId)
        {
            this.carreerId = carreerId;
        }
        protected override void LoadData()
        {
            carreer = GetCareer();
            attrition = GetAttritionByTerm();
            attritionDetalle = GetAttritionDetalle();
            LoadHeader();
            LoadBody();
            SetTitle();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == carreerId);
        }

        private Attrition GetAttritionByTerm()
        {
            return _context.Attrition.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == TermId);
        }

        private List<AttritionDetalle> GetAttritionDetalle()
        {
            return _context.AttritionDetalle.Where(x => x.IdAttrition == attrition.IdAttrition && x.IdCarrera == carreerId).ToList();
        }

        private void SetTitle()
        {
            carreer = _context.Carrera.FirstOrDefault(x => x.IdCarrera == carreerId);
            Title.Add("ES", carreer.NombreEspanol);
            Title.Add("EN", carreer.NombreIngles);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddTermHeader();
            AddBajaHeader();
            AddModalityChangeHeader();
            AddDesertionHeader();
            AddReservationHeader();
            AddEnglishOnlyHeader();
            AddTotalEnrolled();
            AddTotalAttrition();
            AddPercentageAttrition();
        }

        private void AddTermHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Ciclo");
            column.LocalizedName.Add("EN", "Term");
            Header.Columns.Add(column);
        }

        private void AddBajaHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Baja");
            column.LocalizedName.Add("EN", "Term");
            Header.Columns.Add(column);
        }

        private void AddModalityChangeHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Cambio de modalidad");
            column.LocalizedName.Add("EN", "Modality change");
            Header.Columns.Add(column);
        }

        private void AddDesertionHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Deserción");
            column.LocalizedName.Add("EN", "Desertion");
            Header.Columns.Add(column);
        }

        private void AddReservationHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Reserva");
            column.LocalizedName.Add("EN", "Reservation");
            Header.Columns.Add(column);
        }

        private void AddEnglishOnlyHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Solo inglés");
            column.LocalizedName.Add("EN", "English only");
            Header.Columns.Add(column);
        }

        private void AddTotalEnrolled()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total matriculados");
            column.LocalizedName.Add("EN", "Total enrolled");
            Header.Columns.Add(column);
        }

        private void AddTotalAttrition()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total attrition");
            column.LocalizedName.Add("EN", "Total Attrition");
            Header.Columns.Add(column);
        }

        private void AddPercentageAttrition()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Porcentaje attrition");
            column.LocalizedName.Add("EN", "Attrition percentage");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var detalle in attritionDetalle)
            {
                AddBodyRow(detalle);
            }
        }

        private void AddBodyRow(AttritionDetalle detalle)
        {
            List<Column> columns = new List<Column>();
            double porcentajeAttrition = detalle.TotalAttrition *100.0 / detalle.TotalMatriculados;
            columns.Add(GetValueColumn(detalle.Ciclo));
            columns.Add(GetValueColumn(detalle.Baja));
            columns.Add(GetValueColumn(detalle.CambioModalidad));
            columns.Add(GetValueColumn(detalle.Desercion));
            columns.Add(GetValueColumn(detalle.Reserva));
            columns.Add(GetValueColumn(detalle.SoloIngles));
            columns.Add(GetValueColumn(detalle.TotalMatriculados));
            columns.Add(GetValueColumn(detalle.TotalAttrition));
            columns.Add(GetValueColumn(porcentajeAttrition));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            return GetValueColumn(value.ToString());
        }
        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        private Column GetValueColumn(double value)
        {
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            return GetValueColumn(string.Format("{0:0.##}%", value));
        }


        #endregion
    }
}
