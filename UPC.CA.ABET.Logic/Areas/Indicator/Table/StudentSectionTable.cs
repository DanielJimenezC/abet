﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class StudentSectionTable : Table
    {
        private Sede _sede;
        private List<sp_CantAlumnosSeccionSede_Result> _campusData;

        public StudentSectionTable(Sede campus, List<sp_CantAlumnosSeccionSede_Result> campusData)
        {
            _campusData = campusData;
            _sede = campus;
        }

        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", _sede.Nombre);
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddSectionHeader();
            AddCourseHeader();
            AddCareersHeaders();
        }

        private void AddCareersHeaders()
        {
            var IdEscuela = _context.Escuela.FirstOrDefault(x => x.Codigo.Equals("EISC")).IdEscuela;
            var careers = _context.Carrera.Where(x => x.IdEscuela == IdEscuela);

            foreach (var career in careers)
            {
                Column column = new Column();
                column.LocalizedName.Add("ES", career.NombreEspanol);
                Header.Columns.Add(column);
            }
        }

        private void AddSectionHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Sección");
            column.LocalizedName.Add("EN", "Section");
            Header.Columns.Add(column);
        }

        private void AddCourseHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Curso");
            column.LocalizedName.Add("EN", "Course");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            AddBodyRow();
        }

        private void AddBodyRow()
        {
            foreach (var data in _campusData)
            {
                List<Column> columns = new List<Column>();

                columns.Add(GetValueColumn(data.Codigo.ToString()));
                columns.Add(GetValueColumn(data.NombreEspanol));
                columns.Add(GetValueColumn(data.SW.ToString()));
                columns.Add(GetValueColumn(data.SI.ToString()));
                columns.Add(GetValueColumn(data.CC.ToString()));

                Row row = new Row();
                row.Columns.AddRange(columns);
                Body.Add(row);
            }            
        }
    
        private Column GetValueColumn(string value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value);
            return column;
        }
        #endregion

    }
}
