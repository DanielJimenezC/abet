﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class CorrectMakeUpExamTable : Table
    {
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Calidad de elaboración de exámenes de recuperación");
            Title.Add("EN", "Make up exams drafting quality");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddAreaHeader();
            AddCorrectMakeUpExamHeader();
            AddNotCorrectMakeUpExamHeader();
            AddCorrectMakeUpExamPercentageHeader();
            AddNotCorrectMakeUpExamPercentageHeader();
        }

        private void AddAreaHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Area");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddCorrectMakeUpExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Examen de recuperación correcto");
            column.LocalizedName.Add("EN", "Correct make up exam");
            Header.Columns.Add(column);
        }

        private void AddNotCorrectMakeUpExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Examen de recuperación incorrecto");
            column.LocalizedName.Add("EN", "Incorrect make up exam");
            Header.Columns.Add(column);
        }

        private void AddCorrectMakeUpExamPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% examenes de recuperación correctos");
            column.LocalizedName.Add("EN", "% correct make up exams");
            Header.Columns.Add(column);
        }

        private void AddNotCorrectMakeUpExamPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% examenes de recuperación incorrectos");
            column.LocalizedName.Add("EN", "% incorrect make up exams");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddBodyRow(area);
            }
        }

        private void AddBodyRow(UnidadAcademica area)
        {
            List<Column> columns = new List<Column>();
            List<CursoPeriodoAcademico> subjects = GetSubjects(area);

            int subjectsCount = subjects.Count;
            int correctSubjectsCount = GetCorrectMakeUpExamCount(subjects);
            int notCorrectSubjectsCount = GetNotCorrectMakeUpExamCount(subjects);
            double percentageCorrect = correctSubjectsCount * 100.0 / subjectsCount;
            double percentageNotCorrect = notCorrectSubjectsCount * 100.0 / subjectsCount;

            columns.Add(GetAreaNameColumn(area));
            columns.Add(GetValueColumn(correctSubjectsCount));
            columns.Add(GetValueColumn(notCorrectSubjectsCount));
            columns.Add(GetValueColumn(percentageCorrect));
            columns.Add(GetValueColumn(percentageNotCorrect));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }

        private int GetCorrectMakeUpExamCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.RecuperacionCorrecto == true ? 1 : 0;
            }

            return count;
        }

        private int GetNotCorrectMakeUpExamCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.RecuperacionCorrecto == false ? 1 : 0;
            }

            return count;
        }

        private List<CursoPeriodoAcademico> GetSubjects(UnidadAcademica area)
        {
            return IndicatorLogic.getCursoPeriodoAcademicoWithFinalExam(area.IdUnidadAcademica);
        }

        private Column GetAreaNameColumn(UnidadAcademica area)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", area.NombreEspanol);
            column.LocalizedName.Add("EN", area.NombreIngles);
            return column;
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var subjects = IndicatorLogic.GetIndicatorCursoPeriodoAcademicoWithMakeUp(TermId);
            var subjectsCount = subjects.Count;
            int correctSubjectsCount = GetCorrectMakeUpExamCount(subjects);
            int notCorrectSubjectsCount = GetNotCorrectMakeUpExamCount(subjects);
            double percentageCorrect = correctSubjectsCount * 100.0 / subjectsCount;
            double percentageNotCorrect = notCorrectSubjectsCount * 100.0 / subjectsCount;

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(correctSubjectsCount));
            Footer.Columns.Add(GetValueColumn(notCorrectSubjectsCount));
            Footer.Columns.Add(GetValueColumn(percentageCorrect));
            Footer.Columns.Add(GetValueColumn(percentageNotCorrect));
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
