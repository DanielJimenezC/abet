﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorAttendanceRangeTable : Table
    {
        private List<AsistenciaDocente> listaAsistencia;

        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Docentes por rango de inasistencias");
            Title.Add("EN", "Professors by non asistance range");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddPendingClassesRangeColumn();
            AddProfessorCountColumn();       
        }

        private void AddProfessorCountColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Núm docentes x sesiones pendientes");
            column.LocalizedName.Add("EN", "Professors per pending classes");
            Header.Columns.Add(column);
        }

        private void AddPendingClassesRangeColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Rango de sesiones pendientes");
            column.LocalizedName.Add("EN", "Pending classes range");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            listaAsistencia = GetAsistenciaDocentes();

            if (listaAsistencia.Count() > 0)
            {
                for (int i = 0; i < 6; i++)
                {
                    AddBodyRow(i);
                }
            }
            else
            {
                AddDefaultBodyRow();
            }
        }

        private void AddDefaultBodyRow()
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetDefaultColumn());
            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private void AddBodyRow(int iterator)
        {
            List<Column> columns = new List<Column>();

            switch (iterator)
            {
                case 0:
                    columns.Add(GetValueColumn(0, 0));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(0, 0)));
                    break;
                case 1:
                    columns.Add(GetValueColumn(1, 2));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(1, 2)));
                    break;
                case 2:
                    columns.Add(GetValueColumn(3, 5));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(3, 5)));
                    break;
                case 3:
                    columns.Add(GetValueColumn(6, 8));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(6, 8)));
                    break;
                case 4:
                    columns.Add(GetValueColumn(9, 11));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(9, 11)));
                    break;
                case 5:
                    columns.Add(GetValueColumn(12, 50));
                    columns.Add(GetValueColumn(GetProfessorPendingSessionsRange(12, 50)));
                    break;
            }

            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private Column GetDefaultColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "NO HAY INFORMACIÓN PARA ESTE CICLO");
            column.LocalizedName.Add("EN", "NO INFORMATION TO SHOW FOR THE CURRENT TERM");
            return column;
        }

        private Column GetValueColumn(int value1, int value2)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value1.ToString() + " - " + value2.ToString());
            return column;
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString() + " Docentes");
            return column;
        }

        private int GetProfessorPendingSessionsRange(int lowerRank, int higherRank)
        {
            return listaAsistencia.Where(x => GetPendingClasses(x) >= lowerRank && GetPendingClasses(x) <= higherRank).Count();
        }

        private int GetPendingClasses(AsistenciaDocente asistencia)
        {
            int pendingClasses;

            if (asistencia.RecuperacionesAsistidas > asistencia.ClasesInasistidas)
                pendingClasses = 0;
            else
                pendingClasses = asistencia.ClasesInasistidas - asistencia.RecuperacionesAsistidas;

            return pendingClasses;
        }

        private List<AsistenciaDocente> GetAsistenciaDocentes()
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;



            return _context.AsistenciaDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu).ToList();
        }

        private int GetMaxPendingClasses()
        {
            return listaAsistencia.Max(x => GetPendingClasses(x));
        }
        #endregion
    }
}
