﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class CorrectFinalExamTable : Table
    {
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
            LoadFooter();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Calidad de elaboración de exámenes finales");
            Title.Add("EN", "Final exams drafting quality");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddAreaHeader();
            AddCorrectFinalExamHeader();
            AddNotCorrectFinalExamHeader();
            AddCorrectFinalExamPercentageHeader();
            AddNotCorrectFinalExamPercentageHeader();
        }

        private void AddAreaHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Area");
            column.LocalizedName.Add("EN", "Area");
            Header.Columns.Add(column);
        }

        private void AddCorrectFinalExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Examen final correcto");
            column.LocalizedName.Add("EN", "Correct final exam");
            Header.Columns.Add(column);
        }

        private void AddNotCorrectFinalExamHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Examen final incorrecto");
            column.LocalizedName.Add("EN", "Incorrect final exam");
            Header.Columns.Add(column);
        }

        private void AddCorrectFinalExamPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% examenes finales correctos");
            column.LocalizedName.Add("EN", "% correct final exams");
            Header.Columns.Add(column);
        }

        private void AddNotCorrectFinalExamPercentageHeader()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "% examenes finales incorrectos");
            column.LocalizedName.Add("EN", "% incorrect final exams");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddBodyRow(area);
            }
        }

        private void AddBodyRow(UnidadAcademica area)
        {
            List<Column> columns = new List<Column>();
            List<CursoPeriodoAcademico> subjects = GetSubjects(area);

            int subjectsCount = subjects.Count;
            int correctSubjectsCount = GetCorrectFinalExamCount(subjects);
            int notCorrectSubjectsCount = GetNotCorrectFinalExamCount(subjects);
            double percentageCorrect = correctSubjectsCount * 100.0 / subjectsCount;
            double percentageNotCorrect = notCorrectSubjectsCount * 100.0 / subjectsCount;

            columns.Add(GetAreaNameColumn(area));
            columns.Add(GetValueColumn(correctSubjectsCount));
            columns.Add(GetValueColumn(notCorrectSubjectsCount));
            columns.Add(GetValueColumn(percentageCorrect));
            columns.Add(GetValueColumn(percentageNotCorrect));

            Row row = new Row();
            row.Columns.AddRange(columns);

            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString());
            return column;
        }

        private Column GetValueColumn(double value)
        {
            Column column = new Column();
            if (double.IsNaN(value))
            {
                value = 0.0;
            }
            column.LocalizedName.Add("VAL", string.Format("{0:0.##}%", value));
            return column;
        }

        private int GetCorrectFinalExamCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.FinalCorrecto == true ? 1 : 0;
            }

            return count;
        }

        private int GetNotCorrectFinalExamCount(List<CursoPeriodoAcademico> subjects)
        {
            int count = 0;

            foreach (var subject in subjects)
            {
                count += subject.FinalCorrecto == false ? 1 : 0;
            }

            return count;
        }

        private List<CursoPeriodoAcademico> GetSubjects(UnidadAcademica area)
        {
            return IndicatorLogic.getCursoPeriodoAcademicoWithFinalExam(area.IdUnidadAcademica);
        }

        private Column GetAreaNameColumn(UnidadAcademica area)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", area.NombreEspanol);
            column.LocalizedName.Add("EN", area.NombreIngles);
            return column;
        }
        #endregion

        #region LOAD_FOOTER
        private void LoadFooter()
        {
            var subjects = IndicatorLogic.GetIndicatorCursoPeriodoAcademicoWithFinal(TermId);
            var subjectsCount = subjects.Count;
            int correctSubjectsCount = GetCorrectFinalExamCount(subjects);
            int notCorrectSubjectsCount = GetNotCorrectFinalExamCount(subjects);
            double percentageCorrect = correctSubjectsCount * 100.0 / subjectsCount;
            double percentageNotCorrect = notCorrectSubjectsCount * 100.0 / subjectsCount;

            Footer.Columns.Add(GetTotalNameColumn());
            Footer.Columns.Add(GetValueColumn(correctSubjectsCount));
            Footer.Columns.Add(GetValueColumn(notCorrectSubjectsCount));
            Footer.Columns.Add(GetValueColumn(percentageCorrect));
            Footer.Columns.Add(GetValueColumn(percentageNotCorrect));
        }

        private Column GetTotalNameColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Total");
            column.LocalizedName.Add("EN", "Total");
            return column;
        }
        #endregion
    }
}
