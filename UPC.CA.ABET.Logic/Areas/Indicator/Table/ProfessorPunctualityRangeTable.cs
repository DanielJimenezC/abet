﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    class ProfessorPunctualityRangeTable : Table
    {
        private List<PuntualidadDocente> listaPuntualidad;
        protected override void LoadData()
        {
            SetTitle();
            LoadHeader();
            LoadBody();
        }

        private void SetTitle()
        {
            Title.Add("ES", "Docentes por rango de tardanza ");
            Title.Add("EN", "Professors by punctuality range ");
        }

        #region LOAD_HEADER
        private void LoadHeader()
        {
            AddMinuteRangeColumn();
            AddProfessorCountColumn();  
        }
        private void AddProfessorCountColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Núm docentes x rango tardanza");
            column.LocalizedName.Add("EN", "Professors per lateness range");
            Header.Columns.Add(column);
        }

        private void AddMinuteRangeColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "Rango minutos de tardanza");
            column.LocalizedName.Add("EN", "Lateness minute range");
            Header.Columns.Add(column);
        }
        #endregion

        #region LOAD_BODY
        private void LoadBody()
        {
            listaPuntualidad = GetPuntualidadDocentes();

            if (listaPuntualidad.Count() > 0)
            {
                for (int i = 0; i < 8; i++)
                {
                    AddBodyRow(i);
                }
            }
            else
            {
                AddDefaultBodyRow();
            }
        }

        private void AddDefaultBodyRow()
        {
            List<Column> columns = new List<Column>();
            columns.Add(GetDefaultColumn());
            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private void AddBodyRow(int iterator)
        {
            List<Column> columns = new List<Column>();

            switch (iterator)
            {
                case 0:
                    columns.Add(GetValueColumn(0, 9));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(0, 9)));
                    break;
                case 1:
                    columns.Add(GetValueColumn(10, 19));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(10, 19)));
                    break;
                case 2:
                    columns.Add(GetValueColumn(20, 29));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(20, 29)));
                    break;
                case 3:
                    columns.Add(GetValueColumn(30, 49));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(30, 49))); 
                    break;
                case 4:
                    columns.Add(GetValueColumn(50, 99));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(50, 99)));              
                    break;
                case 5:
                    columns.Add(GetValueColumn(100, 199));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(100, 199)));
                    break;
                case 6:
                    columns.Add(GetValueColumn(200, 314));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(200, 314)));
                    break;
                case 7:
                    columns.Add(GetValueColumn(315, 500));
                    columns.Add(GetValueColumn(GetProfessorPerMinuteRange(315, 500)));
                    break;
            }

            Row row = new Row();
            row.Columns.AddRange(columns);
            Body.Add(row);
        }

        private Column GetValueColumn(int value)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value.ToString() + " Docentes");
            return column;
        }

        private Column GetDefaultColumn()
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", "NO HAY INFORMACIÓN PARA ESTE CICLO");
            column.LocalizedName.Add("EN", "NO INFORMATION TO SHOW FOR THE CURRENT TERM");
            return column;
        }

        private Column GetValueColumn(int value1, int value2)
        {
            Column column = new Column();
            column.LocalizedName.Add("VAL", value1.ToString() + "' - " + value2.ToString() + "'");
            return column;
        }

        private int GetProfessorPerMinuteRange(int lowerRank, int higherRank)
        {
            return listaPuntualidad.Where(x => x.MinutosTardanza >= lowerRank && x.MinutosTardanza <= higherRank).Count();
        }

        private int GetMaxLateMinutes()
        {
            return listaPuntualidad.Max(x => x.MinutosTardanza);
        }

        private List<PuntualidadDocente> GetPuntualidadDocentes()
        {
            return _context.PuntualidadDocente.Where(x => x.IdSubModalidadPeriodoAcademicoModulo == TermId).ToList();
        }
        #endregion
    }
}
