﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Table
{
    public class TableFactory
    {
        public static Table GetTable(string type)
        {
            switch (type)
            {
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_SCHEDULE:
                    return new InTimeScheduleTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_SYLLABUS:
                    return new InTimeSyllabusTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_VIRTUAL_CLASS:
                    return new InTimeVirtualClassTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.HIRED_PROFESSORS:
                    return new HiredProfessorsTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_CD:
                    return new InTimeCDTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_FINAL_EXAM:
                    return new InTimeFinalExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_MID_TERM_EXAM:
                    return new InTimeMidTermExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_MAKE_UP_EXAM:
                    return new InTimeMakeUpExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_FINAL_EXAM:
                    return new CorrectFinalExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_MID_TERM_EXAM:
                    return new CorrectMidTermExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_MAKE_UP_EXAM:
                    return new CorrectMakeUpExamTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY:
                    return new ProfessorPunctualityTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY_RANKING:
                    return new ProfessorPunctualityRankingTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY_RANGE:
                    return new ProfessorPunctualityRangeTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE:
                    return new ProfessorAttendanceTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE_RANKING:
                    return new ProfessorAttendanceRankingTable();
                case ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE_RANGE:
                    return new ProfessorAttendanceRangeTable();
                default:
                    return null;
            }

        }
    }
}
