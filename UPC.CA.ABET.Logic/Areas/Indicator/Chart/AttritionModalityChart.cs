﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class AttritionModalityChart : Chart
    {
        private int CareerId;
        private Carrera Career;
        private Column SeparatedColumn;
        private Column ModalityChangeColumn;
        private Column DesertionColumn;
        private Column ReservationColumn;
        private Column EnglishColumn;

        public AttritionModalityChart(int careerId)
        {
            Type = "bar";
            CareerId = careerId;
        }

        protected override void LoadData()
        {
            SetTitle();
            AddColumns();

            foreach (var detail in GetAttritionDetails())
            {
                AddCategory(detail);
                AddData(detail);
            }

        }

        private void SetTitle()
        {
            Career = _context.Carrera.FirstOrDefault(x => x.IdCarrera == CareerId);
            Title.Add("ES", Career.NombreEspanol);
            Title.Add("EN", Career.NombreIngles);
        }

        #region ADD_COLUMNS

        private void AddColumns()
        {
            AddSeparatedColumn();
            AddModalityChangeColumn();
            AddDesertionColumn();
            AddReservationColumn();
            AddEnglishColumn();
        }

        private void AddSeparatedColumn()
        {
            SeparatedColumn = new Column();
            SeparatedColumn.LocalizedName.Add("ES", "BAJA");
            SeparatedColumn.LocalizedName.Add("EN", "SEPARATED");
            Columns.Add(SeparatedColumn);
        }

        private void AddModalityChangeColumn()
        {
            ModalityChangeColumn = new Column();
            ModalityChangeColumn.LocalizedName.Add("ES", "CAMBIO DE MODALIDAD");
            ModalityChangeColumn.LocalizedName.Add("EN", "MODALITY CHANGE");
            Columns.Add(ModalityChangeColumn);
        }
        private void AddDesertionColumn()
        {
            DesertionColumn = new Column();
            DesertionColumn.LocalizedName.Add("ES", "DESERCION");
            DesertionColumn.LocalizedName.Add("EN", "DESERTION");
            Columns.Add(DesertionColumn);
        }
        private void AddReservationColumn()
        {
            ReservationColumn = new Column();
            ReservationColumn.LocalizedName.Add("ES", "RESERVA");
            ReservationColumn.LocalizedName.Add("EN", "RESERVATION");
            Columns.Add(ReservationColumn);
        }

        private void AddEnglishColumn()
        {
            EnglishColumn = new Column();
            EnglishColumn.LocalizedName.Add("ES", "SOLO INGLES");
            EnglishColumn.LocalizedName.Add("EN", "ENGLISH");
            Columns.Add(EnglishColumn);
        }

        #endregion

        private IQueryable<AttritionDetalle> GetAttritionDetails()
        {
            return _context.AttritionDetalle.Where(x => x.Attrition.IdSubModalidadPeriodoAcademico == TermId && x.IdCarrera == CareerId);
        }

        private void AddCategory(AttritionDetalle detail)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", detail.Ciclo);
            category.LocalizedName.Add("EN", detail.Ciclo);
            Categories.Add(category);
        }

        #region ADD_DATA

        private void AddData(AttritionDetalle detail)
        {
            AddSeparated(detail);
            AddModalityChange(detail);
            AddDesertion(detail);
            AddReservation(detail);
            AddEnglish(detail);
        }

        private void AddSeparated(AttritionDetalle detail)
        {
            double level = detail.Baja * 100.0 / detail.TotalAttrition;
            SeparatedColumn.Data.Add(level);
        }

        private void AddModalityChange(AttritionDetalle detail)
        {
            double level = detail.CambioModalidad * 100.0 / detail.TotalAttrition;
            ModalityChangeColumn.Data.Add(level);
        }

        private void AddDesertion(AttritionDetalle detail)
        {
            double level = detail.Desercion * 100.0 / detail.TotalAttrition;
            DesertionColumn.Data.Add(level);
        }

        private void AddReservation(AttritionDetalle detail)
        {
            double level = detail.Reserva * 100.0 / detail.TotalAttrition;
            ReservationColumn.Data.Add(level);
        }

        private void AddEnglish(AttritionDetalle detail)
        {
            double level = detail.SoloIngles * 100.0 / detail.TotalAttrition;
            EnglishColumn.Data.Add(level);
        }

        #endregion

    }
}
