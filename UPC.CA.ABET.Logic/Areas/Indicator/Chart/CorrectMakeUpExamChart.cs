﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class CorrectMakeUpExamChart : Chart
    {
        private Column Correct;
        private Column NotCorrect;

        public CorrectMakeUpExamChart()
        {
            AddCorrectColumn();
            AddNotCorrectColumn();
            Type = "bar";
        }

        private void AddNotCorrectColumn()
        {
            NotCorrect = new Column();
            NotCorrect.LocalizedName.Add("ES", "Incorrecto");
            NotCorrect.LocalizedName.Add("EN", "Incorrect");
            Columns.Add(NotCorrect);
        }

        private void AddCorrectColumn()
        {
            Correct = new Column();
            Correct.LocalizedName.Add("ES", "Correcto");
            Correct.LocalizedName.Add("EN", "Correct");
            Columns.Add(Correct);
        }

        private void AddCategory(UnidadAcademica area)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", area.NombreEspanol);
            category.LocalizedName.Add("EN", area.NombreIngles);
            Categories.Add(category);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Calidad de elaboración de exámenes de recuperación");
            Title.Add("EN", "Make up exams drafting quality ");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory(area);
                AddElements(area);
            }
        }

        private void AddElements(UnidadAcademica area)
        {
            var cursosPeriodoAcademicoDeArea = IndicatorLogic.getCursoPeriodoAcademicoWithMakeUpExam(area.IdUnidadAcademica);
            int cursosPeriodoAcademicoCorrecto = 0;
            int cursosPeriodosAcademicoIncorrecto = 0;
            foreach (var curso in cursosPeriodoAcademicoDeArea)
            {
                cursosPeriodoAcademicoCorrecto += GetCorrectMakeUpExam(curso);
                cursosPeriodosAcademicoIncorrecto += GetNotCorrectMakeUpExam(curso);
            }

            Correct.Add(cursosPeriodoAcademicoCorrecto);
            NotCorrect.Add(cursosPeriodosAcademicoIncorrecto);
        }

        private int GetCorrectMakeUpExam(CursoPeriodoAcademico curso)
        {
            return curso.RecuperacionCorrecto == true ? 1 : 0;
        }

        private int GetNotCorrectMakeUpExam(CursoPeriodoAcademico curso)
        {
            return curso.RecuperacionCorrecto == false ? 1 : 0;
        }
    }
}
