﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class StudentPeriodChart : Chart
    {
        private Column NoStudents;

        public StudentPeriodChart()
        {
            Type = "bar";
            AddNoStudents();
        }

        private void AddNoStudents()
        {
            NoStudents = new Column();
            NoStudents.LocalizedName.Add("ES", "Nro. de estudiantes");
            NoStudents.LocalizedName.Add("EN", "No. of students");
            Columns.Add(NoStudents);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Número de estudiantes");
            Title.Add("EN", "Numbers of students");
            try
            {
                var totalQuantityData = _context.FuncionTotalAlumnosCarrera(TermId).ToList();
                var IdEscuela = _context.Escuela.FirstOrDefault(x => x.Codigo.Equals("EISC")).IdEscuela;
                var carreras = _context.Carrera.Where(x => x.IdEscuela == IdEscuela).ToList();

                foreach (var item in carreras)
                {
                    AddCareer(item);
                    var elemento = totalQuantityData.FirstOrDefault(x => x.Carrera.Equals(item.NombreEspanol)).Total_de_Alumnos_por_Carrera;
                    NoStudents.Add(elemento);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void AddCareer(Carrera carrera)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", carrera.NombreEspanol);
            category.LocalizedName.Add("EN", carrera.NombreIngles);
            Categories.Add(category);
        }
    }
}
