﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class InTimeCDChart : Chart
    {
        private Column InTime;
        private Column NotInTime;

        public InTimeCDChart()
        {
            AddInTimeColumn();
            AddNotInTimeColumn();
            Type = "bar";
        }
        private void AddNotInTimeColumn()
        {
            NotInTime = new Column();
            NotInTime.LocalizedName.Add("ES", "No a tiempo");
            NotInTime.LocalizedName.Add("EN", "Not in time");
            Columns.Add(NotInTime);
        }

        private void AddInTimeColumn()
        {
            InTime = new Column();
            InTime.LocalizedName.Add("ES", "A tiempo");
            InTime.LocalizedName.Add("EN", "In time");
            Columns.Add(InTime);
        }

        private void AddCategory(UnidadAcademica area)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", area.NombreEspanol);
            category.LocalizedName.Add("EN", area.NombreIngles);
            Categories.Add(category);
        }
        protected override void LoadData()
        {
            Title.Add("ES", "Entrega de CD");
            Title.Add("EN", "In time CD");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory(area);
                AddElements(area);
            }
        }

        private void AddElements(UnidadAcademica area)
        {
            var cursosPeriodoAcademicoDeArea = IndicatorLogic.getCursoPeriodoAcademico(area.IdUnidadAcademica);
            int cursosPeriodoAcademicoATiempo = 0;
            int cursosPeriodosAcademicoNoATiempo = 0;
            foreach (var curso in cursosPeriodoAcademicoDeArea)
            {
                cursosPeriodoAcademicoATiempo += GetInTimeCD(curso);
                cursosPeriodosAcademicoNoATiempo += GetNotInTimeCD(curso);
            }

            InTime.Add(cursosPeriodoAcademicoATiempo);
            NotInTime.Add(cursosPeriodosAcademicoNoATiempo);
        }

        private int GetInTimeCD(CursoPeriodoAcademico curso)
        {
            return curso.CDATiempo == true ? 1 : 0;
        }

        private int GetNotInTimeCD(CursoPeriodoAcademico curso)
        {
            return curso.CDATiempo == false ? 1 : 0;
        }
    }
}
