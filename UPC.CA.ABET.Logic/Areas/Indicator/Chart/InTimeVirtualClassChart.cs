﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class InTimeVirtualClassChart : Chart
    {
        private Column InTime;
        private Column NotInTime;

        public InTimeVirtualClassChart()
        {
            AddInTimeColumn();
            AddNotInTimeColumn();
            Type = "bar";
        }

        private void AddNotInTimeColumn()
        {
            NotInTime = new Column();
            NotInTime.LocalizedName.Add("ES", "No a tiempo");
            NotInTime.LocalizedName.Add("EN", "Not in time");
            Columns.Add(NotInTime);
        }

        private void AddInTimeColumn()
        {
            InTime = new Column();
            InTime.LocalizedName.Add("ES", "A tiempo");
            InTime.LocalizedName.Add("EN", "In time");
            Columns.Add(InTime);
        }

        private void AddCategory(UnidadAcademica area)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", area.NombreEspanol);
            category.LocalizedName.Add("EN", area.NombreIngles);
            Categories.Add(category);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Revisión de aula virtual");
            Title.Add("EN", "Virtual class check");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory(area);
                AddElements(area);
            }
        }

        private void AddElements(UnidadAcademica area)
        {
            var cursosPeriodoAcademicoDeArea = IndicatorLogic.getCursoPeriodoAcademico(area.IdUnidadAcademica);
            int cursosPeriodoAcademicoATiempo = 0;
            int cursosPeriodosAcademicoNoATiempo = 0;
            foreach (var curso in cursosPeriodoAcademicoDeArea)
            {
                cursosPeriodoAcademicoATiempo += GetInTimeVirtualClass(curso);
                cursosPeriodosAcademicoNoATiempo += GetNotInTimeVirtualClass(curso);
            }

            InTime.Add(cursosPeriodoAcademicoATiempo);
            NotInTime.Add(cursosPeriodosAcademicoNoATiempo);
        }

        private int GetInTimeVirtualClass(CursoPeriodoAcademico curso)
        {
            return curso.AulaVirtualATiempo == true ? 1 : 0;
        }

        private int GetNotInTimeVirtualClass(CursoPeriodoAcademico curso)
        {
            return curso.AulaVirtualATiempo == false ? 1 : 0;
        }
    }
}
