﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Logic.Areas.Indicator;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public abstract class Chart
    {
        protected AbetEntities _context;
        protected int TermId;
        protected IndicatorLogic IndicatorLogic;
        public List<Column> Columns { get; set; }
        public List<Category> Categories { get; set; }
        public string Type { get; set; }
        public Dictionary<string, string> Title { get; set; }

        protected Chart()
        {
            Columns = new List<Column>();
            Categories = new List<Category>();
            Title = new Dictionary<string, string>();
        }

        public void Load(AbetEntities context, int termId)
        {
            _context = context;
            TermId = termId;
            IndicatorLogic = new IndicatorLogic(context);
            LoadData();
        }

        public void Load(AbetEntities entities, string termId)
        {
            Load(entities, int.Parse(termId));
        }

        protected abstract void LoadData();

    }

    public class Category
    {
        public Dictionary<string, string> LocalizedName { get; set; }
        public Category()
        {
            LocalizedName = new Dictionary<string, string>();
        }
    }

    public class Column
    {
        public Dictionary<string, string> LocalizedName { get; set; }
        public List<double?> Data { get; set; }

        public Column()
        {
            Data = new List<double?>();
            LocalizedName = new Dictionary<string, string>();
        }

        public void Add(double? element)
        {
            Data.Add(element);
        }
    }

}