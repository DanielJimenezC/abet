﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class ChartFactory
    {
        public static Chart GetChart(string type)
        {
            switch(type)
            {
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_SCHEDULE:
                    return new InTimeScheduleChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_SYLLABUS:
                    return new InTimeSyllabusChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_VIRTUAL_CLASS:
                    return new InTimeVirtualClassChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.HIRED_PROFESSORS:
                    return new HiredProfessorsChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_CD:
                    return new InTimeCDChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_FINAL_EXAM:
                    return new InTimeFinalExamChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_MID_TERM_EXAM:
                    return new InTimeMidTermExamChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_MAKE_UP_EXAM:
                    return new InTimeMakeUpExamChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_FINAL_EXAM:
                    return new CorrectFinalExamChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_MID_TERM_EXAM:
                    return new CorrectMidTermExamChart();
                case ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_MAKE_UP_EXAM:
                    return new CorrectMakeUpExamChart();
                default:
                    return null;
            }
            
        }
    }
}
