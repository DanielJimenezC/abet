﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class InTimeScheduleChart : Chart
    {
        private Column InTime;
        private Column NotInTime;

        public InTimeScheduleChart ()
        {
            AddInTimeColumn();
            AddNotInTimeColumn();
            Type = "bar";
        }

        private void AddNotInTimeColumn()
        {
            NotInTime = new Column();
            NotInTime.LocalizedName.Add("ES", "No a tiempo");
            NotInTime.LocalizedName.Add("EN", "Not in time");
            Columns.Add(NotInTime);
        }

        private void AddInTimeColumn()
        {
            InTime = new Column();
            InTime.LocalizedName.Add("ES", "A tiempo");
            InTime.LocalizedName.Add("EN", "In time");
            Columns.Add(InTime);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Revisión de horarios");
            Title.Add("EN", "Schedule check");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory(area);
                AddElements(area);
            }
        }

        private void AddCategory(UnidadAcademica area)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", area.NombreEspanol);
            category.LocalizedName.Add("EN", area.NombreIngles);
            Categories.Add(category);
        }

        private void AddElements(UnidadAcademica area)
        {
            var cursosDeArea = IndicatorLogic.GetSubjects(area.IdUnidadAcademica);
            int clasesATiempo = 0;
            int clasesNoATiempo = 0;
            int total = 0;
            foreach (var curso in cursosDeArea)
            {
                clasesATiempo += GetInTimeClasses(curso);
                clasesNoATiempo += GetNotInTimeClasses(curso);
                total += GetClasses(curso);
            }
            InTime.Add(clasesATiempo);
            NotInTime.Add(clasesNoATiempo);
        }

        private int GetInTimeClasses(UnidadAcademica curso)
        {
            return _context.Seccion.Count(x => x.RevisionHorarioATiempo == true && x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == curso.IdCursoPeriodoAcademico);
        }

        private int GetNotInTimeClasses(UnidadAcademica curso)
        {
            return _context.Seccion.Count(x => x.RevisionHorarioATiempo == false && x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == curso.IdCursoPeriodoAcademico);
        }

        private int GetClasses(UnidadAcademica curso)
        {
            return _context.Seccion.Count(x => x.CursoPeriodoAcademico.IdCursoPeriodoAcademico == curso.IdCursoPeriodoAcademico);
        }
    }
}
