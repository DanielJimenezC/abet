﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class StudentCampusChart : Chart
    {
        private Column MO;
        private Column SI;
        private Column VI;
        private Column SM;

        public StudentCampusChart()
        {
            Type = "bar";
            AddCampuses();
        }

        private void AddCampuses()
        {
            MO = new Column();
            MO.LocalizedName.Add("ES", "Monterrico");
            MO.LocalizedName.Add("EN", "Monterrico");
            Columns.Add(MO);
            SI = new Column();
            SI.LocalizedName.Add("ES", "San Isidro");
            SI.LocalizedName.Add("EN", "San Isidro");
            Columns.Add(SI);
            SM = new Column();
            SM.LocalizedName.Add("ES", "San Miguel");
            SM.LocalizedName.Add("EN", "San Miguel");
            Columns.Add(SM);
            VI = new Column();
            VI.LocalizedName.Add("ES", "Villa");
            VI.LocalizedName.Add("EN", "Villa");
            Columns.Add(VI);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Nro. de estudiantes por sede");
            Title.Add("EN", "No. of students by campus");
            try
            {
                var totalQuantityData = _context.FuncionTotalAlumnosCarrera(TermId).ToList();
                var IdEscuela = _context.Escuela.FirstOrDefault(x => x.Codigo.Equals("EISC")).IdEscuela;
                var carreras = _context.Carrera.Where(x => x.IdEscuela == IdEscuela).ToList();
                var IdMO = _context.Sede.Where(x => x.Nombre.Equals("MONTERRICO")).ToList().First().IdSede;
                var IdSI = _context.Sede.Where(x => x.Nombre.Equals("SAN ISIDRO")).ToList().First().IdSede;
                var IdSM = _context.Sede.Where(x => x.Nombre.Equals("SAN MIGUEL")).ToList().First().IdSede;
                var IdVL = _context.Sede.Where(x => x.Nombre.Equals("VILLA")).ToList().First().IdSede;

                foreach (var item in carreras)
                {
                    AddCareer(item);
                    var MOQuantity = _context.sp_CantAlumnosSede(TermId, IdMO, item.IdCarrera).ToList().First();
                    MO.Add(MOQuantity);
                    var SIQuantity = _context.sp_CantAlumnosSede(TermId, IdSI, item.IdCarrera).ToList().First();
                    SI.Add(SIQuantity);
                    var SMQuantity = _context.sp_CantAlumnosSede(TermId, IdSM, item.IdCarrera).ToList().First();
                    SM.Add(SMQuantity);
                    var VLQuantity = _context.sp_CantAlumnosSede(TermId, IdVL, item.IdCarrera).ToList().First();
                    VI.Add(VLQuantity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddCareer(Carrera carrera)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", carrera.NombreEspanol);
            category.LocalizedName.Add("EN", carrera.NombreIngles);
            Categories.Add(category);
        }
    }
}
