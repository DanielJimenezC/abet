﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class AttritionReservationChart : Chart
    {
        public AttritionReservationChart ()
        {
            Type = "";
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Ratios por reserva");
            Title.Add("EN", "Reservation ratios");

            var ciclos = new SortedSet<string>();
            foreach (var career in _context.Carrera.Where(x => x.AttritionDetalle.Any(y => y.Attrition.IdSubModalidadPeriodoAcademico == TermId)))
            {
                AddCareerColumn(career, ciclos);
            }

            foreach (var ciclo in ciclos)
            {
                Category category = new Category();
                category.LocalizedName.Add("ES", ciclo);
                category.LocalizedName.Add("EN", ciclo);
                Categories.Add(category);     
            }

            var minColumn = new Column();
            var minValue = ciclos.Count;
            foreach (var column in Columns)
            {
                if (column.Data.Count < minValue)
                {
                    minColumn = column;
                    minValue = minColumn.Data.Count;
                }
            }

            int insertar = ciclos.Count - minColumn.Data.Count;
            if (insertar > 0)
            {
                for (int i=0; i<insertar; i++)
                {
                    minColumn.Data.Insert(0, null);
                }
            }

        }

        private void AddCareerColumn(Carrera career, SortedSet<string> ciclos)
        {
            Column column = new Column();
            column.LocalizedName.Add("ES", career.NombreEspanol);
            column.LocalizedName.Add("EN", career.NombreIngles);

            foreach(var detalle in _context.AttritionDetalle.Where(x => x.IdCarrera == career.IdCarrera && x.Attrition.IdSubModalidadPeriodoAcademico == TermId))
            {
                double ratio = detalle.Reserva * 100.0 / detalle.TotalAttrition;
                column.Data.Add(ratio);
                ciclos.Add(detalle.Ciclo);         
            }

            Columns.Add(column);
        }
    }
}
