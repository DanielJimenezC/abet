﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class HiredProfessorsChart : Chart
    {
        private Column InTime;
        private Column NotInTime;

        public HiredProfessorsChart()
        {
            AddInTimeColumn();
            AddNotInTimeColumn();
            Type = "pie";
        }

        private void AddNotInTimeColumn()
        {
            NotInTime = new Column();
            NotInTime.LocalizedName.Add("ES", "No a tiempo");
            NotInTime.LocalizedName.Add("EN", "Not in time");
            Columns.Add(NotInTime);
        }

        private void AddInTimeColumn()
        {
            InTime = new Column();
            InTime.LocalizedName.Add("ES", "A tiempo");
            InTime.LocalizedName.Add("EN", "In time");
            Columns.Add(InTime);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Docentes contratados");
            Title.Add("EN", "Hired professors");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory();
                AddElements(area);
            }
        }

        private void AddCategory()
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", "Profesores");
            category.LocalizedName.Add("EN", "Professors");
            Categories.Add(category);
        }

        private void AddElements(UnidadAcademica area)
        {

            int submoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var indicadorDocente = _context.IndicadorDocente.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == submoda);

            if (indicadorDocente == null)
            {
                indicadorDocente = CreateProfessorsIndicator();
            }

            var docentesATiempo = indicadorDocente.DocentesContratadoTiempo;
            var docentesNoATiempo = indicadorDocente.DocentesContratadoDestiempo;
    
            InTime.Add(docentesATiempo);
            NotInTime.Add(docentesNoATiempo);
        }

        private IndicadorDocente CreateProfessorsIndicator()
        {
            int submoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == TermId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            IndicadorDocente indicadorDocente = new IndicadorDocente
            {
                DocentesContratadoDestiempo = 0,
                DocentesContratadoTiempo = 0,
                IdSubModalidadPeriodoAcademico = submoda
            };
            _context.IndicadorDocente.Add(indicadorDocente);
            _context.SaveChanges();
            return indicadorDocente;
        }
    }
}
