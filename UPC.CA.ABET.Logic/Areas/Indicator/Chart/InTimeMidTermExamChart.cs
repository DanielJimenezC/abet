﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class InTimeMidTermExamChart : Chart
    {
        private Column InTime;
        private Column NotInTime;

        public InTimeMidTermExamChart()
        {
            Type = "bar";
            AddInTimeColumn();
            AddNotInTimeColumn();
        }
        private void AddNotInTimeColumn()
        {
            NotInTime = new Column();
            NotInTime.LocalizedName.Add("ES", "No a tiempo");
            NotInTime.LocalizedName.Add("EN", "Not in time");
            Columns.Add(NotInTime);
        }

        private void AddInTimeColumn()
        {
            InTime = new Column();
            InTime.LocalizedName.Add("ES", "A tiempo");
            InTime.LocalizedName.Add("EN", "In time");
            Columns.Add(InTime);
        }

        protected override void LoadData()
        {
            Title.Add("ES", "Entrega de exámenes parciales");
            Title.Add("EN", "Mid term exams check");

            foreach (var area in IndicatorLogic.GetAreas(TermId))
            {
                AddCategory(area);
                AddElements(area);
            }
        }

        private void AddCategory(UnidadAcademica area)
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", area.NombreEspanol);
            category.LocalizedName.Add("EN", area.NombreIngles);
            Categories.Add(category);
        }

        private void AddElements(UnidadAcademica area)
        {
            var cursosPeriodoAcademicoDeArea = IndicatorLogic.getCursoPeriodoAcademicoWithMidTermExam(area.IdUnidadAcademica);
            int cursosPeriodoAcademicoATiempo = 0;
            int cursosPeriodosAcademicoNoATiempo = 0;

            foreach (var curso in cursosPeriodoAcademicoDeArea)
            {
                cursosPeriodoAcademicoATiempo += GetInTimeMidTerm(curso);
                cursosPeriodosAcademicoNoATiempo += GetNotInTimeMidTerm(curso);
            }

            InTime.Add(cursosPeriodoAcademicoATiempo);
            NotInTime.Add(cursosPeriodosAcademicoNoATiempo);
        }

        private int GetInTimeMidTerm(CursoPeriodoAcademico curso)
        {
            return curso.ParcialATiempo == true ? 1 : 0;
        }

        private int GetNotInTimeMidTerm(CursoPeriodoAcademico curso)
        {
            return curso.ParcialATiempo == false ? 1 : 0;
        }
    }
}
