﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class MeetingsChart : Chart
    {
        private string academicUnitType;

        private List<UnidadAcademica> _academicUnits;

        private Column _rescheduledColumn;
        private Column _canceledColumn;
        private Column _accordingToScheduleColumn;

        public MeetingsChart(string academicUnitType)
        {
            Type = "stacked";
            this.academicUnitType = academicUnitType;
        }


        protected override void LoadData()
        {
            SetTitle();
            _academicUnits = GetAreaAcademicUnits();
            AddColumns();

            foreach (var term in _academicUnits)
            {
                AddCategory(term);
                AddDataToColumns(term);
            }
        }

        private void AddColumns()
        {
            AddReScheduledColunm();
            AddCanceledColumn();
            AddAccordingToScheduleColumn();
        }

        private void AddReScheduledColunm()
        {
            _rescheduledColumn = new Column();
            _rescheduledColumn.LocalizedName.Add("ES", "Reprogramadas");
            _rescheduledColumn.LocalizedName.Add("EN", "Rescheduled");
            Columns.Add(_rescheduledColumn);
        }

        private void AddCanceledColumn()
        {
            _canceledColumn = new Column();
            _canceledColumn.LocalizedName.Add("ES", "Canceladas");
            _canceledColumn.LocalizedName.Add("EN", "Canceled");
            Columns.Add(_canceledColumn);
        }

        private void AddAccordingToScheduleColumn()
        {
            _accordingToScheduleColumn = new Column();
            _accordingToScheduleColumn.LocalizedName.Add("ES", "Según cronograma");
            _accordingToScheduleColumn.LocalizedName.Add("EN", "According to schedule");
            Columns.Add(_accordingToScheduleColumn);
        }

        private void AddCategory(UnidadAcademica academicUnit)
        {
            var termCategory = new Category();
            termCategory.LocalizedName.Add("ES", academicUnit.NombreEspanol);
            termCategory.LocalizedName.Add("EN", academicUnit.NombreIngles);
            Categories.Add(termCategory);
        }

        private void AddDataToColumns(UnidadAcademica academicUnit)
        {
            _rescheduledColumn.Data.Add(GetRescheduled(academicUnit));
            _canceledColumn.Data.Add(GetCanceled(academicUnit));
            _accordingToScheduleColumn.Data.Add(GetAccordingToSchedule(academicUnit));
        }

        private int GetRescheduled(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "REPROGRAMADO" && x.FueEjecutada == true
            && x.EsExtraordinaria == false);
        }

        private int GetCanceled(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "CANCELADO"
            && x.EsExtraordinaria == false);
        }

        private int GetAccordingToSchedule(UnidadAcademica academicUnit)
        {
            return _context.Reunion.Count(x =>
            x.PreAgenda != null &&
            x.PreAgenda.IdUnidaAcademica == academicUnit.IdUnidadAcademica &&
            x.PreAgenda.TipoPreagenda == "RCO"
            && x.EstadoReunion.Estado == "PROGRAMADO" && x.FueEjecutada == true
            && x.EsExtraordinaria == false);
        }

        private List<UnidadAcademica> GetAreaAcademicUnits()
        {
            return _context.UnidadAcademica.Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == TermId && x.Tipo == academicUnitType).ToList();
        }

        private void SetTitle()
        {
            if (academicUnitType == "AREA")
            {
                Title.Add("ES", "Reuniones por área");
                Title.Add("EN", "Meetings per area");
            }
            else if (academicUnitType == "SUBAREA")
            {
                Title.Add("ES", "Reuniones por sub área");
                Title.Add("EN", "Meetings per sub area");
            }
        }
    }
}
