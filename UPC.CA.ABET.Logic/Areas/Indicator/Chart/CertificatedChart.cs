﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class CertificatedChart: Chart
    {
        private const int LAST_N_TERMS = 5;
        private Column Certificated;
        private Column NonCertificated;
        
        private Carrera _career;
        private int _careerId;

        private List<PeriodoAcademico> _academicPeriods;

        public CertificatedChart(int careerId)
        {
            AddInTimeColumn();
            AddNotInTimeColumn();
            Type = "pie";
            _careerId = careerId;
        }

        private void AddNotInTimeColumn()
        {
            NonCertificated = new Column();
            NonCertificated.LocalizedName.Add("ES", "Pendientes por titular");
            NonCertificated.LocalizedName.Add("EN", "Not certificated");
            Columns.Add(NonCertificated);
        }

        private void AddInTimeColumn()
        {
            Certificated = new Column();
            Certificated.LocalizedName.Add("ES", "Titulados");
            Certificated.LocalizedName.Add("EN", "Certificated");
            Columns.Add(Certificated);
        }

        protected override void LoadData()
        {
            _career = GetCareer();
            _academicPeriods = GetLastNTerms(LAST_N_TERMS);

            Title.Add("ES", "Alumnos titulados de " + _career.NombreEspanol);
            Title.Add("EN", _career.NombreIngles + " certificated students");

            AddCategory();
            AddElements();
        }

        private Carrera GetCareer()
        {
            return _context.Carrera.FirstOrDefault(x => x.IdCarrera == _careerId);
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).ToList();
        }

        private void AddCategory()
        {
            Category category = new Category();
            category.LocalizedName.Add("ES", "Alumnos graduados");
            category.LocalizedName.Add("EN", "Certificated students");
            Categories.Add(category);
        }

        private void AddElements()
        {
            var terms = _academicPeriods.Select(t => t.CicloAcademico);
            var graduated = _context.Alumno.Where( x => 
                x.IdCarreraEgreso == _careerId && 
                x.AnioEgreso != null && 
                terms.Contains(x.AnioEgreso))
                .ToList();
            var certificated = graduated.Count(x => x.AnioTitulacion != null && terms.Contains(x.AnioTitulacion));
            var nonCertificated = graduated.Count() - certificated;
    
            Certificated.Add(certificated);
            NonCertificated.Add(nonCertificated);
        }
    }
}
