﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    public class ProfessorAttendanceChart : Chart
    {

        private const int LAST_N_TERMS = 5;
        private Column _attendanceColumn;
        private Column _nonAttendanceColumn;
        public ProfessorAttendanceChart()
        {
            Type = "stacked";
        }

        protected override void LoadData()
        {
            SetTitle();
            AddColumns();

            foreach (var term in GetLastNTerms(LAST_N_TERMS))
            {
                AddCategory(term);
                AddDataToColumns(term);
            }
        }

        private void SetTitle()
        {
            Title.Add("ES", "Asistencia de docentes");
            Title.Add("EN", "Professor attendance");
        }

        private void AddColumns()
        {
            AddAttendanceColumn();
            AddNonattendanceColumn();
        }

        private void AddAttendanceColumn()
        {
            _attendanceColumn = new Column();
            _attendanceColumn.LocalizedName.Add("ES", "Sin sesiones pendientes");
            _attendanceColumn.LocalizedName.Add("EN", "With no pending sessions");
            Columns.Add(_attendanceColumn);
        }

        private void AddNonattendanceColumn()
        {
            _nonAttendanceColumn = new Column();
            _nonAttendanceColumn.LocalizedName.Add("ES", "Con sesiones pendientes");
            _nonAttendanceColumn.LocalizedName.Add("EN", "With pending sessions");
            Columns.Add(_nonAttendanceColumn);
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).ToList();
        }

        private void AddCategory(PeriodoAcademico term)
        {
            var termCategory = new Category();
            termCategory.LocalizedName.Add("ES", term.CicloAcademico);
            termCategory.LocalizedName.Add("EN", term.CicloAcademico);
            Categories.Add(termCategory);
        }

        private void AddDataToColumns(PeriodoAcademico term)
        {
            int professorsCount = GetProfessorsByTerm(term);
            int professorsWithPendingClasses = GetProfessorsWithPendingClasses(term);
            int professorsWithNoPendingClasses = professorsCount - professorsWithPendingClasses;

            _attendanceColumn.Data.Add(professorsWithNoPendingClasses);
            _nonAttendanceColumn.Data.Add(professorsWithPendingClasses);
        }

        private int GetProfessorsByTerm(PeriodoAcademico term)
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == term.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            return _context.AsistenciaDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu);
        }

        private int GetProfessorsWithPendingClasses(PeriodoAcademico term)
        {
            int idsubmoda = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == term.IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            int idsumodapamodu = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idsubmoda).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            return _context.AsistenciaDocente.Count(
                x => x.IdSubModalidadPeriodoAcademicoModulo == idsumodapamodu && 
                x.ClasesInasistidas - x.RecuperacionesAsistidas > 0);
        }

    }
}
