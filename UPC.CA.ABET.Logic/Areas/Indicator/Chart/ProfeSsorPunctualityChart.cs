﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Chart
{
    class ProfessorPunctualityChart : Chart
    {
        private const int LAST_N_TERMS = 5;
        private Column _punctualityColumn;
        private Column _latenessColumn;
        public ProfessorPunctualityChart()
        {
            Type = "stacked";
        }

        protected override void LoadData()
        {
            SetTitle();
            AddColumns();

            foreach (var term in GetLastNTerms(LAST_N_TERMS))
            {
                AddCategory(term);
                AddDataToColumns(term);
            }
        }

        private void SetTitle()
        {
            Title.Add("ES", "Puntualidad de docentes");
            Title.Add("EN", "Professor punctuality");
        }

        private void AddColumns()
        {
            AddPunctualityColumn();
            AddLatenessColumn();
        }

        private void AddPunctualityColumn()
        {
            _punctualityColumn = new Column();
            _punctualityColumn.LocalizedName.Add("ES", "Profesores puntuales");
            _punctualityColumn.LocalizedName.Add("EN", "Punctual professors");
            Columns.Add(_punctualityColumn);
        }

        private void AddLatenessColumn()
        {
            _latenessColumn = new Column();
            _latenessColumn.LocalizedName.Add("ES", "Profesores impuntuales");
            _latenessColumn.LocalizedName.Add("EN", "Tardy professors");
            Columns.Add(_latenessColumn);
        }

        private List<PeriodoAcademico> GetLastNTerms(int n)
        {
            List<PeriodoAcademico> terms = new List<PeriodoAcademico>();
            foreach (var term in _context.PeriodoAcademico.OrderBy(x => x.CicloAcademico))
            {
                terms.Add(term);
                if (term.IdPeriodoAcademico == TermId)
                {
                    break;
                }
            }
            return terms.Take(n).ToList();
        }

        private void AddCategory(PeriodoAcademico term)
        {
            var termCategory = new Category();
            termCategory.LocalizedName.Add("ES", term.CicloAcademico);
            termCategory.LocalizedName.Add("EN", term.CicloAcademico);
            Categories.Add(termCategory);
        }

        private void AddDataToColumns(PeriodoAcademico term)
        {
            int professorsCount = GetProfessorsByTerm(term);
            int tardyProfessors = GetTardyProfessors(term);
            int punctualProfessors = professorsCount - tardyProfessors;

            _punctualityColumn.Data.Add(punctualProfessors);
            _latenessColumn.Data.Add(tardyProfessors);
        }

        private int GetProfessorsByTerm(PeriodoAcademico term)
        {
            return _context.PuntualidadDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo == term.IdPeriodoAcademico);
        }

        private int GetTardyProfessors(PeriodoAcademico term)
        {
            return _context.PuntualidadDocente.Count(x => x.IdSubModalidadPeriodoAcademicoModulo  == term.IdPeriodoAcademico && x.MinutosTardanza > 0);
        }

    }
}
