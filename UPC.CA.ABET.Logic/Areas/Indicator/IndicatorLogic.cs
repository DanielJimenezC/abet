﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.Web;

namespace UPC.CA.ABET.Logic.Areas.Indicator
{
    public class IndicatorLogic
    {
        private AbetEntities _context;

        public IndicatorLogic(AbetEntities entities)
        {
            this._context = entities;
        }

        public List<UnidadAcademica> GetAreas(int termId)
        {
            return _context.UnidadAcademica
                    .Where(x => x.Tipo == "AREA" && x.SubModalidadPeriodoAcademico.PeriodoAcademico.IdPeriodoAcademico == termId)
                    .Select(x => x).ToList();
        }
        public UnidadAcademica GetUnidadAcademica(int id)
        {
            return _context.UnidadAcademica.FirstOrDefault(x => x.IdUnidadAcademica == id);
        }
        public List<PeriodoAcademico> GetAcademicPeriods(int IdModalidad)
        {
			var LstPa = (from a in _context.SubModalidadPeriodoAcademico
						 join b in _context.SubModalidad on a.IdSubModalidad equals b.IdSubModalidad
						 join c in _context.Modalidad on b.IdModalidad equals c.IdModalidad
						 join d in _context.PeriodoAcademico on a.IdPeriodoAcademico equals d.IdPeriodoAcademico
						 where c.IdModalidad == IdModalidad
						 select d).ToList();

			return LstPa.OrderByDescending(x => x.CicloAcademico).ToList();					
        }

        public List<Modulo> GetModulesforPeriod(int IdPeriodoAcademico)
        {
            var LstMo = (from a in _context.SubModalidadPeriodoAcademico
                         join b in _context.SubModalidadPeriodoAcademicoModulo on a.IdSubModalidadPeriodoAcademico equals b.IdSubModalidadPeriodoAcademico
                         join c in _context.Modulo on b.IdModulo equals c.IdModulo
                         where a.IdPeriodoAcademico == IdPeriodoAcademico
                         select c).ToList();

            return LstMo.OrderBy(x => x.IdModulo).ToList();
        }

        public List<PeriodoAcademico> GetActiveAcademicPeriods(int parModalidadId)
        {
            return GetAcademicPeriods(parModalidadId).Where(x => x.Estado == "ACT").ToList();
        }

        

        public List<UnidadAcademica> GetSubjects(int areaId)
        {
            return _context.UnidadAcademica.Where(x => x.IdUnidadAcademica == areaId).Traverse(x => x.UnidadAcademica1).Where(x => x.Tipo == "CURSO").ToList();
        }

        public List<Seccion> GetClasses(int cursoPeriodoAcademicoId)
        {
            return _context.Seccion
                ///  .Where(x => x.IdCursoPeriodoAcademico == cursoPeriodoAcademicoId)
                .Where(x => x.IdCursoPeriodoAcademico == cursoPeriodoAcademicoId)
                .OrderBy(x => x.Codigo)
                .ToList();
        }

        public void saveSeccion(List<Seccion> listaSecciones)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                foreach (Seccion seccion in listaSecciones)
                {
                    var seccionEntities = _context.Seccion.FirstOrDefault(x => x.IdSeccion == seccion.IdSeccion);
                    _context.SaveChanges();
                }

                dbContextTransaction.Commit();
            }
        }

        public List<CursoPeriodoAcademico> GetIndicatorCursoPeriodoAcademico(int termId)
        {
            var cursosPeriodoAcademico = new List<CursoPeriodoAcademico>();
            int smpa = _context.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == termId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            //int smpam = _context.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == smpa).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            var unidadesAcademicas = _context.UnidadAcademica.Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == smpa).Where(x => x.Tipo == "CURSO").ToList();
            unidadesAcademicas.ForEach(unidad =>
            {
                cursosPeriodoAcademico.Add(_context.CursoPeriodoAcademico
                    .Where(x => x.IdCursoPeriodoAcademico == unidad.IdCursoPeriodoAcademico)
                    .FirstOrDefault());
            });

            return cursosPeriodoAcademico;
        }

        public List<CursoPeriodoAcademico> GetIndicatorCursoPeriodoAcademicoWithFinal(int termId)
        {
            return GetIndicatorCursoPeriodoAcademico(termId).Where(x => x.TieneFinal == true).ToList();
        }

        public List<CursoPeriodoAcademico> GetIndicatorCursoPeriodoAcademicoWithMakeUp(int termId)
        {
            return GetIndicatorCursoPeriodoAcademico(termId).Where(x => x.TieneFinal == true || x.TieneParcial == true).ToList();
        }

        public List<CursoPeriodoAcademico> GetIndicatorCursoPeriodoAcademicoWithMidTerm(int termId)
        {
            return GetIndicatorCursoPeriodoAcademico(termId).Where(x => x.TieneParcial == true).ToList();
        }

        public List<CursoPeriodoAcademico> getCursoPeriodoAcademico(int areaId)
        {
            return _context.UnidadAcademica
                .Where(x => x.IdUnidadAcademica == areaId)
                .Traverse(x => x.UnidadAcademica1)
                .Where(x => x.Tipo == "CURSO")
                .Select(x => x.CursoPeriodoAcademico)
                .OrderBy(x => x.Curso.Codigo).ToList();
        }

        public List<CursoPeriodoAcademico> getCursoPeriodoAcademicoWithFinalExam(int areaId)
        {
              return getCursoPeriodoAcademico(areaId).Where(x => x.TieneFinal == true).ToList();
        }

        public List<CursoPeriodoAcademico> getCursoPeriodoAcademicoWithMidTermExam(int areaId)
        {
            return getCursoPeriodoAcademico(areaId).Where(x => x.TieneParcial == true).ToList();
        }

        public List<CursoPeriodoAcademico> getCursoPeriodoAcademicoWithMakeUpExam(int areaId)
        {
            return getCursoPeriodoAcademico(areaId).Where(x => x.TieneParcial == true || x.TieneFinal == true).ToList();
        }

        public int GetProfessorsCount(int IdSMPAM)
        {
            AbetEntities context = new AbetEntities();

            int ModuloId = context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == IdSMPAM).IdModulo;
            int SMPAId = context.SubModalidadPeriodoAcademicoModulo.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademicoModulo == IdSMPAM).IdSubModalidadPeriodoAcademico;

            var Docente = (from a in context.DocenteSeccion
                            join b in context.Docente on a.IdDocente equals b.IdDocente
                            join c in context.Seccion on a.IdSeccion equals c.IdSeccion
                            join d in context.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals d.IdCursoPeriodoAcademico
                            join e in context.SubModalidadPeriodoAcademico on d.IdSubModalidadPeriodoAcademico equals e.IdSubModalidadPeriodoAcademico
                            where d.IdSubModalidadPeriodoAcademico == SMPAId && c.IdModulo == ModuloId
                           select b).Distinct();

            int cantidad = Docente.Count();


            return cantidad;
        }

        public bool IsChild(UnidadAcademica parent, UnidadAcademica child)
        {
            if (child.UnidadAcademica2.Equals(null))
            {
                return false;
            }
            else if (child.UnidadAcademica2.Equals(parent))
            {
                return true;
            }
            else
            {
                return IsChild(parent, child.UnidadAcademica2);
            }
        }
    }
}