﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class InTimeExamsReport : Dashboard
    {
        public InTimeExamsReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var midTermChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_MID_TERM_EXAM);
            midTermChart.Load(entities, termId);

            var midTermTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_MID_TERM_EXAM);
            midTermTable.Load(entities, termId);

            var finalChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_FINAL_EXAM);
            finalChart.Load(entities, termId);

            var finalTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_FINAL_EXAM);
            finalTable.Load(entities, termId);

            var makeUpChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.IN_TIME_MAKE_UP_EXAM);
            makeUpChart.Load(entities, termId);

            var makeUpTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_MAKE_UP_EXAM);
            makeUpTable.Load(entities, termId);

            Charts.Add(midTermChart);
            Charts.Add(finalChart);
            Charts.Add(makeUpChart);

            Tables.Add(midTermTable);
            Tables.Add(finalTable);
            Tables.Add(makeUpTable);
        }
    }
}
