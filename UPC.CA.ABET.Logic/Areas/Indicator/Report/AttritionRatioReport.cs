﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class AttritionRatioReport : Dashboard
    {
        public AttritionRatioReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var careers = _context.Carrera.Where(x => x.AttritionDetalle.Any(y => y.Attrition.IdSubModalidadPeriodoAcademico == termId));


            var attritionSeparated = new AttritionSeparatedChart();
            attritionSeparated.Load(entities, termId);
            Charts.Add(attritionSeparated);

            var attritionModalityChange = new AttritionModalityChangeChart();
            attritionModalityChange.Load(entities, termId);
            Charts.Add(attritionModalityChange);


            var attritionDesertion = new AttritionDesertionChart();
            attritionDesertion.Load(entities, termId);
            Charts.Add(attritionDesertion);

            var attritionReservation = new AttritionReservationChart();
            attritionReservation.Load(entities, termId);
            Charts.Add(attritionReservation);

            foreach (var career in careers)
            {
                var table = new AttritionRatioTable(career.IdCarrera);
                table.Load(entities, termId);
                Tables.Add(table);
            }
        }
    }
}
