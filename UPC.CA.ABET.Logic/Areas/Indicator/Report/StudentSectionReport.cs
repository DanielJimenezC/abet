﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    class StudentSectionReport : Dashboard
    {
        public StudentSectionReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var campuses = _context.Sede.ToList();
            var totalData = _context.sp_CantAlumnosSeccionSede(termId).ToList();

            foreach (var campus in campuses)
            {
                var campusData = totalData.Where(x => x.Nombre == campus.Nombre).ToList();
                var table = new StudentSectionTable(campus, campusData);
                table.Load(entities, termId);
                Tables.Add(table);
            }
        }
    }
}
