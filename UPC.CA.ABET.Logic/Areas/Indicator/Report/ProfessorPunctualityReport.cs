﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class ProfessorPunctualityReport : Dashboard
    {
        public ProfessorPunctualityReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var chart = new ProfessorPunctualityChart();
            chart.Load(entities, termId);

            var punctualityTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY);
            punctualityTable.Load(entities, termId);

            var punctualityRangeTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY_RANGE);
            punctualityRangeTable.Load(entities, termId);

            var punctualityRankingTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_PUNCTUALITY_RANKING);
            punctualityRankingTable.Load(entities, termId);

            Charts.Add(chart);

            Tables.Add(punctualityTable);
            Tables.Add(punctualityRangeTable);
            Tables.Add(punctualityRankingTable);
        }
    }
}
