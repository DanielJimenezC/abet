﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public abstract class Dashboard
    {
        protected AbetEntities _context;
        protected int TermId;
        public List<Chart.Chart> Charts { get; set; }
        public List<Table.Table> Tables { get; set; }

        public Dashboard(AbetEntities context, int termId)
        {
            _context = context;
            TermId = termId;
            Charts = new List<Chart.Chart>();
            Tables = new List<Table.Table>();
        }
    }
}
