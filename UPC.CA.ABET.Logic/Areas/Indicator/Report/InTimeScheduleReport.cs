﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class InTimeScheduleReport: Dashboard
    {
        public InTimeScheduleReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var chart = new InTimeScheduleChart();
            chart.Load(entities, termId);
            var table = new InTimeScheduleTable();
            table.Load(entities, termId);

            Charts.Add(chart);
            Tables.Add(table);
        }
    }
}
