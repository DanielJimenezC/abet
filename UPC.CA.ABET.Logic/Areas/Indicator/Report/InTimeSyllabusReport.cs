﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class InTimeSyllabusReport : Dashboard
    {
        public InTimeSyllabusReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var chart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_SYLLABUS);
            chart.Load(entities, termId);
            var table = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.IN_TIME_SYLLABUS);
            table.Load(entities, termId);
            Charts.Add(chart);
            Tables.Add(table);
        }
    }
}
