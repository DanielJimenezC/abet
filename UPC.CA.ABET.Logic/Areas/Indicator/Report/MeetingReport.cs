﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class MeetingReport : Dashboard
    {
        public MeetingReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var meetingsPerAreaTable = new MeetingsTable("AREA");
            meetingsPerAreaTable.Load(entities, termId);
            Tables.Add(meetingsPerAreaTable);

            var meetingsPerAreachart = new MeetingsChart("AREA");
            meetingsPerAreachart.Load(entities, termId);
            Charts.Add(meetingsPerAreachart);

            var meetingsPerSubAreaTable = new MeetingsTable("SUBAREA");
            meetingsPerSubAreaTable.Load(entities, termId);
            Tables.Add(meetingsPerSubAreaTable);

            var meetinsPerSubAreachart = new MeetingsChart("SUBAREA");
            meetinsPerSubAreachart.Load(entities, termId);
            Charts.Add(meetinsPerSubAreachart);
        }
    }
}
