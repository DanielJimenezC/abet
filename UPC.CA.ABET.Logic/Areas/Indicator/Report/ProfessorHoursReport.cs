﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class ProfessorHoursReport : Dashboard
    {
        public ProfessorHoursReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var table = new ProfessorHoursTable();
            table.Load(entities, termId);
            Tables.Add(table);
        }
    }
}
