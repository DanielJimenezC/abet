﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class CohortStudentReport : Dashboard
    {
        public CohortStudentReport(AbetEntities entities, int termId) : base(entities, termId)
        {            
            var IdEscuela = _context.Escuela.FirstOrDefault(x => x.Codigo.Equals("EISC")).IdEscuela;
            var careers = _context.Carrera.Where(x => x.IdEscuela == IdEscuela);

            foreach (var career in careers)
            {
                var table = new CohortStudentTable(termId, career.IdCarrera);
                table.Load(entities, termId);
                Tables.Add(table);
            }
        }
    }
}
