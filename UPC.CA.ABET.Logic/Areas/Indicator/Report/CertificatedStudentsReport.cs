﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class CertificatedStudentsReport : Dashboard
    {
        public CertificatedStudentsReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var carreers = _context.Carrera.Select(x => x);

            foreach (var carrera in carreers)
            {
                var table = new CertificatedStudentsTable(carrera.IdCarrera);
                table.Load(entities, TermId);
                Tables.Add(table);
                var chart = new CertificatedChart(carrera.IdCarrera);
                chart.Load(entities, TermId);
                Charts.Add(chart);
            }
        }
    }
}
