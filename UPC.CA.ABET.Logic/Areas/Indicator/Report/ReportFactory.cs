﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class ReportFactory
    {
        public static Dashboard GetReport(string type, int termId, AbetEntities context)
        {
            switch(type)
            {
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_LEVEL:
                    return new AttritionLevelReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_MODALITY:
                    return new AttritionModalityReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.ATTRITION_RATIO:
                    return new AttritionRatioReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.CORRECT_EXAMS:
                    return new CorrectExamsReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.HIRED_PROFESSORS:
                    return new HiredProfessorsReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_CD:
                    return new InTimeCDReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_EXAMS:
                    return new InTimeExamsReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_SCHEDULE:
                    return new InTimeScheduleReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_SYLLABUS:
                    return new InTimeSyllabusReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.IN_TIME_VIRTUAL_CLASS:
                    return new InTimeVirtualClassReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.COHORT:
                    return new CohortReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_PUNCTUALITY:
                    return new ProfessorPunctualityReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_HOURS:
                    return new ProfessorHoursReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.PROFESSOR_ATTENDANCE:
                    return new ProfessorAttendanceReport(context, termId);
                case ConstantHelpers.INDICATOR_REPORT_TYPE.CERTIFICATED_STUDENTS:
                    return new CertificatedStudentsReport(context, termId);
                case ConstantHelpers.INDICATOR_TABLE_TYPE.MEETINGS:
                    return new MeetingReport(context, termId);
                case ConstantHelpers.INDICATOR_TABLE_TYPE.STUDENT_CAMPUS:
                    return new StudenCampusReport(context, termId);
                case ConstantHelpers.INDICATOR_TABLE_TYPE.STUDENT_SECTION:
                    return new StudentSectionReport(context, termId);
                case ConstantHelpers.INDICATOR_TABLE_TYPE.STUDENT_COHORT:
                    return new CohortStudentReport(context, termId);
                case ConstantHelpers.INDICATOR_TABLE_TYPE.STUDENT_PERIOD:
                    return new StudentPeriodReport(context, termId);
                default:
                    throw new Exception("Tipo de reporte"+ " '" + type + "' " + " no soportado");
            }
        } 
    }
}
