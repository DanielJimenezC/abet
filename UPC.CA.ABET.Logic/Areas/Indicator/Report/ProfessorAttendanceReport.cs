﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    class ProfessorAttendanceReport: Dashboard
    {
        public ProfessorAttendanceReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var chart = new ProfessorAttendanceChart();
            chart.Load(entities, termId);

            var attendanceTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE);
            attendanceTable.Load(entities, termId);

            var attendanceTableRanking = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE_RANKING);
            attendanceTableRanking.Load(entities, termId);

            var attendanceTableRange = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.PROFESSOR_ATTENDANCE_RANGE);
            attendanceTableRange.Load(entities, termId);

            Tables.Add(attendanceTable);
            Tables.Add(attendanceTableRange);
            Tables.Add(attendanceTableRanking);
            
            Charts.Add(chart);
        }
    }
}
