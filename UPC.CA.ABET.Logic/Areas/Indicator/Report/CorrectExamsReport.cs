﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class CorrectExamsReport : Dashboard
    {
        public CorrectExamsReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var correctFinalExamChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_FINAL_EXAM);
            correctFinalExamChart.Load(entities, termId);

            var correctMidTermExamChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_MID_TERM_EXAM);
            correctMidTermExamChart.Load(entities, termId);

            var correctMakeUpExamChart = ChartFactory.GetChart(ConstantHelpers.INDICATOR_CHART_TYPE.CORRECT_MAKE_UP_EXAM);
            correctMakeUpExamChart.Load(entities, termId);

            var correctFinalExamTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_FINAL_EXAM);
            correctFinalExamTable.Load(entities, termId);

            var correctMidTermExamTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_MID_TERM_EXAM);
            correctMidTermExamTable.Load(entities, termId);

            var correctMakeUpExamTable = TableFactory.GetTable(ConstantHelpers.INDICATOR_TABLE_TYPE.CORRECT_MAKE_UP_EXAM);
            correctMakeUpExamTable.Load(entities, termId);

            Charts.Add(correctFinalExamChart);
            Charts.Add(correctMidTermExamChart);
            Charts.Add(correctMakeUpExamChart);

            Tables.Add(correctFinalExamTable);
            Tables.Add(correctMidTermExamTable);
            Tables.Add(correctMakeUpExamTable);
        }
    }
}
