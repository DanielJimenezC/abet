﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class AttritionModalityReport: Dashboard
    {
        public AttritionModalityReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var careers = _context.Carrera.Where(x => x.AttritionDetalle.Any(y => y.Attrition.IdSubModalidadPeriodoAcademico == termId));

            foreach (var career in careers)
            {
                var attritionModalityChart = new AttritionModalityChart(career.IdCarrera);
                attritionModalityChart.Load(entities, termId);
                Charts.Add(attritionModalityChart);

                var attritionModalityTable = new AttritionModalityTable(career.IdCarrera);
                attritionModalityTable.Load(entities, termId);
                Tables.Add(attritionModalityTable);
            }
        }
    }
}
