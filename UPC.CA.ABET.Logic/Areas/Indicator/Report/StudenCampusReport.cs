﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class StudenCampusReport : Dashboard
    {
        public StudenCampusReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var chart = new StudentCampusChart();
            chart.Load(entities, termId);
            Charts.Add(chart);

            var IdEscuela = _context.Escuela.FirstOrDefault(x => x.Codigo.Equals("EISC")).IdEscuela;
            var careers = _context.Carrera.Where(x => x.IdEscuela == IdEscuela);

            foreach (var career in careers)
            {
                var table = new StudentCampusTable( career.IdCarrera, termId);
                table.Load(entities, termId);
                Tables.Add(table);
            }
        }
    }
}
