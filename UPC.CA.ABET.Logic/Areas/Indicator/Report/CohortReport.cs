﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Indicator.Chart;
using UPC.CA.ABET.Logic.Areas.Indicator.Table;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Indicator.Report
{
    public class CohortReport: Dashboard
    {
        public CohortReport(AbetEntities entities, int termId) : base(entities, termId)
        {
            var careers = _context.Carrera.Where(x => x.AttritionDetalle.Any(y => y.Attrition.IdSubModalidadPeriodoAcademico == termId));

            foreach (var career in careers)
            {
                var table = new CohortTable(career.IdCarrera);
                table.Load(entities, termId);
                Tables.Add(table);
            }
        }
    }
}
