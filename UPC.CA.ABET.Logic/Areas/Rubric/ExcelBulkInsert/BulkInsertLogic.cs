﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using LinqToExcel;
using LinqToExcel.Query;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.BulkInsertModel;
using System.Data.Entity;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Diagnostics;//Para que funciones debug
using System.Data.SqlClient;
using System.Data;
using ClosedXML.Excel;
using System.Web.Mvc;
using System.Web;
using System.Data.OleDb;
using System.Text;
#endregion

namespace UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert
{
    public class BulkInsertLogic
    {
        #region Utilitarios

        /// <summary>
        /// Carga los registros de un archivo Excel tomando como 
        /// referencia su ruta fisica.
        /// </summary>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <returns>ExcelQueryFactory</returns>
        public static ExcelQueryFactory ObtenerExcel(String PathFile)
        {
            return new ExcelQueryFactory(PathFile) { TrimSpaces = TrimSpacesType.Both };
        }

        /// <summary>
        /// Valida que el archivo Excel tenga los Headers correspondientes al
        /// tipo de carga masiva.
        /// </summary>
        /// <param name="Data">Archivo Excel</param>
        /// <param name="Type">Tipo de Carga Masiva</param>
        /// <returns>Boolean</returns>
        public static Boolean ValidarHeaders(ExcelQueryFactory Data, BulkInsertType Type)
        {
            var columnNames = Data.GetColumnNames(Data.GetWorksheetNames().FirstOrDefault());
            Boolean respuesta = false;

            switch (Type)
            {
                case BulkInsertType.VirtualCompany:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListVirtualCompanyHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.AcademicProject:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListaAcademicProjectsHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.EvaluationType:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListEvaluationTypeHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.Evaluation:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListEvaluationHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.CoursesToEvaluation:
                    respuesta = ConstantHelpers.RUBRICFILETYPE.ListCourseToEvaluation()
                        .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.EvaluatorType:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListEvaluatorTypeHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.EvaluatorTypeMaster:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListEvaluatorTypeMasterHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.Evaluator:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListEvaluatorHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.OutcomeGoalCommission:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListOutcomeGoalCommissionHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
                case BulkInsertType.GrupoComite:
                    respuesta =
                        ConstantHelpers.RUBRICFILETYPE.ListGrupoComiteHeaders()
                            .All(x => columnNames.Any(y => y == x));
                    break;
            }

            return respuesta;
        }

        /// <summary>
        /// Retorna un mensaje de respuesta concatenando todas las cadenas de la lista 
        /// </summary>
        /// <param name="ListCadena">Lista de palabras a concatenar</param>
        /// <param name="Mensaje">Mensaje de respuesta</param>
        /// <returns>String</returns>
        public static String GetCodigosConcatenados(List<String> ListCadena, String Mensaje)
        {
            String codigos = ListCadena.Aggregate("", (current, item) => current + (item + ", "));
            return Mensaje + " " + codigos.Remove(codigos.Length - 2);
        }

        /// <summary>
        /// Retorna el ID de la comisión WASC para el presente ciclo académico.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActual">ID del Periodo Académico Activo</param>
        /// <returns>Int32</returns>
        public static Int32 GetWascId(AbetEntities DataContext, Int32 PeriodoAcademicoActual)
        {

            int idcomision = 0;
            var comision = (from a in DataContext.Acreditadora
                    join b in DataContext.AcreditadoraPeriodoAcademico on a.IdAcreditadora equals
                        b.IdAcreditadora.Value
                    join c in DataContext.Comision on b.IdAcreditadoraPreiodoAcademico equals
                        c.IdAcreditadoraPeriodoAcademico
                    where
                        b.IdSubModalidadPeriodoAcademico == PeriodoAcademicoActual &&
                        c.Codigo == ConstantHelpers.RUBRICFILETYPE.WascCommission.WASC
                    select new { c.IdComision }
                    ).FirstOrDefault();

            if (comision != null)
                idcomision = comision.IdComision;

            return idcomision;
        }

        public static List<String> ListCourseProject(AbetEntities DataContext, Int32 PeriodoAcademicoActualId, int? idEscuela)
        {
            var cursosProyectos = DataContext.CursosEvaluarRubrica.Where(x => x.IdEscuela == idEscuela && x.IdSubModalidadPeriodoAcademicoModulo == PeriodoAcademicoActualId).Select(x=>x.Curso.Codigo).ToList();  // ConstantHelpers.RUBRICFILETYPE.ListCourseProjectFullName();
            return
                DataContext.Curso.Where(x => cursosProyectos.Any(y => y.Contains(x.Codigo)))
                    .Select(x => x.Codigo)
                    .ToList();
        }

        /// <summary>
        /// Retorna el ID del AlumnoSeccion para el curso de TDP, TP1 y TP2.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="AlumnoId">ID del Alumno</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <returns>Int32</returns>
        public static Int32? GetAlumnoSeccionId(AbetEntities DataContext, Int32 AlumnoId, Int32 PeriodoAcademicoActualId, int? idEscuela)
        {
           // var cursosProyectos = ListCourseProject(DataContext);

            var respuesta = (from a in DataContext.AlumnoMatriculado
                             join b in DataContext.AlumnoSeccion on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                             join c in DataContext.Seccion on b.IdSeccion equals c.IdSeccion
                             join d in DataContext.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals
                                 d.IdCursoPeriodoAcademico
                             join e in DataContext.Curso on d.IdCurso equals e.IdCurso
                             join ce in DataContext.CursosEvaluarRubrica on e.IdCurso equals ce.IdCurso
                             where
                                 a.IdAlumno == AlumnoId &&
                                 a.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId &&
                                 d.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId && 
                                 ce.IdEscuela == idEscuela
                             select new { b.IdAlumnoSeccion }
                ).FirstOrDefault();

            if (respuesta != null)
                return respuesta.IdAlumnoSeccion;
            return null;
        }

        #endregion

        #region Carga Masiva

        /// <summary>
        /// Inserta las Empresas Virtuales para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los códigos de las empresas no deben estar repetidos en el Excel.
        /// b) Los códigos de las empresas no deben existir en base de datos.
        /// c) Los nombres de las empresas no deben estar repetidos en el Excel.
        /// d) Los nombres de las empresas no deben existir en base de datos.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String VirtualCompanyBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes, Int32? IdEscuela)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.VirtualCompany);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<VirtualCompanyFileModel>(0).Select(x => x).ToList();
            
            var periodoAcademicoActualId = (from smpa in DataContext.SubModalidadPeriodoAcademico
                                            join pa in DataContext.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                            where pa.Estado == "ACT" && smpa.SubModalidad.NombreEspanol == "Regular" && smpa.SubModalidad.IdSubModalidad == 1
                                            select pa.IdPeriodoAcademico).FirstOrDefault();
            Int32 idSubModalidadPeriodoAcademico = DataContext.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            Int32 idSubModalidadPeriodoAcademicoModulo = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            var lstDocentesNoExistentes = new List<String>();
            var lstEmpresaVirtual = new List<EmpresaVirtual>();

            foreach (var item in dataExcel)
            {
                if (item.Codigo == null || item.Nombre == null || item.Descripcion == null )
                    continue;


                /* var docente = DataContext.Docente.Where(x => x.Codigo == item.CodigoGerente).FirstOrDefault();

                 if (docente == null)
                 {
                     lstDocentesNoExistentes.Add(docente.Codigo);
                     continue;
                 }*/

                int pac = periodoAcademicoActualId;               
                
                var empresa = new EmpresaVirtual();
                empresa.Codigo = item.Codigo;
                empresa.Nombre = item.Nombre;
                empresa.Descripcion = item.Descripcion;
                empresa.IdEscuela = IdEscuela;
                empresa.IdSubModalidadPeriodoAcademicoModulo = idSubModalidadPeriodoAcademicoModulo;
                //empresa.IdDocente = docente.IdDocente;

                lstEmpresaVirtual.Add(empresa);
            }

            //VALIDAR QUE TODO LOS CODIGOS DE DOCENTES EXISTAN
            /*if (lstEmpresaVirtual.Any())
            {
                return GetCodigosConcatenados(lstDocentesNoExistentes, "Los siguientes código de docentes no existen: ");
            }*/

            //VALIDAR QUE LOS CODDOCENTE NO ESTEN REPETIDOS
            /*
            var lstIdDocenteRepetidos = lstEmpresaVirtual.GroupBy(x => x.IdDocente)
                .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if(lstIdDocenteRepetidos.Any())
            {
                var lstCodDocentes = new List<String>();
                foreach (var iddocente in lstIdDocenteRepetidos)
                {
                    var docente = DataContext.Docente.Find(iddocente);
                    lstCodDocentes.Add(docente.Codigo);

                }

                return GetCodigosConcatenados(lstCodDocentes, "Los siguientes códigos de profesores se repiten");

            }
            */
            
            //CODIGO REPETIDOS DE EMPRESAS 
            var lstCodigosRepetidosExcel = lstEmpresaVirtual.GroupBy(x => x.Codigo)
                .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if (lstCodigosRepetidosExcel.Any())
            {
                return GetCodigosConcatenados(lstCodigosRepetidosExcel, DicMensajes["CodigosRepetidosEmpresas"]);
            }

            var lstEmpresaVirtualDeBase =
                DataContext.EmpresaVirtual.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId);
            var lstCodigosRepetidosDeBase =
                lstEmpresaVirtual.Where(x => lstEmpresaVirtualDeBase.Any(y => y.Codigo == x.Codigo))
                    .Select(x => x.Codigo).ToList();

            if (lstCodigosRepetidosDeBase.Any())
            {
                return GetCodigosConcatenados(lstCodigosRepetidosDeBase, DicMensajes["CodigosBaseRepetidosEmpresas"]);
            }

            var lstNombresRepetidos = lstEmpresaVirtual.GroupBy(x => x.Nombre)
                .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if (lstNombresRepetidos.Any())
            {
                return GetCodigosConcatenados(lstNombresRepetidos, DicMensajes["NombresRepetidosEmpresas"]);
            }

            var lstNombresRepetidosDeBase =
                lstEmpresaVirtual.Where(x => lstEmpresaVirtualDeBase.Any(y => y.Nombre == x.Nombre))
                    .Select(x => x.Nombre).ToList();

            if (lstNombresRepetidosDeBase.Any())
            {
                return GetCodigosConcatenados(lstNombresRepetidosDeBase, DicMensajes["NombresBaseRepetidosEmpresas"]);
            }

            using (var transaction = new TransactionScope())
            {
                foreach (var empresaVirtual in lstEmpresaVirtual)
                {
                    DataContext.EmpresaVirtual.Add(empresaVirtual);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        public static string insertarEvaluador(AbetEntities DataContext, int IdProyectoAcademico,  string CodigoRol, int IdCurso, string CodigoDocente, int PeriodoAcademicoId)
        {
            var ObjDocente = DataContext.Docente.Where(x => x.Codigo.ToUpper() == CodigoDocente.ToUpper()).FirstOrDefault();
            String descripcion = "";
            if (ObjDocente == null)
                return descripcion = "Docente " + CodigoDocente + " no existe -";

            var objTipoEvaluador = DataContext.TipoEvaluador.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoId &&
            x.TipoEvaluadorMaestra.Codigo == CodigoRol && x.IdCurso == IdCurso).FirstOrDefault();

            if (objTipoEvaluador == null)
                return descripcion = "Rol evaluador "+CodigoRol+" para el curso aún no existe -";

            if (objTipoEvaluador != null)
            {              
                    var objEvaluador = new Evaluador
                    {
                        IdProyectoAcademico = IdProyectoAcademico,
                        IdDocente = ObjDocente.IdDocente,
                        IdTipoEvaluador = objTipoEvaluador.IdTipoEvaluador
                    };
                    DataContext.Evaluador.Add(objEvaluador);
                    DataContext.SaveChanges();
                return descripcion;
               
            }
            return descripcion;
        }

        public class ReturnErrorProyecto
        {
            public List<AcademicProjectFileModel> lstErrorItem { get; set; }
            public List<String> lstErrorDescripcion { get; set; }
            public String Mensaje { get; set; }
        }
        /// <summary>
        /// Inserta los Proyectos Academicos para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los códigos de los proyectos no deben estar repetidos en el Excel.
        /// b) Los códigos de los proyectos no deben existir en base de datos.
        /// c) Los códigos de la empresas deben existir.
        /// d) Un proyecto académico debe tener como mínimo un alumno.
        /// e) Los códigos de los alumnos no deben estar repetidos en el Excel.
        /// f) Los alumnos deben estar matriculados en el periodo académico actual.
        /// g) Los alumnos no deben estar asignados a ningún proyecto.
        /// h) Los alumnos deben estar matriculados en cursos de proyectos (TDP, TP1, TP2).
        /// i) Los nombres de los proyectos no deben estar repetidos en el Excel.
        /// j) Los nombres de los proyectos no deben existir en base de datos.
        /// K) Los nombres de cursos deben de ser TP1 o TP2.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static ReturnErrorProyecto AcademicProjectBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes, int? idEscuela, int? idSubModalidad)
        {
            var objExcel = ObtenerExcel(PathFile);
            ReturnErrorProyecto respuestaError = new ReturnErrorProyecto();
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.AcademicProject);

            if (!isValidHeader)
            {
                respuestaError.Mensaje = DicMensajes["HeadersInvalidos"];
                return respuestaError;
            }

            // Int32 periodoAcademicoActualId = DataContext.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
            //    .FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            //var periodoAcademicosActivo = (from smpam in DataContext.SubModalidadPeriodoAcademicoModulo
            //                               join smpa in DataContext.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
            //                               where (smpa.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && smpa.IdSubModalidad == idSubModalidad)
            //                               select new
            //                               {
            //                                   IdPeriodoAcademico = smpa.IdPeriodoAcademico
            //                               }).FirstOrDefault();
            int idPeriodoAcademicoActivo = DataContext.SubModalidadPeriodoAcademico.Where(x => x.PeriodoAcademico.Estado == "ACT" && x.IdSubModalidad == idSubModalidad).FirstOrDefault().IdPeriodoAcademico;

            var periodoAcademicoModuloActivo = (from smpamo in DataContext.SubModalidadPeriodoAcademicoModulo
                                                join smpa in DataContext.SubModalidadPeriodoAcademico on smpamo.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                                where (smpa.IdPeriodoAcademico == idPeriodoAcademicoActivo)
                                                select new
                                                {
                                                    IdSubModalidadPeriodoAcademicoModulo = smpamo.IdSubModalidadPeriodoAcademicoModulo
                                                }).FirstOrDefault();
                                                
            //2018-1   1
            //2018-2  2
            //2018-3  3

            //NUEVO
            //var tiposEvaluacionCount = DataContext.TipoEvaluacion.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).Count();

            //var tiposEvaluacionCount = (from te in DataContext.TipoEvaluacion
            //                            join smpa in DataContext.SubModalidadPeriodoAcademico on te.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
            //                            join lstpaa in periodosAcademicosActivos on smpa.IdPeriodoAcademico equals lstpaa.IdPeriodoAcademico
            //                            where (smpa.IdSubModalidad == idSubModalidad)
            //                            select new
            //                            {
            //                                idTipoEvaluacion = te.IdTipoEvaluacion
            //                            }).Count();

            var tiposEvaluacionCount = (from te in DataContext.TipoEvaluacion
                                        join smpa in DataContext.SubModalidadPeriodoAcademico on te.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico                                      
                                        where (smpa.IdSubModalidad == idSubModalidad && smpa.IdPeriodoAcademico== idPeriodoAcademicoActivo)
                                        select new
                                        {
                                            idTipoEvaluacion = te.IdTipoEvaluacion
                                        }).Count();


            if (tiposEvaluacionCount == 0)
            {
                respuestaError.Mensaje = DicMensajes["TipoEvaluacionNoCargadas"];
                return respuestaError;
            }

            var dataExcel = objExcel.Worksheet<AcademicProjectFileModel>(0).Select(x => x).ToList();

            var dataNoCargada = new List<AcademicProjectFileModel>();
            var razonNoCargada = new List<String>();

            try
            {
                using (var transaction = DataContext.Database.BeginTransaction())
                {
                        foreach (var item in dataExcel)
                        {
                            String descripcion = "";

                            /*VALIDACIÓN CAMPOS VACIOS*/
                            if (item.Codigo == null)
                                descripcion+="Código vacío - ";
                            if (item.Nombre == null)
                                descripcion += "Nombre vacío - ";
                            if (item.Descripcion == null)
                                descripcion += "Descripción vacío - ";
                            if (item.Curso == null)
                                descripcion += "Curso vacío - ";
                            if (item.CodigoEmpresa == null)
                                descripcion += "Código Empresa vacío - ";
                            if (item.CodigoAlumno01 == null)
                                descripcion += "Alumno 1 vacío - ";
                            if (item.CodGerente == null)
                                descripcion += "Gerente Vacío - ";

                            if (item.Codigo == null || item.Nombre == null || item.Descripcion == null || item.Curso == null
                                || item.CodigoEmpresa == null || item.CodigoAlumno01 == null || item.CodGerente == null)
                            {
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }

                        /*VALIDACIÓN TABLAS*/

                       // //var proyectoacademico = DataContext.ProyectoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico== periodoAcademicoActualId//
                        // && x.Codigo.ToUpper() == item.Codigo.ToUpper()).FirstOrDefault();

                        //var proyectoacademico = (from proy in DataContext.ProyectoAcademico
                        //                         join smpa in DataContext.SubModalidadPeriodoAcademico on proy.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                        //                         join lspaa in periodosAcademicosActivos on smpa.IdPeriodoAcademico equals lspaa.IdPeriodoAcademico
                        //                         where (proy.Codigo.ToUpper() == item.Codigo.ToUpper() && smpa.IdSubModalidad==idSubModalidad)
                        //                         select new
                        //                         {
                        //                            IdProyectoAcademico = proy.IdProyectoAcademico,
                        //                            Nombre=proy.Nombre,
                        //                            IdEmpresaVirtual = proy.IdEmpresaVirtual,
                        //                            Codigo = proy.Codigo,
                        //                            Descripcion = proy.Descripcion,
                        //                            IdCurso = proy.IdCurso,
                        //                            IdSubModalidadPeriodoAcademicoModulo = proy.IdSubModalidadPeriodoAcademicoModulo
                        //                         }).FirstOrDefault();

                        var proyectoacademico = (from proy in DataContext.ProyectoAcademico
                                                 join smpa in DataContext.SubModalidadPeriodoAcademico on proy.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                                  where (proy.Codigo.ToUpper() == item.Codigo.ToUpper() && smpa.IdSubModalidad == idSubModalidad && smpa.IdPeriodoAcademico== idPeriodoAcademicoActivo)
                                                 select new
                                                 {
                                                     IdProyectoAcademico = proy.IdProyectoAcademico,
                                                     Nombre = proy.Nombre,
                                                     IdEmpresaVirtual = proy.IdEmpresaVirtual,
                                                     Codigo = proy.Codigo,
                                                     Descripcion = proy.Descripcion,
                                                     IdCurso = proy.IdCurso,
                                                     IdSubModalidadPeriodoAcademicoModulo = proy.IdSubModalidadPeriodoAcademicoModulo
                                                 }).FirstOrDefault();

                        if (proyectoacademico!=null)
                            {
                                descripcion += "Código de Proyecto Existente en base de datos - ";
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }

                            var curso = DataContext.Curso.Where(x => x.Codigo.ToUpper() == item.Curso.ToUpper()).FirstOrDefault();
                            if(curso==null)
                            {
                                descripcion += "Curso No existe -";
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }
                        //var empresa = DataContext.EmpresaVirtual.Where(x => x.Codigo.ToUpper() == item.CodigoEmpresa.ToUpper() && x.IdSubModalidadPeriodoAcademicoModulo == periodoAcademicoActualId).FirstOrDefault();
                        //var empresa = (from ev in DataContext.EmpresaVirtual
                        //               join smpa in DataContext.SubModalidadPeriodoAcademico on ev.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                        //               join lpaa in periodosAcademicosActivos on smpa.IdPeriodoAcademico equals lpaa.IdPeriodoAcademico
                        //               where (ev.Codigo.ToUpper() == item.CodigoEmpresa.ToUpper())
                        //               select new
                        //               {
                        //                    IdEmpresa= ev.IdEmpresa,
                        //                    Nombre = ev.Nombre,
                        //                    Descripcion = ev.Descripcion,
                        //                    Codigo = ev.Codigo,
                        //                    IdEscuela=ev.IdEscuela,
                        //                    IdSubModalidadPeriodoAcademicoModulo = ev.IdSubModalidadPeriodoAcademicoModulo
                        //               }).FirstOrDefault();

                        var empresa = (from ev in DataContext.EmpresaVirtual
                                       join smpa in DataContext.SubModalidadPeriodoAcademico on ev.SubModalidadPeriodoAcademicoModulo.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                        where (ev.Codigo.ToUpper() == item.CodigoEmpresa.ToUpper() && smpa.IdPeriodoAcademico== idPeriodoAcademicoActivo)
                                       select new
                                       {
                                           IdEmpresa = ev.IdEmpresa,
                                           Nombre = ev.Nombre,
                                           Descripcion = ev.Descripcion,
                                           Codigo = ev.Codigo,
                                           IdEscuela = ev.IdEscuela,
                                           IdSubModalidadPeriodoAcademicoModulo = ev.IdSubModalidadPeriodoAcademicoModulo
                                       }).FirstOrDefault();


                        if (empresa == null)
                            {
                                descripcion += "Empresa No existe - ";
                                Console.Write(descripcion);
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }
                            var alumno1 = DataContext.Alumno.Where(x => x.Codigo.ToUpper() == (item.CodigoAlumno01.Length == 6 ? "200" + item.CodigoAlumno01 : item.CodigoAlumno01)).FirstOrDefault();
                            if (alumno1 == null)
                            { 
                                descripcion += "Código del Alumno1 no existe - ";
                                Console.Write(descripcion);
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }



                            Alumno alumno2 = null;
                            if(item.CodigoAlumno02!=null)
                            { 
                                if (item.CodigoAlumno02.Equals("") == false)
                                {
                                    alumno2 = DataContext.Alumno.Where(x => x.Codigo.ToUpper() == (item.CodigoAlumno02.Length == 6 ? "200" + item.CodigoAlumno02 : item.CodigoAlumno02)).FirstOrDefault();
                                    if (alumno2 == null)
                                    {
                                        descripcion += "Código del Alumno2 no existe -";
                                        Console.Write(descripcion);
                                        dataNoCargada.Add(item);
                                        razonNoCargada.Add(descripcion);
                                        continue;
                                    }
                                }
                            }

                        Int32? idAlumnoSeccion1 = BulkInsertLogic.GetAlumnoSeccionId(DataContext, alumno1.IdAlumno, idPeriodoAcademicoActivo, idEscuela);

                      

                           if (idAlumnoSeccion1.HasValue==false)
                            {
                                descripcion += "Alumno 1 no tiene sección  -";
                                Console.Write(descripcion);
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }

                            var alumnoSeccion1 = DataContext.AlumnoSeccion.Where(x => x.IdAlumnoSeccion == idAlumnoSeccion1.Value).FirstOrDefault();

                            if(alumnoSeccion1.IdProyectoAcademico!=null)
                            {
                                descripcion += "Alumno 1 ya tiene un proyecto registrado  -";
                                Console.Write(descripcion);
                                dataNoCargada.Add(item);
                                razonNoCargada.Add(descripcion);
                                continue;
                            }

                            Int32? idAlumnoSeccion2 = null;
                            AlumnoSeccion alumnoSeccion2 = null;

                            if (alumno2 != null)
                            {
                                idAlumnoSeccion2= BulkInsertLogic.GetAlumnoSeccionId(DataContext, alumno2.IdAlumno, idPeriodoAcademicoActivo, idEscuela);
                                if (idAlumnoSeccion1.HasValue == false)
                                {
                                    descripcion += "Alumno 2 no tiene sección  -";
                                    Console.Write(descripcion);
                                    dataNoCargada.Add(item);
                                    razonNoCargada.Add(descripcion);
                                    continue;
                                }

                                alumnoSeccion2 = DataContext.AlumnoSeccion.Where(x => x.IdAlumnoSeccion == idAlumnoSeccion2.Value).FirstOrDefault();

                                if (alumnoSeccion2.IdProyectoAcademico != null)
                                {
                                    descripcion += "Alumno 2 ya tiene un proyecto registrado  -";
                                    Console.Write(descripcion);
                                    dataNoCargada.Add(item);
                                    razonNoCargada.Add(descripcion);
                                    continue;
                                }
                    
                            }

                            /*GUARDAR EN BASE DE DATOS*/
             
                                    var objProyecto = new ProyectoAcademico
                                    {
                                        //IdSubModalidadPeriodoAcademicoModulo = periodoAcademicoActualId,

                                        IdSubModalidadPeriodoAcademicoModulo= periodoAcademicoModuloActivo.IdSubModalidadPeriodoAcademicoModulo,
                                        Codigo = item.Codigo,
                                        Nombre = item.Nombre,
                                        IdCurso = curso.IdCurso,
                                        Descripcion = item.Descripcion,
                                        IdEmpresaVirtual = empresa.IdEmpresa
                                    };

                                    DataContext.ProyectoAcademico.Add(objProyecto);
                                    DataContext.SaveChanges();

                                    var lstAlumnosRegistro =
                                          DataContext.AlumnoSeccion.Where(
                                              x => x.IdAlumnoSeccion == idAlumnoSeccion1.Value || x.IdAlumnoSeccion == (idAlumnoSeccion2.HasValue == false ? 0 : idAlumnoSeccion2))
                                              .ToList();

                                    lstAlumnosRegistro.ForEach(x => x.IdProyectoAcademico = objProyecto.IdProyectoAcademico);

                                    //GUARDANDO LOS EVALUADORES
                                    if (item.CodDocenteCoautor != null) { 
                                        if (item.CodDocenteCoautor.Equals("") == false)
                                        {
                                            var respuesta = BulkInsertLogic.insertarEvaluador(DataContext, objProyecto.IdProyectoAcademico, "COA", curso.IdCurso, item.CodDocenteCoautor, idPeriodoAcademicoActivo);
                                            if (respuesta.Equals("") == false)
                                            {
                                                dataNoCargada.Add(item);
                                                razonNoCargada.Add(respuesta);
                                                continue;
                                            }
                                        }
                                    }
                                    if(item.CodDocenteCliente!=null)
                                    { 
                                        if (item.CodDocenteCliente.Equals("") == false)
                                        {
                                            var respuesta = BulkInsertLogic.insertarEvaluador(DataContext, objProyecto.IdProyectoAcademico, "CLI", curso.IdCurso, item.CodDocenteCliente, idPeriodoAcademicoActivo);
                                            if (respuesta.Equals("") == false)
                                            {
                                                dataNoCargada.Add(item);
                                                razonNoCargada.Add(respuesta);
                                                continue;
                                            }
                                        }
                                    }

                                    //Agregar al gerente 
                                    if (item.CodGerente.Equals("") == false)
                                    {
                                        var respuesta = BulkInsertLogic.insertarEvaluador(DataContext, objProyecto.IdProyectoAcademico, "GER", curso.IdCurso, item.CodGerente, idPeriodoAcademicoActivo);
                                        if (respuesta.Equals("") == false)
                                        {
                                            dataNoCargada.Add(item);
                                            razonNoCargada.Add(respuesta);
                                            continue;
                                        }
                                    }

                                    //Agregar al grupo de comité
                                    // var lstGrupoComite = DataContext.GrupoComite.Where(x => x.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId).ToList();

                                    var lstGrupoComite = DataContext.GrupoComite.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademicoActivo).ToList();

                                    var objTipoEvaluador2 = DataContext.TipoEvaluador.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademicoActivo &&
                                       x.TipoEvaluadorMaestra.Codigo == "COM" && x.IdCurso == curso.IdCurso).FirstOrDefault();

                                    foreach (var docenteComite in lstGrupoComite)
                                    {
                                        var objEvaluador = new Evaluador
                                        {
                                            IdProyectoAcademico = objProyecto.IdProyectoAcademico,
                                            IdDocente = docenteComite.IdDocente,
                                            IdTipoEvaluador = objTipoEvaluador2.IdTipoEvaluador
                                        };
                                        DataContext.Evaluador.Add(objEvaluador);
                                    }


                                    var lstEvaluacionProyecto =
                                        DataContext.TipoEvaluacion.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idPeriodoAcademicoActivo);


                                    foreach (var tipoEvaluacion in lstEvaluacionProyecto)
                                    {
                                        var objEvaluacionProyecto = new EvaluacionProyecto
                                        {
                                            IdProyecto = objProyecto.IdProyectoAcademico,
                                            IdTipoEvaluacion = tipoEvaluacion.IdTipoEvaluacion
                                        };
                                        DataContext.EvaluacionProyecto.Add(objEvaluacionProyecto);
                                    }

                        DataContext.SaveChanges();
                        /*FIN BASE DE DATOS*/

                        } //fin de loop

                        transaction.Commit();
                    respuestaError.Mensaje = ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
                }
                //if (respuestaError.lstErrorItem != null)
                //{
                    respuestaError.lstErrorItem = dataNoCargada;
                //}

                //if (respuestaError.lstErrorDescripcion != null)
                //{
                    respuestaError.lstErrorDescripcion = razonNoCargada;
                //}               

                
                return respuestaError;

            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                respuestaError.Mensaje = "Ocurrió un error, por favor revise su conexión a internet. En todo caso contáctese con el equipo de ABET.";
                //throw new Exception(e.Message);
                return respuestaError;
            }
        }


        private static string GetConnectionString(string PathFileUpload)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            // XLSX - Excel 2007, 2010, 2012, 2013
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
            props["Extended Properties"] = "Excel 12.0 XML";
            props["Data Source"] = PathFileUpload;

            // XLS - Excel 2003 and Older
            //props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
            //props["Extended Properties"] = "Excel 8.0";
            //props["Data Source"] = "C:\\MyExcel.xls";

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }
        public static string UploadRVToSQL(AbetEntities DataContext, string PathFileUpload, string PathFileOutPut,bool EsMallaArizona)
        {
            XLWorkbook wb = new XLWorkbook();
            string Resultado = "";
            try
            {
                DataSet ds = new DataSet();

                string connectionString = GetConnectionString(PathFileUpload);

                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    con.Open();
                    OleDbCommand cmd = new OleDbCommand("Select * from [EVALUACIONES$]")
                    {
                        Connection = con
                    };

                    DataTable dt = new DataTable
                    {
                        TableName = "EVALUACIONES"
                    };

                    OleDbDataAdapter daoledb = new OleDbDataAdapter(cmd);
                    daoledb.Fill(dt);
                    cmd = null;
                    using (SqlConnection conn = new SqlConnection(DataContext.Database.Connection.ConnectionString))
                    {
                        // Open the SqlConnection.
                        conn.Open();
                        //Create the SQLCommand object
                        string procedure = "";
                        if (EsMallaArizona)
                        {
                            procedure = "Usp_CargarNotasRVArizona";
                        }
                        else
                        {
                            procedure = "Usp_CargarNotasRV";
                        }
                        using (SqlCommand command = new SqlCommand(procedure, conn) { CommandType = System.Data.CommandType.StoredProcedure })
                        {
                            //Pass the parameter values here
                            command.CommandTimeout = 0;
                            command.Parameters.Add(new SqlParameter("@tabla", dt));

                            using (SqlDataAdapter da = new SqlDataAdapter())
                            {
                                //read the data
                                ds = new DataSet();
                                da.SelectCommand = command;
                                da.Fill(ds);

                                DataTable dtr = new DataTable("Resultado");
                                foreach (DataTable table in ds.Tables)
                                {
                                    foreach (DataColumn col in table.Columns)
                                    {
                                        dtr.Columns.Add(col.ColumnName, col.DataType);
                                    }
                                    foreach (DataRow row in table.Rows)
                                    {
                                        dtr.Rows.Add(row.ItemArray);
                                    }
                                }
                                if (ds != null)
                                {
                                    wb.Worksheets.Add(dtr);
                                    Resultado = "completado";
                                    wb.SaveAs(PathFileOutPut);
                                }
                                else
                                {
                                    Resultado = "incompleto";
                                }
                                conn.Close();
                                conn.Dispose();
                                da.Dispose();
                                wb.Dispose();
                            }
                        }
                    }
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(" ERROR : " + ex.Message);
                Resultado = "error: " + ex.Message;
                //Resultado = ConstantHelpers.RUBRICFILETYPE.MessageResult.ERROR_EXCEPCION;
            }
            return Resultado;
        }

        /// <summary>
        /// Inserta los Tipos de Evaluaciones para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Tipos de Evaluaciones no deben estar repetidos en el Excel.
        /// b) Los Tipos de Evaluaciones no deben existir en base de datos.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String EvaluationTypeBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.EvaluationType);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<EvaluationTypeFileModel>(0).Select(x => x).ToList();

            Int32 periodoAcademicoActualId = (from submoda in DataContext.SubModalidadPeriodoAcademico
                                              join pa in DataContext.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                              where (submoda.IdSubModalidad == 1 && pa.Estado == "ACT")
                                              select submoda.IdPeriodoAcademico).FirstOrDefault();
            Int32 smpaActual = DataContext.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            Int32 smpaModuloAct = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;


            var lstTipoEvaluacion = dataExcel.Where(x=>x.TipoEvaluacion!=null).Select(fila => fila.TipoEvaluacion)
                .Select (tipo => new TipoEvaluacion
                {
                    Tipo = tipo,
                    IdSubModalidadPeriodoAcademico = smpaActual,
                    IdEscuela = 1
                }).ToList();

            var lstRepetidosExcel = lstTipoEvaluacion.GroupBy(x => x.Tipo.ToUpper())
                .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if (lstRepetidosExcel.Any())
            {
                return GetCodigosConcatenados(lstRepetidosExcel, DicMensajes["CodigosTipoEvaluacionRepetidos"]);
            }

            var lstTipoEvaluacionDeBase =
                DataContext.TipoEvaluacion.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual);
            var lstRepetidosDeBase =
                lstTipoEvaluacion.Where(x => lstTipoEvaluacionDeBase.Any(y => y.Tipo.ToUpper() == x.Tipo.ToUpper()))
                    .Select(x => x.Tipo).ToList();

            if (lstRepetidosDeBase.Any())
            {
                return GetCodigosConcatenados(lstRepetidosDeBase, DicMensajes["CodigosBaseRepetidosTipoEvaluacion"]);
            }

            using (var transaction = new TransactionScope())
            {
                foreach (var item in lstTipoEvaluacion)
                {
                    DataContext.TipoEvaluacion.Add(item);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        /// <summary>
        /// Inserta los Tipos de Evaluadores (Roles) para el periodo académico actual POR CURSO.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Tipos de Evaluadores no deben estar repetidos en el Excel.
        /// b) Los Tipos de Evaluadores no deben en base de datos previamente a la carga.
        /// c) La suma de pesos entre los evaluadores debe ser igual a 1.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String CoursesToEvaluationBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes)
        {
            Int32 periodoAcademicoActualId = (from submoda in DataContext.SubModalidadPeriodoAcademico
                                              join pa in DataContext.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                              where (submoda.IdSubModalidad == 1 && pa.Estado == "ACT")
                                              select submoda.IdPeriodoAcademico).FirstOrDefault();


            /* VERIFICAR QUE ROLES YA SE HAN INGRESADO EN EL CICLO ACTUAL
            if (DataContext.TipoEvaluador.Any(x => x.IdPeriodoAcademico == periodoAcademicoActualId))
            {
                return DicMensajes["RolesExistentes"];
            }*/

            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.CoursesToEvaluation);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<CourseToEvaluationFileModel>(0).Select(x => x).ToList();

            var dataExcel2 = new List<CourseToEvaluationFileModel>();

            dataExcel2.AddRange(dataExcel.Where(x => x.Codigo != null).Select(item=>new CourseToEvaluationFileModel
            {
                Codigo = item.Codigo

            }));

            
            /*VALIDAR QUE EL CÓDIGO DE CURSO NO EXISTE*/
            var cursos = DataContext.Curso.ToList();
            var listaCodNoExistentes = dataExcel2.Where(x => cursos.All(y => y.Codigo != x.Codigo)).Select(x => x.Codigo).ToList();

            if (listaCodNoExistentes.Any())
                return GetCodigosConcatenados(listaCodNoExistentes, "La siguiente lista de códigos no existen en registro de los cursos: ");



            /*VALIDAR QUE EXISTAN EN EL CICLO ACTUAL*/
            var cursoPeriodoAcademico = DataContext.CursoPeriodoAcademico.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).ToList();
            var listaCodNoExistentesCicloActual = new List<String>();
            foreach (var cursoevaluar  in dataExcel2)
            {
                var curso = DataContext.CursoPeriodoAcademico.Where(x => x.Curso.Codigo == cursoevaluar.Codigo).FirstOrDefault();
                if (curso == null)
                {
                    listaCodNoExistentesCicloActual.Add(cursoevaluar.Codigo);
                    continue;
                }
            }
            if (listaCodNoExistentesCicloActual.Any())
                return GetCodigosConcatenados(listaCodNoExistentesCicloActual, "La siguiente lista de códigos no existen registrados como curso del ciclo actual :");

            /*VALIDAR QUE NO EXISTAN EN BASE DE DATOS*/

            var cursosEvaluar = DataContext.CursosEvaluarRubrica.Where(x=>x.IdEscuela==1 && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).ToList();

            var codigoCursosExistentes = (from c in cursosEvaluar
                                          join d in dataExcel2 on c.Curso.Codigo.ToUpper() equals d.Codigo.ToUpper()
                                          select c.Curso.Codigo).ToList();

            if (codigoCursosExistentes.Any())
                return GetCodigosConcatenados(codigoCursosExistentes, "Los siguientes códigos de cursos ya se encuentran registrados para ser evaluados: ");




            var lstRepetidos = dataExcel2.GroupBy(x => x.Codigo).Where(g => g.Count() > 1).Select(g => g.Key).ToList(); ;
            if (lstRepetidos.Any())
                return GetCodigosConcatenados(lstRepetidos, "Los siguiente códigos se encuentran repetidos en el excel ");


            /*INSERTAR EN BASE DE DATOS*/
            var lista = new List<CursosEvaluarRubrica>();
            var smpa = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            lista.AddRange(dataExcel2.Select(x => new CursosEvaluarRubrica
            {
                IdCurso = DataContext.Curso.Where(y => y.Codigo == x.Codigo).FirstOrDefault().IdCurso,
                IdEscuela = 1,
                IdSubModalidadPeriodoAcademicoModulo = smpa
            }));
            using (var transaction = new TransactionScope())
            {
                DataContext.CursosEvaluarRubrica.AddRange(lista);
                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        /// <summary>
        /// Inserta los Tipos de Evaluadores (Roles) para el periodo académico actual POR CURSO.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Tipos de Evaluadores no deben estar repetidos en el Excel.
        /// b) Los Tipos de Evaluadores no deben en base de datos previamente a la carga.
        /// c) La suma de pesos entre los evaluadores debe ser igual a 1.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String EvaluatorTypeBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes, int? idPeriodoAcademico)
        {
            var periodoAcademicoActualId = idPeriodoAcademico;
            /*var periodoAcademicoActualId = (from smpa in DataContext.SubModalidadPeriodoAcademico join
                                            pa in DataContext.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                              where pa.Estado == "ACT" && smpa.SubModalidad.NombreEspanol == "Regular"
                                              select pa.IdPeriodoAcademico).FirstOrDefault();*/
            Int32 smpaActual = DataContext.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            Int32 smpaModuloAct = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;


            //VERIFICAR QUE ROLES YA SE HAN INGRESADO EN EL CICLO ACTUAL
            if (DataContext.TipoEvaluador.Any(x => x.IdSubModalidadPeriodoAcademico == smpaActual))
            {
                return DicMensajes["RolesExistentes"];
            }
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.EvaluatorType);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<EvaluatorTypeFileModel>(0).Select(x => x).ToList();
            
            var lstCodigoNoExistentes = new List<string>();
            var lstCodCursosNoExistentes = new List<String>();
            var lstTiposEvaluador = new List<TipoEvaluador>();
            var lstTiposEvaluadorPorCurso = new List<String>();
         

            foreach (var rowExcel in dataExcel)
            {
                if (rowExcel.Codigo == null || rowExcel.Peso == null || rowExcel.Curso == null)
                    continue;

                var maestroTipoEvaluador = DataContext.TipoEvaluadorMaestra.Where(x => x.Codigo.ToUpper() == rowExcel.Codigo.ToUpper()).FirstOrDefault();
                if (maestroTipoEvaluador != null)
                {
                    var curso = DataContext.CursosEvaluarRubrica.Where(x => x.Curso.Codigo.ToUpper() == rowExcel.Curso.ToUpper() && x.IdSubModalidadPeriodoAcademicoModulo == smpaModuloAct).FirstOrDefault();

                    if (curso == null)
                    {
                        lstCodCursosNoExistentes.Add(rowExcel.Curso);
                    }
                    else {

                        /*VALIDAR QUE LOS CÓDIGOS NO ESTÉN ASIGNADOS AL CURSO*/

                        var validarTipoEvaluador = DataContext.TipoEvaluador.Where(x => x.TipoEvaluadorMaestra.Codigo == rowExcel.Codigo && x.IdCurso == curso.IdCurso && x.IdSubModalidadPeriodoAcademico == smpaActual).FirstOrDefault();

                        if(validarTipoEvaluador != null)
                        {
                            lstTiposEvaluadorPorCurso.Add(rowExcel.Codigo + " - " + rowExcel.Curso);
                        }
                        else { 
                            lstTiposEvaluador.Add(new TipoEvaluador
                            {
                                Peso = rowExcel.Peso.ToDecimal(),
                                IdCurso = curso.IdCurso,
                                IdSubModalidadPeriodoAcademico = smpaActual,
                                idTipoEvaluadorMaestra = maestroTipoEvaluador.idTipoEvaluador                                                         
                               
                            });
                        }
                    }
                }
                else
                    lstCodigoNoExistentes.Add(rowExcel.Codigo);
            }

            //VALIDAR QUE LOS CÓDIGOS INGRESADOS DEACUERDO AL CURSO NO EXISTAN COMO DATA EN LA TABLA TIPOEVALUADOR
            if (lstTiposEvaluadorPorCurso.Any())
            {
                return GetCodigosConcatenados(lstTiposEvaluadorPorCurso, "Los siguientes códigos de roles por curso ya existen en la base de datos: ");
            }

            /*VALIDAR CODIGO QUE SEAN EXISTENTES EN LA TABLA TIPOEVALUADOR MAESTRO*/
            if (lstCodigoNoExistentes.Any())
                return GetCodigosConcatenados(lstCodigoNoExistentes, DicMensajes["NoExisteRolCreado"]);

            /*VALIDAR QUE LOS CÓDIGOS DE LOS CURSOS SEAN EXISTENTES*/
            if (lstCodCursosNoExistentes.Any())
                return GetCodigosConcatenados(lstCodCursosNoExistentes, "Los códigos de cursos ingresados no existen para la evaluación del ciclo actual : ");


            /*VALIDAR QUE EXISTAN UN ROL ASIGNADO COMO EVALUADOR COMITE CURSO INGRESADO*/
            /*VALIDAR QUE NO EXISTAN CÓDIGOS REPETIDOS DE ROL DE EVALUADOR POR CURSO INGRESADO*/
            

            var CursosParaEvaluar = DataContext.CursosEvaluarRubrica.Where(x=>x.IdEscuela==1 && x.IdSubModalidadPeriodoAcademicoModulo== smpaModuloAct).ToList();

            var CursosParaEvaluar2 = (from xd in CursosParaEvaluar
                                      join lstTE in lstTiposEvaluador on xd.IdCurso equals lstTE.IdCurso
                                      where xd.IdEscuela == 1
                                      select new
                                      {
                                          IdCurso = xd.IdCurso,
                                          Codigo = xd.Curso.Codigo,
                                          Peso = lstTE.Peso,
                                          IdSubModalidadPeriodoAcademico = lstTE.IdSubModalidadPeriodoAcademico,
                                          idTipoEvaluadorMaestra = lstTE.idTipoEvaluadorMaestra
                                      }).AsQueryable();
            foreach (var item in CursosParaEvaluar2)
            {
                var existeRol = lstTiposEvaluador.Where(x => x.IdCurso == item.IdCurso  && x.Peso == 1).ToList();
                //if (!existeRol.Any())
                //    return "Debe asignar el valor de 1 al campo ESROLCOMITE, para el código de curso: " + item.Codigo;
                //if (existeRol.Count() > 1)
                //    return "Debe asignar el valor de 1 al campo ESROLCOMITE solo a un registro, para el código de curso" + item.Codigo;

                 var lstRepetido = lstTiposEvaluador.Where(x => x.IdCurso == item.IdCurso && x.IdSubModalidadPeriodoAcademico == smpaActual).
               GroupBy(x => x.idTipoEvaluadorMaestra).Where(g => g.Count() > 1).Select(g => g.Key.ToString()).ToList(); 
                if (lstRepetido.Any())
                     return GetCodigosConcatenados(lstRepetido, "Existen códigos de roles de evaluadores repetidos para el código de curso: "+ item.Codigo);

            }
            

            using (var transaction = new TransactionScope())
            {
                foreach (var tipoEvaluador in lstTiposEvaluador)
                {
                    DataContext.TipoEvaluador.Add(tipoEvaluador);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }
                        
        /// <summary>
        /// Inserta los Tipos de Evaluadores (Roles) para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Tipos de Evaluadores no deben estar repetidos en el Excel.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
       
        public static String EvaluatorTypeMasterBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.EvaluatorTypeMaster);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<EvaluatorTypeMasterFileModel>(0).Select(x=>x).ToList();

            var lstTiposEvaluador = new List<TipoEvaluadorMaestra>();
            lstTiposEvaluador.AddRange(dataExcel.Where(x => x.Codigo != null && x.Nombre != null).Select(item => new TipoEvaluadorMaestra
            {
                Nombre = item.Nombre,
                Codigo = item.Codigo,
                IdEscuela = 1
            }));

            /*VALIDACIÓN NO REPETIDOS POR CÓDIGO*/
            var lstCodigosRepetidosExcel = lstTiposEvaluador.GroupBy(x => x.Codigo.ToUpper())
               .Where(g => g.Count() > 1).Select(g => g.Key).ToList();
            
            if (lstCodigosRepetidosExcel.Any())
                return GetCodigosConcatenados(lstCodigosRepetidosExcel, DicMensajes["CodigosRepetidosTiposEvaluador"]);

            /*VALIDACIÓN NO REPETIDOS POR NOMBRE*/
            var lstnNombresRepetidosExcel = lstTiposEvaluador.GroupBy(x => x.Nombre.ToUpper())
               .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if(lstnNombresRepetidosExcel.Any())
                return GetCodigosConcatenados(lstnNombresRepetidosExcel, DicMensajes["NombresRepetidosTiposEvaluador"]);


            var roles = DataContext.TipoEvaluadorMaestra.ToList();
            var lstCodigosRepetidosDB =
    lstTiposEvaluador.Where(x => !roles.All(y => y.Codigo.ToUpper() != x.Codigo.ToUpper()))
        .Select(x => x.Codigo).ToList();

            if (lstCodigosRepetidosDB.Any())
            {
                return GetCodigosConcatenados(lstCodigosRepetidosDB, DicMensajes["TiposEvaluadorDeBase"]);
            }
            

            using (var transaction = new TransactionScope())
            {
                foreach (var tipoEvaluador in lstTiposEvaluador)
                {
                    DataContext.TipoEvaluadorMaestra.Add(tipoEvaluador);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        public static String GrupoComiteBulkInsert(AbetEntities DataContext, String PathFile,
           Dictionary<String, String> DicMensajes)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.GrupoComite);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<GrupoComiteFileModel>(0).Select(x => x).ToList();

            var lstGrupoComite = new List<GrupoComiteFileModel>();


            Int32 periodoAcademicoActualId = (from submoda in DataContext.SubModalidadPeriodoAcademico
                                              join pa in DataContext.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                              where (submoda.IdSubModalidad == 1 && pa.Estado == "ACT")
                                              select submoda.IdSubModalidadPeriodoAcademico).FirstOrDefault();

       
   


            lstGrupoComite.AddRange(dataExcel.Where(x => x.CodigoDocente!=null).Select(item => new GrupoComiteFileModel
            {
                CodigoDocente = item.CodigoDocente
            }));

            /*VALIDACIÓN NO REPETIDOS POR CÓDIGO*/
            var lstCodigosRepetidosExcel = lstGrupoComite.GroupBy(x => x.CodigoDocente.ToUpper())
               .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if (lstCodigosRepetidosExcel.Any())
                return GetCodigosConcatenados(lstCodigosRepetidosExcel, "Los siguientes códigos de docentes se encuentran repetidos: ");

            //CODIGO DE DOCENTES EXISTAN
            List<String> ListNoExistenDocente = new List<String>();
            foreach (var item in lstGrupoComite)
            {
                var docente = DataContext.Docente.Where(x => x.Codigo.ToUpper() == item.CodigoDocente.ToUpper()).FirstOrDefault();

                if (docente == null)
                    ListNoExistenDocente.Add(item.CodigoDocente);

            }

            if (ListNoExistenDocente.Any())
            {
                return GetCodigosConcatenados(ListNoExistenDocente, "La siguiente lista de código de docente no existen: ");
            }

            List<String> ListExistenEnGrupoComite = new List<String>();
            foreach (var item in lstGrupoComite)
            {
                var grupocomite = DataContext.GrupoComite.Where(x => x.Docente.Codigo.ToUpper() == item.CodigoDocente.ToUpper()
                && x.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault();

                if (grupocomite != null)
                    ListExistenEnGrupoComite.Add(item.CodigoDocente);
            }

            if (ListExistenEnGrupoComite.Any())
            {
                return GetCodigosConcatenados(ListExistenEnGrupoComite, "La siguiente lista de código de docente ya existen asignados al grupo comité: ");
            }
            

            using (var transaction = new TransactionScope())
            {
                foreach (var item in lstGrupoComite)
                {
                    var docente = DataContext.Docente.Where(x => x.Codigo.ToUpper() == item.CodigoDocente.ToUpper()).FirstOrDefault();
                    var grupocomite = new GrupoComite
                    {
                        IdDocente = docente.IdDocente,
                        IdSubModalidadPeriodoAcademico = periodoAcademicoActualId
                    };

                    DataContext.GrupoComite.Add(grupocomite);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        /// <summary>
        /// Inserta las Evaluaciones de Cursos para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Tipos de Evaluación deben existir.
        /// b) Los Tipos de Evaluación no deben ser repetidos por curso.
        /// c) La suma de las evaluaciones debe ser igual a 1.
        /// d) Los Códigos de las carreras deben existir.
        /// e) Los Códigos de las cursos deben existir.
        /// f) Los cursos no deben tener ningúna evaluación registrada previamente a la carga.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String EvaluationBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.Evaluation);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<EvaluationFileModel>(0).Select(x => x).ToList();

            var periodoAcademicoActualId = (from smpa in DataContext.SubModalidadPeriodoAcademico
                                            join pa in DataContext.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                                            where pa.Estado == "ACT" && smpa.SubModalidad.NombreEspanol == "Regular"
                                            select pa.IdPeriodoAcademico).FirstOrDefault();
            Int32 smpaActual = DataContext.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            Int32 smpaModuloAct = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            //VALIDAR QUE NO EXISTA DATA 
            var  validar = (from b in DataContext.Evaluacion
                            join d in DataContext.CarreraCursoPeriodoAcademico on b.IdCarreraCursoPeriodoAcademico equals d.IdCarreraCursoPeriodoAcademico
                            join c in DataContext.CursoPeriodoAcademico on d.IdCursoPeriodoAcademico equals c.IdCursoPeriodoAcademico
                                                     
                            where
                            c.IdSubModalidadPeriodoAcademico == smpaActual
                            select b).Distinct().ToList();
            if(validar.Count()>0)
            {
                return "Ya se encuentra evaluaciones registradas para el presente ciclo, si deseas agregar nuevas ir a la opción de mantenimiento de tabla evaluación.";
            }



            var lstEvaluationFileModel = new List<EvaluationFileModel>();

            lstEvaluationFileModel.AddRange(dataExcel.Where(x=>x.Curso!=null && x.TipoEvaluacion!=null && x.Carrera!=null && x.Nota!=null).Select(item => new EvaluationFileModel
            {
                Curso = item.Curso,
                Carrera = item.Carrera,
                TipoEvaluacion = item.TipoEvaluacion,
                Nota = item.Nota
            }));

            /*VALIDAR QUE LOS CURSOS INGRESADOS ESTEN REGISTRADOS EN LA TABLA CURSOSEVALUARRUBRICA  */
            var cursosEvaluarRubrica = DataContext.CursosEvaluarRubrica.Where(x => x.IdEscuela == 1 && x.IdSubModalidadPeriodoAcademicoModulo == smpaModuloAct).Select(x=>x.Curso.Codigo).ToList();
            var lstCodNoCursosEvaluar = lstEvaluationFileModel.Where(x => cursosEvaluarRubrica.All(y => y != x.Curso)).Select(x=>x.Curso).ToList();

            if(lstCodNoCursosEvaluar.Any())
            {
                return GetCodigosConcatenados(lstCodNoCursosEvaluar, "Los siguientes códigos de cursos no están registrados para ser evaluados este ciclo.");
            }

            var lstCarreras = DataContext.Carrera;
            var lstCarrerasNoExistentes =
                lstEvaluationFileModel.Where(x => lstCarreras.All(y => y.Codigo.ToUpper() != x.Carrera.ToUpper()))
                    .Select(x => x.Carrera).ToList();

            if (lstCarrerasNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstCarrerasNoExistentes, DicMensajes["CarrerasNoExistentes"]);
            }

            var lstTipoEvaluacion =
                DataContext.TipoEvaluacion.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual);
            var lstTiposEvaluacionNoExistentes =
                lstEvaluationFileModel.Where(x => lstTipoEvaluacion.All(y => y.Tipo.ToUpper() != x.TipoEvaluacion.ToUpper()))
                    .Select(x => x.TipoEvaluacion).ToList();

            if (lstTiposEvaluacionNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstTiposEvaluacionNoExistentes, DicMensajes["TiposEvaluacionNoExistentes"]);
            }

            var lstCursoPeriodoAcademico = DataContext.CursoPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == smpaActual);
            var lstIdCursos = lstCursoPeriodoAcademico.Select(x => x.IdCurso);
            var lstCursos = DataContext.Curso.Where(x => lstIdCursos.Contains(x.IdCurso));
            var lstCursosNoExistentes =
                lstEvaluationFileModel.Where(x => lstCursos.All(y => y.Codigo.ToUpper() != x.Curso.ToUpper()))
                    .Select(x => x.Curso).ToList();

            if (lstCursosNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstCursosNoExistentes, DicMensajes["CursosNoExistentes"]);
            }

            var lstEvaluacionValidator = new List<EvaluationValidatorModel>();
            var lstCarreraCursoPeriodoAcademico = DataContext.CarreraCursoPeriodoAcademico.ToList();

            foreach (var item in lstEvaluationFileModel)
            {
                Int32 carreraId = lstCarreras.FirstOrDefault(x => x.Codigo == item.Carrera).IdCarrera;
                var curso = lstCursos.FirstOrDefault(x => x.Codigo == item.Curso);
                var tipoEvaluacionId =
                    lstTipoEvaluacion.FirstOrDefault(x => x.Tipo == item.TipoEvaluacion).IdTipoEvaluacion;

                Int32 cursoPeriodoAcademicoId = lstCursoPeriodoAcademico.FirstOrDefault(x => x.IdCurso == curso.IdCurso && x.IdSubModalidadPeriodoAcademico == smpaActual).IdCursoPeriodoAcademico;
                Int32 carreraCursoPeriodoAcademicoId = lstCarreraCursoPeriodoAcademico.
                                    FirstOrDefault(x => x.IdCursoPeriodoAcademico == cursoPeriodoAcademicoId && x.IdCarrera == carreraId).IdCarreraCursoPeriodoAcademico;

                var objEvaluacionValidator = new EvaluationValidatorModel
                {
                    Peso = item.Nota.ToDecimal(),
                    TipoEvaluacionId = tipoEvaluacionId,
                    CarreraCursoPeriodoAcademicoId = carreraCursoPeriodoAcademicoId,
                    CursoId = curso.IdCurso,
                    CodigoCurso = curso.Codigo
                };

                lstEvaluacionValidator.Add(objEvaluacionValidator);
            }

            var lstCursosPesoDiferenteUno =
                (
                    from item in lstEvaluacionValidator
                    group item by new { item.CursoId, item.CodigoCurso, item.CarreraCursoPeriodoAcademicoId }
                        into g
                        where g.Sum(y => y.Peso) != 1
                        select g.Key.CodigoCurso
                    ).ToList();

            if (lstCursosPesoDiferenteUno.Any())
            {
                return GetCodigosConcatenados(lstCursosPesoDiferenteUno, DicMensajes["CursoPesoDiferenteUno"]);
            }

            var lstCursosEvaluacionesRepetidas = (
                from item in lstEvaluacionValidator
                group item by new { item.CursoId, item.CodigoCurso, item.CarreraCursoPeriodoAcademicoId, item.TipoEvaluacionId }
                    into g
                    where g.Count() > 1
                    select g.Key.CodigoCurso
                ).ToList();

            if (lstCursosEvaluacionesRepetidas.Any())
            {
                return GetCodigosConcatenados(lstCursosEvaluacionesRepetidas, DicMensajes["CursoEvaluacionRepetida"]);
            }

            var lstCarreraCursoPeriodoAcademicoFiltro =
                lstCarreraCursoPeriodoAcademico.Where(x => lstEvaluacionValidator.Any(y => y.CarreraCursoPeriodoAcademicoId == x.IdCarreraCursoPeriodoAcademico));
            var lstCursosConEvaluacionesId =
                lstCarreraCursoPeriodoAcademicoFiltro.Where(x => x.Evaluacion.Any()).Select(x => x.IdCursoPeriodoAcademico);
            var lstCursosConEvaluaciones = lstCursoPeriodoAcademico.Where(x => lstCursosConEvaluacionesId.Contains(x.IdCursoPeriodoAcademico))
                                                    .Select(x => x.Curso.Codigo).ToList();

            if (lstCursosConEvaluaciones.Any())
            {
                return GetCodigosConcatenados(lstCursosConEvaluaciones, DicMensajes["CursosConEvaluaciones"]);
            }

            using (var transaction = new TransactionScope())
            {
                foreach (var item in lstEvaluacionValidator)
                {
                    var objEvaluacion = new Evaluacion
                    {
                        IdTipoEvaluacion = item.TipoEvaluacionId,
                        Peso = item.Peso,
                        IdCarreraCursoPeriodoAcademico = item.CarreraCursoPeriodoAcademicoId
                    };
                    DataContext.Evaluacion.Add(objEvaluacion);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        /// <summary>
        /// Inserta los Evaluadores para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Códigos de los Proyectos debe existir.
        /// b) Los Códigos de los Docentes debe existir.
        /// c) Los Tipos de Evaluadores deben existir.
        /// d) Los Docentes no se pueden repetir en un mismo proyecto.
        /// e) Cada proyecto debe tener todos los tipos de evaluador. --> Se remueve esta validación (victor.moran)
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String EvaluatorBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.Evaluator);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<EvaluatorFileModel>(0).Select(x => x).ToList();

            Int32 periodoAcademicoActualId = DataContext.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
                .FirstOrDefault(x=>x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;

            var lstEvaluador = new List<EvaluatorFileModel>();
            lstEvaluador.AddRange(dataExcel.Where(x=>x.CodigoDocente!=null && x.CodigoProyecto!=null && x.TipoEvaluador!=null).Select(item => new EvaluatorFileModel
            {
                CodigoDocente = item.CodigoDocente,
                CodigoProyecto = item.CodigoProyecto,
                TipoEvaluador = item.TipoEvaluador
            }));

            var lstDocente = DataContext.Docente;

            var lstDocentesNoExistentes =
                lstEvaluador.Where(x => lstDocente.All(y => y.Codigo.ToUpper() != x.CodigoDocente.ToUpper()))
                    .Select(x => x.CodigoDocente).ToList();

            /*VALIDAR QUE LOS CODIGOS DE DOCENTES EXISTAN*/
            if (lstDocentesNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstDocentesNoExistentes, DicMensajes["DocentesNoExistentes"]);
            }

            var lstProyectos = DataContext.ProyectoAcademico.Where(x => x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicoActualId);
           
            var lstProyectosNoExistentes =
                lstEvaluador.Where(x => lstProyectos.All(y => y.Codigo.ToUpper() != x.CodigoProyecto.ToUpper()))
                    .Select(x => x.CodigoProyecto).ToList();
            /*VALIDAR QUE LOS PROYECTOS EXISTAN*/
            if (lstProyectosNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstProyectosNoExistentes, DicMensajes["ProyectosNoExistentes"]);
            }

            /*
            var lstTipoEvaluador =
                DataContext.TipoEvaluador.Where(x => x.IdPeriodoAcademico == periodoAcademicoActualId);

            var lstTiposNoExistentes =
                lstEvaluador.Where(x => lstTipoEvaluador.All(y => y.TipoEvaluadorMaestra.Codigo.ToUpper() != x.TipoEvaluador.ToUpper()))
                    .Select(x => x.TipoEvaluador).ToList();
                    */
            var lstTipoEvaluador = DataContext.TipoEvaluadorMaestra.ToList();

            var lstTiposNoExistentes =
                lstEvaluador.Where(x => lstTipoEvaluador.All(y => y.Codigo.ToUpper() != x.TipoEvaluador.ToUpper()))
                    .Select(x => x.TipoEvaluador).ToList();

            if (lstTiposNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstTiposNoExistentes, DicMensajes["TipoEvaluadorNoExistente"]);
            }

            /*VALIDAR AQUI*/
            //var lstTipoEvaluadorTP1 = DataContext.TipoEvaluador.Where(x=>x.TipoCurso == ConstantHelpers.RUBRICFILETYPE.CourseProject.CODTP1).ToList();
            /*VALIR QUE EXISTE LOS CÓDIGOS DE ROLES POR CURSO  */
            /*var lstTiposNoExistentesTP1 =
                lstEvaluador.Where(x => lstTipoEvaluadorTP1.All(y => y.Codigo.ToUpper() != x.TipoEvaluador.ToUpper()))
                    .Select(x =>x.CodigoProyecto + "-"+ x.TipoEvaluador).ToList();

            if (lstTiposNoExistentesTP1.Any())
            {
                return GetCodigosConcatenados(lstTiposNoExistentesTP1, "TALLER DE PROYECTO I: Los siguientes códigos de roles de evaluadores son existen: ");
            }*/
           // var lstTipoEvaluadorTP2 = DataContext.TipoEvaluador.Where(x => x.TipoCurso == ConstantHelpers.RUBRICFILETYPE.CourseProject.CODTP2).ToList();
            /*VALIR QUE EXISTE LOS CÓDIGOS DE ROLES POR CURSO  */
           /* var lstTiposNoExistentesTP2 =
                lstEvaluador.Where(x => lstTipoEvaluadorTP2.All(y => y.Codigo.ToUpper() != x.TipoEvaluador.ToUpper()))
                    .Select(x => "Proyecto: "+x.CodigoProyecto + "-RolEvaluador: " + x.TipoEvaluador).ToList();*/
                    /*
            if (lstTiposNoExistentesTP2.Any())
            {
                return GetCodigosConcatenados(lstTiposNoExistentesTP2, "TALLER DE PROYECTO II: Los siguientes códigos de roles de evaluadores son existen: ");
            }*/


            var lstEvaluadorValidator = new List<EvaluatorValidatorModel>();

            foreach (var item in lstEvaluador)
            {
                Int32 docenteId = lstDocente.FirstOrDefault(x => x.Codigo == item.CodigoDocente).IdDocente;
                var proyecto= lstProyectos.FirstOrDefault(x => x.Codigo == item.CodigoProyecto);
                Int32 proyectoId = proyecto.IdProyectoAcademico;

                var tipoevaluador = DataContext.TipoEvaluador.FirstOrDefault(x => x.TipoEvaluadorMaestra.Codigo == item.TipoEvaluador && x.IdCurso == proyecto.IdCurso && x.IdSubModalidadPeriodoAcademico == periodoAcademicoActualId);
                
                Int32 tipoEvaluadorId = tipoevaluador.IdTipoEvaluador;
                                
                var objEvaluatorValidator = new EvaluatorValidatorModel
                {
                    DocenteId = docenteId,
                    ProyectoId = proyectoId,
                    TipoEvaluadorId = tipoEvaluadorId
                };

                lstEvaluadorValidator.Add(objEvaluatorValidator);
            }

            var lstProyectoUnicos = lstEvaluadorValidator.Select(x => x.ProyectoId).Distinct();
            var lstProyectosRepetidos = new List<String>();

            //Int32 cantidadTipoEvaluador = lstTipoEvaluador.Count();

            //foreach (var item in lstProyectoUnicos)
            //{
            //    Int32 lstTipoEvaluadorItem =
            //        lstEvaluadorValidator.Where(x => x.ProyectoId == item).Select(x => x.TipoEvaluadorId).Distinct().Count();

            //    if (lstTipoEvaluadorItem != cantidadTipoEvaluador)
            //    {
            //        lstProyectosRepetidos.Add(lstProyectos.FirstOrDefault(x => x.IdProyectoAcademico == item).Codigo);
            //    }
            //}

            //if (lstProyectosRepetidos.Any())
            //{
            //    return GetCodigosConcatenados(lstProyectosRepetidos, DicMensajes["ProyectosFaltaTipoEvaluador"]);
            //}

            foreach (var item in lstProyectoUnicos)
            {
                var lstDocentesItem =
                    lstEvaluadorValidator.Where(x => x.ProyectoId == item).Select(x => x.DocenteId);

                if (lstDocentesItem.Count() != lstDocentesItem.Distinct().Count())
                {
                    lstProyectosRepetidos.Add(lstProyectos.FirstOrDefault(x => x.IdProyectoAcademico == item).Codigo);
                }
            }

            if (lstProyectosRepetidos.Any())
            {
                return GetCodigosConcatenados(lstProyectosRepetidos, DicMensajes["ProyectosDocentesRepetidos"]);
            }

            var lstProyectoConEvaluadores = new List<String>();
            var lstEvaluadoresProyectos = DataContext.Evaluador;

            foreach (var item in lstEvaluadorValidator)
            {
                var lstEvaluadorProyectoActual = lstEvaluadoresProyectos.Where(x => x.IdProyectoAcademico == item.ProyectoId);
                if (lstEvaluadorProyectoActual.Any())
                {
                    var codigoProyecto = lstProyectos.FirstOrDefault(x => x.IdProyectoAcademico == item.ProyectoId).Codigo;
                    lstProyectoConEvaluadores.Add(codigoProyecto);
                }
            }

            if (lstProyectoConEvaluadores.Any())
            {
                return GetCodigosConcatenados(lstProyectoConEvaluadores.Distinct().ToList(), DicMensajes["ProyectosConDocentes"]);
            }

            using (var transaction = new TransactionScope())
            {
                foreach (var item in lstEvaluadorValidator)
                {
                    var objEvaluador = new Evaluador
                    {
                        IdProyectoAcademico = item.ProyectoId,
                        IdDocente = item.DocenteId,
                        IdTipoEvaluador = item.TipoEvaluadorId
                    };
                    DataContext.Evaluador.Add(objEvaluador);
                }

                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        /// <summary>
        /// Inserta los Logros de los Outcomes de WASC para el periodo académico actual.
        /// Donde los datos se obtienen de un archivo excel.
        /// Se realizan las siguientes validaciones a los datos obtenidos.
        /// a) Los Outcomes deben de existir.
        /// b) Los Outcomes deben de existir para la comisión WASC.
        /// C) Los Outcomes en el Excel no deben estar repetidos.
        /// d) Los Outcomes no deben tener registrados Logros. 
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PathFile">Ruta Fisica del Archivo Excel</param>
        /// <param name="DicMensajes">Diccionario que contiene los mensajes de respuesta de validación</param>
        /// <returns>String - Mensaje de Respuesta</returns>
        public static String OutcomeGoalCommissionBulkInsert(AbetEntities DataContext, String PathFile,
            Dictionary<String, String> DicMensajes, int idSubModalidad)
        {
            var objExcel = ObtenerExcel(PathFile);
            Boolean isValidHeader = ValidarHeaders(objExcel, BulkInsertType.OutcomeGoalCommission);

            if (!isValidHeader)
            {
                return DicMensajes["HeadersInvalidos"];
            }

            var dataExcel = objExcel.Worksheet<OucomeGoalCommissionFileModel>(0).Select(x => x).ToList();

            var lstOutcomeGoalCommission = new List<OucomeGoalCommissionFileModel>();
            lstOutcomeGoalCommission.AddRange(dataExcel.Select(item => new OucomeGoalCommissionFileModel
            {
                CompetenciaGeneral = item.CompetenciaGeneral,
                LogroEsperado = item.LogroEsperado,
                LogroEjemplar = item.LogroEjemplar,
                LogroSobresaliente = item.LogroSobresaliente
            }));

            //Int32 periodoAcademicoActualId = DataContext.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico)
            //    .FirstOrDefault(x=>x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;


            var periodoAcademicosActivo = (from smpam in DataContext.SubModalidadPeriodoAcademicoModulo
                                           join smpa in DataContext.SubModalidadPeriodoAcademico on smpam.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                           where (smpa.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && smpa.IdSubModalidad == idSubModalidad)
                                           select new
                                           {
                                               IdPeriodoAcademico = smpa.IdPeriodoAcademico
                                           }).FirstOrDefault();

            var lstRepetidosExcel = lstOutcomeGoalCommission.GroupBy(x => x.CompetenciaGeneral.ToUpper())
                .Where(g => g.Count() > 1).Select(g => g.Key).ToList();

            if (lstRepetidosExcel.Any())
            {
                return GetCodigosConcatenados(lstRepetidosExcel, DicMensajes["OutcomesRepetidos"]);
            }

            var lstOutcome = DataContext.Outcome;
            var lstOutcomesNoExistentes =
                lstOutcomeGoalCommission.Where(x => lstOutcome.All(y => y.Nombre.ToUpper() != x.CompetenciaGeneral.ToUpper()))
                    .Select(x => x.CompetenciaGeneral)
                    .ToList();

            if (lstOutcomesNoExistentes.Any())
            {
                return GetCodigosConcatenados(lstOutcomesNoExistentes, DicMensajes["OutcomesNoExistentes"]);
            }

            Int32 wascId = GetWascId(DataContext, periodoAcademicosActivo.IdPeriodoAcademico);

            var lstOutcomeComission =
                DataContext.OutcomeComision.Where(
                    x => x.IdComision == wascId && x.SubModalidadPeriodoAcademico.IdPeriodoAcademico == periodoAcademicosActivo.IdPeriodoAcademico);

            var lstOutcomesNoExistenComision =
                lstOutcomeGoalCommission.Where(
                    x => lstOutcomeComission.All(y => y.Outcome.Nombre.ToUpper() != x.CompetenciaGeneral.ToUpper()))
                    .Select(x => x.CompetenciaGeneral).ToList();

            if (lstOutcomesNoExistenComision.Any())
            {
                return GetCodigosConcatenados(lstOutcomesNoExistenComision, DicMensajes["OutcomesNoExistentesEnComision"]);
            }

            var lstOucomeGoal = new List<LogroOutcomeComision>();

            if(lstOutcomeComission.ToList().Count == 0)
            {
                throw new Exception("No hay coincidencias de las Competencias Generales y la data del archivo. Verifique");
            }
            foreach (var item in lstOutcomeComission)
            {
                var nombreOutcome = lstOutcome.FirstOrDefault(x => x.IdOutcome == item.IdOutcome).Nombre;
                OucomeGoalCommissionFileModel objOutcomeGoal = lstOutcomeGoalCommission.FirstOrDefault(x => x.CompetenciaGeneral.RemoveDiacritics().ToUpper() == nombreOutcome.RemoveDiacritics().ToUpper());

                if (objOutcomeGoal != null)
                {
                    if (item.LogroOutcomeComision.Any())
                    {
                        return DicMensajes["OutcomeYaRegistrado"] + nombreOutcome;
                    }

                    for (Int32 i = 0; i < ConstantHelpers.RUBRICFILETYPE.WascCommission.WASC_GOALS; i++)
                    {
                        var objLogro = new LogroOutcomeComision
                        {
                            IdOutcomeComision = item.IdOutcomeComision,
                            Orden = i
                        };

                        switch (i)
                        {
                            case 0:
                                objLogro.Nombre = ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_ESPERADO;
                                objLogro.Descripcion = objOutcomeGoal.LogroEsperado;
                                break;
                            case 1:
                                objLogro.Nombre = ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_SOBRESALIENTE;
                                objLogro.Descripcion = objOutcomeGoal.LogroSobresaliente;
                                break;
                            case 2:
                                objLogro.Nombre = ConstantHelpers.RUBRICFILETYPE.OutcomeGoalCommissionFile.LOGRO_EJEMPLAR;
                                objLogro.Descripcion = objOutcomeGoal.LogroEjemplar;
                                break;
                        }

                        lstOucomeGoal.Add(objLogro);
                    }
                }
            }

            using (var transaction = new TransactionScope())
            {
                lstOucomeGoal.ForEach(x => DataContext.LogroOutcomeComision.Add(x));
                DataContext.SaveChanges();
                transaction.Complete();
            }

            return ConstantHelpers.RUBRICFILETYPE.MessageResult.EXCEL_PROCESADO;
        }

        #endregion
    }
}
