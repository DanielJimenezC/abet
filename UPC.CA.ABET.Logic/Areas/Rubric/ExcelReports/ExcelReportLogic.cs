﻿#region Imports

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ClosedXML.Excel;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel;
using System.IO;
using UPC.CA.ABET.Logic.Areas.Rubric.ExcelBulkInsert;
using UPC.CA.ABET.Logic.Areas.Meeting;

#endregion

namespace UPC.CA.ABET.Logic.Areas.Rubric.ExcelReports
{
    public class ExcelReportLogic
    {
        #region Utilitarios

        /// <summary>
        /// Retorna los datos necesarios para los reportes de los alumnos 
        /// que esten matriculados en TP1 o TP2.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <returns>List<StudentReportModel></returns>
        public static List<StudentReportModel> GetAlumnosSeccionId(AbetEntities DataContext, Int32 IdCarrera,
            Int32 PeriodoAcademicoActualId, int? idEscuela)
        {
            //var cursosProyectos = BulkInsertLogic.ListCourseProject(DataContext, PeriodoAcademicoActualId, idEscuela);

            return (from a in DataContext.AlumnoMatriculado
                    join b in DataContext.AlumnoSeccion on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                    join c in DataContext.Seccion on b.IdSeccion equals c.IdSeccion
                    join d in DataContext.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals
                        d.IdCursoPeriodoAcademico
                    join e in DataContext.Curso on d.IdCurso equals e.IdCurso
                    join f in DataContext.Carrera on a.IdCarrera equals f.IdCarrera
                    join g in DataContext.Alumno on a.IdAlumno equals g.IdAlumno
                    join h in DataContext.Sede on a.IdSede equals h.IdSede
                    join i in DataContext.Seccion on b.IdSeccion equals i.IdSeccion
                    where
                        a.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId &&
                        d.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId &&
                        f.IdCarrera == IdCarrera &&
                        f.IdEscuela == idEscuela &&
                        b.Nota != "RET" && b.Nota != "NR"  && b.Nota != "DPI"
                    select new StudentReportModel()
                    {
                        CursoId = e.IdCurso,
                        CodigoCurso = e.Codigo,
                        AlumnoSeccionId = b.IdAlumnoSeccion,
                        CodigoCarrera = f.Codigo,
                        NombreAlumno = g.Apellidos + " " + g.Nombres,
                        CodigoAlumno = g.Codigo,
                        NombreCarrera = f.NombreEspanol,
                        NombreCurso = e.NombreEspanol,
                        SedeAlumno = h.Nombre,
                        SeccionCurso = i.Codigo
                    }
                ).ToList();
        }

        /// <summary>
        /// Retorna los datos necesarios para los reportes de los alumnos 
        /// que esten matriculados en TP1 o TP2.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <returns>List<StudentReportModel></returns>
        public static List<StudentReportModel> GetAlumnosSeccionId(AbetEntities DataContext,
            Int32 SubModalidadPeriodoAcademicoActualId,int? idEscuela)
        {
            //var cursosProyectos = BulkInsertLogic.ListCourseProject(DataContext, PeriodoAcademicoActualId, idEscuela);

            int parSubModalidadPeriodoAcademicoModuloId = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;

            return (from a in DataContext.AlumnoMatriculado
                    join b in DataContext.AlumnoSeccion on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                    join c in DataContext.Seccion on b.IdSeccion equals c.IdSeccion
                    join d in DataContext.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals
                        d.IdCursoPeriodoAcademico
                    join e in DataContext.Curso on d.IdCurso equals e.IdCurso
                    join ce in DataContext.CursosEvaluarRubrica on e.IdCurso equals ce.IdCurso
                    join f in DataContext.Carrera on a.IdCarrera equals f.IdCarrera
                    join g in DataContext.Alumno on a.IdAlumno equals g.IdAlumno
                    join h in DataContext.Sede on a.IdSede equals h.IdSede
                    join i in DataContext.Seccion on b.IdSeccion equals i.IdSeccion
                    where
                        a.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoActualId &&
                        d.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoActualId &&
                        ce.IdSubModalidadPeriodoAcademicoModulo == parSubModalidadPeriodoAcademicoModuloId &&
                        ce.IdEscuela == idEscuela
                    select new StudentReportModel()
                    {
                        CursoId = e.IdCurso,
                        CodigoCurso = e.Codigo,
                        AlumnoSeccionId = b.IdAlumnoSeccion,
                        CodigoCarrera = f.Codigo,
                        NombreAlumno = g.Apellidos + " " + g.Nombres,
                        CodigoAlumno = g.Codigo,
                        NombreCarrera = f.NombreEspanol,
                        NombreCurso = e.NombreEspanol,
                        SedeAlumno = h.Nombre,
                        SeccionCurso = i.Codigo
                    }
                ).ToList();
        }

        /// <summary>
        /// Retorna los de las rubricas calificadas según el tipo de Evaluación.
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <returns>List<EvaluationReportModel></returns>
        public static List<EvaluationReportModel> GetEvaluacionesReporte(AbetEntities DataContext,
            Int32 SubModalidadPeriodoAcademicoActualId, int? idEscuela)
        {

            int parSubModalidadPeriodoAcademicoModuloId = DataContext.SubModalidadPeriodoAcademicoModulo.Where(x => x.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;


            return (from a in DataContext.Curso
                    join ce in DataContext.CursosEvaluarRubrica on a.IdCurso equals ce.IdCurso
                    join b in DataContext.CursoPeriodoAcademico on a.IdCurso equals b.IdCurso
                    join c in DataContext.CarreraCursoPeriodoAcademico on b.IdCursoPeriodoAcademico equals
                        c.IdCursoPeriodoAcademico
                    join d in DataContext.Evaluacion on c.IdCarreraCursoPeriodoAcademico equals d.IdCarreraCursoPeriodoAcademico
                    join e in DataContext.TipoEvaluacion on d.IdTipoEvaluacion equals e.IdTipoEvaluacion
                    join f in DataContext.Carrera on c.IdCarrera equals f.IdCarrera
                    where
                        b.IdSubModalidadPeriodoAcademico == SubModalidadPeriodoAcademicoActualId &&
                        ce.IdSubModalidadPeriodoAcademicoModulo == parSubModalidadPeriodoAcademicoModuloId &&
                        ce.IdEscuela == idEscuela
                    select new EvaluationReportModel()
                    {
                        EvaluacionId = d.IdEvaluacion,
                        TipoEvaluacionId = e.IdTipoEvaluacion,
                        TipoEvaluacion = e.Tipo,
                        PesoEvaluacion = d.Peso,
                        CodigoCarrera = f.Codigo,
                        CodigoCurso = a.Codigo,
                        NombreCurso = a.NombreEspanol
                    }).ToList();
        }

        public static List<OutcomeEvaluatorFinalReportModel> GetOutcomeEvaluatorList(AbetEntities DataContext,
            Int32 AlumnoSeccionId, Int32 RubricaId)
        {
            var listaOutcomes = DataContext.OutcomeCalificado.Where(x => x.RubricaCalificada.IdAlumnoSeccion == AlumnoSeccionId && x.RubricaCalificada.IdRubrica == RubricaId).
                                        Select(x => new OutcomeEvaluatorFinalReportModel
                                        {
                                            Outcome = DataContext.OutcomeRubrica.FirstOrDefault(y => y.IdOutcomeRubrica == x.IdOutcome).OutcomeComision.Outcome.Nombre,
                                            DescripcionOutcome = DataContext.OutcomeRubrica.FirstOrDefault(y => y.IdOutcomeRubrica == x.IdOutcome).OutcomeComision.Outcome.DescripcionEspanol,
                                            Comision = DataContext.OutcomeRubrica.FirstOrDefault(y => y.IdOutcomeRubrica == x.IdOutcome).OutcomeComision.Comision.Codigo,
                                            CodigoEvaluador = x.RubricaCalificada.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo,
                                            NombreEvaluador = x.RubricaCalificada.Evaluador.Docente.Nombres + " " + x.RubricaCalificada.Evaluador.Docente.Apellidos,
                                            RolEvaluador = x.RubricaCalificada.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Nombre,
                                            NotaOutcome = x.NotaOutcome,
                                            ComentarioOutcome = x.ComentarioOutcome,
                                        }).OrderByDescending(x => x.Outcome).ToList();

            return listaOutcomes;
            //(from a in DataContext.OutcomeCalificado
            //    join b in DataContext.RubricaCalificada on a.IdRubricaCalificada equals b.IdRubricaCalificada
            //    join c in DataContext.Evaluador on b.IdEvaluador equals c.IdEvaluador
            //    join d in DataContext.TipoEvaluador on c.IdTipoEvaluador equals d.IdTipoEvaluador
            //    join e in DataContext.OutcomeRubrica on a.IdOutcome equals e.IdOutcomeRubrica
            //    join f in DataContext.OutcomeComision on e.IdOutcomeComision equals f.IdOutcomeComision
            //    join g in DataContext.Outcome on f.IdOutcome equals g.IdOutcome
            //    join h in DataContext.Comision on f.IdComision equals h.IdComision
            //    join i in DataContext.Docente on c.IdDocente equals i.IdDocente
            //    where
            //        b.IdAlumnoSeccion == AlumnoSeccionId &&
            //        b.IdRubrica == RubricaId
            //    select new OutcomeEvaluatorFinalReportModel()
            //    {
            //        Outcome = g.Nombre,
            //        DescripcionOutcome = g.DescripcionEspanol,
            //        Comision = h.Codigo,
            //        CodigoEvaluador = i.Codigo,
            //        NombreEvaluador = i.Apellidos + " " + i.Nombres,
            //        RolEvaluador = d.Nombre,
            //        NotaOutcome = a.NotaOutcome,
            //        ComentarioOutcome = a.ComentarioOutcome
            //    }).ToList();
        }

        public static List<StudentCriterioOutcomeReportModel> GetOutcomeCriterioList(AbetEntities DataContext,
            Int32 AlumnoSeccionId, Int32 RubricaId)
        {
            return (from a in DataContext.OutcomeCalificado
                    join b in DataContext.RubricaCalificada on a.IdRubricaCalificada equals b.IdRubricaCalificada
                    join e in DataContext.OutcomeRubrica on a.IdOutcome equals e.IdOutcomeRubrica
                    join f in DataContext.OutcomeComision on e.IdOutcomeComision equals f.IdOutcomeComision
                    join g in DataContext.Outcome on f.IdOutcome equals g.IdOutcome
                    join h in DataContext.Comision on f.IdComision equals h.IdComision
                    join i in DataContext.Evaluador on b.IdEvaluador equals i.IdEvaluador
                    join j in DataContext.TipoEvaluador on i.IdTipoEvaluador equals j.IdTipoEvaluador
                    where
                        b.IdAlumnoSeccion == AlumnoSeccionId &&
                        b.IdRubrica == RubricaId &&
                        j.TipoEvaluadorMaestra.Codigo == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.COMITE
                    select new StudentCriterioOutcomeReportModel()
                    {
                        Outcome = g.Nombre,
                        DescripcionOutcome = g.DescripcionEspanol,
                        Comision = h.Codigo,
                        OutcomeCalificadoId = a.IdOutcomeCalificado,
                        ObservacionRubrica = b.Observacion,
                        NotaOutcome = e.NotaOutcome.Value,
                        NotaRubricaCalificada = b.NotaRubrica.Value
                    }).OrderBy(x=>x.Outcome).ToList();
        }

        public static List<StudentCriterioOutcomeReportModel> GetOutcomeCriterioListPA(AbetEntities DataContext,
    Int32 AlumnoSeccionId, Int32 RubricaId)
        {
            return (from a in DataContext.OutcomeCalificado
                    join b in DataContext.RubricaCalificada on a.IdRubricaCalificada equals b.IdRubricaCalificada
                    join e in DataContext.OutcomeRubrica on a.IdOutcome equals e.IdOutcomeRubrica
                    join f in DataContext.OutcomeComision on e.IdOutcomeComision equals f.IdOutcomeComision
                    join g in DataContext.Outcome on f.IdOutcome equals g.IdOutcome
                    join h in DataContext.Comision on f.IdComision equals h.IdComision
                    join i in DataContext.Evaluador on b.IdEvaluador equals i.IdEvaluador
                    join j in DataContext.TipoEvaluador on i.IdTipoEvaluador equals j.IdTipoEvaluador
                    where
                        b.IdAlumnoSeccion == AlumnoSeccionId &&
                        b.IdRubrica == RubricaId &&
                        j.TipoEvaluadorMaestra.Codigo == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.GERENTE
                    select new StudentCriterioOutcomeReportModel()
                    {
                        Outcome = g.Nombre,
                        DescripcionOutcome = g.DescripcionEspanol,
                        Comision = h.Codigo,
                        OutcomeCalificadoId = a.IdOutcomeCalificado,
                        ObservacionRubrica = b.Observacion,
                        NotaOutcome = e.NotaOutcome.Value,
                        NotaRubricaCalificada = b.NotaRubrica.Value
                    }).OrderBy(x=>x.Outcome).ToList();
        }
        public static List<StudentCriterioOutcomeDetailReportModel> GetOutcomeCriterioDetailList(AbetEntities DataContext,
            List<StudentCriterioOutcomeReportModel> LstOutcomes)
        {
            return (from a in LstOutcomes
                    join i in DataContext.CriterioCalificado on a.OutcomeCalificadoId equals i.IdOutcomeCalificado
                    join j in DataContext.CriterioOutcome on i.IdCriterio equals j.IdCriterioOutcome
                    select new StudentCriterioOutcomeDetailReportModel()
                    {
                        Criterio = j.Nombre,
                        DescripcionCriterio = j.Descripcion,
                        NotaAlumno = i.NotaCriterio.Value,
                        NotaCriterio = j.ValorMaximo,
                        OutcomeCalificadoId = a.OutcomeCalificadoId,
                        CriterioId = j.IdCriterioOutcome,
                        NivelesPorDefecto = j.EsEvaluadoPorDefecto
                    }).OrderBy(x => x.CriterioId).ToList();
        }

        public static StudentOutcomeReportModel GetAlumnoCriterio(AbetEntities DataContext,
                            Int32 PeriodoAcademicoActualId, Int32 AlumnoSeccionId)
        {
            return (from a in DataContext.AlumnoSeccion
                    join b in DataContext.AlumnoMatriculado on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                    join c in DataContext.Seccion on a.IdSeccion equals c.IdSeccion
                    join d in DataContext.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals
                        d.IdCursoPeriodoAcademico
                    join e in DataContext.Alumno on b.IdAlumno equals e.IdAlumno
                    join f in DataContext.CarreraCursoPeriodoAcademico on d.IdCursoPeriodoAcademico equals
                        f.IdCursoPeriodoAcademico
                    join cursoPeriodo in DataContext.CursoPeriodoAcademico on f.IdCursoPeriodoAcademico equals cursoPeriodo.IdCursoPeriodoAcademico
                    join curso in DataContext.Curso on cursoPeriodo.IdCurso equals curso.IdCurso
                    join g in DataContext.Carrera on f.IdCarrera equals g.IdCarrera
                    join h in DataContext.ProyectoAcademico on a.IdProyectoAcademico equals h.IdProyectoAcademico
                    where
                        a.IdAlumnoSeccion == AlumnoSeccionId &&
                        b.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId &&
                        b.IdCarrera == f.IdCarrera
                    select new StudentOutcomeReportModel()
                    {
                        AlumnoId = b.IdAlumno,
                        AlumnoSeccionId = a.IdAlumnoSeccion,
                        CarreraId = b.IdCarrera.Value,
                        CursoId = d.IdCurso,
                        CarreraCursoPeriodoAcademicoId = f.IdCarreraCursoPeriodoAcademico,
                        NombreAlumno = e.Nombres,
                        Sede = e.Apellidos,
                        Curso = h.Nombre +" "+h.Descripcion==null?"":h.Descripcion,
                        CodigoAlumno = e.Codigo,
                        Carrera = g.NombreEspanol,
                        Seccion = c.Codigo,
                        CodigoProyecto = h.Codigo,
                        CodigoCursoReporte = curso.Codigo
                    }).FirstOrDefault();
        }

        public static Int32 GetEvaluacionId(AbetEntities DataContext, Int32 PeriodoAcademicoActualId,
            Int32 CarreraCursoPeriodoAcademicoId, String NombreEvaluacion)
        {
            return (from a in DataContext.TipoEvaluacion
                    join b in DataContext.Evaluacion on a.IdTipoEvaluacion equals b.IdTipoEvaluacion
                    where
                        a.SubModalidadPeriodoAcademico.IdPeriodoAcademico == PeriodoAcademicoActualId &&
                        b.IdCarreraCursoPeriodoAcademico == CarreraCursoPeriodoAcademicoId &&
                        a.Tipo.ToUpper() == NombreEvaluacion.ToUpper()
                    select new
                    {
                        b.IdEvaluacion
                    }
                ).Select(x => x.IdEvaluacion).FirstOrDefault();
        }

        #endregion

        #region Reportes

        /// <summary>
        /// Obtiene y calcula las notas de fin de ciclo de los Alumnos de TP1 y TP2
        /// convirtiendo dichas notas en un archivo Excel basado en una plantilla
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <returns>XLWorkbook</returns>
        public static XLWorkbook GenerateStudentGradeReport(AbetEntities DataContext, String path, Int32 PeriodoAcademicoActualId, int? idEscuela)
        {
            int submodapaactualid = DataContext.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoActualId).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            var lstAlumnosProyectos = GetAlumnosSeccionId(DataContext, submodapaactualid, idEscuela);

            var lstEvaluacionesProyectos = GetEvaluacionesReporte(DataContext, submodapaactualid, idEscuela).ToList();


            var lstRubricasProyectos = new List<Rubrica>();
            foreach (var item in DataContext.Rubrica)
            {
                if (lstEvaluacionesProyectos.Any(x => x.EvaluacionId == item.IdEvaluacion))
                    lstRubricasProyectos.Add(item);
            }

            var lstRubricasCalificadas = new List<RubricaCalificada>();
            foreach (var item in DataContext.RubricaCalificada)
            {
                if (lstRubricasProyectos.Any(x => x.IdRubrica == item.IdRubrica))
                    lstRubricasCalificadas.Add(item);
            }



            var lstTipoEvaluacion = DataContext.TipoEvaluacion
                .Where(x => x.IdSubModalidadPeriodoAcademico == submodapaactualid && x.IdEscuela == idEscuela);

            var lstEvaluadores = DataContext.Evaluador
                .Where(x => x.TipoEvaluador.IdSubModalidadPeriodoAcademico == submodapaactualid
                && x.TipoEvaluador.TipoEvaluadorMaestra.IdEscuela==idEscuela);

            var lstReporte = new List<StudentGradeReportModel>();

            foreach (var item in lstAlumnosProyectos)
            {
                var lstRubricasCalificadaslumnos = new List<RubricaCalificada>();
                foreach (var rubricaCalificada in lstRubricasCalificadas)
                {
                    if (item.AlumnoSeccionId == rubricaCalificada.IdAlumnoSeccion)
                        lstRubricasCalificadaslumnos.Add(rubricaCalificada);
                }

                if (lstRubricasCalificadaslumnos.Any())
                {
                    var lstEvaluacionId = lstEvaluacionesProyectos
                        .Where(x => x.CodigoCurso == item.CodigoCurso && x.CodigoCarrera == item.CodigoCarrera)
                        .Select(x => x.EvaluacionId).ToList();

                    var lstRubricasAlumnos = new List<Rubrica>();
                    foreach (var rubrica in lstRubricasProyectos)
                    {
                        if (lstEvaluacionId.Any(x => x == rubrica.IdEvaluacion))
                            lstRubricasAlumnos.Add(rubrica);
                    }

                    Decimal notaFinalCurso = 0;
                    Decimal porcentajeAvance = 0;
                    Decimal notaPa = 0;
                    Decimal notaTf = 0;
                    Decimal notaTp = 0;

                    foreach (var rubrica in lstRubricasAlumnos)
                    {
                        Int32 tipoEvaluacionId = rubrica.Evaluacion.IdTipoEvaluacion;
                        String tipoEvaluacion = lstTipoEvaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == tipoEvaluacionId).Tipo.ToUpper();
                        Decimal pesoEvaluacion = rubrica.Evaluacion.Peso;
                        Boolean esNotaEvaluada = rubrica.EsNotaEvaluada;

                        var lstRubricasFinales = lstRubricasCalificadaslumnos
                            .Where(x => x.IdRubrica == rubrica.IdRubrica).ToList();

                        Decimal notaEvaluacion = 0;

                        foreach (var rubricaCalculo in lstRubricasFinales)
                        {
                            Decimal notaRubrica = rubricaCalculo.NotaRubrica.HasValue
                                ? rubricaCalculo.NotaRubrica.Value : 0;
                            Decimal notaRubricaMaxima = rubricaCalculo.NotaMaxima.HasValue
                                ? rubricaCalculo.NotaMaxima.Value : 0;
                            if(notaRubricaMaxima != 0)
                            {
                                if (tipoEvaluacion == ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA)
                                {
                                    notaEvaluacion += (notaRubrica * 20) / notaRubricaMaxima;
                                }
                                else
                                {
                                    var objEvaluador =
                                    lstEvaluadores.FirstOrDefault(x => x.IdEvaluador == rubricaCalculo.IdEvaluador)
                                        .TipoEvaluador;

                                    if (!esNotaEvaluada && objEvaluador.TipoEvaluadorMaestra.Codigo.ToUpper() == ConstantHelpers.RUBRICFILETYPE.EvaluatorType.COMITE)
                                    {
                                        notaEvaluacion += (notaRubrica * 20) / notaRubricaMaxima;
                                    }
                                    else if (esNotaEvaluada)
                                    {
                                        notaEvaluacion += notaRubrica * objEvaluador.Peso;
                                    }
                                }
                            }
                            else
                            {
                                notaEvaluacion = 0;

                            }
                        }

                        if (lstRubricasFinales.Any())
                        {
                            notaFinalCurso += notaEvaluacion * pesoEvaluacion;
                            porcentajeAvance += pesoEvaluacion;
                        }
                        else
                        {
                            notaEvaluacion = -1;
                        }

                        switch (tipoEvaluacion)
                        {
                            case ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA:
                                notaPa = notaEvaluacion;
                                notaPa = Decimal.Round(notaPa, 2);
                                break;
                            //case ConstantHelpers.RUBRICFILETYPE.EvaluationType.TP:
                            //    notaTp = notaEvaluacion;
                            //    break;
                            case ConstantHelpers.RUBRICFILETYPE.EvaluationType.TF:
                                notaTf = notaEvaluacion;
                                notaTf = Decimal.Round(notaTf, 2);
                                break;
                        }
                    }

                    var objReporte = new StudentGradeReportModel
                    {
                        Codigo = item.CodigoAlumno,
                        Alumno = item.NombreAlumno,
                        Carrera = item.NombreCarrera,
                        Curso = item.NombreCurso,
                        SeccionCurso = item.SeccionCurso,
                        SedeAlumno = item.SedeAlumno,
                        NotaFinalCurso = notaFinalCurso,
                        PorcentajeAvance = porcentajeAvance,
                        NotaPa = notaPa == -1 ? ConstantHelpers.RUBRICFILETYPE.MessageResult.NO_EXISTE_NOTA : notaPa.ToString(),
                        NotaTp = notaTp == -1 ? ConstantHelpers.RUBRICFILETYPE.MessageResult.NO_EXISTE_NOTA : notaTp.ToString(),
                        NotaTf = notaTf == -1 ? ConstantHelpers.RUBRICFILETYPE.MessageResult.NO_EXISTE_NOTA : notaTf.ToString()
                    };

                    lstReporte.Add(objReporte);
                }
            }
            var objExcel = new XLWorkbook(path);
            var worksheet = objExcel.Worksheets.Worksheet(1);
            Int32 indice = 2;

            foreach (var item in lstReporte)
            {
                worksheet.Cell("A" + indice).Value = item.Codigo;
                worksheet.Cell("B" + indice).Value = item.Alumno;
                worksheet.Cell("C" + indice).Value = item.Carrera;
                worksheet.Cell("D" + indice).Value = item.SedeAlumno;
                worksheet.Cell("E" + indice).Value = item.Curso;
                worksheet.Cell("F" + indice).Value = item.SeccionCurso;
                worksheet.Cell("G" + indice).Value = item.NotaPa;
                //worksheet.Cell("H" + indice).Value = item.NotaTp;
                worksheet.Cell("H" + indice).Value = item.NotaTf;
                //worksheet.Cell("I" + indice).Value = item.NotaFinalCurso;
                //worksheet.Cell("I" + indice).Value = item.PorcentajeAvance;
                indice++;
            }

            return objExcel;
        }

        /// <summary>
        /// Obtiene los outcomes calificados de los alumnos de un determinado proyecto
        /// para un tipo de evaluacion de rubrica
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <param name="ProyectoAcademicoId">ID del Proyecto Académico</param>
        /// <param name="TipoEvaluacionId">ID del Tipo de Evaluación</param>
        /// <returns>XLWorkbook</returns>
        public static XLWorkbook GenerateOutcomeEvaluatorReport(AbetEntities DataContext, string path, Int32 PeriodoAcademicoActualId,
                                                                    Int32 ProyectoAcademicoId, Int32 TipoEvaluacionId, ref string nombreReporte)
        {
            var objExcel = new XLWorkbook(path);
            int hoja = 1;

            var alumnos = (from a in DataContext.AlumnoSeccion
                           join b in DataContext.AlumnoMatriculado on a.IdAlumnoMatriculado equals b.IdAlumnoMatriculado
                           join c in DataContext.Seccion on a.IdSeccion equals c.IdSeccion
                           join d in DataContext.CursoPeriodoAcademico on c.IdCursoPeriodoAcademico equals d.IdCursoPeriodoAcademico
                           join e in DataContext.Alumno on b.IdAlumno equals e.IdAlumno
                           join f in DataContext.CarreraCursoPeriodoAcademico on d.IdCursoPeriodoAcademico equals f.IdCursoPeriodoAcademico
                           join g in DataContext.Carrera on f.IdCarrera equals g.IdCarrera
                           join h in DataContext.Sede on b.IdSede equals h.IdSede
                           join i in DataContext.Curso on d.IdCurso equals i.IdCurso
                           join j in DataContext.ProyectoAcademico on a.IdProyectoAcademico equals j.IdProyectoAcademico
                           where
                               a.IdProyectoAcademico == ProyectoAcademicoId &&
                               b.IdSubModalidadPeriodoAcademico == PeriodoAcademicoActualId &&
                               b.IdCarrera == f.IdCarrera
                           select new StudentOutcomeReportModel()
                           {
                               AlumnoId = b.IdAlumno,
                               AlumnoSeccionId = a.IdAlumnoSeccion,
                               CarreraId = b.IdCarrera.Value,
                               CursoId = d.IdCurso,
                               CarreraCursoPeriodoAcademicoId = f.IdCarreraCursoPeriodoAcademico,
                               NombreAlumno = e.Apellidos + " " + e.Nombres,
                               CodigoAlumno = e.Codigo,
                               Carrera = g.NombreEspanol,
                               Sede = h.Nombre,
                               Curso = i.Codigo + " - " + i.NombreEspanol,
                               Seccion = c.Codigo,
                               CodigoProyecto = j.Codigo
                           }).ToList();

            nombreReporte = "Reporte_Notas_Evaluador_" + alumnos.First().CodigoProyecto + ".xlsx";
            var lstRubricas = DataContext.Rubrica;
            var lstEvaluacion = DataContext.Evaluacion;

            foreach (var item in alumnos)
            {
                var evaluacionId = lstEvaluacion.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == item.CarreraCursoPeriodoAcademicoId
                                                            && x.IdTipoEvaluacion == TipoEvaluacionId).IdEvaluacion;

                var rubrica = lstRubricas.FirstOrDefault(x => x.IdEvaluacion == evaluacionId);

                var worksheet = objExcel.Worksheet(hoja);
                worksheet.Name = item.CodigoAlumno + "_" + item.Seccion;

                worksheet.Cell("B1").Value = item.CodigoAlumno;
                worksheet.Cell("B2").Value = item.NombreAlumno;
                worksheet.Cell("B3").Value = item.Carrera;
                worksheet.Cell("B4").Value = item.Sede;
                worksheet.Cell("B5").Value = item.Curso;
                worksheet.Cell("B6").Value = item.Seccion;

                var lstResultado = GetOutcomeEvaluatorList(DataContext, item.AlumnoSeccionId, rubrica.IdRubrica);
                var listaObservaciones = DataContext.RubricaCalificada.Where(x => x.IdAlumnoSeccion == item.AlumnoSeccionId).Select(x => x.Observacion).ToList();

                int indice = 10;
                var observaciones = string.Empty;

                foreach (var observacion in listaObservaciones)
                {
                    observaciones += observacion.ToUpper() + ". ";
                }
                worksheet.Cell("B" + 7).Value = observaciones;

                foreach (var objReporte in lstResultado)
                {
                    worksheet.Cell("A" + indice).Value = objReporte.Outcome;

                    worksheet.Cell("B" + indice).Value = objReporte.NotaOutcome.Value.ToString();

                    worksheet.Cell("C" + indice).Value = objReporte.Comision;
                    worksheet.Cell("D" + indice).Value = objReporte.RolEvaluador;
                    worksheet.Cell("E" + indice).Value = objReporte.CodigoEvaluador;
                    worksheet.Cell("F" + indice).Value = objReporte.NombreEvaluador;
                    worksheet.Cell("G" + indice).Value = objReporte.DescripcionOutcome;
                    indice++;
                }

                hoja++;
            }

            if (alumnos.Count == 1)
            {
                objExcel.Worksheet(2).Delete();
            }

            return objExcel;
        }

        /// <summary>
        /// Obtiene los outcomes calificados de un determinado alumno
        /// para un tipo de evaluacion de rubrica
        /// </summary>
        /// <param name="DataContext">Contexto de la Aplicación</param>
        /// <param name="PeriodoAcademicoActualId">ID del Periodo Académico Activo</param>
        /// <param name="AlumnoSeccionId">ID del Alumno Seccion</param>
        /// <param name="NombreEvaluacion">Nombre de la Evaluación</param>
        /// <returns>XLWorkbook</returns>
        public static XLWorkbook GenerateStudentOutcomeReport(AbetEntities DataContext, String path, Int32 PeriodoAcademicoActualId, Int32 AlumnoSeccionId, String NombreEvaluacion, ref String fileName,int? idEscuela)
        {
            var lstAlumnosProyectos = GetAlumnosSeccionId(DataContext, PeriodoAcademicoActualId,idEscuela);
            var objAlumno = lstAlumnosProyectos.FirstOrDefault(x => x.AlumnoSeccionId == AlumnoSeccionId);

            var lstOutcomes = (from a in DataContext.NotaVerificacionAlumno
                               join b in DataContext.OutcomeRubrica on a.IdOutcomeRubrica equals b.IdOutcomeRubrica
                               join c in DataContext.OutcomeComision on b.IdOutcomeComision equals c.IdOutcomeComision
                               join d in DataContext.Outcome on c.IdOutcome equals d.IdOutcome
                               join e in DataContext.Comision on c.IdComision equals e.IdComision
                               where
                                   a.IdAlumnoSeccion == AlumnoSeccionId &&
                                   a.NombreEvaluacion == NombreEvaluacion
                               select new OutcomeReportModel()
                               {
                                   Outcome = d.Nombre,
                                   NotaAlumno = a.NotaOutcome,
                                   NotaMaximaOutcome = b.NotaOutcome.Value,
                                   NivelAlcanzado = a.NivelAlcanzado,
                                   Comision = e.Codigo,
                                   Descripcion = d.DescripcionEspanol
                               }
                ).ToList();

            var objExcel = new XLWorkbook(path);

            var worksheet = objExcel.Worksheets.Worksheet(1);

            worksheet.Cell("B1").Value = objAlumno.CodigoAlumno;
            worksheet.Cell("B2").Value = objAlumno.NombreAlumno;
            worksheet.Cell("B3").Value = objAlumno.NombreCarrera;
            worksheet.Cell("B4").Value = objAlumno.SedeAlumno;
            worksheet.Cell("B5").Value = objAlumno.NombreCurso;
            worksheet.Cell("B6").Value = objAlumno.SeccionCurso;
            worksheet.Cell("B7").Value = NombreEvaluacion;

            Int32 indice = 10;

            foreach (var item in lstOutcomes)
            {
                worksheet.Cell("A" + indice).Value = item.Comision;
                worksheet.Cell("B" + indice).Value = item.Outcome;
                worksheet.Cell("C" + indice).Value = item.NotaAlumno;
                worksheet.Cell("D" + indice).Value = item.NotaMaximaOutcome;
                worksheet.Cell("E" + indice).Value = item.NivelAlcanzado;
                worksheet.Cell("F" + indice).Value = item.Descripcion;
                indice++;
            }

            fileName = objAlumno.CodigoCurso + "_" + objAlumno.CodigoAlumno + ".xlsx";
            return objExcel;
        }

        #region EXCEL EVALUACION 2017-2
        public static XLWorkbook GeneraExcelEvaluacion(CargarDatosContext ctx, Int32 IdProyectoAcademico, Int32 IdAlumnoSeccion, Int32 IdRubrica, Int32 IdEvaluador, Int32 IdTipoEvaluacion, String Path, ref String nombreReporte, Int32 idsub, Int32 idcurso)
        {
            // var periodoAcademicoAnioX = ctx.context.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).Select(y => y.FechaInicioPeriodo).FirstOrDefault().Value.Year;
            var periodoAcademicoAnio = ctx.context.ProyectoAcademico.Where(x => x.IdProyectoAcademico == IdProyectoAcademico)
                .Select(y => y.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico).Select(z => z.FechaInicioPeriodo).FirstOrDefault().Value.Year;


            string Sistemas = (from a in ctx.context.Carrera
                               where a.IdCarrera == 2
                               select a.NombreEspanol).FirstOrDefault();

            var context = ctx.context;
            var Proyecto = context.ProyectoAcademico.FirstOrDefault(x => x.IdProyectoAcademico == IdProyectoAcademico);
            var Alumno = Proyecto.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == IdAlumnoSeccion);
            var Rubrica = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            var Evaluador = context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
            var EvaluacionTipo = context.TipoEvaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion).Tipo;
            var EvaluacionTipoid = context.TipoEvaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion).IdTipoEvaluacion;
            var NombreEvaluador = Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo;
            var RubricaCalificada = context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == IdRubrica && x.IdAlumnoSeccion == IdAlumnoSeccion
                                                                                && x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo.Equals(NombreEvaluador));

            String[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Int32 CeldaInicial = 10;
            var EvaluacionExcel = new XLWorkbook();
            var Excel = new XLWorkbook(Path);

            nombreReporte = Proyecto.Curso.Codigo + "_" + Alumno.AlumnoMatriculado.Alumno.Codigo + "_" + Proyecto.Codigo;
            var nombreHoja = Proyecto.Codigo;
            var worksheet = Excel.Worksheet(1);
            var worksheetCCA = Excel.Worksheet(2);

            if (Alumno.AlumnoMatriculado.Carrera.NombreEspanol == Sistemas && idsub >= 8)
            {
                worksheetCCA.Name = nombreHoja + "_CAC";
                worksheet.Name = nombreHoja + "_EAC";
            }
            else
            {
                worksheet.Name = nombreHoja;
                worksheetCCA.Delete();
            }

            XLColor color = worksheet.Cell("D7").Style.Fill.BackgroundColor;
            worksheet.Cell("B1").Value = Proyecto.Descripcion;
            worksheet.Cell("B2").Value = Alumno.AlumnoMatriculado.Alumno.Codigo;
            worksheet.Cell("B3").Value = Alumno.AlumnoMatriculado.Alumno.Apellidos + ", " + Alumno.AlumnoMatriculado.Alumno.Nombres;
            worksheet.Cell("B4").Value = Alumno.AlumnoMatriculado.Carrera.NombreEspanol;

            Int32 indice = CeldaInicial;
            Int32 indiceAuxiliar = CeldaInicial;

            worksheet.Cell("B5").Value = Decimal.Round(RubricaCalificada.NotaRubrica.Value * 20 / Rubrica.NotaMaxima.Value, 2);
            //OBSERVACIONES
            var EvaluadoresObservaciones = context.RubricaCalificada.Where(x => x.IdAlumnoSeccion == IdAlumnoSeccion && x.IdRubrica == Rubrica.IdRubrica && x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador != Evaluador.TipoEvaluador.TipoEvaluadorMaestra.idTipoEvaluador)
                                                    .Select(x => new {
                                                        EvaluadorCodigo = x.Evaluador.Docente.Nombres + " " + x.Evaluador.Docente.Apellidos + "(" + x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Nombre + ")",
                                                        Observacion = x.Observacion
                                                    }).AsEnumerable()
                                                        .Select(x => new Tuple<String, String>(x.EvaluadorCodigo, x.Observacion)).ToList();
            String Observaciones = Evaluador.Docente.Nombres + " " + Evaluador.Docente.Apellidos + " (" + Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Nombre + "): " + RubricaCalificada.Observacion;
            foreach (var Observacion in EvaluadoresObservaciones)
                Observaciones += ("\r\n" + Observacion.Item1 + ": " + Observacion.Item2);
            //-------------
            worksheet.Cell("B6").Value = Observaciones;

            var NivelesDesempenio = Rubrica.RubricaNivelDesempenio.Select(x => x.NivelDesempenio);
            var OutcomesRubrica = Rubrica.OutcomeRubrica.OrderByDescending(x => x.OutcomeComision.Comision.Codigo).OrderBy(x=>x.OutcomeComision.Outcome.Nombre).ThenBy(x => x.OutcomeComision.Outcome.Nombre);

            //int indiceOutcome = 10;
            double? SumCriteriNota = 0;
            int SumNotaMaxima = 0;
            if (Alumno.AlumnoMatriculado.Carrera.NombreEspanol == Sistemas && idsub>=8)
            {
                var tableEAC = context.sp_obttablaEAC(Alumno.AlumnoMatriculado.Alumno.Codigo, EvaluacionTipoid, idsub).OrderBy(x => x.NombreOutcome).ToList();
                //var tableCAC = context.sp_obttablaCAC(Alumno.AlumnoMatriculado.Alumno.Codigo, EvaluacionTipoid, idsub).OrderBy(x => x.idOutcome);

                int count = 0;
                int? IdOutComeAuxiliar = 0;
                int contadorFinaleac = 0;
                int contadorFinalcac = 0;
                foreach (var t in tableEAC)
                {

                    int? IdOutcome = t.idOutcome;
                    string NombreOutcome = t.NombreOutcome;


                    if (IdOutComeAuxiliar != IdOutcome)
                    {
                        count = 0;
                        IdOutComeAuxiliar = IdOutcome;
                        if (indice != CeldaInicial)
                        {
                            worksheet.Range("A" + indiceAuxiliar, "A" + (indice - 1)).Merge();
                            worksheet.Cell("C" + (indice)).Value = SumNotaMaxima;
                            worksheet.Cell("D" + (indice)).Value = SumCriteriNota;
                            indice = indice + 1;
                            indiceAuxiliar = indice;
                            SumCriteriNota = 0;
                            SumNotaMaxima = 0;
                        }
                    }

                    if (count == 0)
                    {
                        worksheet.Cell("A" + indice).Value = NombreOutcome;
                        worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(XLColor.Green);
                    }

                    string CriterioOutcome = t.criterio;
                    string NotaMaxima = t.notaMaxCriterio;
                    SumNotaMaxima += Int32.Parse(NotaMaxima);
                    double? criteriNota = Math.Round((double)t.diferencialAppp, 3);
                    SumCriteriNota += criteriNota;
                    string incipiente = t.Incipiente;
                    string proceso = t.proceso;
                    string Esperado = t.Esperado;

                    worksheet.Cell("B" + indice).Value = CriterioOutcome;
                    worksheet.Cell("C" + indice).Value = NotaMaxima;
                    worksheet.Cell("D" + indice).Value = criteriNota;
                    worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);
                    worksheet.Cell("E" + indice).Value = incipiente;
                    worksheet.Cell("F" + indice).Value = proceso;
                    worksheet.Cell("G" + indice).Value = Esperado;


                    contadorFinaleac++;

                    indice = indice + 1;
                    count++;

                    if (tableEAC.Count == contadorFinaleac)
                    {
                        worksheet.Range("A" + indiceAuxiliar, "A" + (indice - 1)).Merge();
                    }
                }
                worksheet.Cell("C" + (indice)).Value = SumNotaMaxima;
                worksheet.Cell("D" + (indice)).Value = SumCriteriNota;
                worksheet.Range("A" + CeldaInicial, "G" + (indice)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range("A" + CeldaInicial, "G" + (indice)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                if (periodoAcademicoAnio < 2019)  // A partir del 2019 sistemas solo utiliza la comision EAC
                {
                    var tableCAC = context.sp_obttablaCAC(Alumno.AlumnoMatriculado.Alumno.Codigo, EvaluacionTipoid, idsub, idcurso).OrderBy(x => x.idOutcome).ToList();

                    //CAC
                    SumCriteriNota = 0;
                    SumNotaMaxima = 0;
                    Int32 indiceCAC = CeldaInicial;
                    indiceAuxiliar = CeldaInicial;
                    count = 0;
                    IdOutComeAuxiliar = 0;

                    worksheetCCA.Cell("D7").Style.Fill.BackgroundColor = color;
                    worksheetCCA.Cell("B1").Value = worksheet.Cell("B1").Value;
                    worksheetCCA.Cell("B2").Value = worksheet.Cell("B2").Value;
                    worksheetCCA.Cell("B3").Value = worksheet.Cell("B3").Value;
                    worksheetCCA.Cell("B4").Value = worksheet.Cell("B4").Value;
                    worksheetCCA.Cell("B5").Value = worksheet.Cell("B5").Value;
                    worksheetCCA.Cell("B6").Value = worksheet.Cell("B6").Value;

                    foreach (var t in tableCAC)
                    {

                        int? IdOutcome = t.idOutcome;
                        string NombreOutcome = t.OutDescrip;

                        if (IdOutComeAuxiliar != IdOutcome)
                        {
                            count = 0;
                            IdOutComeAuxiliar = IdOutcome;
                            if (indiceCAC != CeldaInicial)
                            {
                                worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                                worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                                worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;
                                indiceCAC = indiceCAC + 1;
                                indiceAuxiliar = indiceCAC;
                                SumCriteriNota = 0;
                                SumNotaMaxima = 0;
                            }
                        }

                        if (count == 0)
                        {
                            worksheetCCA.Cell("A" + indiceCAC).Value = NombreOutcome;
                            worksheetCCA.Cell("A" + indiceCAC).Style.Fill.SetBackgroundColor(XLColor.Green);
                        }


                        string CriterioOutcome = t.criterio;
                        string NotaMaxima = t.notaMaxCriterio;
                        SumNotaMaxima += Int32.Parse(NotaMaxima);
                        double? criteriNota = Math.Round((double)t.NotaCriterio, 3);
                        SumCriteriNota += criteriNota;
                        string incipiente = t.Incipiente;
                        string proceso = t.proceso;
                        string Esperado = t.Esperado;

                        worksheetCCA.Cell("B" + indiceCAC).Value = CriterioOutcome;
                        worksheetCCA.Cell("C" + indiceCAC).Value = NotaMaxima;
                        worksheetCCA.Cell("D" + indiceCAC).Value = criteriNota;
                        worksheetCCA.Cell("D" + indiceCAC).Style.Fill.SetBackgroundColor(color);
                        worksheetCCA.Cell("E" + indiceCAC).Value = incipiente;
                        worksheetCCA.Cell("F" + indiceCAC).Value = proceso;
                        worksheetCCA.Cell("G" + indiceCAC).Value = Esperado;

                        indiceCAC = indiceCAC + 1;
                        count++;
                        contadorFinalcac++;

                        if (tableCAC.Count == contadorFinalcac)
                        {
                            worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                        }

                    }
                    worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                    worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;

                    worksheetCCA.Range("A" + CeldaInicial, "G" + (indiceCAC)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                    worksheetCCA.Range("A" + CeldaInicial, "G" + (indiceCAC)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                }
                else
                {
                    Excel.Worksheet(2).Delete();     // Para borrar la segunda hoja que no se usa
                }

            }
            else
            {
                foreach (var Outcome in OutcomesRubrica)
                {
                    var OutcomeCalificado = RubricaCalificada.OutcomeCalificado.FirstOrDefault(x => x.IdOutcome == Outcome.IdOutcomeRubrica);
                    worksheet.Cell("A" + indice).Value = "Student Outcome " + Outcome.OutcomeComision.Comision.Codigo + " (" + Outcome.OutcomeComision.Outcome.Nombre + "): " + Outcome.OutcomeComision.Outcome.DescripcionEspanol;
                    worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(XLColor.Green);

                    foreach (var Criterio in Outcome.CriterioOutcome)
                    {
                        worksheet.Cell("B" + indice).Value = Criterio.Descripcion;
                        var CriterioCalificado = OutcomeCalificado.CriterioCalificado.FirstOrDefault(x => x.IdCriterio == Criterio.IdCriterioOutcome);
                        worksheet.Cell("C" + indice).Value = Criterio.ValorMaximo;
                        worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);
                        worksheet.Cell("D" + indice).Value = CriterioCalificado.NotaCriterio;

                        Int32 ContLetra = 5;
                        foreach (var Nivel in NivelesDesempenio)
                        {
                            if (Nivel.PuntajeMayor == CriterioCalificado.NotaCriterio)
                                worksheet.Cell(alphabetArray[ContLetra] + indice).Value = "X";
                            ContLetra++;
                        }
                        indice++;
                    }

                    if (indice == indiceAuxiliar)
                        indice++;

                    indice++;
                    worksheet.Range("A" + indiceAuxiliar, "A" + (indice - 2)).Merge();
                    worksheet.Cell("C" + (indice - 1)).Value = Outcome.NotaOutcome;
                    worksheet.Cell("D" + (indice - 1)).Value = OutcomeCalificado.NotaOutcome;
                    indiceAuxiliar = indice;
                }

                worksheet.Range("A" + CeldaInicial, "G" + (indice - 1)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range("A" + CeldaInicial, "G" + (indice - 1)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            }

            nombreReporte = nombreReporte + ".xlsx";
            EvaluacionExcel = Excel;
            return EvaluacionExcel;
        }

        public static XLWorkbook GeneraExcelEvaluacionWASC(CargarDatosContext ctx, Int32 IdProyectoAcademico, Int32 IdAlumnoSeccion, Int32 IdRubrica, Int32 IdEvaluador, Int32 IdTipoEvaluacion, String Path, ref String nombreReporte)
        {
            var context = ctx.context;
            var Proyecto = context.ProyectoAcademico.FirstOrDefault(x => x.IdProyectoAcademico == IdProyectoAcademico);
            var Alumno = Proyecto.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == IdAlumnoSeccion);
            var Rubrica = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            var Evaluador = context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
            var EvaluacionTipo = context.TipoEvaluacion.FirstOrDefault(x => x.IdTipoEvaluacion == IdTipoEvaluacion).Tipo;
            var NombreEvaluador = Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo;
            var RubricaCalificada = context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == IdRubrica && x.IdAlumnoSeccion == IdAlumnoSeccion
                                                                                && x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo.Equals(NombreEvaluador));

            String[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Int32 CeldaInicial = 6;
            var EvaluacionExcel = new XLWorkbook();
            var Excel = new XLWorkbook(Path);

            var worksheet = Excel.Worksheet(1);
            var codigoProyectoResumido = 'P' + Proyecto.Codigo.Remove(0, 9);
            nombreReporte = Proyecto.Curso.Codigo + "_" + Alumno.AlumnoMatriculado.Alumno.Codigo + "_" + codigoProyectoResumido;
            worksheet.Name = nombreReporte;

            XLColor color = worksheet.Cell("A5").Style.Fill.BackgroundColor;
            worksheet.Cell("B1").Value = Alumno.AlumnoMatriculado.Alumno.Apellidos + ", " + Alumno.AlumnoMatriculado.Alumno.Nombres;
            worksheet.Cell("B2").Value = Decimal.Round(RubricaCalificada.NotaRubrica.Value * 20 / Rubrica.NotaMaxima.Value, 2);

            Int32 indice = CeldaInicial;

            var OutcomesRubrica = Rubrica.OutcomeRubrica;
            foreach (var Outcome in OutcomesRubrica)
            {
                var OutcomeCalificado = RubricaCalificada.OutcomeCalificado.FirstOrDefault(x => x.IdOutcome == Outcome.IdOutcomeRubrica);
                worksheet.Cell("A" + indice).Value = Outcome.OutcomeComision.Outcome.Nombre;
                worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(color);
                worksheet.Range("A" + indice, "A" + (indice + 1)).Merge();

                foreach (var Criterio in Outcome.CriterioOutcome)
                {
                    worksheet.Cell("B" + indice).Value = Criterio.Descripcion;
                    worksheet.Cell("B" + indice).Style.Fill.SetBackgroundColor(XLColor.AliceBlue);
                    worksheet.Range("B" + indice, "B" + (indice + 1)).Merge();
                    var CriterioCalificado = OutcomeCalificado.CriterioCalificado.FirstOrDefault(x => x.IdCriterio == Criterio.IdCriterioOutcome);

                    Int32 ContLetra = 3;
                    foreach (var Nivel in Criterio.NivelCriterio)
                    {
                        worksheet.Cell(alphabetArray[ContLetra] + indice).Value = Nivel.DescripcionNivel;
                        worksheet.Cell(alphabetArray[ContLetra] + indice).Style.Fill.SetBackgroundColor(XLColor.FromHtml(Nivel.Color));
                        worksheet.Cell(alphabetArray[ContLetra] + (indice + 1)).Value = (Nivel.NotaMenor == Nivel.NotaMayor) ? "[" + Nivel.NotaMayor + "]" : "[" + Nivel.NotaMenor + "-" + Nivel.NotaMayor + "]";
                        worksheet.Cell(alphabetArray[ContLetra] + (indice + 1)).Style.Font.Bold = true;
                        worksheet.Cell(alphabetArray[ContLetra] + (indice + 1)).Style.Fill.SetBackgroundColor(XLColor.FromHtml(Nivel.Color));
                        ContLetra++;
                    }
                    worksheet.Cell(alphabetArray[ContLetra] + indice).Value = CriterioCalificado.NotaCriterio;
                    worksheet.Range(alphabetArray[ContLetra] + indice, alphabetArray[ContLetra] + (indice + 1)).Merge();
                    indice++;
                }
                indice++;
            }
            worksheet.Range("A" + CeldaInicial, "F" + (indice - 1)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet.Range("A" + CeldaInicial, "F" + (indice - 1)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            nombreReporte = nombreReporte + ".xlsx";
            EvaluacionExcel = Excel;
            return EvaluacionExcel;
        }
        #endregion

        public static List<XLWorkbook> GenerateListCriterioReport(AbetEntities DataContext, Int32 IdCarrera, Int32 PeriodoAcademicoId, String NombreEvaluacion, String CodCurso, String path, int? idEscuela, CargarDatosContext ctx)
        {
            int contador = 0;
            try
            {
                var periodoAcademicoAnio = DataContext.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == PeriodoAcademicoId).Select(y => y.FechaInicioPeriodo).FirstOrDefault().Value.Year;

                var context = ctx.context;
                string[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                int celdaInicial = 10;
                var alumnos = GetAlumnosSeccionId(DataContext, IdCarrera, PeriodoAcademicoId, idEscuela);

                alumnos.RemoveAll(x => x.CodigoCurso != CodCurso);

                var lstRubricas = DataContext.Rubrica;
                var lstEvaluacion = DataContext.Evaluacion;
                var lstReporte = new List<XLWorkbook>();
                string Sistemas = (from a in context.Carrera
                                   where a.IdCarrera == 2
                                   select a.NombreEspanol).FirstOrDefault();

                foreach (var alumnoId in alumnos.Select(x => x.AlumnoSeccionId))
                {
                    var alumno = GetAlumnoCriterio(DataContext, PeriodoAcademicoId, alumnoId);

                    if (alumno != null)
                    {
                        var evaluacionId = GetEvaluacionId(DataContext, PeriodoAcademicoId,
                                               alumno.CarreraCursoPeriodoAcademicoId, NombreEvaluacion);

                        var rubrica = DataContext.Rubrica.FirstOrDefault(x => x.IdEvaluacion == evaluacionId);




                        if (rubrica != null)
                        {
                            var rubricacalificada = DataContext.RubricaCalificada.FirstOrDefault(x => x.IdAlumnoSeccion == alumnoId && x.NotaRubrica != null
                            && x.Rubrica.Evaluacion.TipoEvaluacion.Tipo == "TF");
                            bool RubricaGenerada = false;
                            if (rubricacalificada != null)
                            {
                                if (rubricacalificada.RubricaGenerada != null)
                                {
                                    RubricaGenerada = rubricacalificada.RubricaGenerada.Value;
                                }
                                else
                                {
                                    RubricaGenerada = false;
                                }
                                
                            }
                            var lstOutcomes = new List<StudentCriterioOutcomeReportModel>();

                            if (!rubrica.Evaluacion.TipoEvaluacion.Tipo.Equals(ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA))
                                lstOutcomes = GetOutcomeCriterioList(DataContext, alumno.AlumnoSeccionId, rubrica.IdRubrica);
                            else
                                lstOutcomes = GetOutcomeCriterioListPA(DataContext, alumno.AlumnoSeccionId, rubrica.IdRubrica);

                            if (lstOutcomes.Any())
                            {
                                var lstCriterios = GetOutcomeCriterioDetailList(DataContext, lstOutcomes);

                                var objExcel = new XLWorkbook(path);
                                var worksheet = objExcel.Worksheet(1);

                                String NombreReporte = alumno.CodigoProyecto + "_" + alumno.CodigoAlumno;
                                string nombreHoja = alumno.CodigoAlumno;
                                worksheet.Name = nombreHoja;

                                worksheet.Column(1).AdjustToContents();


                                XLColor color = worksheet.Cell("D7").Style.Fill.BackgroundColor;
                                worksheet.Cell("B1").Value = alumno.Curso;
                                worksheet.Cell("B2").Value = alumno.CodigoAlumno;
                                worksheet.Cell("B3").Value = alumno.Sede + " " + alumno.NombreAlumno;
                                worksheet.Cell("B4").Value = alumno.Carrera;

                                Int32 indice = celdaInicial;
                                Int32 indiceAuxiliar = celdaInicial;

                                var studentCriterioOutcomeReportModel = lstOutcomes.FirstOrDefault();

                                if (studentCriterioOutcomeReportModel != null)
                                {
                                    worksheet.Cell("B5").Value = Decimal.Round((studentCriterioOutcomeReportModel.NotaRubricaCalificada * 20 / rubrica.NotaMaxima.Value), 2);

                                    //OBSERVACIONES
                                    var EvaluadoresObservaciones = DataContext.RubricaCalificada.Where(x => x.IdAlumnoSeccion == alumno.AlumnoSeccionId && x.IdRubrica == rubrica.IdRubrica)
                                                                            .Select(x => new
                                                                            {
                                                                                EvaluadorCodigo = x.Evaluador.Docente.Nombres + " " + x.Evaluador.Docente.Apellidos + "(" + x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Nombre + ")",
                                                                                Observacion = x.Observacion == null ? "" : x.Observacion
                                                                            }).AsEnumerable()
                                                                                .Select(x => new Tuple<String, String>(x.EvaluadorCodigo, x.Observacion)).ToList();
                                    String Observaciones = "";
                                    foreach (var Observacion in EvaluadoresObservaciones)
                                        Observaciones += ("\r\n" + Observacion.Item1 + ": " + Observacion.Item2);

                                    //-------------
                                    //worksheet.Cell("B6").Value = studentCriterioOutcomeReportModel.ObservacionRubrica;
                                    worksheet.Cell("B6").Value = Observaciones;
                                }
                                else
                                {
                                    worksheet.Cell("B5").Value = "NINGUNA";
                                }

                                if (alumno.Carrera == Sistemas && PeriodoAcademicoId >= 14)
                                {
                                    worksheet.Name = nombreHoja + "_EAC";
                                    double? SumCriteriNota = 0;
                                    int SumNotaMaxima = 0;

                                    int idsub = (from a in DataContext.SubModalidadPeriodoAcademico
                                                 where a.IdPeriodoAcademico == PeriodoAcademicoId
                                                 select a.IdSubModalidadPeriodoAcademico).FirstOrDefault();

                                    int idcurso = (from a in DataContext.Curso
                                                   where a.Codigo == CodCurso
                                                   select a.IdCurso).FirstOrDefault();

                                    int EvaluacionTipoid = (from a in DataContext.TipoEvaluacion
                                                            where a.Tipo == NombreEvaluacion && a.IdSubModalidadPeriodoAcademico == idsub
                                                            select a.IdTipoEvaluacion).FirstOrDefault();




                                    var tableEAC = DataContext.sp_obttablaEAC(alumno.CodigoAlumno, EvaluacionTipoid, idsub).OrderBy(x => x.NombreOutcome).ToList();
                                    //var tableCAC = context.sp_obttablaCAC(Alumno.AlumnoMatriculado.Alumno.Codigo, EvaluacionTipoid, idsub).OrderBy(x => x.idOutcome);
                                    //tableEAC.RemoveRange(14, 5);

                                    int count = 0;
                                    int? IdOutComeAuxiliar = 0;
                                    int contadorFinaleac = 0;
                                    int contadorFinalcac = 0;
                                    foreach (var t in tableEAC)
                                    {

                                        int? IdOutcome = t.idOutcome;
                                        string NombreOutcome = t.NombreOutcome;

                                        if (IdOutComeAuxiliar != IdOutcome)
                                        {
                                            count = 0;
                                            IdOutComeAuxiliar = IdOutcome;
                                            if (indice != celdaInicial)
                                            {
                                                worksheet.Range("A" + indiceAuxiliar, "A" + (indice - 1)).Merge();
                                                worksheet.Cell("C" + (indice)).Value = SumNotaMaxima;
                                                worksheet.Cell("D" + (indice)).Value = SumCriteriNota;
                                                indice = indice + 1;
                                                indiceAuxiliar = indice;
                                                SumCriteriNota = 0;
                                                SumNotaMaxima = 0;
                                            }
                                        }

                                        if (count == 0)
                                        {
                                            worksheet.Cell("A" + indice).Value = NombreOutcome;
                                            worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(XLColor.Green);
                                        }

                                        string CriterioOutcome = t.criterio;
                                        string NotaMaxima = t.notaMaxCriterio;
                                        SumNotaMaxima += Int32.Parse(NotaMaxima);
                                        double? criteriNota = Math.Round((double)t.diferencialAppp, 3);
                                        SumCriteriNota += criteriNota;
                                        string incipiente = t.Incipiente;
                                        string proceso = t.proceso;
                                        string Esperado = t.Esperado;

                                        worksheet.Cell("B" + indice).Value = CriterioOutcome;
                                        worksheet.Cell("C" + indice).Value = NotaMaxima;
                                        worksheet.Cell("D" + indice).Value = criteriNota;
                                        worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);

                                        if (RubricaGenerada)
                                        {
                                            if (criteriNota >= 0 && criteriNota < 1.3)
                                            {
                                                worksheet.Cell("E" + indice).Value = "X";
                                                worksheet.Cell("F" + indice).Value = "";
                                                worksheet.Cell("G" + indice).Value = "";
                                            }
                                            else if (criteriNota >= 1.3 && criteriNota < 1.6)
                                            {
                                                worksheet.Cell("E" + indice).Value = "";
                                                worksheet.Cell("F" + indice).Value = "X";
                                                worksheet.Cell("G" + indice).Value = "";
                                            }
                                            else if (criteriNota >= 1.6 && criteriNota <= 2)
                                            {
                                                worksheet.Cell("E" + indice).Value = "";
                                                worksheet.Cell("F" + indice).Value = "";
                                                worksheet.Cell("G" + indice).Value = "X";
                                            }
                                        }
                                        else
                                        {
                                            worksheet.Cell("E" + indice).Value = incipiente;
                                            worksheet.Cell("F" + indice).Value = proceso;
                                            worksheet.Cell("G" + indice).Value = Esperado;
                                        }



                                        indice = indice + 1;
                                        count++;
                                        contadorFinaleac++;

                                        if (tableEAC.Count == contadorFinaleac)
                                        {
                                            worksheet.Range("A" + indiceAuxiliar, "A" + (indice - 1)).Merge();
                                        }

                                    }
                                    worksheet.Cell("C" + (indice)).Value = SumNotaMaxima;
                                    worksheet.Cell("D" + (indice)).Value = SumCriteriNota;
                                    worksheet.Range("A" + celdaInicial, "G" + (indice)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                                    worksheet.Range("A" + celdaInicial, "G" + (indice)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                                    //CAC

                                    if (periodoAcademicoAnio < 2019)  // A partir del 2019 sistemas solo utiliza la comision EAC
                                    {
                                        var worksheetCCA = objExcel.Worksheet(2);
                                        var tableCAC = DataContext.sp_obttablaCAC(alumno.CodigoAlumno, EvaluacionTipoid, idsub, idcurso).OrderBy(x => x.idOutcome).ToList();

                                        worksheetCCA.Name = nombreHoja + "_CAC";

                                        SumCriteriNota = 0;
                                        SumNotaMaxima = 0;
                                        Int32 indiceCAC = celdaInicial;
                                        indiceAuxiliar = celdaInicial;
                                        count = 0;
                                        IdOutComeAuxiliar = 0;

                                        worksheetCCA.Cell("D7").Style.Fill.BackgroundColor = color;
                                        worksheetCCA.Cell("B1").Value = worksheet.Cell("B1").Value;
                                        worksheetCCA.Cell("B2").Value = worksheet.Cell("B2").Value;
                                        worksheetCCA.Cell("B3").Value = worksheet.Cell("B3").Value;
                                        worksheetCCA.Cell("B4").Value = worksheet.Cell("B4").Value;
                                        worksheetCCA.Cell("B5").Value = worksheet.Cell("B5").Value;
                                        worksheetCCA.Cell("B6").Value = worksheet.Cell("B6").Value;

                                        foreach (var t in tableCAC)
                                        {

                                            int? IdOutcome = t.idOutcome;
                                            string NombreOutcome = t.OutDescrip;

                                            if (IdOutComeAuxiliar != IdOutcome)
                                            {
                                                count = 0;
                                                IdOutComeAuxiliar = IdOutcome;
                                                if (indiceCAC != celdaInicial)
                                                {
                                                    worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                                                    worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                                                    worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;
                                                    indiceCAC = indiceCAC + 1;
                                                    indiceAuxiliar = indiceCAC;
                                                    SumCriteriNota = 0;
                                                    SumNotaMaxima = 0;
                                                }
                                            }

                                            if (count == 0)
                                            {
                                                worksheetCCA.Cell("A" + indiceCAC).Value = NombreOutcome;
                                            }
                                            worksheetCCA.Cell("A" + indiceCAC).Style.Fill.SetBackgroundColor(XLColor.Green);

                                            string CriterioOutcome = t.criterio;
                                            string NotaMaxima = t.notaMaxCriterio;
                                            SumNotaMaxima += Int32.Parse(NotaMaxima);
                                            double? criteriNota = Math.Round((double)t.NotaCriterio, 3);
                                            SumCriteriNota += criteriNota;
                                            string incipiente = t.Incipiente;
                                            string proceso = t.proceso;
                                            string Esperado = t.Esperado;

                                            worksheetCCA.Cell("B" + indiceCAC).Value = CriterioOutcome;
                                            worksheetCCA.Cell("C" + indiceCAC).Value = NotaMaxima;
                                            worksheetCCA.Cell("D" + indiceCAC).Value = criteriNota;
                                            worksheetCCA.Cell("D" + indiceCAC).Style.Fill.SetBackgroundColor(color);
                                            if (RubricaGenerada)
                                            {
                                                if (criteriNota >= 0 && criteriNota < 1.3)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "X";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "";
                                                }
                                                else if (criteriNota >= 1.3 && criteriNota < 1.6)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "X";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "";
                                                }
                                                else if (criteriNota >= 1.6 && criteriNota <= 2)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "X";
                                                }
                                            }
                                            else
                                            {
                                                worksheetCCA.Cell("E" + indiceCAC).Value = incipiente;
                                                worksheetCCA.Cell("F" + indiceCAC).Value = proceso;
                                                worksheetCCA.Cell("G" + indiceCAC).Value = Esperado;
                                            }

                                            indiceCAC = indiceCAC + 1;
                                            count++;
                                            contadorFinalcac++;

                                            if (tableCAC.Count == contadorFinalcac)
                                            {
                                                worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                                            }


                                        }
                                        worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                                        worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;

                                        worksheetCCA.Range("A" + celdaInicial, "G" + (indiceCAC)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                                        worksheetCCA.Range("A" + celdaInicial, "G" + (indiceCAC)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    }

                                    else
                                    {
                                        //objExcel.Worksheet(2).Delete();      Para borrar la segunda hoja que no se usa
                                        var worksheetCCA = objExcel.Worksheet(2);

                                        if (CodCurso.Equals("SI410"))
                                        {
                                            tableEAC = OutcomeSixCACSI(tableEAC, CodCurso, RubricaGenerada);
                                            tableEAC.RemoveRange(8, 5);
                                        }
                                        else
                                        {
                                            tableEAC = OutcomeSixCACSI(tableEAC, CodCurso, RubricaGenerada);
                                            tableEAC.RemoveRange(14, 5);
                                        }

                                        var tableCAC = tableEAC;
                                        for (int i = 0; i < tableCAC.Count; i++)
                                        {
                                            tableCAC[i].NombreOutcome = tableCAC[i].NombreOutcome.Replace("EAC", "CAC");
                                        }


                                        worksheetCCA.Name = nombreHoja + "_CAC";

                                        SumCriteriNota = 0;
                                        SumNotaMaxima = 0;
                                        Int32 indiceCAC = celdaInicial;
                                        indiceAuxiliar = celdaInicial;
                                        count = 0;
                                        IdOutComeAuxiliar = 0;

                                        worksheetCCA.Cell("D7").Style.Fill.BackgroundColor = color;
                                        worksheetCCA.Cell("B1").Value = worksheet.Cell("B1").Value;
                                        worksheetCCA.Cell("B2").Value = worksheet.Cell("B2").Value;
                                        worksheetCCA.Cell("B3").Value = worksheet.Cell("B3").Value;
                                        worksheetCCA.Cell("B4").Value = worksheet.Cell("B4").Value;
                                        worksheetCCA.Cell("B5").Value = worksheet.Cell("B5").Value;
                                        worksheetCCA.Cell("B6").Value = worksheet.Cell("B6").Value;

                                        foreach (var t in tableCAC)
                                        {

                                            int? IdOutcome = t.idOutcome;
                                            string NombreOutcome = t.NombreOutcome;

                                            if (IdOutComeAuxiliar != IdOutcome)
                                            {
                                                count = 0;
                                                IdOutComeAuxiliar = IdOutcome;
                                                if (indiceCAC != celdaInicial)
                                                {
                                                    worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                                                    worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                                                    worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;
                                                    indiceCAC = indiceCAC + 1;
                                                    indiceAuxiliar = indiceCAC;
                                                    SumCriteriNota = 0;
                                                    SumNotaMaxima = 0;
                                                }
                                            }

                                            if (count == 0)
                                            {
                                                worksheetCCA.Cell("A" + indiceCAC).Value = NombreOutcome;
                                            }
                                            worksheetCCA.Cell("A" + indiceCAC).Style.Fill.SetBackgroundColor(XLColor.Green);

                                            string CriterioOutcome = t.criterio;
                                            string NotaMaxima = t.notaMaxCriterio;
                                            SumNotaMaxima += Int32.Parse(NotaMaxima);
                                            double? criteriNota = Math.Round((double)t.diferencialAppp, 3);
                                            SumCriteriNota += criteriNota;
                                            string incipiente = t.Incipiente;
                                            string proceso = t.proceso;
                                            string Esperado = t.Esperado;

                                            worksheetCCA.Cell("B" + indiceCAC).Value = CriterioOutcome;
                                            worksheetCCA.Cell("C" + indiceCAC).Value = NotaMaxima;
                                            worksheetCCA.Cell("D" + indiceCAC).Value = criteriNota;
                                            worksheetCCA.Cell("D" + indiceCAC).Style.Fill.SetBackgroundColor(color);
                                            if (RubricaGenerada)
                                            {
                                                if (criteriNota >= 0 && criteriNota < 1.3)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "X";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "";
                                                }
                                                else if (criteriNota >= 1.3 && criteriNota < 1.6)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "X";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "";
                                                }
                                                else if (criteriNota >= 1.6 && criteriNota <= 2)
                                                {
                                                    worksheetCCA.Cell("E" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("F" + indiceCAC).Value = "";
                                                    worksheetCCA.Cell("G" + indiceCAC).Value = "X";
                                                }
                                            }
                                            else
                                            {
                                                worksheetCCA.Cell("E" + indiceCAC).Value = incipiente;
                                                worksheetCCA.Cell("F" + indiceCAC).Value = proceso;
                                                worksheetCCA.Cell("G" + indiceCAC).Value = Esperado;
                                            }

                                            indiceCAC = indiceCAC + 1;
                                            count++;
                                            contadorFinalcac++;

                                            if (tableCAC.Count == contadorFinalcac)
                                            {
                                                worksheetCCA.Range("A" + indiceAuxiliar, "A" + (indiceCAC - 1)).Merge();
                                            }


                                        }
                                        worksheetCCA.Cell("C" + (indiceCAC)).Value = SumNotaMaxima;
                                        worksheetCCA.Cell("D" + (indiceCAC)).Value = SumCriteriNota;

                                        worksheetCCA.Range("A" + celdaInicial, "G" + (indiceCAC)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                                        worksheetCCA.Range("A" + celdaInicial, "G" + (indiceCAC)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    }
                                }
                                else
                                {
                                    objExcel.Worksheet(2).Delete();     // Para borrar la segunda hoja que no se usa
                                    foreach (var objOutcome in lstOutcomes)
                                    {
                                        var criterios = lstCriterios.Where(x => x.OutcomeCalificadoId == objOutcome.OutcomeCalificadoId).OrderBy(x => x.CriterioId);

                                        worksheet.Cell("A" + indice).Value = "Student Outcome " + objOutcome.Comision + " (" + objOutcome.Outcome + "): " + objOutcome.DescripcionOutcome;
                                        worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(XLColor.Green);
                                        worksheet.Range("A" + indice, "A" + (indice + criterios.Count() - 1)).Merge();

                                        foreach (var objCriterio in criterios)
                                        {
                                            worksheet.Cell("B" + indice).Value = objCriterio.Criterio + ": " + objCriterio.DescripcionCriterio;

                                            var lstNivelesDesempenio = rubrica.RubricaNivelDesempenio;
                                            if (!lstNivelesDesempenio.Any())
                                            {
                                                var lstNiveles = DataContext.NivelCriterio.Where(x => x.IdCriterioOutcome == objCriterio.CriterioId);

                                                worksheet.Cell("C" + indice).Value = objCriterio.NotaCriterio;

                                                worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);
                                                worksheet.Cell("D" + indice).Value = objCriterio.NotaAlumno;

                                                if (objCriterio.NivelesPorDefecto)
                                                {
                                                    int contLetra = 5;

                                                    if (RubricaGenerada)
                                                    {
                                                        if (objCriterio.NotaAlumno >= 0 && objCriterio.NotaAlumno < Convert.ToDecimal(1.3))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.3) && objCriterio.NotaAlumno < Convert.ToDecimal(1.6))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 1] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.6) && objCriterio.NotaAlumno <= Convert.ToDecimal(2.0))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 2] + indice).Value = "X";
                                                        }


                                                    }
                                                    else
                                                    {
                                                        foreach (var objnivel in lstNiveles)
                                                        {
                                                            if (objnivel.NotaMayor == objCriterio.NotaAlumno)
                                                            {
                                                                worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                            }
                                                            contLetra++;
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    int contLetra = 5;
                                                    if (RubricaGenerada)
                                                    {
                                                        if (objCriterio.NotaAlumno >= 0 && objCriterio.NotaAlumno < Convert.ToDecimal(1.3))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.3) && objCriterio.NotaAlumno < Convert.ToDecimal(1.6))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 1] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.6) && objCriterio.NotaAlumno <= Convert.ToDecimal(2.0))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 2] + indice).Value = "X";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (var objnivel in lstNiveles)
                                                        {
                                                            if (objnivel.NotaMenor <= objCriterio.NotaAlumno && objnivel.NotaMayor >= objCriterio.NotaAlumno)
                                                            {
                                                                worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                            }
                                                            contLetra++;
                                                        }
                                                    }

                                                }
                                                indice++;
                                            }
                                            else
                                            {
                                                var lstNiveles = rubrica.RubricaNivelDesempenio.Select(x => x.NivelDesempenio);

                                                worksheet.Cell("C" + indice).Value = objCriterio.NotaCriterio;

                                                worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);
                                                worksheet.Cell("D" + indice).Value = objCriterio.NotaAlumno;

                                                if (objCriterio.NivelesPorDefecto)
                                                {
                                                    int contLetra = 5;
                                                    if (RubricaGenerada)
                                                    {
                                                        if (objCriterio.NotaAlumno >= 0 && objCriterio.NotaAlumno < Convert.ToDecimal(1.3))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.3) && objCriterio.NotaAlumno < Convert.ToDecimal(1.6))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 1] + indice).Value = "X";
                                                        }
                                                        else if (objCriterio.NotaAlumno >= Convert.ToDecimal(1.6) && objCriterio.NotaAlumno <= Convert.ToDecimal(2.0))
                                                        {
                                                            worksheet.Cell(alphabetArray[contLetra + 2] + indice).Value = "X";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (var objnivel in lstNiveles)
                                                        {
                                                            if (objnivel.PuntajeMayor == objCriterio.NotaAlumno)
                                                            {
                                                                worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                                            }
                                                            contLetra++;
                                                        }
                                                    }

                                                }
                                                indice++;
                                            }
                                        }

                                        if (indice == indiceAuxiliar)
                                        {
                                            indice++;
                                        }

                                        indice++;
                                        worksheet.Cell("C" + (indice - 1)).Value = objOutcome.NotaOutcome;

                                        indiceAuxiliar = indice;
                                    }//////

                                }

                                worksheet.Range("A" + celdaInicial, "G" + (indice - 1)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                                worksheet.Range("A" + celdaInicial, "G" + (indice - 1)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                lstReporte.Add(objExcel);
                            }
                        }
                    }

                    contador++;
                }
                return lstReporte;
            }
            catch (Exception ex)
            {
                var a = contador;
                throw new Exception("Error",ex);
            }
        }


        public static XLWorkbook GenerateSingleStudentCriterioReport(AbetEntities DataContext, Int32 AlumnoSeccionId, String NombreEvaluacion, String path, ref String NombreReporte)
        {
            string[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            int celdaInicial = 10;
            var PeriodoAcademicoId = DataContext.PeriodoAcademico.FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
            var alumno = GetAlumnoCriterio(DataContext, PeriodoAcademicoId, AlumnoSeccionId);
            if (alumno != null)
            {
                var evaluacionId = GetEvaluacionId(DataContext, PeriodoAcademicoId,
                                       alumno.CarreraCursoPeriodoAcademicoId, NombreEvaluacion);

                var rubrica = DataContext.Rubrica.FirstOrDefault(x => x.IdEvaluacion == evaluacionId);

                var lstOutcomes = new List<StudentCriterioOutcomeReportModel>();

                if (!rubrica.Evaluacion.TipoEvaluacion.Tipo.Equals(ConstantHelpers.RUBRICFILETYPE.EvaluationType.PA))
                    lstOutcomes = GetOutcomeCriterioList(DataContext, alumno.AlumnoSeccionId, rubrica.IdRubrica);
                else
                    lstOutcomes = GetOutcomeCriterioListPA(DataContext, alumno.AlumnoSeccionId, rubrica.IdRubrica);

                if (lstOutcomes.Any())
                {
                    var lstCriterios = GetOutcomeCriterioDetailList(DataContext, lstOutcomes);
                    var objExcel = new XLWorkbook(path);

                    var worksheet = objExcel.Worksheet(1);
                    worksheet.Name = alumno.CodigoAlumno + "_" + alumno.Seccion;

                    XLColor color = worksheet.Cell("D7").Style.Fill.BackgroundColor;
                    worksheet.Cell("B1").Value = alumno.Curso;
                    worksheet.Cell("B2").Value = alumno.CodigoAlumno;
                    worksheet.Cell("B3").Value = alumno.Sede + " " + alumno.NombreAlumno;
                    worksheet.Cell("B4").Value = alumno.Carrera;

                    Int32 indice = celdaInicial;
                    Int32 indiceAuxiliar = celdaInicial;

                    var studentCriterioOutcomeReportModel = lstOutcomes.FirstOrDefault();
                    if (studentCriterioOutcomeReportModel != null)
                    {
                        worksheet.Cell("B5").Value = studentCriterioOutcomeReportModel.NotaRubricaCalificada * (20.0).ToDecimal() / rubrica.NotaMaxima;
                        worksheet.Cell("B6").Value = studentCriterioOutcomeReportModel.ObservacionRubrica;
                    }
                    else
                    {
                        worksheet.Cell("B5").Value = "NINGUNA";
                    }

                    foreach (var objOutcome in lstOutcomes)
                    {
                        var criterios = lstCriterios.Where(x => x.OutcomeCalificadoId == objOutcome.OutcomeCalificadoId);
                        worksheet.Cell("A" + indice).Value = "Student Outcome " + objOutcome.Comision + " (" + objOutcome.Outcome + "): " + objOutcome.DescripcionOutcome;
                        worksheet.Cell("A" + indice).Style.Fill.SetBackgroundColor(XLColor.Green);

                        foreach (var objCriterio in criterios)
                        {
                            worksheet.Cell("B" + indice).Value = objCriterio.Criterio + ": " + objCriterio.DescripcionCriterio;
                            var lstNiveles = DataContext.NivelCriterio.Where(x => x.IdCriterioOutcome == objCriterio.CriterioId);

                            worksheet.Cell("C" + indice).Value = objCriterio.NotaCriterio;

                            worksheet.Cell("D" + indice).Style.Fill.SetBackgroundColor(color);
                            worksheet.Cell("D" + indice).Value = objCriterio.NotaAlumno;

                            if (objCriterio.NivelesPorDefecto)
                            {
                                int contLetra = 5;
                                foreach (var objnivel in lstNiveles)
                                {
                                    if (objnivel.NotaMayor == objCriterio.NotaAlumno)
                                    {
                                        worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                    }
                                    contLetra++;
                                }
                            }
                            else
                            {
                                int contLetra = 5;
                                foreach (var objnivel in lstNiveles)
                                {
                                    if (objnivel.NotaMenor <= objCriterio.NotaAlumno && objnivel.NotaMayor >= objCriterio.NotaAlumno)
                                    {
                                        worksheet.Cell(alphabetArray[contLetra] + indice).Value = "X";
                                    }
                                    contLetra++;
                                }
                            }
                            indice++;
                        }

                        if (indice == indiceAuxiliar)
                        {
                            indice++;
                        }

                        indice++;
                        worksheet.Cell("C" + (indice - 1)).Value = objOutcome.NotaOutcome;

                        indiceAuxiliar = indice;
                    }

                    worksheet.Range("A" + celdaInicial, "G" + (indice - 1)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range("A" + celdaInicial, "G" + (indice - 1)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    NombreReporte = alumno.CodigoCursoReporte + "_" + alumno.CodigoAlumno + "_" + alumno.CodigoProyecto + ".xlsx";
                    return objExcel;
                }
            }
            return null;
        }

        public static XLWorkbook GenerateAsistenciaReport(CargarDatosContext ctx, Int32 idsede, DateTime? Fecha, String path, ref String NombreReporte)
        {
            var context = ctx.context;
            var reunionDelegado = (from a in context.ReunionDelegado
                                   join b in context.Asistencia on a.IdReunionDelegado equals b.idReunionDelegado
                                   join c in context.Alumno on b.idAlumno equals c.IdAlumno
                                   join d in context.Sede on a.IdSede equals d.IdSede
                                   where d.IdSede == idsede && a.Fecha == Fecha
                                   select new {
                                       codigo = c.Codigo ,
                                       Nombres = c.Nombres + " " + c.Apellidos ,
                                       Fecha = b.fechaActual,
                                       Sede = d.Nombre } ).ToList();

            String[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Int32 CeldaInicial = 5;
            var EvaluacionExcel = new XLWorkbook();
            var Excel = new XLWorkbook(path);
            int count = 1;
            var sede = context.Sede.FirstOrDefault(x => x.IdSede == idsede);

            NombreReporte = "REPORTE_ASISTENCIA_"+sede.Nombre.ToUpper()+"_"+ Fecha;
            var worksheet = Excel.Worksheet(1);
            foreach (var n in reunionDelegado)
            {

                worksheet.Cell("A" + (CeldaInicial)).Value = count;
                worksheet.Cell("B" + (CeldaInicial)).Value = n.codigo;
                worksheet.Cell("C" + (CeldaInicial)).Value = n.Nombres;
                worksheet.Cell("D" + (CeldaInicial)).Value = n.Sede;
                worksheet.Cell("E" + (CeldaInicial)).Value = n.Fecha;

                CeldaInicial++;
                count++;

            }
            NombreReporte = NombreReporte + ".xlsx";
            EvaluacionExcel = Excel;
            return EvaluacionExcel;
        }
        public static XLWorkbook GenerateArdCarreraCurso(CargarDatosContext ctx, Int32? IdCarrera, Int32? IdCarreraPeriodoAcademico, Int32? IdArea, Int32? IdSubArea, Int32? IdSubModalidadPeriodoAcademico, Int32? IdPeriodo, Int32? Idescuela, String path, ref String NombreReporte)
        {
            var context = ctx.context;
            var language = ctx.currentCulture;

            var Carrera = (from c in context.Carrera
                           where c.IdCarrera == IdCarrera
                           select c).FirstOrDefault();

            /*var reunionDelegado = (from rd in context.ReunionDelegado
                                   join cd in context.ComentarioDelegado on rd.IdReunionDelegado equals cd.IdReunionDelegado
                                   join ch in context.ComentarioHallazgo on cd.IdComentarioDelegado equals ch.IdComentarioDelegado
                                   join h in context.Hallazgos on ch.IdHallazgo equals h.IdHallazgo
                                   join smpa in context.SubModalidadPeriodoAcademico on rd.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                                   join cu in context.Curso on h.IdCurso equals cu.IdCurso
                                   join cpa in context.CursoPeriodoAcademico on cu.IdCurso equals cpa.IdCurso
                                   join ccpa in context.CarreraPeriodoAcademico on smpa.IdSubModalidadPeriodoAcademico equals ccpa.IdSubModalidadPeriodoAcademico
                                   join car in context.Carrera on ccpa.IdCarrera equals car.IdCarrera
                                   join uacurso in context.UnidadAcademica on cpa.IdCursoPeriodoAcademico equals uacurso.IdCursoPeriodoAcademico
                                   join uasubarea in context.UnidadAcademica on uacurso.IdUnidadAcademicaPadre equals uasubarea.IdUnidadAcademica
                                   join uaarea in context.UnidadAcademica on uasubarea.IdUnidadAcademicaPadre equals uaarea.IdUnidadAcademica
                                   join d in context.Sede on rd.IdSede equals d.IdSede
                                   where rd.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                                    && uacurso.Nivel == 3
                                    && ccpa.IdCarreraPeriodoAcademico == IdCarreraPeriodoAcademico
                                    && uaarea.IdCarreraPeriodoAcademico == IdCarreraPeriodoAcademico
                                    && cpa.IdSubModalidadPeriodoAcademico == rd.IdSubModalidadPeriodoAcademico
                                   select new
                                   {
                                       IdSubArea = uasubarea.IdUnidadAcademica,
                                       SubArea = language =="es-PE" ? uasubarea.NombreEspanol : uasubarea.NombreIngles,
                                       IdArea = uaarea.IdUnidadAcademica,
                                       Area = language == "es-PE" ? uaarea.NombreEspanol : uaarea.NombreIngles,
                                       codigo = rd.Comentario,
                                       Hallazgo = language == "es-PE" ? h.DescripcionEspanol: h.DescripcionIngles,
                                       IdEscuela = car.IdEscuela,
                                       Carrera = language == "es-PE" ? car.NombreEspanol : car.NombreIngles,
                                       CodCarrera = car.Codigo,
                                       Curso = language == "es-PE" ? cu.NombreEspanol : cu.NombreIngles,
                                       Sede = d.Nombre,
                                       Comentario = language == "es-PE" ? cd.DescripcionEspanol : cd.DescripcionIngles,
                                       //Criticidad = context.Criticidad.FirstOrDefault(x => x.IdCriticidad == h.IdCriticidad).NombreEspanol,
                                       Fecha = h.FechaRegistro
                                   }).Distinct().ToList(); */

            var delegateCommentaryLogic = new DelegateCommentaryLogic();
            var reunionDelegado = delegateCommentaryLogic.GetDelegateCommentary(IdSubModalidadPeriodoAcademico, IdCarreraPeriodoAcademico, IdCarrera);

            if (IdArea != 0 && IdSubArea != 0)
            {
                reunionDelegado = reunionDelegado.Where(x => x.uasubareaIdUnidadAcademica == IdSubArea).ToList();
            }
            else
            {
                if (IdArea != 0 && IdSubArea == 0)
                {
                    reunionDelegado = reunionDelegado.Where(x => x.uaareaIdUnidadAcademica == IdArea).ToList();
                }
                else
                {
                    reunionDelegado = reunionDelegado.Where(x => x.IdEscuela == Idescuela).ToList();
                }
            }

            String[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Int32 CeldaInicial = 5;
            var EvaluacionExcel = new XLWorkbook();
            var Excel = new XLWorkbook(path);
            int count = 1;
            //var dateAndTime = DateTime.Now;
            //var date = dateAndTime.Date;
            string CicloAcademico = context.PeriodoAcademico.FirstOrDefault(x => x.IdPeriodoAcademico == IdPeriodo).CicloAcademico;
            if (language == "es-PE")
            {
                NombreReporte = "REPORTE_ACTA_" + CicloAcademico + "_" + Carrera.NombreEspanol + "_" + Carrera.Codigo;
            }
            else {
                NombreReporte = "REPORT_MINUTES_" + CicloAcademico + "_" + Carrera.NombreIngles + "_" + Carrera.Codigo;
            }

            var worksheet = Excel.Worksheet(1);

            if (language != "es-PE")
            {
                worksheet.Cell("A" + 1).Value = "FINDING ERD BY CARRER/COURSE";

                worksheet.Cell("B" + 3).Value = "DATE";
                worksheet.Cell("B" + 4).Value = "CODE";
                worksheet.Cell("C" + 4).Value = "FINDING";
                worksheet.Cell("D" + 4).Value = "COURSE";
                worksheet.Cell("E" + 4).Value = "SUBAREA";
                worksheet.Cell("F" + 4).Value = "AREA";
                worksheet.Cell("G" + 4).Value = "CAMPUS";
                worksheet.Cell("H" + 4).Value = "CAREER";
                worksheet.Cell("I" + 4).Value = "COMMETARY";
            }

            foreach (var n in reunionDelegado)
            {
                //Codigo_ARD, Hallazgo, Curso, Carrera, Area, Fecha
                if(count == 1)

                    if(n.FechaRegistro != null)
                    {
                        worksheet.Cell("C" + (CeldaInicial - 2)).Value = n.FechaRegistro.Value.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        worksheet.Cell("C" + (CeldaInicial - 2)).Value = "";
                    }
                    

                worksheet.Cell("A" + (CeldaInicial)).Value = count;
                worksheet.Cell("B" + (CeldaInicial)).Value = n.Comentario;
                worksheet.Cell("C" + (CeldaInicial)).Value = language == "es-PE" ? n.hEspanol : n.hDescripcionIngles;
                worksheet.Cell("D" + (CeldaInicial)).Value = language == "es-PE" ?  n.cuEspanol : n.cuIngles;
                worksheet.Cell("E" + (CeldaInicial)).Value = language == "es-PE" ?  n.uasubareaEspanol : n.uasubareaIngles;
                worksheet.Cell("F" + (CeldaInicial)).Value = language == "es-PE" ?  n.uaareaEspanol : n.uaareaIngles;
                worksheet.Cell("G" + (CeldaInicial)).Value = n.Nombre;
                worksheet.Cell("H" + (CeldaInicial)).Value = n.Codigo;
                worksheet.Cell("I" + (CeldaInicial)).Value = language == "es-PE" ?  n.cdEspanol : n.cdIngles;
                //worksheet.Cell("I" + (CeldaInicial)).Value = n.Fecha;

                CeldaInicial++;
                count++;

            }
            NombreReporte = NombreReporte + ".xlsx";
            EvaluacionExcel = Excel;
            return EvaluacionExcel;
        }
        #endregion

        public static List <sp_obttablaEAC_Result> OutcomeSixCACSI(List<sp_obttablaEAC_Result> tableEAC,string CodigoCurso,bool RubricaGenerada)
        {
            List<sp_obttablaEAC_Result> result = new List<sp_obttablaEAC_Result>();
            List<sp_obttablaEAC_Result> tableEACAux = tableEAC;
            string OutcomeDescription = "Student Outcome CAC (6):LA CAPACIDAD PARA COMPRENDER Y BRINDAR SOPORTE PARA EL USO, ENTREGA Y GESTIÓN DE SISTEMAS DE INFORMACIÓN DENTRO DE UN ENTORNO DE SISTEMAS DE INFORMACIÓN.";

            string[] criteria;

            // Intialization of array
            criteria = new string[5] {
                "c1. Diseño de pruebas preliminares que verifiquen la calidad del servicio",
                "c2. Desarrollo de las pruebas para la calidad en la entrega de un servicio de sistemas de información",
                "c3. Análisis e interpretación de datos/resultados para la eficiente gestión de una solución en un entorno de sistemas de información",
                "c4. Actualiza conceptos y conocimientos necesarios para su desarrollo profesional que garantice la entrega, uso y gestión de sistemas de información",
                "c5. Reconoce la necesidad del aprendizaje permanente para el desempeño profesional que garantice la entrega, uso y gestión de sistemas de información" };
            int arraysize = tableEACAux.Count;
            if (CodigoCurso.Equals("SI410"))
            {
                int j = 0;
                for (int i=8;i< arraysize; i++)
                {
                    
                    sp_obttablaEAC_Result objaux = new sp_obttablaEAC_Result();
                    objaux.diferencialAppp = tableEAC[i].diferencialAppp;
                    
                    objaux.notaMaxCriterio = tableEAC[i].notaMaxCriterio;

                    if (RubricaGenerada)
                    {
                        objaux.proceso ="";
                        objaux.Esperado = "";
                        objaux.Incipiente = "X";
                    }
                    else
                    {
                        objaux.proceso = tableEAC[i].proceso;
                        objaux.Esperado = tableEAC[i].Esperado;
                        objaux.Incipiente = tableEAC[i].Incipiente;
                    }
               

                    objaux.idOutcome = 999999999;
                    objaux.NombreOutcome = OutcomeDescription;
                    objaux.criterio = criteria[j];

                    tableEACAux.Add(objaux);
                    j++;
                }
            }
            else
            {
                int j = 0;

                for (int i = 14; i < arraysize; i++)
                {
                    
                    sp_obttablaEAC_Result objaux = new sp_obttablaEAC_Result();
                    objaux.diferencialAppp = tableEAC[i].diferencialAppp;
                    
                    objaux.notaMaxCriterio = tableEAC[i].notaMaxCriterio;
                    if (RubricaGenerada)
                    {
                        objaux.proceso = "";
                        objaux.Esperado = "";
                        objaux.Incipiente = "X";
                    }
                    else
                    {
                        objaux.proceso = tableEAC[i].proceso;
                        objaux.Esperado = tableEAC[i].Esperado;
                        objaux.Incipiente = tableEAC[i].Incipiente;
                    }

                    objaux.idOutcome = 999999999;
                    objaux.NombreOutcome = OutcomeDescription;
                    objaux.criterio = criteria[j];

                    tableEACAux.Add(objaux);
                    j++;
                }
            }

            

            return tableEACAux;
        }

    }

    
}
