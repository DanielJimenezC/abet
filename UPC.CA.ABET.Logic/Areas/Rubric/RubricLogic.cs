﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Models;
using System.Transactions;
using System.Data.Entity;
using UPC.CA.ABET.Helpers;
using System.Data;
using System.Web;
using System.Web.Mvc;
#endregion

namespace UPC.CA.ABET.Logic.Areas.Rubric
{
    public class RubricLogic
    {
        #region EVALUACIÓN DE RÚBRICA 2017-2
        public static Decimal SaveRate(CargarDatosContext ctx, Int32 IdRubrica, Int32 IdAlumnoSeccion, Int32 IdOutcomeRubrica, Int32 IdCriterioOutcome, Int32 IdEvaluador, Decimal? NotaCriterio)
        {
            try
            {
                var context = ctx.context;
                var Rubrica = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
                var Evaluador = context.Evaluador.FirstOrDefault(x => x.IdEvaluador == IdEvaluador);
                var NombreEvaluador = Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo;

                RubricaCalificada RubricaCalificada;
                RubricaCalificada = context.RubricaCalificada.FirstOrDefault(x => x.IdRubrica == IdRubrica && x.IdAlumnoSeccion == IdAlumnoSeccion
                                                                                && x.Evaluador.TipoEvaluador.TipoEvaluadorMaestra.Codigo.Equals(NombreEvaluador));

                #region REGISTRO DEL OUTCOME Y CRITERIO CALIFICADO
                if (RubricaCalificada == null)
                {
                    var AlumnoSeccion = context.AlumnoSeccion.FirstOrDefault(x => x.IdAlumnoSeccion == IdAlumnoSeccion);
                    RubricaCalificada = new RubricaCalificada();
                    RubricaCalificada.Rubrica = Rubrica;
                    RubricaCalificada.AlumnoSeccion = AlumnoSeccion;
                    RubricaCalificada.IdEvaluador = IdEvaluador;
                    RubricaCalificada.Observacion = "";
                    RubricaCalificada.FechaCalifiacion = DateTime.Now;
                    RubricaCalificada.Guardada = false;
                    context.RubricaCalificada.Add(RubricaCalificada);

                    OutcomeCalificado OutcomeCalificado = new OutcomeCalificado();
                    OutcomeCalificado.RubricaCalificada = RubricaCalificada;
                    OutcomeCalificado.IdOutcome = IdOutcomeRubrica;
                    context.OutcomeCalificado.Add(OutcomeCalificado);

                    CriterioCalificado CriterioCalificado = new CriterioCalificado();
                    CriterioCalificado.OutcomeCalificado = OutcomeCalificado;
                    CriterioCalificado.NotaCriterio = NotaCriterio;
                    CriterioCalificado.IdCriterio = IdCriterioOutcome;
                    context.CriterioCalificado.Add(CriterioCalificado);
                }
                else
                {
                    OutcomeCalificado outcomeExiste = context.OutcomeCalificado.FirstOrDefault(x => x.IdRubricaCalificada == RubricaCalificada.IdRubricaCalificada && x.IdOutcome == IdOutcomeRubrica);

                    if (outcomeExiste == null)
                    {
                        OutcomeCalificado OutcomeCalificado = new OutcomeCalificado();
                        OutcomeCalificado.RubricaCalificada = RubricaCalificada;
                        OutcomeCalificado.IdOutcome = IdOutcomeRubrica;
                        context.OutcomeCalificado.Add(OutcomeCalificado);

                        CriterioCalificado CriterioCalificado = new CriterioCalificado();
                        CriterioCalificado.OutcomeCalificado = OutcomeCalificado;
                        CriterioCalificado.NotaCriterio = NotaCriterio;
                        CriterioCalificado.IdCriterio = IdCriterioOutcome;
                        context.CriterioCalificado.Add(CriterioCalificado);
                    }
                    else
                    {
                        var criteriosCalificados = outcomeExiste.CriterioCalificado.Where(x => x.IdCriterio == IdCriterioOutcome);
                        if (criteriosCalificados.Any())
                            context.CriterioCalificado.RemoveRange(criteriosCalificados);

                        CriterioCalificado CriterioCalificado = new CriterioCalificado();
                        CriterioCalificado.OutcomeCalificado = outcomeExiste;
                        CriterioCalificado.NotaCriterio = NotaCriterio;
                        CriterioCalificado.IdCriterio = IdCriterioOutcome;
                        context.CriterioCalificado.Add(CriterioCalificado);
                    }
                }
                #endregion

                #region RECALCULAR NOTA DE LOS OUTCOMES Y LA RÚBRICA
                var OutcomesCalificados = RubricaCalificada.OutcomeCalificado;
                var OutcomesRubrica = Rubrica.OutcomeRubrica;

                foreach (var OutcomeCalificado in OutcomesCalificados)
                {
                    Decimal NotaOutcome = OutcomeCalificado.CriterioCalificado.Sum(x => x.NotaCriterio.Value);
                    OutcomeCalificado.NotaOutcome = NotaOutcome;
                }

                RubricaCalificada.NotaRubrica = RubricaCalificada.OutcomeCalificado.Sum(x => x.NotaOutcome.Value);
                RubricaCalificada.NotaMaxima = Rubrica.NotaMaxima;
                RubricaCalificada.SePermiteSustentacion = true;
                RubricaCalificada.Guardada = RubricaCalificada.Observacion.Equals("") && NombreEvaluador.Equals("COM") ? false : true;
                Rubrica.SePuedeEditar = false;
                #endregion

                #region REGISTROS EN NOTA DE VERIFICACIÓN ALUMNO
                List<NotaVerificacionAlumno> lstNotaVerificacion;
                if (NombreEvaluador.Equals("GER"))
                    lstNotaVerificacion = RubricaCalificada.AlumnoSeccion.NotaVerificacionAlumno.Where(x => x.NombreEvaluacion.Equals("PA")).ToList();
                else
                    lstNotaVerificacion = RubricaCalificada.AlumnoSeccion.NotaVerificacionAlumno.Where(x => x.NombreEvaluacion.Equals("TF")).ToList();

                if (lstNotaVerificacion.Any())
                    context.NotaVerificacionAlumno.RemoveRange(lstNotaVerificacion);


                var criteriosRubrica = Rubrica.OutcomeRubrica.Select(x => x.CriterioOutcome.Count()).Sum();

                var criteriosCalificado = RubricaCalificada.OutcomeCalificado.Select(x => x.CriterioCalificado.Count()).Sum();

                if (criteriosRubrica == criteriosCalificado)
                    RubricaCalificada.Guardada = true;
                /*
                if (OutcomesCalificados.Count == OutcomesRubrica.Count)
                    RubricaCalificada.Guardada = true;*/
                
                foreach(var OutcomeCalificado in OutcomesCalificados)
                {
                    NotaVerificacionAlumno notaVerificacion = new NotaVerificacionAlumno();
                    notaVerificacion.AlumnoSeccion = RubricaCalificada.AlumnoSeccion;
                    notaVerificacion.NombreEvaluacion = Rubrica.Evaluacion.TipoEvaluacion.Tipo;
                    notaVerificacion.IdOutcomeRubrica = OutcomeCalificado.IdOutcome;
                    notaVerificacion.NotaOutcome = OutcomeCalificado.NotaOutcome.Value;

                    Decimal notaOutcomeRubrica = OutcomesRubrica.FirstOrDefault(x => x.IdOutcomeRubrica == OutcomeCalificado.IdOutcome).NotaOutcome.Value;
                    Decimal nivelParcial = Decimal.Round(OutcomeCalificado.NotaOutcome.Value/ notaOutcomeRubrica, 2, MidpointRounding.ToEven);

                    if (nivelParcial >= 0 && nivelParcial <= (0.33).ToDecimal())
                        notaVerificacion.NivelAlcanzado = 1;
                    if (nivelParcial >= (0.34).ToDecimal() && nivelParcial <= (0.67).ToDecimal())
                        notaVerificacion.NivelAlcanzado = 2;
                    if (nivelParcial >= (0.68).ToDecimal() && nivelParcial <= (1).ToDecimal())
                        notaVerificacion.NivelAlcanzado = 3;

                    context.NotaVerificacionAlumno.Add(notaVerificacion);
                }
                #endregion
                context.SaveChanges();
                return Decimal.Round((RubricaCalificada.NotaRubrica.Value * 20) / Rubrica.NotaMaxima.Value, 2);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int? GetCicloRubrica(CargarDatosContext ctx, Int32 IdRubrica)
        {
            var context = ctx.context;
            var rubrica = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            return rubrica.Evaluacion.TipoEvaluacion.IdSubModalidadPeriodoAcademico;
        }

        public static decimal GetNotaMaximaRubrica(CargarDatosContext ctx, Int32 IdRubrica)
        {
            var context = ctx.context;
            var rubrica = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            return rubrica.OutcomeRubrica.Sum(x => x.NotaOutcome.Value);
        }

        public static void ActualizaNotaMaximaRubrica(AbetEntities context, Int32 IdRubrica)
        {
            Rubrica rubricaActualizar = context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);
            var outcomes = rubricaActualizar.OutcomeRubrica;
            foreach (var outcome in outcomes)
                outcome.NotaOutcome = outcome.CriterioOutcome.Sum(x => x.ValorMaximo);

            rubricaActualizar.NotaMaxima = outcomes.Sum(x => x.NotaOutcome.Value);
            context.SaveChanges();
        }
        #endregion

        #region Creacion de rubrica

        public static bool GenerarRubrica(AbetEntities context, int IdEvaluacionActual, int idEvaluacion)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    var rubricaPasada =
                        context.Rubrica.Include(x => x.OutcomeRubrica).Include(
                            x => x.OutcomeRubrica.Select(y => y.CriterioOutcome)).Include(
                            x => x.OutcomeRubrica.Select(y => y.CriterioOutcome.Select(z => z.NivelCriterio)))
                            .Where(x => x.IdEvaluacion == idEvaluacion).AsQueryable();
                    Rubrica nuevaRubrica = new Rubrica();
                    nuevaRubrica.Descripcion = rubricaPasada.FirstOrDefault().Descripcion;
                    nuevaRubrica.IdEvaluacion = IdEvaluacionActual;
                    nuevaRubrica.EsNotaEvaluada = rubricaPasada.FirstOrDefault().EsNotaEvaluada;
                    decimal NotaMaxima = 0;
                    context.Rubrica.Add(nuevaRubrica);
                    context.SaveChanges();

                    foreach (var item in nuevaRubrica.OutcomeRubrica)
                    {
                        OutcomeRubrica nuevoOutcome = new OutcomeRubrica();
                        nuevoOutcome.IdRubrica = nuevaRubrica.IdRubrica;
                        nuevoOutcome.IdOutcomeComision = item.IdOutcomeComision;
                        nuevoOutcome.NotaOutcome = item.NotaOutcome;
                        nuevoOutcome.NivelMaximo = 3;

                        context.OutcomeRubrica.Add(nuevoOutcome);
                        context.SaveChanges();

                        decimal NotaOutcome = 0;

                        foreach (var subItem in item.CriterioOutcome)
                        {
                            CriterioOutcome nuevoCriterio = new CriterioOutcome();
                            nuevoCriterio.IdOutcomeRubrica = subItem.IdOutcomeRubrica;
                            nuevoCriterio.Descripcion = subItem.Descripcion;
                            nuevoCriterio.ValorMaximo = subItem.ValorMaximo;
                            nuevoCriterio.EsEvaluadoPorDefecto = subItem.EsEvaluadoPorDefecto;
                            nuevoCriterio.Nombre = subItem.Nombre;
                            nuevoCriterio.ValorMinimo = subItem.ValorMinimo;

                            context.CriterioOutcome.Add(nuevoCriterio);
                            context.SaveChanges();

                            foreach (var subSubItem in subItem.NivelCriterio)
                            {
                                NivelCriterio nuevoNivel = new NivelCriterio();
                                nuevoNivel.IdCriterioOutcome = subSubItem.IdCriterioOutcome;
                                nuevoNivel.NotaMenor = subSubItem.NotaMenor;
                                nuevoNivel.NotaMayor = subSubItem.NotaMayor;
                                nuevoNivel.Nombre = subSubItem.Nombre;
                                nuevoNivel.DescripcionNivel = subSubItem.DescripcionNivel;
                                context.NivelCriterio.Add(nuevoNivel);
                                NotaOutcome = NotaOutcome + subSubItem.NotaMayor.Value;
                                NotaMaxima = NotaMaxima + NotaOutcome;
                                context.SaveChanges();
                            }
                        }

                        nuevoOutcome.NotaOutcome = NotaOutcome;
                        context.SaveChanges();
                    }

                    nuevaRubrica.NotaMaxima = NotaMaxima.ToInteger();
                    context.SaveChanges();
                    transaction.Complete();

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static IQueryable<NivelCriterio> AddDefaultNiveles(AbetEntities context, int IdCriterio)
        {
            string[] NombresNiveles = { "Incipiente", "En proceso", "Esperado" };
            string[] DescripcionNiveles = { "No se ha cumplido con el criterio", "El criterio ha sido cumplido parcialmente", "El criterio ha sido cumplido en su totalidad" };

            CriterioOutcome Criterio = context.CriterioOutcome.FirstOrDefault(x => x.IdCriterioOutcome == IdCriterio);
            Criterio.EsEvaluadoPorDefecto = true;
            context.NivelCriterio.RemoveRange(Criterio.NivelCriterio);

            for (int i = 0; i < NombresNiveles.Length; i++)
            {
                NivelCriterio nuevoNivel = new NivelCriterio();
                nuevoNivel.IdCriterioOutcome = IdCriterio;
                nuevoNivel.Nombre = NombresNiveles[i];
                nuevoNivel.NotaMayor = i < NombresNiveles.Length - 1 ? Criterio.ValorMaximo * i / 2 : Criterio.ValorMaximo;
                nuevoNivel.DescripcionNivel = DescripcionNiveles[i];

                context.NivelCriterio.Add(nuevoNivel);
            }

            context.SaveChanges();

            return context.NivelCriterio.Where(x => x.IdCriterioOutcome == IdCriterio);
        }

        public static void EditRubrica(CargarDatosContext dataContext, RubricaEntity viewModel, String NombreEvaluacion)
        {
            Rubrica rubrica = dataContext.context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == viewModel.IdEvaluacion);

            rubrica.EsNotaEvaluada = viewModel.EsNotaEvaluada;

            CreateOutcomePerRubrica(dataContext.context, viewModel, NombreEvaluacion);
        }

        public static void SaveRoles(AbetEntities context, Int32 idRubrica)
        {
            try
            {

                // Validar si ya está el rol de comité guardado por defecto como evaluadores de los outcome
                List<OutcomeRubrica> lstOutcomeRubrica = (from b in context.OutcomeRubrica
                                                          join c in context.OutcomeEvaluador on b.IdOutcomeRubrica equals c.IdOutcomeRubrica
                                                          where b.IdRubrica == idRubrica
                                                          select b).ToList();

                //Validación si ya se han agregado
                if (lstOutcomeRubrica != null)
                    return; 

                //En caso contrario se guarda por primera vez el rol de comité como evaluadores de los outcomes de la rúbrica


                int IdCicloActual = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).First(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
                Models.TipoEvaluador evaluadorComite = context.TipoEvaluador.Where(x => x.IdSubModalidadPeriodoAcademico == IdCicloActual && x.Peso == 1).FirstOrDefault();

                List<OutcomeRubrica> outcomeRubrica = context.OutcomeRubrica.Where(x => x.IdRubrica == idRubrica).ToList();
                
                foreach (var item in outcomeRubrica)
                {                    
                    Int32 idEvaluador, idOutcomeRubrica;
                    idEvaluador = evaluadorComite.IdTipoEvaluador;
                    idOutcomeRubrica = item.IdOutcomeRubrica;

                    OutcomeEvaluador match =  new OutcomeEvaluador();
                    match.IdTipoEvaluador = idEvaluador;
                    match.IdOutcomeRubrica = idOutcomeRubrica;
                    match.Estado = true;
                    context.OutcomeEvaluador.Add(match);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Abet 2017-2 Método que se dejará de usar porque ya no cumple con el alineamiento de negocio
        public static void SaveRoles(AbetEntities context, Dictionary<string, string> Matches)
        {
            try
            {
                Int32 idRubrica;
                Int32.TryParse(Matches.First().Value, out idRubrica);
                var OutcomesEvaluador =
                    context.OutcomeEvaluador
                        .Where(x => x.OutcomeRubrica.IdRubrica == idRubrica);
                var first = Matches.First();
                var last = Matches.Last();
                Matches.Remove(first.Key);
                Matches.Remove(last.Key);

                foreach (var item in OutcomesEvaluador)
                {
                    item.Estado = false;
                }
                context.SaveChanges();

                foreach (var item in Matches)
                {

                    var key = item.Key;
                    var value = item.Value;
                    Int32 idEvaluador, idOutcomeRubrica;
                    var split = key.Split('-');
                    Int32.TryParse(split[1], out idEvaluador);
                    Int32.TryParse(split[0], out idOutcomeRubrica);

                    OutcomeEvaluador match;
                    match =
                        context.OutcomeEvaluador.FirstOrDefault(
                            x => x.IdOutcomeRubrica == idOutcomeRubrica && x.IdTipoEvaluador == idEvaluador);

                    if (match == null)
                    {
                        match = new OutcomeEvaluador();
                        match.IdTipoEvaluador = idEvaluador;
                        match.IdOutcomeRubrica = idOutcomeRubrica;
                        match.Estado = true;
                        context.OutcomeEvaluador.Add(match);
                    }
                    else
                    {
                        match.Estado = true;
                    }
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Rubrica LoadRubric(AbetEntities context, int idEvaluacion)
        {
            return
                context.Rubrica.Include(
                    x => x.OutcomeRubrica.Select(y => y.CriterioOutcome.Select(z => z.NivelCriterio)))
                    .FirstOrDefault(x => x.IdEvaluacion == idEvaluacion);
        }

        public static IQueryable<NivelCriterio> AddListNiveles(AbetEntities context, NivelCriterio Nivel, int idCriterio)
        {
            context.NivelCriterio.Add(Nivel);
            context.SaveChanges();
            return context.NivelCriterio.Where(x => x.IdCriterioOutcome == idCriterio);
        }

        public static IQueryable<CriterioOutcome> AddListCriterio(AbetEntities context, CriterioOutcome Criterio)
        {
            context.CriterioOutcome.Add(Criterio);
            context.SaveChanges();
            return context.CriterioOutcome.Where(x => x.IdOutcomeRubrica == Criterio.IdOutcomeRubrica);
        }

        public static void DeleteNivelesByCriterio(AbetEntities context, int idCriterio, bool porDefecto = true)
        {
            try
            {
                var CriterioEncontrado = context.CriterioOutcome.FirstOrDefault(x => x.IdCriterioOutcome == idCriterio);
                CriterioEncontrado.EsEvaluadoPorDefecto = false;

                context.NivelCriterio.RemoveRange(CriterioEncontrado.NivelCriterio);
                context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static IQueryable<CriterioOutcome> DeleteCriterioById(AbetEntities context, int idCriterio)
        {
            try
            {
                CriterioOutcome criterioEncontrado =
                    context.CriterioOutcome.FirstOrDefault(x => x.IdCriterioOutcome == idCriterio);
                if (criterioEncontrado != null)
                {
                    Int32 idOutcome = criterioEncontrado.IdOutcomeRubrica;
                    context.NivelCriterio.RemoveRange(criterioEncontrado.NivelCriterio);
                    context.CriterioOutcome.Remove(criterioEncontrado);
                    context.SaveChanges();

                    return
                        context.CriterioOutcome.Include(x => x.NivelCriterio)
                            .Where(x => x.IdOutcomeRubrica == idOutcome);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Int32 GetIdActualEvaluacion(AbetEntities context, RubricaEntity rubrica)
        {
            try
            {
                //Temp
                var UltimoCiclo = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x => x.Estado.Equals("ACT")).IdPeriodoAcademico;
                //Temp

                var CarreraCursoPeriodoAnterior = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == rubrica.IdCurso);
                var CarreraCursoPeriodoActual = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.Carrera.IdCarrera == CarreraCursoPeriodoAnterior.Carrera.IdCarrera
                    && x.CursoPeriodoAcademico.IdCurso == CarreraCursoPeriodoAnterior.CursoPeriodoAcademico.IdCurso
                    && x.CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico == UltimoCiclo);

                var TipoEvaluacion =
                    context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == rubrica.IdEvaluacion).TipoEvaluacion.Tipo;

                Evaluacion evaluacion =
                    context.Evaluacion.FirstOrDefault(
                        x => x.IdCarreraCursoPeriodoAcademico == CarreraCursoPeriodoActual.IdCarreraCursoPeriodoAcademico
                            && x.TipoEvaluacion.Tipo.Equals(TipoEvaluacion));

                return evaluacion.IdEvaluacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Int32 CreateOutcomePerRubrica(AbetEntities context, RubricaEntity rubrica, String NombreEvaluacion)
        {

            var Carrera = context.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarreraPeriodoAcademico == rubrica.IdCarrera).Carrera;
            string NombreComision = string.Empty;
            String tipoOutcomeCursoTofind = string.Empty;

            switch (Carrera.Codigo)
            {
                case ConstantHelpers.RUBRICFILETYPE.Career.SWF:
                    NombreComision = ConstantHelpers.RUBRICFILETYPE.Comission.EAC;
                    break;
                case ConstantHelpers.RUBRICFILETYPE.Career.ISI:
                    NombreComision = ConstantHelpers.RUBRICFILETYPE.Comission.CAC;
                    break;
                case ConstantHelpers.RUBRICFILETYPE.Career.CC:
                    NombreComision = ConstantHelpers.RUBRICFILETYPE.Comission.CAC;
                    break;
            }


            Int32 idComision = -1;
            List<Int32> comisiones = new List<Int32>();
            if (NombreEvaluacion != "PA")
            {
                var com = context.Comision.FirstOrDefault(x => x.Codigo.Equals(NombreComision) /*&& x.AcreditadoraPeriodoAcademico.IdPeriodoAcademico == rubrica.IdPeriodoAcademico*/);
                idComision = context.Comision.FirstOrDefault(x => x.Codigo.Equals(NombreComision) && x.AcreditadoraPeriodoAcademico.IdSubModalidadPeriodoAcademico == rubrica.IdPeriodoAcademico).IdComision;
                var SelectedComisiones = rubrica.SelectedComsiones.ToList();
                comisiones = context.CarreraComision.Where(x => x.IdCarrera == Carrera.IdCarrera && SelectedComisiones.Contains(x.IdComision)).Select(x => x.IdComision).ToList();
            }
            else
            {
                idComision = context.Comision.FirstOrDefault(x => x.Codigo.Equals("WASC")).IdComision;
                comisiones = context.CarreraComision.Where(x => x.Carrera.IdCarrera == Carrera.IdCarrera && x.Comision.Codigo.Equals(ConstantHelpers.COMISON_WASC)).Select(x => x.IdComision).ToList();
            }

            var CarreraCicloCurso = context.CarreraCursoPeriodoAcademico.FirstOrDefault(x => x.IdCarreraCursoPeriodoAcademico == rubrica.IdCurso);
            var MallasxCarrera = context.MallaCurricular.Where(x => x.IdCarrera == CarreraCicloCurso.IdCarrera);
            Int32 idMalla = -1;
            foreach (var item in MallasxCarrera)
            {
                var MallasxPeriodo = context.MallaCurricularPeriodoAcademico.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == rubrica.IdPeriodoAcademico && x.IdMallaCurricular == item.IdMallaCurricular);
                if (MallasxPeriodo != null)
                {
                    idMalla = item.IdMallaCurricular;
                    break;
                }
            }
            var idCursoMallaCurricualar = context.CursoMallaCurricular.FirstOrDefault(x => x.IdCurso == CarreraCicloCurso.CursoPeriodoAcademico.IdCurso && x.IdMallaCurricular == idMalla);


            List<MallaCocosDetalle> OutcomesCurso =
                context.MallaCocosDetalle.Include(x => x.OutcomeComision)
                    .Where(
                        x => x.IdCursoMallaCurricular == idCursoMallaCurricualar.IdCursoMallaCurricular
                        && comisiones.Contains(x.OutcomeComision.IdComision)
                        && x.EsCalificado.Value == true
                        && x.TipoOutcomeCurso.DescripcionEspanol.Contains("Verifica"))
                    .ToList();

            if (OutcomesCurso.Count == 0)
            {
                throw new Exception("El curso no tiene outcomes dentro de la malla de cocos");
            }
            Rubrica nuevaRubrica = new Rubrica();
            nuevaRubrica.EsNotaEvaluada = rubrica.EsNotaEvaluada;
            nuevaRubrica.IdEvaluacion = rubrica.IdEvaluacion;
            nuevaRubrica.Descripcion = "";
            nuevaRubrica.SePuedeEditar = true;

            context.Rubrica.Add(nuevaRubrica);
            context.SaveChanges();

            foreach (var item in OutcomesCurso)
            {
                OutcomeRubrica OutRubrica = new OutcomeRubrica();
                OutRubrica.IdRubrica = nuevaRubrica.IdRubrica;
                OutRubrica.IdOutcomeComision = item.IdOutcomeComision;
                OutRubrica.NivelMaximo = 3;
                context.OutcomeRubrica.Add(OutRubrica);
            }

            context.SaveChanges();

            return nuevaRubrica.IdRubrica;


        }

        public static IQueryable<OutcomeEntity> GetOutcomesByRubricaId(AbetEntities context, Int32 idRubrica)
        {
            return
                context.OutcomeRubrica.Include(x => x.OutcomeComision.Outcome)
                    .Where(x => x.IdRubrica == idRubrica)
                    .Select(x => new OutcomeEntity
                    {
                        IdOutcome = x.IdOutcomeRubrica,
                        DescripcionOutcome = x.OutcomeComision.Outcome.DescripcionEspanol,
                        NombreOutcome = x.OutcomeComision.Outcome.Nombre,
                        NotaOutcome = x.NotaOutcome
                    });
        }

        public static bool IsParticipationEvaluation(AbetEntities context, Int32 idEvaluacion)
        {
            return
                context.Evaluacion.FirstOrDefault(x => x.IdEvaluacion == idEvaluacion).TipoEvaluacion.Tipo.Equals("PA")
                    ? true
                    : false;
        }

        public static bool RubricExists(AbetEntities context, Int32 idEvaluacion)
        {
            Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == idEvaluacion);
            return Rubrica != null ? true : false;
        }

        public static bool CanEditRubrica(AbetEntities context, Int32 idEvaluacion)
        {
            Rubrica Rubrica = context.Rubrica.FirstOrDefault(x => x.IdEvaluacion == idEvaluacion);
            return Rubrica.SePuedeEditar && Rubrica.RubricaCalificada.Count == 0;
        }

        public static List<CriterioOutcome> AddListCriteriosCompGen(AbetEntities context, CriterioEntity criterio)
        {
            var LogrosOutComision = context.OutcomeRubrica.Find(criterio.IdOutcome).OutcomeComision.LogroOutcomeComision.ToList();

            int count = 1;

            List<CriterioOutcome> ListCriterioOutcomes = new List<CriterioOutcome>();

            var CriterioExistentes = context.CriterioOutcome.Where(x => x.IdOutcomeRubrica == criterio.IdOutcome);
            if (CriterioExistentes.Count() > 0)
                context.CriterioOutcome.RemoveRange(CriterioExistentes);

            foreach (var item in LogrosOutComision)
            {
                CriterioOutcome nuevoCriterio = new CriterioOutcome();
                nuevoCriterio.Nombre = item.Nombre;
                nuevoCriterio.Descripcion = item.Descripcion;
                nuevoCriterio.IdOutcomeRubrica = criterio.IdOutcome;

                if (count == 1)
                {
                    nuevoCriterio.ValorMaximo = criterio.NotaMaxima * 0;
                    count++;
                }
                else if (count == 2)
                {
                    nuevoCriterio.ValorMaximo = criterio.NotaMaxima * Convert.ToDecimal(0.5);
                    count++;
                }
                else if (count == 3)
                {
                    nuevoCriterio.ValorMaximo = criterio.NotaMaxima * 1;
                    count++;
                }

                context.CriterioOutcome.Add(nuevoCriterio);
                context.SaveChanges();


            }

            ListCriterioOutcomes = context.CriterioOutcome.Where(x => x.IdOutcomeRubrica == criterio.IdOutcome).ToList();
            return ListCriterioOutcomes;
        }

        public static void AsociateOutcomesEvaluatorWASC(AbetEntities context, int idRubrica)
        {

            List<OutcomeRubrica> outcomes = context.OutcomeRubrica.Where(x => x.IdRubrica == idRubrica).ToList();
            Int32 UltimoCiclo = context.PeriodoAcademico.OrderByDescending(x => x.CicloAcademico).FirstOrDefault(x => x.Estado.Equals(ConstantHelpers.ESTADO.ACTIVO)).IdPeriodoAcademico;
            TipoEvaluador rol = context.TipoEvaluador.FirstOrDefault(x => x.IdSubModalidadPeriodoAcademico == UltimoCiclo && x.TipoEvaluadorMaestra.Codigo.Equals(ConstantHelpers.ROL_GERENTE));

            if (rol == null)
            {
                throw new Exception("Debe existir el rol de Gerente (GER) para que pueda ser asignado a la evaluación Participación (PA)");
            }
            foreach (var item in outcomes)
            {
                OutcomeEvaluador outEva = context.OutcomeEvaluador.FirstOrDefault(x => x.IdOutcomeRubrica == item.IdOutcomeRubrica && x.IdTipoEvaluador == rol.IdTipoEvaluador);
                if (outEva == null)
                {
                    outEva = new OutcomeEvaluador();
                    outEva.IdOutcomeRubrica = item.IdOutcomeRubrica;
                    outEva.IdTipoEvaluador = rol.IdTipoEvaluador;
                    context.OutcomeEvaluador.Add(outEva);
                }
                outEva.Estado = true;

            }
            context.SaveChanges();

        }

        public static void CalcularNotaMaximaRubrica(AbetEntities context, int idRubrica)
        {
            Rubrica rubricaActualizar = context.Rubrica.Find(idRubrica);
            List<OutcomeRubrica> outcomes = context.OutcomeRubrica.Where(x => x.IdRubrica == idRubrica).ToList();

            Decimal NotaMaximaRubrica = 0;
            Decimal NotaOutcome = 0;

            if (rubricaActualizar.Evaluacion.TipoEvaluacion.Tipo.Equals(ConstantHelpers.EVALUACION_PARTICIPACION))
            {
                foreach (var outcome in rubricaActualizar.OutcomeRubrica)
                {
                    NotaOutcome = NotaMaximaRubrica + outcome.CriterioOutcome.OrderByDescending(x => x.ValorMaximo).First().ValorMaximo;

                    OutcomeRubrica outcomeRubrica = context.OutcomeRubrica.Find(outcome.IdOutcomeRubrica);
                    outcomeRubrica.NotaOutcome = NotaOutcome;
                    context.SaveChanges();
                }
            }
            else
            {
                foreach (var item in outcomes)
                {
                    NotaOutcome = 0;

                    if (item.CriterioOutcome != null)
                    {
                        foreach (var criterio in item.CriterioOutcome)
                        {
                            NotaOutcome = NotaOutcome + criterio.ValorMaximo;
                        }
                    }

                    OutcomeRubrica outcomeRubrica = context.OutcomeRubrica.Find(item.IdOutcomeRubrica);
                    outcomeRubrica.NotaOutcome = NotaOutcome;
                    context.SaveChanges();
                }
            }
            foreach (var item in outcomes)
            {
                NotaMaximaRubrica = NotaMaximaRubrica + item.NotaOutcome.Value;
            }
            rubricaActualizar.NotaMaxima = NotaMaximaRubrica;
            context.SaveChanges();
        }
        
        public static void SaveRubricQualify(AbetEntities context, Dictionary<string, string> Matches)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {

                    Int32 EvaluadorId;
                    Int32.TryParse(Matches.First().Value, out EvaluadorId);
                    List<RubricaCalificada> rubricas = new List<RubricaCalificada>();
                    List<OutcomeCalificado> outcomes = new List<OutcomeCalificado>();
                    List<CriterioCalificado> criterios = new List<CriterioCalificado>();
                    List<OutcomeRubrica> outcomesRubrica = new List<OutcomeRubrica>();

                    var itemRemove = Matches.First(x => x.Key == "submit");
                    Matches.Remove(itemRemove.Key);
                    Matches.Remove(Matches.First().Key);
                    Matches.Remove(Matches.First().Key);

                    var itemsObservacion = Matches.Where(x => x.Key.Contains("observacion")).ToList();
                    var itemsSustentacion = Matches.Where(x => x.Key.Contains("sustentacion")).ToList();

                    foreach (var itemObservacion in itemsObservacion)
                    {
                        Matches.Remove(itemObservacion.Key);
                    }

                    foreach (var itemSustentacion in itemsSustentacion)
                    {
                        Matches.Remove(itemSustentacion.Key);
                    }

                    var itemsNoEvaluaNota = Matches.Where(x => x.Key.Contains("=")).ToList();
                    var itemsOutcomesWasc = Matches.Where(x => x.Key.Contains("*")).ToList();

                    if (itemsNoEvaluaNota.Count > 0)
                    {
                        #region NoEsNotaEvaluada

                        foreach (var item in Matches)
                        {
                            var key = item.Key;
                            var value = item.Value;
                            Int32 idRubrica, idAlumnoSeccion, idOutcomeRubrica;
                            var split = key.Split('=');
                            Int32.TryParse(split[0], out idRubrica);
                            Int32.TryParse(split[1], out idAlumnoSeccion);
                            Int32.TryParse(split[2], out idOutcomeRubrica);

                            RubricaCalificada rubricaExiste = rubricas.FirstOrDefault(x => x.IdAlumnoSeccion == idAlumnoSeccion);

                            if (rubricaExiste == null)
                            {
                                RubricaCalificada rubricaCalificada = new RubricaCalificada();
                                rubricaCalificada.IdRubrica = idRubrica;
                                rubricaCalificada.IdAlumnoSeccion = idAlumnoSeccion;
                                rubricaCalificada.IdEvaluador = EvaluadorId;
                                rubricaCalificada.FechaCalifiacion = DateTime.Now;
                                rubricaCalificada.Observacion = itemsObservacion.First().Value;
                                rubricaCalificada.NotaMaxima = context.Rubrica.Find(idRubrica).NotaMaxima;

                                if (itemsSustentacion.Count > 0)
                                {
                                    var valor = itemsSustentacion.First().Value;

                                    if (valor.Equals("true"))
                                        rubricaCalificada.SePermiteSustentacion = true;
                                    else
                                        rubricaCalificada.SePermiteSustentacion = false;

                                    itemsSustentacion.Remove(itemsSustentacion.First());
                                }

                                itemsObservacion.Remove(itemsObservacion.First());
                                context.RubricaCalificada.Add(rubricaCalificada);
                                context.SaveChanges();
                                rubricas.Add(rubricaCalificada);

                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaCalificada.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    outcomeCal.ComentarioOutcome = item.Value;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));
                                }
                            }
                            else
                            {
                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaExiste.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    outcomeCal.ComentarioOutcome = item.Value;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));
                                }
                            }

                            Rubrica rubricaFinal = context.Rubrica.Find(idRubrica);
                            rubricaFinal.SePuedeEditar = false;
                            context.SaveChanges();
                        }

                        String codigoEvaluador = context.Evaluador.FirstOrDefault(x => x.IdEvaluador == EvaluadorId).TipoEvaluador.TipoEvaluadorMaestra.Codigo;

                        if (codigoEvaluador == "COM")
                        {
                            foreach (var itemOutcomeCalificado in outcomes)
                            {
                                NotaVerificacionAlumno notaVerificacion = new NotaVerificacionAlumno();
                                notaVerificacion.IdAlumnoSeccion = itemOutcomeCalificado.RubricaCalificada.IdAlumnoSeccion;
                                notaVerificacion.NombreEvaluacion = itemOutcomeCalificado.RubricaCalificada.Rubrica.Evaluacion.TipoEvaluacion.Tipo;
                                notaVerificacion.IdOutcomeRubrica = itemOutcomeCalificado.IdOutcome;
                                notaVerificacion.NotaOutcome = itemOutcomeCalificado.NotaOutcome.Value;
                                Decimal nivelParcial = Decimal.Round(itemOutcomeCalificado.NotaOutcome.Value / outcomesRubrica.FirstOrDefault(x => x.IdOutcomeRubrica == itemOutcomeCalificado.IdOutcome).NotaOutcome.Value, 2);

                                if (nivelParcial >= 0 && nivelParcial <= (0.33).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 1;
                                }
                                if (nivelParcial >= (0.34).ToDecimal() && nivelParcial <= (0.67).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 2;
                                }
                                if (nivelParcial >= (0.68).ToDecimal() && nivelParcial <= (1).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 3;
                                }

                                context.NotaVerificacionAlumno.Add(notaVerificacion);
                                context.SaveChanges();
                            }
                        }


                        transaction.Complete();

                        #endregion
                    }

                    if (itemsOutcomesWasc.Count > 0)
                    {
                        #region RubricaWASC

                        foreach (var item in Matches)
                        {
                            var key = item.Key;
                            var value = item.Value;
                            Int32 idRubrica, idAlumnoSeccion, idOutcomeRubrica, idCriterioOutcome;
                            var split = key.Split('*');
                            Int32.TryParse(split[0], out idRubrica);
                            Int32.TryParse(split[1], out idAlumnoSeccion);
                            Int32.TryParse(split[2], out idOutcomeRubrica);
                            Int32.TryParse(split[3], out idCriterioOutcome);
                            Decimal notaNivel = value.ToDecimal();

                            RubricaCalificada rubricaExiste = rubricas.FirstOrDefault(x => x.IdAlumnoSeccion == idAlumnoSeccion);

                            if (rubricaExiste == null)
                            {
                                RubricaCalificada rubricaCalificada = new RubricaCalificada();
                                rubricaCalificada.IdRubrica = idRubrica;
                                rubricaCalificada.IdAlumnoSeccion = idAlumnoSeccion;
                                rubricaCalificada.IdEvaluador = EvaluadorId;
                                rubricaCalificada.FechaCalifiacion = DateTime.Now;
                                rubricaCalificada.Observacion = itemsObservacion.First().Value;
                                itemsObservacion.Remove(itemsObservacion.First());
                                context.RubricaCalificada.Add(rubricaCalificada);
                                context.SaveChanges();
                                rubricas.Add(rubricaCalificada);

                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaCalificada.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));

                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeCal.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                                else
                                {
                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeExiste.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                            }
                            else
                            {
                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaExiste.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));

                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeCal.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                                else
                                {
                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeExiste.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                            }
                        }

                        foreach (var outcome in outcomes)
                        {
                            Decimal notaOutCome = 0;
                            foreach (var criterio in criterios)
                            {
                                if (outcome.IdOutcomeCalificado == criterio.IdOutcomeCalificado)
                                {
                                    notaOutCome = notaOutCome + criterio.NotaCriterio.Value;
                                }
                            }

                            OutcomeCalificado outcomeCalificado = context.OutcomeCalificado.Find(outcome.IdOutcomeCalificado);
                            outcomeCalificado.NotaOutcome = notaOutCome;
                            context.SaveChanges();
                        }

                        foreach (var rubrica in rubricas)
                        {
                            Decimal notaRubrica = 0;
                            foreach (var outcome in outcomes)
                            {
                                if (rubrica.IdRubricaCalificada == outcome.IdRubricaCalificada)
                                {
                                    notaRubrica = notaRubrica + outcome.NotaOutcome.Value;
                                }
                            }

                            Decimal notaMaxima = 0;
                            foreach (var item in outcomesRubrica.Distinct().ToList())
                            {
                                notaMaxima = notaMaxima + item.NotaOutcome.Value;
                            }

                            RubricaCalificada rubricaCalificada = context.RubricaCalificada.Include(x => x.Rubrica).FirstOrDefault(x => x.IdRubricaCalificada == rubrica.IdRubricaCalificada);
                            rubricaCalificada.NotaRubrica = notaRubrica;
                            rubricaCalificada.NotaMaxima = rubricaCalificada.Rubrica.NotaMaxima;
                            context.SaveChanges();

                            Rubrica rubricaFinal = context.Rubrica.Find(rubrica.IdRubrica);
                            rubricaFinal.SePuedeEditar = false;
                            context.SaveChanges();
                        }

                        String codigoEvaluador = context.Evaluador.Find(EvaluadorId).TipoEvaluador.TipoEvaluadorMaestra.Codigo;

                        if (codigoEvaluador == "GER")
                        {
                            foreach (var itemOutcomeCalificado in outcomes)
                            {
                                NotaVerificacionAlumno notaVerificacion = new NotaVerificacionAlumno();
                                notaVerificacion.IdAlumnoSeccion = itemOutcomeCalificado.RubricaCalificada.IdAlumnoSeccion;
                                notaVerificacion.NombreEvaluacion = itemOutcomeCalificado.RubricaCalificada.Rubrica.Evaluacion.TipoEvaluacion.Tipo;
                                notaVerificacion.IdOutcomeRubrica = itemOutcomeCalificado.IdOutcome;
                                notaVerificacion.NotaOutcome = itemOutcomeCalificado.NotaOutcome.Value;
                                Decimal nivelParcial = Decimal.Round(itemOutcomeCalificado.NotaOutcome.Value / outcomesRubrica.Find(x => x.IdOutcomeRubrica == itemOutcomeCalificado.IdOutcome).NotaOutcome.Value, 2);

                                if (nivelParcial >= 0 && nivelParcial <= (0.33).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 1;
                                }
                                if (nivelParcial >= (0.34).ToDecimal() && nivelParcial <= (0.67).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 2;
                                }
                                if (nivelParcial >= (0.68).ToDecimal() && nivelParcial <= (1).ToDecimal())
                                {
                                    notaVerificacion.NivelAlcanzado = 3;
                                }

                                context.NotaVerificacionAlumno.Add(notaVerificacion);
                                context.SaveChanges();
                            }
                        }

                        transaction.Complete();
                        #endregion
                    }
                    if (itemsNoEvaluaNota.Count == 0 && itemsOutcomesWasc.Count == 0)
                    {
                        #region EsNotaEvaluada

                        foreach (var item in Matches)
                        {
                            var key = item.Key;
                            var value = item.Value;
                            Int32 idRubrica, idAlumnoSeccion, idOutcomeRubrica, idCriterioOutcome;
                            var split = key.Split('-');
                            Int32.TryParse(split[0], out idRubrica);
                            Int32.TryParse(split[1], out idAlumnoSeccion);
                            Int32.TryParse(split[2], out idOutcomeRubrica);
                            Int32.TryParse(split[3], out idCriterioOutcome);
                            Decimal notaNivel = value.ToDecimal();

                            RubricaCalificada rubricaExiste = rubricas.FirstOrDefault(x => x.IdAlumnoSeccion == idAlumnoSeccion);

                            if (rubricaExiste == null)
                            {
                                RubricaCalificada rubricaCalificada = new RubricaCalificada();
                                rubricaCalificada.IdRubrica = idRubrica;
                                rubricaCalificada.IdAlumnoSeccion = idAlumnoSeccion;
                                rubricaCalificada.IdEvaluador = EvaluadorId;
                                rubricaCalificada.FechaCalifiacion = DateTime.Now;
                                rubricaCalificada.Observacion = itemsObservacion.First().Value;

                                itemsObservacion.Remove(itemsObservacion.First());
                                context.RubricaCalificada.Add(rubricaCalificada);
                                context.SaveChanges();
                                rubricas.Add(rubricaCalificada);

                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaCalificada.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));

                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeCal.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                                else
                                {
                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeExiste.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                            }
                            else
                            {
                                OutcomeCalificado outcomeExiste = outcomes.FirstOrDefault(x => x.RubricaCalificada.IdAlumnoSeccion == idAlumnoSeccion && x.IdOutcome == idOutcomeRubrica);

                                if (outcomeExiste == null)
                                {
                                    OutcomeCalificado outcomeCal = new OutcomeCalificado();
                                    outcomeCal.IdRubricaCalificada = rubricaExiste.IdRubricaCalificada;
                                    outcomeCal.IdOutcome = idOutcomeRubrica;
                                    context.OutcomeCalificado.Add(outcomeCal);
                                    context.SaveChanges();
                                    outcomes.Add(outcomeCal);
                                    outcomesRubrica.Add(context.OutcomeRubrica.Find(outcomeCal.IdOutcome));

                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeCal.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                                else
                                {
                                    CriterioCalificado criterioCal = new CriterioCalificado();
                                    criterioCal.IdOutcomeCalificado = outcomeExiste.IdOutcomeCalificado;
                                    criterioCal.NotaCriterio = notaNivel;
                                    criterioCal.IdCriterio = idCriterioOutcome;
                                    context.CriterioCalificado.Add(criterioCal);
                                    context.SaveChanges();
                                    criterios.Add(criterioCal);
                                }
                            }
                        }

                        foreach (var outcome in outcomes)
                        {
                            Decimal notaOutCome = 0;
                            foreach (var criterio in criterios)
                            {
                                if (outcome.IdOutcomeCalificado == criterio.IdOutcomeCalificado)
                                {
                                    notaOutCome = notaOutCome + criterio.NotaCriterio.Value;
                                }
                            }

                            OutcomeCalificado outcomeCalificado = context.OutcomeCalificado.Find(outcome.IdOutcomeCalificado);
                            outcomeCalificado.NotaOutcome = notaOutCome;
                            context.SaveChanges();
                        }

                        foreach (var rubrica in rubricas)
                        {
                            Decimal notaRubrica = 0;
                            foreach (var outcome in outcomes)
                            {
                                if (rubrica.IdRubricaCalificada == outcome.IdRubricaCalificada)
                                {
                                    notaRubrica = notaRubrica + outcome.NotaOutcome.Value;
                                }
                            }

                            Decimal notaMaxima = 0;
                            foreach (var item in outcomesRubrica.Distinct().ToList())
                            {
                                notaMaxima = notaMaxima + item.NotaOutcome.Value;
                            }

                            RubricaCalificada rubricaCalificada = context.RubricaCalificada.Find(rubrica.IdRubricaCalificada);
                            rubricaCalificada.NotaRubrica = notaRubrica;
                            rubricaCalificada.NotaMaxima = context.Rubrica.FirstOrDefault(x => x.IdRubrica == rubrica.IdRubrica).NotaMaxima;
                            context.SaveChanges();

                            Rubrica rubricaFinal = context.Rubrica.Find(rubrica.IdRubrica);
                            rubricaFinal.SePuedeEditar = false;
                            context.SaveChanges();
                        }

                        String codigoEvaluador = context.Evaluador.Find(EvaluadorId).TipoEvaluador.TipoEvaluadorMaestra.Codigo;

                        if (codigoEvaluador == "COM")
                        {
                            foreach (var rubricaCalificada in rubricas)
                            {
                                List<OutcomeCalificado> outcomesCalificadosRoles = context.OutcomeCalificado.Where(x => x.IdRubricaCalificada == rubricaCalificada.IdRubricaCalificada).ToList();
                                List<OutcomeRubrica> outcomesRubricaTodos = context.OutcomeRubrica.Where(x => x.IdRubrica == rubricaCalificada.IdRubrica).ToList();

                                foreach (var outcome in outcomesRubricaTodos)
                                {
                                    Decimal sumaOutcomes = 0;
                                    Decimal promedioOutcomes = 0;
                                    foreach (var outCalificado in outcomesCalificadosRoles.Where(x => x.IdOutcome == outcome.IdOutcomeRubrica))
                                    {
                                        sumaOutcomes = sumaOutcomes + outCalificado.NotaOutcome.Value;
                                    }

                                    NotaVerificacionAlumno notaVerificacion = new NotaVerificacionAlumno();
                                    notaVerificacion.IdAlumnoSeccion = rubricaCalificada.IdAlumnoSeccion;
                                    notaVerificacion.NombreEvaluacion = rubricaCalificada.Rubrica.Evaluacion.TipoEvaluacion.Tipo;
                                    notaVerificacion.IdOutcomeRubrica = outcome.IdOutcomeRubrica;
                                    promedioOutcomes = sumaOutcomes / outcomesCalificadosRoles.Where(x => x.IdOutcome == outcome.IdOutcomeRubrica).Count();
                                    notaVerificacion.NotaOutcome = promedioOutcomes;

                                    Decimal nivelParcial = Decimal.Round(promedioOutcomes / outcomesRubrica.FirstOrDefault(x => x.IdOutcomeRubrica == outcome.IdOutcomeRubrica).NotaOutcome.Value, 2, MidpointRounding.ToEven);

                                    if (nivelParcial >= 0 && nivelParcial <= (0.33).ToDecimal())
                                    {
                                        notaVerificacion.NivelAlcanzado = 1;
                                    }
                                    if (nivelParcial >= (0.34).ToDecimal() && nivelParcial <= (0.67).ToDecimal())
                                    {
                                        notaVerificacion.NivelAlcanzado = 2;
                                    }
                                    if (nivelParcial >= (0.68).ToDecimal() && nivelParcial <= (1).ToDecimal())
                                    {
                                        notaVerificacion.NivelAlcanzado = 3;
                                    }

                                    context.NotaVerificacionAlumno.Add(notaVerificacion);
                                    context.SaveChanges();
                                }

                            }
                        }

                        transaction.Complete();
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static bool RubricAlredyHasOutcomesForComission(CargarDatosContext dataContext, Int32 idEvaluacion, int[] comisiones)
        {
            Rubrica rubrica = dataContext.context.Rubrica.Include(x => x.OutcomeRubrica).FirstOrDefault(x => x.IdEvaluacion == idEvaluacion);
            bool respuesta = true;

            var comsionesExistentes = rubrica.OutcomeRubrica.Select(x => x.OutcomeComision.IdComision).Distinct();

            foreach (var item in comisiones)
            {
                if (!comsionesExistentes.Contains(item))
                {
                    respuesta = false;
                    break;
                }
            }

            foreach (var item in comsionesExistentes)
            {
                if (!comisiones.Contains(item))
                {
                    respuesta = false;
                    break;
                }
            }

            return respuesta;
        }

        public static int DeleteRubric(CargarDatosContext dataContext, int IdRubrica)
        {
            using (var transaction = new TransactionScope())
            {
                List<RubricaCalificada> rubricasCalificadas = dataContext.context.RubricaCalificada.Where(x => x.IdRubrica == IdRubrica).ToList();
                Rubrica rubrica = dataContext.context.Rubrica.FirstOrDefault(x => x.IdRubrica == IdRubrica);

                if (rubrica == null)
                    return 2;
                if (rubricasCalificadas.Count > 0)
                    return 1;

                var NivelesRubrica = dataContext.context.NivelCriterio.Where(x => x.CriterioOutcome.OutcomeRubrica.IdRubrica == IdRubrica);
                var CriteriosRubrica = dataContext.context.CriterioOutcome.Where(x => x.OutcomeRubrica.IdRubrica == IdRubrica);
                var OutcomesEvaluador = dataContext.context.OutcomeEvaluador.Where(x => x.OutcomeRubrica.IdRubrica == IdRubrica);
                var OutcomesRubrica = dataContext.context.OutcomeRubrica.Where(x => x.IdRubrica == IdRubrica);
                dataContext.context.OutcomeEvaluador.RemoveRange(OutcomesEvaluador);
                dataContext.context.NivelCriterio.RemoveRange(NivelesRubrica);
                dataContext.context.CriterioOutcome.RemoveRange(CriteriosRubrica);
                dataContext.context.OutcomeRubrica.RemoveRange(OutcomesRubrica);
                dataContext.context.Rubrica.Remove(rubrica);

                dataContext.context.SaveChanges();
                transaction.Complete();
                return 0;
            }
        }

        public static void DeleteGradesByProject(CargarDatosContext DataContext, Int32 ProyectoID, String TipoEvaluacion)
        {
            DataContext.context.sp_EliminarNotasProyecto(ProyectoID, TipoEvaluacion);
        }
    }

    #endregion
    
}
