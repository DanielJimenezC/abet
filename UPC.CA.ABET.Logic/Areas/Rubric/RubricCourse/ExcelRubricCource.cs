﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Models.Areas.Rubric.ExcelReportModel;

namespace UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse
{
    public class ExcelRubricCourceCapstone
    {
        public static List<XLWorkbook> GenerateExcelRubricCourse_Capstone(AbetEntities DataContext, int idCurso, string CodCurso, string path, CargarDatosContext ctx, int rubricType)
        {
            int contador = 0;

            var context = ctx.context;
            string[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            //int celdaInicial = 18;

            var alumnos = GetAlumnos(DataContext, idCurso, rubricType, ctx.session.GetSubModalidadPeriodoAcademicoId().Value);

            var ExcelRubric = new List<XLWorkbook>();

            foreach (var idAlumno in alumnos.Select(x => x.idAlumno))
            {
                decimal puntajeTOTAL = 0;
                var objExcel = new XLWorkbook(path);
                var worksheet = objExcel.Worksheet(1);
                var datosAlumno = GetCodigoAlumno(DataContext, idAlumno);
                string nombreHoja = datosAlumno.CodigoAlumno + "_PARCIAL";
                worksheet.Name = nombreHoja;

                var i = 2;
                var celdaInicial = 10;

                var idRubrica = alumnos.Select(x => x.idRubricaControl).FirstOrDefault();
                var nivelesAceptacion = GetIdRubricaNivelAceptacion(DataContext, idRubrica);


                var count = 1;


                var datoAlumno = GetDatosAlumno(DataContext, idAlumno);
                var codigo = datoAlumno.CodigoAlumno;
                var nombres = datoAlumno.NombreAlumno + " " + datoAlumno.ApellidoAlum;
                var nombreProyec = datoAlumno.NombreProyecto;
                var observacion = datoAlumno.obs;

                worksheet.Cell("B1").Value = nombreProyec;
                worksheet.Cell("B1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Cell("B1").Style.Alignment.WrapText = true;

                worksheet.Cell("A3").Value = codigo;
                worksheet.Cell("A3").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Cell("A3").Style.Alignment.WrapText = true;

                worksheet.Cell("B3").Value = nombres;
                worksheet.Cell("B3").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Cell("B3").Style.Alignment.WrapText = true;

                worksheet.Cell("A5").Value = observacion;
                worksheet.Cell("A5").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Cell("A5").Style.Alignment.WrapText = true;




                foreach (var nivelAcep in nivelesAceptacion.Select(x => x.idRubricaControlCriterioCapstone))
                {

                    var dataNivelAcep = GetNivelAcepData(DataContext, nivelAcep);
                    var descrip = dataNivelAcep.nombreCriterioEspa;
                    
                    if ( count <= 3)
                    {
                        var celda_Nivel = "" + alphabetArray[i] + "" + celdaInicial + "";
                        //var celda_PESO = "" + alphabetArray[i] + "" + 17 + "";
                        worksheet.Cell(celda_Nivel).Value = descrip;
                        worksheet.Cell(celda_Nivel).Style.Fill.SetBackgroundColor(XLColor.Red);
                        worksheet.Cell(celda_Nivel).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(celda_Nivel).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        worksheet.Cell(celda_Nivel).Style.Alignment.WrapText = true;

                        count++;

                    }
                    

                    i++;
                }

                var celda_Puntaje_Nombre = "" + alphabetArray[count+1] + "" + 10 + "";
                worksheet.Cell(celda_Puntaje_Nombre).Value = "PUNTAJE";
                worksheet.Cell(celda_Puntaje_Nombre).Style.Fill.SetBackgroundColor(XLColor.Red);
                worksheet.Cell(celda_Puntaje_Nombre).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(celda_Puntaje_Nombre).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Cell(celda_Puntaje_Nombre).Style.Alignment.WrapText = true;


                var celdaInicialPregunta = 11;
                var celdaInicialCriterio = 11;
                var celdaInicialPuntaje = 11;
                var idPreguntas = GetIdPreguntas(DataContext, idRubrica);

                foreach (var id_pregunta in idPreguntas.Select(x => x.idRubricaControlPregunta))
                {
                    var pregunta = GetPregunta(DataContext, id_pregunta);


                    var celda_Pregunta = "" + alphabetArray[1] + "" + celdaInicialPregunta + "";
                    celdaInicialPregunta += 1;
                    var celda_Pregunta_SIG = "" + alphabetArray[1] + "" + celdaInicialPregunta + "";
                    worksheet.Cell(celda_Pregunta).Value = pregunta.preguntaEspanol;
                    worksheet.Range(celda_Pregunta, celda_Pregunta_SIG).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(celda_Pregunta, celda_Pregunta_SIG).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Range(celda_Pregunta, celda_Pregunta_SIG).Merge().Style.Alignment.WrapText = true;

                    var z = 2;
                    var add = 2;

                    var idCriterios = GetIdCriterios(DataContext, id_pregunta);
                    foreach (var id_criterio in idCriterios.Select(x => x.idRubricaControlCriterioCapstone))
                    {
                        var idcriterio = GetCriterio(DataContext, id_criterio);
                        var celda_Criterio = "" + alphabetArray[z] + "" + celdaInicialCriterio + "";
                        worksheet.Cell(celda_Criterio).Value = idcriterio.descriptionEspanol;
                        worksheet.Cell(celda_Criterio).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(celda_Criterio).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        worksheet.Cell(celda_Criterio).Style.Alignment.WrapText = true;


                        var puntajeMaximo = idcriterio.puntajeMaximo;
                        var puntajeMinimo = idcriterio.puntajeMinimo;
                        var celda_puntaje = "" + alphabetArray[z] + "" + (celdaInicialCriterio + 1) + "";
                        if (puntajeMaximo == puntajeMinimo)
                        {
                            var puntaje = puntajeMaximo + " puntos";
                            worksheet.Cell(celda_puntaje).Value = puntaje;
                            worksheet.Cell(celda_puntaje).Style.Fill.SetBackgroundColor(XLColor.Red);
                            worksheet.Cell(celda_puntaje).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(celda_puntaje).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            worksheet.Cell(celda_puntaje).Style.Alignment.WrapText = true;
                        }
                        else
                        {
                            var puntaje = "[ " + puntajeMaximo + " - " + puntajeMinimo + " ] puntos";
                            worksheet.Cell(celda_puntaje).Value = puntaje;
                            worksheet.Cell(celda_puntaje).Style.Fill.SetBackgroundColor(XLColor.Red);
                            worksheet.Cell(celda_puntaje).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(celda_puntaje).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            worksheet.Cell(celda_puntaje).Style.Alignment.WrapText = true;
                        }

                        z++;
                    }

                    var puntaje_P = GetPuntaje(DataContext, id_pregunta, idAlumno);
                    var puntaje_pregunta = puntaje_P.idPuntaje;

                    var celda_Puntaje = "" + alphabetArray[z] + "" + celdaInicialPuntaje + "";
                    worksheet.Cell(celda_Puntaje).Value = puntaje_pregunta;
                    worksheet.Cell(celda_Puntaje).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(celda_Puntaje).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Cell(celda_Puntaje).Style.Alignment.WrapText = true;


                    decimal punt = puntaje_pregunta;
                    puntajeTOTAL += punt;

                    /*
                   celdaInicial++;





                   //celdaInicial++;
                   celdaInicial = 10;
                   i++;*/
                    celdaInicialPuntaje++;
                    celdaInicialPuntaje++;
                    celdaInicialPregunta++;
                    celdaInicialCriterio++;
                    celdaInicialCriterio++;
                }
                worksheet.Cell("E3").Value = puntajeTOTAL;
                ExcelRubric.Add(objExcel);

            }

            return ExcelRubric;
        }
        private static List<StudentReportModel> GetAlumnos(AbetEntities DataContext, int idCurso, int rubricType, int submodaPA)
        {
            return (from sec in DataContext.RubricaControlEvaluacion
                    join rc in DataContext.RubricaControl on sec.idRubricaControl equals rc.idRubricaControl
                    where rc.IdSubModalidadPeriodoAcademico == submodaPA && rc.IdCurso == idCurso

                    select new StudentReportModel()
                    {
                        idRubricaControlEvaluacion = sec.IdRubricaControlEvaluacion,
                        idRubricaControl = sec.idRubricaControl,
                        idAlumno = sec.idAlumno,
                        AlumnoSeccionId = sec.idSeccion,
                        idDocenteCalificador = sec.idDocenteCalificador

                    }
            ).ToList();
        }
        public static StudentReportModel GetCodigoAlumno(AbetEntities DataContext, Int32 idAlumno)
        {
            return (from rce in DataContext.RubricaControlEvaluacion
                    join a in DataContext.Alumno on rce.idAlumno equals a.IdAlumno
                    join d in DataContext.Docente on rce.idDocenteCalificador equals d.IdDocente
                    join s in DataContext.Seccion on rce.idSeccion equals s.IdSeccion
                    where rce.idAlumno == idAlumno

                    select new StudentReportModel()
                    {
                        idAlumno = rce.idAlumno,
                        CodigoAlumno = a.Codigo,
                        NombreAlumno = a.Nombres,
                        ApellidoAlum = a.Apellidos,
                        idCarreraEgreso = a.IdCarreraEgreso,
                        idDocenteCalificador = rce.idDocenteCalificador,
                        NombreDocente = d.Nombres,
                        ApellidoDocente = d.Apellidos,
                        codigoDocente = d.Codigo,
                        SeccionCurso = s.Codigo,
                        SedeSeccion = s.IdSede

                    }
           ).FirstOrDefault();

        }
        public static List<StudentReportModel> GetIdRubricaNivelAceptacion(AbetEntities DataContext, Int32 IdRubrica)
        {
            return (from rccc in DataContext.RubricaControlCriterioCapstone
                    join rcp in DataContext.RubricaControlPregunta on rccc.idRubricaControlPregunta equals rcp.idRubricaControlPregunta
                    join rc in DataContext.RubricaControl on rcp.idRubricaControl equals rc.idRubricaControl
                    where rc.idRubricaControl == IdRubrica

                    select new StudentReportModel()
                    {
                        idRubricaControlCriterioCapstone = rccc.idRubricaControlCriterioCapstone,
                        idRubricaControlPregunta = rccc.idRubricaControlPregunta,
                        nombreCriterioEspa = rccc.nombreEspanol,
                        descriptionEspanol = rccc.descripcionEspanol,
                        puntajeMaximo = rccc.maximo,
                        puntajeMinimo = rccc.minimo,
                        preguntaEspanol = rcp.preguntaEspanol

                    }
           ).ToList();

        }
        public static StudentReportModel GetNivelAcepData(AbetEntities DataContext, Int32 idnivel)
        {
            return (from rccc in DataContext.RubricaControlCriterioCapstone
                    join rcp in DataContext.RubricaControlPregunta on rccc.idRubricaControlPregunta equals rcp.idRubricaControlPregunta
                    join rc in DataContext.RubricaControl on rcp.idRubricaControl equals rc.idRubricaControl
                    where rccc.idRubricaControlCriterioCapstone == idnivel

                    select new StudentReportModel()
                    {
                        idRubricaControlPregunta = rccc.idRubricaControlPregunta,
                        nombreCriterioEspa = rccc.nombreEspanol,
                        descriptionEspanol = rccc.descripcionEspanol,
                        puntajeMaximo = rccc.maximo,
                        puntajeMinimo = rccc.minimo,
                        preguntaEspanol = rcp.preguntaEspanol

                    }
           ).FirstOrDefault();

        }
        private static List<StudentReportModel> GetIdPreguntas(AbetEntities DataContext, int idRubrica)
        {
            return (from rcp in DataContext.RubricaControlPregunta
                    where rcp.idRubricaControl == idRubrica

                    select new StudentReportModel()
                    {
                        idRubricaControlPregunta = rcp.idRubricaControlPregunta

                    }
            ).ToList();
        }
        public static StudentReportModel GetPregunta(AbetEntities DataContext, Int32 idPregunta)
        {
            return (from rcp in DataContext.RubricaControlPregunta
                    where rcp.idRubricaControlPregunta == idPregunta

                    select new StudentReportModel()
                    {

                        preguntaEspanol = rcp.preguntaEspanol,
                        preguntaIngles = rcp.preguntaIngles,
                        puntajeMaximo = rcp.puntajeMaximo

                    }
           ).FirstOrDefault();

        }

        private static List<StudentReportModel> GetIdCriterios(AbetEntities DataContext, int id_pregunta)
        {
            return (from rccc in DataContext.RubricaControlCriterioCapstone
                    where rccc.idRubricaControlPregunta == id_pregunta

                    select new StudentReportModel()
                    {
                        idRubricaControlCriterioCapstone = rccc.idRubricaControlCriterioCapstone,
                        idRubricaControlPregunta = rccc.idRubricaControlPregunta

                    }
            ).ToList();
        }
        public static StudentReportModel GetCriterio(AbetEntities DataContext, Int32 id_criterio)
        {
            return (from rcc in DataContext.RubricaControlCriterioCapstone
                    where rcc.idRubricaControlCriterioCapstone == id_criterio

                    select new StudentReportModel()
                    {
                        descriptionEspanol = rcc.descripcionEspanol,
                        puntajeMaximo = rcc.maximo,
                        puntajeMinimo = rcc.minimo

                    }
           ).FirstOrDefault();

        }
        
        public static StudentReportModel GetPuntaje(AbetEntities DataContext,Int32 id_puntaje,Int32 id_alumno)
        {
            return (from rcerp in DataContext.RubricaControlEvaluacionResultadoCapstone 
                    join rce in DataContext.RubricaControlEvaluacion  on rcerp.IdRubricaControlEvaluacion equals rce.IdRubricaControlEvaluacion
                    where rcerp.IdRubricaControlPregunta == id_puntaje && rce.idAlumno == id_alumno


                    select new StudentReportModel()
                    {
                        
                        idRubricaControlEvaluacion = rcerp.IdRubricaControlEvaluacion,
                        idPuntaje = rcerp.puntaje

                    }
           ).FirstOrDefault();

        }
        public static StudentReportModel GetDatosAlumno(AbetEntities DataContext, Int32 id_alumno)
        {
            return (from rce in DataContext.RubricaControlEvaluacion
                    join ase in DataContext.AlumnoSeccion on rce.idAlumnoSeccion equals ase.IdAlumnoSeccion
                    join pa in DataContext.ProyectoAcademico  on ase.IdProyectoAcademico equals pa.IdProyectoAcademico
                    join a in DataContext.Alumno  on rce.idAlumno equals a.IdAlumno
                    where rce.idAlumno == id_alumno


                    select new StudentReportModel()
                    {

                        CodigoAlumno = a.Codigo,
                        NombreAlumno = a.Nombres,
                        ApellidoAlum = a.Apellidos,
                        NombreProyecto = pa.Nombre,
                        obs = rce.observacion

                    }
           ).FirstOrDefault();

        }
    }
    
    public class ExcelRubricCource
    {
        public static List<XLWorkbook> GenerateExcelRubricCourse(AbetEntities DataContext, int idCurso, int idSeccion, string CodCurso, string path, CargarDatosContext ctx , int rubricType)
        {
            int contador = 0;

            var context = ctx.context;
            string[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            //int celdaInicial = 18;

            var alumnos = GetAlumnosSeccionId(DataContext, idCurso,idSeccion, rubricType, ctx.session.GetSubModalidadPeriodoAcademicoId().Value);

            var ExcelRubric = new List<XLWorkbook>();

            

            foreach (var idAlumno in alumnos.Select(x => x.idAlumno))
            {
                decimal puntajeTOTAL = 0;
                var objExcel = new XLWorkbook(path);
                var worksheet = objExcel.Worksheet(1);
                var datosAlumno = GetCodigoAlumno(DataContext,idAlumno);
                string nombreHoja = datosAlumno.CodigoAlumno;
                worksheet.Name = nombreHoja;

                var i = 4;
                var idRubrica = alumnos.Select(x => x.idRubricaControl).FirstOrDefault();
                //var docente = alumnos.Select(x => x.idDocenteCalificador).FirstOrDefault();
                var nivelesAceptacion = GetIdRubricaNivelAceptacion(DataContext, idRubrica);
                foreach (var nivelAcep in nivelesAceptacion.Select(x => x.idRubricaControlNivelAceptacion))
                {
                    var dataNivelAcep = GetNivelAcepData(DataContext, nivelAcep);
                    var descrip = dataNivelAcep.descriptionEspa;
                    //var peso = dataNivelAcep.peso + "%";
                    var celda_DESCRIP = "" + alphabetArray[i] + "" + 16 + "";
                    var celda_DESCRIP_SIGU = "" + alphabetArray[i] + "" + 17 + "";
                    //var celda_PESO = "" + alphabetArray[i] + "" + 17 + "";
                    worksheet.Cell(celda_DESCRIP).Value = descrip;
                    worksheet.Cell(celda_DESCRIP).Style.Fill.SetBackgroundColor(XLColor.Red);
                    worksheet.Range(celda_DESCRIP,celda_DESCRIP_SIGU).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(celda_DESCRIP, celda_DESCRIP_SIGU).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Range(celda_DESCRIP, celda_DESCRIP_SIGU).Merge().Style.Alignment.WrapText = true;
                    //worksheet.Cell(celda_PESO).Value = peso;
                    //worksheet.Cell(celda_PESO).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    //worksheet.Cell(celda_PESO).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    //worksheet.Cell(celda_PESO).Style.Alignment.WrapText = true;

                    i++;

                }

                var celda_Puntaje_Nombre = "" + alphabetArray[i] + "" + 16 + "";
                var celda_Puntaje_Nombre_Sig = "" + alphabetArray[i] + "" + 17 + "";
                worksheet.Cell(celda_Puntaje_Nombre).Value = "NOTA";
                worksheet.Cell(celda_Puntaje_Nombre).Style.Fill.SetBackgroundColor(XLColor.Red);
                worksheet.Range(celda_Puntaje_Nombre, celda_Puntaje_Nombre_Sig).Merge();
                worksheet.Range(celda_Puntaje_Nombre, celda_Puntaje_Nombre_Sig).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(celda_Puntaje_Nombre,celda_Puntaje_Nombre_Sig).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                worksheet.Range(celda_Puntaje_Nombre,celda_Puntaje_Nombre_Sig).Merge().Style.Alignment.WrapText = true;

                worksheet.Cell("D5").Value = datosAlumno.NombreAlumno + " " + datosAlumno.ApellidoAlum;
                worksheet.Cell("D5").Style.Alignment.WrapText = true;
                worksheet.Cell("G5").Value = datosAlumno.CodigoAlumno;

                var dataCurso = GetDataCurso(DataContext, idCurso);
                worksheet.Cell("D7").Value = datosAlumno.NombreDocente + " " + datosAlumno.ApellidoDocente;
                worksheet.Cell("D7").Style.Alignment.WrapText = true;
                worksheet.Cell("G9").Value = datosAlumno.SeccionCurso;
                worksheet.Cell("G7").Value = dataCurso.NombreCursoE;
                worksheet.Cell("G7").Style.Alignment.WrapText = true;


                var idPreguntas = GetIdPreguntas(DataContext, idRubrica);

                    
                int celdaInicial = 18;
                var celda = 18;
                foreach (var id_pregunta in idPreguntas.Select(x => x.idRubricaControlPregunta))
                {
                    var pregunta = GetPregunta(DataContext, id_pregunta);
                       
                    worksheet.Cell("C"+ celdaInicial).Value = pregunta.preguntaEspanol;
                    worksheet.Range("C" + celdaInicial, "C" + (celdaInicial + 1)).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range("C" + celdaInicial, "C" + (celdaInicial + 1)).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Range("C" + celdaInicial, "C" + (celdaInicial + 1)).Merge().Style.Alignment.WrapText = true;
                    celdaInicial++;

                    var z = 4;
                    var add = 1;
                    var idCriterios = GetIdCriterios(DataContext, id_pregunta);
                    foreach (var id_criterio in idCriterios.Select(x => x.idRubricaControlCriterio))
                    {
                        var criterio = GetCriterio(DataContext, id_criterio);
                        var celda_CRITERIO = "" + alphabetArray[z] + "" + celda + "";
                        worksheet.Cell(celda_CRITERIO).Value = criterio.descriptionEspanol;
                        worksheet.Cell(celda_CRITERIO).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(celda_CRITERIO).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        worksheet.Cell(celda_CRITERIO).Style.Alignment.WrapText = true;

                        var idPuntaje = GetIdPuntaje(DataContext, id_pregunta, idAlumno);
                        foreach (var id_puntaje in idPuntaje.Select(x => x.idRubricaControlEvaluacionResultado))
                        {
                            var puntaje = GetPuntaje(DataContext, id_puntaje);
                            var idCri = puntaje.idRubricaControlCriterio;
                            if ( idCri == id_criterio)
                            {
                                

                                var celda_Puntaje = "" + alphabetArray[z] + "" + (celda+add) + "";
                                worksheet.Cell(celda_Puntaje).Value = puntaje.idPuntaje;
                                worksheet.Cell(celda_Puntaje).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(celda_Puntaje).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                                worksheet.Cell(celda_Puntaje).Style.Alignment.WrapText = true;

                                var celda_Final_Puntaje = "" + alphabetArray[i] + "" + (celda) + "";
                                worksheet.Cell(celda_Final_Puntaje).Value = puntaje.idPuntaje;
                                worksheet.Cell(celda_Final_Puntaje).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(celda_Final_Puntaje).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                                worksheet.Cell(celda_Final_Puntaje).Style.Alignment.WrapText = true;

                                decimal punt = puntaje.idPuntaje;
                                puntajeTOTAL += punt;
                            }
                            
                            var celda_Final_Puntaje_anterior = "" + alphabetArray[z] + "" + (celda) + "";
                            worksheet.Cell(celda_Final_Puntaje_anterior).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(celda_Final_Puntaje_anterior).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            worksheet.Cell(celda_Final_Puntaje_anterior).Style.Alignment.WrapText = true;

                            var celda_Final_Puntajes = "" + alphabetArray[z] + "" + (celda + add) + "";
                            worksheet.Cell(celda_Final_Puntajes).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(celda_Final_Puntajes).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            worksheet.Cell(celda_Final_Puntajes).Style.Alignment.WrapText = true;


                        }
                        z++;
                    }
                    celdaInicial++;
                    celda++;
                    celda++;
                }


                worksheet.Cell("D9").Value = puntajeTOTAL;

                ExcelRubric.Add(objExcel);


                contador++;
            }

            return ExcelRubric;
        }

        private static List<StudentReportModel> GetAlumnosSeccionId(AbetEntities DataContext, int idCurso, int idSeccion, int rubricType, int submodaPA)
        {
            bool isFinal = rubricType == 1 ? false : true;    
            return(from sec in  DataContext.RubricaControlEvaluacion
                   join rc in DataContext.RubricaControl on sec.idRubricaControl equals rc.idRubricaControl
                   where sec.idSeccion == idSeccion && rc.IsFinal == isFinal && rc.IdSubModalidadPeriodoAcademico == submodaPA

                   select new StudentReportModel()
                    {
                        idRubricaControlEvaluacion = sec.IdRubricaControlEvaluacion,
                        idRubricaControl = sec.idRubricaControl,
                        idAlumno = sec.idAlumno,
                        AlumnoSeccionId = sec.idSeccion,
                        idDocenteCalificador = sec.idDocenteCalificador
                       
                    }
            ).ToList();
        }

        public static List<StudentReportModel> GetIdRubricaNivelAceptacion(AbetEntities DataContext,
                             Int32 IdRubrica)
        {
            return (from rcna in DataContext.RubricaControlNivelAceptacion
                    where rcna.idRubricaControl == IdRubrica

                    select new StudentReportModel()
                    {
                        idRubricaControlNivelAceptacion = rcna.idRubricaControlNivelAceptacion,
                        idRubricaControl = rcna.idRubricaControl,
                        peso = rcna.peso,
                        descriptionEspa = rcna.descripcionEspanol

                    }
           ).ToList();

        }

        public static StudentReportModel GetNivelAcepData(AbetEntities DataContext,
                              Int32 idnivel)
        {
            return (from rcna in DataContext.RubricaControlNivelAceptacion
                    where rcna.idRubricaControlNivelAceptacion == idnivel

                    select new StudentReportModel()
                    {
                        idRubricaControlNivelAceptacion = rcna.idRubricaControlNivelAceptacion,
                        idRubricaControl = rcna.idRubricaControl,
                        peso = rcna.peso,
                        descriptionEspa = rcna.descripcionEspanol

                    }
           ).FirstOrDefault();

        }

        public static StudentReportModel GetCodigoAlumno(AbetEntities DataContext,
                              Int32 idAlumno)
        {
            return (from rce in DataContext.RubricaControlEvaluacion 
                    join a in DataContext.Alumno on rce.idAlumno equals a.IdAlumno
                    join d in DataContext.Docente on rce.idDocenteCalificador equals d.IdDocente
                    join s in DataContext.Seccion  on rce.idSeccion equals s.IdSeccion
                    where rce.idAlumno == idAlumno

                    select new StudentReportModel()
                    {
                        idAlumno = rce.idAlumno,
                        CodigoAlumno = a.Codigo,
                        NombreAlumno = a.Nombres,
                        ApellidoAlum = a.Apellidos,
                        idCarreraEgreso = a.IdCarreraEgreso,
                        idDocenteCalificador = rce.idDocenteCalificador,
                        NombreDocente = d.Nombres,
                        ApellidoDocente = d.Apellidos,
                        codigoDocente = d.Codigo,
                        SeccionCurso = s.Codigo,
                        SedeSeccion = s.IdSede

                    }
           ).FirstOrDefault();

        }

        public static StudentReportModel GetDataCurso(AbetEntities DataContext,
                              Int32 idCurso)
        {
            return (from c in DataContext.Curso
                    where c.IdCurso == idCurso

                    select new StudentReportModel()
                    {
                        CodigoCurso = c.Codigo,
                        NombreCursoE = c.NombreEspanol,
                        NombreCursoI = c.NombreIngles

                    }
           ).FirstOrDefault();

        }
        private static List<StudentReportModel> GetIdPreguntas(AbetEntities DataContext, int idRubrica)
        {
            return (from rcp in DataContext.RubricaControlPregunta
                    where rcp.idRubricaControl == idRubrica

                    select new StudentReportModel()
                    {
                        idRubricaControlPregunta = rcp.idRubricaControlPregunta 

                    }
            ).ToList();
        }

        public static StudentReportModel GetPregunta(AbetEntities DataContext,
                             Int32 idPregunta)
        {
            return (from rcp in DataContext.RubricaControlPregunta
                    where rcp.idRubricaControlPregunta == idPregunta

                    select new StudentReportModel()
                    {

                        preguntaEspanol = rcp.preguntaEspanol,
                        preguntaIngles = rcp.preguntaIngles,
                        puntajeMaximo = rcp.puntajeMaximo

                    }
           ).FirstOrDefault();

        }

        private static List<StudentReportModel> GetIdCriterios(AbetEntities DataContext, int id_pregunta)
        {
            return (from rcc in DataContext.RubricaControlCriterio
                    where rcc.idRubricaControlPregunta == id_pregunta

                    select new StudentReportModel()
                    {
                        idRubricaControlNivelAceptacion = rcc.idRubricaControlNivelAceptacion,
                        idRubricaControlCriterio = rcc.idRubricaControlCriterio

                    }
            ).ToList();
        }
        public static StudentReportModel GetCriterio(AbetEntities DataContext,
                              Int32 id_criterio)
        {
            return (from rcc in DataContext.RubricaControlCriterio
                    where rcc.idRubricaControlCriterio == id_criterio

                    select new StudentReportModel()
                    {
                        descriptionEspanol = rcc.descripcionEspanol

                    }
           ).FirstOrDefault();

        }

        private static List<StudentReportModel> GetIdPuntaje(AbetEntities DataContext, int id_pregunta,int idAlumno)
        {
            return (from rcp in DataContext.RubricaControlPregunta
                    join rc in DataContext.RubricaControl on rcp.idRubricaControl equals rc.idRubricaControl
                    join rceva in DataContext.RubricaControlEvaluacion on rc.idRubricaControl equals rceva.idRubricaControl
                    join rcer in DataContext.RubricaControlEvaluacionResultado  on rceva.IdRubricaControlEvaluacion equals rcer.IdRubricaControlEvaluacion
                    where rcp.idRubricaControlPregunta == id_pregunta && rceva.idAlumno == idAlumno

                    select new StudentReportModel()
                    {
                        idRubricaControlEvaluacionResultado = rcer.idRubricaControlEvaluacionResultado

                    }
            ).ToList();
        }


        public static StudentReportModel GetPuntaje(AbetEntities DataContext,
                              Int32 id_puntaje)
        {
            return (from rcer in DataContext.RubricaControlEvaluacionResultado
                    where rcer.idRubricaControlEvaluacionResultado == id_puntaje

                    select new StudentReportModel()
                    {
                        idRubricaControlCriterio = rcer.IdRubricaControlCriterio,
                        idRubricaControlEvaluacion = rcer.IdRubricaControlEvaluacion,
                        idPuntaje = rcer.puntaje

                    }
           ).FirstOrDefault();

        }


    }

    public class ExcelSeccionByCourse
    {
        public static XLWorkbook GenerateExcelSeccionByCourse(CargarDatosContext ctx, int idCurso, List<GetListPartialRubricCapstone_Result> lstNotas, string path)
        {
           
            var context = ctx.context;
            string[] alphabetArray = { string.Empty, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            //int celdaInicial = 18;
            
            var objExcel = new XLWorkbook(path);
            var i = 2;
            foreach ( var dato in lstNotas)
            {
                var CodCurso = dato.CodCurso;
                var CodSeccion = dato.CodSeccion;
                var codAlumno = dato.codigoAlumno;
                var NombreAlumno = dato.NombreAlumno;
                var Nota = dato.Nota;

                var worksheet = objExcel.Worksheet(1);
                string nombreHoja = CodCurso;
                worksheet.Name = nombreHoja;
                var celdaA = "" + alphabetArray[1] + "" + i + "";
                worksheet.Cell(celdaA).Value = CodCurso;
                var celdaB = "" + alphabetArray[2] + "" + i + "";
                worksheet.Cell(celdaB).Value = CodSeccion;
                var celdaC = "" + alphabetArray[3] + "" + i + "";
                worksheet.Cell(celdaC).Value = codAlumno;
                var celdaD = "" + alphabetArray[4] + "" + i + "";
                worksheet.Cell(celdaD).Value = NombreAlumno;
                var celdaE = "" + alphabetArray[5] + "" + i + "";
                worksheet.Cell(celdaE).Value = Nota;

                i++;
                
            }

            return (objExcel);
        }

     }

}


