﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Rubric.RubricCourse
{
    public class RubricCourseLogic
    {
        #region RubricCourse

        public static bool SaveStudent(string codigo, string nombre, string apellido, int idCurso, int idCarrera, int idSection, int idDocente)
        {
            try
            {
                using (var context = new AbetEntities())
                {
                    var student = context.Alumno.Where(x => x.Codigo == codigo).Select(x => x).FirstOrDefault();
                    if (student == null)
                    {
                        var insertStudent = new Alumno();
                        insertStudent.Codigo = codigo;
                        insertStudent.Nombres = nombre;
                        insertStudent.Apellidos = apellido;
                        insertStudent.IdCarreraEgreso = idCarrera;
                        insertStudent.IdDocenteQueInscribio = idDocente;
                        context.Alumno.Add(insertStudent);
                        context.SaveChanges();
                        student = insertStudent;
                    }

                    var enrolledStudent = (from am in context.AlumnoMatriculado
                                           join als in context.AlumnoSeccion on am.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                                           where am.IdAlumno == student.IdAlumno && als.IdSeccion == idSection
                                           select am).Count();

                    var dataAM = (from am in context.AlumnoMatriculado
                                  join als in context.AlumnoSeccion on am.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                                  where als.IdSeccion == idSection
                                  select new
                                  {
                                      IdLogCarga = am.IdLogCarga,
                                      IdSubModalidadPeriodoAcademico = am.IdSubModalidadPeriodoAcademico,
                                      IdLogCargaAls = als.IdLogCarga
                                  }
                                      ).FirstOrDefault();

                    var section = context.Seccion.Where(x => x.IdSeccion == idSection).Select(x => x).FirstOrDefault();
                    if (enrolledStudent > 0)
                    {
                        return false;
                    }
                    else
                    {
                        #region Add AlumnoMatriculado
                        var alumnoMatriculado = new AlumnoMatriculado();
                        alumnoMatriculado.IdAlumno = student.IdAlumno;
                        alumnoMatriculado.IdSede = section.IdSede;
                        alumnoMatriculado.IdCarrera = idCarrera;
                        alumnoMatriculado.EstadoMatricula = "MRE";
                        alumnoMatriculado.IdLogCarga = dataAM.IdLogCarga;
                        alumnoMatriculado.IdSubModalidadPeriodoAcademico = dataAM.IdSubModalidadPeriodoAcademico;
                        alumnoMatriculado.IdDocenteQueInscribio = idDocente;
                        context.AlumnoMatriculado.Add(alumnoMatriculado);
                        context.SaveChanges();
                        #endregion

                        #region Add AlumnoSeccion
                        int alumnoMatriculadoId = alumnoMatriculado.IdAlumnoMatriculado;
                        var alumnoSeccion = new AlumnoSeccion();
                        alumnoSeccion.IdAlumnoMatriculado = alumnoMatriculadoId;
                        alumnoSeccion.IdSeccion = idSection;
                        alumnoSeccion.EsDelegado = false;
                        alumnoSeccion.IdLogCarga = dataAM.IdLogCargaAls;
                        //Falta agregar el idDocente y el campo en la tabla AlumnoSeccion
                        context.AlumnoSeccion.Add(alumnoSeccion);
                        context.SaveChanges();
                        #endregion

                        return true;

                    }
                }
                //var firstLevel = new ReunionProfesorConfiguracion
                //{
                //    IdSubModalidadPeriodoAcademicoModulo = subModalityAcademicPeriodModuleId,
                //    Nivel = (int)ORGANIZATION_CHART_LEVELS.FIRST_LEVEL,
                //    PlazoEnDias = FirstLevelDueDate,
                //    Semanas = FirstLevelWeeks,
                //    FechaCreacion = dateFormatted.ToDateTime(),
                //    FechaActualizacion = dateFormatted.ToDateTime()
                //};

                //var secondLevel = new ReunionProfesorConfiguracion
                //{
                //    IdSubModalidadPeriodoAcademicoModulo = subModalityAcademicPeriodModuleId,
                //    Nivel = (int)ORGANIZATION_CHART_LEVELS.SECOND_LEVEL,
                //    PlazoEnDias = SecondLevelDueDate,
                //    Semanas = SecondLevelWeeks,
                //    FechaCreacion = dateFormatted.ToDateTime(),
                //    FechaActualizacion = dateFormatted.ToDateTime()
                //};

                //AbetEntities.ReunionProfesorConfiguracion.AddRange(new List<ReunionProfesorConfiguracion> { firstLevel, secondLevel });
                //var result = await AbetEntities.SaveChangesAsync();
                //if (result == 2)
                //    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #endregion

        #region RubricControl

        public static int CreateRubricControl(int idCurso, int idDocente, int submodalidaPA, int rubricType)
        {

            var now = DateTime.Now;
            //var dateFormatted = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                using (var context = new AbetEntities())
                {
                    var existRubric = context.RubricaControl.Where(x => x.IdCurso == idCurso && x.IsDeleted == false).FirstOrDefault();
                    if (existRubric == null)
                    {
                        var createRubricControl = new RubricaControl();
                        createRubricControl.IdCurso = idCurso;
                        createRubricControl.IdDocente = idDocente;
                        createRubricControl.fecha = now;
                        createRubricControl.IdCurso = idCurso;
                        createRubricControl.IsDeleted = false;
                        createRubricControl.IdSubModalidadPeriodoAcademico = submodalidaPA;
                        if (rubricType == 1)
                        {
                            createRubricControl.IsFinal = false;
                        }
                        else {
                            createRubricControl.IsFinal = true;
                        }
                        context.RubricaControl.Add(createRubricControl);
                        context.SaveChanges();

                        return createRubricControl.idRubricaControl;

                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CreateRubriControlAcceptanceLevel(int idRubricControl, string[] dataAcceptanceLevel)
        {
            try
            {
                using (var context = new AbetEntities())
                {
                    var createRubriControlAcceptanceLevel = new RubricaControlNivelAceptacion();
                    createRubriControlAcceptanceLevel.idRubricaControl = idRubricControl;
                    createRubriControlAcceptanceLevel.descripcionEspanol = dataAcceptanceLevel[0];
                    createRubriControlAcceptanceLevel.peso = Convert.ToInt32(dataAcceptanceLevel[1]);
                    context.RubricaControlNivelAceptacion.Add(createRubriControlAcceptanceLevel);
                    context.SaveChanges();
                    return createRubriControlAcceptanceLevel.idRubricaControlNivelAceptacion;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static bool CreateRubricControlPregunta(string[][] dataSettingRubric, int idRubricControl, List<int> lstIdRubricControlAcceptanceLevel)
        {
            try
            {
                using (var context = new AbetEntities())
                {
                    for (int i = 0; i < dataSettingRubric.Length; i++)
                    {                        
                        #region RubricControlPregunta
                        var createRubricControlPregunta = new RubricaControlPregunta();
                        createRubricControlPregunta.idRubricaControl = idRubricControl;
                        createRubricControlPregunta.preguntaEspanol = dataSettingRubric[i].First();
                        createRubricControlPregunta.puntajeMaximo = Convert.ToDecimal(dataSettingRubric[i].Last());
                        context.RubricaControlPregunta.Add(createRubricControlPregunta);
                        context.SaveChanges();
                        #endregion
                        #region RubricControlCriterio
                   
                     
                        for (int j = 0; j < lstIdRubricControlAcceptanceLevel.Count; j++)
                        {
                            var createRubricControlCriterio = new RubricaControlCriterio();
                            createRubricControlCriterio.idRubricaControlPregunta = createRubricControlPregunta.idRubricaControlPregunta;
                            createRubricControlCriterio.idRubricaControlNivelAceptacion = lstIdRubricControlAcceptanceLevel[j];
                            createRubricControlCriterio.descripcionEspanol = dataSettingRubric[i][j + 1];
                            context.RubricaControlCriterio.Add(createRubricControlCriterio);
                            context.SaveChanges();
                        }
                         
                        #endregion
                    
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateNewUser(int idDocente,string password,int subModalidadPeriodAcadId)
        {
            try
            {
                using (var context = new AbetEntities())
                {
                    var docente = context.Docente.Where(x => x.IdDocente == idDocente).FirstOrDefault();
                    var newUser = new Usuario();

                    var userRol = new RolUsuario();
                    var userRol2 = new RolUsuario();
                    var listRol = new List<RolUsuario>();

                    newUser.IdDocente = docente.IdDocente;
                    newUser.Nombres = docente.Nombres;
                    newUser.Apellidos = docente.Apellidos;
                    newUser.Codigo = docente.Codigo;
                    newUser.Password = password;
                    newUser.FechaCreacion = DateTime.Now;
                    newUser.FechaAlta = DateTime.Now;
                    newUser.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    newUser.IdEscuelaDefecto = 1;
                    context.Usuario.Add(newUser);
                    context.SaveChanges();
                    
                    userRol.IdSubModalidadPeriodoAcademico = subModalidadPeriodAcadId;
                    userRol.IdUsuario = newUser.IdUsuario;
                    userRol.FechaCreacion = DateTime.Now;
                    userRol.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    userRol.IdEscuela = 1;
                    userRol.IdRol = 3;

                    listRol.Add(userRol);
                   
                    userRol2.IdSubModalidadPeriodoAcademico = subModalidadPeriodAcadId;
                    userRol2.IdUsuario = newUser.IdUsuario;
                    userRol2.FechaCreacion = DateTime.Now;
                    userRol2.Estado = ConstantHelpers.ESTADO.ACTIVO;
                    userRol2.IdEscuela = 1;
                    userRol2.IdRol = 10;
                    
                    listRol.Add(userRol2);

                    context.RolUsuario.AddRange(listRol);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CreateRubricControlEvaluation(string[][] arrayData, int idRubricaControl, int idAlumno, int idDocenteCalificador, int idSeccion) {
            try
            {
                using (var context = new AbetEntities())
                {
                    var idAlumnoSeccion = from als in context.AlumnoSeccion
                                          join alm in context.AlumnoMatriculado on als.IdAlumnoMatriculado equals alm.IdAlumnoMatriculado
                                          where alm.IdAlumno == idAlumno && als.IdSeccion == idSeccion
                                          select als.IdAlumnoSeccion;

                    var createRubricControlEvaluation = new RubricaControlEvaluacion();
                    createRubricControlEvaluation.idRubricaControl = idRubricaControl;
                    createRubricControlEvaluation.idAlumno = idAlumno;
                    createRubricControlEvaluation.idDocenteCalificador = Convert.ToInt32(idDocenteCalificador);
                    createRubricControlEvaluation.idSeccion = Convert.ToInt32(idSeccion);
                    createRubricControlEvaluation.fechaEvaluacion = DateTime.Now;
                    createRubricControlEvaluation.idAlumnoSeccion = idAlumnoSeccion.ToInteger();
                    context.RubricaControlEvaluacion.Add(createRubricControlEvaluation);
                    context.SaveChanges();

                    for (int i = 0; i < arrayData.Count(); i++)
                    {
                        var createRubricControlEvaluationResult = new RubricaControlEvaluacionResultado();
                        createRubricControlEvaluationResult.IdRubricaControlEvaluacion = createRubricControlEvaluation.IdRubricaControlEvaluacion;
                        createRubricControlEvaluationResult.IdRubricaControlCriterio = Convert.ToInt32(arrayData[i][0]);
                        createRubricControlEvaluationResult.puntaje = Convert.ToDecimal(arrayData[i][1]);
                        context.RubricaControlEvaluacionResultado.Add(createRubricControlEvaluationResult);
                        context.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void CreateRubricCapstoneEvaluation(string[][] arrayData, string[][] arrayData2, int idRubricaControl, int idProyectoAcademico, int idCurso, int docenteID, string observation) {
            try
            {
                using (var context = new AbetEntities())
                {
                    var lstAlumno = (from al in context.Alumno
                                     join alm in context.AlumnoMatriculado on al.IdAlumno equals alm.IdAlumno
                                     join als in context.AlumnoSeccion on alm.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                                     where als.IdProyectoAcademico == idProyectoAcademico
                                     select al).ToList();

                    var alumnoSeccion = (from als in context.AlumnoSeccion
                                          join pa in context.ProyectoAcademico on als.IdProyectoAcademico equals pa.IdProyectoAcademico
                                          where pa.IdProyectoAcademico == idProyectoAcademico
                                          select als).ToList();                   

                   
                 
                    for (int i = 0; i < lstAlumno.Count(); i++)
                    {
                        var createRubricControlEvaluation = new RubricaControlEvaluacion();
                        createRubricControlEvaluation.idRubricaControl = idRubricaControl;
                        createRubricControlEvaluation.idAlumno = lstAlumno[i].IdAlumno;
                        createRubricControlEvaluation.idDocenteCalificador = docenteID;
                        createRubricControlEvaluation.idSeccion = alumnoSeccion[i].IdSeccion ;
                        createRubricControlEvaluation.fechaEvaluacion = DateTime.Now;
                        createRubricControlEvaluation.idAlumnoSeccion = alumnoSeccion[i].IdAlumnoSeccion;
                        createRubricControlEvaluation.observacion = observation;
                        context.RubricaControlEvaluacion.Add(createRubricControlEvaluation);
                        context.SaveChanges();
                        if (i == 0)
                        {
                            for (int j = 0; j < arrayData.Count(); j++)
                            {
                                var createRubricControlEvaluationResultCapstone = new RubricaControlEvaluacionResultadoCapstone();
                                createRubricControlEvaluationResultCapstone.IdRubricaControlEvaluacion = createRubricControlEvaluation.IdRubricaControlEvaluacion;
                                createRubricControlEvaluationResultCapstone.IdRubricaControlPregunta = Convert.ToInt32(arrayData[j][0]);
                                createRubricControlEvaluationResultCapstone.puntaje = Convert.ToDecimal(arrayData[j][1]);
                                context.RubricaControlEvaluacionResultadoCapstone.Add(createRubricControlEvaluationResultCapstone);
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            for (int j = 0; j < arrayData2.Count(); j++)
                            {
                                var createRubricControlEvaluationResultCapstone = new RubricaControlEvaluacionResultadoCapstone();
                                createRubricControlEvaluationResultCapstone.IdRubricaControlEvaluacion = createRubricControlEvaluation.IdRubricaControlEvaluacion;
                                createRubricControlEvaluationResultCapstone.IdRubricaControlPregunta = Convert.ToInt32(arrayData2[j][0]);
                                createRubricControlEvaluationResultCapstone.puntaje = Convert.ToDecimal(arrayData2[j][1]);
                                context.RubricaControlEvaluacionResultadoCapstone.Add(createRubricControlEvaluationResultCapstone);
                                context.SaveChanges();
                            }

                        }
                        
                    }
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void EditRubricCapstoneEvaluation(List<RubricaControlEvaluacionResultadoCapstone> notas, string[][] arrayData, string[][] arrayData2, int idRubricaControl, int idProyectoAcademico, int idCurso, int docenteID, string observation)
        {
            try
            {
                using (var context = new AbetEntities())
                {
                    var lstAlumno = (from al in context.Alumno
                                     join alm in context.AlumnoMatriculado on al.IdAlumno equals alm.IdAlumno
                                     join als in context.AlumnoSeccion on alm.IdAlumnoMatriculado equals als.IdAlumnoMatriculado
                                     where als.IdProyectoAcademico == idProyectoAcademico
                                     select al).ToList();

                    var alumnoSeccion = (from als in context.AlumnoSeccion
                                         join pa in context.ProyectoAcademico on als.IdProyectoAcademico equals pa.IdProyectoAcademico
                                         where pa.IdProyectoAcademico == idProyectoAcademico
                                         select als).ToList();



                    for (int i = 0; i < lstAlumno.Count(); i++)
                    {
                        var IdRubricaControlEvaluacion = notas.Where(x => x.RubricaControlEvaluacion.idAlumno == lstAlumno[i].IdAlumno).FirstOrDefault().IdRubricaControlEvaluacion;
                        RubricaControlEvaluacion createRubricControlEvaluation = context.RubricaControlEvaluacion.Where(x => x.IdRubricaControlEvaluacion == IdRubricaControlEvaluacion).FirstOrDefault();
                        createRubricControlEvaluation.observacion = observation;                        
                        context.SaveChanges();

                       
                        if (i == 0)
                        {
                            for (int j = 0; j < arrayData.Count(); j++)
                            {
                                var IdRubricaControlEvaluacionResultadoCapstone = notas.Where(x => x.IdRubricaControlEvaluacion == createRubricControlEvaluation.IdRubricaControlEvaluacion
                                                                                               && x.IdRubricaControlPregunta == Convert.ToInt32(arrayData[j][0])).FirstOrDefault().idRubricaControlEvaluacionResultadoCapstone;
                                RubricaControlEvaluacionResultadoCapstone createRubricControlEvaluationResultCapstone = context.RubricaControlEvaluacionResultadoCapstone.Where(x => x.idRubricaControlEvaluacionResultadoCapstone == IdRubricaControlEvaluacionResultadoCapstone).FirstOrDefault();
                                createRubricControlEvaluationResultCapstone.puntaje = Convert.ToDecimal(arrayData[j][1]);
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            for (int j = 0; j < arrayData2.Count(); j++)
                            {
                                var IdRubricaControlEvaluacionResultadoCapstone = notas.Where(x => x.IdRubricaControlEvaluacion == createRubricControlEvaluation.IdRubricaControlEvaluacion
                                                                                               && x.IdRubricaControlPregunta == Convert.ToInt32(arrayData2[j][0])).FirstOrDefault().idRubricaControlEvaluacionResultadoCapstone;
                                RubricaControlEvaluacionResultadoCapstone createRubricControlEvaluationResultCapstone = context.RubricaControlEvaluacionResultadoCapstone.Where(x => x.idRubricaControlEvaluacionResultadoCapstone == IdRubricaControlEvaluacionResultadoCapstone).FirstOrDefault();
                                createRubricControlEvaluationResultCapstone.puntaje = Convert.ToDecimal(arrayData2[j][1]);
                                context.SaveChanges();
                            }

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region MailRubric
        public bool SendMailRubric(String asunto, String msjCuerpo, String email)
        {
            try
            {
                var abetEmailServerHost = AppSettingsHelper.GetAppSettings("AbetEmailServerHost", "10.10.3.119");
                var abetEmailServerPort = AppSettingsHelper.GetAppSettings("AbetEmailServerPort", "25");
                var abetEmailServerUserCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserCredential", "usreiscabet@upc.edu.pe");
                var abetEmailServerUserPassword = AppSettingsHelper.GetAppSettings("AbetEmailServerUserPassword", "escuelasistemas");
                var abetEmailServerSSLEnabled = AppSettingsHelper.GetAppSettings("AbetEmailServerSSLEnabled", "false");
                var abetEmailServerUserDefaultCredential = AppSettingsHelper.GetAppSettings("AbetEmailServerUserDefaultCredential", "false");

                SmtpClient abetEmailServer = new SmtpClient
                {
                    Host = abetEmailServerHost,
                    Port = int.Parse(abetEmailServerPort),
                    EnableSsl = abetEmailServerSSLEnabled.ToBoolean(),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = abetEmailServerUserDefaultCredential.ToBoolean(),
                    Credentials = new System.Net.NetworkCredential(abetEmailServerUserCredential, abetEmailServerUserPassword)
                };

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(abetEmailServerUserCredential)
                };
                mail.To.Add(email);
                mail.Subject = asunto;
                mail.Body = msjCuerpo;
                mail.IsBodyHtml = true;
                abetEmailServer.Send(mail);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
            return true;
        }
        public String GetHtmlEmailRubric(String curso, String url, String urlHeader, String urlFooter,String usuario,String psw)
        {

            return
            "<!DOCTYPE html>" +
            "<html lang='es'>" +
                "<head>" +
                    "<meta charset='utf-8'>" +
                    "<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
                    "<meta name='viewport' content='width=device-width, initial-scale=1'>" +
                    "<title>'UPC-SA -- Evaluacion de Rubrica --'</title>" +

                "</head>" +
                "<body style = 'font-family: Calibri; font-size:14.5px;' >" +
                    "<div align='center' style='text-align:center;'>" +
                    "<table width='787' border='1' cellspacing='0' cellpadding='0' style='width:472.5pt;border:1pt solid white;'>" +
                    "<tbody><tr>" +
                    "<td style='padding:0;border:1pt solid #CCCCCC;'>" +
                    "<div align='center' style='text-align:center;'>" +
                    "<table width='787' border='0' cellspacing='0' cellpadding='0' style='width:472.5pt;background-color:white;'>" +
                    "<tbody><tr height='31' style='height:18.75pt;'>" +
                    "<td style='height:18.75pt;padding:0;'></td>" +
                    "<td colspan='4' style='height:18.75pt;padding:0;'>" +
                    "<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Arial,sans-serif' size='2' color='#3A3A3A' style='font-family: Arial, sans-serif, serif, EmojiFont;'><span style='font-size:8.5pt;'><b>Por favor, no responder este mensaje.</b></span></font></span></font></div>" +
                    "</td>" +
                    "</tr>" +
                    "<tr height='31' style='height:18.75pt;'>" +
                    "<td width='50' style='width:30pt;height:18.75pt;padding:0;'></td>" +
                    "<td width='412' style='width:247.5pt;height:18.75pt;padding:0;'></td>" +
                    "<td width='25' style='width:15pt;height:18.75pt;padding:0;'></td>" +

                    "</tr>" +
                    "<tr>" +
                    "<td colspan='5' style='padding:0;'>" +
                    "<img src='" + urlHeader + "'>" +
                    "</td>" +
                    "</tr>" +
                    "<tr height='25' style='height:15pt;'>" +
                    "<td colspan='5' style='height:15pt;padding:0;'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td colspan='5' style='padding:0;'>" +
                    "<hr width='88%' style='background:gray'/>" +
                    "</td>" +
                    "</tr>" +
                    "<tr height='25' style='height:15pt;'>" +
                    "<td colspan='5' style='height:15pt;padding:0;'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td style='padding:0;'></td>" +
                    "<td colspan='3' style='padding:0;'>" +
                    "<div style='margin-top:0;margin-bottom:0;'>" +
                    "<font face='Arial,sans-serif' size='2' color='#434343' style='font-family: Arial, sans-serif, serif, EmojiFont;'>" +
                    "<div style='text-align: justify;'>"+
                    "<p>Estimado Docente,</p>"+
                    "<p>Le informamos que la rúbrica para evaluar a los alumnos del curso " + curso + " ya está configurada. <br>Ingrese al link adjunto, sus credenciales son:</p>"+
                    "<p>Usuario:"+ usuario +"</p>"+
                    "<p>Contraseña:"+ psw +"</p>"+
                    "<p>Una vez dentro del sistema debe ingresar al modulo Rúbricas -> opción Rúbricas por curso -> Evaluar rúbrica</p>" +
                    "<p>Saludos.</p><br>"+
                    "</div>"+
                    "<a target='_blank' href='" + url + "' style='font-weight:normal;font-size:15px;padding:7px 10px 7px 10px;text-decoration:none;text-align:center;white-space:nowrap;vertical-align:middle;cursor:pointer;border-radius:0px;border:none;text-shadow:0 0px 0px rgba(255, 255, 255, 0.75);color:#ffffff;background-color:#363636;'>Ingresa Aquí</a>" +
                    "</font>" +
                    "</div>" +
                    "</td>" +
                    "<td width='50' style='width:30pt;padding:0;'></td>" +
                    "</tr>" +
                    "<tr height='25' style='height:15pt;'>" +
                    "<td colspan='5' style='height:15pt;padding:0;'></td>" +
                    "</tr>" +
                    "<tr height='37' style='height:22.5pt;'>" +
                    "<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
                    "<td colspan='3' style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'>" +
                    "<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'><font face='Lucida Sans,sans-serif' size='2' color='white' style='font-family: &quot;Lucida Sans&quot;, sans-serif, serif, EmojiFont;'><span style='font-size:10pt;'>&nbsp;</span></font></span></font></div>" +
                    "</span></td>" +
                    "<td style='height:22.5pt;background-color:#434343;padding:0;'><span style='background-color:#434343;'></span></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td style='padding:0;'>" +
                    "<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
                    "</td>" +
                    "<td colspan='3' style='padding:0;'>" +
                    "<img src='" + urlFooter + "'>" +
                    "</td>" +
                    "<td style='padding:0;'>" +
                    "<div style='margin:0;'><font face='Calibri,sans-serif' size='2' style='font-family: Calibri, sans-serif, serif, EmojiFont;'><span style='font-size:11pt;'>&nbsp;</span></font></div>" +
                    "</td>" +
                    "</tr>" +
                    "</tbody></table>" +
                    "</div>" +
                    "</td>" +
                    "</tr>" +
                    "</tbody></table>" +
                    "</div>" +

                "</body >" +
          "</html>";
        }

        #endregion

        #region End Process

        public static bool CapstineRubricFinalProcess(CargarDatosContext dataContext, int idSumodalidadPA)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    dataContext.context.UpdateRubricaControlCalifica(idSumodalidadPA);
                    transaction.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }

        }


        #endregion
    }
}
