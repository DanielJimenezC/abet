﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.ActionPlan.Model;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.ActionPlan.Export
{
    public static class DictionaryHelperForPlan
    {
        public static String UniversityNameSpanish = "Universidad Peruana de Ciencias Aplicadas";
        public static String UniversityNameEnglish = "Peruvian University of Applied Sciences";
        public static String MessageCofidentialSpanish = "La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.";
        public static String MessageConfidentialEnglish = "The information presented in this Self-Study Report is of a confidential use of ABET, which will not be delivered to third parties, without authorization from the Peruvian University of Applied Sciences, except for summary data, that does not identify the institution.";
        public static String MessageOneSpanish = "Se muestran a continuación acciones de mejora para el periodo ";
        public static String MessageOneEnglish = "Below are actions for improvement for the period ";
        public static String MessageTwoSpanish = "Dentro de cada clasificación, ordenadas por Constituyente e instrumento de evaluación.";
        public static String MessageTwoEnglish = "Within each classification, ordered by Constituent and evaluation instrument.";
        public static String MessageThreeSpanish = "Acciones de mejora del programa – Resultados del estudiante";
        public static String MessageThreeEnglish = "Program Improvement Actions - Student Outcomes";

    }

    public class ExportActionPlan
    {
        private PlanAccion planAccion;
        private Comision comision;
        private String _logoUPC;
        private AbetEntities ctx;
        private Boolean Spanish;

        public ExportActionPlan(AbetEntities ctx, Boolean spanish, String logoUpc, Int32 PlanAccionId, Int32 ComisionId)
        {
            _logoUPC = logoUpc;
            this.Spanish = spanish;
            this.ctx = ctx;
            planAccion = ctx.PlanAccion.FirstOrDefault(x => x.IdPlanAccion == PlanAccionId);
            comision = ctx.Comision.FirstOrDefault(x => x.IdComision == ComisionId);
        }

        public String GetFile(ExportFileFormat fef, String directory)
        {
            if (planAccion == null)
            {
                return String.Empty;
            }
            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;

            }
        }

        private String GetPdf(String directory)
        {
            Document document = CreateDocument();
            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            String filename;

            String codigoComision = comision.Codigo;
            String carrera = Spanish ? planAccion.Carrera.NombreEspanol : planAccion.Carrera.NombreIngles;
            String anio = planAccion.Anio.ToString();

            filename = codigoComision + "_" + carrera.Replace(" ", "_") + "_" + anio + "_" + (Spanish ? "PLAN_ACCION" : "ACTION_PLAN") + ".pdf";
            filename = filename.DeleteSlashAndBackslash();

            String path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);
            return path;
        }

        private String GetRtf(String directory)
        {
            Document document = CreateDocument();            

            String codigoComision = comision.Codigo;
            String carrera = Spanish ? planAccion.Carrera.NombreEspanol : planAccion.Carrera.NombreIngles;
            String anio = planAccion.Anio.ToString();

            String filename;
            filename = codigoComision + "_" + carrera.Replace(" ", "_") + "_" + anio + "_" + (Spanish ? "PLAN_ACCION" : "ACTION_PLAN") + ".rtf";
            filename = filename.DeleteSlashAndBackslash();

            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);
            String path = Path.Combine(directory, filename);
            return path;
        }

        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);
            SetContentSection(document);
            SetCaratula(document);
            SetCaratulaPlanAccion(document);
            SetPlanAccion(document);

            return document;
        }

        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 20;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }

        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }

        private void SetCaratula(Document document)
        {
            Section section = document.LastSection;
            String nombreComision = Spanish ? comision.NombreEspanol : comision.NombreIngles;

            Paragraph paragraph = section.AddParagraph(nombreComision,"Heading1");
            paragraph.Format.SpaceBefore = "18mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Shading.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            String nomPlanAccion = (Spanish ? "Plan de Accion " : "Action Plan ") + planAccion.Anio.ToString();
            paragraph = section.AddParagraph(nomPlanAccion,"Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish ? "Para el" : "From", "Heading3");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.SpaceBefore = "18mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.PageBreakBefore = false;

            String NombrePrograma;
            if (Spanish)
                NombrePrograma = "Programa de " + planAccion.Carrera.NombreEspanol;
            else
                NombrePrograma = planAccion.Carrera.NombreIngles + " Program";

            NombrePrograma = NombrePrograma.ToUpper();

            paragraph = section.AddParagraph(NombrePrograma, "Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Shading.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish?"De la":"To","Heading3");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.SpaceBefore = "18mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish?DictionaryHelperForPlan.UniversityNameSpanish:DictionaryHelperForPlan.UniversityNameEnglish,"Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;            
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph("Lima - Perú","Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.PageBreakBefore = false;

            Image image = section.AddImage(_logoUPC);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            paragraph = section.AddParagraph(planAccion.Anio.ToString(),"Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.SpaceBefore = "10mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.PageBreakBefore = false;

            String Confidencial = Spanish ? "CONFIDENCIAL" : "CONFIDENTIAL";
            paragraph = section.AddParagraph(Confidencial,"Heading1");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.SpaceBefore = "10mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish ? DictionaryHelperForPlan.MessageCofidentialSpanish : DictionaryHelperForPlan.MessageConfidentialEnglish,"Normal");
            paragraph.Format.Alignment = ParagraphAlignment.Justify;            
            paragraph.Format.PageBreakBefore = false;

        }

        private void SetCaratulaPlanAccion(Document document)
        {
            Section section = document.LastSection;
            String NombrePlan = (Spanish ? "PLAN DE ACCIÓN " : "ACTION PLAN " ) + planAccion.Anio.ToString();
            Paragraph paragraph = section.AddParagraph(NombrePlan, "Heading1");
            paragraph.Format.SpaceBefore = "125mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Shading.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = true;
        }

        private void SetPlanAccion(Document document)
        {
            Section section = document.LastSection;
            String NombrePlan = (Spanish ? "PLAN DE ACCIÓN " : "ACTION PLAN ") + planAccion.Anio.ToString();
            Paragraph paragraph = section.AddParagraph(NombrePlan, "Heading2");
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;            
            paragraph.Format.PageBreakBefore = true;

            paragraph = section.AddParagraph((Spanish ? DictionaryHelperForPlan.MessageOneSpanish : DictionaryHelperForPlan.MessageOneEnglish) + planAccion.Anio.ToString(), "Normal");
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Justify;            
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish ? DictionaryHelperForPlan.MessageTwoSpanish : DictionaryHelperForPlan.MessageTwoEnglish, "Normal");
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Justify;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(Spanish ? DictionaryHelperForPlan.MessageThreeSpanish : DictionaryHelperForPlan.MessageThreeEnglish, "Heading2");
            paragraph.Format.SpaceBefore = "10mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Justify;
            paragraph.Format.PageBreakBefore = false;

            SetConstituyente(document,"Estudiante");
            SetConstituyente(document, "Graduados");
            SetConstituyente(document, "Empleador");
            SetConstituyenteDocente(document, "Docente");

        }

        private void SetConstituyente(Document document,String nombreConstituyente)
        {
            Constituyente constituyente = ctx.Constituyente.FirstOrDefault(x => x.NombreEspanol.Contains(nombreConstituyente));
            var instrumentos = ctx.ConstituyenteInstrumento.Where(x => x.IdConstituyente == constituyente.IdConstituyente).Select(y => y.Instrumento).ToList();
            
            Section section = document.LastSection;
            Paragraph paragraph;

            foreach(Instrumento instru in instrumentos)
            {
                paragraph = section.AddParagraph();
                paragraph.AddFormattedText(Spanish ? "Constituyente: " : "Constituent: ", TextFormat.Bold);
                paragraph.AddText(Spanish ? constituyente.NombreEspanol : constituyente.NombreIngles);
                paragraph.Format.SpaceBefore = "10mm";
                paragraph.Format.SpaceAfter = "5mm";
                paragraph.Format.Alignment = ParagraphAlignment.Justify;
                paragraph.Format.PageBreakBefore = false;

                ExportPlanAccionModel model = new ExportPlanAccionModel();
                model.planAccion = planAccion;
                model.InstrumentoId = instru.IdInstrumento;
                model.CarreraId = planAccion.IdCarrera;
                model.ComisionId = comision.IdComision;
                model.CargarDatos(ctx);

                paragraph = section.AddParagraph();
                paragraph.AddFormattedText((Spanish ? instru.NombreEspanol : instru.NombreIngles) + ": ", TextFormat.Bold);
                paragraph.AddText(Spanish ? instru.DescripcionEspanol : instru.DescripcionIngles);
                paragraph.Format.SpaceBefore = "5mm";
                paragraph.Format.SpaceAfter = "5mm";
                paragraph.Format.Alignment = ParagraphAlignment.Justify;
                paragraph.Format.PageBreakBefore = false;


                Table table = section.AddTable();
                table.Format.PageBreakBefore = false;
                table.Borders.Visible = true;
                table.Rows.Height = 30;
                table.Format.Alignment = ParagraphAlignment.Center;

                Column column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 80;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Justify;
                column.Width = 150;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 80;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Justify;
                column.Width = 150;

                Row row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = true;
                row.Shading.Color = Colors.Red;
                row.Height = 15;
                row.Format.Font.Bold = true;
                row[0].MergeRight = 1;
                paragraph = row[0].AddParagraph((Spanish?"Hallazgos ":"Findings ") + (planAccion.Anio - 1).ToString());
                paragraph.Format.Font.Color = Colors.White;
                row[2].MergeRight = 1;
                paragraph = row[2].AddParagraph((Spanish?"Acciones a Tomar ": "Actions to Take ") + planAccion.Anio.ToString());
                row[2].Format.Alignment = ParagraphAlignment.Center;
                paragraph.Format.Font.Color = Colors.White;               

                if (model.AccionesPlan.Count == 0)
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;                                        
                    row.Height = 15;
                    row.Format.Font.Bold = false;

                    row[0].MergeRight = 1;
                    paragraph = row[0].AddParagraph(Spanish ? "No se registraron hallazgos" : "No findings were recorded");
                    paragraph.Format.Font.Color = Colors.Black;
                    row[2].MergeRight = 1;
                    paragraph = row[2].AddParagraph(Spanish ? "No existen acciones a tomar" : "There aren't actions to take");
                    row[2].Format.Alignment = ParagraphAlignment.Center;
                    paragraph.Format.Font.Color = Colors.Black;                    
                }
                else
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.HeadingFormat = true;
                    row.Shading.Color = Colors.Red;
                    row.Height = 15;
                    row.Format.Font.Bold = true;

                    paragraph = row[0].AddParagraph(Spanish?"Codigo":"Code");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[1].AddParagraph(Spanish?"Descripcion":"Description");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[2].AddParagraph(Spanish?"Codigo":"Code");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[3].AddParagraph(Spanish?"Descripcion":"Description");
                    paragraph.Format.Font.Color = Colors.White;
                    
                    foreach(var item in model.AccionesPlan)
                    {
                        int numeroHallazgos = Math.Max(1, model.Hallazgos[item.IdAccionMejora].Count);
                        row = table.AddRow();
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Height = 60;
                        row[2].MergeDown = numeroHallazgos -1;
                        paragraph = row[2].AddParagraph(item.Codigo);
                        paragraph.Format.Font.Color = Colors.Black;
                        row[3].MergeDown = numeroHallazgos -1;
                        paragraph = row[3].AddParagraph(item.DescripcionEspanol);
                        paragraph.Format.Font.Color = Colors.Black;

                        for(int k = 0; k < numeroHallazgos; k++)
                        {                            
                            paragraph = row[0].AddParagraph(model.Hallazgos[item.IdAccionMejora][k].Codigo);
                            paragraph.Format.Font.Color = Colors.Black;
                            if (model.Hallazgos[item.IdAccionMejora][k].OutcomeComision.IdOutcome != 0)
                            {
                                String resultadoEstudiante = (Spanish ? "Resultado del Estudiante: " : "Student Outcomes: ") + model.Hallazgos[item.IdAccionMejora][k].OutcomeComision.Outcome.Nombre.ToLower();
                                paragraph = row[1].AddParagraph();
                                paragraph.AddFormattedText(resultadoEstudiante, TextFormat.Bold);
                                row[1].AddParagraph();
                            }
                            paragraph = row[1].AddParagraph();
                            paragraph.AddText(model.Hallazgos[item.IdAccionMejora][k].DescripcionEspanol);                            
                            paragraph.Format.Font.Color = Colors.Black;
                            if (k + 1 == numeroHallazgos) continue;
                            row = table.AddRow();
                            row.VerticalAlignment = VerticalAlignment.Center;
                            row.Height = 30;                            
                        }                        
                    }

                }


            }

        }

        private void SetConstituyenteDocente(Document document, String nombreConstituyente)
        {
            Constituyente constituyente = ctx.Constituyente.FirstOrDefault(x => x.NombreEspanol.Contains(nombreConstituyente));
            var instrumentoIFC = ctx.ConstituyenteInstrumento.Where(x => x.IdConstituyente == constituyente.IdConstituyente).Select(y => y.Instrumento).FirstOrDefault();
            var outcomes = ctx.OutcomeComision.Where(x => x.IdComision == comision.IdComision).Select(y => y.Outcome).ToList();

            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph();
            paragraph.AddFormattedText(Spanish ? "Constituyente: " : "Constituent: ", TextFormat.Bold);
            paragraph.AddText(Spanish ? constituyente.NombreEspanol : constituyente.NombreIngles);
            paragraph.Format.SpaceBefore = "10mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Justify;
            paragraph.Format.PageBreakBefore = false;

            String nombreInstrumento = instrumentoIFC.NombreEspanol + ": " + instrumentoIFC.DescripcionEspanol;
            paragraph = section.AddParagraph(nombreInstrumento, "Heading3");
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.Alignment = ParagraphAlignment.Justify;
            paragraph.Format.PageBreakBefore = false; 
         
            var cursosActuales = planAccion.AccionMejoraPlanAccion.
                Select(x => x.AccionMejora.HallazgoAccionMejora.FirstOrDefault()).
                Select(x => x.Hallazgos)
                .Where(x => x.IdCurso.HasValue && x.ConstituyenteInstrumento.IdInstrumento == instrumentoIFC.IdInstrumento)
                .Select(x=>x.Curso).Distinct().ToList();

            List<MallaCocosDetalle> relacionesCursos = ctx.MallaCocos
                .Where(x => x.CarreraComision.IdCarrera == planAccion.IdCarrera &&
                x.CarreraComision.IdComision == comision.IdComision)
                .SelectMany(x => x.MallaCocosDetalle).ToList();

            foreach (var outcome in outcomes)
            {
                List<Curso> cursosDisponibles = relacionesCursos
                    .Where(x => x.OutcomeComision.IdComision == comision.IdComision && x.OutcomeComision.IdOutcome == outcome.IdOutcome)
                    .Select(x => x.CursoMallaCurricular.Curso).ToList();                

                List<Int32> existeCurso = cursosActuales.Select(X => X.IdCurso).ToList();
                List<Curso> cursos = cursosDisponibles.Where(x => existeCurso.Any(Y => Y == x.IdCurso)).ToList();

                if (cursos.Count == 0) continue;

                String tituloOutcome = (Spanish ? "Resultado del Estudiante " : "Student Outcome ")
                    + '"' + outcome.Nombre.ToLower() + '"';
                String titulo2Outcome = (Spanish? ". Aportan a este resultado los siguientes cursos: ": ". The following courses contribute to this result.");
                paragraph = section.AddParagraph();
                paragraph.AddFormattedText(tituloOutcome, TextFormat.Bold);
                paragraph.AddText(titulo2Outcome);
                paragraph.Format.SpaceBefore = "10mm";
                paragraph.Format.SpaceAfter = "10mm";
                paragraph.Format.Alignment = ParagraphAlignment.Justify;
                paragraph.Format.PageBreakBefore = false; ;

                foreach (var curso in cursos)
                {
                    paragraph = section.AddParagraph();
                    paragraph.AddText(curso.Codigo + " " + (Spanish ? curso.NombreEspanol : curso.NombreIngles));
                    paragraph.Format.SpaceBefore = "5mm";
                    paragraph.Format.SpaceAfter = "5mm";
                    paragraph.Format.Alignment = ParagraphAlignment.Justify;
                    paragraph.Format.PageBreakBefore = false; 
                }


                ExportPlanAccionModel model = new ExportPlanAccionModel();
                model.planAccion = planAccion;
                model.InstrumentoId = instrumentoIFC.IdInstrumento;
                model.CarreraId = planAccion.IdCarrera;
                model.ComisionId = comision.IdComision;
                model.Cursos = cursos;
                model.CargarDatos(ctx);

                Table table = section.AddTable();
                table.Format.PageBreakBefore = false;
                table.Borders.Visible = true;
                table.Rows.Height = 30;
                table.Format.Alignment = ParagraphAlignment.Center;

                Column column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 80;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Justify;
                column.Width = 150;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Center;
                column.Width = 80;
                column = table.AddColumn();
                column.Format.Alignment = ParagraphAlignment.Justify;
                column.Width = 150;

                Row row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = true;
                row.Shading.Color = Colors.Red;
                row.Height = 15;
                row.Format.Font.Bold = true;
                row[0].MergeRight = 1;
                paragraph = row[0].AddParagraph((Spanish ? "Hallazgos " : "Findings ") + (planAccion.Anio - 1).ToString());
                paragraph.Format.Font.Color = Colors.White;
                row[2].MergeRight = 1;
                paragraph = row[2].AddParagraph((Spanish ? "Acciones a Tomar " : "Actions to Take ") + planAccion.Anio.ToString());
                row[2].Format.Alignment = ParagraphAlignment.Center;
                paragraph.Format.Font.Color = Colors.White;

                if (model.AccionesPlan.Count == 0)
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Height = 15;
                    row.Format.Font.Bold = false;

                    row[0].MergeRight = 1;
                    paragraph = row[0].AddParagraph(Spanish ? "No se registraron hallazgos" : "No findings were recorded");
                    paragraph.Format.Font.Color = Colors.Black;
                    row[2].MergeRight = 1;
                    paragraph = row[2].AddParagraph(Spanish ? "No existen acciones a tomar" : "There aren't actions to take");
                    row[2].Format.Alignment = ParagraphAlignment.Center;
                    paragraph.Format.Font.Color = Colors.Black;
                }
                else
                {
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.HeadingFormat = true;
                    row.Shading.Color = Colors.Red;
                    row.Height = 15;
                    row.Format.Font.Bold = true;

                    paragraph = row[0].AddParagraph(Spanish ? "Codigo" : "Code");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[1].AddParagraph(Spanish ? "Descripcion" : "Description");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[2].AddParagraph(Spanish ? "Codigo" : "Code");
                    paragraph.Format.Font.Color = Colors.White;
                    paragraph = row[3].AddParagraph(Spanish ? "Descripcion" : "Description");
                    paragraph.Format.Font.Color = Colors.White;

                    foreach (var item in model.AccionesPlan)
                    {
                        int numeroHallazgos = Math.Max(1, model.Hallazgos[item.IdAccionMejora].Count);
                        row = table.AddRow();
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Height = 60;
                        row[2].MergeDown = numeroHallazgos - 1;
                        paragraph = row[2].AddParagraph(item.Codigo);
                        paragraph.Format.Font.Color = Colors.Black;
                        row[3].MergeDown = numeroHallazgos - 1;
                        paragraph = row[3].AddParagraph(item.DescripcionEspanol);
                        paragraph.Format.Font.Color = Colors.Black;

                        for (int k = 0; k < numeroHallazgos; k++)
                        {                            
                            paragraph = row[0].AddParagraph(model.Hallazgos[item.IdAccionMejora][k].Codigo);
                            paragraph.Format.Font.Color = Colors.Black;                            
                            paragraph = row[1].AddParagraph();
                            paragraph.AddText(model.Hallazgos[item.IdAccionMejora][k].DescripcionEspanol);
                            paragraph.Format.Font.Color = Colors.Black;
                            if (k + 1 == numeroHallazgos) continue;
                            row = table.AddRow();
                            row.VerticalAlignment = VerticalAlignment.Center;
                            row.Height = 30;
                        }
                    }

                }

                

            }
        }
    }
}
