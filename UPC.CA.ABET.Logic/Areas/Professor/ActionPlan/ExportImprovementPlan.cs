﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.IO;

namespace UPC.CA.ABET.Logic.Areas.Professor.ActionPlan
{
    public class DictionarieHelper
    {
        public string UniversityNameSpanish = "Universidad Peruana de Ciencias Aplicada";
        public string UniversityNameEnglish = "Peruvian University of Applied Sciences";
        public string ImportantMessageTitleSpanish = "CONFIDENCIAL";
        public string ImportantMessageTitleEnglish = "CONFIDENTIAL";
        public string ImportantMessageSpanish = "La información presentada en este Reporte de Autoestudio es de uso confidencial de ABET,  que  no será entregada a terceros, sin autorización de la Universidad Peruana de Ciencias Aplicadas, a excepción de datos resumen, que no identifiquen a la institución.";
        public string ImportantMessageEnglish = "The information presented in this Self-Study Report is of a confidential use of ABET, which will not be delivered to third parties, without authorization from the Peruvian University of Applied Sciences, except for summary data, that does not identify the institution.";
    }
    public class ExportImprovementPlan
    {

        private string _logoUpc;
        private AbetEntities ctx;
        private string _culture;
        private Boolean Spanish;
        public string city;
        public string country;
        public string Univeristy;
        DictionarieHelper TextHelper = new DictionarieHelper();

        private TemplateImprovementPlan DataTemplate = new TemplateImprovementPlan();
        public ExportImprovementPlan(AbetEntities ctx, Boolean Spanish, string logo)
        {
            _logoUpc = logo;
            this.Spanish = Spanish;
            this.ctx = ctx;
        }
        public void SetDataTemplate(int IdPlanAccion, int IdComision)
        {
            DataTemplate = FillTemplateData(IdPlanAccion, IdComision);
        }

        public TemplateImprovementPlan FillTemplateData(int IdPlanAccion, int IdComision)
        {
            ///
            /// FillingData
            ///
            var comision = ctx.Comision.FirstOrDefault(x => x.IdComision == IdComision);
            var planaccion = ctx.PlanAccion.FirstOrDefault(x => x.IdPlanAccion == IdPlanAccion);
            var Carrera = ctx.Carrera.FirstOrDefault(x => x.IdCarrera == planaccion.IdCarrera);

            DataTemplate.DiscoveriesYear = (planaccion.Anio - 1);
            DataTemplate.Spanish = Spanish;

            //DataCover BEGIN
            DataTemplate.cover.Spanish = Spanish;
            DataTemplate.cover.ImprovementPlanName.NameSpanish = Carrera.NombreEspanol;
            DataTemplate.cover.ImprovementPlanName.NameEnglish = Carrera.NombreIngles;

            DataTemplate.cover.CommitionName.NameSpanish = comision.NombreEspanol;
            DataTemplate.cover.CommitionName.NameEnglish = comision.NombreIngles;
            DataTemplate.cover.DiscoveriesYear = planaccion.Anio;
            //DataTemplate.cover.CityCountry.NameSpanish = (city.ToUpper() + " - " + country.ToUpper());
            //DataTemplate.cover.CityCountry.NameEnglish = (city.ToUpper() + " - " + country.ToUpper());
            DataTemplate.cover.UniversityName.NameSpanish = TextHelper.UniversityNameSpanish.ToUpper();
            DataTemplate.cover.UniversityName.NameEnglish = TextHelper.UniversityNameEnglish.ToUpper();
            DataTemplate.cover.UniversityLogo.NameSpanish = _logoUpc;
            DataTemplate.cover.UniversityLogo.NameEnglish = _logoUpc;
            DataTemplate.cover.Important.NameSpanish = TextHelper.ImportantMessageTitleSpanish;
            DataTemplate.cover.Important.NameEnglish = TextHelper.ImportantMessageTitleEnglish;
            DataTemplate.cover.ImportantDescripction.NameSpanish = TextHelper.ImportantMessageSpanish;
            DataTemplate.cover.ImportantDescripction.NameEnglish = TextHelper.ImportantMessageEnglish;
            //DataCover END

            //ImprovementPlanBegin
            DataTemplate.improvementPlan.Spanish = Spanish;
            DataTemplate.improvementPlan.DiscoveriesYear = planaccion.Anio;
            DataTemplate.improvementPlan.ImprovementPlanName().NameEnglish = "PLAN DE MEJORA" + planaccion.Anio;
            DataTemplate.improvementPlan.ImprovementPlanName().NameSpanish = "PLAN DE MEJORA" + planaccion.Anio;

            //for(var temp in ctx.ConstituyenteInstrumento.Where(x=>x.IdConstituyente=planaccion.IdPlanAccion))
            //{
            //    DataTemplate.improvementPlan.ADDconstituente(new Elements() { NameEnglish = "", NameSpanish = "" });
            //}
            


            //ImprovementPlanEnd


            return new TemplateImprovementPlan();
        }

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if (DataTemplate == null)
            {
                return String.Empty;
            }

            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;
            }
        }

        /// <summary>
        /// Método para obtener la dirección del archivo PDF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo PDF creado para la malla de cocos</returns>
        private string GetPdf(string directory)
        {
            Document document = CreateDocument();

            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            string filename;
            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                filename = "EAC" + DataTemplate.cover.DiscoveriesYear + "_Planes de Mejora_Final" + ".pdf";
            }
            else
            {
                filename = "EAC" + DataTemplate.cover.DiscoveriesYear + "_Planes de Mejora_Final" + ".pdf";
            }

            filename = filename.DeleteSlashAndBackslash();
            string path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);

            return path;
        }

        /// <summary>
        /// Método para obtener la dirección del archivo RTF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo RTF creado para la malla de cocos</returns>
        private string GetRtf(string directory)
        {
            Document document = CreateDocument();

            string filename;
            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                filename = "EAC" + DataTemplate.cover.DiscoveriesYear + "_Planes de Mejora_Final" + ".rtf";
            }
            else
            {
                filename = "EAC" + DataTemplate.cover.DiscoveriesYear + "_Planes de Mejora_Final" + ".pdf";
            }
            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);
            string path = Path.Combine(directory, filename);

            return path;
        }

        /// <summary>
        /// Método para crear el documento de exportación de malla de cocos
        /// </summary>
        /// <returns>Documento que contiene la información necesaria para generar el archivo de exportación</returns>
        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 12;

            style = document.Styles["ParagraphNormal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 12;
            style.Font.Bold = false;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.SpaceAfter = 12;
            style.ParagraphFormat.SpaceBefore = 12;

            style = document.Styles["JustifiedParagraph"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 11;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.SpaceAfter = 12;
            style.ParagraphFormat.SpaceBefore = 12;

            style = document.Styles["SubTitleBold"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.SpaceAfter = 12;
            style.ParagraphFormat.SpaceBefore = 12;

            style = document.Styles["SubTitleConstituente"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.SpaceBefore = 12;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }
        private Document CreateDocument()
        {
            Document document = new Document();
            //SetStyles(document);
            SetCover(document);
            SetImprovementPlan(document);


            return document;
        }

        private void SetCover(Document document)
        {
            Section section = document.LastSection;



            Paragraph paragraph = section.AddParagraph(DataTemplate.cover.CommitionName.Name(), "Heading1");

            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.PageBreakBefore = false;
            paragraph.Style = "StyleName";




            //Image "LOGOUPC"
            Image image = section.AddImage(_logoUpc);
            image.Left = ShapePosition.Center;
            image.Width = "1.32cm";
            image.Height = "1.32cm";
            image.LockAspectRatio = true;


        }
        public void Tableconstituente(Section section, Paragraph paragraph, Constituente constituente)
        {
            //Table Constituente
            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = "0.69cm";
            table.Format.Alignment = ParagraphAlignment.Center;

            Row row0 = table.AddRow();
            row0.VerticalAlignment = VerticalAlignment.Center;
            row0.HeadingFormat = true;
            row0.Shading.Color = Colors.LightBlue;
            row0.Height = 15;
            row0.Format.Font.Bold = true;

            Column column0 = table.AddColumn();
            column0.Format.Alignment = ParagraphAlignment.Center;
            column0.Width = "7.44cm";

            column0 = table.AddColumn();
            column0.Format.Alignment = ParagraphAlignment.Center;
            column0.Width = "8.00cm";

            paragraph = column0[0].AddParagraph(constituente.GetTblConstituente().DiscoveriesText().Name());
            paragraph.Format.Font.Color = Colors.Black;
            paragraph = column0[1].AddParagraph(constituente.GetTblConstituente().ActionText().Name());
            paragraph.Format.Font.Color = Colors.Black;


            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.DarkBlue;
            row.Height = 15;
            row.Format.Font.Bold = true;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = "2.94cm";


            

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = "4.50cm";

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = "2.75cm";

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = "5.25cm";

            paragraph = column[0].AddParagraph(constituente.GetTblConstituente().ColumnCodeText().Name());
            paragraph.Format.Font.Color = Colors.White;
            paragraph = column[1].AddParagraph(constituente.GetTblConstituente().ColumnDescripctionText().Name());
            paragraph.Format.Font.Color = Colors.White;
            paragraph = column[2].AddParagraph(constituente.GetTblConstituente().ColumnCodeText().Name());
            paragraph.Format.Font.Color = Colors.White;
            paragraph = column[3].AddParagraph(constituente.GetTblConstituente().ColumnDescripctionText().Name());
            paragraph.Format.Font.Color = Colors.White;

            ///Content rows
            ///

            for (int i = 0; i < constituente.GetTblConstituente().ListActions().Count(); i++)
            {
                Row rown = table.AddRow();
                rown.VerticalAlignment = VerticalAlignment.Center;
                rown.HeadingFormat = true;
                rown.Shading.Color = Colors.DarkBlue;
                rown.Height = 15;
                rown.Format.Font.Bold = true;




                rown[0].AddParagraph(constituente.GetTblConstituente().ListDiscoveries()[i].CodetFullText());

                rown[1].AddParagraph(constituente.GetTblConstituente().ListDiscoveries()[i].GETDescription());

                foreach (var results in constituente.GetTblConstituente().ListDiscoveries()[i].GETResultStudent())
                {
                    rown[1].AddParagraph(results.ResultStudentFullText().Name());
                }

                rown[2].AddParagraph(constituente.GetTblConstituente().ListActions()[i].Code.Name());
                rown[3].AddParagraph(constituente.GetTblConstituente().ListActions()[i].Description.Name());



            }




        }
        public void SetImprovementPlan(Document document)
        {
            Section section = document.LastSection;

            Paragraph paragraph = section.AddParagraph(DataTemplate.improvementPlan.ImprovementPlanName().Name(), "Heading1");
            section.AddParagraph(DataTemplate.improvementPlan.Paragrah1().Name(), "ParagrahNormal");
            section.AddParagraph(DataTemplate.improvementPlan.Paragrah2().Name(), "ParagrahNormal");
            section.AddParagraph(DataTemplate.improvementPlan.ConstituenteText().Name(), "SubTitleBold");

            foreach (var constituente in DataTemplate.improvementPlan.ListConstituenete())
            {
                section.AddParagraph(constituente.ConstituenteFullText().Name(), "SubTitleConstituente");

                //List Instuments
                foreach (var instrument in constituente.ListInstruments())
                {
                    section.AddParagraph(instrument.InstrumentFullText().Name(), "SubTitleConstituente");
                }

                Tableconstituente(section, paragraph, constituente);



            }










        }


    }
}
