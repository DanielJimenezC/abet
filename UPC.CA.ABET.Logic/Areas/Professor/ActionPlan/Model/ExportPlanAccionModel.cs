﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using System.Data.Entity;

namespace UPC.CA.ABET.Logic.Areas.Professor.ActionPlan.Model
{
    public class ExportPlanAccionModel
    {
        public PlanAccion planAccion { get; set; }
        public List<AccionMejora> AccionesPlan { get; set; }
        public Dictionary<Int32, List<Hallazgos>> Hallazgos { get; set; }
        public Int32? InstrumentoId { get; set; }
        public Int32? NivelAceptacionId { get; set; }
        public Int32? CarreraId { get; set; }
        public Int32? ComisionId { get; set; }
        public Int32? CursoId { get; set; }
        public List<Curso> Cursos { get; set; }

        public void CargarDatos(AbetEntities ctx)
        {
            if (planAccion == null) return;

            Hallazgos = new Dictionary<int, List<Hallazgos>>();

            var accionesActuales = planAccion.AccionMejoraPlanAccion.Select(X => X.AccionMejora).ToList();

            List<Int32> existeCurso = new List<int>();
            if (InstrumentoId == 1 && Cursos.Count != 0)
            {
                existeCurso = Cursos.Select(x => x.IdCurso).ToList();
            }

            Int32 AnioBuscar = planAccion.Anio - 1;

            var query = ctx.Hallazgos.Where(x => x.ConstituyenteInstrumento.IdInstrumento == InstrumentoId
                && x.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico.Contains(AnioBuscar.ToString())).AsQueryable();
            //FALTA TERMINAR
            String AcronimoComision = "";
            if (CarreraId.HasValue)
                query = query.Where(x => x.OutcomeComision.Comision.CarreraComision.FirstOrDefault().IdCarrera == CarreraId.Value);
            if (ComisionId.HasValue)
            {
                AcronimoComision = ctx.Comision.FirstOrDefault(x => x.IdComision == ComisionId).Codigo;

                query = (from a in query
                         join b in ctx.CursoMallaCurricular on a.IdCurso equals b.IdCurso
                         join c in ctx.MallaCocosDetalle on b.IdCursoMallaCurricular equals c.IdCursoMallaCurricular
                         join d in ctx.OutcomeComision on c.IdOutcomeComision equals d.IdOutcomeComision
                         where (a.OutcomeComision.Comision.Codigo.Contains(AcronimoComision) || d.Comision.Codigo.Contains(AcronimoComision))
                         select a);
            }
            if (InstrumentoId.Value == 1)
                query = query.Where(x => existeCurso.Any(Y => (Y == x.IdCurso)));

            var accionesDisponibles = query.SelectMany(x => x.HallazgoAccionMejora.Select(Y => Y.AccionMejora)).Distinct().ToList();

            var existeAccion = accionesActuales.Select(X => X.IdAccionMejora).ToList();
            AccionesPlan = accionesDisponibles.Where(X => existeAccion.Any(Y => (Y == X.IdAccionMejora))).ToList();
            //AccionesPlan = accionesActuales;

            List<Int32> IdsAccionesMejora = AccionesPlan.Select(X => X.IdAccionMejora).ToList();
            var HallazgosAccionMejora = ctx.HallazgoAccionMejora.Where(X => IdsAccionesMejora.Any(Y => Y == X.IdAccionMejora)).Include(X => X.Hallazgos).ToList();
            AccionesPlan.ForEach(X => Hallazgos[X.IdAccionMejora] = HallazgosAccionMejora.Where(Y => Y.IdAccionMejora == X.IdAccionMejora).Select(Y => Y.Hallazgos)
                .Where(W =>
                    (InstrumentoId.HasValue ? W.ConstituyenteInstrumento.IdInstrumento == InstrumentoId.Value : true) &&
                    (W.SubModalidadPeriodoAcademicoModulo.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico.Contains(AnioBuscar.ToString())) &&
                    (NivelAceptacionId.HasValue && W.IdNivelAceptacionHallazgo.HasValue ? NivelAceptacionId.Value == W.IdNivelAceptacionHallazgo.Value : true) &&
                    ((CarreraId.HasValue && W.OutcomeComision.Comision.CarreraComision.Select(z => z.IdCarrera).Contains(CarreraId.Value) ? CarreraId.Value == W.OutcomeComision.Comision.CarreraComision.FirstOrDefault().IdCarrera : true)) && 
                    (ComisionId.HasValue ? W.OutcomeComision.Comision.Codigo.Contains(AcronimoComision) : true)
                    ).GroupBy(Y => Y.Codigo).Select(x => x.FirstOrDefault()).ToList());
        }

    }
}
