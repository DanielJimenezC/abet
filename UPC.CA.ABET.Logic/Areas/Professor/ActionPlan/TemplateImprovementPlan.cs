﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Professor.ActionPlan
{
    public class Baseclass
    {
        public Boolean Spanish { get; set; }

        public Int32 DiscoveriesYear { get; set; }

    }
    public class TemplateImprovementPlan : Baseclass
    {
        public Cover cover = new Cover();

        public ImprovementPlan improvementPlan = new ImprovementPlan();

    }

    public class Elements
    {
        public Boolean Spanish { get; set; }


        public Double factorpointmm = 0.3528;
        public String NameSpanish { get; set; }

        public String NameEnglish { get; set; }

        public String Name() { return (Spanish ? NameSpanish : NameEnglish); }

        public int SizePoint { get; set; }

        public double SizeName() { return SizePoint * factorpointmm; }

        public double SizeAfter(int pointAfeter) { return pointAfeter * factorpointmm; }

        public double SizeBefore(int pointAfeter) { return pointAfeter * factorpointmm; }
    }

    public class Marker
    {
        public String Color { get; set; }
    }
    public class Cover : Baseclass
    {

        public Elements CommitionName { get; set; }
        public Elements to()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Para el",
                NameEnglish = "To"
            };
        }
        public Elements ImprovementPlanName { get; set; }
        public Elements of()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Para el",
                NameEnglish = "Of"
            };
        }
        public Elements ProgramName { get; set; }
        public Elements UniversityName { get; set; }
        public Elements CityCountry { get; set; }
        public Elements UniversityLogo { get; set; }
        public Elements Year { get; set; }
        public Elements Important { get; set; }
        public Elements ImportantDescripction { get; set; }

        public Cover()
        {
            CommitionName = new Elements();
            ImprovementPlanName = new Elements();
            ProgramName = new Elements();
            UniversityName = new Elements();
            CityCountry = new Elements();
            UniversityLogo = new Elements();
            Year = new Elements();
            Important = new Elements();
            ImportantDescripction = new Elements();
        }

    }

    public class PostCover : Baseclass
    {
        public Elements ImprovementPlanName { get; set; }
    }

    public class Course : Baseclass
    {
        public Elements CourseCode { get; set; }
        public Elements CourseName { get; set; }

        public Elements FullTextName()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameEnglish = CourseCode.NameEnglish + " " + CourseName.NameEnglish,
                NameSpanish = CourseCode.NameSpanish + " " + CourseName.NameSpanish
            };
        }

    }

    public class BaseInstrumentDiscoveries : Baseclass
    {
        public Elements ColumnCodeText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Código ",
                NameEnglish = "Code"
            };
        }
        public Elements ColumnDescripctionText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Código ",
                NameEnglish = "Code"
            };
        }
    }

    public class Instrument : Baseclass
    {
        public Elements InstrumentName { get; set; }
        public Elements InstrumentFullText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Instrumento :" + InstrumentName.Name().ToString(),
                NameEnglish = "Instrument :" + InstrumentName.Name().ToString()
            };
        }


    }
    public class CodeDescription:Baseclass
    {
        public Elements Code = new Elements();

        public Elements Description = new Elements();
    }
    public class ResultsDescription:Baseclass
    {
        public String Period { get; set; }

        public String Note { get; set; }
        public Elements ResultStudentFullText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Resultado obtenido " 
                + Period + ':' +
                 Note,
                NameEnglish = "Obtained Result "
                + Period + ':' +
                 Note
            };
        }
    }
    public class DescriptionStudent:Baseclass
    {
        public String StudentName { get; set; }
        public Elements ResultStudentFullText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Resultado del Estudiante " +
                '"' + StudentName + '"' +
                " :",
                NameEnglish = "Result of the Student " +
                '"' + StudentName + '"' +
                " :"
            };
        }
    }
    public class CodeDescription2 : Baseclass
    {
        public List<Elements> Codes = new List<Elements>();

        public String CodetFullText()
        {
            return SetAnd();
        }
        private String SetAnd()
        {
            String temp = "";

            for(int i=0; i<Codes.Count;i++)
            {
                

                if(i+1==Codes.Count)
                {
                    temp += " y ";
                }
                temp = Codes[i].Name();
            }

            return temp;
        }


        private DescriptionStudent Description = new DescriptionStudent();

        public void AddDescription(String descrition)
        {
            Description.Spanish = Spanish;

            Description.StudentName = descrition;
        }

        public String GETDescription()
        {
            Description.Spanish = Spanish;

            return Description.ResultStudentFullText().Name();
        }

        private List<ResultsDescription> ResultStudent = new List<ResultsDescription>();

        public void AddResultStudentn(ResultsDescription temp)
        {
     
            ResultStudent.Add(temp);
        }

        public List<ResultsDescription> GETResultStudent()
        {
            List<ResultsDescription> temp = new List<ResultsDescription>();

            Description.Spanish = Spanish;
            foreach(ResultsDescription t in ResultStudent)
            {
                t.Spanish = Spanish;
                temp.Add(t);
            }

            return temp;
        }

    }


    public class TableConstituente : BaseInstrumentDiscoveries
    {
        public Elements DiscoveriesText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Hallazgos " + (DiscoveriesYear - 1).ToString(),
                NameEnglish = "Discoveries " + (DiscoveriesYear - 1).ToString()
            };
        }

        public Elements ActionText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Actions a Tomar " + DiscoveriesYear.ToString(),
                NameEnglish = "Actions to Take " + DiscoveriesYear.ToString()

            };
        }

        //Discoveries

        private List<CodeDescription2> listDiscoveries = new List<CodeDescription2>();

        public void ADDDiscoveries(Elements code, Elements description)
        {
            code.Spanish = Spanish;
            description.Spanish = Spanish;

            listActions.Add(new CodeDescription
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                Code = code,
                Description = description
            });
        }

        public List<CodeDescription2> ListDiscoveries()
        {
            List<CodeDescription2> lstactionstemp = new List<CodeDescription2>();

            foreach (CodeDescription2 cons in listDiscoveries)
            {
                cons.Spanish = Spanish; //traduccion
                lstactionstemp.Add(cons);
            }

            return lstactionstemp;

        }

        //actions
        private List<CodeDescription> listActions = new List<CodeDescription>();

        public void ADDActions(Elements code, Elements description)
        {
            code.Spanish = Spanish;
            description.Spanish = Spanish;

            listActions.Add(new CodeDescription
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                Code = code,
                Description = description
            });
        }

        public List<CodeDescription> ListActions()
        {
            List<CodeDescription> lstactionstemp = new List<CodeDescription>();

            foreach (CodeDescription cons in listActions)
            {
                cons.Spanish = Spanish; //traduccion
                lstactionstemp.Add(cons);
            }

            return lstactionstemp;

        }

    }

    public class TableStudentResult: BaseInstrumentDiscoveries
    {
        public Elements ColumnDiscoveriesNameText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Hallazgos " + DiscoveriesYear.ToString(),
                NameEnglish = "Discoveries " + DiscoveriesYear.ToString()

            };
        }

        public Elements ColumnActionsNameText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Actions a Tomar " + DiscoveriesYear.ToString(),
                NameEnglish = "Actions to Take " + DiscoveriesYear.ToString()

            };
        }


        //Discoveries
        private List<CodeDescription> listDiscoveries = new List<CodeDescription>();

        public void ADDDiscoveries(Elements code, Elements description)
        {
            code.Spanish = Spanish;
            description.Spanish = Spanish;

            listDiscoveries.Add(new CodeDescription
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                Code = code,
                Description = description
            });
        }

        public List<CodeDescription> ListDiscoveries()
        {
            List<CodeDescription> lstdiscoveriestemp = new List<CodeDescription>();

            foreach (CodeDescription cons in listDiscoveries)
            {
                cons.Spanish = Spanish; //traduccion
                lstdiscoveriestemp.Add(cons);
            }

            return lstdiscoveriestemp;

        }

        //Actions

        private List<CodeDescription> listActions = new List<CodeDescription>();

        public void ADDActions(Elements code, Elements description)
        {
            code.Spanish = Spanish;
            description.Spanish = Spanish;

            listActions.Add(new CodeDescription
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                Code = code,
                Description = description
            });
        }

        public List<CodeDescription> ListActions()
        {
            List<CodeDescription> lstactionstemp = new List<CodeDescription>();

            foreach (CodeDescription cons in listActions)
            {
                cons.Spanish = Spanish; //traduccion
                lstactionstemp.Add(cons);
            }

            return lstactionstemp;

        }
    }

    public class StudentsResults : Baseclass
    {
        public Elements StudentName { get; set; }
        public Elements ResultStudentFullText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Resultado del Estudiante " +
                '"' + StudentName + '"' +
                '.' + "Aportan a este resultado los siguientes cursos:",
                NameEnglish = "Result of the Student " +
                '"' + StudentName + '"' + '.' +
                "Contributes to this result the following courses:"
            };
        }

        private List<Course> courses = new List<Course>();
        public void ADDInstruments(Elements course)
        {
            course.Spanish = Spanish;

            courses.Add(new Course
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                CourseName = course
            });
        }

        public List<Course> ListCourses()
        {
            List<Course> lstCoursetemp = new List<Course>();

            foreach (Course cons in courses)
            {
                cons.Spanish = Spanish; //traduccion
                lstCoursetemp.Add(cons);
            }

            return lstCoursetemp;

        }

        private TableStudentResult TblStudentResult = new TableStudentResult();

        public void AddTableStudentResult(TableStudentResult temp)
        {
            temp.Spanish = Spanish; ;
            TblStudentResult.Spanish = Spanish;
            TblStudentResult = temp;
        }
        public TableStudentResult GetTableStudentResult()
        {
            TblStudentResult.Spanish = Spanish;
            return TblStudentResult;
        }

    }
    public class Constituente : Baseclass
    {

        public Elements ConstituenteName { get; set; }
        public Elements ConstituenteFullText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Consituyente: "
                + (DiscoveriesYear - 1).ToString()
                + ConstituenteName.NameSpanish.ToString(),
                NameEnglish = "constituent: "
                + (DiscoveriesYear - 1).ToString()
                + ConstituenteName.NameEnglish.ToString()
            };
        }


        private List<Instrument> lstInstruments = new List<Instrument>();
        public void ADDInstruments(Elements instrument)
        {
            instrument.Spanish = Spanish;

            lstInstruments.Add(new Instrument
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                InstrumentName = instrument
            });
        }

        public List<Instrument> ListInstruments()
        {
            List<Instrument> lstInstrumenttemp = new List<Instrument>();

            foreach (Instrument cons in lstInstruments)
            {
                cons.Spanish = Spanish; //traduccion
                lstInstrumenttemp.Add(cons);
            }

            return lstInstrumenttemp;

        }

        private TableConstituente TblConstituente = new TableConstituente();

        public void AddConstituente(TableConstituente temp)
        {
            temp.Spanish = Spanish; ;
            TblConstituente.Spanish = Spanish;
            TblConstituente = temp;
        }
        public TableConstituente GetTblConstituente()
        {
            TblConstituente.Spanish = Spanish;
            return TblConstituente ;
        }

    }
    public class ImprovementPlan : Baseclass
    {
        public Elements ImprovementPlanCover()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "PLAN DE MEJORA " + DiscoveriesYear.ToString(),
                NameEnglish = "IMPROVEMENT PLAN " + DiscoveriesYear.ToString()
            };
        }
        public Elements ImprovementPlanName()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "PLAN DE MEJORA " + DiscoveriesYear.ToString(),
                NameEnglish = "IMPROVEMENT PLAN " + DiscoveriesYear.ToString()

            };
        }

        public Elements Paragrah1()
        {
            
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Se muestran a continuación acciones de mejora para el periodo.",
                NameEnglish = "Improvement actions for the period are shown below."

            };
        }
        public Elements Paragrah2()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Dentro de cada clasificación, ordenadas por Constituyente e instrumento de evaluación.",
                NameEnglish = "Within each classification, ordered by Constituent and evaluation instrument."

            };
        }

        public Elements ConstituenteText()
        {
            return new Elements()
            {
                Spanish = Spanish,
                NameSpanish = "Acciones de mejora del programa – Resultados del estudiante",
                NameEnglish = "Program improvement actions - Student Results"

            };
        }

        private List<Constituente> lstConstituente = new List<Constituente>();
        public void ADDconstituente(Elements constituente)
        {
            constituente.Spanish = Spanish;

            lstConstituente.Add(new Constituente
            {   Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                ConstituenteName = constituente
            });
        }

        public List<Constituente> ListConstituenete()
        {
            List<Constituente> lstconstituentetemp = new List<Constituente>();

            foreach (Constituente cons in lstConstituente)
            {
                cons.Spanish = Spanish; //traduccion
                cons.DiscoveriesYear = DiscoveriesYear;
                lstconstituentetemp.Add(cons);
            }

            return lstconstituentetemp;

        }

        private List<StudentsResults> lstStudentResult = new List<StudentsResults>();

        public void ADDStudentResult(Elements studentResult)
        {
            studentResult.Spanish = Spanish;

            lstStudentResult.Add(new StudentsResults
            {
                Spanish = Spanish,
                DiscoveriesYear = DiscoveriesYear,
                StudentName = studentResult
            });
        }

        public List<StudentsResults> ListStudentResult()
        {
            List<StudentsResults> lstconstituentetemp = new List<StudentsResults>();

            foreach (StudentsResults cons in lstStudentResult)
            {
                cons.Spanish = Spanish; //traduccion
                cons.DiscoveriesYear = DiscoveriesYear;
                lstconstituentetemp.Add(cons);
            }

            return lstconstituentetemp;

        }

    }
}
