﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures
{
    public class OutcomeStructure
    {
        public int IdOutcome { get; set; }
        public int IdTipoOutcomeCurso { get; set; }
        public string Icono { get; set; }
    }
}
