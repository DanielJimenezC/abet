﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures
{
    public class NivelStructure
    {
        public string Numero { get; set; }
        public bool EsNivelDeElectivos { get; set; }
        public List<CursoStructure> Cursos { get; set; }
    }
}
