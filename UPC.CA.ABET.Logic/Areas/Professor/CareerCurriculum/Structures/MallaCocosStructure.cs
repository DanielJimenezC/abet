﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures
{
    public class MallaCocosStructure
    {
        public MallaCocos MallaCocos { get; set; }
        public Carrera Carrera { get; set; }
        public PeriodoAcademico PeriodoAcademico { get; set; }
        public CarreraPeriodoAcademico CarreraPeriodoAcademico { get; set; }
        public MallaCurricular MallaCurricular { get; set; }
        public Comision Comision { get; set; }
        public string IconoFormacion { get; set; }
        public List<Outcome> Outcomes { get; set; }
        public int TotalFormacion { get; set; }
        public Dictionary<int, int> TotalPorOutcome { get; set; }
        public List<NivelStructure> Niveles { get; set; }
        public NivelStructure NivelElectivos { get; set; }
        public Dictionary<int, string> IconosTiposOutcomeCurso { get; set; }
        public Dictionary<int, TipoOutcomeCurso> DicTipoOutcomeCurso { get; set; }
    }
}
