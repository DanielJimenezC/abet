﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures
{
    public class CursoStructure
    {
        public CursoMallaCurricular Curso { get; set; }
        public bool EsFormacion { get; set; }
        public List<OutcomeStructure> Outcomes { get; set; }
    }
}
