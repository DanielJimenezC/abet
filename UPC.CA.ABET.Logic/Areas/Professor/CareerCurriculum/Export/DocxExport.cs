﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Builders;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export
{
    class DocxExport
    {
        private MallaCocosStructure _mallaCocosStructure = new MallaCocosStructure();

        public DocxExport(AbetEntities ctx, int idMallaCocos, ExportResourcesMallaCocos exportResources)
        {
            MallaCocosBuilder mallaCocosBuilder = new MallaCocosBuilder(ctx, exportResources);
            mallaCocosBuilder.BuildMallaCocos(idMallaCocos, _mallaCocosStructure);
        }

        public string GetDocx(string directory)
        {
            if (_mallaCocosStructure == null)
            {
                return String.Empty;
            }return "";
        }
    }
}
