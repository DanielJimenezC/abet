﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Builders
{
    public class MallaCocosBuilder
    {
        private Dictionary<int, string> _pathsIconosTipoOutcome;
        private string _pathLogoUpc;
        private string _pathIconoFormacion;
        private AbetEntities _ctx;

        public MallaCocosBuilder(AbetEntities ctx, ExportResourcesMallaCocos exportResources)
        {
            _pathsIconosTipoOutcome = exportResources.PathsIconosTipoOutcome;
            _pathLogoUpc = exportResources.PathLogoUpc;
            _pathIconoFormacion = exportResources.PathIconoFormacion;
            _ctx = ctx;
        }

        /// <summary>
        /// Método para construir y poblar toda la estructura de una malla de cocos de acuerdo a su código identificador único
        /// </summary>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <param name="mallaCocosStructure">Estructura a ser construida y poblada por el método</param>
        public void BuildMallaCocos(int idMallaCocos, MallaCocosStructure mallaCocosStructure)
        {
            mallaCocosStructure.MallaCocos = 
                GedServices.GetMallaCocosByIdServices(_ctx, idMallaCocos);

            if (mallaCocosStructure.MallaCocos == null)
            {
                mallaCocosStructure = null;
                return;
            }
            
            var carreraComision = GedServices.GetCarreraComisionServices(_ctx, mallaCocosStructure.MallaCocos.IdCarreraComision);
            var mallaCurricular = mallaCocosStructure.MallaCurricular = GedServices.GetMallaCurricularServices(_ctx, carreraComision.IdCarrera, mallaCocosStructure.MallaCocos.IdSubModalidadPeriodoAcademico.Value);

            // Obtener información general
            mallaCocosStructure.Carrera = carreraComision.Carrera;
            mallaCocosStructure.PeriodoAcademico = GedServices.GetPeriodoAcademicoServices(_ctx, mallaCocosStructure.MallaCocos.IdSubModalidadPeriodoAcademico.Value);
            mallaCocosStructure.Comision = carreraComision.Comision;
          
            mallaCocosStructure.Outcomes = GedServices.GetOutcomesInComisionPeriodoAcademicoServices(_ctx, mallaCocosStructure.Comision.IdComision, mallaCocosStructure.MallaCocos.IdSubModalidadPeriodoAcademico.Value);
            mallaCocosStructure.IconoFormacion = _pathIconoFormacion;
            mallaCocosStructure.TotalPorOutcome = new Dictionary<int, int>();
            mallaCocosStructure.IconosTiposOutcomeCurso = _pathsIconosTipoOutcome;
            mallaCocosStructure.DicTipoOutcomeCurso = new Dictionary<int, TipoOutcomeCurso>();

            // Generar niveles
            var mallaCocosDetalles = GedServices.GetMallaCocosDetallesInMallaCocosServices(_ctx, mallaCocosStructure.MallaCocos.IdMallaCocos);
            var dicMallaCocosDetalles = new Dictionary<Tuple<int, int>, MallaCocosDetalle>();
            mallaCocosDetalles.ForEach(x => dicMallaCocosDetalles.Add(new Tuple<int, int>((int)x.IdCursoMallaCurricular, x.IdOutcomeComision), x));

            var outcomeComisiones = GedServices.GetOutcomeComisionesInComisionInPeriodoAcademicoServices(_ctx, mallaCocosStructure.Comision.IdComision, mallaCocosStructure.MallaCocos.IdSubModalidadPeriodoAcademico);
            var dicOutcomeComisiones = new Dictionary<Tuple<int, int>, OutcomeComision>();
            outcomeComisiones.ForEach(x => dicOutcomeComisiones.Add(new Tuple<int, int>(x.IdOutcome, x.IdComision), x));

            var tipoOutcomes = GedServices.GetAllTipoOutcomeCursosServices(_ctx);
            tipoOutcomes.ForEach(x => mallaCocosStructure.DicTipoOutcomeCurso.Add(x.IdTipoOutcomeCurso, x));

            mallaCocosStructure.Outcomes.ForEach(x => mallaCocosStructure.TotalPorOutcome.Add(x.IdOutcome, 0));

            var niveles = new List<NivelStructure>();
            var nivelDeElectivos = new NivelStructure
            {
                Numero = "Cursos Electivos",
                EsNivelDeElectivos = true,
                Cursos = new List<CursoStructure>()
            };

            var dicNiveles = new Dictionary<int, NivelStructure>();

            foreach(var cursoMallaCurricular in mallaCocosStructure.MallaCurricular.CursoMallaCurricular)
            {
                int currentNivel = cursoMallaCurricular.NivelAcademico.Numero;
                bool esElectivo = cursoMallaCurricular.EsElectivo ?? false;

                if (!esElectivo && !dicNiveles.ContainsKey(currentNivel))
                {
                    dicNiveles[currentNivel] = new NivelStructure
                    {
                        Numero = currentNivel.ToString(),
                        Cursos = new List<CursoStructure>()
                    };
                }

                var cursoStructure = new CursoStructure
                {
                    Curso = cursoMallaCurricular,
                    EsFormacion = cursoMallaCurricular.EsFormacion ?? false
                };
                if (cursoStructure.EsFormacion)
                {
                    mallaCocosStructure.TotalFormacion++;
                }
                cursoStructure.Outcomes = new List<OutcomeStructure>();

                foreach(var outcome in mallaCocosStructure.Outcomes)
                {
                    var outcomeStructure = new OutcomeStructure();
                    outcomeStructure.IdOutcome = outcome.IdOutcome;

                    int idOutcomeComision = dicOutcomeComisiones[new Tuple<int, int>(outcome.IdOutcome, mallaCocosStructure.Comision.IdComision)].IdOutcomeComision;

                    if (!dicMallaCocosDetalles.ContainsKey(new Tuple<int, int>(cursoMallaCurricular.IdCursoMallaCurricular, idOutcomeComision)))
                    {
                        outcomeStructure.Icono = String.Empty;
                    }
                    else
                    {
                        var mallaCocosDetalle = dicMallaCocosDetalles[new Tuple<int, int>(cursoMallaCurricular.IdCursoMallaCurricular, idOutcomeComision)];
                        outcomeStructure.IdTipoOutcomeCurso = mallaCocosDetalle.IdTipoOutcomeCurso ?? 0;
                        outcomeStructure.Icono = _pathsIconosTipoOutcome[outcomeStructure.IdTipoOutcomeCurso] ?? String.Empty;
                        mallaCocosStructure.TotalPorOutcome[outcomeStructure.IdOutcome]++;
                    }

                    cursoStructure.Outcomes.Add(outcomeStructure);
                }

                if(!esElectivo)
                {
                    dicNiveles[currentNivel].Cursos.Add(cursoStructure);
                }
                else
                {
                    nivelDeElectivos.Cursos.Add(cursoStructure);
                }
            }

            foreach (var nivelStructure in dicNiveles.Values)
            {
                niveles.Add(nivelStructure);
            }

            niveles = niveles.OrderBy(x => Convert.ToInt32(x.Numero)).ToList();

            mallaCocosStructure.Niveles = niveles;
            mallaCocosStructure.NivelElectivos = nivelDeElectivos;
        }
    }
}
