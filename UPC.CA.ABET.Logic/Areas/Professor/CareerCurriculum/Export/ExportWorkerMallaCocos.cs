﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Builders;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Structures;
using UPC.CA.ABET.Models;
using System.IO;
using System.Web;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export
{
    public enum ExportFileFormat
    {
        Pdf,
        Rtf
    }

    public class ExportWorkerMallaCocos
    {
        private MallaCocosStructure _mallaCocosStructure = new MallaCocosStructure();
        private string _logoUpc;
        private String _culture;

        public ExportWorkerMallaCocos(AbetEntities ctx, int idMallaCocos, ExportResourcesMallaCocos exportResources, String culture)
        {
            MallaCocosBuilder mallaCocosBuilder = new MallaCocosBuilder(ctx, exportResources);
            mallaCocosBuilder.BuildMallaCocos(idMallaCocos, _mallaCocosStructure);
            _logoUpc = exportResources.PathLogoUpc;
            _culture = culture;
        }

        /// <summary>
        /// Método para obtener la dirección del archivo de exportación creado para la malla de cocos
        /// </summary>
        /// <param name="fef">Formato de archivo para la exportación</param>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo de exportación creado para la malla de cocos</returns>
        public string GetFile(ExportFileFormat fef, string directory, string culture)
        {
            if (_mallaCocosStructure == null)
            {
                return String.Empty;
            }

            switch(fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory, culture);
                case ExportFileFormat.Rtf: return GetRtf(directory, culture);
                default: return String.Empty;
            }
        }

        /// <summary>
        /// Método para obtener la dirección del archivo PDF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo PDF creado para la malla de cocos</returns>
        private string GetPdf(string directory, string culture)
        {
            Document document = CreateDocument();

            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            string filename;
            if (culture == ConstantHelpers.CULTURE.ESPANOL)
            { 
                  filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" + 
                  _mallaCocosStructure.Carrera.NombreEspanol.Replace(" ", "")  +
                 "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".pdf";
            }
            else if(culture == ConstantHelpers.CULTURE.INGLES)
            {
                filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" +
                 _mallaCocosStructure.Carrera.NombreIngles.Replace(" ", "") +
                "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".pdf";
            }
            else
            {
                filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" +
                 _mallaCocosStructure.Carrera.NombreEspanol.Replace(" ", "") +
                "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".pdf";
            }
            //string path = directory + "\\" + filename;
            string path = Path.Combine(directory, filename);

            HttpContext context = HttpContext.Current;


            renderer.PdfDocument.Save(path);

            return path;
        }

        /// <summary>
        /// Método para obtener la dirección del archivo RTF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo RTF creado para la malla de cocos</returns>
        private string GetRtf(string directory, string culture)
        {
            Document document = CreateDocument();

            string filename;
            if (culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" +
                _mallaCocosStructure.Carrera.NombreEspanol.Replace(" ", "") +
               "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".rtf";
            }
            else if (culture == ConstantHelpers.CULTURE.INGLES)
            {
                filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" +
                 _mallaCocosStructure.Carrera.NombreIngles.Replace(" ", "") +
                "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".rtf";
            }
            else
            {
                filename = "MallaCocos_" + _mallaCocosStructure.Comision.NombreEspanol.Replace(" ", "") + "_" +
                 _mallaCocosStructure.Carrera.NombreEspanol.Replace(" ", "") +
                "_" + _mallaCocosStructure.PeriodoAcademico.CicloAcademico.Replace(" ", "") + ".rtf";
            }

            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);

            string path = Path.Combine(directory, filename);

            return path;
        }

        /// <summary>
        /// Método para crear el documento de exportación de malla de cocos
        /// </summary>
        /// <returns>Documento que contiene la información necesaria para generar el archivo de exportación</returns>
        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);
            SetContentSection(document);
            SetDetail(document);
            SetTableCursosObligatorios(document);
            AddSeparation(document, 5, "mm");
            SetLegend(document);
            AddSeparation(document, 5, "mm");
            SetTableCursosElectivos(document);

            return document;
        }

        /// <summary>
        /// Método para especificar la leyenda en el documento
        /// </summary>
        /// <param name="document">Documento en el que se especificará la leyenda</param>
        private void SetLegend(Document document)
        {
            Section section = document.LastSection;
            section.PageSetup.LeftMargin = 30;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column columnSimbolo = table.AddColumn();
            columnSimbolo.Format.Alignment = ParagraphAlignment.Center;
            columnSimbolo.Width = 20;

            Column columnNombre = table.AddColumn();
            columnNombre.Format.Alignment = ParagraphAlignment.Left;
            columnNombre.Width = 120;

            Row rowTitle = table.AddRow();
            rowTitle.VerticalAlignment = VerticalAlignment.Center;
            rowTitle.HeadingFormat = true;
            rowTitle.Shading.Color = Colors.LightSteelBlue;
            rowTitle.VerticalAlignment = VerticalAlignment.Center;
            rowTitle.Height = 15;
            rowTitle.Format.Font.Bold = true;


            rowTitle[0].AddParagraph( _culture == ConstantHelpers.CULTURE.ESPANOL ? "LEYENDA" : "LEGEND");
            rowTitle[0].MergeRight = 1;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Height = 15;

            Paragraph paragraph = row[0].AddParagraph();
            Image img = paragraph.AddImage(_mallaCocosStructure.IconoFormacion);
            img.LockAspectRatio = true;
            img.Width = 9;

            row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Curso Formativo": "Formative Course");

            foreach (var key in _mallaCocosStructure.IconosTiposOutcomeCurso.Keys)
            {
                row = table.AddRow();
                row.Height = 15;
                row.VerticalAlignment = VerticalAlignment.Center;

                paragraph = row[0].AddParagraph();
                img = paragraph.AddImage(_mallaCocosStructure.IconosTiposOutcomeCurso[key]);
                img.LockAspectRatio = true;
                img.Width = 9;

                row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? _mallaCocosStructure.DicTipoOutcomeCurso[key].NombreEspanol : _mallaCocosStructure.DicTipoOutcomeCurso[key].NombreIngles);
            }
        }

        /// <summary>
        /// Método para ingresar un espacio de separación en un documento
        /// </summary>
        /// <param name="document">Doucmento en el que se ingresará el espacio de separación</param>
        /// <param name="qty">Cantidad de unidades que tendrá el espacio de separación</param>
        /// <param name="unit">Unidad en la que se medirá el espacio de separación</param>
        private void AddSeparation(Document document, int qty, string unit)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("");
            paragraph.Format.SpaceAfter = qty.ToString() + unit;
        }

        /// <summary>
        /// Método para ingresar la tabla de cursos obligatorios en el documento de malla de cocos
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetTableCursosObligatorios(Document document)
        {
            Section section = document.LastSection;
            section.PageSetup.LeftMargin = 30;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            int amountOfColumns = 3 + _mallaCocosStructure.Outcomes.Count(); // 3 = Código de curso, Nombre de curso, Formación

            Column columnCodigo = table.AddColumn();
            columnCodigo.Format.Alignment = ParagraphAlignment.Center;
            columnCodigo.Width = 50;

            Column columnNombreAsignatura = table.AddColumn();
            columnNombreAsignatura.Format.Alignment = ParagraphAlignment.Center;
            columnNombreAsignatura.Width = 240;

            Column columnCF = table.AddColumn();
            columnCF.Format.Alignment = ParagraphAlignment.Center;
            columnCF.Width = 20;

            for (int i = 3; i < amountOfColumns; i++)
            {
                Column columnStudentOutcome = table.AddColumn();
                columnStudentOutcome.Format.Alignment = ParagraphAlignment.Center;
                columnStudentOutcome.Width = 20;
            }

            // Agregar dos filas para la cabecera
            Row studentOutcomesTitleRow = table.AddRow();
            studentOutcomesTitleRow.VerticalAlignment = VerticalAlignment.Center;
            studentOutcomesTitleRow.HeadingFormat = true;
            studentOutcomesTitleRow.Shading.Color = Colors.LightSteelBlue;
            studentOutcomesTitleRow.Height = 15;
            studentOutcomesTitleRow.Format.Font.Bold = true;
            Row headersRow = table.AddRow();
            headersRow.VerticalAlignment = VerticalAlignment.Center;
            headersRow.HeadingFormat = true;
            headersRow.Shading.Color = Colors.LightSteelBlue;
            headersRow.Height = 15;
            headersRow.Format.Font.Bold = true;

            studentOutcomesTitleRow[0].MergeDown = 1;
            studentOutcomesTitleRow[1].MergeDown = 1;
            studentOutcomesTitleRow[2].MergeDown = 1;
            studentOutcomesTitleRow[3].MergeRight = _mallaCocosStructure.Outcomes.Count() - 1;
            Paragraph studentOutcomesTitleParagraph = studentOutcomesTitleRow[3].AddParagraph();
            studentOutcomesTitleParagraph.AddText("STUDENT OUTCOMES");
            studentOutcomesTitleParagraph.Format.Alignment = ParagraphAlignment.Center;

            studentOutcomesTitleRow[0].AddParagraph("CÓD.");
            studentOutcomesTitleRow[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "ASIGNATURA" : "ASIGNATURE");
            studentOutcomesTitleRow[2].AddParagraph("CF");

            for(int i = 3; i < amountOfColumns; i++)
            {
                headersRow[i].AddParagraph(_mallaCocosStructure.Outcomes[i - 3].Nombre);
            }

            foreach(var nivel in _mallaCocosStructure.Niveles)
            {
                Row rowNivel = table.AddRow();
                rowNivel[0].MergeRight = amountOfColumns - 1;
                if (_culture == ConstantHelpers.CULTURE.ESPANOL)
                    rowNivel[0].AddParagraph("NIVEL " + nivel.Numero.ToString()).Format.Alignment = ParagraphAlignment.Left;
                else
                    rowNivel[0].AddParagraph("LEVEL " + nivel.Numero.ToString()).Format.Alignment = ParagraphAlignment.Left; 
                rowNivel.VerticalAlignment = VerticalAlignment.Center;
                rowNivel.Shading.Color = Colors.LightGray;
                rowNivel.Height = 15;
                rowNivel.Format.Font.Bold = true;

                foreach (var curso in nivel.Cursos)
                {
                    Row rowCurso = table.AddRow();
                    rowCurso.VerticalAlignment = VerticalAlignment.Center;
                    rowCurso.Height = 15;

                    rowCurso[0].AddParagraph(curso.Curso.Curso.Codigo);
                    rowCurso[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL? curso.Curso.Curso.NombreEspanol : curso.Curso.Curso.NombreIngles).Format.Alignment = ParagraphAlignment.Left;
                    if(curso.EsFormacion)
                    {
                        Paragraph paragraph = rowCurso[2].AddParagraph();
                        Image imgFormacion = paragraph.AddImage(_mallaCocosStructure.IconoFormacion);
                        imgFormacion.LockAspectRatio = true;
                        imgFormacion.Width = 9;
                    }
                    for(int i = 0; i < curso.Outcomes.Count(); i++)
                    {
                        if(curso.Outcomes[i].IdTipoOutcomeCurso != 0)
                        {
                            Paragraph paragraph = rowCurso[3 + i].AddParagraph();
                            Image img = paragraph.AddImage(curso.Outcomes[i].Icono);
                            img.LockAspectRatio = true;
                            img.Width = 9;
                        }
                    }
                }
            }

            Row rowTotal = table.AddRow();
            rowTotal.Height = 20;
            rowTotal.Format.Font.Bold = true;
            rowTotal.VerticalAlignment = VerticalAlignment.Center;
            rowTotal.Shading.Color = Colors.LightSteelBlue;
            rowTotal[0].MergeRight = 1;
            rowTotal[0].AddParagraph("TOTAL");
            rowTotal[2].AddParagraph(_mallaCocosStructure.TotalFormacion.ToString());
            for(int i = 0; i < _mallaCocosStructure.Outcomes.Count(); i++)
            {
                rowTotal[i + 3].AddParagraph(_mallaCocosStructure.TotalPorOutcome[_mallaCocosStructure.Outcomes[i].IdOutcome].ToString());
            }
        }

        /// <summary>
        /// Método para ingresar la tabla de cursos electivos en el documento de malla de cocos
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetTableCursosElectivos(Document document)
        {
            Section section = document.LastSection;
            section.PageSetup.LeftMargin = 30;

            Table table = section.AddTable();
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            int amountOfColumns = 3 + _mallaCocosStructure.Outcomes.Count(); // 3 = Código de curso, Nombre de curso, Formación

            Column columnCodigo = table.AddColumn();
            columnCodigo.Format.Alignment = ParagraphAlignment.Center;
            columnCodigo.Width = 50;

            Column columnNombreAsignatura = table.AddColumn();
            columnNombreAsignatura.Format.Alignment = ParagraphAlignment.Center;
            columnNombreAsignatura.Width = 240;

            Column columnCF = table.AddColumn();
            columnCF.Format.Alignment = ParagraphAlignment.Center;
            columnCF.Width = 20;

            for (int i = 3; i < amountOfColumns; i++)
            {
                Column columnStudentOutcome = table.AddColumn();
                columnStudentOutcome.Format.Alignment = ParagraphAlignment.Center;
                columnStudentOutcome.Width = 20;
            }

            // Agregar dos filas para la cabecera
            Row studentOutcomesTitleRow = table.AddRow();
            studentOutcomesTitleRow.VerticalAlignment = VerticalAlignment.Center;
            studentOutcomesTitleRow.HeadingFormat = true;
            studentOutcomesTitleRow.Shading.Color = Colors.LightSteelBlue;
            studentOutcomesTitleRow.Height = 15;
            studentOutcomesTitleRow.Format.Font.Bold = true;
            Row headersRow = table.AddRow();
            headersRow.VerticalAlignment = VerticalAlignment.Center;
            headersRow.HeadingFormat = true;
            headersRow.Shading.Color = Colors.LightSteelBlue;
            headersRow.Height = 15;
            headersRow.Format.Font.Bold = true;

            studentOutcomesTitleRow[0].MergeDown = 1;
            studentOutcomesTitleRow[1].MergeDown = 1;
            studentOutcomesTitleRow[2].MergeDown = 1;
            studentOutcomesTitleRow[3].MergeRight = _mallaCocosStructure.Outcomes.Count() - 1;
            Paragraph studentOutcomesTitleParagraph = studentOutcomesTitleRow[3].AddParagraph();
            studentOutcomesTitleParagraph.AddText("STUDENT OUTCOMES");
            studentOutcomesTitleParagraph.Format.Alignment = ParagraphAlignment.Center;

            studentOutcomesTitleRow[0].AddParagraph("CÓD.");
            studentOutcomesTitleRow[1].AddParagraph(_culture==ConstantHelpers.CULTURE.ESPANOL?"CURSOS ELECTIVOS": "ELECTIVES COURSES");
            studentOutcomesTitleRow[2].AddParagraph("CF");

            for (int i = 3; i < amountOfColumns; i++)
            {
                headersRow[i].AddParagraph(_mallaCocosStructure.Outcomes[i - 3].Nombre);
            }

            foreach (var curso in _mallaCocosStructure.NivelElectivos.Cursos)
            {
                Row rowCurso = table.AddRow();
                rowCurso.VerticalAlignment = VerticalAlignment.Center;
                rowCurso.Height = 15;

                rowCurso[0].AddParagraph(curso.Curso.Curso.Codigo);
                rowCurso[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? curso.Curso.Curso.NombreEspanol: curso.Curso.Curso.NombreIngles).Format.Alignment = ParagraphAlignment.Left;
                if (curso.EsFormacion)
                {
                    Paragraph paragraph = rowCurso[2].AddParagraph();
                    Image imgFormacion = paragraph.AddImage(_mallaCocosStructure.IconoFormacion);
                    imgFormacion.LockAspectRatio = true;
                    imgFormacion.Width = 9;
                }
                for (int i = 0; i < curso.Outcomes.Count(); i++)
                {
                    if (curso.Outcomes[i].IdTipoOutcomeCurso != 0)
                    {
                        Paragraph paragraph = rowCurso[3 + i].AddParagraph();
                        Image img = paragraph.AddImage(curso.Outcomes[i].Icono);
                        img.LockAspectRatio = true;
                        img.Width = 9;
                    }
                }
            }
        }

        /// <summary>
        /// Método para especificar el numerado de páginas
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }

        /// <summary>
        /// Método para ingresar los detalles de la malla de cocos al inicio del documento
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetDetail(Document document)
        {
            Section section = document.LastSection;

            Image image = section.AddImage(_logoUpc);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");
            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.SpaceAfter = "15mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph("MALLA DE COCOS", "Heading2");
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;

            paragraph = section.AddParagraph(_culture==ConstantHelpers.CULTURE.ESPANOL?"COMISIÓN:\t": "COMMISSION:\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            paragraph.AddFormattedText(_mallaCocosStructure.Comision.NombreEspanol, "Heading3").Font.Color = Colors.Black;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CARRERA:\t": "CAREER:\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            paragraph.AddFormattedText(_culture == ConstantHelpers.CULTURE.ESPANOL ? _mallaCocosStructure.Carrera.NombreEspanol : _mallaCocosStructure.Carrera.NombreIngles, "Heading3").Font.Color = Colors.Black;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CICLO:\t\t": "SEMESTER:\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.KeepWithNext = true;
            paragraph.AddFormattedText(_mallaCocosStructure.PeriodoAcademico.CicloAcademico, "Heading3").Font.Color = Colors.Black;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "PLAN CURRICULAR":"CAREER CURRICULUM", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.LeftIndent = 30;
        }

        /// <summary>
        /// Método para definir los estilos a utilizar en la creación del documento de malla de cocos
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.Font.Size = 9;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }
    }
}
