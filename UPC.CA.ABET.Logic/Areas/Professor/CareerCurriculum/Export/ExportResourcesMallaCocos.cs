﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export
{
    public class ExportResourcesMallaCocos
    {
        public Dictionary<int, string> PathsIconosTipoOutcome { get; set; }
        public string PathLogoUpc { get; set; }
        public string PathIconoFormacion { get; set; }

        public ExportResourcesMallaCocos()
        {
            PathsIconosTipoOutcome = new Dictionary<int, string>();
        }
    }
}
