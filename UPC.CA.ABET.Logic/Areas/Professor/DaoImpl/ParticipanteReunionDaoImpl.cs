﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;



namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class ParticipanteReunionDaoImpl
    {
        /// <summary>
        /// Método para obtener al participante de una reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param param name="idDocente">Código identificador del docente</param>
        /// <param name="idReunion">Código identificador de la reunión</param>
        /// <returns>Participante relacionado a la reunión e id ingresado</returns>
        public static ParticipanteReunion GetParticipante(AbetEntities ctx, int idDocente, int idReunion)
        {
            return (from obj in ctx.ParticipanteReunion
                    where obj.IdDocente == idDocente && obj.IdReunion == idReunion
                    select obj).SingleOrDefault();
        }

        public static List<ParticipanteReunion> GetParticipantesByReunion(AbetEntities ctx, int idReunion)
        {
            return (from obj in ctx.ParticipanteReunion
                    where obj.IdReunion == idReunion
                    select obj).ToList();
        }

        public static ParticipanteReunion GetParticipanteExterno(AbetEntities ctx, int idParticipanteReunion)
        {
            return (from obj in ctx.ParticipanteReunion
                    where obj.IdParticipanteReunion == idParticipanteReunion
                    select obj).SingleOrDefault();
        }

        public static ParticipanteReunion CrearParticipanteExterno(AbetEntities ctx, ParticipanteReunion participante)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    ctx.ParticipanteReunion.Add(participante);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return participante;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        public static bool ElminarParticipanteExterno(AbetEntities ctx, int idParticipante)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var participante = GedServices.GetParticipanteExternoServices(ctx, idParticipante);
                    if (participante != null)
                    {
                        ctx.ParticipanteReunion.Remove(participante);
                        ctx.SaveChanges();
                        transaction.Commit();

                        return true;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para crear un participante si previamente no existe en la reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param param name="idDocente">Código identificador del docente</param>
        /// <param name="idReunion">Código identificador de la reunión</param>
        /// <returns>Participante creado en la reunión</returns>
        public static ParticipanteReunion CrearParticipanteSiExiste(AbetEntities ctx, int idDocente, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var aux = GedServices.GetParticipanteServices(ctx, idDocente, idReunion);
                    if (aux == null)
                    {
                        var docente = GedServices.GetDocenteServices(ctx, idDocente);
                        var participante = new ParticipanteReunion();
                        participante.IdReunion = idReunion;
                        participante.IdDocente = docente.IdDocente;
                        participante.Asistio = false;
                        participante.NombreCompleto = docente.Nombres + " " + docente.Apellidos;

                        ctx.ParticipanteReunion.Add(participante);
                        ctx.SaveChanges();
                        transaction.Commit();

                        return participante;
                    }
                    return aux;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        /// <summary>
        /// Método para eliminar un participante si existe en la reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param param name="idDocente">Código identificador del docente</param>
        /// <param name="idReunion">Código identificador de la reunión</param>
        /// <returns>Confirmación de participante elminado</returns>
        public static bool ElminarParticipanteSiExiste(AbetEntities ctx, int idDocente, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var participante = GedServices.GetParticipanteServices(ctx, idDocente, idReunion);
                    if (participante != null)
                    {
                        ctx.ParticipanteReunion.Remove(participante);
                        ctx.SaveChanges();
                        transaction.Commit();

                        return true;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}
