﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class CursoMallaCurricularDaoImpl
    {
        /// <summary>
        /// Método para obtener un curso perteneciente a una malla curricular de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCursoMallaCurricular">Código identificador del curso perteneciente a una malla curricular</param>
        /// <returns>Curso perteneciente a una malla curricular que tenga el identificador ingresado</returns>
        public static CursoMallaCurricular GetCursoMallaCurricular(AbetEntities ctx, int idCursoMallaCurricular)
        {
            var result = (from c in ctx.CursoMallaCurricular
                          where c.IdCursoMallaCurricular == idCursoMallaCurricular
                          select c).FirstOrDefault();

            return result;
        }



        /// <summary>
        /// Método para obtener un curso por malla curricular de acuerdo al código identificador de un curso para la creación de un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCurso">Código identificador del curso</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Curso por malla curricular que cumple con los criterios ingresados</returns>
        public static CursoMallaCurricular GetCursoMallaCurricularForIfc(AbetEntities ctx, int idCurso, int idSubModalidadPeriodoAcademico)
        {
            CursoMallaCurricular cursomc =(from cmc in ctx.CursoMallaCurricular
                    join mcpa in ctx.MallaCurricularPeriodoAcademico on cmc.IdMallaCurricular equals mcpa.IdMallaCurricular
                    where cmc.IdCurso == idCurso && mcpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                    select cmc).FirstOrDefault();
            if(cursomc.LogroFinCicloEspanol == null)
            cursomc.LogroFinCicloEspanol = "";
            if (cursomc.LogroFinCicloIngles == null)
                cursomc.LogroFinCicloIngles = "";
            return cursomc;
        }
    }
}
