﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class MallaCocosDetalleDaoImpl
    {

        /// <summary>
        /// Método para obtener todos los de talles de una malla de cocos
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <returns>Lista con todos los detalles de la malla de cocos ingresada</returns>
        public static List<MallaCocosDetalle> GetMallaCocosDetallesInMallaCocos(AbetEntities ctx, int idMallaCocos)
        {
            return (from mcd in ctx.MallaCocosDetalle
                    where mcd.IdMallaCocos == idMallaCocos
                    select mcd).ToList();
        }

        /// <summary>
        /// Método para obtener un detalle de malla de cocos de acuerdo a un curso perteneciente a una malla curricular y un outcome perteneciente a una comisión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCursoMallaCurricular">Código identificador del curso perteneciente a una malla curricular</param>
        /// <param name="idOutcomeComision">Código identificador de un outcome perteneciente a una comisión</param>
        /// <returns>Detalle de malla de cocos que cumpla con los criterios</returns>
        public static MallaCocosDetalle GetMallaCocosDetalle(AbetEntities ctx, int idCursoMallaCurricular, int idOutcomeComision)
        {
            return (from mccd in ctx.MallaCocosDetalle
                    where mccd.IdCursoMallaCurricular == idCursoMallaCurricular &&
                    mccd.IdOutcomeComision == idOutcomeComision
                    select mccd).FirstOrDefault();
        }

    }
}
