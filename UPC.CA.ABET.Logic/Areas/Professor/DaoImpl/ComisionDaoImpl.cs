﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class ComisionDaoImpl
    {
        /// <summary>
        /// Método para obtener todas las comisiones existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista conteniendo todas las comisiones existentes</returns>
        public static List<Comision> GetAllComisions(AbetEntities ctx)
        {
            return (from c in ctx.Comision
                    select c).ToList();
        }

        /// <summary>
        /// Método para obtener una comisión de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <returns>Comisión que tenga el código identificador ingresado</returns>
        public static Comision GetComision(AbetEntities ctx, int idComision)
        {
            return (from c in ctx.Comision where c.IdComision == idComision select c).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener una comisión de acuerdo a los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigo">Código de la comisión</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Comisión que cumpla con los criterios ingresados</returns>
        public static Comision GetComisionFromCodigoAndPeriodoAcademico(AbetEntities ctx, string codigo, int idSubModalidadPeriodoAcademico)
        {
            return (from c in ctx.Comision
                    join apa in ctx.AcreditadoraPeriodoAcademico on c.IdAcreditadoraPeriodoAcademico equals apa.IdAcreditadoraPreiodoAcademico
                    where apa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                        && c.Codigo == codigo
                    select c).First();
        }

        /// <summary>
        /// Método para obtener los códigos de todas las comisiones existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con los códigos de todas las comisiones existentes</returns>
        public static IEnumerable<KeyValuePair<string, string>> GetAllCodigosFromComisiones(AbetEntities ctx, int idEscuela, Int32? IdCarrera, Int32? IdSubModalidadPeriodoAcaademico, Int32? IdAcreditadora)
        {
            var query = ctx.CarreraComision.Where(cc => cc.Carrera.IdEscuela == idEscuela && cc.Comision.Codigo != "CAC-CC");

            int cantidad = query.Count();

            if (IdSubModalidadPeriodoAcaademico != null)
                query = query.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcaademico);
            if (IdAcreditadora != null)
                query = query.Where(x => x.Comision.AcreditadoraPeriodoAcademico.Acreditadora.IdAcreditadora == IdAcreditadora);
            if (IdCarrera != null)
                query = query.Where(x => x.Carrera.IdCarrera == IdCarrera);

            return query.Select(cc => new { Key = cc.Comision.Codigo, Name = cc.Comision.NombreEspanol }).Distinct().ToList()
            .Select(type => new KeyValuePair<string, string>(type.Key, type.Name));

            //return (from c in ctx.Comision where c.Codigo != "CAC-CC" select c.Codigo).Distinct().ToList();
            //return (from c in ctx.Comision select c.Codigo).Distinct().ToList();
        }
    }

}
