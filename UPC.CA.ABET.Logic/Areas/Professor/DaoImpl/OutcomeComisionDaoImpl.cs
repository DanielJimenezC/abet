﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class OutcomeComisionDaoImpl
    {
        /// <summary>
        /// Método para obtener el outcome por comisión de acuerdo a los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idOutcome">Código identificador del outcome</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <returns>Outcome por comisión que cumpla con los criterios ingresados</returns>
        public static OutcomeComision GetOutcomeComisionInOutcomeComision(AbetEntities ctx, int idOutcome, int idComision)
        {
            return (from oc in ctx.OutcomeComision
                    where oc.IdOutcome == idOutcome && oc.IdComision == idComision
                    select oc).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener todos los outcomes por comisión que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todos los outcomes por comisión que cumplan con los criterios ingresados</returns>
        public static List<OutcomeComision> GetOutcomeComisionesInComisionInPeriodoAcademico(AbetEntities ctx, Int32? idComision, Int32? IdPeriodoAcademico)
        { 
            //ENTRA PERIODOACADEMICO 
            int? idsubmoda = IdPeriodoAcademico;

             //Convertir periodoacademico en idsubmodalidad
            //if (IdPeriodoAcademico != null)
            //{
            ////    idsubmoda = ctx.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == IdPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            //}

            return (from oc in ctx.OutcomeComision
                    where oc.IdComision == idComision && oc.IdSubModalidadPeriodoAcademico == idsubmoda
                    select oc).ToList();
        }

    }
}
