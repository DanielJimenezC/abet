﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;



namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class SedeDaoImpl
    {
        public static List<Sede> GetAllSedes(AbetEntities ctx)
        {
            return (from s in ctx.Sede select s).ToList();
        }

        public static List<Sede> GetAllSedesByPeriodo( AbetEntities ctx, int idperiodo)
        {
            return ctx.Sede.Where(X => X.PeriodoAcademicoSede.Any(Y => Y.SubModalidadPeriodoAcademico.IdPeriodoAcademico == idperiodo)).ToList();


            //Legacy
            return (from s in ctx.Sede
                    join pas in ctx.PeriodoAcademicoSede on s.IdSede equals pas.IdSede
                    join pa in ctx.PeriodoAcademico on pas.IdSubModalidadPeriodoAcademico equals pa.IdPeriodoAcademico
                    where pa.IdPeriodoAcademico == idperiodo select s).ToList();

        }



        /// <summary>
        /// Método para obtener la sede a partir del código de un hallazgo de IFC
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigo">Código del hallazgo de IFC</param>
        /// <returns>Sede que cumpla con los criterios</returns>
        public static Sede GetSedeFromCodigoHallazgoIfc(AbetEntities ctx, string codigo)
        {
            var elementos = codigo.Split(new char[] { '-' });
            var codigoSede = elementos[4];
            return (from s in ctx.Sede where s.Codigo == codigoSede select s).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener las sedes disponibles para la creación del informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idCursoUnidadAcademica">Código identificador de la unidad académica que representa el curso</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las sedes disponibles para el docente para realizar el informe de fin de ciclo</returns>
        public static List<Sede> GetSedesForIfc(AbetEntities ctx, int idDocente, int idCursoUnidadAcademica, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join s in ctx.Sede on sua.IdSede equals s.IdSede
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                          && ua.Nivel == 3
                          && ua.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                          && ua.IdUnidadAcademica == idCursoUnidadAcademica
                          && ua.IdEscuela == idEscuela
                    select s).Distinct().ToList();
            }

    }
}
