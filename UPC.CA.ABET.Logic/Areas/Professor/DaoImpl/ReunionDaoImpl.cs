﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;



namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class ReunionDaoImpl
    {
        /// <summary>
        /// Método para obtener la lista de todas las reuniones
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista de todas las reuniones</returns>
        public static List<Reunion> GetAllReuniones(AbetEntities ctx)
        {
            return (from s in ctx.Reunion select s).ToList();
        }

        /// <summary>
        /// Método para obtener una reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Identificador de la reunión</param>
        /// <returns>Reunión obtenida de acuerdo al identificador ingresado</returns>
        public static Reunion GetReunion(AbetEntities ctx, int idReunion)
        {
            return (from s in ctx.Reunion where s.IdReunion == idReunion select s).SingleOrDefault();
        }

        /// <summary>
        /// Método obtener la reunión asociada a una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idPreAgenda">Identificador de PreAgenda</param>
        /// <returns>Reunión asociada a la PreAgenda ingresada</returns>
        public static Reunion GetReunionByPreAgenda(AbetEntities ctx, int idPreAgenda)
        {
            return (from s in ctx.Reunion where s.IdPreAgenda == idPreAgenda select s).SingleOrDefault();
        }

        /// <summary>
        /// Método obtener la reunión asociada a una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="detalleReunion">objeto de Detalle de Reunión</param>
        /// <returns>Detalle de la reunión ingresada</returns>
        public static int CrearTemaEnDetalleReunion(AbetEntities ctx, List<TemaReunion> detalleReunion)
        {
            //using (var transaction = ctx.Database.BeginTransaction())
            //{
            //    try
            //    {
            //        ctx.DetalleReunion.Add(detalleReunion);
            //        ctx.SaveChanges();
            //        transaction.Commit();
            //        return detalleReunion.IdDetalleReunion;
            //    }
            //    catch(Exception ex)
            //    {
            //        transaction.Rollback();
            //        return 0;
            //    }
            //}
            return 0;
        }

        /// <summary>
        /// Método obtener los temas de una reunión en el detalle
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Identificador de Reunión</param>
        /// <returns>Lista los detalles de la reunión</returns>
        public static List<TemaReunion> GetAllTemasReunion(AbetEntities ctx, int idReunion)
        {
            return (from s in ctx.TemaReunion where s.IdReunion == idReunion select s).ToList();
        }

        /// <summary>
        /// Método para realizar la búsqueda de Reuniones
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idNivel">Nivel jerárquico</param>
        /// <param name="idSemana">Semana académica</param>
        /// <param name="FechaInicio">Fecha de inicio del rango de fechas de búsqueda</param>
        /// <param name="FechaFin">Fecha de fin del rango de fechas de búsqueda</param>
        /// <param name="idSede">Identificador de sede</param>
        /// <returns>Lista de Reuniones encontradas de acuerdo a los parámetros de búsqueda</returns>
        public static List<Reunion> GetReunionesBusqueda(AbetEntities ctx, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede)
        {
            var query = from s in ctx.Reunion where !s.Motivo.Equals("") && FechaInicio <= s.Fecha && FechaFin >= s.Fecha  select s;
            
            FechaFin = FechaFin.Value.AddDays(1);

            if (idNivel != null)
                query = query.Where(m => m.PreAgenda.Nivel == idNivel);
            if (idSemana != null)
                query = query.Where(m => m.PreAgenda.Semana == idSemana);
            if (FechaInicio != null)
                query = query.Where(m => m.Fecha >= FechaInicio);
            if (FechaFin != null)
                query = query.Where(m => m.Fecha <= FechaFin);
            if (idSede != null)
                query = query.Where(m => m.PreAgenda.IdSede == idSede);

            try
            {
                return query.OrderBy(m => m.PreAgenda.Nivel).ThenBy(m => m.PreAgenda.Semana).ThenBy(m => m.Fecha).ToList();
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static List<Reunion> GetReunionesBusquedaByUsuario(AbetEntities ctx, HttpSessionStateBase Session, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede)
        {
            int auxNivelUsuario = GedServices.GetNivelMaximoUsuarioLogueadoServices(ctx, Session.GetDocenteId(), Session);
            IEnumerable<Reunion> lstReuniones = from s in ctx.Reunion where !s.PreAgenda.Motivo.Equals("") && s.PreAgenda.SubModalidadPeriodoAcademico.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && s.PreAgenda.Nivel >= auxNivelUsuario select s;

            if (idNivel != null)
                lstReuniones = lstReuniones.Where(m => m.PreAgenda.Nivel == idNivel);
            if (idSemana != null)
                lstReuniones = lstReuniones.Where(m => m.PreAgenda.Semana == idSemana);
            if (FechaInicio != null)
                lstReuniones = lstReuniones.Where(m => m.Fecha >= FechaInicio);
            if (FechaFin != null)
                lstReuniones = lstReuniones.Where(m => m.Fecha <= FechaFin);
            if (idSede != null)
                lstReuniones = lstReuniones.Where(m => m.PreAgenda.IdSede == idSede);
            return lstReuniones.OrderBy(m => m.PreAgenda.Nivel).ThenBy(m => m.PreAgenda.Semana).ThenBy(m => m.Fecha).ToList();
        }

        /// <summary>
        /// Método para crear una Reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="reunion">Objeto Reunion a ser insertado</param>
        /// <returns>Reunion creada</returns>
        public static Reunion CrearReunion(AbetEntities ctx, Reunion reunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    if (reunion.Frecuencia > 0)
                    {
                        var ciclo = ctx.PeriodoAcademico.FirstOrDefault(x => x.FechaInicioPeriodo<= reunion.Fecha && x.FechaFinPeriodo>= reunion.Fecha);

                        TimeSpan ts = ConvertHelpers.ToDateTime(ciclo.FechaFinPeriodo) - reunion.Fecha;
                        int frec = ConvertHelpers.ToInteger(reunion.Frecuencia);
                        int diffdays = ts.Days;
                        int cantreg = diffdays / frec;

                        reunion.FechaRegistro = DateTime.Now;
                        ctx.Reunion.Add(reunion);
                        ctx.SaveChanges();

                        Reunion reuAux;
                        DateTime anterior = reunion.Fecha;
                        for (int i = 1; i < cantreg; i++)
                        {
                            reuAux = new Reunion();
                            reuAux.Fecha = anterior.AddDays(frec);
                            anterior = reuAux.Fecha;

                            reuAux.IdPreAgenda = reunion.IdPreAgenda;
                            reuAux.Nivel = reunion.Nivel;
                            reuAux.IdUnidadAcademica = reunion.IdUnidadAcademica;
                            reuAux.IdSede = reunion.IdSede;
                            reuAux.IdSubModalidadPeriodoAcademico = reunion.IdSubModalidadPeriodoAcademico;
                            reuAux.Fecha = reunion.Fecha;
                            reuAux.Lugar = reunion.Lugar;
                            reuAux.HoraInicio = reunion.HoraInicio;
                            reuAux.HoraFin = reunion.HoraFin;
                            reuAux.Motivo = reunion.Motivo;
                            reuAux.Frecuencia = reunion.Frecuencia;
                            reuAux.IdDocente = reunion.IdDocente;
                            reuAux.Semana = reunion.Semana;
                            reuAux.EsExtraordinaria = reunion.EsExtraordinaria;
                            reuAux.IdEstadoReunion = reunion.IdEstadoReunion;
                            reuAux.Estado = reunion.Estado;

                            reuAux.FechaRegistro = DateTime.Now;
                            ctx.Reunion.Add(reuAux);
                            ctx.SaveChanges();
                        }
                    }
                    else
                    {
                        reunion.FechaRegistro = DateTime.Now;
                        ctx.Reunion.Add(reunion);
                        ctx.SaveChanges();
                    }

                    transaction.Commit();
                    return reunion;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        /// <summary>
        /// Método para crear una Reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Identificador de Reunion</param>
        /// <returns>Confirmación de elminación de reunión</returns>
        public static bool EliminarReunion(AbetEntities ctx, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var reunion = GedServices.GetReunionServices(ctx, idReunion);
                    ctx.Acta.RemoveRange(reunion.Acta);
                    ctx.SaveChanges();

                    foreach(TemaReunion item in reunion.TemaReunion)
                    {
                        ctx.DetalleTemaReunion.RemoveRange(item.DetalleTemaReunion);
                        ctx.SaveChanges();
                    }

                    ctx.TemaReunion.RemoveRange(reunion.TemaReunion);
                    ctx.SaveChanges();
                    ctx.ParticipanteReunion.RemoveRange(reunion.ParticipanteReunion);
                    ctx.SaveChanges();
                    ctx.ReunionComentario.RemoveRange(reunion.ReunionComentario);
                    ctx.SaveChanges();
                    ctx.Reunion.Remove(reunion);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Reunion ActualizarReunion(AbetEntities ctx, Reunion model)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var reunion = GedServices.GetReunionServices(ctx, model.IdReunion);
                    reunion.Motivo = model.Motivo;
                    reunion.IdSede = model.IdSede;
                    reunion.Fecha = model.Fecha;
                    reunion.Lugar = model.Lugar;
                    reunion.HoraInicio = model.HoraInicio;
                    reunion.HoraFin = model.HoraFin;
                    reunion.EsExtraordinaria = model.EsExtraordinaria;

                    ctx.Entry(reunion).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                    transaction.Commit();

                    return reunion;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        /// <summary>
        /// Método que devuelve los temas de una reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// /// <param name="obj">objeto tema</param>
        /// <param name="idReunion">Id de la Reunión a la que se asocia el tema</param>
        /// <returns>Lista de temas de la Reunión</returns>
        public static TemaReunion CrearTemaDeReunion(AbetEntities ctx, string tema, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    TemaReunion objTema = new TemaReunion();
                    objTema.IdReunion = idReunion;
                    objTema.DescripcionTemaReunion = tema;
                    objTema.FechaRegistro = DateTime.Now;

                    ctx.TemaReunion.Add(objTema);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return objTema;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        /// <summary>
        /// Método que devuelve los temas de una reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Id de la Reunión a la que se asocia el tema</param>
        /// <returns>Lista de temas de la Reunión</returns>
        public static List<TemaReunion> GetAllTemasDeReunion(AbetEntities ctx, int idReunion)
        {
            return ctx.TemaReunion.Where(x => x.IdReunion == idReunion).ToList();
        }

        /// <summary>
        /// Método para eliminar temas de una Reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Id de la Reunión a la que se asocia el tema a eliminar</param>
        /// <returns>Confirmación de eliminación de Reunión</returns>
        public static bool EliminarTemasDeReunion(AbetEntities ctx, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {   
                    var temas = GedServices.GetAllTemasReunionServices(ctx, idReunion);
                    ctx.TemaReunion.RemoveRange(temas);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para eliminar participantes de una Reunión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Id de la Reunión</param>
        /// <returns>Confirmación de eliminación de participantes de la reunión</returns>
        public static bool EliminarParticipantesReunion(AbetEntities ctx, int idReunion)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var participantes = GedServices.GetAllParticipantesReunionServices(ctx, idReunion);
                    ctx.ParticipanteReunion.RemoveRange(participantes);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static List<ParticipanteReunion> GetAllParticipantesReunion(AbetEntities ctx, int idReunion)
        {
            return ctx.ParticipanteReunion.Where(x => x.IdReunion == idReunion).ToList();
        }


        public static List<Tuple<string, string>> GetFrecuenciaReunion(string culture)
        {
            var result = new List<Tuple<string, string>>();

            if (culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.UNICA.CODIGO, ConstantHelpers.FRECUENCIA_REUNION.UNICA.TEXTO ));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.SEMANAL.CODIGO , ConstantHelpers.FRECUENCIA_REUNION.SEMANAL.TEXTO ));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.QUINCENAL.CODIGO , ConstantHelpers.FRECUENCIA_REUNION.QUINCENAL.TEXTO));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.TEXTO));
            }
            else
            {
                result.Add(
                   new Tuple<string, string>
                   (ConstantHelpers.FRECUENCIA_REUNION.UNICA.CODIGO, ConstantHelpers.FRECUENCIA_REUNION.UNICA.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.SEMANAL.CODIGO, ConstantHelpers.FRECUENCIA_REUNION.SEMANAL.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.QUINCENAL.CODIGO, ConstantHelpers.FRECUENCIA_REUNION.QUINCENAL.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.FRECUENCIA_REUNION.MENSUAL.CODIGO, ConstantHelpers.FRECUENCIA_REUNION.MENSUAL.TEXTOINGLES));
            }
            return result;
        }



    }
}
