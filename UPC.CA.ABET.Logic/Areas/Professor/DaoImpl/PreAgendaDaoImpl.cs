﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;



namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    class PreAgendaDaoImpl
    {
        /// <summary>
        /// Método para obtener todas las PreAgendas
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista de todas las PreAgendas</returns>
        public static List<PreAgenda> GetAllPreAgendas(AbetEntities ctx)
        {
            return (from s in ctx.PreAgenda select s).ToList();
        }

        /// <summary>
        /// Método para obtener las PreAgendas de acuerdo a nivel jerárquico
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="nivel">Nivel jerárquico</param>
        /// <returns>Lista de PreAgendas pertenecientes al nivel jerárquico ingresado</returns>
        public static List<PreAgenda> GetPreAgendasByNivel(AbetEntities ctx, int nivel)
        {
            return (from s in ctx.PreAgenda where s.Nivel == nivel select s).ToList();
        }

        /// <summary>
        /// Método para obtener las PreAgendas de acuerdo a nivel jerárquico y tipo RCO o RCC
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="tipo">Tipo de PreAgenda: "RCO" = Reunión de Comite consultivo  "RCC" = Reunión de Comite Consultivo</param>
        /// <param name="nivel">Nivel jerárquico</param>
        /// <returns>Lista de PreAgendas pertenecientes al nivel jerárquico y tipo ingresado</returns>
        public static List<PreAgenda> GetPreAgendasByNivelAndTipo(AbetEntities ctx, string tipo, int nivel)
        {
            return (from s in ctx.PreAgenda where s.Nivel == nivel && s.TipoPreagenda == tipo select s).ToList();
        }

        /// <summary>
        /// Método para obtener la PreAgenda de acuerdo a su identificador
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="id">Identificador de la PreAgenda</param>
        /// <returns>Objeto PreAgenda de acuerdo al identificador ingresado</returns>
        public static PreAgenda GetPreAgenda(AbetEntities ctx, int id)
        {
            return (from s in ctx.PreAgenda where s.IdPreAgenda == id select s).SingleOrDefault();
        }

        /// <summary>
        /// Método para crear una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="session">Sesión del usuario que solicita el recurso</param>
        /// <param name="preAgenda">PreAgenda a registrar</param>
        /// <param name="tipoPreAgenda">Tipo de PreAgenda: "RCO" o "RCC"</param>
        /// <returns>Entidad PreAgenda que acaba de ser creada</returns>
        public static PreAgenda CrearPreAgenda(AbetEntities ctx, HttpSessionStateBase session, PreAgenda preAgenda, string tipoPreAgenda)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    preAgenda.FechaCreacion = DateTime.Now;
                   // if (preAgenda.Nivel  0)
                        preAgenda.IdSubModalidadPeriodoAcademico = GedServices.GetCurrentPeriodoAcademicoCicloByDateServices(ctx, preAgenda.FechaReunion.Value).IdPeriodoAcademico;
                    if(SessionHelper.GetUsuarioId(session) != null)
                        preAgenda.IdUsuarioCreador = session.GetDocenteId();
                    preAgenda.TipoPreagenda = tipoPreAgenda;
                    var pa = GedServices.GetPeriodoAcademicoServices(ctx, preAgenda.IdSubModalidadPeriodoAcademico.Value);
                    if (pa != null)
                        preAgenda.Semana = ConvertHelpers.GetNumeroSemanaOfReunion(pa.FechaInicioPeriodo.Value, preAgenda.FechaReunion.Value);
                    else
                        preAgenda.Semana = null;
                    if (preAgenda.Motivo == null)
                        preAgenda.Motivo = "";

                    ctx.PreAgenda.Add(preAgenda);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return preAgenda;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }

        /// <summary>
        /// Método para actualizar una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="session">Sesión del usuario que solicita el recurso</param>
        /// <param name="preAgenda">PreAgenda a actualizar</param>
        /// <param name="tipoPreAgenda">Tipo de PreAgenda: "RCO" o "RCC"</param>
        /// <returns>Confirmación de PreAgenda que acaba de ser actualizada</returns>
        public static bool ActualizarPreAgenda(AbetEntities ctx, HttpSessionStateBase session, PreAgenda preAgenda, string tipoPreAgenda)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    preAgenda.FechaCreacion = DateTime.Now;
                    preAgenda.IdSubModalidadPeriodoAcademico = GedServices.GetCurrentPeriodoAcademicoCicloByDateServices(ctx, preAgenda.FechaReunion.Value).IdPeriodoAcademico;
                    if (SessionHelper.GetUsuarioId(session) != null)
                        preAgenda.IdUsuarioCreador = SessionHelper.GetDocenteId(session);
                    preAgenda.TipoPreagenda = tipoPreAgenda;
                    var pa = GedServices.GetPeriodoAcademicoServices(ctx, preAgenda.IdSubModalidadPeriodoAcademico.Value);
                    if(tipoPreAgenda.Equals( ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COORDINACION.CODIGO )) {
                        preAgenda.Semana = ConvertHelpers.GetNumeroSemanaOfReunion(pa.FechaInicioPeriodo.Value, preAgenda.FechaReunion.Value);
                    }
                    else if( tipoPreAgenda.Equals( ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO))
                    {
                        preAgenda.Semana = null;
                    }

                    ctx.Entry(preAgenda).State = System.Data.Entity.EntityState.Modified;

                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para actualizar ciertos valores de una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="session">Sesión del usuario que solicita el recurso</param>
        /// <param name="idPreAgenda">Identificador de la PreAgenda</param>
        /// <param name="motivo">Motivo de la PreAgenda</param>
        /// <param name="idSede">Identidicaor de la sede de la PreAgenda</param>
        /// <returns>Confirmación de PreAgenda actualizada</returns>
        public static bool ActualizarPreAgenda(AbetEntities ctx, HttpSessionStateBase session, int idPreAgenda, string motivo, int idSede)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var preAgenda = GedServices.GetPreAgendaServices(ctx, idPreAgenda);
                    preAgenda.FechaCreacion = DateTime.Now;
                    if (SessionHelper.GetUsuarioId(session) != null)
                        preAgenda.IdUsuarioCreador = SessionHelper.GetUsuarioId(session);
                    preAgenda.Motivo = motivo;
                    preAgenda.IdSede = idSede;

                    ctx.Entry(preAgenda).State = System.Data.Entity.EntityState.Modified;

                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }
        
        /// <summary>
        /// Método para eliminar una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idPreAgenda">Identificador de la PreAgenda</param>
        /// <returns>Confirmación de PreAgenda eliminada</returns>
        public static bool EliminarPreAgenda(AbetEntities ctx, int idPreAgenda)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var preAgenda = GedServices.GetPreAgendaServices(ctx, idPreAgenda);
                    ctx.Reunion.RemoveRange(preAgenda.Reunion);
                    ctx.SaveChanges();
                    //ctx.TemaReunion.RemoveRange(preAgenda.TemaPreAgenda);
                    ctx.SaveChanges();
                    ctx.PreAgenda.Remove(preAgenda);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para eliminar las PreAgendas temporales de las reuniones que no han sido creadas
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Confirmación de PreAgendas eliminadas</returns>
        public static bool EliminarPreAgendasExtraordinariasNoUtilizadas(AbetEntities ctx)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    DateTime fechaParam = DateTime.Now.AddDays(-2);
                    var preAgendas = (from obj in ctx.PreAgenda
                                      where obj.FechaCreacion <= fechaParam && obj.Motivo.Equals("") && obj.Nivel == 0
                                      select obj).ToList();
                    if (preAgendas.Count > 0)
                    {
                        foreach (var item in preAgendas)
                        {
                            //ctx.TemaReunion.RemoveRange(item.TemaPreAgenda);
                            ctx.SaveChanges();
                            foreach (var reunion in item.Reunion)
                            {
                                ctx.ParticipanteReunion.RemoveRange(reunion.ParticipanteReunion);
                                ctx.SaveChanges();
                            }
                            ctx.Reunion.RemoveRange(item.Reunion);
                            ctx.SaveChanges();
                        }
                        ctx.PreAgenda.RemoveRange(preAgendas);
                        ctx.SaveChanges();
                        transaction.Commit();
                    }

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para obtener un Tema de PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idTemaPreAgenda">Identificador de TemaPreAgenda</param>
        /// <returns>TemaPreAgenda de acuerdo al id de TemaPreAgenda ingresado</returns>
        public static TemaPreAgenda GetTemaPreAgenda(AbetEntities ctx, int idTemaPreAgenda)
        {
            return (from s in ctx.TemaPreAgenda where s.IdTemaPreAgenda == idTemaPreAgenda select s).First();
        }

        /// <summary>
        /// Método para obtener todos los temas de una PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idPreAgenda">Identificador de PreAgenda</param>
        /// <returns>Lista de Temas de PreAgenda</returns>
        public static List<TemaPreAgenda> GetAllTemasPreAgenda(AbetEntities ctx, int idPreAgenda)
        {
            return (from s in ctx.TemaPreAgenda where s.IdPreAgenda == idPreAgenda select s).ToList();
        }

        /// <summary>
        /// Método para crear un Tema de PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="temaPreAgenda">Objeto TemaPreAgenda a ser insertado</param>
        /// <returns>Identificador del TemaPreAgenda creado</returns>
        
        public static int CrearTemaPreAgenda(AbetEntities ctx, TemaPreAgenda temaPreAgenda)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    temaPreAgenda.Estado = ConstantHelpers.PREAGENDA.TEMA_PREAGENDA.ESTADOS.PENDIENTE.CODIGO;

                    ctx.TemaPreAgenda.Add(temaPreAgenda);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return temaPreAgenda.IdTemaPreAgenda;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return 0;
                }
            }
        }

        /// <summary>
        /// Método para eliminar un tema de PreAgenda
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idTemaPreAgenda"></param>
        /// <returns>Confirmación de eliminación de TemaPreAgenda</returns>
        public static bool EliminarTemaPreAgenda(AbetEntities ctx, int idTemaPreAgenda)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var tema = GedServices.GetTemaPreAgendaServices(ctx, idTemaPreAgenda);
                    ctx.TemaPreAgenda.Remove(tema);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para realizar la búsqueda de PreAgendas : Coordinacion y Acta de Comite consultivo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idNivel">Nivel jerárquico</param>
        /// <param name="idSemana">Semana académica</param>
        /// <param name="fechaInicio">Fecha de inicio del rango de fechas de búsqueda</param>
        /// <param name="fechaFin">Fecha de fin del rango de fechas de búsqueda</param>
        /// <param name="idSede">Identificador de sede</param>
        /// <param name="idArea"></param>
        /// <param name="tipoPregenda">Tipo de PreAgenda a buscar: "RCO" o "RCC"</param>
        /// <returns>Lista de PreAgendas encontradas de acuerdo a los parámetros de búsqueda</returns>
        public static List<PreAgenda> GetPreAgendasBusqueda(AbetEntities ctx, int? idNivel, int? idSemana, DateTime? fechaInicio, DateTime? fechaFin, int? idSede, int? idArea, string tipoPregenda)
        {
            IQueryable<PreAgenda> lstPreagenda = from s in ctx.PreAgenda where !s.Motivo.Equals("") && s.SubModalidadPeriodoAcademico.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && s.Nivel != 0 && s.TipoPreagenda == tipoPregenda select s;

            if (idNivel != null)
                lstPreagenda = lstPreagenda.Where(m => m.Nivel == idNivel);
            if (idSemana != null)
                lstPreagenda = lstPreagenda.Where(m => m.Semana == idSemana);
            if (fechaInicio != null && fechaInicio.ToString() != "1/01/0001 12:00:00 a. m.")
                lstPreagenda = lstPreagenda.Where(m => m.FechaReunion >= fechaInicio);
            if (fechaFin != null && fechaFin.ToString() != "1/01/0001 12:00:00 a. m.")
                lstPreagenda = lstPreagenda.Where(m => m.FechaReunion <= fechaFin);
            if (idSede != null)
                lstPreagenda = lstPreagenda.Where(m => m.IdSede == idSede);
            if (idArea != null)
                lstPreagenda = lstPreagenda.Where(m => m.IdUnidaAcademica == idArea);

            if (lstPreagenda == null)
            {
                return lstPreagenda.ToList();
            }
            if (ConstantHelpers.PREAGENDA.TIPO_PREAGENDA.REUNION_COMITE_CONSULTIVO.CODIGO.Equals(tipoPregenda))
                return lstPreagenda.OrderBy(m => m.Nivel).ThenBy(m => m.Semana).ThenBy(m => m.FechaReunion).ToList();
            else
                return lstPreagenda.OrderBy(m => m.FechaReunion).ToList();
        }


        //NO SE USA
        //public static List<PreAgenda> GetPreAgendasBusquedaByUsuario(AbetEntities ctx, HttpSessionStateBase Session, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, string tipoPregenda, int? idArea)
        //{
        //    int auxNivelUsuario = GedServices.GetNivelMaximoUsuarioLogueadoServices(ctx, Session);
        //    //IEnumerable<PreAgenda> lstPreagenda = from s in ctx.PreAgenda where !s.Motivo.Equals("") && s.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && s.Nivel != 0 && s.Nivel >= auxNivelUsuario && s.TipoPreagenda == tipoPregenda select s;
        //    IEnumerable<PreAgenda> lstPreagenda = from s in ctx.PreAgenda where !s.Motivo.Equals("") && s.PeriodoAcademico.Estado == ConstantHelpers.ESTADO.ACTIVO && s.Nivel >= auxNivelUsuario && s.TipoPreagenda == tipoPregenda select s;


        //    if (idNivel != null)
        //        lstPreagenda = lstPreagenda.Where(m => m.Nivel == idNivel);
        //    if (idSemana != null)
        //        lstPreagenda = lstPreagenda.Where(m => m.Semana == idSemana);
        //    if (FechaInicio != null)
        //        lstPreagenda = lstPreagenda.Where(m => m.FechaReunion >= FechaInicio);
        //    if (FechaFin != null)
        //        lstPreagenda = lstPreagenda.Where(m => m.FechaReunion <= FechaFin);
        //    if (idSede != null)
        //        lstPreagenda = lstPreagenda.Where(m => m.IdSede == idSede);
        //    if (idArea != null)
        //    {
        //        lstPreagenda = lstPreagenda.Where(m => m.IdUnidaAcademica == idArea);
        //    }

        //    return lstPreagenda.OrderBy(m => m.Nivel).ThenBy(m => m.Semana).ThenBy(m => m.FechaReunion).ToList();
        //}

        /// <summary>
        /// Método obtener la lista de semanas disponibles
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista de semanas</returns>
        public static List<int> GetSemanasDisponibles(AbetEntities ctx)
        {
            return new List<int> { 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        }

        public static List<string> GetFrecuenciaReunion(AbetEntities ctx)
        {
            return new List<String> { "Única", "Semanal", "Quincenal", "Mensual"};
        }
    }
}
