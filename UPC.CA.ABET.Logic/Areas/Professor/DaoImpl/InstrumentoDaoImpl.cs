﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class InstrumentoDaoImpl
    {
        public static Instrumento GetIfcInstrumento(AbetEntities ctx)
        {
            return (from i in ctx.Instrumento where i.Acronimo == "IFC" select i).FirstOrDefault();
        }

    }
}
