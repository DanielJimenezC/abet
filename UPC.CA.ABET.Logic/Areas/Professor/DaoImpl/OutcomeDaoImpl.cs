﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class OutcomeDaoImpl
    {
        /// <summary>
        /// Método para obtener outcomes que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <param name="idSubModalidadPeriodoAcademico"></param>
        /// <returns>Lista con todos los outcomes que cumplan con los criterios ingresados</returns>
        public static List<Outcome> GetOutcomesInComisionPeriodoAcademico(AbetEntities ctx, int idComision, int idSubModalidadPeriodoAcademico)
        {
            return ctx.OutcomeComision.Where(X => X.IdComision == idComision && X.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).Select(x => x.Outcome).ToList();
            return (from o in ctx.Outcome
                    join oc in ctx.OutcomeComision on o.IdOutcome equals oc.IdOutcome
                    where oc.IdComision == idComision && oc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                    select o).ToList();
        }

    }
}
