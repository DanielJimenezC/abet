﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using System.Data.Entity;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class MallaCocosDaoImpl
    {
        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código único de la comisión</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromComision(AbetEntities ctx, string codigoComision)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 join com in ctx.Comision on cc.IdComision equals com.IdComision
                 where com.Codigo == codigoComision
                 select m).ToList();
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarrera">Cödigo identificador de la carrera</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromCarrera(AbetEntities ctx, int idCarrera)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 where cc.IdCarrera == idCarrera
                 select m).ToList();
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código único de la comisión</param>
        /// <param name="idCarrera">Cödigo identificador de la carrera</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromCarreraComision(AbetEntities ctx, string codigoComision, int idCarrera)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 join com in ctx.Comision on cc.IdComision equals com.IdComision
                 where cc.IdCarrera == idCarrera && com.Codigo == codigoComision
                 select m).ToList();
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarrera">Cödigo identificador de la carrera</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromCarreraPeriodoAcademico(AbetEntities ctx, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 where m.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && cc.IdCarrera == idCarrera
                 select m).ToList();
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código único de la comisión</param>
        /// <param name="idCarrera">Cödigo identificador de la carrera</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromCarreraComisionPeriodoAcademico(AbetEntities ctx, string codigoComision, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 join com in ctx.Comision on cc.IdComision equals com.IdComision
                 where
                     m.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && cc.IdCarrera == idCarrera &&
                     com.Codigo.StartsWith(codigoComision)
                     //com.Codigo ==codigoComision
                 select m).ToList();
        }


        /// <summary>
        /// Método para obtener todas las mallas de cocos existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todas las mallas de cocos existentes</returns>
        public static List<MallaCocos> GetAllMallaCocos(AbetEntities ctx)
        {
            return (from m in ctx.MallaCocos select m).ToList();
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código único de la comisión</param>
        /// <param name="idCarrera">Cödigo identificador de la carrera</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocos(CargarDatosContext dataContext,int idEscuela, string codigoComision, Int32? idCarrera, Int32? idSubModalidadPeriodoAcademico, Int32? IdAcreditadora)
        {
            //RECIBE SUBMODA OK 

            var query = dataContext.context.MallaCocos.AsQueryable();

            if (!String.IsNullOrEmpty(codigoComision))
                query = query.Where(x => x.CarreraComision.Comision.Codigo == codigoComision);
            //if (dataContext.session == null)
            //    idEscuela = 1;
            //else
            //{
            //var idEscuela = dataContext.session.GetEscuelaId();
            //}
            //  var idEscuela = dataContext.session.GetEscuelaId();

            var idCarreras = dataContext.context.Carrera.Where(c => c.IdEscuela == idEscuela && (idCarrera.HasValue ? c.IdCarrera == idCarrera.Value : true)).Select(c => c.IdCarrera);
            query = query.Where(x => idCarreras.Contains(x.CarreraComision.Carrera.IdCarrera));
            if (idSubModalidadPeriodoAcademico.HasValue)
                query = query.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico);
            if (IdAcreditadora.HasValue)
                query = query.Where(x => x.CarreraComision.Comision.AcreditadoraPeriodoAcademico.Acreditadora.IdAcreditadora == IdAcreditadora);
            return query.ToList();
        }

        /// <summary>
        /// Método para obtener una malla de cocos de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <returns>Malla de cocos que tenga el código identificador ingresado</returns>
        public static MallaCocos GetMallaCocosById(AbetEntities ctx, int idMallaCocos)
        {
            return (from m in ctx.MallaCocos
                    where m.IdMallaCocos == idMallaCocos
                    select m).FirstOrDefault();
        }


        /// <summary>
        /// Método para obtener una malla de cocos de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <returns>Malla de cocos que tenga el código identificador ingresado</returns>
        public static MallaCocos GetMallaCocos(AbetEntities ctx, int idMallaCocos)
        {
            return (from m in ctx.MallaCocos
                    where m.IdMallaCocos == idMallaCocos
                    select m).FirstOrDefault();
        }


        /// <summary>
        /// Método para eliminar una malla de cocos
        /// </summary>
        /// <param name="context">Contexto de base de datos</param>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <returns>Valor booleano que especifica si se pudo realizar la operación con éxito (true) o no (false)</returns>
        public static bool DeleteMallaCocos(AbetEntities context, int idMallaCocos)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var mallaCocosDtalle = context.MallaCocosDetalle.Where(x => x.IdMallaCocos == idMallaCocos);
                    context.MallaCocosDetalle.RemoveRange(mallaCocosDtalle);

                    var malla = context.MallaCocos.Where(m => m.IdMallaCocos == idMallaCocos).FirstOrDefault();

                    context.MallaCocos.Remove(malla);
                    context.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para obtener las mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromPeriodoAcademico(AbetEntities ctx, int idSubModalidadPeriodoAcademico)
        {
            return
                (from m in ctx.MallaCocos
                 where m.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                 select m).ToList();
        }

        /// <summary>
        /// Método para obtener mallas de cocos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código único de la comisión</param>
        /// <param name="idSubModalidadPeriodoAcademico">Cödigo identificador del periodo académico</param>
        /// <returns>Lista con todas las mallas de cocos que cumplan los criterios ingresados</returns>
        public static List<MallaCocos> GetMallaCocosFromComisionPeriodoAcademico(AbetEntities ctx, string codigoComision, int idSubModalidadPeriodoAcademico)
        {
            return
                (from m in ctx.MallaCocos
                 join cc in ctx.CarreraComision on m.IdCarreraComision equals cc.IdCarreraComision
                 join com in ctx.Comision on cc.IdComision equals com.IdComision
                 where m.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && com.Codigo == codigoComision
                 select m).ToList();
        }


        /// <summary>
        /// Método para crear una malla de cocos
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="mallaCurricular">Malla curricular que contiene la información de los detalles para la creación de la malla de cocos</param>
        /// <param name="detalle">Detalle o descripción de la malla de cocos</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador de la malla de cocos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <returns>Valor booleano que especifica si se pudo realizar la operación con éxito (true) o no (false)</returns>
        public static int CrearMallaCocos(AbetEntities ctx, MallaCurricular mallaCurricular, string detalle, int idperiodoacademico, int idComision, int idCarrera)
        {
            int idsubmoda = ctx.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idperiodoacademico).FirstOrDefault().IdSubModalidadPeriodoAcademico;
            
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var carreraComision = CarreraComisionDaoImpl.GetCarreraComision(ctx, idComision, idCarrera, idsubmoda);

                    var mallaCocos = new MallaCocos
                    {
                        Detalle = detalle ?? "",
                        IdCarreraComision = carreraComision.IdCarreraComision,
                        IdSubModalidadPeriodoAcademico = idsubmoda
                    };

                    ctx.MallaCocos.Add(mallaCocos);
                    ctx.SaveChanges();

                    foreach (var cursoMallaCurricular in mallaCurricular.CursoMallaCurricular)
                    {
                        var cmc = ctx.CursoMallaCurricular.FirstOrDefault(x => x.IdCursoMallaCurricular == cursoMallaCurricular.IdCursoMallaCurricular);
                        if (cmc != null) cmc.EsFormacion = cursoMallaCurricular.EsFormacion;
                        foreach (var mallaCocosDetalle in cursoMallaCurricular.MallaCocosDetalle)
                        {
                            mallaCocosDetalle.IdMallaCocos = mallaCocos.IdMallaCocos;

                            ctx.MallaCocosDetalle.Add(mallaCocosDetalle);
                        }
                    }

                    ctx.SaveChanges();
                    transaction.Commit();

                    return mallaCocos.IdMallaCocos;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return -1;
                }
            }
        }


        /// <summary>
        /// Método para actualizar una malla de cocos
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="mallaCurricular">Malla curricular que contiene los detalles para la actualización de la malla de cocos</param>
        /// <param name="detalle">Detalle o descripción de la malla de cocos</param>
        /// <param name="idMallaCocos">Código identificador de la malla de cocos</param>
        /// <returns>Valor booleano que especifica si se pudo realizar la operación con éxito (true) o no (false)</returns>
        public static bool ActualizarMallaCocos(AbetEntities ctx, MallaCurricular mallaCurricular, string detalle, int idMallaCocos)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var mallaCocos = GetMallaCocos(ctx, idMallaCocos);

                    if (mallaCocos == null)
                    {
                        return false;
                    }

                    mallaCocos.Detalle = detalle ?? "";

                    foreach (var cursoMallaCurricular in mallaCurricular.CursoMallaCurricular)
                    {
                        var cmc = ctx.CursoMallaCurricular.FirstOrDefault(x => x.IdCursoMallaCurricular == cursoMallaCurricular.IdCursoMallaCurricular);
                        if (cmc != null) cmc.EsFormacion = cursoMallaCurricular.EsFormacion;
                        foreach (var mallaCocosDetalle in cursoMallaCurricular.MallaCocosDetalle)
                        {
                            var mallaCocosDetalleActualizado = (from mcd in ctx.MallaCocosDetalle
                                                                where mcd.IdCursoMallaCurricular == cursoMallaCurricular.IdCursoMallaCurricular &&
                                                                mcd.IdMallaCocos == mallaCocos.IdMallaCocos &&
                                                                mcd.IdOutcomeComision == mallaCocosDetalle.IdOutcomeComision
                                                                select mcd).FirstOrDefault();
                            if (mallaCocosDetalleActualizado == null)
                            {
                                var nuevoMallaCocosDetalle = new MallaCocosDetalle
                                {
                                    IdCursoMallaCurricular = cursoMallaCurricular.IdCursoMallaCurricular,
                                    IdMallaCocos = idMallaCocos,
                                    IdOutcomeComision = mallaCocosDetalle.IdOutcomeComision,
                                    IdTipoOutcomeCurso = mallaCocosDetalle.IdTipoOutcomeCurso
                                };
                                ctx.MallaCocosDetalle.Add(nuevoMallaCocosDetalle);
                            }
                            else
                            {
                                if (mallaCocosDetalle.IdTipoOutcomeCurso != 0)
                                {
                                    mallaCocosDetalleActualizado.IdTipoOutcomeCurso = mallaCocosDetalle.IdTipoOutcomeCurso;
                                }
                                else
                                {
                                    //ACA BORRA SI SQ se ha eliminado
                                    CursoMallaCurricular cursoMallaCurricularRubrica = ctx.RubricaCalificada
                                        .Select(x => x.Rubrica.Evaluacion.CarreraCursoPeriodoAcademico.CursoPeriodoAcademico.Curso)
                                        .SelectMany(o=>o.CursoMallaCurricular)
                                        .FirstOrDefault(y=>y.IdCursoMallaCurricular == mallaCocosDetalle.IdCursoMallaCurricular);

                                    if (cursoMallaCurricularRubrica != null)
                                    {
                                        List<Rubrica> listRubricas = ctx.OutcomeRubrica
                                            .Where(x => x.OutcomeComision.IdOutcomeComision == mallaCocosDetalle.IdOutcomeComision
                                                    && cursoMallaCurricularRubrica.IdCursoMallaCurricular == mallaCocosDetalle.IdCursoMallaCurricular)
                                            .Select(y=>y.Rubrica).ToList();
                                        if (listRubricas.Count() > 0)
                                        {
                                            return false;
                                        }
                                    }
                                    ctx.MallaCocosDetalle.Remove(mallaCocosDetalleActualizado);
                                }
                            }
                        }
                    }

                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }


    }
}
