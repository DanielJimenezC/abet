﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class MallaCurricularDaoImpl
    {
        /// <summary>
        /// Método para obtener una malla curricular perteneciente a una carrera y un periodo académico
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Malla curricular que cumpla con los criterios</returns>
        public static MallaCurricular GetMallaCurricular(AbetEntities ctx, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            return (from mc in ctx.MallaCurricular
                    join mcpa in ctx.MallaCurricularPeriodoAcademico on mc.IdMallaCurricular equals mcpa.IdMallaCurricular
                    where mc.IdCarrera == idCarrera && mcpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                    select mc).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener una malla curricular de acuerdo a su identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idMallaCurricular">Código identificador de la malla curricular</param>
        /// <returns>Malla curricular que cumpla con los criterios</returns>
        public static MallaCurricular GetMallaCurricular(AbetEntities ctx, int idMallaCurricular)
        {
            return (from mc in ctx.MallaCurricular
                    where mc.IdMallaCurricular == idMallaCurricular
                    select mc).FirstOrDefault();
        }


    }
}
