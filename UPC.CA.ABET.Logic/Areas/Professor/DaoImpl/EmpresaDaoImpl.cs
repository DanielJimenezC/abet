﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class EmpresaDaoImpl
    {
        public static List<Empresa> GetAllEmpresas(AbetEntities ctx)
        {
            return
                (from c in ctx.Empresa
                 select c).ToList();
        }

        public static Empresa CrearEmpresa(AbetEntities ctx, Empresa empresa)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    ctx.Empresa.Add(empresa);
                    ctx.SaveChanges();
                    transaction.Commit();

                    return empresa;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return null;
                }
            }
        }
    }
}
