﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class CarreraComisionDaoImpl
    {

        /// <summary>
        /// Método para obtener la carrera por comisión de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarreraComision">Código identificador de la carrera por comisión</param>
        /// <returns>Carrera por comisión que tenga el código identificador ingresado</returns>
        public static CarreraComision GetCarreraComision(AbetEntities ctx, int idCarreraComision)
        {
            return
                (from cc in ctx.CarreraComision
                 where cc.IdCarreraComision == idCarreraComision
                 select cc).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener la carrera por comisión que cumpla con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Carrera por comisión que cumpla con los criterios ingresados</returns>
        public static CarreraComision GetCarreraComision(AbetEntities ctx, int idComision, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            //idsumodalidad recibe sybmodalidadperiodoacademico

            

            return
                (from cc in ctx.CarreraComision
                 where
                     cc.IdComision == idComision && cc.IdCarrera == idCarrera &&
                     cc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                 select cc).FirstOrDefault();
        }



    }
}
