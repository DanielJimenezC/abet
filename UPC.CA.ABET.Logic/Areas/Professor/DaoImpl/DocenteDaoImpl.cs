﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class DocenteDaoImpl
    {
        /// <summary>
        /// Método para obtener los participantes de una reunion
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idReunion">Código identificador de la reunion</param>
        /// <returns>Lista con todas los docentes pertenecientes a la reunion</returns>
        public static List<Docente> GetParticipantesReunion(AbetEntities ctx, int idReunion)
        {
            return
                (from c in ctx.ParticipanteReunion
                 where c.IdReunion == idReunion && !c.EsExterno
                 select c.Docente).ToList();
        }

        public static List<ParticipanteReunion> GetParticipantesReunionExternos(AbetEntities ctx, int idReunion)
        {
            return
                (from c in ctx.ParticipanteReunion
                 where c.IdReunion == idReunion && c.EsExterno.Equals(true)
                 select c).ToList();
        }

        /// <summary>
        /// Método para obtener un docente
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente</param>
        /// <returns>Docente relacionado al idDocente ingresado</returns>
        public static Docente GetDocente(AbetEntities ctx, int idDocente)
        {
            return
                (from c in ctx.Docente
                 where c.IdDocente == idDocente
                 select c).SingleOrDefault();
        }

        /// <summary>
        /// Método para obtener la lista de docentes disponibles para participar en una réunión de acuerdo a cierto nivel
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param param name="nivel">Jerarquía de la universidad</param>
        /// <returns>Lista con todos los docentes que pueden participar en una reunión de cierto nivel</returns>
        public static List<Docente> GetParticipantesReunionDisponibles(AbetEntities ctx, int nivel, int idPeriodoAcademico, int idUnidadAcademica, int idEscuela)
        {
            if (nivel == 3)
                return ctx.UnidadAcademica.First(X => X.IdUnidadAcademica == idUnidadAcademica && X.IdEscuela == idEscuela).CursoPeriodoAcademico.Seccion.SelectMany(X => X.DocenteSeccion).Select(X => X.Docente).ToList();


            return (from c in ctx.UnidadAcademica
                    orderby c.Nivel descending
                    join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                    join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                    where (c.IdUnidadAcademica == idUnidadAcademica || c.UnidadAcademica2.IdUnidadAcademica == idUnidadAcademica) && c.IdSubModalidadPeriodoAcademico == idPeriodoAcademico && c.IdEscuela == idEscuela
                    select ccc.Docente).Distinct().ToList();
        }

        public static List<Docente> GetParticipantesReunionExtDisponibles(AbetEntities ctx, int idPeriodoAcademico)
        {
            return (from c in ctx.UnidadAcademica
                    join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                    join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                    where c.IdSubModalidadPeriodoAcademico == idPeriodoAcademico select ccc.Docente).Distinct().ToList();
        }

        public static List<Docente> GetParticipantesReunionCCDisponibles(AbetEntities ctx, int nivel, int idPeriodoAcademico, int idUnidadAcademica, int idEscuela)
        {
            return (from c in ctx.UnidadAcademica
                    orderby c.Nivel descending
                    join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                    join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                    where c.IdSubModalidadPeriodoAcademico == idPeriodoAcademico && c.Nivel <= 1 && ccc.Docente.Codigo != "PCSIVPAR" && c.IdEscuela == idEscuela
                    select ccc.Docente).Distinct().ToList();
        }

        public static List<Int32> GetNivelesUsuario(AbetEntities ctx, Int32 IdDocente, Int32 idSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from c in ctx.UnidadAcademica
                    join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                    join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                    where c.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && ccc.IdDocente == IdDocente && c.IdEscuela == idEscuela
                    select c.Nivel).Distinct().ToList();
        }

        public static List<int> GetNivelesUsuarioLogueado(AbetEntities ctx, HttpSessionStateBase Session)
        {
            Int32 idSubModalidadPeriodoAcademico = Helpers.SessionHelper.GetPeriodoAcademicoId(Session).Value;
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            if (Helpers.SessionHelper.GetDocenteId(Session) != null)
            {
                int aux = Helpers.SessionHelper.GetDocenteId(Session).Value;
                List<int> res = (from c in ctx.UnidadAcademica
                                 join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                                 join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                                 where ccc.Docente.IdDocente == aux && c.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                                 select c.Nivel).Distinct().ToList();
                res.Sort();
                    
                return res;
            }
            else
            {
                List<int> res = (from c in ctx.UnidadAcademica
                                 where c.Nivel != 0 && c.IdEscuela == idEscuela
                                 select c.Nivel).Distinct().ToList();
                res.Sort();
                return res;
            }
        }

        public static int GetNivelMaximoUsuarioLogueado(AbetEntities ctx, int idDocente, HttpSessionStateBase Session)
        {
            Int32 idSubModalidadPeriodoAcademico = Helpers.SessionHelper.GetPeriodoAcademicoId(Session).Value;
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            if (idDocente != 0)
            {
                List<int> res = (from c in ctx.UnidadAcademica
                                 join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                                 join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                                 where ccc.Docente.IdDocente == idDocente && c.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                                 select c.Nivel).Distinct().ToList();
                res.Sort();
                return res.First();
            }
            else
                return 1;
        }

        public static int GetNivelMaximoDocente(AbetEntities ctx, int idDocente, int idEscuela)
        {
            List<int> res = (from c in ctx.UnidadAcademica
                             join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                             join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                             where ccc.Docente.IdDocente == idDocente && c.IdEscuela == idEscuela
                             select c.Nivel).Distinct().ToList();
            res.Sort();
            return res.First();
        }
    }
}
