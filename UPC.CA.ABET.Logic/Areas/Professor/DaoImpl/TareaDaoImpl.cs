﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class TareaDaoImpl
    {
        public static List<Tuple<string, string>> GetAllEstadosTarea(AbetEntities ctx)
        {
            return new List<Tuple<string, string>>
            {
                new Tuple<string, string>
                (
                    ConstantHelpers.PROFESSOR.TAREA.ESTADOS.PENDIENTE.CODIGO,
                    ConstantHelpers.PROFESSOR.TAREA.ESTADOS.PENDIENTE.TEXTO
                ),
                new Tuple<string, string>
                (
                    ConstantHelpers.PROFESSOR.TAREA.ESTADOS.REALIZADO.CODIGO,
                    ConstantHelpers.PROFESSOR.TAREA.ESTADOS.REALIZADO.TEXTO
                )
            };
        }
    }
}
