﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class TipoOutcomeCursoDaoImpl
    {
        /// <summary>
        /// Método para obtener todos los tipos de outcome existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todos los tipos de outcome existentes</returns>
        public static List<TipoOutcomeCurso> GetAllTipoOutcomeCursos(AbetEntities ctx)
        {
            return (from toc in ctx.TipoOutcomeCurso
                    select toc).ToList();
        }
    }
}
