﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class CursoDaoImpl
    {

        /// <summary>
        /// Método para obtener un curso de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCurso">Código identificador del curso</param>
        /// <returns>Curso que tiene el código identificador ingresado</returns>
        public static Curso GetCurso(AbetEntities ctx, int idCurso)
        {
            return (from c in ctx.Curso where c.IdCurso == idCurso select c).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener los cursos pertenecientes a una carrera para el docente para realizar un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idArea">Código identificador de la unidad académica que representa el área a considerar</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con los cursos que cumplen los criterios ingresados</returns>
        public static List<Curso> GetCursosForArea(AbetEntities ctx, int idDocente, int idArea, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
            var cursos = (from d in ctx.Docente
                          join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                          join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                          join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                          join cpa in ctx.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                          where d.IdDocente == idDocente
                                && ua.Nivel == 3
                                && cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                                && ua.IdEscuela == idEscuela
                          select ua).Distinct().ToList();

            var result = new List<Curso>();

            foreach (var curso in cursos)
            {
                var areaUnidadAcademica = UnidadAcademicaDaoImpl.GetAreaForCurso(ctx, curso.IdUnidadAcademica);
                if (areaUnidadAcademica.IdUnidadAcademica == idArea)
                {
                    result.Add(curso.CursoPeriodoAcademico.Curso);
                }
            }

            return result;
        }
    }
}
