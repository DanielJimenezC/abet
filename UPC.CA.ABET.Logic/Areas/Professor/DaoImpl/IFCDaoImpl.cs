﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using System.Data.Entity.Validation;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class IFCDaoImpl
    {

        public static IFC GetIfcByUnidadAcademica(AbetEntities ctx, int idUnidadAcademica)
        {
            return (from i in ctx.IFC where i.IdUnidadAcademica == idUnidadAcademica select i).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener los informes de fin de ciclo que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idAreaUnidadAcademica">Código identificador del área</param>
        /// <param name="idSubareaUnidadAcademica">Código identificador de la subárea</param>
        /// <param name="idSubModalidadPeriodoAcademico">Código identificador del periodo académico</param>
        /// <param name="idCursoUnidadAcademica">Código identificador del curso</param>
        /// <param name="estado">Estado del informe de fin de ciclo</param>
        /// <returns>Lista con los informes de fin de ciclo que cumplan con los criterios</returns>
        public static List<IFC> GetIfcs(AbetEntities ctx, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, int? idSubModalidadPeriodoAcademico, string idCursoUnidadAcademica, string estado, int idEscuela)
        {//RECIBE IDSUBMODALIDADPERIODOACADEMICO 0 ahora el correcto

            var ifcs = (from i in ctx.IFC
                        join ua in ctx.UnidadAcademica on i.IdUnidadAcademica equals ua.IdUnidadAcademica
                        join sua in ctx.SedeUnidadAcademica on i.IdUnidadAcademica equals sua.IdUnidadAcademica
                        where
                            i.Estado == (!string.IsNullOrEmpty(estado) ? estado : i.Estado)
                            && ua.IdEscuela == idEscuela
                        select i);

            if(idSubModalidadPeriodoAcademico != 0)
            {
                ifcs = (from i in ifcs
                        where
                     i.UnidadAcademica.IdSubModalidadPeriodoAcademico == (idSubModalidadPeriodoAcademico ?? i.UnidadAcademica.IdSubModalidadPeriodoAcademico)
                     select i);
            }
           

            if(!string.IsNullOrEmpty(idCursoUnidadAcademica))
                ifcs = (from i in ifcs where i.UnidadAcademica.NombreEspanol == idCursoUnidadAcademica || i.UnidadAcademica.NombreIngles == idCursoUnidadAcademica select i);
            if (!string.IsNullOrEmpty(idSubareaUnidadAcademica))
                ifcs = (from i in ifcs where i.UnidadAcademica.UnidadAcademica2.NombreEspanol == idSubareaUnidadAcademica || i.UnidadAcademica.UnidadAcademica2.NombreIngles == idSubareaUnidadAcademica select i);
            if (!string.IsNullOrEmpty(idAreaUnidadAcademica))
                ifcs = (from i in ifcs where i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol == idAreaUnidadAcademica || i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles == idAreaUnidadAcademica select i);

            return ifcs.ToList();///
        }

        /// <summary>
        /// Método para obtener el código alfanumérico de un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="ifc">Informe de fin de ciclo</param>
        /// <returns>Código alfanumérico para el informe de fin de ciclo ingresado</returns>
        public static string GetCodigoForIfc(AbetEntities ctx, IFC ifc)
        {
            var sede = (from sua in ifc.UnidadAcademica.SedeUnidadAcademica
                        join s in ctx.Sede on sua.IdSede equals s.IdSede
                        select s).FirstOrDefault();
            var comision = (from cc in ifc.UnidadAcademica.CarreraPeriodoAcademico.Carrera.CarreraComision
                            join c in ctx.Comision on cc.IdComision equals c.IdComision
                            select c).FirstOrDefault();

            string codigo = "IFC";
            codigo += "-" + comision.Codigo ?? "UNK";
            //codigo += "-" + ifc.UnidadAcademica.CursoMallaCurricular.Curso.Codigo ?? "UNK";
            codigo += "-" + ifc.UnidadAcademica.CarreraPeriodoAcademico.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico ?? "UNK";
            codigo += "-" + sede.Codigo ?? "UNK";
            codigo += "-" + ifc.IdIFC.ToString().PadRight(3, '0') ?? "UNK";
            return codigo;
        }

        /// <summary>
        /// Método para registrar un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="ifc">Informe de fin de ciclo a registrar</param>
        /// <returns>Valor booleano que indica si la operación tuvo éxito (true) o no (false)</returns>
        public static async Task<bool> CrearIfc(AbetEntities ctx, IFC ifc)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    ifc.FechaCreacion = DateTime.Now;
                    ctx.IFC.Add(ifc);

                    await ctx.SaveChangesAsync();

                    foreach (var ifcHallazgo in ifc.IFCHallazgo)
                    {
                        //  var hallazgo = ifcHallazgo.Hallazgo;                                                                                    // RML001
                        var hallazgo = ifcHallazgo.Hallazgos;
                        hallazgo.Codigo = HallazgoDaoImpl.MakeCodigoForHallazgoIfc(hallazgo.Codigo);

                        foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                        {
                            var accionMejora = hallazgoAccionMejora.AccionMejora;
                            accionMejora.Codigo = AccionMejoraDaoImpl.MakeCodigoForAccionMejoraIfc(accionMejora.Codigo);
                        }
                    }

                    await ctx.SaveChangesAsync();

                    transaction.Commit();

                    return true;
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static bool CrearIfcCargaMasiva(AbetEntities ctx, IFC ifc)
        {
            return true;
            /*
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    ifc.FechaCreacion = DateTime.Now;

                    List<IFCHallazgo> tempIfcHallazgos = ifc.IFCHallazgo.ToList();
                    List<Hallazgos> hallazgos = new List<Hallazgos>();

                    
                   
                    foreach (var item in ifc.IFCHallazgo)
                    {
                        hallazgos.Add(item.Hallazgos);
                    }

                    ctx.Hallazgos.AddRange(hallazgos);


                    ifc.IFCHallazgo = null;


                    ctx.IFC.Add(ifc);

                    ctx.SaveChanges();

                    
                    for (int i = 0; i < tempIfcHallazgos.Count; i++)
                    {
                        tempIfcHallazgos.ElementAt(i).idIFC = ifc.IdIFC;
                        tempIfcHallazgos.ElementAt(i).IdHallazgo = hallazgos.ElementAt(i).IdHallazgo;
                        tempIfcHallazgos.ElementAt(i).IFC = ifc;
                        tempIfcHallazgos.ElementAt(i).Hallazgos = hallazgos.ElementAt(i);
                    }

                    ifc.IFCHallazgo = tempIfcHallazgos;

                    ctx.SaveChanges();

                    /*
                    ctx.IFC.
                        Add(ifc);

                        


                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    return false;
                }
            }
            */
        }


        public static IFC GetIfc(AbetEntities ctx, int idIfc)
        {
            return (from i in ctx.IFC where i.IdIFC == idIfc select i).FirstOrDefault();
        }
        public static ReunionProfesor GetARP(AbetEntities ctx, int idReunion)
        {
            return (from i in ctx.ReunionProfesor where i.IdReunionProfesor == idReunion select i).FirstOrDefault();
        }

        public static bool DeleteIfc(AbetEntities ctx, int idIfc)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var ifc = GetIfc(ctx, idIfc);
                    var ifcHallazgos = ifc.IFCHallazgo.ToList();
                    var hallazgos = (from ih in ifcHallazgos
                                     join h in ctx.Hallazgo on ih.IdHallazgo equals h.IdHallazgo
                                     select h).ToList();
                    var hallazgoAccionesMejora = (from h in hallazgos
                                                  join ham in ctx.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                                                  select ham).ToList();
                    var accionesMejora = (from ham in hallazgoAccionesMejora
                                          join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                          select am).ToList();

                    ctx.IFCHallazgo.RemoveRange(ifcHallazgos);
                    ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
                    ctx.AccionMejora.RemoveRange(accionesMejora);
                    ctx.Hallazgo.RemoveRange(hallazgos);
                    ctx.IFC.Remove(ifc);

                    ctx.SaveChanges();

                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static bool DeleteIfcZ(AbetEntities ctx, int idIfc)                                                                  // RML001 TEMP con la tabla hallazgos
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var ifcoutcomehallazgo = new List<IFCHallazgoOutcome>();
                    var ifc = GetIfc(ctx, idIfc);
                    var ifcHallazgos = ifc.IFCHallazgo.ToList();
                    var hallazgos = (from ih in ifcHallazgos
                                     join h in ctx.Hallazgos on ih.IdHallazgo equals h.IdHallazgo
                                     select h).ToList();
                    
                    foreach(var item in hallazgos)
                    {
                        ifcoutcomehallazgo.AddRange(ctx.IFCHallazgoOutcome.Where(x => x.IdHallazgo == item.IdHallazgo).ToList());
                    }

                    foreach (var item in ifcoutcomehallazgo)
                    {
                        ctx.IFCHallazgoOutcome.Remove(item);
                    }

                    var hallazgoAccionesMejora = (from h in hallazgos
                                                  join ham in ctx.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                                                  select ham).ToList();
                    var accionesMejora = (from ham in hallazgoAccionesMejora
                                          join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                          where am.ParaPlan == false
                                          select am).ToList();
                    var planmejoraaccion = (from am in accionesMejora
                                            join pma in ctx.PlanMejoraAccion on am.IdAccionMejora equals pma.IdAccionMejora
                                            select pma).ToList();

                    ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
                    ctx.IFCHallazgo.RemoveRange(ifcHallazgos);
                    ctx.PlanMejoraAccion.RemoveRange(planmejoraaccion);
                    ctx.AccionMejora.RemoveRange(accionesMejora);
                    ctx.Hallazgos.RemoveRange(hallazgos);
                    ctx.IFC.Remove(ifc);

                    ctx.SaveChanges();

                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: log exception
                    transaction.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// Método para obtener todos los estados existentes para los informes de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todos los estados existentes para los informes de fin de ciclo</returns>
        public static List<Tuple<string, string>> GetAllEstadosIfc(string culture)
        {
            var result = new List<Tuple<string, string>>();

            if (culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.TEXTO));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.TEXTO));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.TEXTO));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.TEXTO));
            }
            else
            {
                result.Add(
                   new Tuple<string, string>
                   (ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.APROBADO.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.ENVIADO.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.ESPERA.TEXTOINGLES));
                result.Add(
                    new Tuple<string, string>
                    (ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ESTADOS.OBSERVADO.TEXTOINGLES));
            }
            return result;
        }

        /// <summary>
        /// Método para obtener los outcomes por comision para la creación del informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idCursoUnidadAcademica">Código identificador de la unidad académica que constituye el curso</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todos los outcomes por comisión para el docente para realizar el informe de fin de ciclo</returns>
        public static List<Tuple<string, List<Outcome>>> GetOutcomesForIfc(AbetEntities ctx, int idDocente, int idCursoUnidadAcademica, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
            var curso = UnidadAcademicaDaoImpl.GetUnidadAcademica(ctx, idCursoUnidadAcademica);
            var idCurso = curso.CursoPeriodoAcademico.IdCurso;
            var outcomeComisiones = (from d in ctx.Docente
                                     join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                                     join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                                     join s in ctx.Sede on sua.IdSede equals s.IdSede
                                     join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                                     join cpa in ctx.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                                     join cmc in ctx.CursoMallaCurricular on cpa.IdCurso equals cmc.IdCurso
                                     join mcd in ctx.MallaCocosDetalle on cmc.IdCursoMallaCurricular equals mcd.IdCursoMallaCurricular
                                     join mc in ctx.MallaCocos on mcd.IdMallaCocos equals mc.IdMallaCocos
                                     join oc in ctx.OutcomeComision on mcd.IdOutcomeComision equals oc.IdOutcomeComision
                                     where d.IdDocente == idDocente
                                           && ua.Nivel == 3
                                           && cpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                                           && cpa.IdCurso == idCurso
                                           && mc.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                                           && ua.IdEscuela == idEscuela
                                     select oc).Distinct().ToList();

            var result = new List<Tuple<string, List<Outcome>>>();

            foreach (var outcomeComision in outcomeComisiones)
            {
                if (outcomeComision.Comision.Codigo == "WASC") continue;
                string nombreComision = outcomeComision.Comision.Codigo == "EAC" ? "Ingeniería" : "Computación";

                List<Outcome> listaOutcomes = null;

                foreach (var tuple in result)
                {
                    if (tuple.Item1 == nombreComision)
                    {
                        listaOutcomes = tuple.Item2;
                        break;
                    }
                }

                if (listaOutcomes == null)
                {
                    listaOutcomes = new List<Outcome>();
                    result.Add(new Tuple<string, List<Outcome>>(nombreComision, listaOutcomes));
                }

                listaOutcomes.Add(outcomeComision.Outcome);
            }

            return result;
        }

        public static string GetTextoForCodigoEstadoIfc(string codigo,string culture)
        {
            var estados = GetAllEstadosIfc(culture);

            foreach (var estado in estados)
            {
                if (estado.Item1 == codigo)
                {
                    return estado.Item2;
                }
            }

            return "No especificado";
        }
    }
}
