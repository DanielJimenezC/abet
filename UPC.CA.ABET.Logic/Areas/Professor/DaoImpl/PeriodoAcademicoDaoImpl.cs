﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class PeriodoAcademicoDaoImpl
    {
        
        /// <summary>
        /// Método para obtener un periodo académico de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Periodo académico que tenga el código identificador ingresado</returns>
        public static PeriodoAcademico GetPeriodoAcademico(AbetEntities ctx, int idSubModalidadPeriodoAcademico)
        {
            //return (from smpa in ctx.SubModalidadPeriodoAcademico
            //        join pa in ctx.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
            //        where smpa.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
            //        select pa).FirstOrDefault();

            //return ctx.PeriodoAcademico.Where(x => x.Estado == ConstantHelpers.ESTADO.ACTIVO).FirstOrDefault();


            //return (from pa in ctx.PeriodoAcademico

            //        where pa.IdPeriodoAcademico == idPeriodoAcademico
            //        select pa).FirstOrDefault();

            return (from submoda in ctx.SubModalidadPeriodoAcademico
                    join pa in ctx.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                    where submoda.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                    select pa).FirstOrDefault();


        }
        
        //AGREGADO STEFANO
        public static List<PeriodoAcademico> GetAllMyPeriodosAcademicos(AbetEntities ctx, int idModalidad)
        {
            /*return (from submoda in ctx.SubModalidadPeriodoAcademico
                    join pa in ctx.PeriodoAcademico on submoda.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                    where submoda.IdSubModalidadPeriodoAcademico == idModalidad
                    select pa).ToList();*/

            return (from modalidad in ctx.Modalidad join submoda in ctx.SubModalidad 
                    on modalidad.IdModalidad equals submoda.IdModalidad
                    join submodper in ctx.SubModalidadPeriodoAcademico 
                    on submoda.IdSubModalidad equals submodper.IdSubModalidad
                    join periodo in ctx.PeriodoAcademico
                    on submodper.IdPeriodoAcademico equals periodo.IdPeriodoAcademico
                    where modalidad.IdModalidad == idModalidad select periodo).ToList();
        }
        //***************
        
        /// <summary>
        /// Método para obtener todos los periodos académicos existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todos los periodos académicos existentes</returns>
        public static List<PeriodoAcademico> GetAllPeriodoAcademicos(AbetEntities ctx)
        {
            return (from p in ctx.PeriodoAcademico select p).ToList();
        }

        /*
        public static List<PeriodoAcademico> GetPeriodosAcademicos(AbetEntities ctx)
        {
            return (from)
        }
        */
        public static List<Modalidad> GetAllModalidades(AbetEntities ctx)
        {
            return (from p in ctx.Modalidad select p).ToList();
        }

        /// <summary>
        /// Método para obtener los periodos académicos que cumplan con los criterios ingresados
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <returns>Lista con los periodos académicos que cumplan con los criterios ingresados</returns>
        public static List<PeriodoAcademico> GetPeriodoAcademicosInComisionCarrera(AbetEntities ctx, string codigoComision, int idCarrera)
        {
            return
                (from carCom in ctx.CarreraComision
                 join per in ctx.PeriodoAcademico on carCom.SubModalidadPeriodoAcademico.IdPeriodoAcademico equals per.IdPeriodoAcademico
                 join com in ctx.Comision on carCom.IdComision equals com.IdComision
                 where com.Codigo.StartsWith(codigoComision)
                 && carCom.IdCarrera == idCarrera
                 select per).Distinct().ToList();
        }


        /// <summary>
        /// Método para obtener el periodo académico actual
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Periodo académico actual</returns>
        public static PeriodoAcademico GetCurrentPeriodoAcademico(AbetEntities ctx)
        {
            DateTime currentDate = DateTime.Now;
            return (from p in ctx.PeriodoAcademico
                    where currentDate >= p.FechaInicioPeriodo && currentDate <= p.FechaFinPeriodo
                    select p).First();
        }

        /// <summary>
        /// Método para obtener el periodo académico anterior al actual
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Periodo académico anterior al actual</returns>
        public static PeriodoAcademico GetPreviosPeriodoAcademico(AbetEntities ctx, int idSubModalidadPeriodoAcademico)
        {
            var periodoAcademicoActual = GetPeriodoAcademico(ctx, idSubModalidadPeriodoAcademico);
            return (from p in ctx.PeriodoAcademico
                    where p.FechaFinPeriodo < periodoAcademicoActual.FechaInicioPeriodo
                    orderby p.FechaFinPeriodo descending
                    select p).FirstOrDefault() ?? (new PeriodoAcademico());
        }

        /// <summary>
        /// Método para obtener el periodo académico al que pertenece una fecha
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Periodo académico correspondiente</returns>
        public static PeriodoAcademico GetCurrentPeriodoAcademicoCicloByDate(AbetEntities ctx, DateTime fecha)
        {
            return ctx.PeriodoAcademico.FirstOrDefault(X => X.Estado.Contains(ConstantHelpers.ESTADO.ACTIVO) && fecha >= X.FechaInicioPeriodo && fecha <= X.FechaFinPeriodo);
            return (from p in ctx.PeriodoAcademico
                    where p.Estado == ConstantHelpers.ESTADO.ACTIVO && fecha >= p.FechaFinPeriodo && fecha <= p.FechaFinPeriodo
                    select p).First();
        }

        /// <summary>
        /// Método para obtener el periodo académico por Ifc
        /// </summary>
        /// <param param name="idDocente">Código identificador del docente</param>
        /// <param name="idReunion">Código identificador de la reunión</param>
        /// <returns>Lista de periodo académico correspondiente al Ifc del docente</returns>
        public static List<PeriodoAcademico> GetPeriodoAcademicosForIfc(AbetEntities ctx, int idDocente, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                            && ua.Nivel == 3
                            && ua.IdEscuela == idEscuela
                    select ua.SubModalidadPeriodoAcademico.PeriodoAcademico).Distinct().ToList();
        }

        public static List<DateTime> GetDateStartWeekCurrentPeriodoAcademicoBySemana(AbetEntities ctx, HttpSessionStateBase Session, int semana)
        {
            try
            {                
                int aux = SessionHelper.GetPeriodoAcademicoId(Session).Value;
                var currentPeriodo = (from obj in ctx.PeriodoAcademico
                                      where obj.IdPeriodoAcademico == aux
                                      select obj).OrderByDescending(m=>m.FechaInicioPeriodo).First();
            
                int numeroSemanaEnElAnio = ConvertHelpers.GetWeekNumber(currentPeriodo.FechaInicioPeriodo.Value);
                int numeroSemanaEnElAnioParametro = numeroSemanaEnElAnio + semana - 1;

                var listaFechas = new List<DateTime>();
                DateTime firstDayOfWeek;
                DateTime lastDayOfWeek;
                if (semana == 0)
                {
                    firstDayOfWeek = ConvertHelpers.FirstDateOfWeek(currentPeriodo.FechaInicioPeriodo.Value.Year, numeroSemanaEnElAnio);
                    int numeroFinalEnElAnio = ConvertHelpers.GetWeekNumber(currentPeriodo.FechaFinPeriodo.Value);
                    lastDayOfWeek = ConvertHelpers.FirstDateOfWeek(currentPeriodo.FechaFinPeriodo.Value.Year,numeroFinalEnElAnio+1);
                }
                else
                {
                    firstDayOfWeek = ConvertHelpers.FirstDateOfWeek(currentPeriodo.FechaInicioPeriodo.Value.Year, numeroSemanaEnElAnioParametro);
                    lastDayOfWeek = firstDayOfWeek.AddDays(7);
                }
                listaFechas.Add(firstDayOfWeek);
                listaFechas.Add(lastDayOfWeek);

                return listaFechas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
