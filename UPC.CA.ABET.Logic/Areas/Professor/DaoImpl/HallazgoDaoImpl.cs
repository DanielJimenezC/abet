﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using System.Data.Entity.Validation;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class HallazgoDaoImpl
    {

        public static Hallazgos GetHallazgo(AbetEntities ctx, string codigo)
        {
            return (from h in ctx.Hallazgos where h.Codigo == codigo select h).FirstOrDefault();
        }

        public static List<Hallazgo> GetHallazgosIfc(AbetEntities ctx, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, string idCursoUnidadAcademica, int? idSubModalidadPeriodoAcademico, string codigoNivelAceptacion, int? idSede)
        {
            var hallazgos = (from h in ctx.Hallazgo
                             where h.IdSede == (idSede ?? h.IdSede)
                                   && h.IdSubModalidadPeriodoAcademico == (idSubModalidadPeriodoAcademico ?? h.IdSubModalidadPeriodoAcademico)
                                   && h.NivelDificultad == (!string.IsNullOrEmpty(codigoNivelAceptacion) ? codigoNivelAceptacion : h.NivelDificultad)
                             select h);

            hallazgos = (from h in hallazgos
                         join ih in ctx.IFCHallazgo on h.IdHallazgo equals ih.IdHallazgo
                         join i in ctx.IFC on ih.idIFC equals i.IdIFC
                         where
                            (i.UnidadAcademica.NombreEspanol == (string.IsNullOrEmpty(idCursoUnidadAcademica) ? i.UnidadAcademica.NombreEspanol : idCursoUnidadAcademica)
                            || i.UnidadAcademica.NombreIngles == (string.IsNullOrEmpty(idCursoUnidadAcademica) ? i.UnidadAcademica.NombreIngles : idCursoUnidadAcademica))
                            &&
                            (i.UnidadAcademica.UnidadAcademica2.NombreEspanol == (string.IsNullOrEmpty(idSubareaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.NombreEspanol : idSubareaUnidadAcademica)
                            || i.UnidadAcademica.UnidadAcademica2.NombreIngles == (string.IsNullOrEmpty(idSubareaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.NombreIngles : idSubareaUnidadAcademica))
                            &&
                            (i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol == (string.IsNullOrEmpty(idAreaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol : idAreaUnidadAcademica)
                            || i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles == (string.IsNullOrEmpty(idAreaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles : idAreaUnidadAcademica))
                         select h);

            return hallazgos.ToList();
        }

        public static List<Hallazgos> GetHallazgosZIfc(AbetEntities ctx, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, string idCursoUnidadAcademica, int? idSubModalidadPeriodoAcademico, string codigoNivelAceptacion)
        {
            Int32? idSMPAM = ctx.SubModalidadPeriodoAcademicoModulo.Where(x => x.SubModalidadPeriodoAcademico.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdSubModalidadPeriodoAcademicoModulo;
            var hallazgos = (from h in ctx.Hallazgos
                             where h.IdSubModalidadPeriodoAcademicoModulo == (idSMPAM ?? h.IdSubModalidadPeriodoAcademicoModulo)
                             select h);

            var hallazgosZ = (from h in hallazgos
                              join ih in ctx.IFCHallazgo on h.IdHallazgo equals ih.IdHallazgo
                              join i in ctx.IFC on ih.idIFC equals i.IdIFC
                              where
                                 (i.UnidadAcademica.NombreEspanol == (string.IsNullOrEmpty(idCursoUnidadAcademica) ? i.UnidadAcademica.NombreEspanol : idCursoUnidadAcademica)
                                 || i.UnidadAcademica.NombreIngles == (string.IsNullOrEmpty(idCursoUnidadAcademica) ? i.UnidadAcademica.NombreIngles : idCursoUnidadAcademica))
                                 &&
                                 (i.UnidadAcademica.UnidadAcademica2.NombreEspanol == (string.IsNullOrEmpty(idSubareaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.NombreEspanol : idSubareaUnidadAcademica)
                                 || i.UnidadAcademica.UnidadAcademica2.NombreIngles == (string.IsNullOrEmpty(idSubareaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.NombreIngles : idSubareaUnidadAcademica))
                                 &&
                                 (i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol == (string.IsNullOrEmpty(idAreaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol : idAreaUnidadAcademica)
                                 || i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles == (string.IsNullOrEmpty(idAreaUnidadAcademica) ? i.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles : idAreaUnidadAcademica))
                              select h);
            return hallazgosZ.ToList();
        }

        public static List<Hallazgos> GetHallazgosForIfc(AbetEntities ctx, int idIfc)
        {
            var query = ctx.IFCHallazgo.Where(x => x.idIFC == idIfc).Select(x => x.Hallazgos).GroupBy(x => x.Codigo);
            var lstHallazgo = query.Select(x => x.FirstOrDefault()).ToList();
            return lstHallazgo;
        }

        public static List<Hallazgos> GetHallazgosZForIfc(AbetEntities ctx, int idIfc)                                                                          // RML0001
        {
            var query = ctx.IFCHallazgo.Where(x => x.idIFC == idIfc).Select(x => x.Hallazgos).GroupBy(x => x.IdHallazgo);                                       // RML0001
            var lstHallazgo = query.Select(x => x.FirstOrDefault()).ToList();                                                                                   // RML0001
            return lstHallazgo;                                                                                                                                 // RML0001
        }

        public static List<Hallazgo> GetHallazgosForAcc(AbetEntities ctx, int? idSubModalidadPeriodoAcademico, int? idSede, int? idCarrera, string codigoNivelAceptacion)
        {
            var hallazgos = (from h in ctx.Hallazgo
                             where h.IdSede == (idSede ?? h.IdSede)
                                    && h.IdSubModalidadPeriodoAcademico == (idSubModalidadPeriodoAcademico ?? h.IdSubModalidadPeriodoAcademico)
                                    && h.NivelDificultad == (!string.IsNullOrEmpty(codigoNivelAceptacion) ? codigoNivelAceptacion : h.NivelDificultad)
                             select h);
            //hallazgos = (from h in ctx.Hallazgo
            //             where h.IdActa != null
            //             select h);

            hallazgos = (from h in hallazgos
                         join ac in ctx.Acta on h.IdActa equals ac.IdActa
                         select h);

            return hallazgos.ToList();
        }

        public static bool EliminarHallazgo(AbetEntities ctx, int idHallazgo)
        {
            try
            {
                var ifcHallazgos = (from ih in ctx.IFCHallazgo where ih.IdHallazgo == idHallazgo select ih);
                ctx.IFCHallazgo.RemoveRange(ifcHallazgos);

                var hallazgoAccionesMejora =
                    (from ham in ctx.HallazgoAccionMejora where ham.IdHallazgo == idHallazgo select ham);
                var accionesMejora = (from ham in ctx.HallazgoAccionMejora
                                      join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                      where ham.IdHallazgo == idHallazgo
                                      select am);
                ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
                ctx.AccionMejora.RemoveRange(accionesMejora);

                var hallazgo = (from h in ctx.Hallazgo where h.IdHallazgo == idHallazgo select h).FirstOrDefault();
                ctx.Hallazgo.Remove(hallazgo);
                return true;
            }
            catch (Exception ex)
            {
                // TODO: Log exception
                return false;
            }
        }

        /// <summary>
        /// Método para obtener el código actualizado para un hallazgo de IFC
        /// </summary>
        /// <param name="codigo">Código actual del hallazgo de IFC</param>
        /// <param name="idHallazgo">Código identificador del hallazgo de IFC</param>
        /// <returns>Código actualizado para el hallazgo de IFC</returns>
        public static string MakeCodigoForHallazgoIfc(string codigo)
        {
            var elementos = codigo.Split(new char[] { '-' }).ToList();
            char[] TempChar = { 'T', 'E', 'M', 'P' };
            var cadena = elementos.Last();
            cadena = cadena.TrimStart(TempChar);
            elementos.RemoveAt(elementos.Count() - 1);
            var partial = "";
            foreach (var elemento in elementos)
            {
                partial += elemento + "-";
            }
            partial += cadena;
            return partial;
        }

        public static bool ActualizarIfc(AbetEntities ctx, IFC actualIfc, IFC ifc)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    AccionMejora antiguoAccionMejora = null;
                    var codigosActuales = new List<string>();
                    foreach (var hallazgo in GetHallazgosForIfc(ctx, actualIfc.IdIFC))
                    {
                        codigosActuales.Add(hallazgo.Codigo);
                    }
                    var codigosNuevos = new List<string>();
                    foreach (var ifcHallazgo in ifc.IFCHallazgo)
                    {
                        codigosNuevos.Add(ifcHallazgo.Hallazgos.Codigo);
                    }

                    foreach (var codigoActual in codigosActuales)
                    {
                        bool encontrado = false;
                        foreach (var codigoNuevo in codigosNuevos)
                        {
                            if (codigoActual.Equals(codigoNuevo))
                            {
                                encontrado = true;
                                break;
                            }
                        }
                        if (!encontrado)
                        {
                            EliminarHallazgo(ctx, GetHallazgo(ctx, codigoActual).IdHallazgo);
                        }
                    }

                    codigosActuales = new List<string>();
                    foreach (var accion in AccionMejoraDaoImpl.GetAccionMejorasForIfc(ctx, actualIfc.IdIFC))
                    {
                        codigosActuales.Add(accion.Codigo);
                    }
                    codigosNuevos = new List<string>();
                    foreach (var ifcHallazgo in ifc.IFCHallazgo)
                    {
                        var hallazgo = ifcHallazgo.Hallazgos;

                        foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                        {
                            codigosNuevos.Add(hallazgoAccionMejora.AccionMejora.Codigo);
                        }
                    }

                    codigosNuevos = codigosNuevos.Distinct().ToList();
                    foreach (var codigoActual in codigosActuales)
                    {
                        bool encontrado = false;
                        foreach (var codigoNuevo in codigosNuevos)
                        {
                            if (codigoActual.Equals(codigoNuevo))
                            {
                                encontrado = true;
                                break;
                            }
                        }
                        if (!encontrado)
                        {
                            AccionMejoraDaoImpl.EliminarAccionMejora(ctx, AccionMejoraDaoImpl.GetAccionMejora(ctx, codigoActual).IdAccionMejora);
                        }
                    }

                    foreach (var ifcHallazgo in ifc.IFCHallazgo)
                    {
                        var hallazgo = ifcHallazgo.Hallazgos;
                        var actualHallazgo = GetHallazgo(ctx, hallazgo.Codigo);
                        if (actualHallazgo != null)
                        {
                            foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                            {
                                var accionMejora = hallazgoAccionMejora.AccionMejora;
                                var actualAccionMejora = AccionMejoraDaoImpl.GetAccionMejora(ctx, accionMejora.Codigo);
                                if (actualAccionMejora != null)
                                {
                                    // do nothing
                                }
                                else
                                {
                                    var nuevoHallazgoAccionMejora = new HallazgoAccionMejora();
                                    nuevoHallazgoAccionMejora.Hallazgos = actualHallazgo;
                                    var nuevaAccionMejora = new AccionMejora();
                                    nuevaAccionMejora.Codigo = accionMejora.Codigo;
                                    nuevaAccionMejora.DescripcionEspanol = accionMejora.DescripcionEspanol;
                                    nuevaAccionMejora.DescripcionIngles = accionMejora.DescripcionIngles;
                                    nuevaAccionMejora.Estado = accionMejora.Estado;

                                    nuevoHallazgoAccionMejora.AccionMejora = nuevaAccionMejora;
                                    actualHallazgo.HallazgoAccionMejora.Add(nuevoHallazgoAccionMejora);
                                }
                            }
                        }
                        else
                        {
                            int idSubModalidadPeriodoAcademico = ctx.UnidadAcademica.Where(x => x.IdUnidadAcademica == actualIfc.IdUnidadAcademica).FirstOrDefault().CursoPeriodoAcademico.IdSubModalidadPeriodoAcademico.Value;
                            var OutcomeComisionDelCurso = ctx.UnidadAcademicaResponsable.Include(x => x.SedeUnidadAcademica)
                              .Where(x => x.IdDocente == actualIfc.IdDocente).Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                              .Where(x => x.Nivel == 3 && x.IdUnidadAcademica == actualIfc.IdUnidadAcademica).Select(x => x.CursoPeriodoAcademico)
                              .Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico && x.IdCurso == actualIfc.UnidadAcademica.CursoPeriodoAcademico.IdCurso)
                              .Select(x => x.Curso.CursoMallaCurricular).FirstOrDefault().SelectMany(x => x.MallaCocosDetalle)
                              .Where(x => x.MallaCocos.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico)
                              .Select(x => x.OutcomeComision).ToList();

                            if (OutcomeComisionDelCurso != null)
                            {
                                foreach (var outcomeComision in OutcomeComisionDelCurso)
                                {
                                    var nuevoIfcHallazgo = new IFCHallazgo();
                                    nuevoIfcHallazgo.IFC = actualIfc;
                                    var nuevoHallazgo = new Hallazgos();
                                    nuevoHallazgo.Codigo = hallazgo.Codigo;
                                    nuevoHallazgo.DescripcionEspanol = hallazgo.DescripcionEspanol;
                                    nuevoHallazgo.DescripcionIngles = hallazgo.DescripcionIngles;
                                    nuevoHallazgo.IdSubModalidadPeriodoAcademicoModulo = hallazgo.IdSubModalidadPeriodoAcademicoModulo;
                                    nuevoHallazgo.IdConstituyenteInstrumento = hallazgo.IdConstituyenteInstrumento;
                                    nuevoHallazgo.IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;
                                    nuevoHallazgo.IdConstituyenteInstrumento = hallazgo.IdConstituyenteInstrumento;
                                    nuevoHallazgo.IdCurso = hallazgo.IdCurso;
                                    nuevoHallazgo.FechaRegistro = hallazgo.FechaRegistro;
                                    nuevoIfcHallazgo.Hallazgos = nuevoHallazgo;
                                    actualIfc.IFCHallazgo.Add(nuevoIfcHallazgo);

                                    foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                                    {
                                        var accionMejora = hallazgoAccionMejora.AccionMejora;
                                        var actualAccionMejora = AccionMejoraDaoImpl.GetAccionMejora(ctx, accionMejora.Codigo);
                                        var nuevoHallazgoAccionMejora = new HallazgoAccionMejora();
                                        nuevoHallazgoAccionMejora.IdHallazgo = nuevoHallazgo.IdHallazgo;
                                        if (actualAccionMejora != null)
                                        {
                                            nuevoHallazgoAccionMejora.AccionMejora = actualAccionMejora;
                                            nuevoHallazgo.HallazgoAccionMejora.Add(nuevoHallazgoAccionMejora);
                                        }
                                        else
                                        {
                                            if (antiguoAccionMejora == null)
                                            {
                                                var nuevaAccionMejora = new AccionMejora();
                                                nuevaAccionMejora.Codigo = accionMejora.Codigo;
                                                nuevaAccionMejora.DescripcionEspanol = accionMejora.DescripcionEspanol;
                                                nuevaAccionMejora.DescripcionIngles = accionMejora.DescripcionIngles;
                                                nuevaAccionMejora.Estado = accionMejora.Estado;
                                                antiguoAccionMejora = nuevaAccionMejora;
                                            }

                                            nuevoHallazgoAccionMejora.AccionMejora = antiguoAccionMejora;
                                            nuevoHallazgo.HallazgoAccionMejora.Add(nuevoHallazgoAccionMejora);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var nuevoIfcHallazgo = new IFCHallazgo();
                                nuevoIfcHallazgo.IFC = actualIfc;
                                var nuevoHallazgo = new Hallazgos();
                                nuevoHallazgo.Codigo = hallazgo.Codigo;
                                nuevoHallazgo.DescripcionEspanol = hallazgo.DescripcionEspanol;
                                nuevoHallazgo.DescripcionIngles = hallazgo.DescripcionIngles;
                                nuevoHallazgo.IdSubModalidadPeriodoAcademicoModulo = hallazgo.IdSubModalidadPeriodoAcademicoModulo;            
                                nuevoHallazgo.IdConstituyenteInstrumento = hallazgo.IdConstituyenteInstrumento;
                                nuevoHallazgo.IdNivelAceptacionHallazgo = hallazgo.IdNivelAceptacionHallazgo;   
                                nuevoHallazgo.FechaRegistro = hallazgo.FechaRegistro;
                                nuevoHallazgo.IdCurso = hallazgo.IdCurso;
                                nuevoIfcHallazgo.Hallazgos = nuevoHallazgo;
                                actualIfc.IFCHallazgo.Add(nuevoIfcHallazgo);

                                foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                                {
                                    var accionMejora = hallazgoAccionMejora.AccionMejora;
                                    var actualAccionMejora = AccionMejoraDaoImpl.GetAccionMejora(ctx, accionMejora.Codigo);
                                    var nuevoHallazgoAccionMejora = new HallazgoAccionMejora();
                                    nuevoHallazgoAccionMejora.Hallazgos = nuevoHallazgo;
                                    if (actualAccionMejora != null)
                                    {
                                        nuevoHallazgoAccionMejora.AccionMejora = actualAccionMejora;
                                        nuevoHallazgo.HallazgoAccionMejora.Add(nuevoHallazgoAccionMejora);
                                    }
                                    else
                                    {
                                        if (antiguoAccionMejora == null)
                                        {
                                            var nuevaAccionMejora = new AccionMejora();
                                            nuevaAccionMejora.Codigo = accionMejora.Codigo;
                                            nuevaAccionMejora.DescripcionEspanol = accionMejora.DescripcionEspanol;
                                            nuevaAccionMejora.DescripcionIngles = accionMejora.DescripcionIngles;
                                            nuevaAccionMejora.Estado = accionMejora.Estado;
                                            antiguoAccionMejora = nuevaAccionMejora;
                                        }

                                        nuevoHallazgoAccionMejora.AccionMejora = antiguoAccionMejora;
                                        nuevoHallazgo.HallazgoAccionMejora.Add(nuevoHallazgoAccionMejora);
                                    }
                                }
                            }                            
                        }
                    }

                    ctx.SaveChanges();

                    foreach (var ifcHallazgo in actualIfc.IFCHallazgo)
                    {
                        var hallazgo = ifcHallazgo.Hallazgos;
                        hallazgo.Codigo = MakeCodigoForHallazgoIfc(hallazgo.Codigo);

                        foreach (var hallazgoAccionMejora in hallazgo.HallazgoAccionMejora)
                        {
                            var accionMejora = hallazgoAccionMejora.AccionMejora;
                            accionMejora.Codigo = AccionMejoraDaoImpl.MakeCodigoForAccionMejoraIfc(accionMejora.Codigo);
                        }
                    }

                    ctx.SaveChanges();

                    transaction.Commit();

                    return true;
                }
                catch (DbEntityValidationException ex)
                {
                    // TODO: log exception
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                    transaction.Rollback();
                    return false;
                }
            }
        }



        public static List<Tuple<string, string>> GetAllNivelesAceptacionForHallazgos()
        {
            var result = new List<Tuple<string, string>>();

            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.ESPERADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.ESPERADO.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.SOBRESALIENTE.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.SOBRESALIENTE.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NECESITA_MEJORA.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NECESITA_MEJORA.TEXTO));

            return result;
        }


        //public static List<Hallazgo> GetHallazgosIfc(AbetEntities ctx, int? idAreaUnidadAcademica,
        //    int? idSubareaUnidadAcademica, int? idCursoUnidadAcademica, int? idPeriodoAcademico,
        //    string codigoNivelAceptacion, int? idSede)
        //{
        //    var hallazgos = (from h in ctx.Hallazgo
        //        where h.IdSede == (idSede ?? h.IdSede)
        //              && h.IdPeriodoAcademico == (idPeriodoAcademico ?? h.IdPeriodoAcademico)
        //              &&
        //              h.NivelDificultad ==
        //              (!string.IsNullOrEmpty(codigoNivelAceptacion) ? codigoNivelAceptacion : h.NivelDificultad)
        //        select h);

        //    hallazgos = (from h in hallazgos
        //        join ih in ctx.IFCHallazgo on h.IdHallazgo equals ih.IdHallazgo
        //        join i in ctx.IFC on ih.idIFC equals i.IdIFC
        //        where i.IdUnidadAcademica == (idCursoUnidadAcademica ?? i.IdUnidadAcademica)
        //              &&
        //              i.UnidadAcademica.IdUnidadAcademicaPadre ==
        //              (idSubareaUnidadAcademica ?? i.UnidadAcademica.IdUnidadAcademicaPadre)
        //              &&
        //              i.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre ==
        //              (idAreaUnidadAcademica ?? i.UnidadAcademica.UnidadAcademica2.IdUnidadAcademicaPadre)
        //        select h);

        //    return hallazgos.ToList();
        //}

    }
}
