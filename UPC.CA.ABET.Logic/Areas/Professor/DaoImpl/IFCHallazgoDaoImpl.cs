﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class IFCHallazgoDaoImpl
    {
        public static bool EliminarHallazgoIfc(AbetEntities ctx, int idHallazgo)
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var ifcHallazgos = (from ih in ctx.IFCHallazgo where ih.IdHallazgo == idHallazgo select ih).ToList();
                    ctx.IFCHallazgo.RemoveRange(ifcHallazgos);

                    var hallazgoAccionesMejora =
                        (from ham in ctx.HallazgoAccionMejora where ham.IdHallazgo == idHallazgo select ham).ToList();
                    var accionesMejora = (from ham in ctx.HallazgoAccionMejora
                                          join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                          where ham.IdHallazgo == idHallazgo
                                          select am).ToList();

                    var accionesMejoraPlan = ctx.PlanAccion.SelectMany(x => x.AccionMejoraPlanAccion)
                        .Select(x => x.AccionMejora).ToList();
                    
                    var existeAccion = accionesMejoraPlan.Select(X => X.IdAccionMejora).ToList();

                    var acciones = accionesMejora.Where(x => existeAccion.Any(Y => (Y == x.IdAccionMejora))).ToList();
                    if (acciones.Count == 0)
                        return false;

                    ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
                    ctx.AccionMejora.RemoveRange(accionesMejora);

                    var hallazgo = (from h in ctx.Hallazgo where h.IdHallazgo == idHallazgo select h).FirstOrDefault();
                    ctx.Hallazgo.Remove(hallazgo);

                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: Log exception
                    transaction.Rollback();

                    return false;
                }
            }
        }

        public static bool EliminarHallazgoZIfc(AbetEntities ctx, int idHallazgo)                                                           // RML001
        {
            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    var ifcHallazgos = (from ih in ctx.IFCHallazgo where ih.IdHallazgo == idHallazgo select ih).ToList();
                    ctx.IFCHallazgo.RemoveRange(ifcHallazgos);

                    var hallazgoAccionesMejora =
                        (from ham in ctx.HallazgoAccionMejora where ham.IdHallazgo == idHallazgo select ham).ToList();
                    var accionesMejora = (from ham in ctx.HallazgoAccionMejora
                                          join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                                          where ham.IdHallazgo == idHallazgo
                                          select am).ToList();

                    var accionesMejoraPlan = ctx.PlanAccion.SelectMany(x => x.AccionMejoraPlanAccion)
                        .Select(x => x.AccionMejora).ToList();
                    
                    var existeAccion = accionesMejoraPlan.Select(X => X.IdAccionMejora).ToList();

                    var acciones = accionesMejora.Where(x => existeAccion.Any(Y => (Y == x.IdAccionMejora))).ToList();
                    /*                                                                                                                          // RML001 Reglas del negocio?
                    if (acciones.Count == 0)
                        return false;

                    */
                    if (acciones.Count != 0)
                        return false;
                    ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
                    ctx.AccionMejora.RemoveRange(accionesMejora);

                    var hallazgo = (from h in ctx.Hallazgos where h.IdHallazgo == idHallazgo select h).FirstOrDefault();
                    ctx.Hallazgos.Remove(hallazgo);

                    ctx.SaveChanges();
                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: Log exception
                    transaction.Rollback();

                    return false;
                }
            }
        }

        public static IFCHallazgo GetIfcHallazgo(AbetEntities ctx, int idIfc, int idHallazgo)
        {
            return
                (from ifh in ctx.IFCHallazgo where ifh.idIFC == idIfc && ifh.IdHallazgo == idHallazgo select ifh)
                    .FirstOrDefault();
        }

        public static bool ActualizarHallazgoifc(AbetEntities ctx, int idHallazgo, string codigoNivelAceptacion,
           string descripcion)
        {
            var hallazgo = (from h in ctx.Hallazgo
                            where h.IdHallazgo == idHallazgo
                            select h).FirstOrDefault();

            if (hallazgo == null)
            {
                return false;
            }

            var hallazgos = ctx.Hallazgo.Where(x => x.Codigo == hallazgo.Codigo).ToList();

            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    foreach(var hall in hallazgos)
                    {
                        hall.NivelDificultad = codigoNivelAceptacion;
                        hall.DescripcionEspanol = descripcion;
                        hall.DescripcionIngles = descripcion;   
                    }
                    ctx.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: Log exception
                    transaction.Rollback();
                    return false;
                }

            }
        }

        public static bool ActualizarHallazgoZifc(AbetEntities ctx, int idHallazgo, Int32? IdNivelAceptacionHallazgo,                                                   
           string descripcion)                                                                                                                  // RML001
        {
            var hallazgo = (from h in ctx.Hallazgos
                            where h.IdHallazgo == idHallazgo
                            select h).FirstOrDefault();

            if (hallazgo == null)
            {
                return false;
            }

           //  var hallazgos = ctx.Hallazgos.Where(x => x.Codigo == hallazgo.Codigo).ToList();

            using (var transaction = ctx.Database.BeginTransaction())
            {
                try
                {
                    /*
                    foreach (var hall in hallazgos)
                    {
                        hall.NivelDificultad = codigoNivelAceptacion;
                        hall.DescripcionEspanol = descripcion;
                        hall.DescripcionIngles = descripcion;
                    }
                    ctx.SaveChanges();
                    transaction.Commit();
                    return true;
                    */
                        hallazgo.IdNivelAceptacionHallazgo = IdNivelAceptacionHallazgo; // RML001 Ajustar para idiomas
                        hallazgo.DescripcionEspanol = descripcion;
                        hallazgo.DescripcionIngles = descripcion;

                    ctx.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // TODO: Log exception
                    transaction.Rollback();
                    return false;
                }

            }
        }

        public static List<Tuple<string, string>> GetAllNivelesAceptacionForHallazgosIfc()
        {
            var result = new List<Tuple<string, string>>();

            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.ESPERADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.ESPERADO.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.SOBRESALIENTE.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.SOBRESALIENTE.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NECESITA_MEJORA.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NECESITA_MEJORA.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NO_ASIGNADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.HALLAZGOS.NIVELES_DE_ACEPTACION.NO_ASIGNADO.TEXTO));

            return result;
        }

        public static string GetTextoForCodigoNivelAceptacionHallazgoIfc(string codigo)
        {
            var estados = GetAllNivelesAceptacionForHallazgosIfc();

            foreach (var estado in estados)
            {
                if (estado.Item1 == codigo)
                {
                    return estado.Item2;
                }
            }

            return "No especificado";
        }


        public static string GetTextoForIdNivelAceptacionHallazgoIfc(int? IdNivelAceptacionHallazgo)                                                                 // RML001
        {
            AbetEntities context = new AbetEntities();

            if (IdNivelAceptacionHallazgo.HasValue)
                return context.NivelAceptacionHallazgo.Where(x => x.IdNivelAceptacionHallazgo == IdNivelAceptacionHallazgo).FirstOrDefault().NombreEspanol;             // RML001 Manejar idomas
            else
                return "No Asignado";

        }

    }
}
