﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Models;


namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class UnidadAcademicaDaoImpl
    {
        public static List<UnidadAcademica> GetAllAreasInPeriodoAcademico(AbetEntities ctx, int idSubModalidadPeriodoAcademico, int idEscuela)
        {//Idsubmodalidad de filtro correcto
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 1
                        && ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                        && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }

        public static List<UnidadAcademica> GetAllCarrerasInPeriodoAcademico(AbetEntities ctx, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (ctx.UnidadAcademica.Where(x => x.Nivel == 1 && x.IdCarreraPeriodoAcademico != null && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.IdEscuela == idEscuela).ToList());
        }

        public static List<UnidadAcademica> GetAllSUbareasInArea(AbetEntities ctx, string idAreaUnidadAcademica, int idEscuela)
        {
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 2
                        && (ua.UnidadAcademica2.NombreEspanol == idAreaUnidadAcademica || ua.UnidadAcademica2.NombreIngles == idAreaUnidadAcademica)
                        && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }
        public static List<UnidadAcademica> GetAllSUbareasInAreaInt(AbetEntities ctx, int idAreaUnidadAcademica, int idEscuela)
        {
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 2
                 && (ua.IdUnidadAcademicaPadre == idAreaUnidadAcademica)
                 && ua.IdEscuela == idEscuela
             select ua).ToList();

        }
        public static List<UnidadAcademica> GetAllCursosInArea(AbetEntities ctx, string idAreaUnidadAcademica, int idEscuela)
        {
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 3                          
                          && (ua.UnidadAcademica2.UnidadAcademica2.NombreEspanol == idAreaUnidadAcademica || ua.UnidadAcademica2.UnidadAcademica2.NombreIngles == idAreaUnidadAcademica)
                          && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }

        public static List<UnidadAcademica> GetAllCursosInSubarea(AbetEntities ctx, string idSubareaAcademica, int idEscuela)
        {
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 3
                          && (ua.UnidadAcademica2.NombreEspanol == idSubareaAcademica || ua.UnidadAcademica2.NombreIngles == idSubareaAcademica)
                          && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }

        public static List<UnidadAcademica> GetAllEscuelas(AbetEntities ctx, int idEscuela)
        {
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 0 && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }

        public static List<UnidadAcademica> GetUnidadAcademicaNivel(AbetEntities ctx, Int32 IdDocente, Int32 Nivel, Int32 IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from c in ctx.UnidadAcademica join cc in ctx.SedeUnidadAcademica
                    on c.IdUnidadAcademica equals cc.IdUnidadAcademica join ccc in ctx.UnidadAcademicaResponsable
                    on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                    where ccc.IdDocente == IdDocente && c.Nivel == Nivel && c.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                    select c).Distinct().ToList();
        }

        public static UnidadAcademica GetUnidadAcademica(AbetEntities ctx, int idUnidadAcademica)
        {
            return
                (from ua in ctx.UnidadAcademica where ua.IdUnidadAcademica == idUnidadAcademica select ua)
                    .FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener las áreas pertenecientes a una unidad académica que representa un curso
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idUnidadAcademica">Código identificador de la unidad académica que representa el curso del cual se quiere obtener las áreas</param>
        /// <returns>Unidad académica que representa el área al que pertenece el curso ingresado</returns>
        public static UnidadAcademica GetAreaForCurso(AbetEntities ctx, int idUnidadAcademica)
        {
            var unidadAcademica = GetUnidadAcademica(ctx, idUnidadAcademica);

            while (unidadAcademica.Nivel > 1)
            {
                if (unidadAcademica.IdUnidadAcademicaPadre == null)
                {
                    return null;
                }
                idUnidadAcademica = (int)unidadAcademica.IdUnidadAcademicaPadre;
                unidadAcademica = GetUnidadAcademica(ctx, idUnidadAcademica);
            }

            return unidadAcademica;
        }

        /// <summary>
        /// Método para obtener las unidades académicas que representan áreas disponibles para el docente para realizar un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idSubModalidadPeriodoAcademico"></param>
        /// <returns>Lista con todas las unidades académicas que representan áreas disponibles para el docente para realizar el informe de fin de ciclo</returns>
        public static List<UnidadAcademica> GetAreasForIfc(AbetEntities ctx, int idDocente, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
             List<UnidadAcademica> lstUn =(from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                            && (ua.Nivel == 3)
                            && ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                            && ua.IdEscuela == idEscuela
                    select ua.UnidadAcademica2.UnidadAcademica2).Distinct().ToList();
            return lstUn;
        }

        public static List<UnidadAcademica> GetSubareasForIfc(AbetEntities ctx, int idDocente, int idSubModalidadPeriodoAcademico, int idAreaUnidadAcademica, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                            && ua.Nivel == 3 && ua.IdEscuela == idEscuela
                            && ua.UnidadAcademica2.IdUnidadAcademicaPadre == idAreaUnidadAcademica
                            && ua.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico
                            &&  ua.UnidadAcademica2.IdEscuela == idEscuela
                    select ua.UnidadAcademica2).Distinct().ToList();
        }

        public static List<UnidadAcademica> GetCursosUnidadAcademicaForIfc(AbetEntities ctx, int idDocente,
            int IdSubModalidadPeriodoAcademico, int idSubareaUnidadAcademica, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                            && ua.Nivel == 3
                            && ua.IdUnidadAcademicaPadre == idSubareaUnidadAcademica
                            && ua.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                            && ua.IdEscuela == idEscuela
                    select ua).Distinct().ToList();
        }


        public static List<UnidadAcademica> GetAllCursos(AbetEntities ctx, int idEscuela,int parModalidadId)
        {
            List<UnidadAcademica> result = (from ua in ctx.UnidadAcademica
                    join submodapa in ctx.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                    join submoda in ctx.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                    where ua.Nivel == 3 && ua.IdEscuela == idEscuela && submoda.IdModalidad== parModalidadId
                    select ua).Distinct().ToList();

            return result;
        }

        /// <summary>
        /// Método para obtener todas las áreas
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todas las unidades académicas que representan áreas</returns>
        public static List<UnidadAcademica> GetAllAreas(AbetEntities ctx, int idEscuela, int parModalidadId)
        {
            return (from ua in ctx.UnidadAcademica 
                    join submodapa in ctx.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                    join submoda in ctx.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                    where ua.Nivel == 1 && ua.IdEscuela == idEscuela && submoda.IdModalidad==parModalidadId
                    select ua).ToList();
        }

        /// <summary>
        /// Método para obtener todas las subáreas
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todas las unidades académicas que representan subáreas</returns>
        public static List<UnidadAcademica> GetAllSubareas(AbetEntities ctx, int idEscuela, int parModalidadId)
        {
            return (from ua in ctx.UnidadAcademica
                    join submodapa in ctx.SubModalidadPeriodoAcademico on ua.IdSubModalidadPeriodoAcademico equals submodapa.IdSubModalidadPeriodoAcademico
                    join submoda in ctx.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
                    where ua.Nivel == 2 && ua.IdEscuela == idEscuela && submoda.IdModalidad==parModalidadId && ua.Tipo == "SUBAREA"
                    select ua).ToList();
        }

        public static List<UnidadAcademica> GetAllUnidadesByUsuarioAndNivel(AbetEntities ctx, HttpSessionStateBase Session, int nivel)
        {
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            if (Helpers.SessionHelper.GetDocenteId(Session) != null)
            {
                
                int idDocente = Helpers.SessionHelper.GetDocenteId(Session).Value;
                int IdSubModalidadPeriodoAcademico = Helpers.SessionHelper.GetPeriodoAcademicoId(Session).Value;
                var res = (from c in ctx.UnidadAcademica
                           join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                           join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                           where ccc.Docente.IdDocente == idDocente && c.Nivel == nivel && c.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                           select c).Distinct().ToList();
                //res.Sort(); 
                
                return res;
            }
            else
            {
                var res = (from c in ctx.UnidadAcademica
                           where c.Nivel == nivel && c.IdEscuela == idEscuela
                           select c).ToList();
                return res;
            }
        }

        public static List<UnidadAcademica> GetAllAreasByUsuario(AbetEntities ctx, HttpSessionStateBase Session)
        {
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 1 && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }
        public static List<UnidadAcademica> GetAllAreasByCarrera(AbetEntities ctx,int IdEscuela,int IdCarrera, int? IdSubModalidadPeriodoAcademico)
        {
            try
            {
                int IdCarreraPeriodoAcademico = ctx.CarreraPeriodoAcademico.FirstOrDefault(x => x.IdCarrera == IdCarrera && x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico).IdCarreraPeriodoAcademico;

                return (from ua in ctx.UnidadAcademica
                        where ua.Nivel == 1
                        && ua.IdCarreraPeriodoAcademico == IdCarreraPeriodoAcademico
                        && ua.IdEscuela == IdEscuela
                        select ua).ToList();
            }
            catch (Exception ex) {

                throw new Exception(ex.Message);
            }
        }
        public static List<UnidadAcademica> GetAllSubareasByUsuario(AbetEntities ctx, HttpSessionStateBase Session)
        {
            var idEscuela = Helpers.SessionHelper.GetEscuelaId(Session);
            return (from ua in ctx.UnidadAcademica
                    where ua.Nivel == 2 && ua.IdEscuela == idEscuela
                    select ua).ToList();
        }

        /// <summary>
        /// Método para obtener las unidades académicas que representan subáreas disponibles para el docente para realizar un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las unidades académicas que representan subáreas disponibles para el docente para realizar el informe de fin de ciclo</returns>
        public static List<UnidadAcademica> GetSubareasForIfc(AbetEntities ctx, int idDocente, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where d.IdDocente == idDocente
                            && ua.Nivel == 2
                            && ua.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                            && ua.IdEscuela == idEscuela
                    select ua).Distinct().ToList();
        }


        /// <summary>
        /// Método para obtener la unidad académica para el docente para realizar la creación del informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <param name="idCurso">Código identificador del curso</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Unidad académica que cumple con los criterios ingresados</returns>
        public static UnidadAcademica GetUnidadAcademicaForIfc(AbetEntities ctx, int idDocente, int idCurso, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from d in ctx.Docente
                    join uar in ctx.UnidadAcademicaResponsable on d.IdDocente equals uar.IdDocente
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join s in ctx.Sede on sua.IdSede equals s.IdSede
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    join cpa in ctx.CursoPeriodoAcademico on ua.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                    where d.IdDocente == idDocente
                          && ua.Nivel == 3 && ua.IdEscuela == idEscuela
                          && cpa.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                          && cpa.IdCurso == idCurso
                    select ua).FirstOrDefault();
        }

      

        public static List<int> GetAllNivelesUnidadesAcademicas(AbetEntities ctx, int idEscuela)
        {
            return ctx.UnidadAcademica.Where(X => X.Nivel > 0 && X.IdEscuela == idEscuela).Select(X => X.Nivel).Distinct().ToList();
        }

        public static List<UnidadAcademica> GetAllUnidadesAcademicas(AbetEntities ctx, int idEscuela)
        {
            return (from d in ctx.UnidadAcademica
                    where d.Nivel > 0 && d.IdEscuela == idEscuela
                    select d).ToList();
        }

        public static List<UnidadAcademica> GetUnidadesAcademicasDocente(AbetEntities ctx, int idDocente,int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            List<UnidadAcademica> res = (from c in ctx.UnidadAcademica
                                         join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                                         join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                                         where ccc.Docente.IdDocente == idDocente && c.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                                         select c).Distinct().OrderBy(m => m.Nivel).ToList();
            return res;
        }

        public static UnidadAcademica GetUnidadAcademicaForMeeting(AbetEntities ctx, int idDocente, int idNivel, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return (from uar in ctx.UnidadAcademicaResponsable
                    join sua in ctx.SedeUnidadAcademica on uar.IdSedeUnidadAcademica equals sua.IdSedeUnidadAcademica
                    join ua in ctx.UnidadAcademica on sua.IdUnidadAcademica equals ua.IdUnidadAcademica
                    where uar.IdDocente == idDocente
                          && ua.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico
                          && ua.Nivel == idNivel
                          && ua.IdEscuela == idEscuela
                    select ua).FirstOrDefault();
        }

        public static List<UnidadAcademica> GetAreasByPeriodoAcademico(AbetEntities ctx,  int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            return ctx.UnidadAcademica.Where(x => x.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && x.Nivel == 1 && x.IdEscuela == idEscuela).ToList();
            //List<UnidadAcademica> res = (from c in ctx.UnidadAcademica
            //                             join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
            //                             join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
            //                             where c.IdPeriodoAcademico == idPeriodoAcademico && c.Nivel == 1
            //                             select c).Distinct().OrderBy(m => m.Nivel).ToList();
            //return res;
        }

        public static bool DocentePerteneceOrganigrama(AbetEntities ctx,int? idDocente, int IdSubModalidadPeriodoAcademico, int idEscuela)
        {
            var res = new List<UnidadAcademica>();
            if (idDocente != 2)
            {
                 res = (from c in ctx.UnidadAcademica
                                             join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                                             join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                                             where c.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && ccc.IdDocente == idDocente && c.IdEscuela == idEscuela
                                             select c).ToList();
            }
            else {

                res = (from c in ctx.UnidadAcademica
                       join cc in ctx.SedeUnidadAcademica on c.IdUnidadAcademica equals cc.IdUnidadAcademica
                       join ccc in ctx.UnidadAcademicaResponsable on cc.IdSedeUnidadAcademica equals ccc.IdSedeUnidadAcademica
                       where c.IdSubModalidadPeriodoAcademico == IdSubModalidadPeriodoAcademico && c.IdEscuela == idEscuela
                       select c).ToList();
            }
            return res.Count() > 0 ? true : false;                                
        }
    }
}
