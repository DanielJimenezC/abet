﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class TemaPreAgendaDaoImpl
    {
        public static List<Tuple<string, string>> GetAllEstadosTemaPreAgenda(AbetEntities ctx)
        {
            return new List<Tuple<string, string>>
            {
                new Tuple<string, string>
                (
                    ConstantHelpers.PROFESSOR.TEMA_PREAGENDA.ESTADOS.PENDIENTE.CODIGO,
                    ConstantHelpers.PROFESSOR.TEMA_PREAGENDA.ESTADOS.PENDIENTE.TEXTO
                ),
                new Tuple<string, string>
                (
                    ConstantHelpers.PROFESSOR.TEMA_PREAGENDA.ESTADOS.REALIZADO.CODIGO,
                    ConstantHelpers.PROFESSOR.TEMA_PREAGENDA.ESTADOS.REALIZADO.TEXTO
                )
            };
        }
    }
}
