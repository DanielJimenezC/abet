﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class HallazgoAccionMejoraDaoImpl
    {

        public static HallazgoAccionMejora GetHallazgoAccionMejora(AbetEntities ctx, int idHallazgo, int idAccionMejora)
        {
            return (from ham in ctx.HallazgoAccionMejora
                    where ham.IdHallazgo == idHallazgo && ham.IdAccionMejora == idAccionMejora
                    select ham).FirstOrDefault();
        }


    }
}
