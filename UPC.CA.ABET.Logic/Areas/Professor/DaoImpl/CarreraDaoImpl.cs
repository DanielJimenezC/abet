﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class CarreraDaoImpl
    {
        /// <summary>
        /// Método para obtener las carreras pertenecientes a una comisión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idComision">Código identificador de la comisión</param>
        /// <returns>Lista con todas las carreras pertenecientes a la comisión ingresada</returns>
        public static List<Carrera> GetCarrerasInComision(AbetEntities ctx, int idComision)
        {
            return
                (from cc in ctx.CarreraComision
                 join c in ctx.Carrera on cc.IdCarrera equals c.IdCarrera
                 where cc.IdComision == idComision
                 select c).Distinct().ToList();
        }

        /// <summary>
        /// Método para obtener las carreras pertenecientes a una comisión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="codigoComision">Código identificador de la comisión</param>
        /// <returns>Lista con todas las carreras pertenecientes a la comisión ingresada</returns>
        public static List<Carrera> GetCarrerasInComision(AbetEntities ctx, string codigoComision, int idEscuela)
        {
            return
                (from cc in ctx.CarreraComision
                 join car in ctx.Carrera on cc.IdCarrera equals car.IdCarrera
                 join com in ctx.Comision on cc.IdComision equals com.IdComision
                 where com.Codigo.StartsWith(codigoComision) && car.IdEscuela == idEscuela
                 //where com.Codigo == codigoComision
                 select car).Distinct().ToList();
        }

        /// <summary>
        /// Método para obtener la carrera perteneciente a una carrera por comisión
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarreraComision">Código identificador de la carrera por comisión</param>
        /// <returns>Carrera perteneciente a la carrera por comisión ingresada</returns>
        public static Carrera GetCarreraInCarreraComision(AbetEntities ctx, int idCarreraComision)
        {
            return
                (from cc in ctx.CarreraComision
                 join c in ctx.Carrera on cc.IdCarrera equals c.IdCarrera
                 where cc.IdCarreraComision == idCarreraComision
                 select c).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener una carrera de acuerdo a su código identificador único
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idCarrera">Código identificador de la carrera</param>
        /// <returns>Carrera que tenga el código identificador ingresado</returns>
        public static Carrera GetCarrera(AbetEntities ctx, int idCarrera)
        {
            return (from c in ctx.Carrera
                    where c.IdCarrera == idCarrera
                    select c).FirstOrDefault();
        }

        /// <summary>
        /// Método para obtener todas las carreras existentes
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <returns>Lista con todas las carreras existentes</returns>
        public static IEnumerable<Carrera> GetAllCarreras(AbetEntities ctx, int idEscuela)
        {
            
            return ctx.Carrera.Where(c => c.IdEscuela == idEscuela);
        }



        public static List<Carrera> GetCarreraBySede(AbetEntities ctx, int idSede)
        {
            return (from c in ctx.Carrera
                    join sd in ctx.SedeCarrera on c.IdCarrera equals sd.IdCarrera 
                    join s in ctx.Sede on sd.IdSede equals s.IdSede 
                    where s.IdSede == idSede select c ).ToList();
        }

        
    }
}
