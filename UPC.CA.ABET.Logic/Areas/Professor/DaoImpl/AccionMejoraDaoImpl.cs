﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;

namespace UPC.CA.ABET.Logic.Areas.Professor.DaoImpl
{
    public static class AccionMejoraDaoImpl
    {
        public static void EliminarAccionMejora(AbetEntities ctx, int idAccionMejora)
        {
            var hallazgoAccionesMejora =
                (from ham in ctx.HallazgoAccionMejora where ham.IdAccionMejora == idAccionMejora select ham);
            if (hallazgoAccionesMejora != null)
            {
                ctx.HallazgoAccionMejora.RemoveRange(hallazgoAccionesMejora);
            }

            var accionMejora =
                (from am in ctx.AccionMejora where am.IdAccionMejora == idAccionMejora select am).FirstOrDefault();
            if (accionMejora != null)
            {
                ctx.AccionMejora.Remove(accionMejora);
            }
        }

        public static AccionMejora GetAccionMejora(AbetEntities ctx, string codigo)
        {
            return (from am in ctx.AccionMejora where am.Codigo == codigo select am).FirstOrDefault();
        }

        public static List<AccionMejora> GetAccionMejorasForIfc(AbetEntities ctx, int idIfc)
        {
            return (from h in ctx.Hallazgo
                    join ih in ctx.IFCHallazgo on h.IdHallazgo equals ih.IdHallazgo
                    join ham in ctx.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                    join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                    where ih.idIFC == idIfc
                    select am).Distinct().ToList();
        }

        public static List<AccionMejora> GetAccionMejorasForIfcZ(AbetEntities ctx, int idIfc)                                                                               // RML001
        {
            return (from h in ctx.Hallazgos
                    join ih in ctx.IFCHallazgo on h.IdHallazgo equals ih.IdHallazgo
                    join ham in ctx.HallazgoAccionMejora on h.IdHallazgo equals ham.IdHallazgo
                    join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                    where ih.idIFC == idIfc
                    select am).Distinct().ToList();
        }

        /// <summary>
        /// Método para obtener las acciones previas para la creación de un informe de fin de ciclo
        /// </summary>
        /// <param name="ctx">Contexto de base de datos</param>
        /// <param name="idDocente">Código identificador del docente a realizar el informe de fin de ciclo</param>
        /// <param name="idCursoUnidadAcademica">Código identificador de la unidad académica que representa un curso</param>
        /// <param name="idPeriodoAcademico">Código identificador del periodo académico</param>
        /// <returns>Lista con todas las acciones de mejora previas para la creación de un informe de fin de ciclo</returns>
        public static List<AccionMejora> GetAccionesMejoraPreviasForIfc(AbetEntities ctx, int idmodalidad, int idCursoUnidadAcademica, string cicloAcademico, int idEscuela)
        {
            /*
            string anio = cicloAcademico.Substring(0, 4);
            List<AccionMejora> lstAc = new List<AccionMejora>();
            var tempGetPrev = ctx.FuncionGetAccionesPreviasIFC(idmodalidad, idCursoUnidadAcademica, idEscuela, anio).ToList();                               // RML 001 CAMBIAR DESDE LA BD

            foreach (var accionPrevia in tempGetPrev)
            {
                AccionMejora am = new AccionMejora();
                am.IdAccionMejora = accionPrevia.IdAccionMejora;
                am.DescripcionIngles = accionPrevia.DescripcionIngles;
                am.DescripcionEspanol = accionPrevia.DescripcionEspanol;
                am.Estado = accionPrevia.Estado;
                am.Codigo = accionPrevia.Codigo;
                am.Resultado = accionPrevia.Resultado;
                am.IdEscuela = accionPrevia.IdEscuela;
                am.IdCurso = accionPrevia.IdCurso;
                am.Anio = accionPrevia.Anio;
                am.ParaPlan = accionPrevia.ParaPlan;
                am.IdSubModalidadPeriodoAcademico = accionPrevia.IdSubModalidadPeriodoAcademico;

                lstAc.Add(am);
            }
            */                                                                                                                              // CORREGIR ERROR

            
           // string anio = cicloAcademico.Substring(0, 4);
            List<AccionMejora> lstAc = new List<AccionMejora>();
            var query = ctx.Database.SqlQuery<AccionMejora>("Select * from [dbo].[FuncionGetAccionesPreviasIFC]("+ idmodalidad + ","+ idCursoUnidadAcademica + ","+ idEscuela + ","+ cicloAcademico + ")");
        //    var query = ctx.Database.SqlQuery<AccionMejora>("Select * from [dbo].[FuncionGetAccionesPreviasIFC](@p1,@p2,@p3,@p4)", idmodalidad, idCursoUnidadAcademica, idEscuela, anio);
            var tempGetPrev = query.ToList();

            if (tempGetPrev.Count() > 0)
            {
                foreach (var accionPrevia in tempGetPrev)
                {
                    AccionMejora am = new AccionMejora();
                    am.IdAccionMejora = accionPrevia.IdAccionMejora;
                    am.DescripcionIngles = accionPrevia.DescripcionIngles;
                    am.DescripcionEspanol = accionPrevia.DescripcionEspanol;
                    am.Estado = accionPrevia.Estado;
                    am.Codigo = accionPrevia.Codigo + '-' + accionPrevia.Identificador.ToString();
                    am.Resultado = accionPrevia.Resultado;
                    am.IdEscuela = accionPrevia.IdEscuela;
                    am.IdCurso = accionPrevia.IdCurso;
                    am.Anio = accionPrevia.Anio;
                    am.ParaPlan = accionPrevia.ParaPlan;
                    am.IdSubModalidadPeriodoAcademico = accionPrevia.IdSubModalidadPeriodoAcademico;

                    lstAc.Add(am);
                }

            }



            //int parModalidadId = ctx.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().SubModalidad.IdModalidad;
            //int periodoAcademicoActual = ctx.SubModalidadPeriodoAcademico.Where(x => x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademico).FirstOrDefault().IdPeriodoAcademico;
            //var periodoAcademicoActualc = ctx.PeriodoAcademico.Where(x => x.IdPeriodoAcademico == periodoAcademicoActual).FirstOrDefault();

            //Int32? idPeriodoAcademicoPrevio =  PeriodoAcademicoDaoImpl.GetPreviosPeriodoAcademico(ctx, idSubModalidadPeriodoAcademico).IdPeriodoAcademico;

            //var periodosacademicosprevios = (from pa in ctx.PeriodoAcademico
            //                                 join submodapa in ctx.SubModalidadPeriodoAcademico on pa.IdPeriodoAcademico equals submodapa.IdPeriodoAcademico
            //                                 join submoda in ctx.SubModalidad on submodapa.IdSubModalidad equals submoda.IdSubModalidad
            //                                 where ((pa.FechaFinPeriodo < periodoAcademicoActualc.FechaInicioPeriodo) && submoda.IdModalidad == parModalidadId)
            //                                 orderby pa.FechaFinPeriodo descending
            //                                 select pa).FirstOrDefault();

            //if (periodosacademicosprevios != null)
            //{
            //    idPeriodoAcademicoPrevio = periodosacademicosprevios.IdPeriodoAcademico;
            //}
            //else
            //{
            //    idPeriodoAcademicoPrevio = 1;
            //}

            //Int32? idSubModalidadPeriodoAcademicoPrevio = ctx.SubModalidadPeriodoAcademico.Where(x => x.IdPeriodoAcademico == idPeriodoAcademicoPrevio).FirstOrDefault().IdSubModalidadPeriodoAcademico;

            //UnidadAcademica unidadAcademica = ctx.UnidadAcademica.FirstOrDefault(x=>x.IdUnidadAcademica == idCursoUnidadAcademica);
            //String codigoCurso = unidadAcademica.CursoPeriodoAcademico.Curso.Codigo;
            //String nombreCurso = unidadAcademica.CursoPeriodoAcademico.Curso.NombreEspanol;

            //UnidadAcademica UnidadAcademicaPrevia = ctx.UnidadAcademica
            //    .Where(x => (idSubModalidadPeriodoAcademicoPrevio.HasValue ? (x.IdSubModalidadPeriodoAcademico == idSubModalidadPeriodoAcademicoPrevio) : false) &&
            //        (x.CursoPeriodoAcademico.Curso.Codigo.Equals(codigoCurso) || x.CursoPeriodoAcademico.Curso.NombreEspanol.Equals(nombreCurso) &&
            //        x.IdEscuela == idEscuela)
            //    ).FirstOrDefault();

            //if (UnidadAcademicaPrevia != null)
            //{
            //    var ifc = ctx.IFC.FirstOrDefault(x => x.IdUnidadAcademica == UnidadAcademicaPrevia.IdUnidadAcademica);
            //    if(ifc!=null)
            //    {
            //        var accionesPrevias = ifc.IFCHallazgo.Select(x=>x.Hallazgo)
            //            .SelectMany(x=>x.HallazgoAccionMejora).Select(x=>x.AccionMejora)
            //            .GroupBy(Y=>Y.Codigo).Select(x=>x.FirstOrDefault()).ToList();
            //        return accionesPrevias;
            //    }
            //}
            return lstAc;
            
       
        }

        //GetAccionesMejoraPreviasForActa
        public static List<AccionMejora> GetAccionesMejoraPreviasForActa(AbetEntities ctx, string nomArea, int Anio)
        {

            //return (from am in ctx.AccionMejora
            //        join ham in ctx.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
            //        join h in ctx.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
            //        join pe in ctx.PeriodoAcademico on h.IdSubModalidadPeriodoAcademico equals pe.IdPeriodoAcademico
            //        join a in ctx.Acta on h.IdActa equals a.IdActa
            //        join re in ctx.Reunion on a.IdReunion equals re.IdReunion
            //        join pre in ctx.PreAgenda on re.IdPreAgenda equals pre.IdPreAgenda
            //        join u in ctx.UnidadAcademica on pre.IdUnidaAcademica equals u.IdUnidadAcademica
            //        where u.NombreEspanol.ToUpper() == nomArea
            //              && pe.FechaFinPeriodo.Value.Year < Anio
            //              && ((pe.FechaInicioPeriodo.Value.Year == Anio - 1 && am.Estado == "IMPLE") || (am.Estado == "PROCE") || (am.Estado == "PENDI"))
            //        orderby am.Estado ascending
            //        select am).ToList();

            return (from am in ctx.AccionMejora
                    join ham in ctx.HallazgoAccionMejora on am.IdAccionMejora equals ham.IdAccionMejora
                    join h in ctx.Hallazgo on ham.IdHallazgo equals h.IdHallazgo
                    join sub in ctx.SubModalidadPeriodoAcademico on h.IdSubModalidadPeriodoAcademico equals sub.IdSubModalidadPeriodoAcademico
                    join pe in ctx.PeriodoAcademico on sub.IdPeriodoAcademico equals pe.IdPeriodoAcademico
                    join a in ctx.Acta on h.IdActa equals a.IdActa
                    join re in ctx.Reunion on a.IdReunion equals re.IdReunion
                    join pre in ctx.PreAgenda on re.IdPreAgenda equals pre.IdPreAgenda
                    join u in ctx.UnidadAcademica on pre.IdUnidaAcademica equals u.IdUnidadAcademica
                    where u.NombreEspanol.ToUpper() == nomArea
                          && pe.FechaInicioPeriodo.Value.Year < Anio
                          && ((pe.FechaInicioPeriodo.Value.Year == Anio - 1 && am.Estado == "IMPLE") || (am.Estado == "PROCE") || (am.Estado == "PENDI"))
                    orderby am.Estado ascending
                    select am).ToList();
        }

        public static List<Tuple<string, string>> GetAllEstadosAccionMejoraIfc()
        {
            var result = new List<Tuple<string, string>>();

            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.CODIGO, ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PENDIENTE.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PROCESADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.PROCESADO.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.IMPLEMENTADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.IMPLEMENTADO.TEXTO));
            result.Add(
                new Tuple<string, string>
                (ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.RECHAZADO.CODIGO, ConstantHelpers.PROFESSOR.IFC.ACCIONES_MEJORA.ESTADOS.RECHAZADO.TEXTO));

            return result;
        }

        public static string GetTextoForCodigoEstadoAccionMejoraIfc(string codigo)
        {
            var estados = GetAllEstadosAccionMejoraIfc();

            foreach (var estado in estados)
            {
                if (estado.Item1 == codigo)
                {
                    return estado.Item2;
                }
            }

            return "No especificado";
        }

        /// <summary>
        /// Método para obtener el código actualizado para una acción de mejora de IFC
        /// </summary>
        /// <param name="codigo">Código actual de la acción de mejora de IFC</param>
        /// <param name="idAccionMejora">Código identificador de la acción de mejora de IFC</param>
        /// <returns>Código actualizado para la acción de mejora de IFC</returns>
        public static string MakeCodigoForAccionMejoraIfc(string codigo)
        {
            var elementos = codigo.Split(new char[] { '-' }).ToList();
            char[] TempChar = { 'T', 'E', 'M', 'P'};
            var cadena = elementos.Last();
            cadena = cadena.TrimStart(TempChar); 
            elementos.RemoveAt(elementos.Count() - 1);
            var partial = "";
            foreach (var elemento in elementos)
            {
                partial += elemento + "-";
            }
            partial += cadena;
            return partial;
        }

        public static List<AccionMejora> GetAccionesMejoraForHallazgoIfc(AbetEntities ctx, int idHallazgo)
        {
            return (from ham in ctx.HallazgoAccionMejora
                    join am in ctx.AccionMejora on ham.IdAccionMejora equals am.IdAccionMejora
                    where ham.IdHallazgo == idHallazgo
                    select am).Distinct().ToList();
        }

        
       
    }
}
