using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Transactions;
using System.Web.Mvc;
using Microsoft.ReportingServices.Interfaces;
using UPC.CA.ABET.Models;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using System.Web;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Logic.Areas.Professor
{
    public static class GedServices
    {
        
        #region AccionMejoraServices
        public static void EliminarAccionMejoraServices(AbetEntities ctx, int idAccionMejora)
        {
            AccionMejoraDaoImpl.EliminarAccionMejora(ctx, idAccionMejora);
        }

        public static AccionMejora GetAccionMejoraServices(AbetEntities ctx, string codigo)
        {
            return AccionMejoraDaoImpl.GetAccionMejora(ctx, codigo);
        }


        public static List<AccionMejora> GetAccionMejorasForIfcServices(AbetEntities ctx, int idIfc)
        {
            return AccionMejoraDaoImpl.GetAccionMejorasForIfc(ctx, idIfc);
        }

        public static List<AccionMejora> GetAccionMejorasForIfcServicesZ(AbetEntities ctx, int idIfc)                                                                       // RML001
        {
            return AccionMejoraDaoImpl.GetAccionMejorasForIfcZ(ctx, idIfc);
        }


        public static List<AccionMejora> GetAccionesMejoraPreviasForIfcServices(AbetEntities ctx, int idmodalidad, int idCursoUnidadAcademica, string cicloAcademico, int idEscuela)
        {
            
            return AccionMejoraDaoImpl.GetAccionesMejoraPreviasForIfc(ctx, idmodalidad, idCursoUnidadAcademica, cicloAcademico, idEscuela);
        }     

        public static List<AccionMejora> GetAccionesMejoraPreviasForActaServices(AbetEntities ctx, string nomArea,int Anio)
        {
            return AccionMejoraDaoImpl.GetAccionesMejoraPreviasForActa(ctx, nomArea, Anio);
        }

        public static List<Tuple<string, string>> GetAllEstadosAccionMejoraIfcServices()
        {
            return AccionMejoraDaoImpl.GetAllEstadosAccionMejoraIfc();
        }

        public static string GetTextoForCodigoEstadoAccionMejoraIfcServices(string codigo)
        {
            return AccionMejoraDaoImpl.GetTextoForCodigoEstadoAccionMejoraIfc(codigo);
        }

        public static String MakeCodigoForAccionMejoraIfcServices(string codigo)
        {
            return AccionMejoraDaoImpl.MakeCodigoForAccionMejoraIfc(codigo);
        }

        #endregion

        #region CarreraComisionServices

        public static CarreraComision GetCarreraComisionServices(AbetEntities ctx, int idCarreraComision)
        {
            return CarreraComisionDaoImpl.GetCarreraComision(ctx, idCarreraComision);
        }

        public static CarreraComision GetCarreraComisionServices(AbetEntities ctx, int idComision, int idCarrera, int idPeriodoAcademico)
        {
            return CarreraComisionDaoImpl.GetCarreraComision(ctx, idComision, idCarrera, idPeriodoAcademico);
        }


        #endregion

        #region CarreraService
        public static List<Carrera> GetCarrerasInComisionServices(AbetEntities ctx, int idComision)
        {
            return CarreraDaoImpl.GetCarrerasInComision(ctx, idComision);
        }

        public static List<Carrera> GetCarrerasInComisionServices(AbetEntities ctx, string codigoComision, int idEscuela)
        {
            return CarreraDaoImpl.GetCarrerasInComision(ctx, codigoComision, idEscuela);
        }

        public static Carrera GetCarreraInCarreraComisionServices(AbetEntities ctx, int idCarreraComision)
        {
            return CarreraDaoImpl.GetCarreraInCarreraComision(ctx, idCarreraComision);
        }

        public static Carrera GetCarreraServices(AbetEntities ctx, int idCarrera)
        {
            return CarreraDaoImpl.GetCarrera(ctx, idCarrera);
        }

        public static IEnumerable<Carrera> GetAllCarrerasServices(AbetEntities ctx, int idEscuela)
        {   
            return CarreraDaoImpl.GetAllCarreras(ctx, idEscuela);
        }

        public static List<Carrera> GetCarreraBySedeServices(AbetEntities ctx, int idSede)
        {
            return CarreraDaoImpl.GetCarreraBySede(ctx, idSede);
        }

        

        #endregion

        #region ComisionServices

        public static List<Comision> GetAllComisionsServices(AbetEntities ctx)
        {
            return ComisionDaoImpl.GetAllComisions(ctx);
        }

        public static Comision GetComisionServices(AbetEntities ctx, int idComision)
        {
            return ComisionDaoImpl.GetComision(ctx, idComision);
        }

        public static Comision GetComisionFromCodigoAndPeriodoAcademicoServices(AbetEntities ctx, string codigo, int idSubModalidadPeriodoAcademico)
        {
            return ComisionDaoImpl.GetComisionFromCodigoAndPeriodoAcademico(ctx, codigo, idSubModalidadPeriodoAcademico);
        }

        public static IEnumerable<KeyValuePair<string, string>> GetAllCodigosFromComisionesServices(AbetEntities ctx, int idEscuela, Int32? IdCarrera, Int32? IdSubModalidadPeriodoAcaademico, Int32? IdAcreditadora)
        {
            return ComisionDaoImpl.GetAllCodigosFromComisiones(ctx, idEscuela, IdCarrera, IdSubModalidadPeriodoAcaademico, IdAcreditadora);
        }




        #endregion

        #region CursoServices
        public static Curso GetCursoServicesServices(AbetEntities ctx, int idCurso)
        {
            return CursoDaoImpl.GetCurso(ctx, idCurso);

        }

        public static List<Curso> GetCursosForAreaServices(AbetEntities ctx, int idDocente, int idArea, int idPeriodoAcademico, int idEscuela)
        {
            return CursoDaoImpl.GetCursosForArea(ctx, idDocente, idArea, idPeriodoAcademico, idEscuela);
        }

        public static Curso GetCursoServices(AbetEntities ctx, int idCurso)
        {
            return (from c in ctx.Curso where c.IdCurso == idCurso select c).FirstOrDefault();
        }

        #endregion

        #region  CursoMallaCurricularServices

        public static CursoMallaCurricular GetCursoMallaCurricularServices(AbetEntities ctx, int idCursoMallaCurricular)
        {
            return CursoMallaCurricularDaoImpl.GetCursoMallaCurricular(ctx, idCursoMallaCurricular);
        }

        public static CursoMallaCurricular GetCursoMallaCurricularForIfcServices(AbetEntities ctx, int idCurso, int idSubModalidadPeriodoAcademico)
        {
            return CursoMallaCurricularDaoImpl.GetCursoMallaCurricularForIfc(ctx, idCurso, idSubModalidadPeriodoAcademico);
        }

        #endregion

        #region Docente
        public static List<Docente> GetParticipantesReunionServices(AbetEntities ctx, int idReunion)
        {
            return DocenteDaoImpl.GetParticipantesReunion(ctx, idReunion);
        }

        public static List<ParticipanteReunion> GetParticipantesReunionExternosServices(AbetEntities ctx, int idReunion)
        {
            return DocenteDaoImpl.GetParticipantesReunionExternos(ctx, idReunion);
        }

        public static List<Docente> GetParticipantesReunionDisponiblesServices(AbetEntities ctx, int nivel,int idPeriodoAcademico, int idUnidadAcademica, int idEscuela)
        {
            return DocenteDaoImpl.GetParticipantesReunionDisponibles(ctx, nivel,idPeriodoAcademico, idUnidadAcademica, idEscuela);
        }
        
        public static List<Docente> GetParticipantesReunionExtDisponiblesServices(AbetEntities ctx, int idPeriodoAcademico)
        {
            return DocenteDaoImpl.GetParticipantesReunionExtDisponibles(ctx, idPeriodoAcademico);
        }

        public static List<Docente> GetParticipantesReunionCCDisponiblesServices(AbetEntities ctx, int nivel, int idPeriodoAcademico, int idUnidadAcademica, int idEscuela)
        {
            return DocenteDaoImpl.GetParticipantesReunionCCDisponibles(ctx, nivel, idPeriodoAcademico, idUnidadAcademica, idEscuela);
        }

        public static Docente GetDocenteServices(AbetEntities ctx, int idDocente)
        {
            return DocenteDaoImpl.GetDocente(ctx, idDocente);
        }

        public static List<Int32> GetNivelesUsuarioServices(AbetEntities ctx, Int32 IdDocente, Int32 IdPeriodoAcademico, int idEscuela)
        {
            return DocenteDaoImpl.GetNivelesUsuario(ctx, IdDocente, IdPeriodoAcademico, idEscuela);
        }

        public static List<int> GetNivelesUsuarioLogueadoServices(AbetEntities ctx, HttpSessionStateBase Session)
        {
            return DocenteDaoImpl.GetNivelesUsuarioLogueado(ctx, Session);
        }

        public static int GetNivelMaximoUsuarioLogueadoServices(AbetEntities ctx, int? idDocente, HttpSessionStateBase Session)
        {
            return DocenteDaoImpl.GetNivelMaximoUsuarioLogueado(ctx, idDocente ?? 0, Session);
        }

        public static int GetNivelMaximoDocenteServices(AbetEntities ctx, int idDocente, int idEscuela)
        {
            return DocenteDaoImpl.GetNivelMaximoDocente(ctx, idDocente, idEscuela);
        }
        #endregion

        #region Empresa
        public static List<Empresa> GetAllEmpresasServices(AbetEntities ctx)
        {
            return EmpresaDaoImpl.GetAllEmpresas(ctx);
        }

        public static Empresa CrearEmpresaServices(AbetEntities ctx, Empresa empresa)
        {
            return EmpresaDaoImpl.CrearEmpresa(ctx, empresa);
        }
        #endregion

        #region HallazgoAccionMejoraServices

        public static HallazgoAccionMejora GetHallazgoAccionMejoraServices(AbetEntities ctx, int idHallazgo, int idAccionMejora)
        {
            return HallazgoAccionMejoraDaoImpl.GetHallazgoAccionMejora(ctx, idHallazgo, idAccionMejora);
        }


        #endregion

        #region HallazgoServices

        public static Hallazgos GetHallazgoServices(AbetEntities ctx, string codigo)
        {
            return HallazgoDaoImpl.GetHallazgo(ctx, codigo);
        }

        public static List<Hallazgo> GetHallazgosIfcServices(AbetEntities ctx, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, string idCursoUnidadAcademica, int? idPeriodoAcademico,
                                                      string codigoNivelAceptacion, int? idSede)
        {
            return HallazgoDaoImpl.GetHallazgosIfc(ctx, idAreaUnidadAcademica, idCursoUnidadAcademica, idCursoUnidadAcademica, idPeriodoAcademico, codigoNivelAceptacion, idSede);

        }

        public static List<Hallazgos> GetHallazgosForIfcServices(AbetEntities ctx, int idIfc)
        {
            return HallazgoDaoImpl.GetHallazgosForIfc(ctx, idIfc);
        }

        public static List<Hallazgos> GetHallazgosZForIfcServices(AbetEntities ctx, int idIfc)                                                      // RML0001
        {                                               
            return HallazgoDaoImpl.GetHallazgosZForIfc(ctx, idIfc);                                                                                 // RML0001
        }

        public static bool EliminarHallazgoServices(AbetEntities ctx, int idHallazgo)
        {
            return HallazgoDaoImpl.EliminarHallazgo(ctx, idHallazgo);
        }

        public static List<Tuple<string, string>> GetAllNivelesAceptacionForHallazgosServices()
        {
            return HallazgoDaoImpl.GetAllNivelesAceptacionForHallazgos();
        }

        public static List<Hallazgo> GetHallazgosForAccServices(AbetEntities ctx, int? idSubModalidadPeriodoAcademico, int? idSede, int? idCarrera, string codigoNivelAceptacion)
        {
            return HallazgoDaoImpl.GetHallazgosForAcc(ctx, idSubModalidadPeriodoAcademico, idSede, idCarrera, codigoNivelAceptacion);
        }

        #endregion

        #region IFC

        public static IFC GetIfcByUnidadAcademicaServices(AbetEntities ctx, int idUnidadAcademica)
        {
            return IFCDaoImpl.GetIfcByUnidadAcademica(ctx, idUnidadAcademica);
        }

        public static List<IFC> GetIfcsServices(AbetEntities ctx, string idAreaUnidadAcademica, string idSubareaUnidadAcademica, int? idSubModalidadPeriodoAcademico, string idCursoUnidadAcademica, string estado, int idEscuela)
        {
            return IFCDaoImpl.GetIfcs(ctx, idAreaUnidadAcademica, idSubareaUnidadAcademica, idSubModalidadPeriodoAcademico, idCursoUnidadAcademica, estado, idEscuela);
        }

       
        public static string GetCodigoForIfcServices(AbetEntities ctx, IFC ifc)
        {
            return IFCDaoImpl.GetCodigoForIfc(ctx, ifc);
        }

        public static Task<bool> CrearIfcServices(AbetEntities ctx, IFC ifc)
        {
            return IFCDaoImpl.CrearIfc(ctx, ifc);
        }

        public static bool CrearIfcCargaMasivaServices(AbetEntities ctx, IFC ifc)
        {
            return IFCDaoImpl.CrearIfcCargaMasiva(ctx, ifc);
        }

        public static IFC GetIfcServices(AbetEntities ctx, int idIfc)
        {
            return IFCDaoImpl.GetIfc(ctx, idIfc);
        }

        public static bool DeleteIfcServices(AbetEntities ctx, int idIfc)
        {
            return IFCDaoImpl.DeleteIfc(ctx, idIfc);
        }

        public static bool DeleteIfcServicesZ(AbetEntities ctx, int idIfc)                                                                          // RML001
        {
            return IFCDaoImpl.DeleteIfcZ(ctx, idIfc);
        }

        public static List<Tuple<string, string>> GetAllEstadosIfcServices(string culture)
        {
            return IFCDaoImpl.GetAllEstadosIfc(culture);
        }

        public static List<Tuple<string, List<Outcome>>> GetOutcomesForIfcServices(AbetEntities ctx, int idDocente, int idCursoUnidadAcademica, int idPeriodoAcademico, int idEscuela)
        {
            return IFCDaoImpl.GetOutcomesForIfc(ctx, idDocente, idCursoUnidadAcademica, idPeriodoAcademico, idEscuela);
        }

        public static string GetTextoForCodigoEstadoIfcServices(string codigo, string culture)
        {
            return IFCDaoImpl.GetTextoForCodigoEstadoIfc(codigo, culture);
        }

        #endregion

        #region HallazgosIFC

        public static string MakeCodigoForHallazgoIfcServices(string codigo)
        {
            return HallazgoDaoImpl.MakeCodigoForHallazgoIfc(codigo);
        }

        public static bool ActualizarIfcServices(AbetEntities ctx, IFC actualIfc, IFC ifc)
        {
            return HallazgoDaoImpl.ActualizarIfc(ctx, actualIfc, ifc);

        }

        public static bool EliminarHallazgoIfcServices(AbetEntities ctx, int idHallazgo)
        {
            return IFCHallazgoDaoImpl.EliminarHallazgoIfc(ctx, idHallazgo);
        }

        public static bool EliminarHallazgoZIfcServices(AbetEntities ctx, int idHallazgo)
        {
            return IFCHallazgoDaoImpl.EliminarHallazgoZIfc(ctx, idHallazgo);                                                                 // RML001
        }

        public static IFCHallazgo GetIfcHallazgoServices(AbetEntities ctx, int idIfc, int idHallazgo)
        {
            return IFCHallazgoDaoImpl.GetIfcHallazgo(ctx, idIfc, idHallazgo);
        }

        public static bool ActualizarHallazgoifcServices(AbetEntities ctx, int idHallazgo, string codigoNivelAceptacion,
           string descripcion)
        {
            return IFCHallazgoDaoImpl.ActualizarHallazgoifc(ctx, idHallazgo, codigoNivelAceptacion, descripcion);
        }

        public static bool ActualizarHallazgoZifcServices(AbetEntities ctx, int idHallazgo, Int32? IdNivelAceptacionHallazgo,    
          string descripcion)                                                                                                             // RML001
        {
            return IFCHallazgoDaoImpl.ActualizarHallazgoZifc(ctx, idHallazgo, IdNivelAceptacionHallazgo, descripcion);
        }

        public static List<Tuple<string, string>> GetAllNivelesAceptacionForHallazgosIfcServices()
        {
            return IFCHallazgoDaoImpl.GetAllNivelesAceptacionForHallazgosIfc();
        }

        public static string GetTextoForCodigoNivelAceptacionHallazgoIfcServices(string codigo)
        {
            return IFCHallazgoDaoImpl.GetTextoForCodigoNivelAceptacionHallazgoIfc(codigo);
        }

        public static string GetTextoForIdNivelAceptacionHallazgoIfcServices(int? IdNivelAceptacionHallazgo)                            // RML001
        {
            return IFCHallazgoDaoImpl.GetTextoForIdNivelAceptacionHallazgoIfc(IdNivelAceptacionHallazgo);
        }

        #endregion

        #region Instrumento
        public static Instrumento GetIfcInstrumentoServices(AbetEntities ctx)
        {
            return InstrumentoDaoImpl.GetIfcInstrumento(ctx);
        }

        #endregion

        #region MallaCocos
        public static List<MallaCocos> GetMallaCocosFromComisionServices(AbetEntities ctx, string codigoComision)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromComision(ctx, codigoComision);
        }

        public static List<MallaCocos> GetMallaCocosFromCarreraServices(AbetEntities ctx, int idCarrera)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromCarrera(ctx, idCarrera);
        }

        public static List<MallaCocos> GetMallaCocosFromCarreraComisionServices(AbetEntities ctx, string codigoComision, int idCarrera)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromCarreraComision(ctx, codigoComision, idCarrera);
        }

        public static List<MallaCocos> GetMallaCocosFromCarreraPeriodoAcademicoServices(AbetEntities ctx, int idCarrera, int idPeriodoAcademico)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromCarreraPeriodoAcademico(ctx, idCarrera, idPeriodoAcademico);
        }

        public static List<MallaCocos> GetMallaCocosFromCarreraComisionPeriodoAcademicoServices(AbetEntities ctx, string codigoComision, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromCarreraComisionPeriodoAcademico(ctx, codigoComision, idCarrera, idSubModalidadPeriodoAcademico);
        }

        public static List<MallaCocos> GetAllMallaCocosServices(AbetEntities ctx)
        {
            return MallaCocosDaoImpl.GetAllMallaCocos(ctx);
        }

        public static bool DeleteMallaCocosServices(AbetEntities ctx, int idMallaCocos)
        {
            return MallaCocosDaoImpl.DeleteMallaCocos(ctx, idMallaCocos);
        }

        public static List<MallaCocos> GetMallaCocosFromPeriodoAcademicoServices(AbetEntities ctx, int idPeriodoAcademico)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromPeriodoAcademico(ctx, idPeriodoAcademico);
        }

        public static MallaCocos GetMallaCocosByIdServices(AbetEntities ctx, int idMallaCocos)
        {
            return MallaCocosDaoImpl.GetMallaCocosById(ctx, idMallaCocos);
        }

        public static List<MallaCocos> GetMallaCocosServices(CargarDatosContext dataContext, int idEscuela, string codigoComision, Int32? idCarrera, Int32? idSubModalidadPeriodoAcademico, Int32? IdAcreditadora)
        {
            //RECIBE SUBMODALIDAD
            return MallaCocosDaoImpl.GetMallaCocos(dataContext, idEscuela, codigoComision, idCarrera, idSubModalidadPeriodoAcademico, IdAcreditadora);
        }

        public static List<MallaCocos> GetMallaCocosFromComisionPeriodoAcademicoServices(AbetEntities ctx, string codigoComision, int idPeriodoAcademico)
        {
            return MallaCocosDaoImpl.GetMallaCocosFromComisionPeriodoAcademico(ctx, codigoComision, idPeriodoAcademico);
        }

        public static int CrearMallaCocosServices(AbetEntities ctx, MallaCurricular mallaCurricular, string detalle, int idSubModalidadPeriodoAcademico, int idComision, int idCarrera)
        {
            return MallaCocosDaoImpl.CrearMallaCocos(ctx, mallaCurricular, detalle, idSubModalidadPeriodoAcademico, idComision, idCarrera);
        }

        public static bool ActualizarMallaCocosServices(AbetEntities ctx, MallaCurricular mallaCurricular, string detalle, int idMallaCocos)
        {
            return MallaCocosDaoImpl.ActualizarMallaCocos(ctx, mallaCurricular, detalle, idMallaCocos);
        }
        #endregion

        #region MallaCocosDetalle

        public static List<MallaCocosDetalle> GetMallaCocosDetallesInMallaCocosServices(AbetEntities ctx, int idMallaCocos)
        {
            return MallaCocosDetalleDaoImpl.GetMallaCocosDetallesInMallaCocos(ctx, idMallaCocos);
        }
                

        public static MallaCocosDetalle GetMallaCocosDetalleServices(AbetEntities ctx, int idCursoMallaCurricular, int idOutcomeComision)
        {
            return MallaCocosDetalleDaoImpl.GetMallaCocosDetalle(ctx, idCursoMallaCurricular, idOutcomeComision);
        }



        #endregion

        #region MallaCurricular

        public static MallaCurricular GetMallaCurricularServices(AbetEntities ctx, int idCarrera, int idSubModalidadPeriodoAcademico)
        {
            return MallaCurricularDaoImpl.GetMallaCurricular(ctx, idCarrera, idSubModalidadPeriodoAcademico);
        }
        #endregion

        #region OutcomeComision

        public static OutcomeComision GetOutcomeComisionInOutcomeComisionServices(AbetEntities ctx, int idOutcome, int idComision)
        {
            return OutcomeComisionDaoImpl.GetOutcomeComisionInOutcomeComision(ctx, idOutcome, idComision);
        }

        public static List<OutcomeComision> GetOutcomeComisionesInComisionInPeriodoAcademicoServices(AbetEntities ctx, Int32? idComision, Int32? IdPeriodoAcademico)
        {
            return OutcomeComisionDaoImpl.GetOutcomeComisionesInComisionInPeriodoAcademico(ctx, idComision, IdPeriodoAcademico);
        }
        #endregion

        #region Outcome

        public static List<Outcome> GetOutcomesInComisionPeriodoAcademicoServices(AbetEntities ctx, int idComision, int idSubModalidadPeriodoAcademico)
        {
            return OutcomeDaoImpl.GetOutcomesInComisionPeriodoAcademico(ctx, idComision, idSubModalidadPeriodoAcademico);
        }

        #endregion

        #region PeriodoAcademico

        public static List<PeriodoAcademico> GetPeriodoAcademicosForIfc(AbetEntities ctx, int idDocente, int idEscuela)
        {
            return PeriodoAcademicoDaoImpl.GetPeriodoAcademicosForIfc(ctx, idDocente, idEscuela);
        }

        public static PeriodoAcademico GetPeriodoAcademicoServices(AbetEntities ctx, int idSubModalidadPeriodoAcademico)
        {
            //RECIBE SUBMODALIDAD OK
            return PeriodoAcademicoDaoImpl.GetPeriodoAcademico(ctx, idSubModalidadPeriodoAcademico);
        }

        public static List<PeriodoAcademico> GetAllPeriodoAcademicosServices(AbetEntities ctx)
        {
            return PeriodoAcademicoDaoImpl.GetAllPeriodoAcademicos(ctx);
        }
        

        //AGREGADO STEFANO
        public static List<PeriodoAcademico> GetAllMyPeriodosAcademicosServices(AbetEntities ctx, int idModalidad)
        {
            return PeriodoAcademicoDaoImpl.GetAllMyPeriodosAcademicos(ctx, idModalidad);
        }
        //********************


        public static List<Modalidad> GetAllModalidadServices(AbetEntities ctx)
        {
            return PeriodoAcademicoDaoImpl.GetAllModalidades(ctx);
        }

        public static List<PeriodoAcademico> GetPeriodoAcademicosInComisionCarreraServices(AbetEntities ctx, string codigoComision, int idCarrera)
        {
            return PeriodoAcademicoDaoImpl.GetPeriodoAcademicosInComisionCarrera(ctx, codigoComision, idCarrera);
        }

        public static PeriodoAcademico GetCurrentPeriodoAcademicoServices(AbetEntities ctx)
        {
            return PeriodoAcademicoDaoImpl.GetCurrentPeriodoAcademico(ctx);
        }


        public static PeriodoAcademico GetPreviosPeriodoAcademicoServices(AbetEntities ctx, int idPeriodoAcademico)
        {
            return PeriodoAcademicoDaoImpl.GetPreviosPeriodoAcademico(ctx, idPeriodoAcademico);
        }

        public static PeriodoAcademico GetCurrentPeriodoAcademicoCicloByDateServices(AbetEntities ctx, DateTime fecha)
        {
            return PeriodoAcademicoDaoImpl.GetCurrentPeriodoAcademicoCicloByDate(ctx, fecha);
        }

        public static List<DateTime> GetDateStartWeekCurrentPeriodoAcademicoBySemanaServices(AbetEntities ctx, HttpSessionStateBase Session, int semana)
        {
            return PeriodoAcademicoDaoImpl.GetDateStartWeekCurrentPeriodoAcademicoBySemana(ctx, Session, semana);
        }

        #endregion

        #region Sede

        public static List<Sede> GetAllSedesServices(AbetEntities ctx)
        {
            return SedeDaoImpl.GetAllSedes(ctx);
        }
        public static Sede GetSedeFromCodigoHallazgoIfcServices(AbetEntities ctx, string codigo)
        {
            return SedeDaoImpl.GetSedeFromCodigoHallazgoIfc(ctx, codigo);
        }

        public static List<Sede> GetSedesForIfcServices(AbetEntities ctx, int idDocente, int idCursoUnidadAcademica, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
            return SedeDaoImpl.GetSedesForIfc(ctx, idDocente, idCursoUnidadAcademica, idSubModalidadPeriodoAcademico, idEscuela);
        }

        public static List<Sede> GetAllSedesByPeriodoServices(AbetEntities ctx, int idperiodo)
        {
            return SedeDaoImpl.GetAllSedesByPeriodo(ctx, idperiodo);
        }



        #endregion

        #region TipoOutcomeCurso

        public static List<TipoOutcomeCurso> GetAllTipoOutcomeCursosServices(AbetEntities ctx)
        {
            return TipoOutcomeCursoDaoImpl.GetAllTipoOutcomeCursos(ctx);
        }
        
        #endregion

        #region UnidadAcademica

        public static List<UnidadAcademica> GetAllCarrerasInPeriodoAcademico(AbetEntities ctx, int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllCarrerasInPeriodoAcademico(ctx, idPeriodoAcademico, idEscuela);
        }

        public static List<UnidadAcademica> GetAllSUbareasInAreaServices(AbetEntities ctx, string idAreaUnidadAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllSUbareasInArea(ctx, idAreaUnidadAcademica, idEscuela);
        }
        public static List<UnidadAcademica> GetAllSUbareasInAreaServicesInt(AbetEntities ctx, int idAreaUnidadAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllSUbareasInAreaInt(ctx, idAreaUnidadAcademica, idEscuela);
        }
        public static List<UnidadAcademica> GetAllCursosInAreaServices(AbetEntities ctx, string idAreaUnidadAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllCursosInArea(ctx, idAreaUnidadAcademica, idEscuela);
        }

        public static List<UnidadAcademica> GetAllCursosInSubareaServices(AbetEntities ctx, string idSubareaAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllCursosInSubarea(ctx, idSubareaAcademica, idEscuela);
        }

        public static UnidadAcademica GetUnidadAcademicaServices(AbetEntities ctx, int idUnidadAcademica)
        {
            return UnidadAcademicaDaoImpl.GetUnidadAcademica(ctx, idUnidadAcademica);
        }

        public static UnidadAcademica GetAreaForCursoServices(AbetEntities ctx, int idUnidadAcademica)
        {
            return UnidadAcademicaDaoImpl.GetAreaForCurso(ctx, idUnidadAcademica);
        }

        public static List<UnidadAcademica> GetAreasForIfcServices(AbetEntities ctx, int idDocente, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAreasForIfc(ctx, idDocente, idSubModalidadPeriodoAcademico, idEscuela);
        }

        public static List<UnidadAcademica> GetSubareasForIfcServices(AbetEntities ctx, int idDocente, int idSubModalidadPeriodoAcademico, int idAreaUnidadAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetSubareasForIfc(ctx, idDocente, idSubModalidadPeriodoAcademico, idAreaUnidadAcademica, idEscuela);
        }

        public static List<UnidadAcademica> GetUnidadAcademicaNivelServices(AbetEntities ctx, Int32 IdDocente, Int32 Nivel, Int32 IdPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetUnidadAcademicaNivel(ctx, IdDocente, Nivel, IdPeriodoAcademico, idEscuela);
        }

        public static List<UnidadAcademica> GetCursosUnidadAcademicaForIfcServices(AbetEntities ctx, int idDocente,
            int idPeriodoAcademico, int idSubareaUnidadAcademica, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetCursosUnidadAcademicaForIfc(ctx, idDocente, idPeriodoAcademico, idSubareaUnidadAcademica, idEscuela);
        }

        public static List<UnidadAcademica> GetAllCursosServices(AbetEntities ctx, int idEscuela, int parModalidadId)
        {
            return UnidadAcademicaDaoImpl.GetAllCursos(ctx, idEscuela,parModalidadId);
        }

        public static List<UnidadAcademica> GetAllAreasServices(AbetEntities ctx, int idEscuela, int parModalidadId)
        {
            return UnidadAcademicaDaoImpl.GetAllAreas(ctx, idEscuela,parModalidadId);
        }
        public static List<UnidadAcademica> GetAllAreasByCarrera(AbetEntities ctx, int idEscuela,int idCarrera, int? idSubModalidadPeriodoAcademico)
        {
            return UnidadAcademicaDaoImpl.GetAllAreasByCarrera(ctx, idEscuela, idCarrera, idSubModalidadPeriodoAcademico);
        }
        //Este es el que he utilizado en create (STEFANO)
        public static List<UnidadAcademica> GetAllAreasForPeriodoAcademico(AbetEntities ctx, int idSubModalidadPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllAreasInPeriodoAcademico(ctx, idSubModalidadPeriodoAcademico, idEscuela);
        }
        //*************************************************

        public static List<UnidadAcademica> GetAreasByPeriodoAcademico(AbetEntities ctx, int id, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAreasByPeriodoAcademico(ctx, id, idEscuela);
        }
        public static List<UnidadAcademica> GetAllUnidadesByUsuarioAndNivelServices(AbetEntities ctx, HttpSessionStateBase Session, int nivel)
        {
            return UnidadAcademicaDaoImpl.GetAllUnidadesByUsuarioAndNivel(ctx, Session, nivel);
        }

        public static List<UnidadAcademica> GetAllEscuelasServices(AbetEntities ctx, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllEscuelas(ctx, idEscuela);
        }

        public static List<AccionMejora> GetAccionesMejoraForHallazgoIfcServices(AbetEntities ctx, int idHallazgo)
        {
            return AccionMejoraDaoImpl.GetAccionesMejoraForHallazgoIfc(ctx, idHallazgo);
        }

        public static List<UnidadAcademica> GetAllSubareasServices(AbetEntities ctx, int idEscuela, int parModalidadId)
        {
            return UnidadAcademicaDaoImpl.GetAllSubareas(ctx, idEscuela,parModalidadId);
        }

        public static List<UnidadAcademica> GetSubareasForIfcServices(AbetEntities ctx, int idDocente, int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetSubareasForIfc(ctx, idDocente, idPeriodoAcademico, idEscuela);
        }


        public static UnidadAcademica GetUnidadAcademicaForIfcServices(AbetEntities ctx, int idDocente, int idCurso, int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetUnidadAcademicaForIfc(ctx, idDocente, idCurso, idPeriodoAcademico, idEscuela);
        }

        public static List<int> GetAllNivelesUnidadesAcademicasServices(AbetEntities ctx, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllNivelesUnidadesAcademicas(ctx, idEscuela);
        }

        public static List<UnidadAcademica> GetAllUnidadesAcademicasServices(AbetEntities ctx, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetAllUnidadesAcademicas(ctx, idEscuela);
        }

        public static List<UnidadAcademica> GetUnidadesAcademicasDocenteServices(AbetEntities ctx, int idDocente,int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetUnidadesAcademicasDocente(ctx, idDocente,idPeriodoAcademico, idEscuela);
        }

        public static UnidadAcademica GetUnidadAcademicaForMeetingServices(AbetEntities ctx, int idDocente, int idNivel, int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.GetUnidadAcademicaForMeeting(ctx, idDocente, idNivel, idPeriodoAcademico, idEscuela);
        }

        public static bool DocentePerteneceOrganigrama(AbetEntities ctx, int? idDocente, int idPeriodoAcademico, int idEscuela)
        {
            return UnidadAcademicaDaoImpl.DocentePerteneceOrganigrama(ctx, idDocente, idPeriodoAcademico, idEscuela);
        }

        #endregion

        #region ParticipanteReunion
        public static ParticipanteReunion GetParticipanteServices(AbetEntities ctx, int idDocente, int idReunion)
        {
            return ParticipanteReunionDaoImpl.GetParticipante(ctx, idDocente, idReunion);
        }

        public static ParticipanteReunion GetParticipanteExternoServices(AbetEntities ctx, int idParticipanteReunion)
        {
            return ParticipanteReunionDaoImpl.GetParticipanteExterno(ctx, idParticipanteReunion);
        }

        public static List<ParticipanteReunion> GetParticipantesByReunion(AbetEntities ctx, int idReunion)
        {
            return ParticipanteReunionDaoImpl.GetParticipantesByReunion(ctx, idReunion);
        }

        public static ParticipanteReunion CrearParticipanteExternoServices(AbetEntities ctx, ParticipanteReunion participante)
        {
            return ParticipanteReunionDaoImpl.CrearParticipanteExterno(ctx, participante);
        }

        public static bool ElminarParticipanteExternoServices(AbetEntities ctx, int idParticipante)
        {
            return ParticipanteReunionDaoImpl.ElminarParticipanteExterno(ctx, idParticipante);
        }
        public static ParticipanteReunion CrearParticipanteSiExisteServices(AbetEntities ctx, int idDocente, int idReunion)
        {
            return ParticipanteReunionDaoImpl.CrearParticipanteSiExiste(ctx, idDocente, idReunion);
        }
        public static bool ElminarParticipanteSiExisteServices(AbetEntities ctx, int idDocente, int idReunion)
        {
            return ParticipanteReunionDaoImpl.ElminarParticipanteSiExiste(ctx, idDocente, idReunion);
        }
        #endregion

        #region TemaReunion
        public static TemaReunion CrearTemaDeReunionService(AbetEntities ctx, string tema, int idReunion)
        {
            return ReunionDaoImpl.CrearTemaDeReunion(ctx, tema, idReunion);
        }
        public static List<TemaReunion> GetAllTemasDeReunionService(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.GetAllTemasDeReunion(ctx, idReunion);
        }

        public static bool EliminarTemasDeReunionService(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.EliminarTemasDeReunion(ctx, idReunion);
        }
        #endregion

        #region PreAgenda
        public static List<PreAgenda> GetAllPreAgendasServices(AbetEntities ctx)
        {
            return PreAgendaDaoImpl.GetAllPreAgendas(ctx);
        }

        public static List<PreAgenda> GetPreAgendasByNivelServices(AbetEntities context, int nivel)
        {
            return PreAgendaDaoImpl.GetPreAgendasByNivel(context, nivel);
        }

        public static List<PreAgenda> GetPreAgendasByNivelAndTipoServices(AbetEntities context, String tipo, int nivel)
        {
            return PreAgendaDaoImpl.GetPreAgendasByNivelAndTipo(context, tipo, nivel);
        }

        public static PreAgenda GetPreAgendaServices(AbetEntities ctx, int id)
        {
            return PreAgendaDaoImpl.GetPreAgenda(ctx, id);
        }

        public static PreAgenda CrearPreAgendaServices(AbetEntities ctx, HttpSessionStateBase Session, PreAgenda preAgenda, string tipoPreAgenda)
        {
            return PreAgendaDaoImpl.CrearPreAgenda(ctx, Session, preAgenda, tipoPreAgenda);
        }

        public static bool ActualizarPreAgendaServices(AbetEntities ctx, HttpSessionStateBase Session, PreAgenda preAgenda, string tipoPreAgenda)
        {
            return PreAgendaDaoImpl.ActualizarPreAgenda(ctx, Session, preAgenda, tipoPreAgenda);
        }

        public static bool ActualizarPreAgendaServices(AbetEntities ctx, HttpSessionStateBase Session, int idPreAgenda, string motivo, int idSede)
        {
            return PreAgendaDaoImpl.ActualizarPreAgenda(ctx, Session, idPreAgenda, motivo, idSede);
        }

        public static bool EliminarPreAgendaServices(AbetEntities ctx, int idPreAgenda)
        {
            return PreAgendaDaoImpl.EliminarPreAgenda(ctx, idPreAgenda);
        }

        public static bool EliminarPreAgendasExtraordinariasNoUtilizadasServices(AbetEntities ctx)
        {
            return PreAgendaDaoImpl.EliminarPreAgendasExtraordinariasNoUtilizadas(ctx);
        }

        public static TemaPreAgenda GetTemaPreAgendaServices(AbetEntities context, int idTemaPreAgenda)
        {
            return PreAgendaDaoImpl.GetTemaPreAgenda(context, idTemaPreAgenda);
        }

        public static List<TemaPreAgenda> GetAllTemasPreAgendaServices(AbetEntities context, int idPreAgenda)
        {
            return PreAgendaDaoImpl.GetAllTemasPreAgenda(context, idPreAgenda);
        }

        public static int CrearTemaPreAgendaServices(AbetEntities context, TemaPreAgenda temaPreAgenda)
        {
            return PreAgendaDaoImpl.CrearTemaPreAgenda(context, temaPreAgenda);
        }

        public static bool EliminarTemaPreAgendaServices(AbetEntities context, int idTemaPreAgenda)
        {
            return PreAgendaDaoImpl.EliminarTemaPreAgenda(context, idTemaPreAgenda);
        }

        public static List<PreAgenda> GetPreAgendasBusquedaServices(AbetEntities ctx, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, int? idArea, string tipoPreAgenda)
        {
            return PreAgendaDaoImpl.GetPreAgendasBusqueda(ctx, idNivel, idSemana, FechaInicio, FechaFin, idSede, idArea, tipoPreAgenda);
        }

        //public static List<PreAgenda> GetPreAgendasBusquedaByUsuarioServices(AbetEntities ctx, HttpSessionStateBase Session, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede, string tipoPreAgenda, int? idArea)
        //{
        //    return PreAgendaDaoImpl.GetPreAgendasBusquedaByUsuario(ctx, Session, idNivel, idSemana, FechaInicio, FechaFin, idSede, tipoPreAgenda, idArea);
        //}

        public static List<int> GetSemanasDisponiblesServices(AbetEntities ctx)
        {
            return PreAgendaDaoImpl.GetSemanasDisponibles(ctx);
        }
        #endregion

        #region Reunion
        public static List<Reunion> GetAllReunionesServices(AbetEntities ctx)
        {
            return ReunionDaoImpl.GetAllReuniones(ctx);
        }

        public static Reunion GetReunionServices(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.GetReunion(ctx, idReunion);
        }

        public static Reunion GetReunionByPreAgendaServices(AbetEntities ctx, int idPreAgenda)
        {
            return ReunionDaoImpl.GetReunionByPreAgenda(ctx, idPreAgenda);
        }

        public static Int32 CrearTemaDetalleReunionServices(AbetEntities ctx, List<TemaReunion> detalleReunion)
        {
            return ReunionDaoImpl.CrearTemaEnDetalleReunion(ctx, detalleReunion);
        }

        public static List<TemaReunion> GetAllTemasReunionServices(AbetEntities context, int idReunion)
        {
            return ReunionDaoImpl.GetAllTemasReunion(context, idReunion);
        }

        public static List<Reunion> GetReunionesBusquedaServices(AbetEntities ctx, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede)
        {
            return ReunionDaoImpl.GetReunionesBusqueda(ctx, idNivel, idSemana, FechaInicio, FechaFin, idSede);
        }

        public static List<Reunion> GetReunionesBusquedaByUsuarioServices(AbetEntities ctx, HttpSessionStateBase Session, int? idNivel, int? idSemana, DateTime? FechaInicio, DateTime? FechaFin, int? idSede)
        {
            return ReunionDaoImpl.GetReunionesBusquedaByUsuario(ctx, Session, idNivel, idSemana, FechaInicio, FechaFin, idSede);
        }

        public static Reunion CrearReunionServices(AbetEntities ctx, Reunion reunion)
        {
            return ReunionDaoImpl.CrearReunion(ctx, reunion);
        }

        public static bool EliminarReunionServices(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.EliminarReunion(ctx, idReunion);
        }

        public static Reunion ActualizarReunionServices(AbetEntities ctx, Reunion reunion)
        {
            return ReunionDaoImpl.ActualizarReunion(ctx, reunion);
        }

        public static List<ParticipanteReunion> GetAllParticipantesReunionServices(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.GetAllParticipantesReunion(ctx, idReunion);
        }

        public static bool EliminarParticipantesReunionServices(AbetEntities ctx, int idReunion)
        {
            return ReunionDaoImpl.EliminarParticipantesReunion(ctx, idReunion);
        }
        #endregion

        #region TemaPreAgenda
        public static List<Tuple<string, string>> GetAllEstadosTemaPreAgendaServices(AbetEntities ctx)
        {
            return TemaPreAgendaDaoImpl.GetAllEstadosTemaPreAgenda(ctx);
        }
        #endregion

        #region Tarea
        public static List<Tuple<string, string>> GetAllEstadosTareaServices(AbetEntities ctx)
        {
            return TareaDaoImpl.GetAllEstadosTarea(ctx);
        }
        #endregion
    }

}
