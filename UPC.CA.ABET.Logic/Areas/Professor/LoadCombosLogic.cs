﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;

namespace UPC.CA.ABET.Logic.Areas.Professor
{
    public class LoadCombosLogic
    {
        private readonly AbetEntities _context;
        public LoadCombosLogic(AbetEntities Context)
        {
            this._context = Context;
        }
        public IEnumerable<ComboItem> ListModalities(string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboModalidad {0}", Language).AsEnumerable();
        }
    }
}
