﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Models;
using System.Globalization;

namespace UPC.CA.ABET.Logic.Areas.Professor.MeetingMinuteCC.Export
{
    public class ExportMeetingMinuteCC
    {
        private Acta _acta;
        private string _logoUPC;
        private AbetEntities ctx;
        private string _rutaFoto;
        private string _tipoActa;

        public ExportMeetingMinuteCC(AbetEntities ctx, int idActa, string pathLogoUPC, string pathRutaFotoReunion)
        {
            _logoUPC = pathLogoUPC;
            _rutaFoto = pathRutaFotoReunion;
            this.ctx = ctx;
            _acta = ctx.Acta.First(x => x.IdActa == idActa);
            _tipoActa = _acta.TipoActa;
        }

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if (_acta == null)
            {
                return String.Empty;
            }
            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;
            }
        }


       private string GetPdf(string directory)
        {
            string path = "";
            try
            {
                Document document = CreateDocument();
                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                renderer.Document = document;

                renderer.RenderDocument();

                var motivo = _acta.Reunion.PreAgenda.Motivo;
                var tipoActa = _acta.TipoActa;
                var periodoAcademico = _acta.SubModalidadPeriodoAcademico.PeriodoAcademico;
                var subModalidad = _acta.SubModalidadPeriodoAcademico.SubModalidad;
                // var nivel = _acta.Reunion.PreAgenda.Nivel;

                string filename;
                filename = "ACTA_" + tipoActa + periodoAcademico.CicloAcademico.Replace(" ", "")
                    + "_" + subModalidad.NombreEspanol.Replace(" ","") + "_" + motivo.Replace(" ", "_") + ".pdf";

                filename = filename.DeleteSlashAndBackslash();
                path = Path.Combine(directory, filename);
                renderer.PdfDocument.Save(path);
            }
            catch (Exception e)
            {
                
            }

            

            return path;
        }

        private string GetRtf(string directory)
        {
            Document document = CreateDocument();

            var motivo = _acta.Reunion.PreAgenda.Motivo;
            var tipoActa = _acta.TipoActa;
            var periodoAcademico = _acta.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _acta.SubModalidadPeriodoAcademico.SubModalidad;
            var nivel = _acta.Reunion.PreAgenda.Nivel;

            string filename;
            filename = "ACTA_" + tipoActa + "_Nivel_" + nivel + "_" + periodoAcademico.CicloAcademico.Replace(" ", "")
                + "_" + subModalidad.NombreEspanol.Replace(" ","") + "_" + motivo.Replace(" ", "_") + ".rtf";

            filename = filename.DeleteSlashAndBackslash();
            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);
            string path = Path.Combine(directory, filename);

            return path;

        }

        private Document CreateDocument()
        {
            Document document = new Document();
            SetStyles(document);
            SetContentSection(document);
            SetTituloUniversidad(document);
            setTituloPrograma(document);
            SetTituloReunion(document);
            SetTitleObjetivo(document);
            SetObjetivo(document);
            setComiteConsultivo(document);
            SetOrganizadores(document);
            SetFooterFecha(document);
            setTitleAgenda(document);
            setAgenda(document);
            SetFoto(document);
            setDescripcionFoto(document);
            setTitleDetalle(document);
            setDetalle(document);
            setTituloOpinionesSugerencias(document);
            setOpinionesSugerencias(document);
            return document;
        }

        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }

        private void SetTituloUniversidad(Document document)
        {
            Section section = document.LastSection;
            Image image = section.AddImage(_logoUPC);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;
            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");
            paragraph.Format.SpaceAfter = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

        }

        public void setTituloPrograma(Document document)
        {
            Section section = document.LastSection;
            String texto = _acta.Reunion.UnidadAcademica.NombreEspanol;
            Paragraph paragraph = section.AddParagraph("PROGRAMA DE " + texto , "Heading2");
            paragraph.Format.SpaceAfter = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.Black;

        }


        private void SetTituloReunion(Document document)
        {
            Section section = document.LastSection;
            String fecha = _acta.FechaRegistro.Year.ToString();
            Paragraph paragraph = section.AddParagraph("REUNIÓN CON EL COMITÉ CONSULTIVO " + fecha);
            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.SpaceAfter = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.Black;
        }

        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;
            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();
            section.Footers.Primary.Add(paragraph);
        }

        public void SetTitleObjetivo(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 1.  OBJETIVO ", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
        }

        public void SetObjetivo(Document document)
        {
            Paragraph paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            String texto = Regex.Replace(_acta.Reunion.ObjetivoComiteConsultivo, "<br>", "\n\r");
            texto = Regex.Replace(texto, "<.*?>", String.Empty);
            texto = Regex.Replace(texto, "&nbsp;", String.Empty);
            paragraph.AddText(texto);
        }

        public bool existComiteConsultivo(List<ParticipanteReunion> participantes)
        {
            if (participantes != null)
            {
                foreach (var participante in participantes)
                {
                    if (participante.IdDocente == null)
                        return true;
                }
            }
            return false;
       }

        public bool existOrganizadores(List<ParticipanteReunion> participantes)
        {
            if (participantes != null)
            {
                foreach (var participante in participantes)
                {
                    if (participante != null && participante.IdDocente == null)
                        return true;
                }
                return false;
            }
            return false;
        }

        public void setComiteConsultivo(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 2.  COMITÉ  CONSULTIVO ", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            var participantes = _acta.Reunion.ParticipanteReunion.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 250;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 210;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nombres y Apellidos");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Cargo");
            paragraph.Format.Font.Color = Colors.White;

            if (!existComiteConsultivo(participantes))
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph("No existen participantes de Comité Consultivo en esta reunión");
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(" ");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            else {
                foreach (var participante in participantes)
                {
                    if (participante != null)
                    {
                        if (participante.IdDocente == null)
                        {
                            row = table.AddRow();
                            row.VerticalAlignment = VerticalAlignment.Center;
                            row.HeadingFormat = false;
                            row.Shading.Color = Colors.White;
                            row.Format.Font.Bold = false;
                            paragraph = row[0].AddParagraph(participante.NombreCompleto);
                            paragraph.Format.Font.Color = Colors.Black;
                            paragraph = row[1].AddParagraph(participante.Cargo);
                            paragraph.Format.Font.Color = Colors.Black;
                        }
                    }
                }
            }
        }

        public void SetOrganizadores(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 3.  ORGANIZADORES UPC ", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
        
            var participantes = _acta.Reunion.ParticipanteReunion.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 250;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 210;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nombres y Apellidos");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Cargo");
            paragraph.Format.Font.Color = Colors.White;

            if (!existOrganizadores(participantes))
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph("No existen participantes organizadores en esta reunión");
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(" ");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            else {
                 foreach (var participante in participantes)
                 {
                    if (participante != null)
                    {
                        if (participante.IdDocente != null)
                        {
                            row = table.AddRow();
                            row.VerticalAlignment = VerticalAlignment.Center;
                            row.HeadingFormat = false;
                            row.Shading.Color = Colors.White;
                            row.Format.Font.Bold = false;
                            paragraph = row[0].AddParagraph(participante.NombreCompleto);
                            paragraph.Format.Font.Color = Colors.Black;
                            if (participante.Cargo != null)
                            {
                                paragraph = row[1].AddParagraph(participante.Cargo);
                                paragraph.Format.Font.Color = Colors.Black;
                            }
                            else
                            {
                                paragraph = row[1].AddParagraph("No tiene Cargo");
                                paragraph.Format.Font.Color = Colors.Black;
                            }
                            

                        }
                    }   
                }
            }
        }


        public void SetFooterFecha(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.DifferentFirstPageHeaderFooter = true;
            Paragraph paragraph = section.Footers.FirstPage.AddParagraph();
            String fecha = DateTime.Today.Date.ToString();
            //String dia = DateTime.Today.Day.ToString();
            //String anio = DateTime.Today.Year.ToString();
            String dia = _acta.FechaRegistro.Day.ToString();
            String anio = _acta.FechaRegistro.Year.ToString();
            int mesInt = _acta.FechaRegistro.Month;
            String mes = new CultureInfo("es-PE").DateTimeFormat.MonthNames[mesInt - 1].ToString();
            paragraph.AddText("Surco , " + dia + " de " + mes + " de " + anio);
       }

        public void setTitleAgenda(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 4. AGENDA ", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
        }

        public void setAgenda(Document document)
        {
            Paragraph paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //String texto = _acta.Reunion.AgendaComiteConsultivo;
            
            String texto = Regex.Replace(_acta.Reunion.AgendaComiteConsultivo, "<br>", "\n\r");
            texto = Regex.Replace(texto, "<.*?>", String.Empty);
            texto = Regex.Replace(texto, "&nbsp;", String.Empty);
            paragraph.AddText(texto);
        }

        public void SetFoto(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 5.  Foto de la Reunión", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            Image image = section.AddImage(_rutaFoto);
            image.Left = ShapePosition.Center;
            image.Width = 350;
            image.LockAspectRatio = true;
        }

        public void setDescripcionFoto(Document document)
        {
            Paragraph paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.Format.SpaceBefore = "8mm";
            String texto = Regex.Replace(_acta.DescripcionFoto, "<br>", "\n\r");
            texto = Regex.Replace(texto, "<.*?>", String.Empty);
            texto = Regex.Replace(texto, "&nbsp;", String.Empty);
            paragraph.AddText(texto);
        }
         
        public void setTitleDetalle(Document document)
        {
            Section section = document.LastSection;
            String anio = DateTime.Today.Year.ToString();
            Paragraph paragraph = section.AddParagraph(" Sección 6.  DETALLE DE LA REUNIÓN " + anio , "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
        }

        public void setDetalle(Document document)
        {
            Paragraph paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            String texto = Regex.Replace(_acta.DetalleReunionComiteConsultivo, "<br>", "\n\r");
            texto = Regex.Replace(texto, "<.*?>", String.Empty);
            texto = Regex.Replace(texto,"&nbsp;",String.Empty);
            paragraph.AddText(texto);
        }
     
        public void setTituloOpinionesSugerencias(Document document)
        {
            Section section = document.LastSection;
            String anio = DateTime.Today.Year.ToString();
            Paragraph paragraph = section.AddParagraph(" Sección 7.  OPINIONES Y SUGERENCIAS DEL COMITÉ CONSULTIVO  " + anio, "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            //paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
        }

        public void setOpinionesSugerencias(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph();
            String texto = "Luego de la presentación, se procedió con la ronda de opiniones del Comité Consultivo, las cuales se detallan a continuación:";
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            paragraph.AddText(texto);
        
            var participantes = _acta.Reunion.ParticipanteReunion.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 200;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 260;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nombres y Apellidos");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Comentarios");
            paragraph.Format.Font.Color = Colors.White;

            if (!existOrganizadores(participantes))
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph(" No existen participantes de Comite Consultivo registrados en esta reunión");
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(" ");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            else
            {

                foreach (var participante in participantes)
                {
                    if (!participante.IdDocente.HasValue)
                    {
                        if (participante.IdDocente==null)
                        {
                            row = table.AddRow();
                            row.VerticalAlignment = VerticalAlignment.Center;
                            row.HeadingFormat = false;
                            row.Shading.Color = Colors.White;
                            row.Format.Font.Bold = false;
                            paragraph = row[0].AddParagraph(participante.NombreCompleto);
                            paragraph.Format.Font.Color = Colors.Black;
                            if (participante != null && participante.ComentarioParticipanteReunion.Any())
                            {
                                paragraph = row[1].AddParagraph(participante.ComentarioParticipanteReunion.FirstOrDefault().Texto);
                            }
                            else
                            {
                                paragraph = row[1].AddParagraph("No existen comentarios.");
                            }
                            paragraph.Format.Alignment = ParagraphAlignment.Justify;
                            paragraph.Format.Font.Color = Colors.Black;
                        }
                    }
                }
            }
            

        }

        public string replaceHtml(String parrafo)
        {
            string retorno;
            retorno = parrafo.Replace("<p>", " ");
            retorno = parrafo.Replace("</p>"," ");
            retorno = parrafo.Replace("</br>", "");

            return retorno;
        }
    }
}
