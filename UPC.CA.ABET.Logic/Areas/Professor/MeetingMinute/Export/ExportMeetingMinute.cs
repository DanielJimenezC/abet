﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.IO;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;

namespace UPC.CA.ABET.Logic.Areas.Professor.MeetingMinute.Export
{
    public class ExportMeetingMinute
    {
        private Acta _acta;
        private string _logoUPC;
        private AbetEntities ctx;
        private string _rutaFoto;
        private string _tipoActa;

        public ExportMeetingMinute(AbetEntities ctx, int idActa, string pathLogoUPC, string pathRutaFotoReunion)
        {
            _logoUPC = pathLogoUPC;
            _rutaFoto = pathRutaFotoReunion;
            this.ctx = ctx;
            _acta = ctx.Acta.First(x => x.IdActa == idActa);
            _tipoActa = _acta.TipoActa;
        }

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if(_acta == null)
            {
                return String.Empty;
            }
            switch(fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;
            }
        }

        private string GetPdf(string directory)
        {
            Document document = CreateDocument();
            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            var motivo = _acta.Reunion.Motivo;
            var tipoActa = _acta.TipoActa;
            var periodoAcademico = _acta.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _acta.SubModalidadPeriodoAcademico.SubModalidad.Modalidad;
            var nivel = _acta.Reunion.Nivel ?? 0;
            
            string filename;
            filename = "ACTA_" + tipoActa + "_Nivel_" + nivel + "_" + periodoAcademico.CicloAcademico.Replace(" ", "")
                + "_" + subModalidad.NombreEspanol.Replace(" ","")+"_"+ motivo.Replace(" ", "_") + ".pdf";

            filename = filename.DeleteSlashAndBackslash();
            filename = filename.Replace(Environment.NewLine, "");
            string path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);

            return path;
        }
        private string GetRtf(string directory)
        {
            Document document = CreateDocument();
            
            var motivo = _acta.Reunion.Motivo;
            var tipoActa = _acta.TipoActa;
            var periodoAcademico = _acta.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _acta.SubModalidadPeriodoAcademico.SubModalidad.Modalidad;
            var nivel = _acta.Reunion.Nivel ??0;

            string filename;
            filename = "ACTA_" + tipoActa + "_Nivel_" + nivel + "_" + periodoAcademico.CicloAcademico.Replace(" ", "")
                + "_" + subModalidad.NombreEspanol.Replace(" ", "") + "_" + motivo.Replace(" ", "_") + ".rtf";

            filename = filename.DeleteSlashAndBackslash();
            filename = filename.Replace(Environment.NewLine, "");
            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);
            string path = Path.Combine(directory, filename);

            return path;

        }

        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);
            SetContentSection(document);
            SetDetail(document);
            SetInformacionGeneral(document);
            SetAgenda(document);
            SetParticipantes(document);
            SetDetalleReunion(document);
            if (_tipoActa == ConstantHelpers.PROFESSOR.ACTAS.TIPOS.ACTA_NORMAL.CODIGO)
            {
                SetAcuerdos(document);
                SetTareas(document);
            }
            else
            {
                SetAccionesPrevias(document);
                SetHallasgoz(document);

            }
            SetFoto(document);
            //SetFirma(document);
            return document;
        }

        private void SetAccionesPrevias(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 5.  Acciones Previas", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            //var accionesPrevias = _acta.AccionesPrevias;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 380;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Codigo");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Descripcion");
            row[1].Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            int num = 0;
            //foreach (var accionPrevia in accionesPrevias)
            //{
            //    num++;
            //    row = table.AddRow();
            //    row.VerticalAlignment = VerticalAlignment.Center;
            //    row.HeadingFormat = false;
            //    row.Shading.Color = Colors.White;
            //    row.Format.Font.Bold = false;

            //    paragraph = row[0].AddParagraph(accionPrevia.Codigo);
            //    paragraph.Format.Font.Color = Colors.Black;
            //    paragraph = row[1].AddParagraph(accionPrevia.DescripcionEspanol);
            //    paragraph.Format.Font.Color = Colors.Black;
            //}
        }
        private void SetHallasgoz(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 6.  Hallazgos", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var hallazgos = _acta.Hallazgo;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 380;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Codigo");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Descripcion");
            row[1].Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            int num = 0;
            foreach (var hallazgo in hallazgos)
            {
                num++;
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                paragraph = row[0].AddParagraph(hallazgo.Codigo);
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(hallazgo.DescripcionEspanol);
                paragraph.Format.Font.Color = Colors.Black;
            }
        }
        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }

        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }

        private void SetDetail(Document document)
        {
            Section section = document.LastSection;

            Image image = section.AddImage(_logoUPC);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");
            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.SpaceAfter = "15mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;            
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;
            
        }

        private void SetInformacionGeneral(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 1.  Información General", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var reunion = _acta.Reunion;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 85;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 145;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 85;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 145;
            //Primera Fila
            Row row = table.AddRow();            
            row.VerticalAlignment = VerticalAlignment.Center;            
            row.HeadingFormat = false;
            //row.Height = 15;            
            row[0].Shading.Color = row[2].Shading.Color = Colors.Red;
            row[0].Format.Font.Bold = row[2].Format.Font.Bold =true;
            row[1].Shading.Color = row[3].Shading.Color = Colors.White;
            row[1].Format.Font.Bold = row[3].Format.Font.Bold = false;

            paragraph = row[0].AddParagraph("Nivel");            
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(reunion.Nivel.ToString());
            paragraph.Format.Font.Color = Colors.Black;
            paragraph = row[2].AddParagraph("Unidad Académica");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[3].AddParagraph(reunion.UnidadAcademica.NombreEspanol);
            paragraph.Format.Font.Color = Colors.Black;
            //SegundaFila
            row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = false;
            //row.Height = 15;
            row[0].Shading.Color = row[2].Shading.Color = Colors.Red;
            row[0].Format.Font.Bold = row[2].Format.Font.Bold = true;
            row[1].Shading.Color = row[3].Shading.Color = Colors.White;
            row[1].Format.Font.Bold = row[3].Format.Font.Bold = false;

            paragraph = row[0].AddParagraph("Motivo");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(reunion.Motivo);
            paragraph.Format.Font.Color = Colors.Black;
            paragraph = row[2].AddParagraph("Semana");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[3].AddParagraph(reunion.Semana.ToString());
            paragraph.Format.Font.Color = Colors.Black;
            //TerceraFila
            row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = false;
            //row.Height = 15;
            row[0].Shading.Color = row[2].Shading.Color = Colors.Red;
            row[0].Format.Font.Bold = row[2].Format.Font.Bold = true;
            row[1].Shading.Color = row[3].Shading.Color = Colors.White;
            row[1].Format.Font.Bold = row[3].Format.Font.Bold = false;

            paragraph = row[0].AddParagraph("Fecha");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(reunion.Fecha.ToShortDateString());
            paragraph.Format.Font.Color = Colors.Black;
            paragraph = row[2].AddParagraph("Lugar");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[3].AddParagraph(reunion.Lugar);
            paragraph.Format.Font.Color = Colors.Black;
            //CuartaFila
            row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = false;
            //row.Height = 15;
            row[0].Shading.Color = row[2].Shading.Color = Colors.Red;
            row[0].Format.Font.Bold = row[2].Format.Font.Bold = true;
            row[1].Shading.Color = row[3].Shading.Color = Colors.White;
            row[1].Format.Font.Bold = row[3].Format.Font.Bold = false;

            paragraph = row[0].AddParagraph("Hora Inicio");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(reunion.HoraInicio.ToShortTimeString());
            paragraph.Format.Font.Color = Colors.Black;
            paragraph = row[2].AddParagraph("Hora Fin");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[3].AddParagraph(reunion.HoraFin.ToShortTimeString());
            paragraph.Format.Font.Color = Colors.Black;
        }

        private void SetAgenda(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 2.  Agenda", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var agenda = _acta.Reunion.TemaReunion.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 40;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 420;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nro.");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Tema");
            row[1].Format.Alignment = ParagraphAlignment.Center;            
            paragraph.Format.Font.Color = Colors.White;

            if(agenda.Count==0)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph("No hay temas disponibles para este acta de reunión");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            int num = 1;
            foreach (var tema in agenda)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                paragraph = row[0].AddParagraph(num.ToString());
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(tema.DescripcionTemaReunion);
                paragraph.Format.Font.Color = Colors.Black;                
                num++;
            }
        }

        private void SetParticipantes(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 3.  Participantes", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var participantes = _acta.Reunion.ParticipanteReunion.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 380;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nombres y Apellidos");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Asistió");
            paragraph.Format.Font.Color = Colors.White;

            if (participantes.Count == 0)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph("No hay participantes registrados en esta reunión");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            
            foreach (var participante in participantes)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                paragraph = row[0].AddParagraph(participante.NombreCompleto);
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(participante.Asistio?"Sí":"No");
                paragraph.Format.Font.Color = Colors.Black;                                
            }
        }

        private void SetDetalleReunion(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 4.  Detalle de lo tratado", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var reunion = _acta.Reunion;

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 40;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 40;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 380;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nro.");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Nro Agenda");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph("Descripción de lo tratado");
            row[1].Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;

            int cant = ctx.TemaReunion.Where(x => x.IdReunion == reunion.IdReunion).SelectMany(x => x.DetalleTemaReunion).ToList().Count;

            if (cant == 0)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph("No hay detalle de Reunion disponibles para este acta de reunión");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }
            var TemasReunion = _acta.Reunion.TemaReunion.ToList();
            int num = 0;
            foreach(var tema in TemasReunion)
            {
                num++;
                for(int i = 0; i < tema.DetalleTemaReunion.Count;i++)
                {
                    var detalleTemaReunion = tema.DetalleTemaReunion.ElementAt(i);
                    row = table.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.HeadingFormat = false;
                    row.Shading.Color = Colors.White;
                    row.Format.Font.Bold = false;
                    paragraph = row[0].AddParagraph("D" + (i + 1).ToString());
                    paragraph.Format.Font.Color = Colors.Black;

                    paragraph = row[1].AddParagraph(num.ToString());
                    paragraph.Format.Font.Color = Colors.Black;

                    paragraph = row[2].AddParagraph(detalleTemaReunion.DetalleTema);
                    paragraph.Format.Font.Color = Colors.Black;
                }
            }            
        }

        private void SetAcuerdos(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 5.  Acuerdos", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var acuerdos = _acta.Acuerdo;
            
            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 40;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Justify;
            column.Width = 420;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nro.");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Acuerdos");
            row[1].Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Color = Colors.White;
            int num = 0;
            foreach(var acuerdo in acuerdos)
            {
                num++;
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                paragraph = row[0].AddParagraph(num.ToString());
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(acuerdo.DescripcionEspanol);
                paragraph.Format.Font.Color = Colors.Black;
            }
        }

        private void SetTareas(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 6.  Tareas", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var tareas = _acta.Tarea.ToList();

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 40;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph("Nro.");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph("Tarea");            
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph("Responsable");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[3].AddParagraph("Estado");
            paragraph.Format.Font.Color = Colors.White;
            int num = 0;
            foreach (var tarea in tareas)
            {
                num++;
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                paragraph = row[0].AddParagraph(num.ToString());
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(tarea.DescripcionEspanol);
                paragraph.Format.Font.Color = Colors.Black;
                var responsableTarea = tarea.ResponsableTarea.ToList();
                foreach(var responsable in responsableTarea)
                {                    
                    paragraph = row[2].AddParagraph(responsable.ParticipanteReunion.NombreCompleto);
                    paragraph.Format.Font.Color = Colors.Black;
                }
                paragraph = row[3].AddParagraph(tarea.Estado);
                paragraph.Format.Font.Color = Colors.Black;
            }
        }

        private void SetFoto(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 7.  Foto de la Reunión", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
                        
            Image image = section.AddImage(_rutaFoto);
            image.Left = ShapePosition.Center;
            image.Width = 350;
            image.LockAspectRatio = true;
        }

        private void SetFirma(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph("Sección 8.  Firma", "Heading2");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            
        }

    }
}
