﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.CA.ABET.Logic.Areas.Professor.CareerCurriculum.Export;
using UPC.CA.ABET.Logic.Areas.Professor.DaoImpl;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using System.IO;
using System.Web;

namespace UPC.CA.ABET.Logic.Areas.Professor.IfcManagement.Export
{
    public class ExportWorkerIfc
    {
        private IFC _ifc;
        private ReunionProfesor _arp;
        private string _logoUpc;
        private AbetEntities ctx;
        private string _culture;
        private string _infraestructura;
        private string _apreciacionAlumnos;
        private string _apreciacionDelegado;
        private string _comentarioEncuesta;


        public ExportWorkerIfc(AbetEntities ctx, int idIfc, string pathLogoUpc, string culture, string infraestructura, string apreciacionAlumnos, string apreciacionDelegado, string comentarioEncuesta)
        {
            _logoUpc = pathLogoUpc;
            _ifc = IFCDaoImpl.GetIfc(ctx, idIfc);
            _culture = culture;
            this.ctx = ctx;
            _infraestructura = infraestructura;
            _apreciacionAlumnos = apreciacionAlumnos;
            _apreciacionDelegado = apreciacionDelegado;
            _comentarioEncuesta = comentarioEncuesta;
        }

      

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if (_ifc == null)
            {
                return String.Empty;
            }

            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;
            }
        }

        /// <summary>
        /// Método para obtener la dirección del archivo PDF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo PDF creado para la malla de cocos</returns>
        private string GetPdf(string directory)
        {
            Document document = CreateDocument();
            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            var curso = _ifc.UnidadAcademica.CursoPeriodoAcademico.Curso;
            var periodoAcademico = _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.SubModalidad;
            string filename;

            if (_culture == ConstantHelpers.CULTURE.ESPANOL || curso.NombreIngles == null)
            {
                filename = "IFC_" + curso.Codigo.Replace(" ", "") + "_" + curso.NombreEspanol.Replace(" ", "")+ subModalidad.NombreEspanol.Replace(" ", "") +
               "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + "_" + _ifc.Docente.Codigo + ".pdf";
            }
            else
            {
                filename = "IFC_" + curso.Codigo.Replace(" ", "") + "_" + curso.NombreIngles.Replace(" ", "") + subModalidad.NombreIngles.Replace(" ", "") +
                "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + "_" + _ifc.Docente.Codigo + ".pdf";
            }

            filename = filename.DeleteSlashAndBackslash();
            string path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);

            return path;
        }

        /// <summary>
        /// Método para obtener la dirección del archivo RTF creado para la malla de cocos
        /// </summary>
        /// <param name="directory">Directorio donde se guardará el archivo de exportación</param>
        /// <returns>Dirección en el sistema de archivos del archivo RTF creado para la malla de cocos</returns>
        private string GetRtf(string directory)
        {
            Document document = CreateDocument();

            var curso = _ifc.UnidadAcademica.CursoPeriodoAcademico.Curso;
            var periodoAcademico = _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.SubModalidad;
            string filename;
            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                filename = "IFC_" + curso.Codigo.Replace(" ", "") + "_" + curso.NombreEspanol.Replace(" ", "") + "_" + subModalidad.NombreEspanol.Replace(" ","")+
                "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + "_" + _ifc.Docente.Codigo + ".rtf";
            }
            else
            {
                filename = "IFC_" + curso.Codigo.Replace(" ", "") + "_" + curso.NombreIngles.Replace(" ", "") + "_" + subModalidad.NombreIngles.Replace(" ", "") +
                "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + "_" + _ifc.Docente.Codigo + ".rtf";
            }
            RtfDocumentRenderer renderer = new RtfDocumentRenderer();
            renderer.Render(document, filename, directory);
            string path = Path.Combine(directory, filename);

            return path;
        }

        /// <summary>
        /// Método para crear el documento de exportación de malla de cocos
        /// </summary>
        /// <returns>Documento que contiene la información necesaria para generar el archivo de exportación</returns>

        public string GetFileActas(ExportFileFormat fef, string directory)
        {
            if (_ifc == null)
            {
                return String.Empty;
            }

            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                case ExportFileFormat.Rtf: return GetRtf(directory);
                default: return String.Empty;
            }
        }

        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);
            SetContentSection(document);
            SetDetail(document);
            SetAnalisisDeResultado(document);
            SetResultadoAlcanzado(document);
            SetHallazgosYAccionesPrevias(document);
            SetHallazgos(document);
            SetAccionesPropuestas(document);
            SetDesarrolloUnidadesAprendizaje(document);

            return document;
        }

        private void SetDesarrolloUnidadesAprendizaje(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "5. DESARROLLO DE UNIDADES DE APRENDIZAJE" : "5. DEVELOPMENT OF LEARNING UNITS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "5.1 INFRAESTRUCTURA" : "5.1 INFRASTRUCTURE", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_infraestructura, "Normal");
            paragraph.Format.SpaceAfter = "2mm";

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "5.2 APRECIACIÓN DE ALUMNOS" : "5.2 APPRECIATION OF STUDENTS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_apreciacionAlumnos, "Normal");
            paragraph.Format.SpaceAfter = "2mm";

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "5.3 APRECIACIÓN DEL DELEGADO DEL CURSO" : "5.3 APPRECIATION OF COURSE DELEGATE", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_apreciacionDelegado, "Normal");
            paragraph.Format.SpaceAfter = "2mm";

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "5.4 COMENTARIOS DE LA ENCUESTA ACADÉMICA" : "5.4 COMMENTS OF ACADEMIC SURVEY", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_comentarioEncuesta, "Normal");
            paragraph.Format.SpaceAfter = "2mm";
        }

        private void SetAccionesPropuestas(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "4. ACCIONES PROPUESTAS" : "4. IMPROVEMENT ACTIONS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            // var acciones = GedServices.GetAccionMejorasForIfcServices(ctx, _ifc.IdIFC);                                               // RML001
            var acciones = GedServices.GetAccionMejorasForIfcServicesZ(ctx, _ifc.IdIFC);

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 135;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 190;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 135;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO HALLAZGO" : "FINDING CODE");
            paragraph.Format.Font.Color = Colors.White;

            foreach (var accion in acciones)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                paragraph = row[0].AddParagraph(accion.Codigo+'-'+accion.Identificador.ToString());                                                              // RML001
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? accion.DescripcionEspanol : accion.DescripcionIngles);
                paragraph.Format.Font.Color = Colors.Black;
                row[1].Format.Alignment = ParagraphAlignment.Justify;
                // paragraph = row[2].AddParagraph(accion.HallazgoAccionMejora.First().Hallazgos.Codigo);                                                    RML001
                paragraph = row[2].AddParagraph(accion.HallazgoAccionMejora.First().Hallazgos.Codigo+'-'+ accion.HallazgoAccionMejora.First().Hallazgos.Identificador.ToString());
                paragraph.Format.Font.Color = Colors.Black;
            }
        }

        private void SetHallazgos(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "3. HALLAZGOS" : "3. FINDINGS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            //var hallazgos = GedServices.GetHallazgosForIfcServices(ctx, _ifc.IdIFC);                                                      // RML001
            var hallazgos = GedServices.GetHallazgosZForIfcServices(ctx, _ifc.IdIFC);

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 290;

            //column = table.AddColumn();
            //column.Format.Alignment = ParagraphAlignment.Center;
            //column.Width = 70;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            //paragraph = row[2].AddParagraph("NIVEL DE ACEPTACIÓN");
            //paragraph.Format.Font.Color = Colors.White;

            foreach (var hallazgo in hallazgos)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                // paragraph = row[0].AddParagraph(hallazgo.Codigo);                                                                                     // RML001
                paragraph = row[0].AddParagraph(hallazgo.Codigo+'-'+hallazgo.Identificador.ToString());
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? hallazgo.DescripcionEspanol : hallazgo.DescripcionIngles);
                paragraph.Format.Font.Color = Colors.Black;
                row[1].Format.Alignment = ParagraphAlignment.Justify;
                //paragraph = row[2].AddParagraph(GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad));
                //paragraph.Format.Font.Color = Colors.Black;
            }
        }

        private void SetHallazgosYAccionesPrevias(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "2. HALLAZGOS Y ACCIONES PREVIAS" : "2. PRIOR FINDINGS AND ACTIONS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var idEscuela = HttpContext.Current.Session.GetEscuelaId();
            var idModalidad = HttpContext.Current.Session.GetModalidadId();
            //         var accionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(ctx, (int)_ifc.IdDocente, _ifc.IdUnidadAcademica, _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idEscuela);
            //   var accionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(ctx, (int)_ifc.IdDocente, _ifc.IdUnidadAcademica, _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idEscuela);
            var accionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(ctx, idModalidad, _ifc.IdUnidadAcademica, _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idEscuela);
            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 210;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "ESTADO" : "STATE");
            paragraph.Format.Font.Color = Colors.White;

            if (accionesPrevias.Count == 0)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;
                row[0].MergeRight = 2;
                paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "No hay acciones previas disponibles para este informe." : "No previous actions available for this Report.");
                paragraph.Format.Font.Color = Colors.Black;
                return;
            }

            foreach (var accion in accionesPrevias)
            {
                row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.HeadingFormat = false;
                row.Shading.Color = Colors.White;
                row.Format.Font.Bold = false;

                paragraph = row[0].AddParagraph(accion.Codigo);                                                                         // RML 001
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? accion.DescripcionEspanol : accion.DescripcionIngles);
                row[1].Format.Alignment = ParagraphAlignment.Justify;
                paragraph.Format.Font.Color = Colors.Black;
                paragraph = row[2].AddParagraph(accion.Estado);
                paragraph.Format.Font.Color = Colors.Black;
            }
        }

        private void SetResultadoAlcanzado(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "1.1 RESULTADO ALCANZADO" : "1.1 RESULT ACHIEVED", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "El curso tiene planteado el siguiente logro terminal:" : "The course has raised the following terminal achievement:", "Normal");
            paragraph.Format.SpaceAfter = "2mm";
            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                paragraph = section.AddParagraph(GedServices
                    .GetCursoMallaCurricularForIfcServices(ctx, _ifc.UnidadAcademica.CursoPeriodoAcademico.IdCurso, (int)_ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico)
                    .LogroFinCicloEspanol,
                    "Normal");
            }
            else
            {
                paragraph = section.AddParagraph(GedServices
                    .GetCursoMallaCurricularForIfcServices(ctx, _ifc.UnidadAcademica.CursoPeriodoAcademico.IdCurso, (int)_ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico)
                    .LogroFinCicloIngles,
                    "Normal");
            }
            paragraph.Format.SpaceAfter = "2mm";
        }

        private class CarreraComisionOutcome
        {
            public Carrera carrera { get; set; }
            public Comision comision { get; set; }
            public Outcome outcome { get; set; }

            public CarreraComisionOutcome()
            {

            }

            public CarreraComisionOutcome(Carrera _carrera, Comision _comision, Outcome _outcome)
            {
                this.carrera = _carrera;
                this.comision = _comision;
                this.outcome = _outcome;
            }

            public String getComisionEspañol()
            {
                String _nombreComision = "";
                switch (comision.Codigo)
                {
                    case "CAC":
                    case "CAC-CC": _nombreComision = "COMPUTACIÓN"; break;
                    case "EAC": _nombreComision = "INGENIERÍA"; break;
                    case "WASC": _nombreComision = "WASC"; break;
                    default: _nombreComision = comision.NombreEspanol; break;
                }
                return _nombreComision;
            }

            public String getComisionIngles()
            {
                String _nombreComision = "";
                switch (comision.Codigo)
                {
                    case "CAC":
                    case "CAC-CC": _nombreComision = "COMPUTING"; break;
                    case "EAC": _nombreComision = "ENGINEERING"; break;
                    case "WASC": _nombreComision = "WASC"; break;
                    default: _nombreComision = comision.NombreIngles; break;
                }
                return _nombreComision;
            }

        }

        private void SetAnalisisDeResultado(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "1. INFORMACIÓN GENERAL" : "1. GENERAL INFORMATION", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var carreraComisionOutcome = ctx.UnidadAcademicaResponsable
                          .Where(x => x.IdDocente == _ifc.IdDocente).Select(x => x.SedeUnidadAcademica.UnidadAcademica)
                          .Where(x => x.Nivel == 3 && x.IdUnidadAcademica == _ifc.IdUnidadAcademica).Select(x => x.CursoPeriodoAcademico)
                          .Where(x => x.IdSubModalidadPeriodoAcademico == _ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico && x.IdCurso == _ifc.UnidadAcademica.CursoPeriodoAcademico.IdCurso)
                          .Select(x => x.Curso.CursoMallaCurricular).FirstOrDefault().SelectMany(x => x.MallaCocosDetalle)
                          .Where(x => x.MallaCocos.IdSubModalidadPeriodoAcademico == _ifc.UnidadAcademica.IdSubModalidadPeriodoAcademico).ToList()
                          .Select(c => new CarreraComisionOutcome
                          {
                              carrera = c.MallaCocos.CarreraComision.Carrera,
                              comision = c.OutcomeComision.Comision,
                              outcome = c.OutcomeComision.Outcome,
                          }).ToList();

            var masDeUno = false;
            if (carreraComisionOutcome.Count > 1)
                masDeUno = true;

            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                paragraph = section.AddParagraph("El curso tiene definido un solo logro terminal. Los estudiantes " +
                                                 "alcanzan el logro terminal al finalizar el ciclo.", "Normal");
            }
            else
            {
                paragraph = section.AddParagraph("The course has defined a single terminal achievement. Students reach the terminal achievement at the end of the semester.", "Normal");
            }
            paragraph.Format.SpaceAfter = "2mm";

            if (masDeUno)
                paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Para la carrera, el curso contribuye a alcanzar los siguientes Student Outcomes:" : "For the career, the course contributes to achieve the next Student Outcomes:", "Normal");
            else
                paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Para la carrera, el curso contribuye a alcanzar el siguiente Student Outcome:" : "For the career, the course contributes to achieve the next Student Outcome:", "Normal");

            paragraph.Format.SpaceBefore = "2mm";
            paragraph.Format.SpaceAfter = "2mm";

            Style style = document.AddStyle("BulletList", "Normal");
            style.ParagraphFormat.LeftIndent = "0.3cm";
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            var idx = 0;

            foreach (var cco in carreraComisionOutcome)
            {
                String texto;
                if (_culture == ConstantHelpers.CULTURE.ESPANOL)
                {
                    texto = "Student Outcome (" + cco.carrera.NombreEspanol.ToUpper() + " - " +
                        cco.getComisionEspañol().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \""
                        + cco.outcome.DescripcionEspanol + "\"";
                }
                else
                {
                    texto = "Student Outcome (" + cco.carrera.NombreIngles.ToUpper() + " - " +
                        cco.getComisionIngles().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \""
                        + cco.outcome.DescripcionIngles + "\"";
                }
                ListInfo listinfo = new ListInfo();
                listinfo.ContinuePreviousList = idx > 0;
                listinfo.ListType = ListType.BulletList1;
                paragraph = section.AddParagraph(texto);
                paragraph.Style = "BulletList";
                paragraph.Format.ListInfo = listinfo;
                paragraph.Format.SpaceBefore = "1mm";
                idx++;
            }
            /*
            var carrera = _culture==ConstantHelpers.CULTURE.ESPANOL? 
                _ifc.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreEspanol :
                _ifc.UnidadAcademica.UnidadAcademica2.UnidadAcademica2.NombreIngles;
            var palabrasCarrera = carrera.Split(new char[] {' '});
            for (int i = 0; i < outcomes.Count; i++)
            {
                for (int j = 0; j < outcomes[i].Item2.Count; j++)
                {
                    var texto = _culture == ConstantHelpers.CULTURE.ESPANOL ?
                        "Student Outcome (" + carrera.ToUpper() + " - " + outcomes[i].Item1.ToUpper() + " - " + outcomes[i].Item2[j].Nombre.ToUpper() + ") \"" + outcomes[i].Item2[j].DescripcionEspanol + "\"" :
                        "Student Outcome (" + carrera.ToUpper() + " - " + outcomes[i].Item1.ToUpper() + " - " + outcomes[i].Item2[j].NombreIngles.ToUpper() + ") \"" + outcomes[i].Item2[j].DescripcionIngles + "\"";
                    ListInfo listinfo = new ListInfo();
                    listinfo.ContinuePreviousList = idx > 0;
                    listinfo.ListType = ListType.BulletList1;
                    paragraph = section.AddParagraph(texto);
                    paragraph.Style = "BulletList";
                    paragraph.Format.ListInfo = listinfo;
                    paragraph.Format.SpaceBefore = "1mm";
                    idx++;
                }
            }
             */
        }

        /// <summary>
        /// Método para especificar el numerado de páginas
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }

        /// <summary>
        /// Método para ingresar los detalles de la malla de cocos al inicio del documento
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetDetail(Document document)
        {
            Section section = document.LastSection;

            Image image = section.AddImage(_logoUpc);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");

            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            var UnidadAcademicaPadre = ctx.UnidadAcademica.Where(x => x.IdUnidadAcademica == _ifc.IdUnidadAcademica)
                .FirstOrDefault().UnidadAcademica2.UnidadAcademica2;
            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ?
                UnidadAcademicaPadre.NombreEspanol : UnidadAcademicaPadre.NombreIngles, "Heading1");
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "INFORME DE FIN DE CICLO" : "END OF SEMESTER REPORT", "Heading2");
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CICLO:\t\t\t\t\t" : "SEMESTER:\t\t\t\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var periodoAcademico = _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            paragraph.AddFormattedText(periodoAcademico.CicloAcademico, "Heading3").Font.Color = Colors.Black;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CURSO:\t\t\t\t" : "COURSE:\t\t\t\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var curso = _ifc.UnidadAcademica.CursoPeriodoAcademico.Curso;
            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
                paragraph.AddFormattedText(curso.Codigo + " - " + curso.NombreEspanol, "Heading3").Font.Color = Colors.Black;
            else
                paragraph.AddFormattedText(curso.Codigo + " - " + curso.NombreIngles, "Heading3").Font.Color = Colors.Black;
            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "COORDINADOR DE CURSO:\t\t" : "COURSE COORDINATOR:\t\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.KeepWithNext = true;
            var docente = _ifc.Docente;
            paragraph.AddFormattedText(docente.Apellidos + ", " + docente.Nombres, "Heading3").Font.Color = Colors.Black;
        }

        /// <summary>
        /// Método para definir los estilos a utilizar en la creación del documento de malla de cocos
        /// </summary>
        /// <param name="document">Documento de malla de cocos</param>
        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }
    }

    public class ExportWorkerARP
    {
       
        private ReunionProfesor _arp;
        private string _logoUpc;
        private AbetEntities ctx;
        private string _culture;

        public ExportWorkerARP(AbetEntities ctx, int idActa, string pathLogoUpc, string culture)
        {
            _logoUpc = pathLogoUpc;
            _arp = IFCDaoImpl.GetARP(ctx, idActa);
            _culture = culture;
            this.ctx = ctx;
        }

        public string GetFile(ExportFileFormat fef, string directory)
        {
            if (_arp == null)
            {
                return String.Empty;
            }

            switch (fef)
            {
                case ExportFileFormat.Pdf: return GetPdf(directory);
                default: return String.Empty;
            }
        }

        private string GetPdf(string directory)
        {
            Document document = CreateDocument();

            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;

            renderer.RenderDocument();

            var curso = _arp.AreaEncargada;
            var periodoAcademico = _arp.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            var subModalidad = _arp.UnidadAcademica.SubModalidadPeriodoAcademico.SubModalidad;
            string filename;

           
                filename = "ActaDeReunion_" + curso.Replace(" ", "") + "_"+ subModalidad.NombreEspanol.Replace(" ", "") +
               "_" + periodoAcademico.CicloAcademico.Replace(" ", "") + "_" + ".pdf";
           
            filename = filename.DeleteSlashAndBackslash();
            string path = Path.Combine(directory, filename);
            renderer.PdfDocument.Save(path);

            return path;
        }

        private Document CreateDocument()
        {
            Document document = new Document();

            SetStyles(document);//
            SetContentSection(document);//
            SetDetail(document);//
            SetAnalisisDeResultado(document);//
            SetHallazgosYAccionesPrevias(document);//
            SetHallazgos(document);//
            SetAccionesPropuestas(document);//

            return document;
        }
        
        private void SetAccionesPropuestas(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Sección 2. Agenda" : "4. IMPROVEMENT ACTIONS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            //var DetalleReun = _arp.DetalleReunion.Where(X => X.IdTipoDetalleReunion == 1);                                               // RML001
            //var acciones = GedServices.GetAccionMejorasForIfcServicesZ(ctx, _arp.IdIFC);

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 135;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 190;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 135;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO HALLAZGO" : "FINDING CODE");
            paragraph.Format.Font.Color = Colors.White;

            //foreach (var a in DetalleReun)
            //{
            //    row = table.AddRow();
            //    row.VerticalAlignment = VerticalAlignment.Center;
            //    row.HeadingFormat = false;
            //    row.Shading.Color = Colors.White;
            //    row.Format.Font.Bold = false;

            //    paragraph = row[0].AddParagraph(a.IdDetalleReunion.ToString());                                                              // RML001
            //    paragraph.Format.Font.Color = Colors.Black;
            //    paragraph = row[1].AddParagraph(a.Descripcion);
            //    paragraph.Format.Font.Color = Colors.Black;
            //    row[1].Format.Alignment = ParagraphAlignment.Justify;
            //    // paragraph = row[2].AddParagraph(accion.HallazgoAccionMejora.First().Hallazgos.Codigo);                                                    RML001
            //    //paragraph = row[2].AddParagraph(accion.HallazgoAccionMejora.First().Hallazgos.Codigo + '-' + accion.HallazgoAccionMejora.First().Hallazgos.Identificador.ToString());
            //    //paragraph.Format.Font.Color = Colors.Black;
            //}
        }//

        private void SetHallazgos(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Sección 3.	Detalle de lo Tratado" : "3. FINDINGS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            //var hallazgos = GedServices.GetHallazgosForIfcServices(ctx, _ifc.IdIFC);                                                      // RML001
            //var DetalleReun = _arp.DetalleReunion.Where(x => x.IdTipoDetalleReunion ==3);

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 290;

            //column = table.AddColumn();
            //column.Format.Alignment = ParagraphAlignment.Center;
            //column.Width = 70;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            //paragraph = row[2].AddParagraph("NIVEL DE ACEPTACIÓN");
            //paragraph.Format.Font.Color = Colors.White;

            //foreach (var a in DetalleReun)
            //{
            //    row = table.AddRow();
            //    row.VerticalAlignment = VerticalAlignment.Center;
            //    row.HeadingFormat = false;
            //    row.Shading.Color = Colors.White;
            //    row.Format.Font.Bold = false;

            //    // paragraph = row[0].AddParagraph(hallazgo.Codigo);                                                                                     // RML001
            //    paragraph = row[0].AddParagraph(a.Dependencia.ToString());
            //    paragraph.Format.Font.Color = Colors.Black;
            //    paragraph = row[1].AddParagraph(a.Descripcion);
            //    paragraph.Format.Font.Color = Colors.Black;
            //    row[1].Format.Alignment = ParagraphAlignment.Justify;
            //    //paragraph = row[2].AddParagraph(GedServices.GetTextoForCodigoNivelAceptacionHallazgoIfcServices(hallazgo.NivelDificultad));
            //    //paragraph.Format.Font.Color = Colors.Black;
            //}
        }//

        private void SetHallazgosYAccionesPrevias(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Sección 4.	Acuerdos" : "2. PRIOR FINDINGS AND ACTIONS", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

            var idEscuela = HttpContext.Current.Session.GetEscuelaId();
            var idModalidad = HttpContext.Current.Session.GetModalidadId();
            //         var accionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(ctx, (int)_ifc.IdDocente, _ifc.IdUnidadAcademica, _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idEscuela);
            //   var accionesPrevias = GedServices.GetAccionesMejoraPreviasForIfcServices(ctx, (int)_ifc.IdDocente, _ifc.IdUnidadAcademica, _ifc.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico.CicloAcademico, idEscuela);

            //var DetalleReun = _arp.DetalleReunion.Where(x => x.IdTipoDetalleReunion == 2);

            Table table = section.AddTable();
            table.Format.PageBreakBefore = false;
            table.Borders.Visible = true;
            table.Rows.Height = 30;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 170;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 210;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Width = 80;

            Row row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            row.HeadingFormat = true;
            row.Shading.Color = Colors.Red;
            row.Height = 15;
            row.Format.Font.Bold = true;

            paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CÓDIGO" : "CODE");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[1].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "DESCRIPCIÓN" : "DESCRIPTION");
            paragraph.Format.Font.Color = Colors.White;
            paragraph = row[2].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "ESTADO" : "STATE");
            paragraph.Format.Font.Color = Colors.White;

            //if (accionesPrevias.Count == 0)
            //{
            //    row = table.AddRow();
            //    row.VerticalAlignment = VerticalAlignment.Center;
            //    row.HeadingFormat = false;
            //    row.Shading.Color = Colors.White;
            //    row.Format.Font.Bold = false;
            //    row[0].MergeRight = 2;
            //    paragraph = row[0].AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "No hay acciones previas disponibles para este informe." : "No previous actions available for this Report.");
            //    paragraph.Format.Font.Color = Colors.Black;
            //    return;
            //}

            //foreach (var a in DetalleReun)
            //{
            //    row = table.AddRow();
            //    row.VerticalAlignment = VerticalAlignment.Center;
            //    row.HeadingFormat = false;
            //    row.Shading.Color = Colors.White;
            //    row.Format.Font.Bold = false;

            //    paragraph = row[0].AddParagraph(a.IdReunionProfesores.ToString());                                                                         // RML 001
            //    paragraph.Format.Font.Color = Colors.Black;
            //    paragraph = row[1].AddParagraph(a.Descripcion);
            //    row[1].Format.Alignment = ParagraphAlignment.Justify;
            //    //paragraph.Format.Font.Color = Colors.Black;
            //    //paragraph = row[2].AddParagraph(accion.Estado);
            //    //paragraph.Format.Font.Color = Colors.Black;
            //}
        }//

        private void SetAnalisisDeResultado(Document document)
        {
            Section section = document.LastSection;
            Paragraph paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "1. INFORMACIÓN GENERAL" : "1. GENERAL INFORMATION", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Alignment = ParagraphAlignment.Left;

         

            var masDeUno = false;
            //if (carreraComisionOutcome.Count > 1)
            //    masDeUno = true;

            if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            {
                paragraph = section.AddParagraph("El curso tiene definido un solo logro terminal. Los estudiantes " +
                                                 "alcanzan el logro terminal al finalizar el ciclo.", "Normal");
            }
            else
            {
                paragraph = section.AddParagraph("The course has defined a single terminal achievement. Students reach the terminal achievement at the end of the semester.", "Normal");
            }
            paragraph.Format.SpaceAfter = "2mm";

            if (masDeUno)
                paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Para la carrera, el curso contribuye a alcanzar los siguientes Student Outcomes:" : "For the career, the course contributes to achieve the next Student Outcomes:", "Normal");
            else
                paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Para la carrera, el curso contribuye a alcanzar el siguiente Student Outcome:" : "For the career, the course contributes to achieve the next Student Outcome:", "Normal");

            paragraph.Format.SpaceBefore = "2mm";
            paragraph.Format.SpaceAfter = "2mm";

            Style style = document.AddStyle("BulletList", "Normal");
            style.ParagraphFormat.LeftIndent = "0.3cm";
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            var idx = 0;

            //foreach (var cco in carreraComisionOutcome)
            //{
            //    String texto;
            //    if (_culture == ConstantHelpers.CULTURE.ESPANOL)
            //    {
            //        texto = "Student Outcome (" + cco.carrera.NombreEspanol.ToUpper() + " - " +
            //            cco.getComisionEspañol().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \""
            //            + cco.outcome.DescripcionEspanol + "\"";
            //    }
            //    else
            //    {
            //        texto = "Student Outcome (" + cco.carrera.NombreIngles.ToUpper() + " - " +
            //            cco.getComisionIngles().ToUpper() + " - " + cco.outcome.Nombre.ToUpper() + ") \""
            //            + cco.outcome.DescripcionIngles + "\"";
            //    }
            //    ListInfo listinfo = new ListInfo();
            //    listinfo.ContinuePreviousList = idx > 0;
            //    listinfo.ListType = ListType.BulletList1;
            //    paragraph = section.AddParagraph(texto);
            //    paragraph.Style = "BulletList";
            //    paragraph.Format.ListInfo = listinfo;
            //    paragraph.Format.SpaceBefore = "1mm";
            //    idx++;
            //}
           
        }//

        private void SetContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.StartingNumber = 1;

            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            section.Footers.Primary.Add(paragraph);
        }//

        private void SetDetail(Document document)
        {
            Section section = document.LastSection;

            Image image = section.AddImage(_logoUpc);
            image.Left = ShapePosition.Center;
            image.Width = 50;
            image.LockAspectRatio = true;

            Paragraph paragraph = section.AddParagraph("UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS", "Heading1");

            paragraph.Format.SpaceBefore = "8mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

 
            paragraph.Format.SpaceBefore = "5mm";
            paragraph.Format.SpaceAfter = "10mm";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.PageBreakBefore = false;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "INFORME DE FIN DE CICLO" : "END OF SEMESTER REPORT", "Heading2");
            paragraph.Format.Borders.Bottom.Visible = true;
            paragraph.Format.Borders.Top.Visible = false;
            paragraph.Format.Borders.Left.Visible = false;
            paragraph.Format.Borders.Right.Visible = false;
            paragraph.Format.Borders.Color = Colors.Red;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "CICLO:\t\t\t\t\t" : "SEMESTER:\t\t\t\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;




            var Docente = _arp.UnidadAcademica.SedeUnidadAcademica.FirstOrDefault().UnidadAcademicaResponsable.FirstOrDefault().Docente;
    

            var periodoAcademico = _arp.UnidadAcademica.SubModalidadPeriodoAcademico.PeriodoAcademico;
            paragraph.AddFormattedText(periodoAcademico.CicloAcademico, "Heading3").Font.Color = Colors.Black;

            paragraph = section.AddParagraph(_culture == ConstantHelpers.CULTURE.ESPANOL ? "Encargado:\t\t\t\t" : "Area:\t\t\t\t", "Heading3");
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.KeepWithNext = true;
            var curso = _arp;



          
           paragraph.AddFormattedText(Docente.Nombres + " - " + _arp.AreaEncargada, "Heading3").Font.Color = Colors.Black;
     
            paragraph.Format.Font.Color = Colors.Red;
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            //paragraph.Format.LeftIndent = 30;
            paragraph.Format.SpaceAfter = "5mm";
            paragraph.Format.KeepWithNext = true;
            //////var docente = _arp.ReunionProfesoresParticipantes..Docente;
           ///// paragraph.AddFormattedText(docente.Apellidos + ", " + docente.Nombres, "Heading3").Font.Color = Colors.Black;
        }//

        private void SetStyles(Document document)
        {
            Style style = document.Styles["Normal"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.Font.Size = 10;
            style.Font.Color = Colors.Black;
            style.Font.Name = "Arial";

            style = document.Styles["Heading1"];
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.Font.Name = "Arial";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.Black;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.Font.Name = "Arial";
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        }//
    }
}
