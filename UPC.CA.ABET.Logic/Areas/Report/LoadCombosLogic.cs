﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using UPC.CA.ABET.Helpers;
using UPC.CA.ABET.Models;
using Culture = UPC.CA.ABET.Helpers.ConstantHelpers.CULTURE;
using Modality = UPC.CA.ABET.Helpers.ConstantHelpers.MODALITY;
using System;

namespace UPC.CA.ABET.Logic.Areas.Report
{
    public class LoadCombosLogic
    {
        private readonly AbetEntities _context;
        private int CollegeId { get { return HttpContext.Current.Session != null ? HttpContext.Current.Session.GetEscuelaId() : 1; } }

        public LoadCombosLogic(AbetEntities Context)
        {
            this._context = Context;
            if (this._context == null)
            {
                this._context = new AbetEntities();

            }
        }

        //Se debe agregar para que liste solo los periodos academicos de una determinada submodalidad

        #region ParaExportarPlan

        public IEnumerable<ComboItem> ListAnios(int CollegeId = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboAnios {0}", CollegeId).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListModalities(int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboModalidades {0}", Skip).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListCareerForModality(string Idioma,int ModalidadId,int EscuelaId)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboCarrera {0},{1},{2}", Idioma, EscuelaId, ModalidadId).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListCommissionForCareerandAnio(int CareerId = 0, string Anio ="")
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboComisiones {0},{1}", CareerId,Anio).AsEnumerable();
        }  
        
        public IEnumerable<ComboItem> ListInstrumentForPlanandCarrer(int PlanId = 0, int CareerId = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboInstrumentosParaPlan {0},{1}", PlanId, CareerId).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListConstituentForInstrument(int InstrumentId = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboConstituyenteParaPlan {0}", InstrumentId).AsEnumerable();
        }

        #endregion

        public IEnumerable<ComboItem> ListCycles(int Skip = 0)
        {
            //validado 
            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclos {0}", Skip).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCyclesForModality(string ModalityId = Modality.PREGRADO_REGULAR, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclos {0}, {1}, {2}", Skip, ModalityId, 0).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListCyclesFromForModality(string ModalityId = Modality.PREGRADO_REGULAR, int CycleFromId = 0, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclos {0}, {1}, {2}", Skip, ModalityId, CycleFromId).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListCyclesToForModality(string ModalityId = Modality.PREGRADO_REGULAR, int CycleFromId = 0, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclos {0}, {1}, {2}", Skip, ModalityId, CycleFromId).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListModulesBetweenCyclesForModality(string ModalityId = Modality.PREGRADO_REGULAR, int CycleFromId = 0, int CycleToId = 0, string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboModuloPorModalidadEntreCiclos {0}, {1}, {2}, {3}", Language, ModalityId, CycleFromId, CycleToId);
        }
        public IEnumerable<ComboItem> ListModulesForCycleForModality(string ModalityId = Modality.PREGRADO_REGULAR, int CycleId = 0, string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboModuloPorModalidadEntreCiclos {0}, {1}, {2}, {3}", Language, ModalityId, CycleId, CycleId);
        }

        public IEnumerable<ComboItem> ListAccreditationTypes(int CycleID = 0)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboAcreditacion {0}", CycleID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListAccreditationTypeBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboAcreditacionEntreCiclos {0}, {1}", CycleFromID, CycleToID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCommissions(int AccreditationID = 0, int CycleID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboComision {0}, {1}, {2}", AccreditationID, CycleID, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCommissionBetweenCycles(int AccreditationID = 0, int CycleFromID = 0, int CycleToID = 0, int CareerID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboComisionEntreCiclos {0}, {1}, {2}, {3}, {4}", AccreditationID, CycleFromID, CycleToID, CareerID, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCommissionsByCareer(int AccreditationID = 0, int CareerId = 0, int CycleId = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboComisionPorCarrera {0}, {1}, {2}, {3}", AccreditationID, CareerId, CycleId, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCareersByCommission(int ComissionID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboCarreraPorComision {0}, {1}, {2}", ComissionID, Language, CollegeId).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCareers(string Language = Culture.ESPANOL, string ModalityId = Modality.PREGRADO_REGULAR)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboCarrera {0}, {1}, {2}", Language, CollegeId, ModalityId).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCampus(int CycleID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboSede {0},{1}", CycleID,Language).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListEvd(int CycleID = 0)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboEVD {0}", CycleID);
        }

        public IEnumerable<ComboItem> ListCampusBetweenCycles(int CycleFromID = 0, int CycleToID = 0)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboSedeEntreCiclos {0}, {1}", CycleFromID, CycleToID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListConstituents(string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboConstituyentes {0}", Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCoursesByOutcomeType(string CourseOutcomeTypeName, int CareerID = 0, int StudentOutcomeID = 0, int CampusID = 0, int CommissionID = 0, int CycleFromID = 0, int CycleToID = 0, string Language = Culture.ESPANOL)
        {
            //validado--
            return _context.Database.SqlQuery<ComboItem>("ListarComboCursoPorTipoOutcome {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", Language, CourseOutcomeTypeName, CampusID, CareerID, StudentOutcomeID, CommissionID, CycleFromID, CycleToID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCoursesOfFormation(int CareerID = 0, int CampusID = 0, int CycleFromID = 0, int CycleToID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboCursoDeFormacion {0}, {1}, {2}, {3}, {4}", Language, CampusID, CareerID, CycleFromID, CycleToID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCoursesByConstituent(int ConstituentID, int CareerID = 0, int CycleID = 0, int CampusID = 0, int CommissionID = 0, int StudentOutcomeID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboCursoPorConstituyente {0}, {1}, {2}, {3}, {4}, {5}, {6}", ConstituentID, CareerID, CycleID, CampusID, StudentOutcomeID, CommissionID, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListStudentOutcomes(int CommissionID = 0, int CycleId = 0, int CareerID = 0, int CampusID = 0, string Language = Culture.ESPANOL, string CourseOutcomeTypeName = "")
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboStudentOutcome {0}, {1}, {2}, {3}, {4}, {5}", Language, CourseOutcomeTypeName, CareerID, CommissionID, CycleId, CampusID).AsEnumerable();
        }
        
        public IEnumerable<ComboItem> ListStudentOutcomesSemaphore(int CommissionID = 0, int CycleId = 0, int CareerID = 0, int CampusID = 0, string Language = Culture.ESPANOL, string CourseOutcomeTypeName = "")
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboStudentOutcomeAll {0}, {1}, {2}, {3}, {4}, {5}", Language, CourseOutcomeTypeName, CareerID, CommissionID, CycleId, CampusID).AsEnumerable();
        }

        public IEnumerable<ComboItem> ListStudentOutcomesBetweenCycles(int CommissionID = 0, int CycleFromId = 0, int CycleToId = 0, int CareerID = 0, int CampusID = 0, string Language = Culture.ESPANOL, string CourseOutcomeTypeName = "")
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboStudentOutcomeEntreCiclos {0}, {1}, {2}, {3}, {4}, {5}, {6}", Language, CourseOutcomeTypeName, CareerID, CommissionID, CycleFromId, CycleToId, CampusID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListStudentOutcomesByCommission(int CommissionID = 0, int CycleID = 0, int AccreditationID = 0, string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboStudentOutcomePorComision {0}, {1}, {2}, {3}", CycleID, CommissionID, AccreditationID, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListInstruments(int ConstituentID = 0, string Acronyms = "", string Language = Culture.ESPANOL)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboInstrumentos {0}, {1}, {2}", Language, Acronyms, ConstituentID).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListLevels()
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("ListarComboNiveles").AsEnumerable();
        }
        public IEnumerable<ComboItem> SpListLevels(int CycleFromId = 0,int IdSubModalidadPeriodoAcademico=0)
        {
            //validado
            return _context.Database.SqlQuery<ComboItem>("SpListarComboNiveles {0},{1}", CycleFromId, IdSubModalidadPeriodoAcademico).AsEnumerable();
        }
        public IEnumerable<Alumno> ListExitStudents(int CycleID = 0, int CareerID = 0, int CampusID = 0, string Text = "")
        {
            //validado
            return _context.Database.SqlQuery<Alumno>("ListarEstudiantesEgresados {0}, {1}, {2}, {3}", CycleID, CareerID, CampusID, Text).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListPastCycles()
        {

            return _context.Database.SqlQuery<ComboItem>("ListarComboCiclosPasados").AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCriticality(string Language = Culture.ESPANOL)
        {

            return _context.Database.SqlQuery<ComboItem>("ListarCriticidad {0}", Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListAcceptanceLevels(string ReportCode, string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboNivelesAceptacion {0}, {1}", ReportCode, Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListAcceptanceLevelsFindings(string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboNivelesAceptacionHallazgo {0}", Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListModalities(string Language = Culture.ESPANOL)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboModalidad {0}", Language).AsEnumerable();
        }
        public IEnumerable<ComboItem> ListCyclesToModalidadSubModalidad(string ModalityId = Modality.PREGRADO_REGULAR, int CycleFromId = 0, int Skip = 0)
        {
            return _context.Database.SqlQuery<ComboItem>("ListarComboSubModalidad {0}, {1}, {2}", Skip, ModalityId, CycleFromId).AsEnumerable();
        }
    }


}
