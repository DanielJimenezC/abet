
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace UPC.CA.ABET.Helpers
{
    public static class ConvertHelpers
    {
        #region Int32 Convert Helper
        /// <summary>
        /// Covierte un objeto a un entero tipo Int32
        /// </summary>
        /// <param name="val"></param>
        /// <param name="def"></param>
        /// <returns>Int32</returns>
        public static Int32 ToInteger(this object val, Int32 def)
        {
            try
            {
                Int32 reval = 0;

                if (Int32.TryParse(val.ToString(), out reval))
                    return reval;
            }
            catch (Exception)
            {
            }

            return def;
        }

        public static Int32 ToInteger(this object val)
        {
            return ConvertHelpers.ToInteger(val, 0);
        }
        #endregion

        #region Decimal Convert Helper
        /// <summary>
        /// Covierte un objeto a un entero tipo Decimal
        /// </summary>
        /// <param name="val"></param>
        /// <param name="def"></param>
        /// <returns>Decimal</returns>
        public static Decimal ToDecimal(this object val, Decimal def)
        {
            try
            {
                Decimal reval = 0;

                if (Decimal.TryParse(val.ToString(), out reval))
                    return reval;
            }
            catch (Exception)
            {
            }

            return def;
        }

        /// <summary>
        /// Covierte un objeto a un entero tipo Decimal
        /// </summary>
        /// <param name="val"></param>
        /// <param name="def"></param>
        /// <returns>Decimal</returns>
        public static Decimal ToDecimal(this object val)
        {
            return ConvertHelpers.ToDecimal(val, 0);
        }
        #endregion

        #region Boolean Convert Helper
        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <param name="def"></param>
        /// <returns>Boolean</returns>
        public static Boolean ToBoolean(this object val, Boolean def)
        {
            try
            {
                Boolean reval = false;

                if (Boolean.TryParse(val.ToString(), out reval))
                    return reval;
            }
            catch (Exception)
            {
            }

            return def;
        }

        public static Boolean ToBoolean(this object val)
        {
            return ConvertHelpers.ToBoolean(val, false);
        }
        #endregion

        #region DateTime Convert Helper
        public static String ToFullCallendarDate(this DateTime val)
        {
            return val.ToString("yyyy-MM-dd hh:mm:ss");
        }
        public static DateTime ToDateTime(this object val)
        {
            return ConvertHelpers.ToDateTime(val, DateTime.MinValue);
        }
        public static String ToDateTimeFormatByCulture(this DateTime val, CultureInfo culture)
        {
            var format = culture.DateTimeFormat.ShortDatePattern.ToString();
            return val.ToString(format);
        }

        public static DateTime ToDateTime(this object val, DateTime def)
        {
            try
            {
                DateTime reval = DateTime.MinValue;

                if (DateTime.TryParse(val.ToString(), out reval))
                    return reval;
            }
            catch (Exception)
            {
            }

            return def;
        }

        public static int GetWeekNumber(DateTime date)
        {
            CultureInfo ci = CultureInfo.CurrentCulture;
            int weekNum = ci.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return weekNum;
        }

        public static DateTime FirstDateOfWeek(int year, int weekOfYear)
        {
            CultureInfo ci = CultureInfo.CurrentCulture;
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if (firstWeek <= 1 || firstWeek > 50)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }

        public static int GetNumeroSemanaOfReunion(DateTime inicio, DateTime fecha)
        {
            return (GetWeekNumber(fecha) - GetWeekNumber(inicio)) + 1;
        }
        #endregion

        #region String Convert Helper
        /// <summary>
        /// Retorna un String de manera segura evitando errores de objetos nulos
        /// </summary>
        /// <param name="val"></param>
        /// <returns>String</returns>
        public static String ToSafeString(this object val)
        {
            return (val ?? String.Empty).ToString();
        }

        public static String TostringUTF(this String val)
        {
            return Encoding.UTF8.GetString(Encoding.GetEncoding(1252).GetBytes(val ?? String.Empty));
        }
        public static String ToStringJavaScript(this String val)
        {
            return HttpUtility.JavaScriptStringEncode(val ?? String.Empty);
        }

        #endregion

        #region Enum Convert Helper
        public static T ToEnum<T>(this String value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        #endregion

        #region long Convert Helper
        /// <summary>
        /// Covierte un decimal a un entero tipo long
        /// </summary>
        /// <param name="val">Decimal</param>
        /// <returns>long</returns>
        public static long ToLong(this Decimal val)
        {
            long reval = 0;
            try
            {
                if (long.TryParse(val.ToString(), out reval))
                    return reval;
            }
            catch (Exception)
            {
            }
            return reval;

        }
        #endregion

        #region Json Convert Helper
        public static String ToJson(this object val)
        {
            return JsonConvert.SerializeObject(val, Formatting.Indented, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.None, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ObjectCreationHandling = ObjectCreationHandling.Reuse });
        }

        public static MvcHtmlString ToHTMLJson(this object val)
        {
            return MvcHtmlString.Create(val.ToJson());
        }
        #endregion

        #region TempData Convert Helper
        /// <summary>
        /// Retorna un objeto seg�n su tipo de dato (T) guardado en el diccionario TempData, 
        /// en caso el objeto no exista se crea una nueva instancia del mismo
        /// </summary>
        /// <param name="TempData">Diccionario TempData</param>
        /// <param name="KeyData">Nombre del Key a buscar en el TempData</param>
        /// <returns>Object</returns>
        public static T ToTempDataValue<T>(TempDataDictionary TempData, String KeyData)
        {
            var temporal = TempData[KeyData];
            return temporal != null ? (T)temporal : (T)Activator.CreateInstance(typeof(T));
        }
        #endregion

        #region ToRoman Convert Helper
        /// <summary>
        /// Convierte un numero a romanos
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        public static string ToRoman(this int Number)
        {
            if ((Number < 0) || (Number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (Number < 1) return string.Empty;
            if (Number >= 1000) return "M" + ToRoman(Number - 1000);
            if (Number >= 900) return "CM" + ToRoman(Number - 900); //EDIT: i've typed 400 instead 900
            if (Number >= 500) return "D" + ToRoman(Number - 500);
            if (Number >= 400) return "CD" + ToRoman(Number - 400);
            if (Number >= 100) return "C" + ToRoman(Number - 100);
            if (Number >= 90) return "XC" + ToRoman(Number - 90);
            if (Number >= 50) return "L" + ToRoman(Number - 50);
            if (Number >= 40) return "XL" + ToRoman(Number - 40);
            if (Number >= 10) return "X" + ToRoman(Number - 10);
            if (Number >= 9) return "IX" + ToRoman(Number - 9);
            if (Number >= 5) return "V" + ToRoman(Number - 5);
            if (Number >= 4) return "IV" + ToRoman(Number - 4);
            if (Number >= 1) return "I" + ToRoman(Number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }
        #endregion
    }
}