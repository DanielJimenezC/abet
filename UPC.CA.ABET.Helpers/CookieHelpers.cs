﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UPC.CA.ABET.Helpers
{
    public static class CookieHelpers
    {
        private static HttpContext _context { get { return HttpContext.Current; } }

        #region Get & Set
        public static void Set(CookieKey key, String value)
        {
            var cookie = new HttpCookie(key.ToString())
            {
                Value = value,
                Expires = DateTime.Now.AddHours(1),
                HttpOnly = true
            };
            _context.Response.Cookies.Add(cookie);
        }

        public static HttpCookie GetCookie(CookieKey key)
        {
            return _context.Request.Cookies[key.ToString()];
        }

        public static String GetValue(CookieKey key)
        {
            var cookie = GetCookie(key);
            return cookie != null ? _context.Server.HtmlEncode(cookie.Value).Trim() : String.Empty;
        }

        public static Boolean Exists(CookieKey key)
        {
            return _context.Request.Cookies[key.ToString()] != null;
        }
        #endregion

        #region Delete
        public static void Delete(CookieKey key)
        {
            if (Exists(key))
            {
                var cookie = new HttpCookie(key.ToString()) { Expires = DateTime.Now.AddDays(-1) };
                _context.Response.Cookies.Add(cookie);
            }
        }

        public static void DeleteAll()
        {
            for (Int32 i = 0; i <= _context.Request.Cookies.Count - 1; i++)
                if (_context.Request.Cookies[i] != null)
                    Delete(_context.Request.Cookies[i].Name.ToEnum<CookieKey>());

        }
        #endregion

    }

}

