﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace UPC.CA.ABET.Helpers
{
    public static class UrlHelpers
    {
        #region ContentUrl Helpers
        public static string ContentUrl(this UrlHelper url, string contentRoute, string areaName = null)
        {
            var contentBase = "~/";
            contentBase += !String.IsNullOrEmpty(areaName) ? "Areas/" + areaName + "/Content/" : "Content/";
            return url.Content(contentBase + contentRoute);
        }

        public static string ContentUrl(this UrlHelper url, string contentRoute)
        {
            return ContentUrl(url, contentRoute, null);
        }
        #endregion

        #region Modal Helper
        public static string ModalHelperFor(this UrlHelper url, string actionName, string controllerName, object routeValues = null, ModalSize size = new ModalSize())
        {
            var modalSize = String.Empty;
            switch (size)
            {
                case ModalSize.Small:
                    modalSize = "modal-sm";
                    break;
                case ModalSize.Normal:
                     modalSize = "modal-md";
                    break;
                case ModalSize.Large:
                    modalSize = "modal-lg";
                    break;
                default:
                    break;
            }

            return "data-type=modal-link data-modal-size=" + modalSize + " data-source-url=" + url.Action(actionName, controllerName, routeValues);
        }

        public static string ModalHelperFor(this UrlHelper url, string actionName, string controllerName, ModalSize size = new ModalSize())
        {
            return ModalHelperFor(url, actionName, controllerName, null, size);
        }
        public static string ModalHelperFor(this UrlHelper url, string actionName, string controllerName)
        {
            return ModalHelperFor(url, actionName, controllerName, null, new ModalSize());
        }
        public static string ModalHelperFor(this UrlHelper url, string actionName, object routeValues = null, ModalSize size = new ModalSize())
        {
            return ModalHelperFor(url, actionName, null, routeValues, size);
        }
        public static string ModalHelperFor(this UrlHelper url, string actionName, object routeValues = null)
        {
            return ModalHelperFor(url, actionName, null, routeValues);
        }
        public static string ModalHelperFor(this UrlHelper url, string actionName, ModalSize size = new ModalSize())
        {
            return ModalHelperFor(url, actionName, null, null, size);
        }
        public static string ModalHelperFor(this UrlHelper url, string actionName)
        {
            return ModalHelperFor(url, actionName, null, null);
        }
        #endregion

        #region AbsoluteAction Helper
        /// <summary>
        /// Genera la url ansoluta del la accion 
        /// especificando el controller y atributos adicionales.
        /// </summary>        
        /// <param name="actionName">Nombre de la accion</param>
        /// <param name="controllerName">Nombre del controlador</param>
        /// <param name="routeValues">route values</param>
        /// <returns>URL Absoluta</returns>
        public static string AbsoluteAction(this UrlHelper url, string actionName, string controllerName, object routeValues = null)
        {
            var scheme = url.RequestContext.HttpContext.Request.Url.Scheme;
            return url.Action(actionName, controllerName, routeValues, scheme);
        }
        public static string AbsoluteAction(this UrlHelper url, string actionName, object routeValues = null)
        {
            return AbsoluteAction(url, actionName, null, routeValues);
        }
        public static string AbsoluteAction(this UrlHelper url, string actionName, string controllerName)
        {
            return AbsoluteAction(url, actionName, controllerName, null);
        }
        #endregion

    }
}
