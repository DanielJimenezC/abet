﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Helpers
{
    public class JsonEntity
    {
        public String Value { get; set; }
        public String Text { get; set; }
        public String Name { get; set; }
    }

    public class JsonFilterEntity
    {
        public String Value { get; set; }
        public String Text { get; set; }
        public String Name { get; set; }
        public String Filter { get; set; }
    }
    #region Rubricas

    public class RubricaEntity
    {
        public int IdRubrica { get; set;}
        public int IdCarrera { get; set; }
        public int IdPeriodoAcademico { get; set; }
        public int IdEvaluacion { get; set; }
        public int IdCurso { get; set; }
        public int IdCicloActual { get; set; }
        public bool EsNotaEvaluada { get; set; }
        public string NombreCicloActual { get; set; }
        public int[] SelectedComsiones { get; set; }

        public RubricaEntity() { }
        public RubricaEntity(int idCarrera,int idEvaluacion,int idCurso, int idCicloActual)
        {
            IdCarrera = idCarrera;
            IdEvaluacion = idEvaluacion;
            IdCurso = idCurso;
            IdCicloActual = idCicloActual;
        }

        public RubricaEntity(int idCarrera, int idEvaluacion, int idCurso, int idCicloActual, bool esNotaEvaluada, int idPeriodoAcademico, int[] Comsiones)
        {
            IdCarrera = idCarrera;
            IdEvaluacion = idEvaluacion;
            IdCurso = idCurso;
            IdCicloActual = idCicloActual;
            EsNotaEvaluada = esNotaEvaluada;
            IdPeriodoAcademico = idPeriodoAcademico;
            SelectedComsiones = Comsiones;
        }

    }

    public class OutcomeEntity
    {
        public int IdOutcome { get; set; }
        public String NombreOutcome { get; set; }
        public string DescripcionOutcome { get; set; }
        public Decimal? NotaOutcome { get; set; }
        public List<CriterioEntity> Criterios { get; set; }
    }

    public class CriterioEntity
    {
        public int IdOutcome { get; set; }
        public int IdCriterio { get; set; }
        public string NombreCriterio { get; set; }
        public string DescripcionCriterio { get; set; }
        public Decimal NotaMaxima { get; set; }
        public Decimal? NotaMinima { get; set; }
        public bool EsEvaluadoPorDefecto { get; set; }
        public List<NivelCriterioEntity> Niveles { get; set; }

        public CriterioEntity() { Niveles = new List<NivelCriterioEntity>(); }

        public CriterioEntity(int idOutcome, int idCriterio)
        {
            IdOutcome = idOutcome;
            IdCriterio = idCriterio;
        }

        public CriterioEntity(int idOutcome, string nombre, string descripcion, float notaMinima, float notaMaxima)
        {
            IdOutcome = idOutcome;
            NombreCriterio = nombre;
            DescripcionCriterio = descripcion;
            NotaMaxima = (Decimal)notaMaxima;
            NotaMinima = (Decimal)notaMinima;
        }

        public CriterioEntity(int idOutcome, float notaMaxima,float notaMInima)
        {
            IdOutcome = idOutcome;
            NotaMaxima = (Decimal)notaMaxima;
            NotaMinima = (Decimal)notaMInima;
        }
    }

    public class NivelCriterioEntity
    {
        public int IdCriterio { get; set; }
        public int IdNivel { get; set; }
        public string NombreNivel { get; set; }
        public Decimal? NivelNotaMaxima { get; set; }
        public Decimal? NivelNotaMinima { get; set; }
        public string DescripcionNivel { get; set; }

        public NivelCriterioEntity() { }

        public NivelCriterioEntity(int idCriterio, string nombreNivel, float notaMaxima, float notaMinima, string descripcionNivel)
        {
            IdCriterio = idCriterio;
            NombreNivel = nombreNivel;
            NivelNotaMaxima = (Decimal)notaMaxima;
            NivelNotaMinima = (Decimal)notaMinima;
            DescripcionNivel = descripcionNivel;
        }
    }

    public class JsonClass
    {
        public Int32 id { get; set; }
        public String text { get; set; }
    }
    #endregion

}
