﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace UPC.CA.ABET.Helpers
{
    public static class AppSettingsHelper
    {
        public static string GetAppSettings(string key, string defaultValue = "")
        {
            return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains(key)) &&
                     ConfigurationManager.AppSettings[key] != null
                     ? ConfigurationManager.AppSettings[key].ToString() : defaultValue;
        }
    }
}
