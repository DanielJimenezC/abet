﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace UPC.CA.ABET.Helpers
{
    public class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            ModelState modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                actualValue = Convert.ToDecimal(valueResult.AttemptedValue, CultureInfo.CurrentCulture);
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }

    public class MultilanguajeDateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var displayFormat = bindingContext.ModelMetadata.DisplayFormatString;
            var modelName = bindingContext.ModelName;
            var value = bindingContext.ValueProvider.GetValue(modelName);
            var attemptedValue = value != null ? value.AttemptedValue : null;

            try
            {
                Int32 day, month, year;
                DateTime? date;

                if (System.Threading.Thread.CurrentThread.CurrentUICulture.ToString() == ConstantHelpers.CULTURE.INGLES)
                {
                    string[] dateChain = attemptedValue.Split('/');
                    Int32.TryParse(dateChain[0], out month);
                    Int32.TryParse(dateChain[1], out day);
                    Int32.TryParse(dateChain[2], out year);

                    var dateString = string.Format("{0}/{1}/{2}", day, month, year);
                    date = Convert.ToDateTime(dateString);

                }
                else
                {
                    date = Convert.ToDateTime(attemptedValue);
                }

                return date;
            }
            catch (Exception)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("{0} no es una fecha correcta.", attemptedValue));
                return base.BindModel(controllerContext, bindingContext);
            }

        }
    }
}
