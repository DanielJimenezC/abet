﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace UPC.CA.ABET.Helpers
{
    public static class ExtensionsHelpers
    {
        /// <summary>
        ///   Guarda archivo xml dependiendo del objeto y el tipo definido.
        /// </summary>
        public static void SaveAsXML(this object obj, String fileName)
        {
            var xmlRequest = new StreamWriter(fileName);
            var xmlFileRequest = new XmlSerializer(obj.GetType());
            xmlFileRequest.Serialize(xmlRequest, obj);
            xmlRequest.Close();
        }

        public static Boolean IsBetween(this DateTime val, DateTime Start, DateTime End)
        {
            return val >= Start && val <= End;
        }

        /// <summary>
        ///   Remueve digitos con acentos dentro de una cadena.
        /// </summary>
        public static String RemoveDiacritics(this String srt)
        {
            try
            {
                var normalizedString = srt.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();
                foreach (var character in normalizedString)
                {
                    var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(character);
                    if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                        stringBuilder.Append(character);
                }
                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Reemplaza los espacios NonBreaking Space (char 160) por un espacio simple
        /// </summary>
        /// <param name="srt"></param>
        /// <returns>Cadena con espacio simple</returns>
        public static String ReplaceNoBbreakingSpace(this String srt)
        {
            var result = String.Empty;
            try
            {
                var espacioRaro = Convert.ToChar(160).ToString();
                result = srt.Replace(espacioRaro, " ");
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }



        /// <summary>
        ///   Remueve espacios en blanco y digitos con acentos dentro de una cadena.
        /// </summary>
        public static String TrimRemoveDiacritics(this String srt)
        {
            try
            {
                return RemoveDiacritics(srt.Trim()).DeleteDotAndComma().ToUpper();
            }
            catch (Exception)
            {
                return String.Empty;
                throw;
            }
        }

        public static String DeleteSlashAndBackslash(this String str)
        {
            str = str.Replace("/", string.Empty);
            str = str.Replace(@"\", string.Empty);
            return str;
        }

        /// <summary>
        ///   Calcula la cifra de recondeo para la moneda peruana con la regla .05 centimos
        /// </summary>
        /// <returns></returns>
        public static Decimal GetPeruCentsRound(this Decimal val)
        {
            val = val.RoundTwoDecimalPlaces();
            var value = val.ToString().Split('.')[1].Substring(1).ToDecimal();
            if (value == 5 || value == 0)
                return new Decimal(0);
            else if (value < 5)
                return value / (new Decimal(100));
            else
                return (value - 5) / (new Decimal(100));
        }

        /// <summary>
        ///  Devuelve un string con formato #.00 
        /// </summary>
        public static String ToStringDecimalCommaSeparate(this Decimal val)
        {
            //return val.ToString("#,##0.00");
            return val.ToString("F", CultureInfo.GetCultureInfo(ConstantHelpers.CULTURE.ESPANOL));
        }

        /// <summary>
        ///  Elimina los puntos y comas de un arreglo 
        /// </summary>
        public static String DeleteDotAndComma(this String str)
        {
            str = str.Replace(".", String.Empty);
            str = str.Replace(",", String.Empty);
            return str;
        }

        /// <summary>
        /// Crea nuevo directorio
        /// </summary>        
        public static void CreateDirectory(this DirectoryInfo directory)
        {
            if (!directory.Exists)
                directory.Create();
        }

        /// <summary>
        ///   Redondea decimal a 2 campos
        /// </summary>
        public static Decimal RoundTwoDecimalPlaces(this Decimal val)
        {
            return Decimal.Round(val, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Retonrna la diferencia entre 2 fechas detallando la hora, minuto, segundo
        /// </summary>
        /// <param name="val"></param>
        /// <param name="cultureString">Cultura que se quiere mostrar</param>
        /// <returns></returns>
        public static String ToDiferenceTimeString(this TimeSpan val, String cultureString)
        {
            var result = String.Empty;
            if (cultureString == ConstantHelpers.CULTURE.ESPANOL)
                result = String.Format("{0} dias, {1} horas, {2} minutos, {3} segundos", val.Days, val.Hours, val.Minutes, val.Seconds);
            else if (cultureString == ConstantHelpers.CULTURE.INGLES)
                result = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", val.Days, val.Hours, val.Minutes, val.Seconds);

            return result;
        }

        /// <summary>
        /// Busca si esiste un rol existente dentro
        /// </summary>
        /// <param name="lstRol">Lista de Roles</param>
        /// <param name="rol">rol a validar</param>
        /// <returns></returns>
        public static Boolean HasRole(this AppRol[] lstRol, params AppRol[] rol)
        {
            if (lstRol == null)
                return false;
            foreach (var item in rol)
                if (lstRol.Contains(item))
                    return true;
            return false;
        }

        /// <summary>
        /// Guarda la imagen en una ruta especifica del servidor
        /// </summary>
        /// <param name="file">Archivo a guardar</param>
        /// <param name="server"></param>
        /// <param name="size">Tamaño de la imagen a guardar</param>
        /// <returns>Ruta del de la imagen</returns>
        public static String SaveImageToServer(this HttpPostedFileBase file, HttpServerUtilityBase server, Int32 size = 320)
        {
            var result = String.Empty;
            try
            {
                if (file != null && file.ContentLength != 0)
                {
                    var guid = Guid.NewGuid().ToString().Substring(0, 6);
                    result = guid + "_" + Path.GetFileName(file.FileName);
                    var path = server.MapPath(ConstantHelpers.DEFAULT_SERVER_PATH);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    var image = System.Drawing.Image.FromStream(file.InputStream);
                    image = image.GetThumbnailImage(size, (size * (image.Height / image.Width).ToDecimal()).ToInteger(), null, IntPtr.Zero);
                    image.Save(Path.Combine(path, result));
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Guarda la imagen en una ruta especifica del servidor
        /// </summary>
        /// <param name="file">Archivo a guardar</param>
        /// <param name="server"></param>
        /// <returns>Ruta del de la imagen</returns>
        public static String SaveImageToServer(this HttpPostedFileBase file, HttpServerUtilityBase server)
        {
            return SaveImageToServer(file, server, 320);
        }



    }
}