﻿using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Configuration;

namespace UPC.CA.ABET.Helpers
{
    public static class ConstantHelpers
    {
        #region AREAS
        public static class AREAS
        {
            public const String ADMIN = "Admin";
            public const String INDICATOR = "Indicator";
            public const String MEETING = "Meeting";
            public const String PROFESSOR = "Professor";
            public const String REPORT = "Report";
            public const String RUBRIC = "Rubric";
            public const String SURVEY = "Survey";
            public const String UPLOAD = "Upload";
        }
        #endregion

        #region ESTADO
        public static class ESTADO
        {
            public const String ACTIVO = "ACT";
            public const String INACTIVO = "INA";
            public const String REVISADO = "REV";
            public const String APROBADO = "APR";
        }

        public static class ESTADO_MATRICULA
        {
            public const string MATRICULADO = "MRE";
            public const string RETIRADO = "RTE";
        }

        #endregion

        #region HALLAZGO
        public static class HALLAZGO
        {
            #region GENERADO
            public const String AUTO = "AUT";
            public const String NORMAL = "NOR";
            #endregion
        }
        #endregion

        #region TEACHER_MEETING
        public static class TEACHER_MEETING
        {
            public static class ESTADO
            {
                public enum ACTA
                {
                    CERRADA_A_TIEMPO = 1,
                    CERRADA_CON_RETRASO = 2,
                    NO_COMPLETADA = 3,
                }

                public enum TAREA
                {
                    PENDIENTE = 4,
                    COMPLETADA = 5,
                    NO_COMPLETADA = 6,
                }
                public enum ACUERDO
                {
                    PENDIENTE = 7,
                    COMPLETADA = 8,
                    NO_COMPLETADA = 9,
                }
            }
        }
        public enum ORGANIZATION_CHART_LEVELS
        {
            FIRST_LEVEL = 1,
            SECOND_LEVEL = 2,
            THIRD_LEVEL = 3,
        }
        #endregion
        #region GLOBAL HELPERS

        public const String DEFAULT_SERVER_PATH = "~/Files/images";
        public const String DEFAULT_PASSWORD = "password2016";

        #region ROL
        public static class ROL
        {
            public const String ACREDITADOR = "ACR";
            public const String ADMINISTRADOR = "ADM";
            public const String DOCENTE = "DOC";
            public const String PROFESOR_GERENTE = "PGE";
            public const String COORDINADOR_CARRERA = "CCA";
            public const String DIRECTOR_CARRERA = "DCA";
            public const String ENCARGADO_CARGAS = "ECA";
            public const String GERENTE_GENERAL = "GGE";
            public const String CALIFICADOR_CONTROL = "CAC";

            public static AppRol? GetAppRol(String rol)
            {
                switch (rol)
                {
                    case ACREDITADOR:
                        return AppRol.Acreditador;
                    case ADMINISTRADOR:
                        return AppRol.Administrador;
                    case COORDINADOR_CARRERA:
                        return AppRol.CoordinadorCarrera;
                    case DOCENTE:
                        return AppRol.Docente;
                    case PROFESOR_GERENTE:
                        return AppRol.ProfesorGerente;
                    case DIRECTOR_CARRERA:
                        return AppRol.DirectorCarrera;
                    case ENCARGADO_CARGAS:
                        return AppRol.EncargadoCargas;
                    case GERENTE_GENERAL:
                        return AppRol.GerenteGeneral;
                    case CALIFICADOR_CONTROL:
                        return AppRol.CalificadorControl;
                    default:
                        return null;
                }

            }
        }
        #endregion

        

        #region HTML LAYOUT PATH
        public static class LAYOUT
        {
            public static readonly String DEFAULT_LAYOUT_PATH = "~/Views/Shared/_Layout.cshtml";
            public static readonly String MODAL_LAYOUT_PATH = "~/Views/Shared/_ModalLayout.cshtml";
            public static readonly String MODAL_EMAIL_PATH = "~/Views/Shared/_MailLayout.cshtml";
        }
        #endregion
        #endregion

        #region CULTURE
        public static class CULTURE
        {
            public const String ESPANOL = "es-PE";
            public const String INGLES = "en-US";
            public const Int32 TYPE_ESPANOL = 1;
            public const Int32 TYPE_INGLES = 2;

        }
        #endregion
        #region MODALITY
        public static class MODALITY
        {
            public const String PREGRADO_REGULAR = "1";
            public const String PREGRADO_EPE = "2";
        }
        #endregion

        #region ESCUELA
        public static class ESCUELA
        {
            public const String ESCUELA_INGENIERIA_SISTEMAS_COMPUTACION = "EISC";
            public const String ESCUELA_INGENIERIA_CIVIL = "EIC";
            public const String ESCUELA_INGENIERIA_INDUSTRIAL = "EII";
            public const String INDUSTRIAL_ENGINEERING_EXECUTIVE = "IIEPE";
        }
        #endregion

        #region PAGINATION
        public const Int32 DEFAULT_PAGE_SIZE = 20;
        public static PagedListRenderOptions TwitterBootstrapPagerAligned
        {
            get
            {
                return new PagedListRenderOptions()
                {
                    DisplayLinkToFirstPage = PagedListDisplayMode.Never,
                    DisplayLinkToLastPage = PagedListDisplayMode.Never,
                    DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
                    DisplayLinkToNextPage = PagedListDisplayMode.Always,
                    DisplayLinkToIndividualPages = true,
                    ContainerDivClasses = null,
                    UlElementClasses = new[] { "pagination" },
                    ClassToApplyToFirstListItemInPager = "first",
                    ClassToApplyToLastListItemInPager = "last",
                    LinkToFirstPageFormat = "<i class='fa fa-angle-double-left'><i>",
                    LinkToPreviousPageFormat = "<i class='fa fa-angle-left'></i>",
                    LinkToIndividualPageFormat = "{0}",
                    LinkToNextPageFormat = "<i class='fa fa-angle-right'></i>",
                    LinkToLastPageFormat = "<i class='fa fa-angle-double-right'><i>"
                };
            }
        }
        #endregion
            
        #region ENCUESTA
        public static class ENCUESTA
        {
            public const string IRD = "";

            public const Int32 ESCALA_FDC = 10;
            public const String SURVEY_DIRECTORY = "~/Files/SurveyReport";
            public const Int32 CANTIDAD_MAX_INFORME = 2;
            public const String CODIGO_CARRERA_SISTEMAS = "SI";
            public const String CONSTITUYENTE_EMPLEADOR = "Empleador";
            public const String CONSTITUYENTE_ESTUDIANTE = "Estudiante";
            public const String CONSTITUYENTE_GRADUADOS = "Graduados";
            public const String CRITERIO_CURSO = "Curso";
            public const String CRITERIO_CARRERA = "Carrera";
            public const String USUARIO_INVITADO = "Invitado";
            #region MAIL_ENCUESTA
            public const String ESTADO_ENVIADO = "ENV";
            public const String ESTADO_SIN_ENVIAR = "SEN";
            public const String ESTADO_NO_ENVIADO = "NOE";
            #endregion
            #region TIPO_EXTENSION
            public const String PDF = ".pdf";
            public const String XLS = ".xls";
            public const String DOC = ".doc";
            #endregion
            #region TIPO_ENCUESTA
            public const String PPP = "PPP";
            public const String FDC = "FDC";
            public const String GRA = "GRA";
            public const String EVD = "EVD";
            public const String RD = "RD";
            public const String ARD = "ARD";
            public const String HRD = "HRD";
            public const String COM = "COM";
            #endregion
            #region ESTADO_ENCUESTA
            public const String ESTADO_ACTIVO = "ACT";
            public const String ESTADO_INACTIVO = "INA";
            public const String ESTADO_ENTREGADO = "Entregado";
            public const String ESTADO_DEVUELTO = "Devuelto";
            public const String ESTADO_POR_ENTREGAR = "Por Entregar";
            #endregion
            #region TIPO_OUTCOME
            public const String COMPETENCIA_ESPECIFICA = "COMPETENCIAS ESPECIFICAS";
            public const String COMPETENCIA_GENERAL = "COMPETENCIAS GENERALES";
            public const String STUDENT_OUTECOME = "SO";
            public const String PREGUNTA_ADICIONAL = "PA";
            #endregion
            #region TIPO_REPORTE
            public const String PERCEPCION_PROMEDIO = "PPRO";
            public const String HISTORICO_PPP_SO = "HPSO";
            public const String RESULTADO_ENCUESTA = "RENC";
            public const String HISTORICO_RESULTADO_ENCUESTA = "HREN";
            public const String PERCEPCION_CG = "PESG";
            public const String PERCEPCION_SO = "PESO";
            public const String PROMEDIO_CG = "PRSG";
            public const String PROMEDIO_SO = "PRSO";
            public const String HISTORICO_SO = "HISO";
            #endregion
            #region NIVELES_ACEPTACION
            public const Double PPP_ROJO = 2.5;
            public const Double PPP_AMARILLO = 3.2;
            public const Double PPP_VERDE = 4;

            public const Double FDC_ROJO = 6.5;
            public const Double FDC_AMARILLO = 8.5;
            public const Double FDC_VERDE = 10;

            public const Double GRA_ROJO = 3.25;
            public const Double GRA_AMARILLO = 4.25;
            public const Double GRA_VERDE = 5;
            #endregion
            #region COMISIONES
            public const String COMISION_EAC = "EAC";
            public const String COMISION_CAC = "CAC";
            public const String COMISION_CAC_CC = "CAC-CC";
            public const String COMISION_CIC = "CIC";
            public const String COMISION_WASC = "WASC";
            #endregion
        }
        public static String GetEstadoEmail(String estado)
        {
            switch (estado)
            {
                case ConstantHelpers.ENCUESTA.ESTADO_ENVIADO: return "Enviado";
                case ConstantHelpers.ENCUESTA.ESTADO_SIN_ENVIAR: return "Sin Enviar";
                case ConstantHelpers.ENCUESTA.ESTADO_NO_ENVIADO: return "No Enviado";
            }
            return String.Empty;
        }
        public static String GetIconPorExtension(String extension)
        {
            switch (extension)
            {
                case ".pdf": return "fa fa-file-pdf-o";
                case ".xls": return "fa fa-file-excel-o";
                case ".doc": return "fa fa-file-word-o";
            }
            return String.Empty;
        }
        public static String GetColorPorCodigoComision(String Codigo)
        {
            switch (Codigo)
            {
                case "EAC":
                    return "badge badge-warning";
                case "CAC":
                    return "badge badge-info";
                case "WASC":
                    return "badge badge-primary";
            }
            return "badge badge-success";
        }
        public static String GetTipoEncuesta(String Tipo)
        {
            switch (Tipo)
            {
                case ConstantHelpers.ENCUESTA.PPP: return "Práctica Pre Profesional";
                case ConstantHelpers.ENCUESTA.FDC: return "Fin de Ciclo";
                case ConstantHelpers.ENCUESTA.GRA: return "Graduando";
            }
            return String.Empty;
        }
        public static String GetSpanEstadoFDC(String Acronimo, String Estado)
        {
            switch (Acronimo)
            {
                case "PENT": return "<h4><span class='label label-danger'>" + Estado + "</span></h4>";
                case "ENTR": return "<h4><span class='label label-warning'>" + Estado + "</span></h4>";
                case "DEVU": return "<h4><span class='label label-success'>" + Estado + "</span></h4>";
            }
            return String.Empty;
        }

        public static String GetSpanEstado(String Acronimo, String Estado)
        {
            switch (Acronimo)
            {
                case "INA": return "<h4><span class='label label-warning'>" + Estado + "</span></h4>";
                case "ACT": return "<h4><span class='label label-success'>" + Estado + "</span></h4>";
            }
            return String.Empty;
        }
        #endregion

        #region QUERY STRING PARAMETER
        public static class QueryStringParameters
        {
            public const string ACCREDITATION_TYPE_ID = "AccreditationTypeId";
            public const string COMMISSION_ID = "CommissionId";
            public const string CAREER_ID = "CareerId";
            public const string CAMPUS_ID = "CampusId";
            public const string CYCLE_ID = "CycleId";
            public const string CYCLE_FROM_ID = "CycleFromId";
            public const string CYCLE_TO_ID = "CycleToId";
            public const string CONSTITUENT_ID = "ConstituentId";
            public const string COURSE_ID = "CourseId";
            public const string LEVEL_ID = "LevelId";
            public const string STUDENT_OUTCOME_ID = "StudentOutcomeId";
            public const string ACCEPTANCE_LEVEL_ID = "AcceptanceLevelId";
            public const string INSTRUMENT_ID = "InstrumentId";
            public const string LANGUAGE_CULTURE = "LanguageCulture";
            public const string COLLEGE_ID = "CollegeId";
            public const string SUBMODALIDAD_ID = "ModalidadId";
            public const string MODULO_ID = "ModuloId";
            public const string DELEGATEDVIRTUALSURVEY_ID= "DelegateVirtualSurveyId";
            public const string SUBMODALIDADID = "SubModalidadId";
            public const string CODIGOCOMISION = "CodComision";
            public const string CARRERA_ID = "IdCarrera";
            public const string ANIO = "Anio";
            public const string MODALIDAD_ID = "Modalidad";
            public const string COSTITUYENTE_ID = "IdCostituyente";
            public const string INSTRUMENTO_ID = "IdInstrumento";
            public const string LENGUAJEID = "IdIdioma";
            public const string CURSOID = "IdCurso";
            public const string PLANMEJORA_ID = "IdPlanMejora";

        }
        #endregion

        #region RUBRIC       
        //public static string TEMP_PATH_LOADS_RUBRICS = "C:/TemporalSA/";
        public static string TEMP_PATH_LOADS_RUBRICS = WebConfigurationManager.AppSettings["TempPath"].ToString();
        public static string TEMP_PATH_REPORTS_RUBRICS = "~/Areas/Rubric/Resources/Files/Templates/";
        public static string TEMP_PATH_REPORTS_ARD = "~/Areas/Meeting/Resources/Files/Templates/";
        public static string TEMP_PATH_REPORTS_ASISTENCIA_ALUMNOS_ARD = "~/Areas/Meeting/Resources/Files/Templates/";
        public static string TEMPLATE_NOTAS_FINALES = "NotaFinCicloTemplate.xlsx";
        public static string TEMPLATE_ALUMNO_OUTCOME = "OutcomeAlumnoTemplate.xlsx";
        public static string TEMPLATE_OUTCOME_EVALUADOR = "OutcomeEvaluadorTemplate.xlsx";
        public static string TEMPLATE_OUTCOME_ZIP = "OutcomeZipTemplate.xlsx";
        public static string TEMPLATE_RUBRIC_COURCE = "RUBRICA_CURSO_TEMPLATE_V2.xlsx";
        public static string TEMPLATE_RUBRIC_COURCE_CAPSTONE = "RUBRICA_CURSO_CAPSTONE_TEMPLATE.xlsx";
        public static string TEMPLATE_CALIFICACION_COURCE = "CALIFICACION_CURSO_TEMPLATE.xlsx";
        public static string TEMPLATE_OUTCOME_EAC_CAC_ZIP = "OutcomeEAC_CACZipTemplate.xlsx";
        public static string TEMPLATE_ASISTENCIA_ALUMNOS_ARD = "AsistenciaAlumnosTemplate.xlsx";
        public static string TEMPLATE_ARD_CARRERA_CURSO = "ArdCarreraCursoTemplate.xlsx";
        public static string TEMPLATE_COMPETENCE_ZIP = "CompetenceZipTemplate.xlsx";
        public static string TEMPLATE_STATUS_REPORT = "TEMPLATE_REPORTE_STATUS.xlsx";
        public static string PROYECTOS_ACADEMICOS_CARGA_MASIVA = "7.PROYECTOS ACADEMICOS CARGA MASIVA.xlsx";
        public static string ROL_GERENTE = "GER";
        public static string COMISON_WASC = "WASC";
        public static string ACREDITADORA_WASC = "WASC";
        public static string ACREDITADORA_ABET = "ABET";
        public static string EVALUACION_PARTICIPACION = "PA";
        public static string ESTADO_RETIRADO = "RTE";
        public static int NIVEL_REUNIONES_COMITE = 1;
        public static string TIPO_PRE_AGENDA_REUNION_COMITE = "COC";

        public static class RUBRICFILETYPE
        {
            public static class ExcelType
            {
                public const String XLS = ".xls";
                public const String XLSX = ".xlsx";
            }

            public static class MessageResult
            {
                public const String EXCEL_PROCESADO = "EXCEL PROCESADO";
                public const String EXCEL_NO_PROCESADO = "EXCEL NO PROCESADO";
                public const String NO_EXISTE_NOTA = "No existe nota registrada";
                public const String ERROR_FORMATO = "Error de formato de Excel";
                public const String ERROR_EXCEPCION = "ERROR DE EXCEPCION";
            }

            public static class EvaluatorType
            {
                public const String COMITE = "COM";
                public const String GERENTE = "GER";
                public const String CLI = "CLI";
                public const String COA = "COA";
            }

            public static class EvaluationType
            {
                public const String PA = "PA";
                public const String TF = "TF";
                public const String TP = "TP";
            }

            public static class WascCommission
            {
                public const String WASC = "WASC";
                public const Int32 WASC_GOALS = 3;
            }

            public static List<String> ListEvaluationProject()
            {
                return new List<String> { "TF", "PA" };
            }
            /*
            public static List<String> ListNameCourseProject()
            {
                return new List<String> { "TP1", "TP2" };
            }

            public static List<String> ListNameCourseProjectFullname()
            {
                return new List<String> { "Taller De Proyecto I", "Taller De Proyecto II" };
            }*/

            /*
            public static class CourseProject
            {
                public const string CODTP1 = "SI408";
                public const string CODTP2 = "SI410";
                public const String TP1 = "Taller De Proyecto I";
                public const String TP2 = "Taller De Proyecto II";
                public const String TP1_ALTERNATIVO = "Taller De Proyecto 1";
                public const String TP2_ALTERNATIVO = "Taller De Proyecto 2";
            }*/
            /*
            public static List<String> ListCourseProjectFullName()
            {
                return new List<String>
                {
                    "Taller De Proyecto 1", "Taller De Proyecto 2",
                    "Taller De Proyecto I", "Taller De Proyecto II"
                };
            }*/

            public static class VirtualCompanyFile
            {
                public const String CODIGO = "CÓDIGO";
                public const String NOMBRE = "NOMBRE";
                public const String DESCRIPCION = "DESCRIPCIÓN";
            }

            public static List<String> ListVirtualCompanyHeaders()
            {
                return new List<String>
                {
                    "CÓDIGO",
                    "NOMBRE",
                    "DESCRIPCIÓN"
                };
            }

            public static class AcademicProjectFile
            {
                public const String CODIGO = "CÓDIGO";
                public const String CURSO = "CURSO";
                public const String NOMBRE = "NOMBRE";
                public const String DESCRIPCION = "DESCRIPCIÓN";
                public const String CODIGO_EMPRESA = "CÓDIGO EMPRESA";
                public const String ALUMNO_01 = "ALUMNO 01";
                public const String ALUMNO_02 = "ALUMNO 02";
                public const String CODIGO_CLIENTE = "CLIENTE";
                public const String CODIGO_COAUTOR = "COAUTOR";
                public const String CODIGO_GERENTE = "GERENTE";
            }

            public static List<String> ListaAcademicProjectsHeaders()
            {
                return new List<String>
                {
                    "CÓDIGO",
                    "NOMBRE",
                    "CURSO",
                    "DESCRIPCIÓN",
                    "CÓDIGO EMPRESA",
                    "ALUMNO 01",
                    "ALUMNO 02",
                    "COAUTOR",
                    "CLIENTE",
                    "GERENTE"
                };
            }

            public static class EvaluationTypeFile
            {
                public const String TIPO_EVALUACION = "TIPO EVALUACIÓN";
            }

            public static List<String> ListEvaluationTypeHeaders()
            {
                return new List<String>
                {
                    "TIPO EVALUACIÓN"
                };
            }

            public static class EvaluationFile
            {
                public const String CURSO = "CURSO";
                public const String CARRERA = "CARRERA";
                public const String TIPO_EVALUACION = "TIPO EVALUACIÓN";
                public const String NOTA = "NOTA";
            }

            public static List<String> ListEvaluationHeaders()
            {
                return new List<String>
                {
                    "CURSO",
                    "CARRERA",
                    "TIPO EVALUACIÓN",
                    "NOTA"
                };
            }
            public static class CourseToEvaluationFile
            {
                public const String CODIGO = "CÓDIGO CURSO";
            }
            public static List<String> ListCourseToEvaluation()
            {
                return new List<String>
                {
                    "CÓDIGO CURSO"
                };
            }


            public static class EvaluatorTypeFile
            {
                public const String CODIGO = "CÓDIGO";
                
                public const String PESO = "ESROLCOMITE";

                public const String NOMBRE = "NOMBRE";

                public const String CURSO = "CÓDIGOCURSO";
            }

            public static List<String> ListEvaluatorTypeHeaders()
            {
                return new List<String>
                {
                    //"NOMBRE",
                    "CÓDIGO",
                    "ESROLCOMITE",
                    "CÓDIGOCURSO"
                };
            }

            public static List<String> ListEvaluatorTypeMasterHeaders()
            {
                return new List<String>
                {
                    "CÓDIGO",
                    "NOMBRE"
                };
            }
            

            public static class EvaluatorFile
            {
                public const String CODIGO_DOCENTE = "CÓDIGO DOCENTE";
                public const String CODIGO_PROYECTO = "CÓDIGO PROYECTO";
                public const String TIPO_EVALUADOR = "CÓDIGO TIPO EVALUADOR";
            }

            public static List<String> ListEvaluatorHeaders()
            {
                return new List<String>
                {
                    "CÓDIGO DOCENTE",
                    "CÓDIGO PROYECTO",
                    "CÓDIGO TIPO EVALUADOR"
                };
            }

            public static class OutcomeGoalCommissionFile
            {
                public const String COMPETENCIA_GENERAL = "COMPETENCIA GENERAL";
                public const String LOGRO_ESPERADO = "LOGRO ESPERADO";
                public const String LOGRO_SOBRESALIENTE = "LOGRO SOBRESALIENTE";
                public const String LOGRO_EJEMPLAR = "LOGRO EJEMPLAR";
            }

            public static List<String> ListOutcomeGoalCommissionHeaders()
            {
                return new List<String>
                {
                    "COMPETENCIA GENERAL",
                    "LOGRO ESPERADO",
                    "LOGRO SOBRESALIENTE",
                    "LOGRO EJEMPLAR"
                };
            }

            public static class GrupoComiteFile
            {
                public const String CODIGO_DOCENTE = "CÓDIGO DOCENTE";
            }
            public static List<String> ListGrupoComiteHeaders()
            {
                return new List<String>{
                    "CÓDIGO DOCENTE"
                };
            }

            public static class Comission
            {
                public const String EAC = "EAC";
                public const String CAC = "CAC";
                public const String WASC = "WASC";
            }

            public static class Career
            {
                public const String SWF = "SW";
                public const String ISI = "SI";
                public const String CC = "CC";
            }
        }

        #endregion

        #region UPLOAD
        public static class UPLOAD
        {
            public static class ERRORES
            {
                public const string NOMBRE_HOJA = "Hoja1";
            }

            public static class FILE_DOWNLOAD
            {
                public static string PATH_TEMPLATES = "~/Areas/Upload/Resources/Templates/";
            }

            public static class OUTCOMES
            {

                public const string CODIGO_OUTCOME = "CÓDIGO OUTCOME";
                public const string NOMBRE_OUTCOME = "NOMBRE OUTCOME";
                public const string NOMBRE_OUTCOME_INGLES = "NOMBRE INGLÉS";
                public const string DESCRIPCION = "DESCRIPCIÓN";
                public const string DESCRIPCION_INGLES = "DESCRIPCIÓN INGLÉS";
                public const string COMISION = "COMISIÓN";
            }
            public static class SECCION
            {
                public const string CODIGO_CURSO = "codigocurso";
                public const string SECCIONES = "seccion";
                public const string DOCENTE = "docente";
                public const string LOCAL = "local";
            }
            public static class ORGANIGRAMA
            {
                public const string ID = "ID";
                public const string NIVEL = "NIVEL";
                public const string UNIDAD_ACADEMICA = "UNIDADACADEMICA";
                public const string NOMBRE_INGLES = "NOMBRE INGLES";
                public const string NOMBRE_USUARIO = "NOMBRE USUARIO";
                public const string PADRE = "PADRE";
                public const string SEDE = "SEDE";
                public const string CARRERA = "CARRERA";
                public const string CURSO = "CURSO";
                public const string TIPO = "TIPO";
                public const string ESCUELA = "ESCUELA";
            }
            public static class DOCENTES
            {
                public const string NOMBRE_DE_USUARIO = "NOMBRE DE USUARIO";
                public const string NOMBRE_COMPLETO_DEL_DOCENTE = "NOMBRE COMPLETO DEL DOCENTE";
            }
            public static class DELEGADOS
            {
                public const string CODIGO_CURSO = "CÓDIGO CURSO";
                public const string CODIGO_SECCION = "CÓDIGO SECCIÓN";
                public const string CODIGO_ALUMNO = "CÓDIGO ALUMNO";
                public const string NOMBRE_ALUMNO = "NOMBRE ALUMNO";
            }
            public static class CURRICULA
            {
                public const string CODIGO_CURRICULA = "CODIGO CURRICULA";
                public const string CARRERA = "CARRERA";
                public const string NIVEL = "NIVEL";
                public const string CODIGO_DE_CURSO = "CODIGO DE CURSO";
                public const string NOMBRE_DE_CURSO = "NOMBRE DE CURSO";
                public const string NOMBRE_EN_INGLES = "NOMBRE EN INGLES";
                public const string REQUISITOS = "REQUISITOS";
                public const string LOGRO = "LOGRO";
                public const string LOGRO_INGLES = "LOGRO INGLES";
                public const string CAMPO_EVALUACION = "CAMPO EVALUACIÓN";
                public const string ES_ELECTIVO = "ES ELECTIVO";
                public const string NOMBRE_MALLA = "NOMBRE MALLA";
            }
            public static class ALUMNOS_MATRICULADOS
            {
                public const string CODIGO_ALUMNO = "CÓDIGO ALUMNO";
                public const string NOMBRE_COMPLETO = "NOMBRE COMPLETO";
                public const string CARRERA = "CARRERA";
                public const string ESTADO_MATRICULA = "ESTADO MATRICULA";
                public const string SEDE = "SEDE";
            }
            public static class ALUMNOS_POR_SECCION
            {
                public const string CODIGO_CURSO = "codigo_curso";
                public const string CODIGO_SECCION = "codigo_seccion";
                public const string CODIGO_ALUMNO = "codigo_alumno";
            }

            public static class ATTRITION
            {
                public const string BAJA = "BAJA";
                public const string CAMBIO_DE_MODALIDAD = "CAMBIO DE MODALIDAD";
                public const string DESERCION = "DESERCION";
                public const string RESERVA = "RESERVA";
                public const string SOLO_INGLES = "SOLO INGLES";
                public const string TOTAL_ATTRITION = "TOTAL ATTRITION";
                public const string CICLO = "CICLO";
                public const string CARRERA = "CARRERA";
                public const string TOTAL_MATRICULADOS = "TOTAL MATRICULADOS";
            }

            public static class EVALUACIONES
            {
                public const string CODIGO_CURSO = "codigocurso";
                public const string CODIGO_SECCION = "codigoseccion";
                public const string CODIGO_ALUMNO = "codigoalumno";
                public const string NOTA = "nota";
            }

            public static class COHORTE
            {
                public const string CODIGO_ALUMNO = "Código";
                public const string NOMBRES_Y_APELLIDOS = "Nombres y apellidos";
                public const string ESTUDIO = "Estudio";
                public const string PROMOCION_EGRESO = "Promoción Egreso";
                public const string NUMERO_CICLOS_MATRICULADOS = "Ciclos Matriculados";
                public const string DIRECCION = "Dirección";
                public const string DISTRITO = "Distrito";
                public const string EMAIL = "Email";
                public const string TELEFONOS = "Teléfonos";
            }

            public static class ASISTENCIA_DOCENTES
            {
                public const string NOMBRE_USUARIO = "Nombre Usuario";
                public const string TOTAL_CLASES = "Total Clases";
                public const string TOTAL = "% Total";
                public const string LISTA = "Lista";
                public const string CLASES_INASISTIDAS = "Clases inasistidas";
                public const string RECUPERACIONES_ASISTIDAS = "Recuperaciones asistidas";
                public const string RECUPERACIONES_INASISTIDAS = "Recuperaciones inasistidas";
                public const string ADICIONALES_ASISTIDAS = "Adicionales asistidas";
                public const string ADICIONALES_INASISTIDAS = "Adicionales inasistidas";
                public const string ADELANTOS_ASISTIDOS = "Adelantos asistidos";
                public const string ADELANTOS_INASISTIDOS = "Adelantos inasistidos";
            }

            public static class COHORTE_INGRESANTES
            {
                public const string CICLO = "ciclo";
                public const string CANTIDAD_INGRESANTES = "cantidad de ingresantes";
                public const string CANTIDAD_GRADUADOS = "cantidad de graduados";
                public const string CODIGO_CARRERA = "carrera";
            }

            public static class PUNTUALIDAD_DOCENTES
            {
                public const string DOCENTES = "Docentes";
                public const string MINUTOS_TARDANZA = "Minutos";
            }

            public static class TITULADOS
            {
                public const string CODIGO = "Código";
                public const string NOMBRES_Y_APELLIDOS = "Nombres y apellidos";
                public const string ESTUDIO = "Estudio";
                public const string FECHA_RESOLUCION = "Fecha de resolución";
            }
        }

        public static class UPLOAD_TYPE
        {
            public const string OUTCOME = "Outcome";
            public const string ORGANIGRAMA = "Organigrama";
            public const string DOCENTE = "Docente";
            public const string MALLA_CURRICULAR = "Malla";
            public const string ALUMNOS_MATRICULADOS = "AlumnosMatriculados";
            public const string SECCION = "Seccion";
            public const string ALUMNOS_POR_SECCION = "AlumnoSeccion";
            public const string DELEGADOS = "Delegados";
            public const string ATTRITION = "Attrition";
            public const string COHORTE = "Cohorte";
            public const string ASISTENCIA_DOCENTES = "AsistenciaDocentes";
            public const string EVALUACIONES = "Evaluaciones";
            public const string COHORTE_INGRESANTES = "CohorteIngresantes";
            public const string PUNTUALIDAD_DOCENTES = "PuntualidadDocentes";
            public const string TITULADOS = "Titulados";
        }

        public static class UPLOAD_FILE_NAME
        {
            public const string OUTCOME = "Outcome.xlsx";
            public const string ORGANIGRAMA = "Organigrama.xlsx";
            public const string DOCENTE = "Docente.xlsx";
            public const string MALLA_CURRICULAR = "Malla.xlsx";
            public const string ALUMNOS_MATRICULADOS = "AlumnosMatriculados.xlsx";
            public const string SECCION = "Seccion.xlsx";
            public const string ALUMNOS_POR_SECCION = "AlumnoSeccion.xlsx";
            public const string DELEGADOS = "Delegados.xlsx";
            public const string ATTRITION = "Attrition.xlsx";
            public const string COHORTE = "Cohorte.xlsx";
            public const string ASISTENCIA_DOCENTES = "AsistenciaDocentes.xlsx";
            public const string EVALUACIONES = "Evaluaciones.xlsx";
            public const string COHORTE_INGRESANTES = "CohorteIngresantes.xlsx";
            public const string PUNTUALIDAD_DOCENTES = "PuntualidadDocentes.xlsx";
            public const string TITULADOS = "Titulados.xlsx";
        }
        #endregion

        #region INDICATOR
        public static class INDICATOR_CHART_TYPE
        {
            public const string IN_TIME_SCHEDULE = "InTimeSchedule";
            public const string ATTRITION = "Attrition";
            public const string IN_TIME_SYLLABUS = "InTimeSyllabus";
            public const string IN_TIME_VIRTUAL_CLASS = "InTimeVirtualClass";
            public const string HIRED_PROFESSORS = "HiredProfessors";
            public const string IN_TIME_CD = "InTimeCD";
            public const string IN_TIME_FINAL_EXAM = "InTimeFinalExam";
            public const string IN_TIME_MID_TERM_EXAM = "InTimeMidTermExam";
            public const string IN_TIME_MAKE_UP_EXAM = "InTimeMakeUpExam";
            public const string CORRECT_FINAL_EXAM = "CorrectFinalExam";
            public const string CORRECT_MID_TERM_EXAM = "CorrectMidTermExam";
            public const string CORRECT_MAKE_UP_EXAM = "CorrectMakeUpExam";
            public const string PROFESSOR_ATTENDANCE = "ProfessorAttendance";
            public const string STUDENT_PERIOD = "StudentPeriod";
        }
        public static class INDICATOR_TABLE_TYPE
        {
            public const string IN_TIME_SCHEDULE = "InTimeSchedule";
            public const string IN_TIME_SYLLABUS = "InTimeSyllabus";
            public const string ATTRITION = "Attrition";
            public const string IN_TIME_VIRTUAL_CLASS = "InTimeVirtualClass";
            public const string HIRED_PROFESSORS = "HiredProfessors";
            public const string IN_TIME_CD = "InTimeCD";
            public const string IN_TIME_FINAL_EXAM = "InTimeFinalExam";
            public const string IN_TIME_MID_TERM_EXAM = "InTimeMidTermExam";
            public const string IN_TIME_MAKE_UP_EXAM = "InTimeMakeUpExam";
            public const string CORRECT_FINAL_EXAM = "CorrectFinalExam";
            public const string CORRECT_MID_TERM_EXAM = "CorrectMidTermExam";
            public const string CORRECT_MAKE_UP_EXAM = "CorrectMakeUpExam";
            public const string PROFESSOR_HOURS = "ProfessorHours";
            public const string PROFESSOR_ATTENDANCE = "ProfessorAttendance";
            public const string PROFESSOR_ATTENDANCE_RANKING = "ProfessorAttendanceRanking";
            public const string PROFESSOR_ATTENDANCE_RANGE = "ProfessorAttendanceRange";
            public const string PROFESSOR_PUNCTUALITY = "ProfessorPunctuality";
            public const string PROFESSOR_PUNCTUALITY_RANKING = "ProfessorPunctualityRanking";
            public const string PROFESSOR_PUNCTUALITY_RANGE = "ProfessorPunctualityRange";
            public const string CERTIFICATED_STUDENTS = "CertificatedStudents";
            public const string MEETINGS = "Meetings";
            public const string ATTRITION_RATIO = "AttritionRatio";
			public const string STUDENT_CAMPUS = "StudentCampus";
            public const string STUDENT_SECTION = "StudentSection";
            public const string STUDENT_PERIOD = "StudentPeriod";
            public const string STUDENT_COHORT = "StudentCohort";
        }

        public static class INDICATOR_REPORT_TYPE
        {
            public const string ATTRITION_LEVEL = "AttritionLevel";
            public const string ATTRITION_MODALITY = "AttritionModality";
            public const string ATTRITION_RATIO = "AttritionRatio";
            public const string CORRECT_EXAMS = "CorrectExams";
            public const string HIRED_PROFESSORS = "HiredProfessors";
            public const string IN_TIME_CD = "InTimeCD";
            public const string IN_TIME_EXAMS = "InTimeExams";
            public const string IN_TIME_SCHEDULE = "InTimeSchedule";
            public const string IN_TIME_SYLLABUS = "InTimeSyllabus";
            public const string IN_TIME_VIRTUAL_CLASS = "InTimeVirtualClass";
            public const string PROFESSOR_ATTENDANCE = "ProfessorAttendance";
            public const string COHORT = "Cohort";
            public const string PROFESSOR_HOURS = "ProfessorHours";
            public const string PROFESSOR_PUNCTUALITY = "ProfessorPunctuality";
            public const string CERTIFICATED_STUDENTS = "CertificatedStudents";
            public const string MEETINGS = "Meetings";
			public const string STUDENT_CAMPUS = "StudentCampus";
            public const string STUDENT_SECTION = "StudentSection";
            public const string STUDENT_PERIOD = "StudentPeriod";
            public const string STUDENT_COHORT = "StudentCohort";
        }

        public static class INDICATOR_DATA_TYPE
        {
            public const string SYLLABUS = "silabo";
            public const string VIRTUAL_CLASS = "aulaVirtual";
            public const string CD = "CD";
            public const string FINAL_EXAM = "examenFinal";
            public const string MID_TERM_EXAM = "examenParcial";
            public const string MAKE_UP_EXAM = "examenRecuperacion";
            public const string CORRECT_FINAL_EXAM = "calidadFinal";
            public const string CORRECT_MID_TERM_EXAM = "calidadParcial";
            public const string CORRECT_MAKE_UP_EXAM = "calidadRecuperacion";
            public const string HAS_FINAL = "tieneFinal";
            public const string HAS_MID_TERM = "tieneParcial";
        }
        #endregion

        #region MEETING
        public static class PREAGENDA
        {
            public static class TIPO_PREAGENDA
            {
                public static class REUNION_COORDINACION
                {
                    public const string CODIGO = "RCO";
                    public const string TEXTO = "Reunión de Coordinación";
                }
                public static class REUNION_COMITE_CONSULTIVO
                {
                    public const string CODIGO = "RCC";
                    public const string TEXTO = "Reunión de Comité Consultivo";
                }
            }
            public static class TEMA_PREAGENDA
            {
                public static class ESTADOS
                {
                    public static class REALIZADO
                    {
                        public const string CODIGO = "REALI";
                        public const string TEXTO = "Realizado";
                    }
                    public static class PENDIENTE
                    {
                        public const string CODIGO = "PENDI";
                        public const string TEXTO = "Pendiente";
                    }
                }
            }


        }
        public static class NIVEL_UNIDAD_ACADEMICA
        {
            public const int ESCUELA = 0;
            public const int AREA = 1;
            public const int SUBAREA = 2;
            public const int COORD_CURSO = 3;
        }

        public static class FRECUENCIA_REUNION
        {
            public static class UNICA
            {
                public const string CODIGO = "UNICA";
                public const string TEXTO = "Única";
                public const string TEXTOINGLES = "Unique";
            }

            public static class SEMANAL
            {
                public const string CODIGO = "SEMANAL";
                public const string TEXTO = "Semanal";
                public const string TEXTOINGLES = "Weekly";
            }

            public static class QUINCENAL
            {
                public const string CODIGO = "QUINCENAL";
                public const string TEXTO = "Quincenal";
                public const string TEXTOINGLES = "Quince";

            }

            public static class MENSUAL
            {
                public const string CODIGO = "MENSUAL";
                public const string TEXTO = "Mensual";
                public const string TEXTOINGLES = "Mensual";

            }
        }
        #endregion

        #region PROFESSOR

        public static class PROFESSOR
        {
            public const string SERVER_FILES_DIRECTORY = "~/Files\\";
            public const string MALLA_COCOS_DIRECTORY = "GED\\MallaCocos";
            public const string IFC_DIRECTORY = "GED\\IFC";
            public const string PLAN_ACCION_DIRECTORY = "GED\\PlanAccion";
            public const string CARGA_MASIVA_IFC_DIRECTORY = "GED\\IFC_CARGA_MASIVA";

            public const string ACTAS_DIRECTORY = "GED\\Actas";

            public const string PLANTILLA_IFC_DIRECTORY = "~/Areas\\Professor\\Resources\\Templates\\Excel";

            public const string COLOR_BOTON_ACEPTAR = "#2196f3";

            public static class CONSTITUYENTE
            {
                public const string DOCENTE = "Docente";
                public const string ESTUDIANTE = "Estudiante";
                public const string EMPLEADOR = "Empleador";
                public const string GRADUADOS = "Graduados";
            }


            public static class ACTAS
            {
                public static class TIPOS
                {
                    public static class ACTA_NORMAL
                    {
                        public const string CODIGO = "NOR";
                        public const string TEXTO = "Normal";
                    }
                    public static class ACTA_COMITE
                    {
                        public const string CODIGO = "COM";
                        public const string TEXTO = "Comité";
                    }
                }

                public static class ESTADOS
                {
                    public static class OK
                    {
                        public const string CODIGO = "APROBADA";
                        public const string TEXTO = "Aprobada";
                    }
                    public static class OBSERVADA
                    {
                        public const string CODIGO = "OBSERVADA";
                        public const string TEXTO = "Observada";
                    }
                    public static class FINALIZADO
                    {
                        public const string CODIGO = "FINALIZADA";
                        public const string TEXTO = "Finalizada";
                    }
                }

                public static class OBSERVACIONES
                {
                    public static class ESTADOS
                    {
                        public static class PENDIENTE
                        {
                            public const string CODIGO = "PEN";
                            public const string TEXTO = "Pendiente";
                            public const string TEXTOINGLES = "Pending";
                        }
                        public static class RECHAZADO
                        {
                            public const string CODIGO = "REC";
                            public const string TEXTO = "Rechazado";
                            public const string TEXTOINGLES = "Rejected";
                        }
                        public static class RESUELTO
                        {
                            public const string CODIGO = "RES";
                            public const string TEXTO = "Resuelto";
                            public const string TEXTOINGLES = "Resolved";
                        }
                        public static String GetEstadoObservacion(String codigo)
                        {
                            String TEXTO = "";
                            switch (codigo)
                            {
                                case "PEN": TEXTO += "Pendiente"; break;
                                case "REC": TEXTO += "Rechazado"; break;
                                case "RES": TEXTO += "Resuelto"; break;
                                default: TEXTO += "No Tiene Estado"; break;
                            }
                            return TEXTO;
                        }
                    }
                }

                public static class HALLAZGOS
                {

                    public static class NIVELES_DE_ACEPTACION
                    {
                        public static class NO_ASIGNADO
                        {
                            public const string CODIGO = "NO_ASIGNADO";
                            public const string TEXTO = "No Asignado";

                        }
                        public static class ESPERADO
                        {
                            public const string CODIGO = "ESPERADO";
                            public const string TEXTO = "Esperado";
                        }
                        public static class NECESITA_MEJORA
                        {
                            public const string CODIGO = "NECESITA_MEJORA";
                            public const string TEXTO = "Necesita mejora";
                        }
                        public static class SOBRESALIENTE
                        {
                            public const string CODIGO = "SOBRESALIENTE";
                            public const string TEXTO = "Sobresaliente";
                        }
                    }
                }

            }

            public static class FILTROS
            {
                public static String GetFiltroByInstrumentoId(Int32 InstrumentoId)
                {
                    if (InstrumentoId == 0)
                        InstrumentoId = 8;
                    String f = "_Filtros";
                    switch (InstrumentoId)
                    {
                        case 1: f += "IFC"; break;
                        case 2: f += "ACC"; break;
                        case 3: f += "PPP"; break;
                        case 4: f += "EG"; break;
                        case 5: f += "LFC"; break;
                        case 8: f += "IRD"; break;
                        default: f += "IFC"; break;
                    }
                    return f;
                }
                public static String GetVistaResultadoById(Int32 ResultadoId)
                {
                    String f = "_Resultado";
                    switch (ResultadoId)
                    {
                        case 1: f += "HallazgoIFC"; break;
                        case 2: f += "AccionMejora"; break;
                        default: f += "AccionMejora"; break;
                    }
                    return f;
                }
            }
            public static class IFC
            {
                public static class HALLAZGOS
                {
                    public static class NIVELES_DE_ACEPTACION
                    {
                        public static class NO_ASIGNADO
                        {
                            public const string CODIGO = "NO_ASIGNADO";
                            public const string TEXTO = "No Asignado";

                        }
                        public static class ESPERADO
                        {
                            public const string CODIGO = "ESPERADO";
                            public const string TEXTO = "Esperado";
                        }
                        public static class NECESITA_MEJORA
                        {
                            public const string CODIGO = "NECESITA_MEJORA";
                            public const string TEXTO = "Necesita mejora";
                        }
                        public static class SOBRESALIENTE
                        {
                            public const string CODIGO = "SOBRESALIENTE";
                            public const string TEXTO = "Sobresaliente";
                        }

                    }
                }

                public static class ACCIONES_MEJORA
                {
                    public static class ESTADOS
                    {
                        public static class PENDIENTE
                        {
                            public const string CODIGO = "PENDI";
                            public const string TEXTO = "Pendiente";
                        }
                        public static class IMPLEMENTADO
                        {
                            public const string CODIGO = "IMPLE";
                            public const string TEXTO = "Implementado";
                        }
                        public static class PROCESADO
                        {
                            public const string CODIGO = "PROCE";
                            public const string TEXTO = "Procesado";
                        }
                        public static class RECHAZADO
                        {
                            public const string CODIGO = "RECHA";
                            public const string TEXTO = "Rechazado";
                        }
                        public static String GetEstadoAccionMejora(String codigo)
                        {
                            String TEXTO = "";
                            switch (codigo)
                            {
                                case "PENDI": TEXTO += "Pendiente"; break;
                                case "IMPLE": TEXTO += "Implementado"; break;
                                case "PROCE": TEXTO += "Procesado"; break;
                                case "RECHA": TEXTO += "Rechazado"; break;
                                default: TEXTO += "No Tiene Estado"; break;
                            }
                            return TEXTO;
                        }
                    }
                }


                public static class ESTADOS
                {
                    public static class ESPERA
                    {
                        public const string CODIGO = "ESP";
                        public const string TEXTO = "Espera";
                        public const string TEXTOINGLES = "On hold";
                    }
                    public static class APROBADO
                    {
                        public const string CODIGO = "APR";
                        public const string TEXTO = "Aprobado";
                        public const string TEXTOINGLES = "Approved";
                    }
                    public static class OBSERVADO
                    {
                        public const string CODIGO = "OBS";
                        public const string TEXTO = "Observado";
                        public const string TEXTOINGLES = "Observed";
                    }
                    public static class ENVIADO
                    {
                        public const string CODIGO = "ENV";
                        public const string TEXTO = "Enviado";
                        public const string TEXTOINGLES = "Sent";
                    }
                }
            }

            public static class TEMA_PREAGENDA
            {
                public static class ESTADOS
                {
                    public static class REALIZADO
                    {
                        public const string CODIGO = "REALI";
                        public const string TEXTO = "Realizado";
                    }
                    public static class PENDIENTE
                    {
                        public const string CODIGO = "PENDI";
                        public const string TEXTO = "Pendiente";
                    }
                }
            }

            public static class TAREA
            {
                public static class ESTADOS
                {
                    public static class PENDIENTE
                    {
                        public const string CODIGO = "PEN";
                        public const string TEXTO = "Pendiente";
                    }

                    public static class REALIZADO
                    {
                        public const string CODIGO = "REA";
                        public const string TEXTO = "Realizado";
                    }
                }
            }

            public const string EVIDENCE_DIRECTORY = "EVIDENCIAS";
        }
        #endregion

        #region INSTRUMENTOS
        public static class INSTRUMENTOS
        {
            public const int IFC = 1;
            public const int ACC = 2;
            public const int PPP = 3;
            public const int GRA = 4;
            public const int LCFC = 5;
            public const int RC = 6;
            public const int RV = 7;
            public const int ARD = 8;
        }

        public static class INSTRUMENTOS_ACRONIMOS
        {
            public const string IFC = "IFC";
            public const string ACC = "ACC";
            public const string PPP = "PPP";
            public const string GRA = "GRA";
            public const string LCFC = "LCFC";
            public const string RC = "RC";
            public const string RV = "RV";
            public const string ARD = "ARD";
        }
        #endregion
    }
}
