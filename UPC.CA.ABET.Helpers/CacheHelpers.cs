﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace UPC.CA.ABET.Helpers
{
    public static class CacheHelper
    {
        #region Get & Set
        public static T Get<T>(string key) where T : class
        {
            try
            {
                var memoryCache = MemoryCache.Default;
                return (T)memoryCache.Get(key);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static T Get<T>(CacheKey key) where T : class
        {
            return Get<T>(key.ToString());
        }

        public static bool Set<T>(T o, string key, Int32 expirationMinutes = 60) where T : class
        {
            var cacheTime = AppSettingsHelper.GetAppSettings("CacheExpirationMinutes", expirationMinutes.ToString()).ToInteger();
            var expiratioTime = new DateTimeOffset(DateTime.Now.AddMinutes(cacheTime));

            var memoryCache = MemoryCache.Default;
            return memoryCache.Add(key, o, expiratioTime);
        }
        public static bool Set<T>(T o, CacheKey key) where T : class
        {
            return Set<T>(o, key.ToString());
        }

        public static bool Set(Object o, string key, Int32 expirationMinutes = 60)
        {
            var cacheTime = AppSettingsHelper.GetAppSettings("CacheExpirationMinutes", expirationMinutes.ToString()).ToInteger();
            var expiratioTime = new DateTimeOffset(DateTime.Now.AddMinutes(cacheTime));

            var memoryCache = MemoryCache.Default;
            return memoryCache.Add(key, o, expiratioTime);
        }
        public static bool Set(Object o, CacheKey key)
        {
            return Set(o, key.ToString());
        }
        #endregion
        
        public static bool Exists(string key)
        {
            var memoryCache = MemoryCache.Default;
            return memoryCache.Get(key) != null;
        }
        public static bool Exists(CacheKey key)
        {
            return Exists(key.ToString());
        }

        public static void Clear(string key)
        {
            var memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }
        public static void Clear(CacheKey key)
        {
            Clear(key.ToString());
        }

        #region Replaces
        public static void Replace<T>(T o, string key) where T : class
        {
            Clear(key);
            Set<T>(o, key);
        }
        public static void Replace<T>(T o, CacheKey key) where T : class
        {
            Replace<T>(o, key.ToString());
        }
        #endregion
    }
}
