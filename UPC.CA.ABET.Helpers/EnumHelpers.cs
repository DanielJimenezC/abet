﻿namespace UPC.CA.ABET.Helpers
{
    #region APP ROL
    public enum AppRol
    {
        Acreditador, 
        Administrador,
        Docente,
        ProfesorGerente,
        CoordinadorCarrera,
        DirectorCarrera,
        EncargadoCargas,
        Invitado,
        GerenteGeneral,
        AdministradorEscuela,
        CalificadorControl,
        MiembroComite,
        Usuario,
    }
    #endregion

    #region SESSION COOKIE KEY
    public enum SessionKey
    {
        UsuarioId,
        DocenteId,
        Nombre,
        Apellido,
        Codigo,
        Email,
        Culture,
        Rol,
        Roles,
        PeriodoAcademicoId,
        PeriodoAcademico,
        PeriodoEPEId,
        PeriodoEPE,
        Imagen,
        ExMessage,
        ExInnerMessage,
        ExTrace,
        ExUrlReferer,
        ExSource,
        ExTargetSite,
        EXStatusCode,
        ExStatusCode,
        UsuarioSecundarioId,
        CodigoSecundario,
        EscuelaId,
        NombreEscuela,
        DictRoles,
        SubModalidadPeriodoAcademicoId,
        SubModalidadPeriodoAcademico,
        Modality,
        ModalidadId,
        NombreModalidad,
		Token,
		TokenId,
		EncuestaTokenId,
		EncuestVirtualDelegadoId
	}

    public enum CacheKey
    {
        LstCarrera,
        LstEscuela,
        LstRol,
        LstModalidad
    }

    public enum CookieKey
    {
        UsuarioId,
        Nombre,
        Apellido,
        Codigo,
        Email,
        Culture,
        Rol,
        Modality
    }
    #endregion

    #region MESSAGE TYPE
    public enum MessageType
    {
        Success,
        Warning,
        Info,
        Error
    }
    public enum Theme
    {
        Dark,
        Light
    }
    #endregion

    #region PANEL ACTIONS
    public enum PanelActions
    {
        Collapse,
        Fullscreen,
        Refresh,
        Close
    }
    #endregion

    #region MODAL SIZE
    public enum ModalSize
    {
        Normal,
        Small,
        Large
    }
    #endregion

    #region FILE TYPE
    public enum FileType
    {
        PDF,
        XLS,
        XML
    }
    #endregion

    #region BULK INSERT TYPE

    public enum BulkInsertType
    {
        OutcomeGoalCommission,
        CoursesToEvaluation,
        VirtualCompany,
        AcademicProject,
        EvaluationType,
        EvaluatorType,
        Evaluation,
        Evaluator,
        EvaluatorTypeMaster,
        GrupoComite,
        RubricToEvaluation

    }

    #endregion

    #region REPORT TYPE

    public enum ReportType
    {
        RC,
        RV,
        RF,
        RH
    }

    #endregion
}
