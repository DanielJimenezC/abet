﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace UPC.CA.ABET.Helpers
{
    public static class HtmlHelpers
    {
        #region Html.Alert Helper
        /// <summary>
        /// Genera Mensaje de Alerta Html Boostrap
        /// </summary>
        /// <param name="type">Tipo de Alerta</param>
        /// <param name="style">Tema Alerta</param>
        /// <param name="message">Mensaje</param>
        /// <returns>MvcHtmlString</returns>
        public static MvcHtmlString Alert(this HtmlHelper Html, MessageType type, Theme style, string message)
        {
            var div = new TagBuilder("div");
            div.Attributes.Add("role", "alert");
            div.InnerHtml = String.Empty;

            var buttonDimiss = new TagBuilder("button");
            buttonDimiss.Attributes.Add("type", "button");
            buttonDimiss.Attributes.Add("data-dismiss", "alert");
            buttonDimiss.Attributes.Add("aria-label", "Close");
            buttonDimiss.AddCssClass("close");

            var span = new TagBuilder("span");
            span.Attributes.Add("aria-hidden", "true");
            span.InnerHtml = "×";

            buttonDimiss.InnerHtml = Html.Raw(span.ToString(TagRenderMode.Normal)).ToHtmlString();

            var i = new TagBuilder("i");
            var divClass = "alert alert-dismissible";
            if (Theme.Dark == style)
                divClass += " dark";

            switch (type)
            {
                case MessageType.Success:
                    divClass += " alert-success";
                    i.AddCssClass("fa fa-check");
                    break;
                case MessageType.Warning:
                    divClass += " alert-warning";
                    i.AddCssClass("fa fa-exclamation-triangle");
                    break;
                case MessageType.Info:
                    divClass += " alert-info";
                    i.AddCssClass("fa fa-info");
                    break;
                case MessageType.Error:
                    divClass += " alert-danger";
                    i.AddCssClass("fa fa-times");
                    break;
                default:
                    break;
            }

            div.AddCssClass(divClass);
            div.InnerHtml += Html.Raw(i.ToString(TagRenderMode.Normal) + "&nbsp;").ToHtmlString();
            div.InnerHtml += Html.Raw(buttonDimiss.ToString(TagRenderMode.Normal)).ToHtmlString();
            div.InnerHtml += Html.Raw(message.ToSafeString()).ToHtmlString();

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString AlertLight(this HtmlHelper Html, MessageType Type, string Message)
        {
            return Alert(Html, Type, Theme.Light, Message);
        }

        public static MvcHtmlString AlertDark(this HtmlHelper Html, MessageType Type, string Message)
        {
            return Alert(Html, Type, Theme.Dark, Message);
        }

        #endregion

        #region Html.Image Helper
        /// <summary>
        /// Devuelne un tag html <img />
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="src"></param>
        /// <param name="alt"></param>
        /// <param name="Class"></param>
        /// <returns></returns>
        public static MvcHtmlString Image(this HtmlHelper Html, string src, string alt, string Class = null)
        {
            var url = new UrlHelper(Html.ViewContext.RequestContext);
            var img = new TagBuilder("img");
            img.MergeAttribute("src", url.Content(src));
            img.MergeAttribute("alt", alt);
            img.AddCssClass(Class);
            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Image(this HtmlHelper Html, string src, string alt)
        {
            return Image(Html, src, alt, String.Empty);
        }

        /// <summary>
        /// Genera una imagen para un objeto 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var src = expression.Compile()(htmlHelper.ViewData.Model);
            return BuildImageTag(src.ToString(), null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var src = expression.Compile()(htmlHelper.ViewData.Model);
            return BuildImageTag(src.ToString(), htmlAttributes);
        }

        private static MvcHtmlString BuildImageTag(string src, object htmlAttributes)
        {
            TagBuilder img = new TagBuilder("img");
            img.Attributes.Add("src", src);
            if (htmlAttributes != null)
                img.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }
        #endregion

        #region Html.Content Helper
        /// <summary>
        /// Devuelve definiciones Html para definir estilos y scripts 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="contentRoute"></param>
        /// <param name="areaName"></param>
        /// <param name="isScripts"></param>
        /// <returns></returns>
        public static MvcHtmlString Content(this HtmlHelper html, string contentRoute, string areaName = null, bool isScripts = false)
         {
            TagBuilder tag;
            var url = new UrlHelper(html.ViewContext.RequestContext);
            var contentBase = "~/";
            if (isScripts)
                contentBase += !String.IsNullOrEmpty(areaName) ? "Areas/" + areaName + "/Scripts/" : "Scripts/";
            else
                contentBase += !String.IsNullOrEmpty(areaName) ? "Areas/" + areaName + "/Content/" : "Content/";

            if (contentRoute.EndsWith(".css"))
            {
                tag = new TagBuilder("link");
                tag.Attributes.Add("rel", "stylesheet");
                tag.Attributes.Add("href", url.Content(contentBase + contentRoute));
                tag.InnerHtml = String.Empty;
                return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
            }
            else if (contentRoute.EndsWith(".js"))
            {
                tag = new TagBuilder("script");
                tag.Attributes.Add("type", "text/javascript");
                tag.Attributes.Add("src", url.Content(contentBase + contentRoute));
                tag.InnerHtml = String.Empty;
                return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
            }
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString Content(this HtmlHelper html, string contentName, string areaName)
        {
            return Content(html, contentName, areaName, false);
        }
        public static MvcHtmlString Content(this HtmlHelper html, string contentName, bool isScripts)
        {
            return Content(html, contentName, String.Empty, isScripts);
        }
        public static MvcHtmlString Content(this HtmlHelper html, string contentName)
        {
            return Content(html, contentName, String.Empty, false);
        }
        #endregion

        #region Bootstraps Helper
        /// <summary>
        /// Devuelve la clásica estructura de un "from group" de bootstrap indicando el label y el control
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="LabelText"></param>
        /// <param name="FormControl"></param>
        /// <param name="Inline"></param>
        /// <returns></returns>
        public static MvcHtmlString FormGroup(this HtmlHelper Html, string LabelText, MvcHtmlString FormControl, bool Inline = false, MvcHtmlString ValidateMessage = null, int colMd = 6)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("form-group col-sm-12 col-md-" + colMd);
            div.InnerHtml = string.Empty;

            var label = new TagBuilder("label");
            label.AddCssClass("col-sm-" + (Inline ? "3" : "12") + " control-label");
            label.InnerHtml = Html.Raw(LabelText).ToHtmlString();

            div.InnerHtml += Html.Raw(label.ToString(TagRenderMode.Normal)).ToHtmlString();

            var divInput = new TagBuilder("div");
            divInput.AddCssClass("col-sm-" + (Inline ? "9" : "12"));
            divInput.InnerHtml = FormControl.ToHtmlString();

            div.InnerHtml += Html.Raw(divInput.ToString(TagRenderMode.Normal)).ToHtmlString();

            if (ValidateMessage != null)
            {
                divInput = new TagBuilder("div");
                divInput.AddCssClass("col-sm-12");
                divInput.InnerHtml = ValidateMessage.ToHtmlString();

                div.InnerHtml += Html.Raw(divInput.ToString(TagRenderMode.Normal)).ToHtmlString();
            }

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// Devuelve un panel header con opciones
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="Title"></param>
        /// <param name="PanelActions"></param>
        /// <returns></returns>
        public static MvcHtmlString PanelHeader(this HtmlHelper Html, string Title, params PanelActions[] panelActions)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("panel-heading");
            div.InnerHtml = string.Empty;

            var h3 = new TagBuilder("h3");
            h3.AddCssClass("panel-title");
            string[] id = Title.Split(new[] { "</i> " }, StringSplitOptions.None);
            h3.GenerateId(id[0]);
            h3.InnerHtml = Html.Raw(Title).ToHtmlString();

            div.InnerHtml += Html.Raw(h3.ToString(TagRenderMode.Normal)).ToHtmlString();

            var divActions = new TagBuilder("div");
            divActions.AddCssClass("panel-actions");
            divActions.InnerHtml = string.Empty;

            foreach (var action in panelActions)
            {
                TagBuilder a = null;

                switch (action)
                {
                    case PanelActions.Collapse:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-minus");
                        a.Attributes.Add("data-toggle", "panel-collapse");

                        break;
                    case PanelActions.Fullscreen:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-expand");
                        a.Attributes.Add("data-toggle", "panel-fullscreen");

                        break;
                    case PanelActions.Refresh:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-refresh");
                        a.Attributes.Add("data-toggle", "panel-refresh");

                        break;
                    case PanelActions.Close:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-close");
                        a.Attributes.Add("data-toggle", "panel-close");

                        break;
                    default:
                        break;
                }

                if (a != null)
                {
                    a.Attributes.Add("aria-hidden", "true");

                    divActions.InnerHtml += Html.Raw(a.ToString(TagRenderMode.Normal)).ToHtmlString();
                }
            }

            div.InnerHtml += Html.Raw(divActions.ToString(TagRenderMode.Normal)).ToHtmlString();

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }
        public static MvcHtmlString PanelHeaderNoIcon(this HtmlHelper Html, string Title, params PanelActions[] panelActions)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("panel-heading");
            div.InnerHtml = string.Empty;

            var h3 = new TagBuilder("h3");
            h3.AddCssClass("panel-title");
            h3.GenerateId(Title);
            h3.InnerHtml = Html.Raw(Title).ToHtmlString();

            div.InnerHtml += Html.Raw(h3.ToString(TagRenderMode.Normal)).ToHtmlString();

            var divActions = new TagBuilder("div");
            divActions.AddCssClass("panel-actions");
            divActions.InnerHtml = string.Empty;

            foreach (var action in panelActions)
            {
                TagBuilder a = null;

                switch (action)
                {
                    case PanelActions.Collapse:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-minus");
                        a.Attributes.Add("data-toggle", "panel-collapse");

                        break;
                    case PanelActions.Fullscreen:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-expand");
                        a.Attributes.Add("data-toggle", "panel-fullscreen");

                        break;
                    case PanelActions.Refresh:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-refresh");
                        a.Attributes.Add("data-toggle", "panel-refresh");

                        break;
                    case PanelActions.Close:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-close");
                        a.Attributes.Add("data-toggle", "panel-close");

                        break;
                    default:
                        break;
                }

                if (a != null)
                {
                    a.Attributes.Add("aria-hidden", "true");

                    divActions.InnerHtml += Html.Raw(a.ToString(TagRenderMode.Normal)).ToHtmlString();
                }
            }

            div.InnerHtml += Html.Raw(divActions.ToString(TagRenderMode.Normal)).ToHtmlString();

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString PanelHeaderIcon(this HtmlHelper Html,string icon, string Title, params PanelActions[] panelActions)
        {
            var div = new TagBuilder("div");    
            div.AddCssClass("panel-heading");
            div.InnerHtml = string.Empty;

            var h3 = new TagBuilder("h3");
            h3.AddCssClass("panel-title");
            h3.GenerateId(Title);
            h3.InnerHtml = Html.Raw(icon + Title).ToHtmlString();

            div.InnerHtml += Html.Raw(h3.ToString(TagRenderMode.Normal)).ToHtmlString();

            var divActions = new TagBuilder("div");
            divActions.AddCssClass("panel-actions");
            divActions.InnerHtml = string.Empty;

            foreach (var action in panelActions)
            {
                TagBuilder a = null;

                switch (action)
                {
                    case PanelActions.Collapse:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-minus");
                        a.Attributes.Add("data-toggle", "panel-collapse");

                        break;
                    case PanelActions.Fullscreen:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-expand");
                        a.Attributes.Add("data-toggle", "panel-fullscreen");

                        break;
                    case PanelActions.Refresh:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-refresh");
                        a.Attributes.Add("data-toggle", "panel-refresh");

                        break;
                    case PanelActions.Close:
                        a = new TagBuilder("a");

                        a.AddCssClass("panel-action icon wb-close");
                        a.Attributes.Add("data-toggle", "panel-close");

                        break;
                    default:
                        break;
                }

                if (a != null)
                {
                    a.Attributes.Add("aria-hidden", "true");

                    divActions.InnerHtml += Html.Raw(a.ToString(TagRenderMode.Normal)).ToHtmlString();
                }
            }

            div.InnerHtml += Html.Raw(divActions.ToString(TagRenderMode.Normal)).ToHtmlString();

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// Devuelve un panel footer que contiene los botones de acción
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="ExportButton"></param>
        /// <returns></returns>
        public static MvcHtmlString PanelFooter(this HtmlHelper Html, string submitButtonText, string resetButtonText = "", string exportButtonText = "", string submitButtonClass = "btn-primary")
        {
            var div = new TagBuilder("div");
            div.AddCssClass("panel-footer text-right");
            div.InnerHtml = string.Empty;

            if (!string.IsNullOrWhiteSpace(resetButtonText))
            {
                var iconResetReport = new TagBuilder("i");
                iconResetReport.AddCssClass("fa fa-refresh");
                iconResetReport.Attributes.Add("aria-hidden", "true");

                var btnResetReport = new TagBuilder("button");
                btnResetReport.AddCssClass("btn btn-default");
                btnResetReport.InnerHtml = iconResetReport.ToString(TagRenderMode.Normal) + " " + resetButtonText;
                btnResetReport.Attributes.Add("type", "button");
                btnResetReport.Attributes.Add("id", "btnResetReport");

                div.InnerHtml += Html.Raw(btnResetReport.ToString(TagRenderMode.Normal) + "&nbsp;").ToHtmlString();
            }

            if (!string.IsNullOrWhiteSpace(exportButtonText))
            {
                var btnGroup = new TagBuilder("div");
                btnGroup.AddCssClass("btn-group");
                btnGroup.Attributes.Add("role", "group");
                btnGroup.InnerHtml = string.Empty;

                var btnExportReport = new TagBuilder("button");
                btnExportReport.AddCssClass("btn btn-primary dropdown-toggle");
                btnExportReport.InnerHtml = Html.Raw("<i class=\"fa fa-file-o\" aria-hidden=\"true\"></i> " + exportButtonText +"<span class=\"caret\"></span>").ToHtmlString();
                btnExportReport.Attributes.Add("type", "button");
                btnExportReport.Attributes.Add("id", "btnExportReport");
                btnExportReport.Attributes.Add("data-toggle", "dropdown");
                btnExportReport.Attributes.Add("aria-expanded", "false");

                btnGroup.InnerHtml += Html.Raw(btnExportReport.ToString(TagRenderMode.Normal)).ToHtmlString();

                var ul = new TagBuilder("ul");
                ul.AddCssClass("dropdown-menu");
                ul.Attributes.Add("role", "menu");
                ul.InnerHtml = string.Empty;

                ul.InnerHtml += Html.Raw("<li><a id=lnkPdf href=# role=menuitem data-format=PDF export-link><i class=\"fa fa-file-pdf-o\"></i> PDF</a></li>").ToHtmlString();
                ul.InnerHtml += Html.Raw("<li><a id=lnkExcel href=# role=menuitem data-format=EXCEL export-link><i class=\"fa fa-file-excel-o\"></i> Excel</a></li>").ToHtmlString();
                ul.InnerHtml += Html.Raw("<li><a id=lnkWord href=# role=menuitem data-format=WORD export-link><i class=\"fa fa-file-word-o\"></i> Word</a></li>").ToHtmlString();

                btnGroup.InnerHtml += Html.Raw(ul.ToString(TagRenderMode.Normal)).ToHtmlString();

                div.InnerHtml += Html.Raw(btnGroup.ToString(TagRenderMode.Normal) + "&nbsp").ToHtmlString();
            }

            var iconGenerateReport = new TagBuilder("i");
            iconGenerateReport.AddCssClass("fa fa-search");
            iconGenerateReport.Attributes.Add("aria-hidden", "true");

            var btnGenerateReport = new TagBuilder("button");
            btnGenerateReport.AddCssClass("btn " + submitButtonClass);
            btnGenerateReport.InnerHtml = iconGenerateReport.ToString(TagRenderMode.Normal) + " " + submitButtonText;
            btnGenerateReport.Attributes.Add("type", "submit");
            btnGenerateReport.Attributes.Add("id", "btnGenerateReport");

            div.InnerHtml += Html.Raw(btnGenerateReport.ToString(TagRenderMode.Normal)).ToHtmlString();

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }
        #endregion
    }
}
