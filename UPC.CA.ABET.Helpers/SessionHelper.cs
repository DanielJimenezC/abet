﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace UPC.CA.ABET.Helpers
{
    public static class SessionHelper
    {
        #region Set & Get
        public static object Get(HttpSessionStateBase Session, SessionKey Key)
        {
            return Session[Key.ToString()];
        }
        public static object Get(HttpSessionState Session, SessionKey Key)
        {
            return Session[Key.ToString()];
        }
        public static void Set(this HttpSessionStateBase Session, SessionKey Key, object value)
        {
            Session[Key.ToString()] = value;
        }
        public static void Set(this HttpSessionState Session, SessionKey Key, object value)
        {
            Session[Key.ToString()] = value;
        }
        #endregion

        #region IsLoggedIn
        public static Boolean IsLoggedIn(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.UsuarioId) != null;
        }

        public static Boolean IsLoggedIn(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.UsuarioId) != null;
        }
        #endregion

        #region GetNombre
        public static String GetImagen(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Imagen).ToSafeString();
        }
        public static String GetImagen(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Imagen).ToSafeString();
        }
        #endregion

        #region GetDocenteId
        public static Int32? GetDocenteId(this HttpSessionStateBase Session)
        {
            return (Int32?)Get(Session, SessionKey.DocenteId);
        }
        public static Int32? GetDocenteId(this HttpSessionState Session)
        {
            return (Int32?)Get(Session, SessionKey.DocenteId);
        }
        #endregion

        #region GetUsuarioId
        public static Int32? GetUsuarioId(this HttpSessionStateBase Session)
        {
            return (Int32?)Get(Session, SessionKey.UsuarioId);
        }
        public static Int32? GetUsuarioId(this HttpSessionState Session)
        {
            return (Int32?)Get(Session, SessionKey.UsuarioId);
        }

        #endregion

        #region GetNombre
        public static String GetNombre(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Nombre).ToString();
        }
        public static String GetNombre(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Nombre).ToString();
        }
        #endregion

        #region GetPeriodoAcademico
        public static Int32? GetPeriodoAcademicoId(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.PeriodoAcademicoId).ToInteger();
        }
        public static Int32? GetPeriodoAcademicoId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.PeriodoAcademicoId).ToInteger();
        }
		
        public static String GetPeriodoAcademico(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.PeriodoAcademico).ToString();
        }

        #endregion

        #region GetPeriodoEPE 
        public static Int32? GetPeriodoEPEId(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.PeriodoEPEId).ToInteger();
        }
        public static Int32? GetPeriodoEPEId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.PeriodoEPEId).ToInteger();
        }
        public static String GetPeriodoEPE(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.PeriodoEPE).ToString();
        }
        #endregion

        #region GetSubModalidadPeriodoAcademico
        public static Int32? GetSubModalidadPeriodoAcademicoId(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.SubModalidadPeriodoAcademicoId).ToInteger();
        }
        public static Int32? GetSubModalidadPeriodoAcademicoId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.SubModalidadPeriodoAcademicoId).ToInteger();
        }
        public static String GetSubModalidadPeriodoAcademico(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.SubModalidadPeriodoAcademico).ToString();
        }
        public static String GetSubModalidadPeriodoAcademico(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.SubModalidadPeriodoAcademico).ToString();
        }
        #endregion

        #region GetApellido
        public static String GetApellido(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Apellido).ToString();
        }
        public static String GetApellido(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Apellido).ToString();
        }
        #endregion

        #region GetCulture
        public static object GetCulture(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Culture);
        }
        public static object GetCulture(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Culture);
        }
        #endregion

        #region GetRol
        public static AppRol GetRol(this HttpSessionStateBase Session)
        {
            return (AppRol)Get(Session, SessionKey.Rol);
        }
        public static AppRol GetRol(this HttpSessionState Session)
        {
            return (AppRol)Get(Session, SessionKey.Rol);
        }
        #endregion

        #region GetDictRoles
        public static Dictionary<int, AppRol[]> GetDictRoles(this HttpSessionStateBase Session)
        {
            return (Dictionary<int, AppRol[]>)Get(Session, SessionKey.DictRoles);
        }
        public static Dictionary<int, AppRol[]> GetDictRoles(this HttpSessionState Session)
        {
            return (Dictionary<int, AppRol[]>)Get(Session, SessionKey.DictRoles);
        }
        #endregion

        #region GetRoles
        public static AppRol[] GetRoles(this HttpSessionStateBase Session)
        {
            return (AppRol[])Get(Session, SessionKey.Roles);
        }
        public static AppRol[] GetRoles(this HttpSessionState Session)
        {
            return (AppRol[])Get(Session, SessionKey.Roles);
        }
        #endregion

        #region GetEmail
        public static object GetEmail(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Email).ToString();
        }
        public static object GetEmail(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Email).ToString();
        }
        #endregion

        #region GetCodigo
        public static object GetCodigo(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Codigo).ToString();
        }
        public static object GetCodigo(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Codigo).ToString();
        }
        #endregion

        #region GetUsuarioSecundarioId
        public static Int32? GetUsuarioSecundarioId(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.UsuarioSecundarioId).ToInteger();
        }
        public static Int32? GetUsuarioSecundarioId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.UsuarioSecundarioId).ToInteger();
        }
        #endregion

        #region GetCodigoSecundario
        public static String GetCodigoSecundario(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.CodigoSecundario).ToSafeString();
        }
        public static String GetCodigoSecundario(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.CodigoSecundario).ToSafeString();
        }
        #endregion

        #region GetEscuelaId
        public static Int32 GetEscuelaId(this HttpSessionStateBase Session)
        {

            return Get(Session, SessionKey.EscuelaId).ToInteger();
        }
        public static Int32 GetEscuelaId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.EscuelaId).ToInteger();
        }
        #endregion       

        #region GetNombreEscuela
        public static String GetNombreEscuela(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.NombreEscuela).ToString();
        }
        public static String GetNombreEscuela(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.NombreEscuela).ToString();
        }
        #endregion

        #region GetNombreModalidad
        public static String GetNombreModalidad(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.NombreModalidad).ToString();
        }
        public static String GetNombreModalidad(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.NombreModalidad).ToString();
        }
        #endregion

        #region GetModalidadId
        public static Int32 GetModalidadId(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.ModalidadId).ToInteger();
        }
        public static Int32 GetModalidadId(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.ModalidadId).ToInteger();
        }
        #endregion

        public static object GetModalidad(this HttpSessionStateBase Session)
        {
            return Get(Session, SessionKey.Modality);
        }
        public static object GetModalidad(this HttpSessionState Session)
        {
            return Get(Session, SessionKey.Modality);
        }

		#region EVD
		public static Int32 GetEncuestaTokenId(this HttpSessionStateBase Session)
		{
			return Get(Session, SessionKey.EncuestaTokenId).ToInteger();
		}
		public static Int32 GetEncuestaTokenId(this HttpSessionState Session)
		{
			return Get(Session, SessionKey.EncuestaTokenId).ToInteger();
		}
		public static Int32 GetEncuestaVirtualDelegadoId(this HttpSessionStateBase Session)
		{
			return Get(Session, SessionKey.EncuestVirtualDelegadoId).ToInteger();
		}
		public static Int32 GetEncuestaVirtualDelegadoId(this HttpSessionState Session)
		{
			return Get(Session, SessionKey.EncuestVirtualDelegadoId).ToInteger();
		}
		#endregion
	}
}