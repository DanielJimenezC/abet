﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UPC.CA.ABET.Services.Controllers
{
    [RoutePrefix("api/courses")]
    public class CourseController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("academic_period/{cycle}")]
        public IHttpActionResult AcademicPeriod(string cycle)
        {
            dynamic response = new ExpandoObject();
            try
            {
                //response.Cursos = context.Curso.ToList();
                response = (from c in context.Curso
                            join sc in context.SeccionCurso on c.IdCurso equals sc.IdCurso
                            join s in context.Seccion on sc.IdSeccion equals s.IdSeccion
                            join ds in context.DocenteSeccion on s.IdSeccion equals ds.IdSeccion
                            join cpa in context.CursoPeriodoAcademico on s.IdCursoPeriodoAcademico equals cpa.IdCursoPeriodoAcademico
                            join smpa in context.SubModalidadPeriodoAcademico on cpa.IdSubModalidadPeriodoAcademico equals smpa.IdSubModalidadPeriodoAcademico
                            join pa in context.PeriodoAcademico on smpa.IdPeriodoAcademico equals pa.IdPeriodoAcademico
                            join m in context.Modulo on s.IdModulo equals m.IdModulo
                            join cmp in context.Sede on s.IdSede equals cmp.IdSede
                            join ccpa in context.CarreraCursoPeriodoAcademico on cpa.IdCursoPeriodoAcademico equals ccpa.IdCursoPeriodoAcademico
                            join ca in context.Carrera on ccpa.IdCarrera equals ca.IdCarrera

                            where pa.CicloAcademico == cycle
                            && (m.IdentificadorSeccion == "B" || m.IdentificadorSeccion == "A") //B -> EPE
                            select new
                            {
                                CodigoCurso = c.Codigo,
                                Curso = c.NombreEspanol,
                                SeccionCurso = s.Codigo,
                                Periodo = pa.CicloAcademico,
                                CodigoCarreraCurso = ca.Codigo,
                                Carrera = ca.NombreEspanol,
                                CodigoDocente = ds.Docente.Codigo,
                                NombreDocente = ds.Docente.Nombres + " " + ds.Docente.Apellidos,
                                Modalidad = smpa.SubModalidad.Modalidad.NombreEspanol,
                                Modulo = m.IdentificadorSeccion,
                                Campus = cmp.Nombre
                            }
                                    ).ToList();

                return Ok(response);
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                return Content(HttpStatusCode.InternalServerError, response);
            }
        }
    }
}