﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UPC.CA.ABET.Models;

namespace UPC.CA.ABET.Services.Controllers
{
    public class BaseController : ApiController
    {
        public AbetEntities context= new AbetEntities();
    }
}
