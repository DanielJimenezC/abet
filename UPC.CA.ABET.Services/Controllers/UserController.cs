﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UPC.CA.ABET.Logic.Areas.Admin;
using UPC.CA.ABET.Presentation.ViewModels.Home;

namespace UPC.CA.ABET.Services.Controllers
{
    [RoutePrefix("api/users")]
    public class UserController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult Login(LoginViewModel model)
        {
            dynamic response = new ExpandoObject();
            try
            {
                var security = new SecurityLogic();
                String usuario = model.Usuario.ToLower();
                String pass = model.Password;

                var usuario_pass = (from user in context.Usuario
                                    join roluser in context.RolUsuario on user.IdUsuario equals roluser.IdUsuario
                                    join rol in context.Rol on roluser.IdRol equals rol.IdRol
                                    where user.Codigo == usuario
                                    && (rol.Acronimo == "CCA" || rol.Acronimo == "DCA") //Coordinador
                                    select new
                                    {
                                        Password = user.Password
                                    }
                            ).FirstOrDefault();

                String pass_bd = (usuario_pass.Password);

                if (security.AreEqualMD5(pass, pass_bd))
                {
                    response = (from user in context.Usuario
                                join roluser in context.RolUsuario on user.IdUsuario equals roluser.IdUsuario
                                join rol in context.Rol on roluser.IdRol equals rol.IdRol
                                where user.Codigo == usuario
                                && (rol.Acronimo == "CCA" || rol.Acronimo == "DCA")
                                select new
                                {
                                    Usuario = user.Codigo,
                                    Nombre = user.Nombres + " " + user.Apellidos,
                                    Cargo = rol.Descripcion

                                }
                           ).ToList().FirstOrDefault();
                    return Ok(response);
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, response);
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                return Content(HttpStatusCode.InternalServerError, response);
            }
        }
    }
}